# Swagger/OpenAPI Framework and Connectors

This project includes a Java framework that extends the Boomi Connector SDK interfaces to ease development of branded connectors on top of swagger/OpenAPI compliant products. 

**This project has both the framework and branded connectors. The source of the framework itself is contained in src/main/java/com/boomi/swaggerframework. The framework will be broken out into a separate swagger project but for now it is bundled with the other connectors.** 

Documentation for individual connectors is available here: https://bitbucket.org/officialboomi/swagger-openapi-framework-and-connectors/src/master/documentation/

Instructions for uploading private connectors to your account are here: https://help.boomi.com/bundle/connectors/page/t-atm-Adding_a_connector_group.html . Download connector descriptor (xml) and archive (zip) files from [HERE](../../downloads).

There is documentation on individual connectors [[HERE](./documentation)]

Although the framework itself is solid and improved iteratively, most of the connectors built on it have not been verified due to the lack of sandboxes, SMEs and free hands to actually verify them. Volunteers are welcome! For new systems configuration needs to be done for query splitter/pagination, authentication, operations supported, select/filter/sort for Query operations.

To report issues, please click on the **Give Feedback** button on the lower left.

The framework has built in functionality for commodity type operations such as 

 * Presenting the list of types during Import (Browse)
 * Generation of Boomi operations and request/response profiles
 * Basic, OAuth 2.0 Authentication
 * Commodity operations such as Create, Get, Update and Delete
 * Interfaces to ease the implementation of the Query operation including support for pagination and field selection/filtering/sorting
 * Extensions to the SDK test framework to ease TDD/Test-First development
 * Ability to auto-generate comprehensive documentation for each connector in help.boomi.com format
 	* UI documentation generated automatically from the "descriptor" file from which the UI is rendered in the first place
 	* A list of all Operation/Types with descriptions of each type rendered from the swagger file. (This is something we never had in our documentation)
 	* Browse the __documentation__ folder above for samples
 
All functionality can be overridden but for most connectors, the out of the box behavior provides most of the functionality required. For example, the Visa Cybersource connector less than 100 lines of code to implement a proprietary HTTP Signature Authentication Mechanism. The Boomi DCP connector required only 100 lines of code to implement the Query operation.

A draft boomi community article describing the framework is available here: https://docs.google.com/document/d/1EYTPCcJEtHe4-xwehYTCqkrt-_7jVvfUlRkGHuXsX94
 
This project also includes 20+ implementations of connectors built on the framework.

## Continuous Integration/Continuous Deployment Deployment (CICD) Methodology

A CICD/Test Driven Development approach ensures high quality and high velocity of new features and fixes. With each check-in of source code changes, all connectors are tested automatically and deployed to the __Downloads__ page on the left side of this page.

Test automation runs right here for every code change checked in. It is not required to upload a connector to the Boomi platform and running in an actual process in order to get good test coverage. The tests run for each build in minutes, would take days to be manually performed by an actual person using the platform. Test automation for each connector includes:

 * Testing of all operation types, simulating a user doing an Import in the platform UI for every single operation
 * Testing of profile generation, simulating a user clicking _Next_ during import and verifying that the profiles and the operation where generated OK.
 * Live operation testing against real systems by performing queries, creates etc. Since it is a shared platform, theoretically if one connector operates live OK, others will to. Still there is no substitution for running a specific connector against a specific system. Live operation verification has been performed for:
 	* Oracle Cloud Finance
 	* Visa Cybersource
 	* Stripe
 	* Boomi DCP
 	* SAP Concur
 	* Kiteworks
 	* Workday REST
 	* OKTA
 	* Data.World
 	

## Current Connector List

Connectors can be downloaded from [here](https://bitbucket.org/davehock/boomi-swagger-openapi-framework-and-connectors/downloads/)

 1. [Amdocs](https://www.amdocs.com/about)
 1. Aquarius - Aquarius provides an off-chain database cache for metadata that is published on-chain. This enables faster query operations on datasets metadata. https://github.com/oceanprotocol/aquarius
 1. [Blackboard](https://www.blackboard.com/)
 1. Boomi Data Catalog and Prep (DCP)
 1. Boomi Flow
 1. Brizo - a component providing capabilities for Publishers to interact with their cloud or on-premise infrastructure. https://docs.oceanprotocol.com/concepts/architecture
 1. [Cybersource - a Visa solution](https://www.cybersource.com/) - payment acceptance
 1. Dell PowerStore
 1. Docusign - document and signature management
 1. Dropbox - content management
 1. Experian - Customer contact verification
 1. Facebook Marketing API (Need more swaggers)
 1. Flexport - logistics
 1. [Intapp](https://www.intapp.com/)
 1. JD Edwards - ERP
 1. Adobe Magento - ecommerce platform
 1. Marketo - email marketing
 1. M-Pesa: M-Pesa is a mobile phone-based money transfer service, payments and micro-financing service, launched in 2007 by Vodafone Group plc and Safaricom, the largest mobile network operator in Kenya. https://www.vodafone.com/what-we-do/services/m-pesa
 1. [Okta](https://www.okta.com/) - Identity Management
 1. [Open Bank](https://www.openbankproject.com/) - The leading Open API middleware for banking
 1. Oracle Cloud/Fusion Apps (HCM, Finance, Supply Chain, Service...)
 1. [PagerDuty](https://www.pagerduty.com/) - IT Response Management
 1. Petstore - A connector built on top of the Open API reference API implemented with no code using the framework.
 1. Salesforce IOT API
 1. SAP Concur
 1. Shopify - ecommerce platform
 1. Stripe - payment solutions
 1. Workday - HCM
 1. Worldpay - payment solutions

## TODOs
 * (COMPLETE) x-operations for Oracle Finance, HCM
 * (COMPLETE) Stripe uses form based encoded requests. We can use a JSON Profile for this but translation required at runtime
 * Oracle query filter grammars vary across cloud products, maybe break each into its own connector?
 * Need XPATH support in JSONSplitter for query response next page URL when document is in the form of /links/[type='next']/url . This is a rare case but FHIR is one API that uses this construct.
 * Docusign and Openbank Connection.getQueryPaginationItemPath can't be static as the object name is in the path. Need some sort of wild card to resolve down to the items/* array
 * Create cookie for basepath and request/response types. (WARNING Cybersource base path ends in / (/pmts/) so strip doubles // to workaround
 * allOf, oneOf, anyOf needs work for Stripe
 * Tune and verify Query Operations for many of the connectors
 	* isQuery
 	* JSONSplitter pagination items
 	* select, filter, sort
 	* expand...
 * Improve Test Tools
 	* Iterate and verify all object definitions for all operations
 	* Test more operations live
 	* Validate operation responses against json schema
 * Implementation of fault tolerant capabilities such as retry
 
## Sandboxes
 * Stripe
 * Cybersource
 * Oracle Finance
 * Petstore
 * Docusign (pending)
 * Dropbox (pending)
 * Worldpay (but haven't tried API)

### ORACLE

Maybe use basepath to build url? Or let user do manually in Connection... 
               
    "basePath":"/fscmRestApi/resources/11.13.18.05",
    "paths":{
        "/ledgerBalances":{
            "get":{
            
            
Oracle has a items array for pagination of query results. It uses offset values for pagination

https://docs.oracle.com/en/cloud/saas/supply-chain-management/20d/fasrp/use_case_get_list_of_categories.html

            
Oracle uses an "expand" model to drill down to a lower level related info

	{
		"OrganizationCode": "M1",
		"ItemNumber": "VC110",
		"PrimaryUnitOfMeasure": "Each",
		"QuantityOnhand": 2,
		"ReturnStatus": "SUCCESS",
		"ReturnMessageName": null,
		"ReturnMessageText": null,
		"links": [
		{
			"rel": "self",
			"href": "https://servername/fscmRestApi/resources/version/availableQuantityDetails/99999",
			"name": "availableQuantityDetails",
			"kind": "item"
		},
		{
			"rel": "canonical",
			"href": "https://servername/fscmRestApi/resources/version/availableQuantityDetails/99999",
			"name": "availableQuantityDetails",
			"kind": "item"
		}
		]
	}

### OPEN BANKING
- has query
- pagination has links response structure /Links/Next
- TODO WHAT IS PATH TO AN ITEM IN RESULT SET

    "Links": {
      "type": "object",
      "description": "Links relevant to the payload",
      "properties": {
        "Self": {
          "type": "string",
          "format": "uri"
        },
        "First": {
          "type": "string",
          "format": "uri"
        },

- no query params

- params must be resolved
     
     "post": {
        "tags": [
          "Account Access"
        ],
        "summary": "Create Account Access Consents",
        "operationId": "CreateAccountAccessConsents",
        "parameters": [
          {
            "$ref": "#/parameters/OBReadConsent1Param"
          },
 
 parameter info resolves at root. This is bad because the path cookie requires having the parameters available. The only solution is to make the path cookie have a custom structure and not just raw json.
 
	"parameters": [
             "OBReadConsent1Param": {
      "name": "OBReadConsent1Param",
      "in": "body",
      "description": "Default",
      "required": true,
      "schema": {
        "$ref": "#/definitions/OBReadConsent1"
      }
    },
	
        "responses": {
          "200": {
            "$ref": "#/responses/200AccountsRead"
          },
responses resolve at root

	"responses":[

    "200AccountsRead": {
      "description": "Accounts Read",
      "headers": {
        "x-fapi-interaction-id": {
          "type": "string",
          "description": "An RFC4122 UID used as a correlation id."
        }
      },
      "schema": {
        "$ref": "#/definitions/OBReadAccount2"
      }
    },
    
    "OBReadAccount2": {
      "type": "object",
      "properties": {
        "Data": {
          "type": "object",
          "properties": {
            "Account": {
              "items": {
                "$ref": "#/definitions/OBAccount2"
              },
              "type": "array",
              "description": "Unambiguous identification of the account to which credit and debit entries are made."
            }
          },
          "additionalProperties": false
        },		  
- Header parameters - handle these with input document properties? 
		  
	
