package com.boomi.swaggerframework;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;

import com.boomi.connector.api.OAuth2Context;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PrivateKeyStore;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.PublicKeyStore;
import com.boomi.swaggerframework.swaggerutil.JSONResponseSplitter;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.util.IOUtil;
import com.boomi.util.json.splitter.JsonSplitter;


class SwaggerBrowseUtilTest {

	String payload =" {\"id\":\"33\",\"name\":\"thename\"}";
	@Test
	void testgetLastID() throws Exception {
		
		InputStream is = new ByteArrayInputStream(payload.getBytes());
		OutputStream os = new ByteArrayOutputStream();
		String id = SwaggerBrowseUtil.getLastID("/id", is, System.out);
		assertEquals("33",id);
	}
	
	@Test
	void testgetURLPathWithParameterValues() throws Exception
	{
        PropertyMap opProps = new MockPropertyMap();
        opProps.put(SwaggerBrowseUtil.PATH_PARAMETER_FIELD_ID_PREFIX+"meetingId", "XXXX");
        opProps.put(SwaggerBrowseUtil.PATH_PARAMETER_FIELD_ID_PREFIX+"userId", "YYYY");
        opProps.put(SwaggerBrowseUtil.PATH_PARAMETER_FIELD_ID_PREFIX+"id", 10);
        opProps.put(SwaggerBrowseUtil.PATH_PARAMETER_FIELD_ID_PREFIX+"refresh", true);
		String path = SwaggerBrowseUtil.getURLPathWithParameterValues("/meetings/{meetingId}/attendees/{userId}___get", opProps, null);
		assertEquals("/meetings/XXXX/attendees/YYYY",path);

		path = SwaggerBrowseUtil.getURLPathWithParameterValues("/meetings/{meetingId}/attendees/{userId}/users___get", opProps, null);
		assertEquals("/meetings/XXXX/attendees/YYYY/users",path);

		path = SwaggerBrowseUtil.getURLPathWithParameterValues("/meetings/{id}/attendees/{refresh}/users___get", opProps, null);
		assertEquals("/meetings/10/attendees/true/users",path);
	
	}
	
	class MockPropertyMap implements PropertyMap
	{
		Map<String, Object> properties;
		MockPropertyMap()
		{
			properties = new HashMap<String,Object>();
		}
		@Override
		public int size() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public boolean isEmpty() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean containsKey(Object key) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean containsValue(Object value) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public Object get(Object key) {
			// TODO Auto-generated method stub
			return properties.get(key);
		}

		@Override
		public Object put(String key, Object value) {
			// TODO Auto-generated method stub
			return properties.put(key, value);
		}

		@Override
		public Object remove(Object key) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void putAll(Map<? extends String, ? extends Object> m) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void clear() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Set<String> keySet() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Collection<Object> values() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Set<Entry<String, Object>> entrySet() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getProperty(String key) {
			// TODO Auto-generated method stub
			return (String)properties.get(key);
		}

		@Override
		public String getProperty(String key, String defaultValue) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Boolean getBooleanProperty(String key) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Boolean getBooleanProperty(String key, Boolean defaultValue) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Long getLongProperty(String key) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Long getLongProperty(String key, Long defaultValue) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public PrivateKeyStore getPrivateKeyStoreProperty(String key) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public PublicKeyStore getPublicKeyStoreProperty(String key) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public OAuth2Context getOAuth2Context(String key) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Map<String, String> getCustomProperties(String key) {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
}
