package com.boomi.swaggerframework;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import org.junit.jupiter.api.Test;
import com.boomi.connector.api.Payload;
import com.boomi.swaggerframework.swaggerutil.JSONResponseSplitter;
import com.boomi.util.IOUtil;
import com.boomi.util.json.splitter.JsonSplitter;


class JSONResponseSplitterTest {

	@Test
	void testJSONResponseSplitter() throws IOException {
        JSONResponseSplitter respSplitter = null;
        try {
            // split "list" result document into multiple payloads and process each as a partial result
            respSplitter = new JSONResponseSplitter(this.getClass().getClassLoader().getResourceAsStream("resources/blackboard/queryresponse.json"),
            		"/results/*", "/paging/nextPage", null);

            int count = 0;
            for(Payload p : respSplitter) 
            {
            	count++;
            	p.writeTo(System.out);
            	System.out.println();
            }
            
            assertEquals("http://api.com/nextpagetoken", respSplitter.getNextPageElementValue());
            assertEquals(3, count);
            String nextOffset = respSplitter.getNextPageElementValue();           
            System.out.println(nextOffset);
        } finally {
            IOUtil.closeQuietly(respSplitter);
        }
	}
}
