package com.boomi.swaggerframework;

import static org.junit.jupiter.api.Assertions.*;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.swaggerutil.SwaggerUtil;

class SwaggerUtilTest {

	String json = "{\r\n" + 
			"  \"amount\" : 10,\r\n" + 
			"  \"card\" : {\r\n" + 
			"    \"cvc\" : \"222\",\r\n" + 
			"    \"exp_month\" : 12,\r\n" + 
			"    \"exp_year\" : 23,\r\n" + 
			"    \"number\" : \"4242424242424242\"\r\n" + 
			"  },\r\n" + 
			"  \"currency\" : \"usd\"\r\n" + 
			"}";
	@Test
	void testJsonToUrlFormEncoded() {
		String form = SwaggerUtil.jsonToUrlFormEncoded(new JSONObject(json));
		assertEquals("amount=10&currency=usd&card[cvc]=222&card[number]=4242424242424242&card[exp_month]=12&card[exp_year]=23",  form);
	}

}
