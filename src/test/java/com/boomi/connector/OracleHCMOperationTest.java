// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.Sort;
import com.boomi.connector.oraclesaas.OracleSaaSConnector;
import com.boomi.swaggerframework.MockOperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerExecuteOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.swaggerframework.swaggerutil.JSONUtil;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.QueryFilterBuilder;
import com.boomi.connector.testutil.QueryGroupingBuilder;
import com.boomi.connector.testutil.QuerySimpleBuilder;
import com.boomi.connector.testutil.SimpleOperationResult;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Dave Hock
 */
public class OracleHCMOperationTest 
{
    private static final String URL = "https://ucf5-zsjk-fa-ext.oracledemos.com/hcmRestApi/resources/11.13.18.05";
    private static final String USERNAME = "XXXXX";
    private static final String PASSWORD = "XXXXX";
    private static final String SWAGGER_URL = "resources/oraclesaas/human-resouces 20c.json";

    @Test
    public void testGetEmployeeOperation() throws Exception
    {
    	String objectTypeId = "/emps/{empsUniqID}___get";
        OracleSaaSConnector connector = new OracleSaaSConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.name(), URL);
        connProps.put(SwaggerConnection.ConnectionProperties.USERNAME.name(), USERNAME);
        connProps.put(SwaggerConnection.ConnectionProperties.PASSWORD.name(), PASSWORD);
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), SWAGGER_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
//        opProps.put(SwaggerExecuteOperation.OperationProperties.URL_PATH.name(), "https://ucf5-ztuy-fa-ext.oracledemos.com/hcmRestApi/resources/11.13.18.05/emps/00020000000EACED00057708000110D93445295F0000004AACED00057372000D6A6176612E73716C2E4461746514FA46683F3566970200007872000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001794946140078");
        
		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, objectTypeId, null, null);
        context.setCustomOperationType("GET");	
		tester.setOperationContext(context);

		List <SimpleOperationResult> actual = tester.executeGetOperation("6074542101066456503005");
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }

    @Test
    public void testQueryEmployeeOperation() throws Exception
    {
    	String objectTypeId = "/emps___get";
        OracleSaaSConnector connector = new OracleSaaSConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        Map<String, Object> opProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.name(), URL);
        connProps.put(SwaggerConnection.ConnectionProperties.USERNAME.name(), URL);
        connProps.put(SwaggerConnection.ConnectionProperties.PASSWORD.name(), PASSWORD);
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), SWAGGER_URL);
        opProps.put(SwaggerQueryOperation.OperationProperties.MAXDOCUMENTS.name(), 28L);
        
        QueryFilter qf =  new QueryFilterBuilder(QueryGroupingBuilder.and(
                new QuerySimpleBuilder("gender", "eq", "male"),
//                new QuerySimpleBuilder("given", "eq", "Niel"),              
                new QuerySimpleBuilder("_lastUpdated", "ge", "2019-09-01"))).toFilter();
        Sort sort1 = new Sort();
        sort1.setSortOrder("desc");
        sort1.setProperty("given");
//        Sort sort2 = new Sort();
//        sort2.setSortOrder("asc");
//        sort2.setProperty("gender");
//        qf.withSort(sort1, sort2);//2 sorts throwing 500, 
//        qf.withSort(sort1); //sort not working returns <issue><diagnostics value="ERROR: canceling statement due to user request"/>

        tester.setOperationContext(OperationType.QUERY, connProps, opProps, objectTypeId, null);
        List <SimpleOperationResult> actual = tester.executeQueryOperation(qf);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(28, actual.get(0).getPayloads().size());
        String actualString = new String(actual.get(0).getPayloads().get(0));
        System.out.println(actualString);
    }
}
