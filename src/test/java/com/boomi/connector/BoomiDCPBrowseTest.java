// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.boomi_dcp.BoomiDCPConnector;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;

/**
 * @author Dave Hock
 */
public class BoomiDCPBrowseTest {
    static final String connectorName = "boomi_dcp";
    private static String swaggerURI = null; //URI loaded in connection implementation
    private static final boolean captureExpected = false;
    
    @Test
    public void testBrowseTypes() throws JSONException, Exception
    {
    	BoomiDCPConnector connector = new BoomiDCPConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerURI, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerURI, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerURI, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerURI, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerURI, connectorName, "", getClass(), captureExpected);
    }
    
    @Test
    public void testBrowseQueryDefinitionsQueryUsers() throws JSONException, Exception
    {
    	BoomiDCPConnector connector = new BoomiDCPConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
//        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerURI);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/users/___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseQueryDefinitionsQueryUsers", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsCreateUser() throws JSONException, Exception
    {
    	BoomiDCPConnector connector = new BoomiDCPConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
//        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerURI);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/users/___post");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateUser", getClass(), connectorName, captureExpected);
    }

//TODO DCP BUG no return schema specified for 200 response   
//    @Test
    public void testBrowseDefinitionsGetUserDCPBUGMISSING200Schema() throws JSONException, Exception
    {
    	BoomiDCPConnector connector = new BoomiDCPConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
//        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerURI);
        
        tester.setBrowseContext(OperationType.GET, connProps, null);
        actual = tester.browseProfiles("/users/{id}/___get");
        //TODO DCP Bug, GET /users/{id} has no 200 response schema specified
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetUser", getClass(), connectorName, captureExpected);
    }

    @Test
	void AnalyzeSwagger() {
		System.out.println("");
		System.out.println(connectorName.toUpperCase());
		JSONObject fullSwaggerSchema = new JSONObject(new JSONTokener(this.getClass().getClassLoader().getResourceAsStream("resources/boomi_dcp/DCP-rest-api-swagger.json")));
		SwaggerDocumentationUtil.analyzeOpenAPI(fullSwaggerSchema);
	}	
}
