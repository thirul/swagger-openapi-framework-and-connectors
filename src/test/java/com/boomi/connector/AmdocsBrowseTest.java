// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.amdocs.AmdocsConnector;
import com.boomi.connector.api.OperationType;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;

/**
 * @author Dave Hock
 */
public class AmdocsBrowseTest {
    static final String connectorName = "amdocs";
    private static final boolean captureExpected = false;
	String swaggerPath = "resources/"+connectorName+"/customer-experience-management.json";
    
    @Test
    public void testBrowseTypesAmdocs() throws JSONException, Exception
    {
        AmdocsConnector connector = new AmdocsConnector();

    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }   
    
    @Test
    public void testBrowseDefinitionsGetCustomer() throws JSONException, Exception
    {
    	AmdocsConnector connector = new AmdocsConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/customer___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetCustomer", getClass(), connectorName, captureExpected);
    } 
}
