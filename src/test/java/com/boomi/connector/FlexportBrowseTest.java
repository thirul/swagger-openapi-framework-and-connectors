// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.flexport.FlexportConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class FlexportBrowseTest {
    static final String connectorName = "flexport";
    private static final String swaggerPath = "resources/flexport/flexportswagger3.json";
    private static final boolean captureExpected = false;
    
    @Test
    public void testBrowseTypes() throws JSONException, Exception
    {
    	FlexportConnector connector = new FlexportConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
    
    @Test
    //Many items are objects with no properties
    public void testBrowseDefinitionsGetInvoice() throws JSONException, Exception
    {
    	FlexportConnector connector = new FlexportConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.EXECUTE, connProps, null);
        actual = tester.browseProfiles("/invoices/{id}___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetInvoice", getClass(), connectorName, captureExpected);
    }	
    
    @Test
    //TODO Failed processing profile: com.github.fge.jsonschema.exceptions.ProcessingException: Unable to process nodes without a type
    //has properties with no items? "properties": {}
    public void testBrowseDefinitionsQueryBookings() throws JSONException, Exception
    {
    	FlexportConnector connector = new FlexportConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();      
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 30L);

        tester.setBrowseContext(OperationType.QUERY, connProps, opProps);
        actual = tester.browseProfiles("/bookings___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryBookings", getClass(), connectorName, captureExpected);
    }	
    
    @Test
    //Failed processing profile: com.github.fge.jsonschema.exceptions.ProcessingException: Unable to process nodes without a type
    public void testBrowseDefinitionsCreateBooking() throws JSONException, Exception
    {
    	FlexportConnector connector = new FlexportConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 15L);
       
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, opProps);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/bookings___post");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateBooking", getClass(), connectorName, captureExpected);
    }	
    
    @Test
    public void testBrowseDefinitionsGetCompany() throws JSONException, Exception
    {
    	FlexportConnector connector = new FlexportConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/network/company_entities/{id}___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetCompany", getClass(), connectorName, captureExpected);
    }	
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("Flexport REST API Connector", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}
