// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.dataworld.DataWorldConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class DataWorldBrowseTest {
    static final String connectorName = "dataworld";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = null;
    
    @Test
    public void testBrowseTypesCREATEDataWorld() throws JSONException, Exception
    {
    	DataWorldConnector connector = new DataWorldConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesQUERYDataWorld() throws JSONException, Exception
    {
    	DataWorldConnector connector = new DataWorldConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEDataWorld() throws JSONException, Exception
    {
    	DataWorldConnector connector = new DataWorldConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEGETDataWorld() throws JSONException, Exception
    {
    	DataWorldConnector connector = new DataWorldConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesDELETEDataWorld() throws JSONException, Exception
    {
    	DataWorldConnector connector = new DataWorldConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsQueryDatasetsDataWorld() throws JSONException, Exception
    {
        DataWorldConnector connector = new DataWorldConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/user/datasets/contributing___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryDatasetsDataWorld", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsQueryDataWorld() throws JSONException, Exception
    {
        DataWorldConnector connector = new DataWorldConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/user/datasets/contributing___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryDataWorld", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsGetDataWorld() throws JSONException, Exception
    {
        DataWorldConnector connector = new DataWorldConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/queries/{id}___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetDataWorld", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsCreateDataWorld() throws JSONException, Exception
    {
        DataWorldConnector connector = new DataWorldConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/projects/{owner}/{id}/queries___post");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateDataWorld", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("This connector provides access to the REST API for the Data World Cloud-Native enterprise data catalog solution.", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    }    
}