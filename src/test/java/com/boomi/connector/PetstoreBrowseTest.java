// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.petstore.PetstoreConnector;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;

/**
 * @author Dave Hock
 */
public class PetstoreBrowseTest {
    static final String connectorName = "petstore";
    private static String swaggerPath = null; //URI loaded in connection implementation
    private static final boolean captureExpected = false;
    
    @Test
    public void testBrowseTypes() throws JSONException, Exception
    {
    	PetstoreConnector connector = new PetstoreConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
    
    @Test
    public void testBrowseQueryDefinitionsQuery() throws JSONException, Exception
    {
    	PetstoreConnector connector = new PetstoreConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
//        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerURI);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/pet/findByTags___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQuery", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsCreatePet() throws JSONException, Exception
    {
    	PetstoreConnector connector = new PetstoreConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
//        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerURI);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/pet___post");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreatePet", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsCreateOrder() throws JSONException, Exception
    {
    	PetstoreConnector connector = new PetstoreConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
//        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerURI);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/store/order___post");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateOrder", getClass(), connectorName, captureExpected);
    }

    @Test
    public void testBrowseDefinitionsGetPet() throws JSONException, Exception
    {
    	PetstoreConnector connector = new PetstoreConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
//        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerURI);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/pet/{petId}___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetPet", getClass(), connectorName, captureExpected);
    }
}
