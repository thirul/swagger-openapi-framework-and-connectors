// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.vmwarevelocloud.VMWareVeloCloudConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class VMWareVeloCloudBrowseTest {
    static final String connectorName = "vmwarevelocloud";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/openapi.json";
    
    @Test
    public void testBrowseTypesCREATEVMWareVeloCloud() throws JSONException, Exception
    {
    	VMWareVeloCloudConnector connector = new VMWareVeloCloudConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
              
//    @Test
//    public void testBrowseTypesQUERYVMWareVeloCloud() throws JSONException, Exception
//    {
//    	VMWareVeloCloudConnector connector = new VMWareVeloCloudConnector();
//    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
//    }
       
    @Test
    public void testBrowseTypesUPDATEVMWareVeloCloud() throws JSONException, Exception
    {
    	VMWareVeloCloudConnector connector = new VMWareVeloCloudConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
//    @Test
//    public void testBrowseTypesEXECUTEGETVMWareVeloCloud() throws JSONException, Exception
//    {
//    	VMWareVeloCloudConnector connector = new VMWareVeloCloudConnector();
//    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
//    }
//       
//    @Test
//    public void testBrowseTypesEXECUTEDELETEVMWareVeloCloud() throws JSONException, Exception
//    {
//    	VMWareVeloCloudConnector connector = new VMWareVeloCloudConnector();
//    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
//    }
       
    @Test
    public void testBrowseDefinitionsQueryVMWareVeloCloud() throws JSONException, Exception
    {
        VMWareVeloCloudConnector connector = new VMWareVeloCloudConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/learn/api/public/v1/users___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryVMWareVeloCloud", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsExecuteGetVMWareVeloCloud() throws JSONException, Exception
    {
        VMWareVeloCloudConnector connector = new VMWareVeloCloudConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/learn/api/public/v1/users/{userId}___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsExecuteGetVMWareVeloCloud", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsCreateVMWareVeloCloud() throws JSONException, Exception
    {
        VMWareVeloCloudConnector connector = new VMWareVeloCloudConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/learn/api/public/v1/users___post");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateVMWareVeloCloud", getClass(), connectorName, captureExpected);
    }    
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    	SwaggerDocumentationUtil.generateDescriptorDoc("VMWare VeloCloud Orchestration API", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}