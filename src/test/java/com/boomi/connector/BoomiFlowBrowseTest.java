// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.boomi_flow.BoomiFlowConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class BoomiFlowBrowseTest {
    static final String connectorName = "boomi_flow";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/openapi.json";
    
    @Test
    public void testBrowseTypesCREATEBoomiFlow() throws JSONException, Exception
    {
    	BoomiFlowConnector connector = new BoomiFlowConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    }
              
    @Test
    public void testBrowseTypesQUERYBoomiFlow() throws JSONException, Exception
    {
    	BoomiFlowConnector connector = new BoomiFlowConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEBoomiFlow() throws JSONException, Exception
    {
    	BoomiFlowConnector connector = new BoomiFlowConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEGETBoomiFlow() throws JSONException, Exception
    {
    	BoomiFlowConnector connector = new BoomiFlowConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEDELETEBoomiFlow() throws JSONException, Exception
    {
    	BoomiFlowConnector connector = new BoomiFlowConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseDefinitionsGetBoomiFlow() throws JSONException, Exception
    {
        BoomiFlowConnector connector = new BoomiFlowConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/api/draw/1/authentication/{tenant}___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetBoomiFlow", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsCreateBoomiFlow() throws JSONException, Exception
    {
        BoomiFlowConnector connector = new BoomiFlowConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/api/draw/1/assets___post");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateBoomiFlow", getClass(), connectorName, captureExpected);
    }    
    
 //   @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    	SwaggerDocumentationUtil.generateDescriptorDoc("Boomi Flow", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}