// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.petstore.PetstoreConnector;
import com.boomi.swaggerframework.MockOperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
//Create/Get/Delete in that order
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PetstoreOperationTest 
{
    private static final String SERVICE_URL = "https://petstore.swagger.io";
    private static Long _petID;
    private static final String cookieString = "{\"basePath\" : \"/v2\"}";

    
    @Test   
    @Order(1)
    public void testCreatePetOperation() throws Exception
    {
    	String objectTypeID = "/pet___post";
        PetstoreConnector connector = new PetstoreConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.toString(), SERVICE_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();

		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.INPUT, cookieString);
		
		String payload = "{\n" + 
//				"  \"id\": 0,\n" + 
				"  \"category\": {\n" + 
				"    \"id\": 0,\n" + 
				"    \"name\": \"string\"\n" + 
				"  },\n" + 
				"  \"name\": \"doggie\",\n" + 
				"  \"photoUrls\": [\n" + 
				"    \"string\"\n" + 
				"  ],\n" + 
				"  \"tags\": [\n" + 
				"    {\n" + 
				"      \"id\": 0,\n" + 
				"      \"name\": \"string\"\n" + 
				"    }\n" + 
				"  ],\n" + 
				"  \"status\": \"available\"\n" + 
				"}";

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, objectTypeID, cookieMap, null);
        context.setCustomOperationType("CREATE");	
        tester.setOperationContext(context);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream(payload.getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        JSONObject responseObject = new JSONObject(responseString);
        _petID = responseObject.getLong("id");
        System.out.println(responseString);
    }    
    
    @Test   
    @Order(1)
    public void testUpdatePetOperation() throws Exception
    {
    	String objectTypeID = "/pet___put";
        PetstoreConnector connector = new PetstoreConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.toString(), SERVICE_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();

		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.INPUT, cookieString);
		
		String payload = "{\n" + 
//				"  \"id\": 0,\n" + 
				"\"___pathParameters\" : {\"petId\":\"%d\"},\r\n" +
				"  \"category\": {\n" + 
				"    \"id\": 0,\n" + 
				"    \"name\": \"string\"\n" + 
				"  },\n" + 
				"  \"name\": \"doggieUpdated\",\n" + 
				"  \"photoUrls\": [\n" + 
				"    \"string\"\n" + 
				"  ],\n" + 
				"  \"tags\": [\n" + 
				"    {\n" + 
				"      \"id\": 0,\n" + 
				"      \"name\": \"string\"\n" + 
				"    }\n" + 
				"  ],\n" + 
				"  \"status\": \"available\"\n" + 
				"}";

		payload = String.format(payload ,_petID);

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, objectTypeID, cookieMap, null);
        context.setCustomOperationType("UPDATE");	
        tester.setOperationContext(context);

        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream(payload.getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        JSONObject responseObject = new JSONObject(responseString);
        System.out.println(responseString);
    }    
    
    @Test   
    @Order(3)
    public void testGetPetOperation() throws Exception
    {
    	String objectTypeId = "/pet/{petId}___get";
        PetstoreConnector connector = new PetstoreConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.toString(), SERVICE_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        
        opProps.put("PATHPARAM_petId", _petID);

		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.INPUT, cookieString);
        
		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, objectTypeId, cookieMap, null);
        context.setCustomOperationType("GET");	
        tester.setOperationContext(context);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream("".getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }

    @Test 
    //TODO can't test execute/delete until we figure out how to pass the customtype
    @Order(4)
    public void testDeletePetOperation() throws Exception
    {
    	String objectTypeId = "/pet/{petId}___delete";
        PetstoreConnector connector = new PetstoreConnector();
        ConnectorTester tester = new ConnectorTester(connector);

		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.INPUT, cookieString);

		Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.toString(), SERVICE_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put("PATHPARAM_petId", _petID);

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, objectTypeId, cookieMap, null);
        context.setCustomOperationType("DELETE");	
        tester.setOperationContext(context);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream("".getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }
}
