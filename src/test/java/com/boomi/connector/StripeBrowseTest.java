// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.stripe.StripeConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;
//import com.github.fge.jsonschema.main.JsonSchemaFactory;

/**
 * @author Dave Hock
 */
public class StripeBrowseTest {
    static final String connectorName = "stripe";
    private static final String swaggerPath = "resources/stripe/stripeapi.json";
    private static final boolean captureExpected = false;
    
    @Test
    public void testBrowseTypes() throws JSONException, Exception
    {
    	StripeConnector connector = new StripeConnector();
       	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsGetAccount() throws JSONException, Exception
    {
    	StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 2L);

        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, opProps);
        tester.setBrowseContext(sbc);
 //       actual = tester.browseProfiles("/v1/account/bank_accounts/{id}___get");
        actual = tester.browseProfiles("/v1/accounts/{account}___get");
        
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetAccount", getClass(), connectorName, captureExpected);
    }	
    
    @Test
    //TODO returns object with no items
    public void testBrowseDefinitionsGetCustomer() throws JSONException, Exception
    {
    	StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 2L);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, opProps);
        tester.setBrowseContext(sbc);
 //       actual = tester.browseProfiles("/v1/account/bank_accounts/{id}___get");
        actual = tester.browseProfiles("/v1/customers/{customer}___get");
        
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetCustomer", getClass(), connectorName, captureExpected);
    }	
    
    @Test
    public void testBrowseDefinitionsGetBalance() throws JSONException, Exception
    {
    	StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 2L);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, opProps);
        tester.setBrowseContext(sbc);
 //       actual = tester.browseProfiles("/v1/account/bank_accounts/{id}___get");
        actual = tester.browseProfiles("/v1/balance___get");
        
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetBalance", getClass(), connectorName, captureExpected);
    }	
    
    @Test
    //TODO throws Failed processing profile: com.github.fge.jsonschema.exceptions.ProcessingException: Unable to process nodes without a type
    //Caused by polymorphic anyof? that yields object with "properties": {}
    //    "default_source": {
//        "anyOf": [
//          {
//            "maxLength": 5000,
//            "type": "string"
//          },
//          {
//            "$ref": "#/components/schemas/alipay_account"
//          },

    
    public void testBrowseDefinitionsQueryCustomers() throws JSONException, Exception
    {
    	StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();      
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 2L);
     
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.QUERY, "", connProps, opProps);
        tester.setBrowseContext(sbc);
        
 //       actual = tester.browseProfiles("/v1/account/bank_accounts/{id}___get");
        actual = tester.browseProfiles("/v1/customers___get");
 //       final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryCustomers", getClass(), connectorName, captureExpected);
    }	
    
    @Test
    public void testBrowseDefinitionsQueryBalanceTransactions() throws JSONException, Exception
    {
    	StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();      
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 2L);
     
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.QUERY, "", connProps, opProps);
        tester.setBrowseContext(sbc);
        
 //       actual = tester.browseProfiles("/v1/account/bank_accounts/{id}___get");
        actual = tester.browseProfiles("/v1/balance_transactions___get");
 //       final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryBalanceTransactions", getClass(), connectorName, captureExpected);
    }	
    
    @Test
    public void testBrowseDefinitionsCreatePaymentMethodAttachment() throws JSONException, Exception
    {
    	StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 2L);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, opProps);
        tester.setBrowseContext(sbc);
 //       actual = tester.browseProfiles("/v1/account/bank_accounts/{id}___get");
        actual = tester.browseProfiles("/v1/payment_methods/{payment_method}/attach___post");
        
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreatePaymentMethodAttachment", getClass(), connectorName, captureExpected);
    }	

    @Test
    public void testBrowseDefinitionsCreatePaymentMethodCustomerAttachment() throws JSONException, Exception
    {
    	StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 2L);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, opProps);
        tester.setBrowseContext(sbc);
 //       actual = tester.browseProfiles("/v1/account/bank_accounts/{id}___get");
        actual = tester.browseProfiles("/v1/payment_methods/{payment_method}/attach___post");
        
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreatePaymentMethodCustomerAttachment", getClass(), connectorName, captureExpected);
    }	

    @Test
    public void testBrowseDefinitionsCreateCustomer4Deep() throws JSONException, Exception
    {
    	StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        Map<String, Object> opProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 4L);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, opProps);
        tester.setBrowseContext(sbc);
 //       actual = tester.browseProfiles("/v1/account/bank_accounts/{id}___get");
        actual = tester.browseProfiles("/v1/customers___post");
        
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateCustomer4Deep", getClass(), connectorName, captureExpected);
    }	

     @Test
    public void testBrowseDefinitionsCreateCustomer3Deep() throws JSONException, Exception
    {
    	StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        Map<String, Object> opProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 3L);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, opProps);
        tester.setBrowseContext(sbc);
 //       actual = tester.browseProfiles("/v1/account/bank_accounts/{id}___get");
        actual = tester.browseProfiles("/v1/customers___post");
        
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateCustomer3Deep", getClass(), connectorName, captureExpected);
    }	

    @Test
    public void testBrowseDefinitionsCreateCustomerDefaultDeep() throws JSONException, Exception
    {
    	StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        Map<String, Object> opProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 2L);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, opProps);
        tester.setBrowseContext(sbc);
 //       actual = tester.browseProfiles("/v1/account/bank_accounts/{id}___get");
        actual = tester.browseProfiles("/v1/customers___post");
        
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateCustomer", getClass(), connectorName, captureExpected);
    }	
}
