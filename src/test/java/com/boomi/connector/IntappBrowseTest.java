// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.intapp.IntappConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.connector.testutil.ConnectorTester;

/**
 * @author Dave Hock
 */
public class IntappBrowseTest {
    static final String connectorName = "intapp";
    private static final boolean captureExpected = false;
	String swaggerPath = "resources/"+connectorName+"/time-2020.05.12.json";
    
    @Test
    public void testBrowseTypesIntapp() throws JSONException, Exception
    {
        IntappConnector connector = new IntappConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }   
    
    @Test
    public void testBrowseDefinitionsQuery() throws JSONException, Exception
    {
    	IntappConnector connector = new IntappConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/time/api/v1/captureactivities___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQuery", getClass(), connectorName, captureExpected);
    } 
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("IntApp REST API Connector", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}
