// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.dell_powerstore.DellPowerStoreConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class DellPowerStoreBrowseTest {
    static final String connectorName = "dell_powerstore";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/openapi.json";
    
    @Test
    public void testBrowseTypesCREATEDellPowerStore() throws JSONException, Exception
    {
    	DellPowerStoreConnector connector = new DellPowerStoreConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesGETDellPowerStore() throws JSONException, Exception
    {
    	DellPowerStoreConnector connector = new DellPowerStoreConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesQUERYDellPowerStore() throws JSONException, Exception
    {
    	DellPowerStoreConnector connector = new DellPowerStoreConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEDellPowerStore() throws JSONException, Exception
    {
    	DellPowerStoreConnector connector = new DellPowerStoreConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    }
              
    @Test
    public void testBrowseTypesEXECUTEDELETEDellPowerStore() throws JSONException, Exception
    {
    	DellPowerStoreConnector connector = new DellPowerStoreConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseDefinitionsQueryDellPowerStore() throws JSONException, Exception
    {
        DellPowerStoreConnector connector = new DellPowerStoreConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/local_user___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryDellPowerStore", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsGetDellPowerStore() throws JSONException, Exception
    {
        DellPowerStoreConnector connector = new DellPowerStoreConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/local_user/{id}___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetDellPowerStore", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsCreateDellPowerStore() throws JSONException, Exception
    {
        DellPowerStoreConnector connector = new DellPowerStoreConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/local_user___post");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateDellPowerStore", getClass(), connectorName, captureExpected);
    }    
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("The PowerStore Management REST API is a set of resources (objects), operations, and attributes that provide interactive, scripted, and programmatic management control of the PowerStore cluster.", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    }    
}