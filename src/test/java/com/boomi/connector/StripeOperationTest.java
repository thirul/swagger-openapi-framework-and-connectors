// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.stripe.StripeConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class StripeOperationTest 
{
    private static final String URL = "https://api.stripe.com/v1";
    private static final String USERNAME = "sk_test_l6qNiNnNuorniaHAfSCfbd1D00gid1u6Og";
    private static final String PASSWORD = "";
    private static final String SWAGGER_URL = "resources/stripe/stripeapi.json";
	String cookie = "{\"basePath\" : \"\"}";


//    @Test
//    public void testExecuteGetInvoiceOperation() throws Exception
//    {
//    	String objectTypeId = "/invoices/{InvoiceId}___get";
//        OracleSaaSConnector connector = new OracleSaaSConnector();
//        ConnectorTester tester = new ConnectorTester(connector);
//
//        Map<String, Object> connProps = new HashMap<String,Object>();
//        connProps.put(SwaggerConnection.ConnectionProperties.URL.name(), URL);
//        connProps.put(SwaggerConnection.ConnectionProperties.USERNAME.name(), USERNAME);
//        connProps.put(SwaggerConnection.ConnectionProperties.PASSWORD.name(), PASSWORD);
//        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), SWAGGER_URL);
//        
//        List<InputStream> inputs = new ArrayList<InputStream>();
//        String requestBody = "{\"InvoiceId\": \"1\"}";
//        inputs.add(getClass().getClassLoader().getResourceAsStream(requestBody));
//                
//        Map<String, Object> opProps = new HashMap<String,Object>();
//        SimpleOperationContext context =new SimpleOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, objectTypeId, null);
////        ConnectorTestContext context = new ConnectorTestContext();//new SimpleOperationContext(null, connector, OperationType.EXECUTE, "GET", opProps, opProps);
//        
//        tester.setOperationContext(context);
//        List<SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
//        assertEquals("OK", actual.get(0).getMessage());
//        assertEquals("200",actual.get(0).getStatusCode());
//        assertEquals(1, actual.get(0).getPayloads().size());
//    }


    @Test
    public void testQueryCustomersOperation() throws Exception
    {
    	String objectTypeId = "/customers___get";
        StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.name(), URL);
        connProps.put(SwaggerConnection.ConnectionProperties.USERNAME.name(), USERNAME);
//        connProps.put(SwaggerConnection.ConnectionProperties.PASSWORD.name(), PASSWORD);
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), SWAGGER_URL);
//        connProps.put(SwaggerConnection.ConnectionProperties.AUTHTYPE.name(), SwaggerConnection.AuthType.BASIC.name());
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        long maxDocs=28;
        opProps.put(SwaggerQueryOperation.OperationProperties.MAXDOCUMENTS.name(), maxDocs);
        opProps.put(SwaggerQueryOperation.OperationProperties.PAGESIZE.name(), 10L);
//        QueryFilter qf =  new QueryFilterBuilder(QueryGroupingBuilder.and(
//                new QuerySimpleBuilder("InvoiceAmount", "gt", 1111111111)
                
//                new QuerySimpleBuilder("given", "eq", "Niel"),              
//                ,new QuerySimpleBuilder("CreationDate", "lt", "2010-09-01")
//                )).toFilter();
///        Sort sort2 = new Sort();
//        sort2.setSortOrder("asc");
//        sort2.setProperty("gender");
//        qf.withSort(sort1, sort2);//2 sorts throwing 500, 

		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.OUTPUT, cookie);
        tester.setOperationContext(OperationType.QUERY, connProps, opProps, objectTypeId, cookieMap);
        List <SimpleOperationResult> actual = tester.executeQueryOperation(null);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(maxDocs, actual.get(0).getPayloads().size());
        for (int i=0; i<actual.get(0).getPayloads().size();i++)
        {
            System.out.println(new String(actual.get(0).getPayloads().get(i)));
        }
    }
    @Test
    public void testCreateChargeOperation() throws Exception
    {
    	String objectTypeId = "/charges___post";
        StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.name(), URL);
        connProps.put(SwaggerConnection.ConnectionProperties.USERNAME.name(), USERNAME);
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), SWAGGER_URL);
        
        
    	String payload = "{\r\n" + 
    			"  \"amount\" : 10,\r\n\"source\":\"tok_visa\", \"description\": \"a new charge\"," + 
    			"  \"card\" : {\r\n" + 
    			"    \"cvc\" : \"222\",\r\n" + 
    			"    \"exp_month\" : 12,\r\n" + 
    			"    \"exp_year\" : 23,\r\n" + 
    			"    \"number\" : \"4242424242424242\"\r\n" + 
    			"  },\r\n" + 
    			"  \"currency\" : \"usd\"\r\n" + 
    			"}";
		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.OUTPUT, cookie);
       tester.setOperationContext(OperationType.CREATE, connProps, null, objectTypeId, cookieMap);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream(payload.getBytes()));
        List <SimpleOperationResult> actual = tester.executeCreateOperation(inputs);
        assertEquals("Created", actual.get(0).getMessage());
        assertEquals("201",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        System.out.println(responseString);
   }
    
    @Test
    public void testCreateCustomerOperation() throws Exception
    {
    	String objectTypeId = "/customers___post";
        StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.name(), URL);
        connProps.put(SwaggerConnection.ConnectionProperties.USERNAME.name(), USERNAME);
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), SWAGGER_URL);
        
        
    	String payload = "{\"name\":\"Jane Doe\"}";
		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.OUTPUT, cookie);
        tester.setOperationContext(OperationType.CREATE, connProps, null, objectTypeId, cookieMap);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream(payload.getBytes()));
        List <SimpleOperationResult> actual = tester.executeCreateOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        System.out.println(responseString);
   }
    @Test
    public void testCreatePaymentMethodOperation() throws Exception
    {
    	String objectTypeId = "/payment_methods___post";
        StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.name(), URL);
        connProps.put(SwaggerConnection.ConnectionProperties.USERNAME.name(), USERNAME);
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), SWAGGER_URL);
        
        
    	String payload =  "{\n" + 
//    		    "	\"customer\": \"cus_IcbQCkq1nbX6ok\",\n" + 
    		    "	\"type\": \"card\",\n" + 
    		    "	\"card\": {\n" + 
    		    "		\"number\": \"4242424242424242\",\n" + 
    		    "		\"exp_month\": \"01\",\n" + 
    		    "		\"cvc\": \"314\",\n" + 
    		    "		\"exp_year\": \"2025\"\n" + 
    		    "	}\n" + 
    		    "}";
		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.OUTPUT, cookie);
        tester.setOperationContext(OperationType.CREATE, connProps, null, objectTypeId, cookieMap);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream(payload.getBytes()));
        List <SimpleOperationResult> actual = tester.executeCreateOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        System.out.println(responseString);
   }
    
    @Test
    public void testCreatePaymentMethodAttachmentOperation() throws Exception
    {
    	String objectTypeId = "/payment_methods/{payment_method}/attach___post";
        StripeConnector connector = new StripeConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.name(), URL);
        connProps.put(SwaggerConnection.ConnectionProperties.USERNAME.name(), USERNAME);
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), SWAGGER_URL);
       
        Map<String, Object> opProps = new HashMap<String,Object>();
           	
    	opProps.put("PATHPARAM_payment_method", "pm_1I5dwQDYGRiWPfPMd1jmbhtr");
    	opProps.put("PATHPARAM_customer", "cus_IcbQCkq1nbX6ok");
		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		cookieMap.put(ObjectDefinitionRole.OUTPUT, cookie);
        tester.setOperationContext(OperationType.CREATE, connProps, opProps, objectTypeId, cookieMap);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream("{}".getBytes()));
        List <SimpleOperationResult> actual = tester.executeCreateOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        System.out.println(new String(actual.get(0).getPayloads().get(0)));
   }
}
