// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.blackboard.BlackboardConnector;
import com.boomi.connector.magento.MagentoConnector;
import com.boomi.connector.oraclesaas.OracleSaaSConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.ConnectionProperties;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;

/**
 * @author Dave Hock
 */
public class MagentoBrowseTest {
    static final String connectorName = "magento";
	String swaggerPath = "resources/magento/magento_latest-2.2.schema.json";
    private static final boolean captureExpected = true;
    
    @Test
    public void testBrowseTypesMagento() throws JSONException, Exception
    {
        MagentoConnector connector = new MagentoConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
        
    @Test
    //TODO Failed processing profile: com.github.fge.jsonschema.exceptions.ProcessingException: Unable to process nodes without a type
    //"downloadable_option": {}, extension_attributes": {}
    public void testBrowseDefinitionsGetCart() throws JSONException, Exception
    {
    	MagentoConnector connector = new MagentoConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 20L);
        tester.setBrowseContext(OperationType.EXECUTE, connProps, opProps);
        actual = tester.browseProfiles("/V1/carts/{cartId}___get");
        System.out.println(actual);
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetCart", getClass(), connectorName, captureExpected);
    }
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("Magento REST API Connector", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}
