// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.gloat.GloatConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class GloatBrowseTest {
    static final String connectorName = "gloat";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/gloat-swagger.json";
    
    @Test
    public void testBrowseTypesCREATEGloat() throws JSONException, Exception
    {
    	GloatConnector connector = new GloatConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesGETGloat() throws JSONException, Exception
    {
    	GloatConnector connector = new GloatConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesQUERYGloat() throws JSONException, Exception
    {
    	GloatConnector connector = new GloatConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEGloat() throws JSONException, Exception
    {
    	GloatConnector connector = new GloatConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEDELETEGloat() throws JSONException, Exception
    {
    	GloatConnector connector = new GloatConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseDefinitionsQueryGloat() throws JSONException, Exception
    {
        GloatConnector connector = new GloatConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/users___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryGloat", getClass(), connectorName, captureExpected);
    }
    
//    @Test
//    public void testBrowseDefinitionsExecuteGetGloat() throws JSONException, Exception
//    {
//        GloatConnector connector = new GloatConnector();
//        ConnectorTester tester = new ConnectorTester(connector);
//        
//        String actual;
//        Map<String, Object> connProps = new HashMap<String,Object>();
//        
//        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
//        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
//        tester.setBrowseContext(sbc);
//        actual = tester.browseProfiles("/learn/api/public/v1/users/{userId}___get");
//        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsExecuteGetGloat", getClass(), connectorName, captureExpected);
//    }    
    
    @Test
    public void testBrowseDefinitionsCreateGloat() throws JSONException, Exception
    {
        GloatConnector connector = new GloatConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/candidacies___post");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateGloat", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("This document covers the data models and the APIs exposed by Gloat. Before starting out, make sure you have the necessary fields for authenticating to the APIs, namely: your tenant ID and your API key.", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    }    
}