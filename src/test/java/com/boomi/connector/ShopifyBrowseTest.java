// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.blackboard.BlackboardConnector;
import com.boomi.connector.oraclesaas.OracleSaaSConnector;
import com.boomi.connector.shopify.ShopifyConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;

/**
 * @author Dave Hock
 */
public class ShopifyBrowseTest {
    static final String connectorName = "shopify";
    private static final boolean captureExpected = false;
	String swaggerPath = "resources/"+connectorName+"/shopify_openapi.json";
    
    @Test
    public void testBrowseTypesShopify() throws JSONException, Exception
    {
        ShopifyConnector connector = new ShopifyConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }   
    
//    @Test
    public void testBrowseDefinitionsGetCustomer() throws JSONException, Exception
    {
    	ShopifyConnector connector = new ShopifyConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.GET, connProps, null);
        actual = tester.browseProfiles("/customer___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetCustomer", getClass(), connectorName, captureExpected);
    } 
    
  @Test
  void documentationGeneration() throws JSONException, IOException, DocumentException
  {
 		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
  	SwaggerDocumentationUtil.generateDescriptorDoc("Shopify Connector", connectorName);
  	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
  }  
}
