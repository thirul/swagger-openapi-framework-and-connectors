// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.blackboard.BlackboardConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser.OperationProperties;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.ConnectionProperties;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class BlackboardBrowseTest {
    static final String connectorName = "blackboard";
    private static final boolean captureExpected = false;
//    private static final swaggerPath = "https://developer.blackboard.com/portal/docs/apis/learn-swagger.json";
    private static final String swaggerPath = "resources/"+connectorName+"/blackboardswagger.json";
    @Test
    public void testBrowseTypesBlackboard() throws JSONException, Exception
    {
    	BlackboardConnector connector = new BlackboardConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseDefinitionsQueryBlackboard2() throws JSONException, Exception
    {
///Query Course group users - Get Group Memberships
    	BlackboardConnector connector = new BlackboardConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        String objectTypeId = "/learn/api/public/v2/courses/{courseId}/groups/{groupId}/users___get";
        objectTypeId = "/learn/api/public/v1/catalog/categories/{categoryType}/{categoryId}/courses___get";
        //"/learn/api/public/v1/catalog/categories/{categoryType}/{categoryId}/courses___get"
        actual = tester.browseProfiles(objectTypeId);
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryBlackboard2", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsQueryUsersBlackboard() throws JSONException, Exception
    {
        BlackboardConnector connector = new BlackboardConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(OperationProperties.MAXBROWSEDEPTH.name(), 20L);
        tester.setBrowseContext(OperationType.QUERY, connProps, opProps);
        actual = tester.browseProfiles("/learn/api/public/v1/users___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryBlackboard", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsGetBlackboard() throws JSONException, Exception
    {
        BlackboardConnector connector = new BlackboardConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/learn/api/public/v1/users/{userId}___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetBlackboard", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsDeleteBlackboard() throws JSONException, Exception
    {
        BlackboardConnector connector = new BlackboardConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "DELETE", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/learn/api/public/v1/courses/{courseId}/meetings/{meetingId}/users/{userId}___delete");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsDeleteBlackboard", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsCreateBlackboard() throws JSONException, Exception
    {
        BlackboardConnector connector = new BlackboardConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/learn/api/public/v1/users___post");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateBlackboard", getClass(), connectorName, captureExpected);
    }    
    
 //   @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("Blackboard", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}
