// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.pagerduty.PagerDutyConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;

/**
 * @author Dave Hock
 */
public class PagerDutyBrowseTest {
    static final String connectorName = "pagerduty";
    private static final boolean captureExpected = false;
	String swaggerPath = "resources/"+connectorName+"/rest-apis-openapiv3.json";
    
    @Test
    public void testBrowseTypesPagerDuty() throws JSONException, Exception
    {
        PagerDutyConnector connector = new PagerDutyConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    }   
    
    @Test
    public void testBrowseDefinitionsQueryAuditRecords() throws JSONException, Exception
    {
    	PagerDutyConnector connector = new PagerDutyConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/audit/records___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryAuditRecords", getClass(), connectorName, captureExpected);
    } 
 
    @Test
    public void testBrowseDefinitionsQueryBusinessServices() throws JSONException, Exception
    {
    	PagerDutyConnector connector = new PagerDutyConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.QUERY, connProps, null);
        actual = tester.browseProfiles("/business_services___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryBusinessServices", getClass(), connectorName, captureExpected);
    } 
    
    @Test
    public void testBrowseDefinitionsGetUser() throws JSONException, Exception
    {
    	PagerDutyConnector connector = new PagerDutyConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerBrowser.OperationProperties.MAXBROWSEDEPTH.name(), 30L);
        
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", connProps, opProps);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/users/{id}___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetUser", getClass(), connectorName, captureExpected);
    } 
    
    //@Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("Pager Duty Connector", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}
