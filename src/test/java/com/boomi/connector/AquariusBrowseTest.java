// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.aquarius.AquariusConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;

/**
 * @author Dave Hock
 */
public class AquariusBrowseTest {
    static final String connectorName = "aquarius";
    private static final boolean captureExpected = false;
	String swaggerPath = "resources/"+connectorName+"/swagger.json";
    
    @Test
    public void testBrowseTypesAquarius() throws JSONException, Exception
    {
        AquariusConnector connector = new AquariusConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, swaggerPath, connectorName, getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", swaggerPath, connectorName, "", getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", swaggerPath, connectorName, "", this.getClass(), captureExpected);
    } 
    
//TODO  BUG no return schema specified for 200 response   @Test
//   @Test
    public void testBrowseDefinitionsGetMetadataBUG200HASNOSCHEMA() throws JSONException, Exception
    {
    	AquariusConnector connector = new AquariusConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        Map<String, Object> connProps = new HashMap<String,Object>();
        
        connProps.put(SwaggerConnection.ConnectionProperties.SWAGGERURL.name(), swaggerPath);
        
        tester.setBrowseContext(OperationType.GET, connProps, null);
        actual = tester.browseProfiles("/api/v1/aquarius/assets/metadata/{did}___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsGetMetadata", getClass(), connectorName, captureExpected);
    } 
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
    	SwaggerDocumentationUtil.generateDescriptorDoc("Visa Cybersource Connector", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}
