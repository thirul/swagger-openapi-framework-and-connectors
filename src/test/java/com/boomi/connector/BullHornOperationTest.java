// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.bullhorn.BullhornConnection;
import com.boomi.connector.bullhorn.BullhornConnector;
import com.boomi.connector.cybersource.CybersourceConnection;
import com.boomi.connector.cybersource.CybersourceConnector;
import com.boomi.swaggerframework.MockOperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class BullHornOperationTest 
{
    static final String connectorName = "bullhorn";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/openapi.json";
	private static JSONObject _credentials = new JSONObject(new JSONTokener(BullhornBrowseTest.class.getClassLoader().getResourceAsStream("resources/bullhorn/testCredentials.json")));
	private static Map<String, Object> _connProps;
	Logger logger = Logger.getLogger(this.getClass().getName());

	@BeforeAll
    static void initAll() {
		_connProps = new HashMap<String,Object>();
        _connProps.put(BullhornConnection.CustomConnectionProperties.REGION.name(), "west9");
        _connProps.put(BullhornConnection.ConnectionProperties.USERNAME.name(), _credentials.get("username"));
        _connProps.put(BullhornConnection.ConnectionProperties.PASSWORD.name(), _credentials.get("password"));
        _connProps.put(BullhornConnection.CustomConnectionProperties.CLIENT_SECRET.name(), _credentials.get("clientSecret"));
        _connProps.put(BullhornConnection.CustomConnectionProperties.CLIENT_ID.name(), _credentials.get("clientId"));
    }	

    //TODO we need the swagger for Transaction Search
    @Test
    public void testGetActivityGoalOperation() throws Exception
    {
//    	String paymentsCookie = "{\"basePath\" : \"\"}";
    	String objectTypeId = "/entity/Candidate/{entityId}___get";
    	BullhornConnector connector = new BullhornConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
		opProps.put("EXTRA_URL_PARAMETERS", "fields=id,lastName,firstName");
		opProps.put(SwaggerQueryOperation.OperationProperties.PAGESIZE.name(), 3);	
        opProps.put("PATHPARAM_entityId","24908");

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, _connProps, opProps, objectTypeId, null, null);
        context.setCustomOperationType("GET");	
        tester.setOperationContext(context);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream("".getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        logger.info("Response payload: " + actual);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }
    @Test
    public void testCreateCandidateOperation() throws Exception
    {
//    	String paymentsCookie = "{\"basePath\" : \"\"}";
    	String objectTypeId = "/entity/Candidate___post";
    	BullhornConnector connector = new BullhornConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        Map<String, Object> opProps = new HashMap<String,Object>();

		MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, _connProps, opProps, objectTypeId, null, null);
        context.setCustomOperationType("CREATE");	
        tester.setOperationContext(context);
        String candidate = "{ \"firstName\" : \"Alanzo\",\"lastName\" : \"Smith\" }";
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream(candidate.getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        logger.info("Response payload: " + actual);
        String responseString = new String(actual.get(0).getPayloads().get(0));
        JSONObject responseObject = new JSONObject(responseString);
        String id = responseObject.getString("id");
    }
    @Test
    public void testQueryCandidateOperation() throws Exception
    {
//    	String paymentsCookie = "{\"basePath\" : \"\"}";
    	String objectTypeId = "/query/Candidate___get";
    	BullhornConnector connector = new BullhornConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		
//		opProps.put("EXTRA_URL_PARAMETERS", "query=lastName:Candidate");
        opProps.put(SwaggerQueryOperation.OperationProperties.PAGESIZE.name(), 10L);
			
        List<String> fields = new ArrayList<String>();
        fields.add("id");
        fields.add("lastName");
		MockOperationContext context = new MockOperationContext(null, connector, OperationType.QUERY, _connProps, opProps, objectTypeId, null, fields);

        tester.setOperationContext(context);
        List <SimpleOperationResult> actual = tester.executeQueryOperation(null);
        logger.info("Response payload: " + actual);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }
    @Test
    public void testSearchCandidateOperation() throws Exception
    {
//    	String paymentsCookie = "{\"basePath\" : \"\"}";
    	String objectTypeId = "/search/Candidate___get";
    	BullhornConnector connector = new BullhornConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		
		opProps.put("EXTRA_URL_PARAMETERS", "query=lastName:Candidate");
        opProps.put(SwaggerQueryOperation.OperationProperties.PAGESIZE.name(), 10L);
			
        List<String> fields = new ArrayList<String>();
        fields.add("id");
        fields.add("lastName");
		MockOperationContext context = new MockOperationContext(null, connector, OperationType.QUERY, _connProps, opProps, objectTypeId, null, fields);

        tester.setOperationContext(context);
        List <SimpleOperationResult> actual = tester.executeQueryOperation(null);
        logger.info("Response payload: " + actual);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }
}
