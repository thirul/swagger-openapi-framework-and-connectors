// Copyright (c) 2018 Boomi, Inc.

package com.boomi.connector;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.Sort;
import com.boomi.connector.blackboard.BlackboardConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.swaggerframework.swaggerutil.JSONUtil;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.QueryFilterBuilder;
import com.boomi.connector.testutil.QueryGroupingBuilder;
import com.boomi.connector.testutil.QuerySimpleBuilder;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 */
public class BlackboardOperationTest 
{
    private static final String SERVICE_URL = "https://api.blackboard.com";
    private static final String SWAGGER_URL = "resources/blackboard/blackboardswagger.json";

 //   @Test   
    public void testQueryAttendanceByUserOperation() throws Exception
    {
    	//TODO this returns paginated results!!!!!!!!!!!!
    	//We need another way to detect queries...maybe existence of RowBasedPagingParams.* parameter? 
    	String objectTypeId = "/learn/api/public/v1/courses/{courseId}/meetings/users/{userId}___get";
        BlackboardConnector connector = new BlackboardConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.toString(), SERVICE_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();

        QueryFilter qf =  new QueryFilterBuilder(QueryGroupingBuilder.and(
                new QuerySimpleBuilder("courseId", "eq", "aCourse"),
                new QuerySimpleBuilder("userId", "eq", "aUser"))).toFilter();
        opProps.put(SwaggerQueryOperation.OperationProperties.MAXDOCUMENTS.name(), 28L);
        opProps.put(SwaggerQueryOperation.OperationProperties.PAGESIZE.name(), 10L);
        Sort sort1 = new Sort();
        sort1.setProperty("created");
        qf.withSort(sort1); 
        sort1.setSortOrder("desc");
        tester.setOperationContext(OperationType.QUERY, connProps, opProps, objectTypeId, null);
        List <SimpleOperationResult> actual = tester.executeQueryOperation(qf);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }
//    @Test   
    public void testQueryUsersOperation() throws Exception
    {
    	//TODO this returns paginated results!!!!!!!!!!!!
    	//We need another way to detect queries...maybe existence of RowBasedPagingParams.* parameter? 
    	String objectTypeId = "/learn/api/public/v1/users___get";
        BlackboardConnector connector = new BlackboardConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.toString(), SERVICE_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(SwaggerQueryOperation.OperationProperties.MAXDOCUMENTS.name(), 28L);
        opProps.put(SwaggerQueryOperation.OperationProperties.PAGESIZE.name(), 10L);
        QueryFilter qf =  new QueryFilterBuilder(QueryGroupingBuilder.and(
                new QuerySimpleBuilder("roles", "eq", "arole"),
                new QuerySimpleBuilder("lastAccessed", "greaterOrEqual", "2019-09-01"))).toFilter();
        Sort sort1 = new Sort();
        sort1.setProperty("lastAccessed");
        sort1.setSortOrder("asc");
        Sort sort2 = new Sort();
        sort2.setSortOrder("desc");
        sort2.setProperty("created");
        qf.withSort(sort1, sort2); 
        tester.setOperationContext(OperationType.QUERY, connProps, opProps, objectTypeId, null);
        List <SimpleOperationResult> actual = tester.executeQueryOperation(qf);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }

//    @Test   
    public void testDeleteAllAttendanceByCourseAndUserIDOperation() throws Exception
    {
    	String objectTypeID = "/learn/api/public/v1/courses/{courseId}/meetings/users/{userId}___delete";
        BlackboardConnector connector = new BlackboardConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.toString(), SERVICE_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
//TODO Custom Operation Type
        tester.setOperationContext(OperationType.EXECUTE, connProps, opProps, objectTypeID, null);
        String payload = "{\"courseId\":\"course1\",\"userId\":\"user1\"}";
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream(payload.getBytes()));
        List <SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
    }

//    @Test   
    public void testCreateAttendancRecordOperation() throws Exception
    {
    	String objectTypeID = "/learn/api/public/v1/courses/{courseId}/meetings/{meetingId}/users___post";
        BlackboardConnector connector = new BlackboardConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put(SwaggerConnection.ConnectionProperties.URL.toString(), SERVICE_URL);
        
        Map<String, Object> opProps = new HashMap<String,Object>();
        JSONObject swaggerDefinition = JSONUtil.getJSONFromResource(this.getClass(), SWAGGER_URL);

        SwaggerAPIService swaggerAPIService = new SwaggerAPIService(objectTypeID, swaggerDefinition, 30);
		HashMap<ObjectDefinitionRole, String> cookieMap = new HashMap<ObjectDefinitionRole, String>();
		String cookie="";
		cookieMap.put(ObjectDefinitionRole.INPUT, cookie);
		
		String payload = "{\n" + 
				"  \"___pathParameters\":{\"courseId\":\"course1\",\"meetingId\":\"meeting1\"},\n" + 
				"  \"meetingId\": \"meeting1\",\n" + 
				"  \"userId\": \"user1\",\n" + 
				"  \"status\": \"Absent\"\n" + 
				"}";

        tester.setOperationContext(OperationType.CREATE, connProps, opProps, objectTypeID, null);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream(payload.getBytes()));
        List <SimpleOperationResult> actual = tester.executeCreateOperation(inputs);
        assertEquals("OK", actual.get(0).getMessage());
        assertEquals("200",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        System.out.println(responseString);
    }
}
