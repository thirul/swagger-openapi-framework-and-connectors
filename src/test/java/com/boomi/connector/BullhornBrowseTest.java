// Copyright (c) 2019 Boomi, Inc.

package com.boomi.connector;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.boomi.swaggerframework.SwaggerDocumentationUtil;
import com.boomi.swaggerframework.SwaggerTestUtil;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.bullhorn.BullhornConnection;
import com.boomi.connector.bullhorn.BullhornConnector;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.TestUtil;

/**
 * @author Dave Hock
 */
public class BullhornBrowseTest {
    static final String connectorName = "bullhorn";
    private static final boolean captureExpected = false;
    private static final String swaggerPath = "resources/"+connectorName+"/openapi.json";
	private static JSONObject _credentials = new JSONObject(new JSONTokener(BullhornBrowseTest.class.getClassLoader().getResourceAsStream("resources/bullhorn/testCredentials.json")));
	private static Map<String, Object> _connProps;

	@BeforeAll
    static void initAll() {
		_connProps = new HashMap<String,Object>();
        _connProps.put(BullhornConnection.CustomConnectionProperties.REGION.name(), "west9");
        _connProps.put(BullhornConnection.ConnectionProperties.USERNAME.name(), _credentials.get("username"));
        _connProps.put(BullhornConnection.ConnectionProperties.PASSWORD.name(), _credentials.get("password"));
        _connProps.put(BullhornConnection.CustomConnectionProperties.CLIENT_SECRET.name(), _credentials.get("clientSecret"));
        _connProps.put(BullhornConnection.CustomConnectionProperties.CLIENT_ID.name(), _credentials.get("clientId"));
    }	

    @Test
    public void testBrowseTypesCREATEBullhorn() throws JSONException, Exception
    {
    	BullhornConnector connector = new BullhornConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "CREATE", _connProps, null, connectorName, this.getClass(), captureExpected);
    }
              
    @Test
    public void testBrowseTypesQUERYBullhorn() throws JSONException, Exception
    {
    	BullhornConnector connector = new BullhornConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.QUERY, "", _connProps, null, connectorName, this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesUPDATEBullhorn() throws JSONException, Exception
    {
    	BullhornConnector connector = new BullhornConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "UPDATE", _connProps, null, connectorName, this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEGETBullhorn() throws JSONException, Exception
    {
    	BullhornConnector connector = new BullhornConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "GET", _connProps, null, connectorName, this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseTypesEXECUTEDELETEBullhorn() throws JSONException, Exception
    {
    	BullhornConnector connector = new BullhornConnector();
    	SwaggerTestUtil.testBrowseTypes(connector, OperationType.EXECUTE, "DELETE", _connProps, null, connectorName, this.getClass(), captureExpected);
    }
       
    @Test
    public void testBrowseDefinitionsQueryBullhorn() throws JSONException, Exception
    {
        BullhornConnector connector = new BullhornConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        
        tester.setBrowseContext(OperationType.QUERY, _connProps, null);
        actual = tester.browseProfiles("/search/ActivityGoal___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsQueryBullhorn", getClass(), connectorName, captureExpected);
    }
    
    @Test
    public void testBrowseDefinitionsExecuteGetBullhorn() throws JSONException, Exception
    {
        BullhornConnector connector = new BullhornConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "GET", _connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/entity/Candidate/{entityId}___get");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsExecuteGetBullhorn", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsExecuteDeleteBullhorn() throws JSONException, Exception
    {
        BullhornConnector connector = new BullhornConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "DELETE", _connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/entity/ActivityGoal___delete");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsExecuteDeleteBullhorn", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsCreateBullhorn() throws JSONException, Exception
    {
        BullhornConnector connector = new BullhornConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", _connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/entity/ActivityGoal___post");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsCreateBullhorn", getClass(), connectorName, captureExpected);
    }    
    
    @Test
    public void testBrowseDefinitionsUpdateBullhorn() throws JSONException, Exception
    {
        BullhornConnector connector = new BullhornConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        
        String actual;
        SimpleBrowseContext sbc = new SimpleBrowseContext(null, connector, OperationType.EXECUTE, "CREATE", _connProps, null);
        tester.setBrowseContext(sbc);
        actual = tester.browseProfiles("/entity/ActivityGoal/{entityId}___delete");
        SwaggerTestUtil.compareXML(actual, "testBrowseDefinitionsUpdateBullhorn", getClass(), connectorName, captureExpected);
    }    
    
//    @Test
    void documentationGeneration() throws JSONException, IOException, DocumentException
    {
   		SwaggerDocumentationUtil.analyzeSwagger(connectorName);
    	SwaggerDocumentationUtil.generateDescriptorDoc("Bullhorn REST API", connectorName);
    	SwaggerDocumentationUtil.generateOperationTypeDocs(connectorName);
    }    
}