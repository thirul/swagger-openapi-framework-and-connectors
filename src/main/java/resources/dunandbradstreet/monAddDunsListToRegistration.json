{
	"swagger": "2.0",
	"host": "plus.dnb.com",
	"schemes": [
		"https"
	],
	"info": {
		"description": "Manage Monitoring registrations",
		"version": "1",
		"title": "Monitoring API"
	},
	"basePath": "/",
	"tags": [
		{
			"name": "registrations"
		}
	],
	"paths": {
		"/v1/monitoring/registrations/{reference}/duns/add": {
			"x-DNB-Name": "Add D-U-N-S Numbers to Existing Registration",
			"x-DNB-ID": "monAddDunsListToRegistration",
			"patch": {
				"tags": [
					"registrations"
				],
				"summary": "Endpoint",
				"description": "Provides the ability to add D-U-N-S Numbers to an existing LOD Monitoring Registration. D-U-N-S Numbers should be batched when calling this API.  D-U-N-S Numbers will be added faster and more efficiently to a registration where multiple D-U-N-S Numbers are passed in a single invocation. For example, it will be much faster to add 100 D-U-N-S Numbers submitted in one file and passed in a single invocation of the API than to call the API one hundred times to add a single D-U-N-S Number each time. Large numbers of calls to add a single D-U-N-S Number may lead to delays in registrations being updated or requests being throttled for a period of time.",
				"operationId": "addDunsListToRegistration",
				"consumes": [
					"multipart/form-data"
				],
				"produces": [
					"application/json"
				],
				"parameters": [
					{
						"in": "path",
						"name": "reference",
						"description": "A string used to identify the registration.  The reference must be unique for each subscriber.",
						"required": true,
						"type": "string",
						"x-example": "myregistration"
					},
					{
						"name": "duns",
						"in": "formData",
						"description": "A file containing the list of D-U-N-S Numbers (one per line) to be added or removed from the registration.<br/><br/>Supported file types:<br/>* .txt<br/>* .zip",
						"x-example": "duns.txt",
						"required": true,
						"type": "file"
					},
					{
						"in": "header",
						"name": "Authorization",
						"description": "The access token provided by authentication.",
						"required": true,
						"type": "string",
						"x-example": "Bearer alphanumerictoken"
					}
				],
				"responses": {
					"202": {
						"description": "Request accepted; awaiting processing",
						"schema": {
							"properties": {
								"transactionDetail": {
									"type": "object",
									"description": "The information used to process this request.",
									"properties": {
										"transactionID": {
											"description": "A value assigned by the Dun & Bradstreet application to uniquely identify this request.",
											"example": "rlh-hi9puyoijk-jop8u-kd-d-1",
											"type": "string"
										},
										"transactionTimestamp": {
											"description": "The date and time, in ISO 8601 UTC Z standard, when this response was created.",
											"example": "2017-02-21T17:46:19.839Z",
											"type": "string"
										},
										"inLanguage": {
											"description": "An IETF BCP 47 code value that defines the language in which this product was rendered.",
											"example": "en-US",
											"type": "string"
										}
									}
								},
								"information": {
									"description": "The details of the request.",
									"type": "object",
									"properties": {
										"code": {
											"description": "A unique code assigned by Dun & Bradstreet to identify the status of this request. See the Error and Information Code list for more information.",
											"example": 99999,
											"type": "string"
										},
										"message": {
											"description": "The description assigned to the information code.",
											"example": "A message appropriate to the request outcome.",
											"type": "string"
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}