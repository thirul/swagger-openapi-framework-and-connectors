{
  "swagger": "2.0",
  "info": {
    "description": "The Student Engagement service enables you to access student holds and other information related to Student Campus Engagement in Workday. \n\nRelated Information:\n* Administrator Guide: Setup Considerations: Student Holds",
    "version": "v1",
    "title": "studentEngagement"
  },
  "host": "<tenant hostname>",
  "basePath": "/studentEngagement/v1",
  "tags": [
    {
      "name": "students"
    }
  ],
  "schemes": [
    "https"
  ],
  "paths": {
    "/students/{ID}/holds": {
      "get": {
        "tags": [
          "students"
        ],
        "summary": "Retrieves a collection of student holds assigned to the specified student ID.",
        "description": "Retrieves a collection of student holds assigned to the specified student ID. This method also returns the student hold reasons and the processes that the holds apply to.\nYou can filter student holds by their hold reason (exact match), hold type (any in common), and whether or not the hold is inactive.\n\nScope: Campus Engagement\n\nSecured by: Self-Service: Holds, Manage: Holds",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "ID",
            "in": "path",
            "description": "The Workday ID of the resource.",
            "required": true,
            "type": "string"
          },
          {
            "name": "holdType",
            "in": "query",
            "description": "Filter for holds matching these hold types.",
            "required": false,
            "type": "array",
            "items": {
              "type": "string"
            },
            "collectionFormat": "multi"
          },
          {
            "name": "limit",
            "in": "query",
            "description": "The maximum number of objects in a single response. The default is 20. The maximum is 0.",
            "required": false,
            "type": "integer",
            "format": "int64"
          },
          {
            "name": "offset",
            "in": "query",
            "description": "The zero-based index of the first object in a response collection. The default is 0. Use offset with the limit parameter to control paging of a response collection. Example: If limit is 5 and offset is 9, the response returns a collection of 5 objects starting with the 10th object.",
            "required": false,
            "type": "integer",
            "format": "int64"
          },
          {
            "name": "reason",
            "in": "query",
            "description": "Filter for holds matching this hold reason.",
            "required": false,
            "type": "string"
          },
          {
            "name": "showInactive",
            "in": "query",
            "description": "Parameter that will include inactive holds along with the active ones.",
            "required": false,
            "type": "boolean"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful response. A successful response can return no matched data.",
            "schema": {
              "type": "object",
              "properties": {
                "data": {
                  "type": "array",
                  "items": {
                    "$ref": "#/definitions/holdableAssignment_fea4274892bd10001f64ea98900301ca"
                  }
                },
                "total": {
                  "type": "integer",
                  "format": "int64"
                }
              },
              "description": "collection something or other"
            }
          },
          "400": {
            "description": "Invalid request. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "401": {
            "description": "Invalid resource or operation. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "403": {
            "description": "User has insufficient permissions. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "404": {
            "description": "Resource not found. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "default": {
            "description": "An error occurred.",
            "schema": {
              "$ref": "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/students": {
      "get": {
        "tags": [
          "students"
        ],
        "summary": "Retrieves a collection of students.",
        "description": "Retrieves a collection of students.\nScope: Student Core\n\nSecured by: Self-Service: Student Profile, Reports: Students",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "academicLevel",
            "in": "query",
            "description": "Derived Academic Level.",
            "required": false,
            "type": "array",
            "items": {
              "type": "string"
            },
            "collectionFormat": "multi"
          },
          {
            "name": "academicUnit",
            "in": "query",
            "description": "Derived Academic Units.",
            "required": false,
            "type": "array",
            "items": {
              "type": "string"
            },
            "collectionFormat": "multi"
          },
          {
            "name": "limit",
            "in": "query",
            "description": "The maximum number of objects in a single response. The default is 20. The maximum is 0.",
            "required": false,
            "type": "integer",
            "format": "int64"
          },
          {
            "name": "offset",
            "in": "query",
            "description": "The zero-based index of the first object in a response collection. The default is 0. Use offset with the limit parameter to control paging of a response collection. Example: If limit is 5 and offset is 9, the response returns a collection of 5 objects starting with the 10th object.",
            "required": false,
            "type": "integer",
            "format": "int64"
          },
          {
            "name": "programOfStudy",
            "in": "query",
            "description": "For students who haven't matriculated, the field shows the program of study they either applied to or have been admitted to. For matriculated students, the field shows the primary program of study from their reporting record.",
            "required": false,
            "type": "array",
            "items": {
              "type": "string"
            },
            "collectionFormat": "multi"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful response. A successful response can return no matched data.",
            "schema": {
              "type": "object",
              "properties": {
                "data": {
                  "type": "array",
                  "items": {
                    "$ref": "#/definitions/studentSummary_53782cb2f04e10001976f95b3edd0121"
                  }
                },
                "total": {
                  "type": "integer",
                  "format": "int64"
                }
              },
              "description": "collection something or other"
            }
          },
          "400": {
            "description": "Invalid request. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "401": {
            "description": "Invalid resource or operation. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "403": {
            "description": "User has insufficient permissions. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "404": {
            "description": "Resource not found. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "default": {
            "description": "An error occurred.",
            "schema": {
              "$ref": "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/students/{ID}": {
      "get": {
        "tags": [
          "students"
        ],
        "summary": "Retrieves a collection of students.",
        "description": "Retrieves a collection of students.\nScope: Student Core\n\nSecured by: Self-Service: Student Profile, Reports: Students",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "ID",
            "in": "path",
            "description": "The Workday ID of the resource.",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful response. A successful response can return no matched data.",
            "schema": {
              "$ref": "#/definitions/studentDetail_6393b5f0dcfa10000f524d62761400f4"
            }
          },
          "400": {
            "description": "Invalid request. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "401": {
            "description": "Invalid resource or operation. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "403": {
            "description": "User has insufficient permissions. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "404": {
            "description": "Resource not found. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "default": {
            "description": "An error occurred.",
            "schema": {
              "$ref": "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/students/{ID}/holds/{subresourceID}": {
      "get": {
        "tags": [
          "students"
        ],
        "summary": "Retrieves a collection of student holds assigned to the specified student ID.",
        "description": "Retrieves a collection of student holds assigned to the specified student ID. This method also returns the student hold reasons and the processes that the holds apply to.\nYou can filter student holds by their hold reason (exact match), hold type (any in common), and whether or not the hold is inactive.\n\nScope: Campus Engagement\n\nSecured by: Self-Service: Holds, Manage: Holds",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "ID",
            "in": "path",
            "description": "The Workday ID of the resource.",
            "required": true,
            "type": "string"
          },
          {
            "name": "subresourceID",
            "in": "path",
            "description": "The Workday ID of the subresource.",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful response. A successful response can return no matched data.",
            "schema": {
              "$ref": "#/definitions/holdableAssignment_fea4274892bd10001f64ea98900301ca"
            }
          },
          "400": {
            "description": "Invalid request. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "401": {
            "description": "Invalid resource or operation. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "403": {
            "description": "User has insufficient permissions. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "404": {
            "description": "Resource not found. (https://community.workday.com/rest/error-messages)",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "default": {
            "description": "An error occurred.",
            "schema": {
              "$ref": "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "OAuth2": {
      "type": "oauth2",
      "authorizationUrl": "https://<tenant authorization hostname>",
      "flow": "implicit"
    }
  },
  "definitions": {
    "INSTANCE_MODEL_REFERENCE": {
      "type": "object",
      "required": [
        "id"
      ],
      "properties": {
        "id": {
          "type": "string",
          "description": "wid / id / reference id",
          "pattern": "^(?:(?:[0-9a-f]{32})|(?:[0-9]+\\$[0-9]+)|(\\S+=\\S+))$"
        },
        "descriptor": {
          "type": "string",
          "description": "A description of the instance",
          "readOnly": true
        },
        "href": {
          "type": "string",
          "description": "A link to the instance",
          "readOnly": true
        }
      }
    },
    "ERROR_MODEL_REFERENCE": {
      "type": "object",
      "required": [
        "error"
      ],
      "properties": {
        "error": {
          "type": "string",
          "description": "A description of the error"
        }
      }
    },
    "VALIDATION_ERROR_MODEL_REFERENCE": {
      "allOf": [
        {
          "$ref": "#/definitions/ERROR_MODEL_REFERENCE"
        },
        {
          "properties": {
            "errors": {
              "type": "array",
              "description": "An array of validation errors",
              "items": {
                "$ref": "#/definitions/ERROR_MODEL_REFERENCE"
              }
            }
          }
        }
      ]
    },
    "FACETS_MODEL_REFERENCE": {
      "type": "array",
      "items": {
        "type": "object",
        "description": "This object represents the possible facets for this resource",
        "readOnly": true,
        "properties": {
          "descriptor": {
            "type": "string",
            "description": "A description of the facet"
          },
          "facetParameter": {
            "type": "string",
            "description": "The alias used to select the facet"
          },
          "values": {
            "type": "array",
            "description": "the facet values",
            "items": {
              "type": "object",
              "properties": {
                "count": {
                  "type": "integer",
                  "format": "int32",
                  "description": "The number of instances returned by this facet"
                },
                "id": {
                  "type": "string",
                  "description": "wid / id / reference id",
                  "pattern": "^(?:(?:[0-9a-f]{32})|(?:[0-9]+\\$[0-9]+)|(\\S+=\\S+))$"
                },
                "descriptor": {
                  "type": "string",
                  "description": "A description of the facet"
                },
                "href": {
                  "type": "string",
                  "description": "A link to the instance",
                  "readOnly": true
                }
              },
              "required": [
                "id"
              ]
            }
          }
        }
      }
    },
    "MULTIPLE_INSTANCE_MODEL_REFERENCE": {
      "type": "object",
      "properties": {
        "total": {
          "type": "integer"
        },
        "data": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
          }
        }
      }
    },
    "location_6393b5f0dcfa10000fe069e4a5620102": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "person_15308ff39d95100018213f71d8a60183": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "studentDetail_6393b5f0dcfa10000f524d62761400f4": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "location": {
              "description": "The location associated with the student's earliest active institutional reporting record.",
              "$ref": "#/definitions/location_6393b5f0dcfa10000fe069e4a5620102"
            },
            "Student_ID": {
              "type": "string",
              "example": "627272",
              "description": "The student ID of the student.",
              "x-workday-type": "Text"
            },
            "internationalStudent": {
              "type": "boolean",
              "example": true,
              "description": "True if the student is not a citizen of the United States or one of its territories and is a citizen of another ~country~.",
              "x-workday-type": "Boolean"
            },
            "person": {
              "description": "Returns the person for this role.",
              "$ref": "#/definitions/person_15308ff39d95100018213f71d8a60183"
            },
            "firstGeneration": {
              "type": "boolean",
              "example": true,
              "description": "Indicator for first generation student.",
              "x-workday-type": "Boolean"
            },
            "primaryStudentRecord": {
              "type": "array",
              "description": "Returns all Academic Records for the Student.",
              "items": {
                "$ref": "#/definitions/studentRecordSummary_4502a49c080f10001e6f44d7a4ec0104"
              },
              "x-workday-type": "Multi-instance"
            },
            "militaryRelationship": {
              "type": "boolean",
              "example": true,
              "description": "Returns students that have a military relationship (e.g. veteran, military spouse)",
              "x-workday-type": "Boolean"
            },
            "preferredName": {
              "type": "string",
              "example": "Sally Student",
              "description": "Returns the fully formatted preferred name for the Student.",
              "x-workday-type": "Text"
            },
            "descriptor": {
              "type": "string",
              "example": "Lorem ipsum dolor sit ame",
              "description": "A preview of the instance"
            },
            "id": {
              "type": "string",
              "description": "Id of the instance"
            }
          }
        }
      ]
    },
    "studentSummary_53782cb2f04e10001976f95b3edd0121": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "Student_ID": {
              "type": "string",
              "example": "627272",
              "description": "The student ID of the student.",
              "x-workday-type": "Text"
            },
            "primaryStudentRecord": {
              "type": "array",
              "description": "Returns all Academic Records for the Student.",
              "items": {
                "$ref": "#/definitions/studentRecordSummary_4502a49c080f10001e6f44d7a4ec0104"
              },
              "x-workday-type": "Multi-instance"
            },
            "descriptor": {
              "type": "string",
              "example": "Lorem ipsum dolor sit ame",
              "description": "A preview of the instance"
            },
            "id": {
              "type": "string",
              "description": "Id of the instance"
            }
          }
        }
      ]
    },
    "overrideEvent_2f57fa2bc72b1000149389270e460151": {
      "allOf": [
        {
          "$ref": "#/definitions/overrideEvent_2f57fa2bc72b1000149220fdf530014c"
        },
        {}
      ]
    },
    "reason_fea4274892bd10001f64eabe80b701ce": {
      "allOf": [
        {
          "$ref": "#/definitions/reason_1d11a92f7fec100003c448a710e00125"
        },
        {}
      ]
    },
    "holdableAssignment_fea4274892bd10001f64ea98900301ca": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "overrideEvent": {
              "description": "Details of the Hold Override.",
              "$ref": "#/definitions/overrideEvent_2f57fa2bc72b1000149389270e460151"
            },
            "reason": {
              "description": "Reason the Hold is assigned to a Student",
              "$ref": "#/definitions/reason_fea4274892bd10001f64eabe80b701ce"
            },
            "createdBy": {
              "type": "string",
              "example": "Logan McNeil",
              "description": "The user who assigned the hold.",
              "x-workday-type": "Text"
            },
            "typeContexts": {
              "type": "array",
              "description": "The Details of the Hold Type, including a Hold Type and the context for that Type. Depending on the Hold Type the context may be Academic Period, Academic Record, Institution, Federal School Code Rule Set, and/or Financial Aid Award Year.",
              "items": {
                "$ref": "#/definitions/typeContext_1d11a92f7fec100003fadf4510d80129"
              },
              "x-workday-type": "Multi-instance"
            },
            "removedOn": {
              "type": "string",
              "format": "date",
              "example": "2020-01-05T08:00:00.000Z",
              "description": "Date when the Hold will be removed from the Student",
              "x-workday-type": "Date"
            },
            "createdOn": {
              "type": "string",
              "format": "date",
              "example": "2020-01-01T08:00:00.000Z",
              "description": "Date the Hold was created",
              "x-workday-type": "Date"
            },
            "descriptor": {
              "type": "string",
              "example": "Lorem ipsum dolor sit ame",
              "description": "A preview of the instance"
            },
            "id": {
              "type": "string",
              "description": "Id of the instance"
            }
          }
        }
      ]
    },
    "programOfStudy_4502a49c080f10001e6f44f976050107": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "academicUnit_4502a49c080f10001e6f44f3a80e0106": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "academicLevel_4502a49c080f10001e6f44e9fb670105": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "status_382d56cb804d100012f64e43e6120147": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "classStanding_382d56cb804d1000131fef31178f0149": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "studentRecordSummary_4502a49c080f10001e6f44d7a4ec0104": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "programOfStudy": {
              "description": "Returns the primary Program of Study for the Student Record.",
              "$ref": "#/definitions/programOfStudy_4502a49c080f10001e6f44f976050107"
            },
            "academicUnit": {
              "description": "Returns the Academic Unit of the Primary Program of Study of the Reporting Record (for Matriculated Student) or Primary Program of Study of the Application (for Applicant) as of the standard start date for the financial aid period record.",
              "$ref": "#/definitions/academicUnit_4502a49c080f10001e6f44f3a80e0106"
            },
            "academicLevel": {
              "description": "Academic Level for Student Record",
              "$ref": "#/definitions/academicLevel_4502a49c080f10001e6f44e9fb670105"
            },
            "status": {
              "description": "Returns the current designation of Active or Inactive for the Student. A Student is considered Active if they are actively pursuing at least one Program of Study, as determined by the status on their Student Program of Study Record.",
              "$ref": "#/definitions/status_382d56cb804d100012f64e43e6120147"
            },
            "classStanding": {
              "description": "The currently effective Class Standing for this Student Record",
              "$ref": "#/definitions/classStanding_382d56cb804d1000131fef31178f0149"
            },
            "descriptor": {
              "type": "string",
              "example": "Lorem ipsum dolor sit ame",
              "description": "A preview of the instance"
            },
            "id": {
              "type": "string",
              "description": "Id of the instance"
            }
          }
        }
      ]
    },
    "financialAidAwardYear_1d11a92f7fec100003fadfd1631b012e": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "holdType_1d11a92f7fec100003fadfe6391f012f": {
      "allOf": [
        {
          "$ref": "#/definitions/holdType_2f57fa2bc72b100019159a09b46f0187"
        },
        {}
      ]
    },
    "institution_1d11a92f7fec100003fadf75de40012a": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "federalSchoolCodeRuleSet_1d11a92f7fec100003fadfbe4abf012d": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "academicPeriod_1d11a92f7fec100003fadf930ddd012b": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "academicRecord_1d11a92f7fec100003fadfaa7c5f012c": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "typeContext_1d11a92f7fec100003fadf4510d80129": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "financialAidAwardYear": {
              "description": "The Financial Aid Award Year this Hold Type applies to.",
              "$ref": "#/definitions/financialAidAwardYear_1d11a92f7fec100003fadfd1631b012e"
            },
            "holdType": {
              "description": "The processes blocked by this student hold assignment.",
              "$ref": "#/definitions/holdType_1d11a92f7fec100003fadfe6391f012f"
            },
            "institution": {
              "description": "The institutional Academic Unit this Hold Type applies to.",
              "$ref": "#/definitions/institution_1d11a92f7fec100003fadf75de40012a"
            },
            "federalSchoolCodeRuleSet": {
              "description": "The school code this hold type applies to.",
              "$ref": "#/definitions/federalSchoolCodeRuleSet_1d11a92f7fec100003fadfbe4abf012d"
            },
            "academicPeriod": {
              "description": "The Academic Period this hold type applies to.",
              "$ref": "#/definitions/academicPeriod_1d11a92f7fec100003fadf930ddd012b"
            },
            "academicRecord": {
              "description": "The Academic Record this hold type applies to.",
              "$ref": "#/definitions/academicRecord_1d11a92f7fec100003fadfaa7c5f012c"
            }
          }
        }
      ]
    },
    "overrideEvent_2f57fa2bc72b1000149220fdf530014c": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "endDate": {
              "type": "string",
              "format": "date",
              "example": "2020-01-04T08:00:00.000Z",
              "description": "The date the override ends.",
              "x-workday-type": "Date"
            },
            "holdTypes": {
              "type": "array",
              "description": "The hold type(s) the Override applies to.",
              "items": {
                "$ref": "#/definitions/holdType_2f57fa2bc72b100019159a09b46f0187"
              },
              "x-workday-type": "Multi-instance"
            },
            "appliedOn": {
              "type": "string",
              "format": "date",
              "example": "2020-01-02T08:00:00.000Z",
              "description": "The Date the Hold Override was placed.",
              "x-workday-type": "Date"
            },
            "createdBy": {
              "type": "string",
              "example": "Brian King",
              "description": "The person who applied the override.",
              "x-workday-type": "Text"
            },
            "startDate": {
              "type": "string",
              "format": "date",
              "example": "2020-01-03T08:00:00.000Z",
              "description": "The date the override starts.",
              "x-workday-type": "Date"
            },
            "id": {
              "type": "string",
              "description": "Id of the instance"
            }
          }
        }
      ]
    },
    "reason_1d11a92f7fec100003c448a710e00125": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "description": {
              "type": "string",
              "example": "An advisor has requested that a student must meet with him/her prior to proceeding with registration or a student has not completed necessary academic milestones.",
              "description": "The description of the hold reason.",
              "x-workday-type": "Rich Text"
            },
            "resolutionInstructions": {
              "type": "string",
              "example": "Meet with your primary advisor to remedy the situation and have the 'Registration Hold' released in order to register for classes.",
              "description": "The resolution instructions defined for this hold reason.",
              "x-workday-type": "Rich Text"
            },
            "id": {
              "type": "string",
              "description": "Id of the instance"
            },
            "descriptor": {
              "type": "string",
              "example": "Lorem ipsum dolor sit ame",
              "description": "A preview of the instance"
            }
          }
        }
      ]
    },
    "holdType_2f57fa2bc72b100019159a09b46f0187": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "descriptor": {
              "type": "string",
              "example": "Lorem ipsum dolor sit ame",
              "description": "A preview of the instance"
            },
            "id": {
              "type": "string",
              "description": "Id of the instance"
            }
          }
        }
      ]
    }
  }
}