{
  "swagger" : "2.0",
  "info" : {
    "description" : "The Performance Enablement (previously Performance Management) public service contains resources that expose Workday Employee Performance Management Business Services data. This service can be used for integration with other employee performance management systems.",
    "version" : "v2",
    "title" : "Performance Enablement"
  },
  "host" : "<tenant hostname>",
  "basePath" : "/performanceManagement/v2",
  "tags" : [ {
    "name" : "workers"
  }, {
    "name" : "feedbackBadges",
    "description" : "A collection of active Feedback Badges in Workday"
  } ],
  "schemes" : [ "https" ],
  "paths" : {
    "/workers" : {
      "get" : {
        "tags" : [ "workers" ],
        "description" : "Secured by: Worker Data: Public Worker Reports, Self-Service: Current Staffing Information",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "search",
          "in" : "query",
          "description" : "A string which enables searching for workers by name",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "The number of instances to retrieve",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        }, {
          "name" : "offset",
          "in" : "query",
          "description" : "The offset in the collection",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        } ],
        "responses" : {
          "200" : {
            "description" : "getting response",
            "schema" : {
              "type" : "object",
              "description" : "collection something or other",
              "properties" : {
                "data" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/workerSummary_a489aef739484c13a59e6d502a9e7b68"
                  }
                },
                "total" : {
                  "type" : "integer",
                  "format" : "int64"
                }
              }
            }
          },
          "default" : {
            "description" : "an error occurred",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/workers/{ID}/anytimeFeedbackEntries/{subresourceID}" : {
      "get" : {
        "tags" : [ "workers" ],
        "description" : "Retrieves a collection of feedback given about the specified worker.\n\nSecured by: Self-Service: Anytime Feedback, Worker Data: Anytime Feedback\n\nScope: Talent Core",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "ID",
          "in" : "path",
          "description" : "ID of specified Resource instance",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "subresourceID",
          "in" : "path",
          "description" : "ID of specified Subresource instance",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "getting response",
            "schema" : {
              "$ref" : "#/definitions/feedbackSummary_a6ab9069588610002b8c0b4f60a8000f"
            }
          },
          "default" : {
            "description" : "an error occurred",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/workers/{ID}" : {
      "get" : {
        "tags" : [ "workers" ],
        "description" : "Secured by: Worker Data: Public Worker Reports, Self-Service: Current Staffing Information",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "ID",
          "in" : "path",
          "description" : "ID of specified Resource instance",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "getting response",
            "schema" : {
              "$ref" : "#/definitions/workerProfile_c0a0dce56eb142d39dbffeb505becf7a"
            }
          },
          "default" : {
            "description" : "an error occurred",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/workers/{ID}/anytimeFeedbackEntries" : {
      "get" : {
        "tags" : [ "workers" ],
        "description" : "Retrieves a collection of feedback given about the specified worker.\n\nSecured by: Self-Service: Anytime Feedback, Worker Data: Anytime Feedback\n\nScope: Talent Core",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "relatesTo",
          "in" : "query",
          "description" : "The talent tag that relates to the feedback response for feedback given.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "The number of instances to retrieve",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        }, {
          "name" : "offset",
          "in" : "query",
          "description" : "The offset in the collection",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        }, {
          "name" : "ID",
          "in" : "path",
          "description" : "ID of specified Resource instance",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "getting response",
            "schema" : {
              "type" : "object",
              "description" : "collection something or other",
              "properties" : {
                "data" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/feedbackSummary_a6ab9069588610002b8c0b4f60a8000f"
                  }
                },
                "total" : {
                  "type" : "integer",
                  "format" : "int64"
                }
              }
            }
          },
          "default" : {
            "description" : "an error occurred",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      },
      "post" : {
        "tags" : [ "workers" ],
        "description" : "Creates a feedback given instance about the specified worker.\n\nSecured by: Give Feedback\n\nScope: Talent Core",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "relatesTo",
          "in" : "query",
          "description" : "The talent tag that relates to the feedback response for feedback given.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "ID",
          "in" : "path",
          "description" : "ID of specified Resource instance",
          "required" : true,
          "type" : "string"
        }, {
          "in" : "body",
          "name" : "giveFeedbackDetail",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/giveFeedbackDetail_96569dfd073710000ff99236d1850077"
          }
        } ],
        "responses" : {
          "default" : {
            "description" : "an error occurred",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          },
          "201" : {
            "description" : "posting response",
            "schema" : {
              "$ref" : "#/definitions/giveFeedbackDetail_96569dfd073710000ff99236d1850077"
            }
          },
          "400" : {
            "description" : "a validation error occurred (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/feedbackBadges" : {
      "get" : {
        "tags" : [ "feedbackBadges" ],
        "description" : "Retrieves a collection of active Feedback Badges.\n\nSecured by: Give Feedback\n\nScope: Talent Core",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "limit",
          "in" : "query",
          "description" : "The number of instances to retrieve",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        }, {
          "name" : "offset",
          "in" : "query",
          "description" : "The offset in the collection",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        } ],
        "responses" : {
          "200" : {
            "description" : "getting response",
            "schema" : {
              "type" : "object",
              "description" : "collection something or other",
              "properties" : {
                "data" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/feedbackBadgeDetail_9eab868ca81410001402525d054211f7"
                  }
                },
                "total" : {
                  "type" : "integer",
                  "format" : "int64"
                }
              }
            }
          },
          "default" : {
            "description" : "an error occurred",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/feedbackBadges/{ID}" : {
      "get" : {
        "tags" : [ "feedbackBadges" ],
        "description" : "Retrieves a collection of active Feedback Badges.\n\nSecured by: Give Feedback\n\nScope: Talent Core",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "ID",
          "in" : "path",
          "description" : "ID of specified Resource instance",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "getting response",
            "schema" : {
              "$ref" : "#/definitions/feedbackBadgeDetail_9eab868ca81410001402525d054211f7"
            }
          },
          "default" : {
            "description" : "an error occurred",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    }
  },
  "securityDefinitions" : {
    "OAuth2" : {
      "type" : "oauth2",
      "authorizationUrl" : "https://<tenant authorization hostname>",
      "flow" : "implicit"
    }
  },
  "definitions" : {
    "INSTANCE_MODEL_REFERENCE" : {
      "type" : "object",
      "required" : [ "id" ],
      "properties" : {
        "id" : {
          "type" : "string",
          "description" : "wid / id / reference id",
          "pattern" : "^(?:(?:[0-9a-f]{32})|(?:[0-9]+\\$[0-9]+)|(\\S+=\\S+))$"
        },
        "descriptor" : {
          "type" : "string",
          "description" : "A description of the instance",
          "readOnly" : true
        },
        "href" : {
          "type" : "string",
          "description" : "A link to the instance",
          "readOnly" : true
        }
      }
    },
    "ERROR_MODEL_REFERENCE" : {
      "type" : "object",
      "required" : [ "error" ],
      "properties" : {
        "error" : {
          "type" : "string",
          "description" : "A description of the error"
        }
      }
    },
    "VALIDATION_ERROR_MODEL_REFERENCE" : {
      "allOf" : [ {
        "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
      }, {
        "properties" : {
          "errors" : {
            "type" : "array",
            "description" : "An array of validation errors",
            "items" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      } ]
    },
    "FACETS_MODEL_REFERENCE" : {
      "type" : "array",
      "items" : {
        "type" : "object",
        "description" : "This object represents the possible facets for this resource",
        "readOnly" : true,
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "description" : "A description of the facet"
          },
          "facetParameter" : {
            "type" : "string",
            "description" : "The alias used to select the facet"
          },
          "values" : {
            "type" : "array",
            "description" : "the facet values",
            "items" : {
              "type" : "object",
              "properties" : {
                "count" : {
                  "type" : "integer",
                  "format" : "int32",
                  "description" : "The number of instances returned by this facet"
                },
                "id" : {
                  "type" : "string",
                  "description" : "wid / id / reference id",
                  "pattern" : "^(?:(?:[0-9a-f]{32})|(?:[0-9]+\\$[0-9]+)|(\\S+=\\S+))$"
                },
                "descriptor" : {
                  "type" : "string",
                  "description" : "A description of the facet"
                },
                "href" : {
                  "type" : "string",
                  "description" : "A link to the instance",
                  "readOnly" : true
                }
              },
              "required" : [ "id" ]
            }
          }
        }
      }
    },
    "MULTIPLE_INSTANCE_MODEL_REFERENCE" : {
      "type" : "object",
      "properties" : {
        "total" : {
          "type" : "integer"
        },
        "data" : {
          "type" : "array",
          "items" : {
            "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
          }
        }
      }
    },
    "location_d6235be431f940598806053af7ba94ef" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "primarySupervisoryOrganization_851e1342d4c2489c9680fb2a899227ca" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "workerProfile_c0a0dce56eb142d39dbffeb505becf7a" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "primaryWorkEmail" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "isManager" : {
            "type" : "boolean",
            "example" : true,
            "x-workday-type" : "Boolean"
          },
          "primaryWorkPhone" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "location" : {
            "$ref" : "#/definitions/location_d6235be431f940598806053af7ba94ef"
          },
          "primarySupervisoryOrganization" : {
            "$ref" : "#/definitions/primarySupervisoryOrganization_851e1342d4c2489c9680fb2a899227ca"
          },
          "primaryWorkAddressText" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "yearsOfService" : {
            "type" : "integer",
            "example" : "560763167",
            "x-workday-type" : "Numeric"
          },
          "businessTitle" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "dateOfBirth" : {
            "type" : "string",
            "format" : "date",
            "example" : "2019-12-17T08:00:00.000Z",
            "x-workday-type" : "Date"
          },
          "supervisoryOrganizationsManaged" : {
            "type" : "string",
            "format" : "url",
            "readOnly" : true,
            "x-workday-type" : "Multi-instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "href" : {
            "type" : "string",
            "description" : "A link to the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          }
        }
      } ]
    },
    "primarySupervisoryOrganization_642d7497e69c48da946fc503f66d7da1" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "workerSummary_a489aef739484c13a59e6d502a9e7b68" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "primaryWorkPhone" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "isManager" : {
            "type" : "boolean",
            "example" : true,
            "x-workday-type" : "Boolean"
          },
          "primaryWorkEmail" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "businessTitle" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "primarySupervisoryOrganization" : {
            "$ref" : "#/definitions/primarySupervisoryOrganization_642d7497e69c48da946fc503f66d7da1"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "href" : {
            "type" : "string",
            "description" : "A link to the instance"
          }
        }
      } ]
    },
    "event_e51f91610b6a100025350c013cd8000e" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "badge_a6ab9069588610002b8c0c8217af0012" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "toWorker_a6ab9069588610002b8c0c1d3d610011" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "fromWorker_a6ab9069588610002b8c0bb4e5d80010" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "feedbackSummary_a6ab9069588610002b8c0b4f60a8000f" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "relatesTo" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/gigTalentTag_cc3d9bf215cb1000068d845144d900ec"
            },
            "x-workday-type" : "Multi-instance"
          },
          "showFeedbackProviderName" : {
            "type" : "boolean",
            "example" : true,
            "x-workday-type" : "Boolean"
          },
          "hiddenFromWorker" : {
            "type" : "boolean",
            "example" : true,
            "x-workday-type" : "Boolean"
          },
          "event" : {
            "$ref" : "#/definitions/event_e51f91610b6a100025350c013cd8000e"
          },
          "comment" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Rich Text"
          },
          "feedbackGivenDate" : {
            "type" : "string",
            "format" : "date",
            "example" : "2019-12-17T08:00:00.000Z",
            "x-workday-type" : "Date"
          },
          "hiddenFromManager" : {
            "type" : "boolean",
            "example" : true,
            "x-workday-type" : "Boolean"
          },
          "badge" : {
            "$ref" : "#/definitions/badge_a6ab9069588610002b8c0c8217af0012"
          },
          "toWorker" : {
            "$ref" : "#/definitions/toWorker_a6ab9069588610002b8c0c1d3d610011"
          },
          "fromWorker" : {
            "$ref" : "#/definitions/fromWorker_a6ab9069588610002b8c0bb4e5d80010"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "href" : {
            "type" : "string",
            "description" : "A link to the instance"
          }
        }
      } ]
    },
    "badge_96569dfd073710000ff9930f04160079" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "giveFeedbackDetail_96569dfd073710000ff99236d1850077" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "hiddenFromManager" : {
            "type" : "boolean",
            "example" : true,
            "x-workday-type" : "Boolean"
          },
          "showFeedbackProviderName" : {
            "type" : "boolean",
            "example" : true,
            "x-workday-type" : "Boolean"
          },
          "feedbackGivenDate" : {
            "type" : "string",
            "format" : "date",
            "example" : "2019-12-17T08:00:00.000Z",
            "x-workday-type" : "Date"
          },
          "hiddenFromWorker" : {
            "type" : "boolean",
            "example" : true,
            "x-workday-type" : "Boolean"
          },
          "badge" : {
            "$ref" : "#/definitions/badge_96569dfd073710000ff9930f04160079"
          },
          "relatesTo" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/gigTalentTag_cc3d9bf215cb1000068d845144d900ec"
            },
            "x-workday-type" : "Multi-instance"
          },
          "comment" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Rich Text"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "href" : {
            "type" : "string",
            "description" : "A link to the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          }
        }
      } ]
    },
    "feedbackBadgeIcon_d4d355b2d3db100020b8608b75250016" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "feedbackBadgeDetail_9eab868ca81410001402525d054211f7" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "feedbackBadgeIcon" : {
            "$ref" : "#/definitions/feedbackBadgeIcon_d4d355b2d3db100020b8608b75250016"
          },
          "workdayID" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "maxLength" : 36,
            "x-workday-type" : "Text"
          },
          "feedbackBadgeID" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "name" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          }
        }
      } ]
    },
    "gigTalentTag_cc3d9bf215cb1000068d845144d900ec" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          }
        }
      } ]
    }
  }
}
