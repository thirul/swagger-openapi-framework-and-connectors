{
  "swagger" : "2.0",
  "info" : {
    "description" : "The Student Core service enables you to get core information about students.",
    "version" : "v1",
    "title" : "studentCore"
  },
  "host" : "<tenant hostname>",
  "basePath" : "/studentCore/v1",
  "tags" : [ {
    "name" : "students",
    "description" : "This resource returns all matriculated students or a collection of matriculated students filtered by query parameters.\nScope: Student Core"
  } ],
  "schemes" : [ "https" ],
  "paths" : {
    "/students/{ID}/residencies" : {
      "get" : {
        "tags" : [ "students" ],
        "summary" : "Retrieves a collection of residence information about the specified student ID.",
        "description" : "Retrieves a collection of residence information about the specified student ID.\nScope: Student Core\n\nSecured by: Manage: Student Residency, Self-Service: Student External Site User Access",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "ID",
          "in" : "path",
          "description" : "The Workday ID of the resource.",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "institutionalAcademicUnit",
          "in" : "query",
          "description" : "The Institutional Academic Units.",
          "required" : false,
          "type" : "array",
          "items" : {
            "type" : "string"
          },
          "collectionFormat" : "multi"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "The maximum number of objects in a single response. The default is 20. The maximum is 100.",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        }, {
          "name" : "offset",
          "in" : "query",
          "description" : "The zero-based index of the first object in a response collection. The default is 0. Use offset with the limit parameter to control paging of a response collection. Example: If limit is 5 and offset is 9, the response returns a collection of 5 objects starting with the 10th object.",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        } ],
        "responses" : {
          "200" : {
            "description" : "Successful response. A successful response can return no matched data.",
            "schema" : {
              "type" : "object",
              "properties" : {
                "data" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/residencySummary_b68d565b8eb610001987381601cf0144"
                  }
                },
                "total" : {
                  "type" : "integer",
                  "format" : "int64"
                }
              },
              "description" : "collection something or other"
            }
          },
          "default" : {
            "description" : "An error occurred.",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          },
          "400" : {
            "description" : "Invalid request. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "401" : {
            "description" : "Invalid resource or operation. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "403" : {
            "description" : "User has insufficient permissions. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "404" : {
            "description" : "Resource not found. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/students" : {
      "get" : {
        "tags" : [ "students" ],
        "summary" : "Retrieves a collection of students.",
        "description" : "Retrieves a collection of students.\nScope: Student Core\n\nSecured by: Self-Service: Student Profile, Reports: Students",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "academicLevel",
          "in" : "query",
          "description" : "Derived Academic Level.",
          "required" : false,
          "type" : "array",
          "items" : {
            "type" : "string"
          },
          "collectionFormat" : "multi"
        }, {
          "name" : "academicUnit",
          "in" : "query",
          "description" : "Derived Academic Units.",
          "required" : false,
          "type" : "array",
          "items" : {
            "type" : "string"
          },
          "collectionFormat" : "multi"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "The maximum number of objects in a single response. The default is 20. The maximum is 100.",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        }, {
          "name" : "offset",
          "in" : "query",
          "description" : "The zero-based index of the first object in a response collection. The default is 0. Use offset with the limit parameter to control paging of a response collection. Example: If limit is 5 and offset is 9, the response returns a collection of 5 objects starting with the 10th object.",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        }, {
          "name" : "programOfStudy",
          "in" : "query",
          "description" : "For students who haven't matriculated, the field shows the program of study they either applied to or have been admitted to. For matriculated students, the field shows the primary program of study from their reporting record.",
          "required" : false,
          "type" : "array",
          "items" : {
            "type" : "string"
          },
          "collectionFormat" : "multi"
        } ],
        "responses" : {
          "200" : {
            "description" : "Successful response. A successful response can return no matched data.",
            "schema" : {
              "type" : "object",
              "properties" : {
                "data" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/studentSummary_53782cb2f04e10001976f95b3edd0121"
                  }
                },
                "total" : {
                  "type" : "integer",
                  "format" : "int64"
                }
              },
              "description" : "collection something or other"
            }
          },
          "default" : {
            "description" : "An error occurred.",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          },
          "400" : {
            "description" : "Invalid request. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "401" : {
            "description" : "Invalid resource or operation. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "403" : {
            "description" : "User has insufficient permissions. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "404" : {
            "description" : "Resource not found. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/students/{ID}" : {
      "get" : {
        "tags" : [ "students" ],
        "summary" : "Retrieves a collection of students.",
        "description" : "Retrieves a collection of students.\nScope: Student Core\n\nSecured by: Self-Service: Student Profile, Reports: Students",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "ID",
          "in" : "path",
          "description" : "The Workday ID of the resource.",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "Successful response. A successful response can return no matched data.",
            "schema" : {
              "$ref" : "#/definitions/studentDetail_6393b5f0dcfa10000f524d62761400f4"
            }
          },
          "default" : {
            "description" : "An error occurred.",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          },
          "400" : {
            "description" : "Invalid request. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "401" : {
            "description" : "Invalid resource or operation. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "403" : {
            "description" : "User has insufficient permissions. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "404" : {
            "description" : "Resource not found. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/students/{ID}/residencies/{subresourceID}" : {
      "get" : {
        "tags" : [ "students" ],
        "summary" : "Retrieves a collection of residence information about the specified student ID.",
        "description" : "Retrieves a collection of residence information about the specified student ID.\nScope: Student Core\n\nSecured by: Manage: Student Residency, Self-Service: Student External Site User Access",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "ID",
          "in" : "path",
          "description" : "The Workday ID of the resource.",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "subresourceID",
          "in" : "path",
          "description" : "The Workday ID of the subresource.",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "Successful response. A successful response can return no matched data.",
            "schema" : {
              "$ref" : "#/definitions/residencySummary_b68d565b8eb610001987381601cf0144"
            }
          },
          "default" : {
            "description" : "An error occurred.",
            "schema" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          },
          "400" : {
            "description" : "Invalid request. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "401" : {
            "description" : "Invalid resource or operation. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "403" : {
            "description" : "User has insufficient permissions. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          },
          "404" : {
            "description" : "Resource not found. (https://community.workday.com/rest/error-messages)",
            "schema" : {
              "$ref" : "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    }
  },
  "securityDefinitions" : {
    "OAuth2" : {
      "type" : "oauth2",
      "authorizationUrl" : "https://<tenant authorization hostname>",
      "flow" : "implicit"
    }
  },
  "definitions" : {
    "INSTANCE_MODEL_REFERENCE" : {
      "type" : "object",
      "required" : [ "id" ],
      "properties" : {
        "id" : {
          "type" : "string",
          "description" : "wid / id / reference id",
          "pattern" : "^(?:(?:[0-9a-f]{32})|(?:[0-9]+\\$[0-9]+)|(\\S+=\\S+))$"
        },
        "descriptor" : {
          "type" : "string",
          "description" : "A description of the instance",
          "readOnly" : true
        },
        "href" : {
          "type" : "string",
          "description" : "A link to the instance",
          "readOnly" : true
        }
      }
    },
    "ERROR_MODEL_REFERENCE" : {
      "type" : "object",
      "required" : [ "error" ],
      "properties" : {
        "error" : {
          "type" : "string",
          "description" : "A description of the error"
        }
      }
    },
    "VALIDATION_ERROR_MODEL_REFERENCE" : {
      "allOf" : [ {
        "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
      }, {
        "properties" : {
          "errors" : {
            "type" : "array",
            "description" : "An array of validation errors",
            "items" : {
              "$ref" : "#/definitions/ERROR_MODEL_REFERENCE"
            }
          }
        }
      } ]
    },
    "FACETS_MODEL_REFERENCE" : {
      "type" : "array",
      "items" : {
        "type" : "object",
        "description" : "This object represents the possible facets for this resource",
        "readOnly" : true,
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "description" : "A description of the facet"
          },
          "facetParameter" : {
            "type" : "string",
            "description" : "The alias used to select the facet"
          },
          "values" : {
            "type" : "array",
            "description" : "the facet values",
            "items" : {
              "type" : "object",
              "properties" : {
                "count" : {
                  "type" : "integer",
                  "format" : "int32",
                  "description" : "The number of instances returned by this facet"
                },
                "id" : {
                  "type" : "string",
                  "description" : "wid / id / reference id",
                  "pattern" : "^(?:(?:[0-9a-f]{32})|(?:[0-9]+\\$[0-9]+)|(\\S+=\\S+))$"
                },
                "descriptor" : {
                  "type" : "string",
                  "description" : "A description of the facet"
                },
                "href" : {
                  "type" : "string",
                  "description" : "A link to the instance",
                  "readOnly" : true
                }
              },
              "required" : [ "id" ]
            }
          }
        }
      }
    },
    "MULTIPLE_INSTANCE_MODEL_REFERENCE" : {
      "type" : "object",
      "properties" : {
        "total" : {
          "type" : "integer"
        },
        "data" : {
          "type" : "array",
          "items" : {
            "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
          }
        }
      }
    },
    "location_6393b5f0dcfa10000fe069e4a5620102" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "person_15308ff39d95100018213f71d8a60183" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "studentDetail_6393b5f0dcfa10000f524d62761400f4" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "location" : {
            "description" : "The location associated with the student's earliest active institutional reporting record.",
            "$ref" : "#/definitions/location_6393b5f0dcfa10000fe069e4a5620102"
          },
          "Student_ID" : {
            "type" : "string",
            "example" : "627272",
            "description" : "The student ID of the student.",
            "x-workday-type" : "Text"
          },
          "internationalStudent" : {
            "type" : "boolean",
            "example" : true,
            "description" : "True if the student is not a citizen of the United States or one of its territories and is a citizen of another ~country~.",
            "x-workday-type" : "Boolean"
          },
          "person" : {
            "description" : "Returns the person for this role.",
            "$ref" : "#/definitions/person_15308ff39d95100018213f71d8a60183"
          },
          "firstGeneration" : {
            "type" : "boolean",
            "example" : true,
            "description" : "Indicator for first generation student.",
            "x-workday-type" : "Boolean"
          },
          "primaryStudentRecord" : {
            "type" : "array",
            "description" : "Returns all Academic Records for the Student.",
            "items" : {
              "$ref" : "#/definitions/studentRecordSummary_4502a49c080f10001e6f44d7a4ec0104"
            },
            "x-workday-type" : "Multi-instance"
          },
          "militaryRelationship" : {
            "type" : "boolean",
            "example" : true,
            "description" : "Returns students that have a military relationship (e.g. veteran, military spouse)",
            "x-workday-type" : "Boolean"
          },
          "preferredName" : {
            "type" : "string",
            "example" : "Sally Student",
            "description" : "Returns the fully formatted preferred name for the Student.",
            "x-workday-type" : "Text"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          }
        }
      } ]
    },
    "studentSummary_53782cb2f04e10001976f95b3edd0121" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "Student_ID" : {
            "type" : "string",
            "example" : "627272",
            "description" : "The student ID of the student.",
            "x-workday-type" : "Text"
          },
          "primaryStudentRecord" : {
            "type" : "array",
            "description" : "Returns all Academic Records for the Student.",
            "items" : {
              "$ref" : "#/definitions/studentRecordSummary_4502a49c080f10001e6f44d7a4ec0104"
            },
            "x-workday-type" : "Multi-instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          }
        }
      } ]
    },
    "effectiveAcademicPeriod_42148ec8f5c51000110201db1e0e0136" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "detail_42148ec8f5c51000110201e54d680138" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "reason_42148ec8f5c51000110201e068d30137" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "status_b68d565b8eb6100019873838ec930146" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "residencySummary_b68d565b8eb610001987381601cf0144" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "effectiveAcademicPeriod" : {
            "description" : "The academic period in which this student residency record took effect.",
            "$ref" : "#/definitions/effectiveAcademicPeriod_42148ec8f5c51000110201db1e0e0136"
          },
          "events" : {
            "type" : "array",
            "description" : "All student residency events associated with this student residency record.",
            "items" : {
              "$ref" : "#/definitions/residencyEvent_2d9e3128b5ff10000fedaefb4fdd0113"
            },
            "x-workday-type" : "Multi-instance"
          },
          "detail" : {
            "description" : "Indicates whether the student is in or out of district (for residents only).",
            "$ref" : "#/definitions/detail_42148ec8f5c51000110201e54d680138"
          },
          "documents" : {
            "type" : "array",
            "description" : "The documents that the student provided through the residency determination questionnaire for this applicant's residency status as of the end date of the academic period (or dynamic date).",
            "items" : {
              "$ref" : "#/definitions/studentDocument_42148ec8f5c51000113499b7a5f8013a"
            },
            "x-workday-type" : "Multi-instance"
          },
          "reason" : {
            "description" : "The residency reason for the student residency record.",
            "$ref" : "#/definitions/reason_42148ec8f5c51000110201e068d30137"
          },
          "effective" : {
            "type" : "string",
            "format" : "date",
            "example" : "2020-09-08T07:00:00.000Z",
            "description" : "The date on which this residency status became (or will become) effective.",
            "x-workday-type" : "Date"
          },
          "institutionalAcademicUnits" : {
            "type" : "array",
            "description" : "The Institution Academic Units.",
            "items" : {
              "$ref" : "#/definitions/academicUnit_2d9e3128b5ff10000f66e7609e070111"
            },
            "x-workday-type" : "Multi-instance"
          },
          "declaration" : {
            "type" : "string",
            "format" : "date",
            "example" : "2020-09-08T07:00:00.000Z",
            "description" : "The date on which this student declared residency.",
            "x-workday-type" : "Date"
          },
          "status" : {
            "description" : "The student's current residency status.",
            "$ref" : "#/definitions/status_b68d565b8eb6100019873838ec930146"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          }
        }
      } ]
    },
    "academicUnit_2d9e3128b5ff10000f66e7609e070111" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          }
        }
      } ]
    },
    "studentDocument_42148ec8f5c51000113499b7a5f8013a" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          }
        }
      } ]
    },
    "residencyEvent_2d9e3128b5ff10000fedaefb4fdd0113" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          }
        }
      } ]
    },
    "programOfStudy_4502a49c080f10001e6f44f976050107" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "academicUnit_4502a49c080f10001e6f44f3a80e0106" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "academicLevel_4502a49c080f10001e6f44e9fb670105" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "status_382d56cb804d100012f64e43e6120147" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "classStanding_382d56cb804d1000131fef31178f0149" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "studentRecordSummary_4502a49c080f10001e6f44d7a4ec0104" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "programOfStudy" : {
            "description" : "Returns the primary Program of Study for the Student Record.",
            "$ref" : "#/definitions/programOfStudy_4502a49c080f10001e6f44f976050107"
          },
          "academicUnit" : {
            "description" : "Returns the Academic Unit of the Primary Program of Study of the Reporting Record (for Matriculated Student) or Primary Program of Study of the Application (for Applicant) as of the standard start date for the financial aid period record.",
            "$ref" : "#/definitions/academicUnit_4502a49c080f10001e6f44f3a80e0106"
          },
          "academicLevel" : {
            "description" : "Academic Level for Student Record",
            "$ref" : "#/definitions/academicLevel_4502a49c080f10001e6f44e9fb670105"
          },
          "status" : {
            "description" : "Returns the current designation of Active or Inactive for the Student. A Student is considered Active if they are actively pursuing at least one Program of Study, as determined by the status on their Student Program of Study Record.",
            "$ref" : "#/definitions/status_382d56cb804d100012f64e43e6120147"
          },
          "classStanding" : {
            "description" : "The currently effective Class Standing for this Student Record",
            "$ref" : "#/definitions/classStanding_382d56cb804d1000131fef31178f0149"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          }
        }
      } ]
    }
  }
}