{
  "swagger": "2.0",
  "info": {
    "description": "The Performance Enablement public service contains resources that expose Workday Employee Performance Enablement Business Services data. This service can be used for integration with other employee performance enablement systems.",
    "version": "v3",
    "title": "performanceEnablement"
  },
  "host": "<tenant hostname>",
  "basePath": "/performanceEnablement/v3",
  "tags": [
    {
      "name": "feedbackBadges",
      "description": "A collection of active Feedback Badges in Workday"
    },
    {
      "name": "workers"
    }
  ],
  "schemes": [
    "https"
  ],
  "paths": {
    "/feedbackBadges": {
      "get": {
        "tags": [
          "feedbackBadges"
        ],
        "summary": "Retrieves a collection of active Feedback Badges.",
        "description": "Secured by: Give Feedback initiating action\nScope: Talent Core",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "limit",
            "in": "query",
            "description": "The number of instances to retrieve",
            "required": false,
            "type": "integer",
            "format": "int64"
          },
          {
            "name": "offset",
            "in": "query",
            "description": "The offset in the collection",
            "required": false,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful response. A successful response can return no matched data.",
            "schema": {
              "type": "object",
              "description": "A collection of active Feedback Badges.",
              "properties": {
                "data": {
                  "type": "array",
                  "items": {
                    "$ref": "#/definitions/feedbackBadgeDetail_9eab868ca81410001402525d054211f7"
                  }
                },
                "total": {
                  "type": "integer",
                  "format": "int64"
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized. See the REST API Error Messages on https://community.workday.com/rest/error-messages.",
            "schema": {
              "$ref": "#/definitions/ERROR_MODEL_REFERENCE"   
            }
          }
        }
      }
    },
    "/feedbackBadges/{ID}": {
      "get": {
        "tags": [
          "feedbackBadges"
        ],
        "summary": "Retrieves an instance of Feedback Badge.",
        "description": "Secured by: Give Feedback initiating action\nScope: Talent Core",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "ID",
            "in": "path",
            "description": "Id of specified instance",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful response. A successful response can return no matched data.",
            "schema": {
              "$ref": "#/definitions/feedbackBadgeDetail_9eab868ca81410001402525d054211f7"
            }
          },
          "401": {
            "description": "Unauthorized. See the REST API Error Messages on https://community.workday.com/rest/error-messages.",
            "schema": {
              "$ref": "#/definitions/ERROR_MODEL_REFERENCE"   
            }
          }
        }
      }
    },
        "/workers" : {
      "get" : {
        "tags" : [ "workers" ],
        "description" : "Secured by: Worker Data: Public Worker Reports, Self-Service: Current Staffing Information",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "search",
          "in" : "query",
          "description" : "Searches ~workers~ by name. The search is case-insensitive. You can include space-delimited search strings for an OR search.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "The number of instances to retrieve",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        }, {
          "name" : "offset",
          "in" : "query",
          "description" : "The offset in the collection",
          "required" : false,
          "type" : "integer",
          "format" : "int64"
        } ],
        "responses" : {
          "200" : {
            "description" : "Successful response. A successful response can return no matched data.",
            "schema" : {
              "type" : "object",
              "properties" : {
                "data" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/definitions/workerSummary_a489aef739484c13a59e6d502a9e7b68"
                  }
                },
                "total" : {
                  "type" : "integer",
                  "format" : "int64"
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized. See the REST API Error Messages on https://community.workday.com/rest/error-messages.",
            "schema": {
              "$ref": "#/definitions/ERROR_MODEL_REFERENCE"   
            }
          }
        }
      }
    },
    "/workers/{ID}" : {
      "get" : {
        "tags" : [ "workers" ],
        "description" : "Secured by: Worker Data: Public Worker Reports, Self-Service: Current Staffing Information",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "ID",
          "in" : "path",
          "description" : "The Workday ID of the worker. You can use a returned id from GET /workers in the Staffing service /staffing.",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "Successful response. A successful response can return no matched data.",
            "schema" : {
              "$ref" : "#/definitions/workerProfile_c0a0dce56eb142d39dbffeb505becf7a"
            }
          },
          "401": {
            "description": "Unauthorized. See the REST API Error Messages on https://community.workday.com/rest/error-messages.",
            "schema": {
              "$ref": "#/definitions/ERROR_MODEL_REFERENCE"   
            }
          }
        }
      }
    },
    "/workers/{ID}/anytimeFeedbackEntries": {
      "get": {
        "tags": [
          "workers"
        ],
        "summary": "Retrieves a collection of feedback given about the specified worker.",
        "description": "Secured by: Self-Service: Anytime Feedback, Worker Data: Anytime Feedback\nScope: Talent Core",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "limit",
            "in": "query",
            "description": "The maximum number of objects in a single response. The default is 20. The maximum is 100.",
            "required": false,
            "type": "integer",
            "format": "int64"
          },
          {
            "name": "offset",
            "in": "query",
            "description": "The zero-based index of the first object in a response collection. The default is 0.",
            "required": false,
            "type": "integer",
            "format": "int64"
          },
          {
            "name": "ID",
            "in": "path",
            "description": "The Workday ID of the worker. You can use a returned id from GET /workers in the Staffing service /staffing.",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful response. A successful response can return no matched data.",
            "schema": {
              "type": "object",
              "description": "A collection of Feedback Response objects.",
              "properties": {
                "data": {
                  "type": "array",
                  "items": {
                    "$ref": "#/definitions/feedbackSummary_a6ab9069588610002b8c0b4f60a8000f"
                  }
                },
                "total": {
                  "type": "integer",
                  "format": "int64"
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized. See the REST API Error Messages on https://community.workday.com/rest/error-messages.",
            "schema": {
              "$ref": "#/definitions/ERROR_MODEL_REFERENCE"   
            }
          }
        }
      },
      "post": {
        "tags": [
          "workers"
        ],
        "summary": "Creates feedback about the specified workers.If you want to see the status of conditionally hidden attributes for this request, call this method with the wd-metadata-api-version header. Set the header value to v1 (or the latest Workday Metadata API version). When you specify the wd-metadata-api-version header, this method returns the response metadata, instead of the actual data.",
        "description": "Secured by: Give Feedback initiating Action\nScope: Talent Core",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "ID",
            "in": "path",
            "description": "The Workday ID of the worker. You can use a returned id from GET /workers in the Staffing service /staffing.",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "giveFeedbackDetail",
            "description" : "The feedback entry to create. Specify at least the required field: comment",
            "required": true,
            "schema": {
              "$ref": "#/definitions/giveFeedbackDetail_f85bac1dba7d10001a106f2a4fbc03e3"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Feedback entry created.",
            "schema": {
              "$ref": "#/definitions/giveFeedbackDetail_f85bac1dba7d10001a106f2a4fbc03e3"
            }
          },
          "400": {
            "description": "A validation error occurred. See the REST API Error Messages on https://community.workday.com/rest/error-messages.",
            "schema": {
              "$ref": "#/definitions/VALIDATION_ERROR_MODEL_REFERENCE"
            }
          }
        }
      }
    },
    "/workers/{ID}/anytimeFeedbackEntries/{subresourceID}" : {
      "get" : {
        "tags" : [ "workers" ],
        "summary" : "Retrieves an instance of feedback given about the specified worker.",
        "description" : "Secured by: Self-Service: Anytime Feedback, Worker Data: Anytime Feedback\nScope: Talent Core",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "ID",
          "in" : "path",
          "description" : "The Workday ID of the worker. You can use a returned id from GET /workers in the Staffing service /staffing.",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "subresourceID",
          "in" : "path",
          "description" : "The Workday ID of the feedback given. You can use a returned id from GET /anytimeFeedbackEntries for the specified worker in the Performance Enablement service /performanceEnablement.",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200": {
            "description": "Successful response. A successful response can return no matched data.",
            "schema": {
              "type": "object",
              "description": "Feedback Response object.",
              "properties": {
                "data": {
                  "type": "array",
                  "items": {
                    "$ref": "#/definitions/feedbackSummary_a6ab9069588610002b8c0b4f60a8000f"
                  }
                },
                "total": {
                  "type": "integer",
                  "format": "int64"
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized. See the REST API Error Messages on https://community.workday.com/rest/error-messages.",
            "schema": {
              "$ref": "#/definitions/ERROR_MODEL_REFERENCE"   
            }
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "OAuth2": {
      "type": "oauth2",
      "authorizationUrl": "https://<tenant authorization hostname>",
      "flow": "implicit"
    }
  },
  "definitions": {
    "feedbackBadgeIcon_d4d355b2d3db100020b8608b75250016": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "feedbackBadgeDetail_9eab868ca81410001402525d054211f7": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "feedbackBadgeIcon": {
              "$ref": "#/definitions/feedbackBadgeIcon_d4d355b2d3db100020b8608b75250016"
            },
            "workdayID": {
              "type": "string",
              "description": "Returns the Workday ID for a given object.",
              "maxLength": 36,
              "x-workday-type": "Text"
            },
            "feedbackBadgeID": {
              "type": "string",
              "description": "The Reference ID to use for lookups within our Workday Web Services. For supervisory organizations, this is also the 'Organization ID'",
              "x-workday-type": "Text"
            },
            "name": {
              "type": "string",
              "description": "The name of this feedback badge.",
              "x-workday-type": "Text"
            }
          }
        }
      ]
    },
    "badge_3ab33366103410000fb3b67b142602ee": {
      "allOf": [
        {
          "$ref": "#/definitions/FB_BADGE_INSTANCE_MODEL_REFERENCE"
        },
      ],
      "x-conditionally-hidden" : true
    },
    "giveFeedbackDetail_f85bac1dba7d10001a106f2a4fbc03e3": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "feedbackGivenDate": {
              "type": "string",
              "format": "date",
              "description": "The date the feedback was provided, using yyyy-mm-dd format.",
              "x-workday-type": "Date"
            },
            "workersToNotify" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/getWorkersToNotify_9ce4e737a1a310000b42a2fb607801fe"
              },
              "x-workday-type" : "Multi-instance"
            },
            "feedbackAlsoAbout": {
              "type": "array",
              "description": "The other ~workers~ this feedback is about.",
              "items": {
                "$ref": "#/definitions/getWorkersFeedbackAlsoAbout_3ab333661034100010b5635b2f7a0302"
              },
              "x-workday-type": "Multi-instance"
            },
            "hiddenFromManager": {
              "type": "boolean",
              "description": "If true, the feedback is shared between the feedback provider and the Worker. Manager does not see the feedback. The default is false.",
              "x-workday-type": "Boolean",
              "x-conditionally-hidden" : true
            },
            "comment": {
              "type": "string",
              "description": "Returns the Feedback Comment given about the subject.",
              "x-workday-type": "Rich Text"
            },
            "hiddenFromWorker": {
              "type": "boolean",
              "description": "If true, feedback is shared between the feedback provider and the Worker's Manager. Worker does not see the feedback. The default is false.",
              "x-workday-type": "Boolean",
              "x-conditionally-hidden" : true
            },
            "showFeedbackProviderName": {
              "type": "boolean",
              "description": "If true, the name of the feedback provider is displayed. The default is false.",
              "x-workday-type": "Boolean",
              "x-conditionally-hidden" : true
            },
            "feedbackGiven": {
              "type": "array",
              "description": "Returns all the Feedback Given instances for this Feedback Response.",
              "items": {
                "$ref": "#/definitions/feedbackGivenDetail_08183b236a34100006a87450b82d03f1"
              },
              "x-workday-type": "Multi-instance"
            },
            "badge": {
              "$ref": "#/definitions/badge_3ab33366103410000fb3b67b142602ee"
            }
          }
        }
      ]
    },
    "event_e51f91610b6a100025350c013cd8000e": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "badge_a6ab9069588610002b8c0c8217af0012": {
      "allOf": [
        {
          "$ref": "#/definitions/FB_BADGE_INSTANCE_MODEL_REFERENCE"
        },
        {}
      ],
      "x-conditionally-hidden" : true
    },
    "toWorker_a6ab9069588610002b8c0c1d3d610011": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "fromWorker_a6ab9069588610002b8c0bb4e5d80010": {
      "allOf": [
        {
          "$ref": "#/definitions/INSTANCE_MODEL_REFERENCE"
        },
        {}
      ]
    },
    "feedbackSummary_a6ab9069588610002b8c0b4f60a8000f": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "showFeedbackProviderName": {
              "type": "boolean",
              "description": "If true, the name of the feedback provider is displayed.",
              "x-workday-type": "Boolean",
              "x-conditionally-hidden" : true
            },
            "event": {
              "$ref": "#/definitions/event_e51f91610b6a100025350c013cd8000e"
            },
            "hiddenFromWorker": {
              "type": "boolean",
              "description": "If true, feedback is shared between the feedback provider and the Worker's Manager. Worker does not see the feedback.",
              "x-workday-type": "Boolean",
              "x-conditionally-hidden" : true
            },
            "comment": {
              "type": "string",
              "description": "Returns the comment text for Anytime Feedback.",
              "x-workday-type": "Rich Text"
            },
            "hiddenFromManager": {
              "type": "boolean",
              "description": "If true, the feedback is shared between the feedback provider and the Worker. Manager does not see the feedback.",
              "x-workday-type": "Boolean",
              "x-conditionally-hidden" : true
            },
            "feedbackGivenDate": {
              "type": "string",
              "format": "date",
              "description": "The date the feedback was provided, using yyyy-mm-dd format.",
              "x-workday-type": "Date"
            },
            "badge": {
              "$ref": "#/definitions/badge_a6ab9069588610002b8c0c8217af0012"
            },
            "toWorker": {
              "$ref": "#/definitions/toWorker_a6ab9069588610002b8c0c1d3d610011"
            },
            "fromWorker": {
              "$ref": "#/definitions/fromWorker_a6ab9069588610002b8c0bb4e5d80010"
            },
            "workersToNotify" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/getWorkersToNotify_9ce4e737a1a310000affedad546b01fb"
              },
              "x-workday-type" : "Multi-instance"
            },
            "feedbackAlsoGivenTo": {
              "type": "array",
              "description": "The other ~workers~ this feedback is about.",
              "items": {
                "$ref": "#/definitions/getFeedbackAlsoGivenToDetails_cc1aebeffa8c1000216d8d4c4d98033f"
              },
              "x-workday-type": "Multi-instance"
            },
            "descriptor": {
              "type": "string",
              "example": "Lorem ipsum dolor sit ame",
              "description": "A preview of the instance"
            },
            "id": {
              "type": "string",
              "description": "Id of the instance"
            },
            "href": {
              "type": "string",
              "description": "A link to the instance"
            }
          }
        }
      ]
    },
     "workerSummary_a489aef739484c13a59e6d502a9e7b68" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "primaryWorkPhone" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "isManager" : {
            "type" : "boolean",
            "example" : true,
            "x-workday-type" : "Boolean"
          },
          "primaryWorkEmail" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "businessTitle" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "primarySupervisoryOrganization" : {
            "$ref" : "#/definitions/primarySupervisoryOrganization_642d7497e69c48da946fc503f66d7da1"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "href" : {
            "type" : "string",
            "description" : "A link to the instance"
          }
        }
      } ]
    },
     "workerProfile_c0a0dce56eb142d39dbffeb505becf7a" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "primaryWorkEmail" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "isManager" : {
            "type" : "boolean",
            "example" : true,
            "x-workday-type" : "Boolean"
          },
          "primaryWorkPhone" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "location" : {
            "$ref" : "#/definitions/location_d6235be431f940598806053af7ba94ef"
          },
          "primarySupervisoryOrganization" : {
            "$ref" : "#/definitions/primarySupervisoryOrganization_851e1342d4c2489c9680fb2a899227ca"
          },
          "primaryWorkAddressText" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "yearsOfService" : {
            "type" : "integer",
            "example" : "1210581916",
            "x-workday-type" : "Numeric"
          },
          "businessTitle" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit amet, cum choro singulis consectetuer ut, ubique iisque contentiones ex duo. Quo lorem etiam eu.",
            "x-workday-type" : "Text"
          },
          "dateOfBirth" : {
            "type" : "string",
            "format" : "date",
            "example" : "2020-04-08T07:00:00.000Z",
            "x-workday-type" : "Date"
          },
          "supervisoryOrganizationsManaged" : {
            "type" : "string",
            "format" : "url",
            "readOnly" : true,
            "x-workday-type" : "Multi-instance"
          },
          "id" : {
            "type" : "string",
            "description" : "Id of the instance"
          },
          "href" : {
            "type" : "string",
            "description" : "A link to the instance"
          },
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          }
        }
      } ]
    },
      "primarySupervisoryOrganization_642d7497e69c48da946fc503f66d7da1" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },    
    "location_d6235be431f940598806053af7ba94ef" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "primarySupervisoryOrganization_851e1342d4c2489c9680fb2a899227ca" : {
      "allOf" : [ {
        "$ref" : "#/definitions/INSTANCE_MODEL_REFERENCE"
      }, { } ]
    },
    "INSTANCE_MODEL_REFERENCE": {
      "type": "object",
      "required": [
        "id"
      ],
      "properties": {
        "id": {
          "type": "string",
          "description": "The Workday ID of the instance",
          "pattern": "^(?:(?:[0-9a-f]{32})|(?:[0-9]+\\$[0-9]+)|(\\S+=\\S+))$"
        },
        "descriptor": {
          "type": "string",
          "description": "A description of the instance",
          "readOnly": true
        },
        "href": {
          "type": "string",
          "description": "A link to the instance",
          "readOnly": true
        }
      }
    },
    "FB_BADGE_INSTANCE_MODEL_REFERENCE": {
      "type": "object",
      "required": [
        "id"
      ],
      "properties": {
        "id": {
          "type": "string",
          "description": "The Workday ID of the Feedback Badge. You can use a returned id from GET /feedbackBadges in the Performance Enablement service /performanceEnablement.",
          "pattern": "^(?:(?:[0-9a-f]{32})|(?:[0-9]+\\$[0-9]+)|(\\S+=\\S+))$"
        },
        "descriptor": {
          "type": "string",
          "description": "A description of the instance",
          "readOnly": true
        },
        "href": {
          "type": "string",
          "description": "A link to the instance",
          "readOnly": true
        }
      }
    },
    "ERROR_MODEL_REFERENCE": {
      "type": "object",
      "required": [
        "error"
      ],
      "properties": {
        "error": {
          "type": "string",
          "description": "A description of the error"
        }
      }
    },
    "VALIDATION_ERROR_MODEL_REFERENCE": {
      "allOf": [
        {
          "$ref": "#/definitions/ERROR_MODEL_REFERENCE"
        },
        {
          "properties": {
            "errors": {
              "type": "array",
              "description": "An array of validation errors",
              "items": {
                "$ref": "#/definitions/ERROR_MODEL_REFERENCE"
              }
            }
          }
        }
      ]
    },
    "feedbackGivenDetail_08183b236a34100006a87450b82d03f1": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "descriptor": {
              "type": "string",
              "example": "Lorem ipsum dolor sit ame",
              "description": "A preview of the instance"
            },
            "id": {
              "type": "string",
              "description": "Id of the instance"
            }
          }
        }
      ]
    },
    "getWorkersToNotify_9ce4e737a1a310000b42a2fb607801fe" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "The Workday ID of the worker. You can use a returned id from GET /workers in the Staffing service /staffing."
          }
        }
      } ]
    },
    "getFeedbackAlsoGivenToDetails_cc1aebeffa8c1000216d8d4c4d98033f": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "descriptor": {
              "type": "string",
              "example": "Lorem ipsum dolor sit ame",
              "description": "A preview of the instance"
            },
            "id": {
              "type": "string",
              "description": "Id of the instance"
            }
          }
        }
      ]
    },
    "getWorkersFeedbackAlsoAbout_3ab333661034100010b5635b2f7a0302": {
      "allOf": [
        {
          "type": "object",
          "properties": {
            "descriptor": {
              "type": "string",
              "example": "Lorem ipsum dolor sit ame",
              "description": "A preview of the instance"
            },
            "id": {
              "type": "string",
              "description": "Id of the instance"
            }
          }
        }
      ]
    },
    "getWorkersToNotify_9ce4e737a1a310000affedad546b01fb" : {
      "allOf" : [ {
        "type" : "object",
        "properties" : {
          "descriptor" : {
            "type" : "string",
            "example" : "Lorem ipsum dolor sit ame",
            "description" : "A preview of the instance"
          },
          "id" : {
            "type" : "string",
            "description" : "The Workday ID of the worker. You can use a returned id from GET /workers in the Staffing service /staffing."
          }
        }
      } ]
    }
  }
}