{
   "swagger":"2.0",
   "info":{
      "version":"v2",
      "title":"EmailValidation"
   },
   "host":"api.experianaperture.io",
   "basePath":"/",
   "paths":{
      "/email/validation/v1":{
         "post":{
            "tags":[
               "EmailValidation"
            ],
            "summary":"Submits an email address to the service to be validated and returns the result of the validation.",
            "operationId":"EmailValidationV1Post",
            "consumes":[
               "application/json",
               "application/xml",
               "application/x-msgpack"
            ],
            "produces":[
               "application/json",
               "application/xml",
               "application/x-msgpack"
            ],
            "parameters":[
               {
                  "name":"Auth-Token",
                  "in":"header",
                  "description":"Your unique key, called a token, that is required to submit an API request.",
                  "required":true,
                  "type":"string"
               },
               {
                  "name":"Reference-Id",
                  "in":"header",
                  "description":"Optional identifier that will be returned in the response to help you track the request.",
                  "required":false,
                  "type":"string"
               },
               {
                  "name":"emailRequest",
                  "in":"body",
                  "description":"The request body.",
                  "required":true,
                  "schema":{
                     "$ref":"#/definitions/EmailValidationV1Request",
                     "example":{
                        "email":"support@experian.com",
                        "timeout":5
                     }
                  }
               }
            ],
            "responses":{
               "200":{
                  "description":"Success",
                  "schema":{
                     "$ref":"#/definitions/EmailValidationV1Response"
                  },
                  "examples":{
                     "application/json":{
                        "transaction_id":"00000000-0000-0000-0000-000000000000",
                        "result":{
                           "email":"support@experian.com",
                           "confidence":"illegitimate",
                           "verbose":"roleAccount"
                        }
                     }
                  }
               },
               "400":{
                  "description":"Bad Request"
               },
               "401":{
                  "description":"Unauthorized"
               },
               "403":{
                  "description":"Forbidden"
               },
               "404":{
                  "description":"Not Found"
               },
               "406":{
                  "description":"Not Acceptable"
               },
               "408":{
                  "description":"Request Timeout"
               },
               "415":{
                  "description":"Unsupported Media Type"
               },
               "429":{
                  "description":"Too Many Requests"
               },
               "500":{
                  "description":"Internal Server Error"
               },
               "503":{
                  "description":"Service Unavailable"
               }
            }
         }
      }
   },
   "definitions":{
      "EmailValidationV1Request":{
         "description":"The request model.",
         "required":[
            "email"
         ],
         "type":"object",
         "properties":{
            "email":{
               "description":"The email address that is the subject of the validation.",
               "type":"string",
               "example":"support@experian.com"
            },
            "timeout":{
               "format":"int32",
               "description":"Maximum time you are prepared to wait for a response, expressed in seconds. Acceptable values: 3-15. If a timeout occurs, a confidence status of \"unknown\" and a verbose result of \"timeout\" will be returned.",
               "type":"integer"
            }
         },
         "xml":{
            "name":"request"
         },
         "example":{
            "email":"support@experian.com",
            "timeout":5
         }
      },
      "EmailValidationV1Response":{
         "description":"The response model.",
         "type":"object",
         "properties":{
            "reference_id":{
               "description":"If you chose to submit a \"Reference-Id\" in the response body, the value will be returned with the response.",
               "type":"string"
            },
            "transaction_id":{
               "description":"Unique Experian-assigned transaction identifier.",
               "type":"string",
               "example":"ab123ab1-abc1-1234-abcd-ab1a123a1a12"
            },
            "error":{
               "$ref":"#/definitions/ResponseError",
               "description":"The error details."
            },
            "result":{
               "$ref":"#/definitions/EmailValidationV1Result",
               "description":"The results."
            }
         },
         "xml":{
            "name":"response"
         }
      },
      "ResponseError":{
         "description":"Error model containing the error details.",
         "type":"object",
         "properties":{
            "type":{
               "description":"A link to documentation that provides more details about the error you've encountered.",
               "type":"string"
            },
            "title":{
               "description":"The title of the error.",
               "type":"string",
               "example":"Bad Request"
            },
            "detail":{
               "description":"A description of the error.",
               "type":"string",
               "example":"The request body was malformed."
            },
            "instance":{
               "description":"The endpoint that returned the error.",
               "type":"string"
            }
         }
      },
      "EmailValidationV1Result":{
         "description":"Details about the result. Includes the validated data and its confidence level.",
         "type":"object",
         "properties":{
            "email":{
               "description":"The email address that is the subject of the validation.",
               "type":"string",
               "example":"support@experian.com"
            },
            "confidence":{
               "description":"The outcome (confidence level) of the validation.",
               "type":"string",
               "example":"Verified, Unknown"
            },
            "message":{
               "description":"Ignore, currently not used.",
               "type":"string"
            },
            "verbose":{
               "description":"Additional information on the confidence level.",
               "type":"string",
               "example":"verified, mailboxFull, unreachable"
            },
            "corrections":{
               "description":"A list of more likely email addresses. Suggestions include fixes to syntax errors in the provided email address, typos in domains etc.",
               "uniqueItems":false,
               "type":"array",
               "items":{
                  "type":"string",
                  "xml":{
                     "name":"correction"
                  }
               },
               "xml":{
                  "name":"corrections",
                  "wrapped":true
               }
            }
         }
      }
   }
}