<?xml version="1.0" encoding="UTF-8"?>
<GenericConnectorDescriptor
   onPremiseBrowseOnly="false"
    dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ"
    requireConnectionForBrowse="true">
	<description>The Okta Connector provides access to the Okta REST API for managing Okta objects. 

Many Okta objects support pagination and those will be classified as Query operations. 

Some Get APIs return an array of objects but do not support pagination. Those will be treated as Get operations and the results can be split into separate documents using a Data Process shape if required.

For more information refer to: https://developer.okta.com/docs/reference/api/roles/ 
	</description>
	<field id="URL" label="URL" type="string">
		<helpText>The URL for the Service service</helpText>
		<defaultValue/>
	</field>
	<field id="AUTHTYPE" label="AuthenticationType" type="string">
		<helpText>Choose the type of Authentication to use. For more information refer to: https://developer.okta.com/docs/guides/create-an-api-token/-/overview/
		</helpText>
		<defaultValue>CUSTOM</defaultValue>
		<allowedValue label="Okta API Token (SSWS)">
			<value>CUSTOM</value>
		</allowedValue>
		<allowedValue label="OAuth 2.0 Authorization Code">
			<value>OAUTH</value>
		</allowedValue>
	</field>
	<field id="PASSWORD" label="Okta API Token (SSWS)" type="password">
		<helpText>Enter the Okta API Token in the form SSWS 00QCjAl4MlV-WPXM...0HmjFx-vbGua</helpText>
		<defaultValue/>
	</field>
	<field id="OAUTHOPTIONS" label="OAuth 2.0 Authorization Code" type="oauth">
		<helpText>OAuth 2.0 Authorization Code authentication is available for select end points with specific scope values configured. For more information refer to: https://developer.okta.com/docs/guides/implement-oauth-for-okta/create-oauth-app/</helpText>
		<oauth2FieldConfig>
			<authorizationTokenEndpoint><url/></authorizationTokenEndpoint>
			<authorizationParameters/>
			<accessTokenEndpoint>
				<url/>
			</accessTokenEndpoint>
			<scope>
				<defaultValue>okta.apps.manage,okta.authorizationServers.manage,okta.clients.manage,okta.groups.manage,okta.roles.manage,okta.schemas.manage,okta.sessions.manage,okta.users.manage,okta.policies.manage</defaultValue>
			</scope>
			<grantType access="hidden"> 
				<defaultValue>code</defaultValue>
			</grantType>
		</oauth2FieldConfig>
	</field>
	<testConnection method="CUSTOM"/>


	<operation types="EXECUTE" customTypeId="CREATE" customTypeLabel="CREATE/EXECUTE"/>
	<operation types="EXECUTE" customTypeId="UPDATE" customTypeLabel="UPDATE"/>

	<!-- Get must be custom if a restful URL can have more than one ID -->
	<operation types="EXECUTE" customTypeId="GET" customTypeLabel="GET">
		<field id="EXTRA_URL_PARAMETERS" label="Extra URL Query Parameters" type="string" scope="operationOnly">
			<helpText>Extra URL Query Parameters are appended to the http URL used to execute an API transaction. This can be used for entering endpoint specific parameters such as 'expand=' or 'q='. Do not include a leading ? nor &amp;. For example: extra1=value1&amp;extra2=&amp;value2</helpText>
		</field>
	</operation>

	<!-- Delete must be custom if a restful URL can have more than one ID -->
	<operation types="EXECUTE" customTypeId="DELETE" customTypeLabel="DELETE"/>

	<!-- Query only works if there are no IDs in the restful path because currently Query Operation Shapes will no accept input parameters -->
	<!-- For example /invoices/{invoice id}/lineitems is a valid scenerio for query but it can not be implemented today with the SDK, a custom EXECUTE/GET must be used -->
	<operation types="QUERY" allowFieldSelection="false" fieldSelectionLevels="0" fieldSelectionNone="true">
		<field id="PAGESIZE" label="Page Size" type="integer" scope="operationOnly">
			<helpText>Specifies the number of documents to retrieve with each page transaction.</helpText>
			<defaultValue>20</defaultValue>
		</field>
		<field id="MAXDOCUMENTS" label="Maximum Documents" type="integer" scope="operationOnly">
			<helpText>Limits the number of documents returned. If value is less than 1, all records are returned.</helpText>
			<defaultValue>-1</defaultValue>
		</field>
		<field id="EXTRA_URL_PARAMETERS" label="Extra URL Query Parameters" type="string" scope="operationOnly">
			<helpText>Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &amp;. For example: extra1=value1&amp;extra2=&amp;value2</helpText>
		</field>
		<queryFilter grouping="singleNestingImplicitAnd" sorting="one">
			<operator id="eq" label="Equal To"/>  
			<operator id="ge" label="Greater Than or Equal"/>
			<operator id="gt" label="Greater Than"/>
			<operator id="lt" label="Less Than"/>  
			<operator id="le" label="Less Than or Equal"/>
			<operator id="ne" label="Not Equal To"/>  
			<operator id="pr" label="Has Value"/>  
			<operator id="sw" label="Starts With"/>  
			<sortOrder id="ASC" label="Ascending"/>
			<sortOrder id="DESC" label="Descending"/>
		</queryFilter>
	</operation>
	<!-- Inbound Dynamic Document Properties -->
	<dynamicProperty id="EXTRA_URL_PARAMETERS" label="Extra URL Query Parameters (Get,Query)" type="string">
		<helpText>Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &amp;. For example: extra1=value1&amp;extra2=&amp;value2</helpText>
	</dynamicProperty>
</GenericConnectorDescriptor>
