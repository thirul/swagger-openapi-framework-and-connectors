package com.boomi.connector.dataworld;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class DataWorldConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new DataWorldBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new DataWorldQueryOperation(createConnection(context));
    }
    protected DataWorldConnection createConnection(BrowseContext context) {
        return new DataWorldConnection(context);
    }
}