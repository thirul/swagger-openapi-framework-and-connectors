package com.boomi.connector.microstrategy;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class MicrostrategyConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new MicrostrategyBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new MicrostrategyQueryOperation(createConnection(context));
    }
    protected MicrostrategyConnection createConnection(BrowseContext context) {
        return new MicrostrategyConnection(context);
    }
}