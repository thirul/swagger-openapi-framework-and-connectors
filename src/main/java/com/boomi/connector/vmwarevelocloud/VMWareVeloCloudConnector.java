package com.boomi.connector.vmwarevelocloud;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class VMWareVeloCloudConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new VMWareVeloCloudBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new VMWareVeloCloudQueryOperation(createConnection(context));
    }
    protected VMWareVeloCloudConnection createConnection(BrowseContext context) {
        return new VMWareVeloCloudConnection(context);
    }
}