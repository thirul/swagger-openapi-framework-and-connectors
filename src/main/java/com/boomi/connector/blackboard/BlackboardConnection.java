package com.boomi.connector.blackboard;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;

public class BlackboardConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public BlackboardConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/results/*";
    }

    @Override
    protected AuthType getAuthenticationType()
    {
       	return AuthType.OAUTH;
    }
}