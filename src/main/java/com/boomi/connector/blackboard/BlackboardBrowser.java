package com.boomi.connector.blackboard;

import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.JSONUtil;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;

public class BlackboardBrowser extends SwaggerBrowser  implements ConnectionTester {
    public BlackboardBrowser(SwaggerConnection conn) {
        super(conn);
    }
	
	private FieldSpecField getFieldSpecByName(String name, List<FieldSpecField> fieldSpecs)
	{
		for (FieldSpecField fieldSpec : fieldSpecs)
		{
			if (fieldSpec.getName().contentEquals(name))
				return fieldSpec;
		}
		return null;
	}
	
	//TODO can the parent class merge the filter/sort/select so we don't have to do the getfieldspec by name merging??
	//TODO pass in schema and path instead of swaggerservice
	@Override
	protected void getFilterSortSpecs(SwaggerAPIService swaggerService, List<FieldSpecField> fields)
	{
		JSONObject queryRules = JSONUtil.getJSONFromResource(this.getClass(), "resources/blackboard/bbqueryrules.json");
		if (queryRules!=null && queryRules.has(swaggerService.getPathURL()))
		{
			JSONObject rules = queryRules.getJSONObject(swaggerService.getPathURL());
			//FieldSpecFields based on bbqueryrules.json
			//Filters First
			if (rules.has("filters"))
			{
				JSONArray filters = rules.getJSONArray("filters");
				for (int i=0; filters!=null && i<filters.length(); i++)
				{
					String name=filters.getJSONObject(i).getString("name");
					String type=filters.getJSONObject(i).getString("type");
					FieldSpecField filterable = getFieldSpecByName(name, fields);
					if (filterable==null)
					{
						filterable = new FieldSpecField().withName(name).withFilterable(true).withSortable(false).withType(type);				
						fields.add(filterable);						
					} else {
						filterable.withFilterable(true);
					}
				}
			}
			
			//Sort Next
			if (rules.has("filters"))
			{
				String sort=rules.getString("sort");
				if (sort!=null)
				{
					String sortFields[] = sort.split(",");
					for (int i=0; sortFields!=null && i<sortFields.length; i++)
					{
						String name = sortFields[i].trim();
						FieldSpecField sortable = getFieldSpecByName(name, fields);
						if (sortable==null)
						{
							sortable = new FieldSpecField().withName(name).withFilterable(false).withSortable(true).withType("string");				
							fields.add(sortable);
						} else {
							sortable.withSortable(true);
						}
					}						
				}
			}
		}
		//Selectable Last
		//TODO make this behavior the default behavior to make top level items selectable?
		//Add all the top level schema items as selectable fields
		JSONObject schema = swaggerService.getQueryPaginationResponseBodySchema(getConnection().getQueryPaginationSplitPath());
		JSONObject childschema=null;
		Iterator<?> it = schema.keys();
		while(it.hasNext())
		{
			String key = (String)it.next();
			if (!key.contentEquals("$schema"))
			{
				childschema = schema.getJSONObject(key);
				break;
			}
		}
		if (childschema!=null)
		{
			if (childschema.has("properties"))
			{
				JSONObject properties = childschema.getJSONObject("properties");
				Iterator<?> propIt = properties.keys();
				while(propIt.hasNext())
				{
					String key = (String)propIt.next();
					FieldSpecField selectable = getFieldSpecByName(key, fields);
					if (selectable==null)
					{
						selectable = new FieldSpecField().withName(key).withFilterable(false).withSortable(false).withSelectable(true).withType("string");				
						fields.add(selectable);
					} else {
						selectable.withSortable(true);
					}
				}
			}
		}
	}
	
	@Override
	//Check for pagination query parameters in the request schema. This allows for query operations that end with a path parameter
	protected boolean isQueryOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
	{
		if (!"get".contentEquals(methodName) || queryParameters==null)
			return false;
		for (int i=0; i<queryParameters.length(); i++)
		{	JSONObject parameter = (JSONObject)queryParameters.get(i);
			if (parameter.has("$ref"))
				if (parameter.getString("$ref").indexOf("RowBasedPagingParams.offset")>-1)
					return true;
		}
		return false;
	}

	@Override
    public BlackboardConnection getConnection() {
        return (BlackboardConnection) super.getConnection();
    }
}