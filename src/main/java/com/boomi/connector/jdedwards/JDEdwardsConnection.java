package com.boomi.connector.jdedwards;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class JDEdwardsConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public JDEdwardsConnection(BrowseContext context) {
		super(context);
	}
	
	@Override
	protected String getSwaggerUrl()
	{
		return "resources/jdedwards/swagger.json";
	}
	
    public String getQueryPaginationSplitPath()
    {
    	return "properties/items/*";
    }
}