package com.boomi.connector.jdedwards;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class JDEdwardsConnector extends SwaggerConnector {
   
    protected JDEdwardsConnection createConnection(BrowseContext context) {
        return new JDEdwardsConnection(context);
    }
}