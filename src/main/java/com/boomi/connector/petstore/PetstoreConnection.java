package com.boomi.connector.petstore;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;

public class PetstoreConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public PetstoreConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }
    
    @Override
    public List<String> getExcludedObjectTypeIDs()
    {
    	return Arrays.asList("/pet/{petId}___post", "/user/login___get", "/user/logout___get");
    }
    
    @Override
    protected String getSwaggerUrl()
    {
    	return "https://petstore.swagger.io/v2/swagger.json";
    }    
    	
    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.NONE;
    }
    
    @Override
    protected String getCustomAuthHeader() 
    {
    	return null;
    }
}