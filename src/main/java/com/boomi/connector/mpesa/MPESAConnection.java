package com.boomi.connector.mpesa;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class MPESAConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public MPESAConnection(BrowseContext context) {
		super(context);
	}
	
    public String getQueryPaginationSplitPath()
    {
    	return "/items/*";
    }
}