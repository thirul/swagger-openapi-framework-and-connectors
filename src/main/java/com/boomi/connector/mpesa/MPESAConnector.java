package com.boomi.connector.mpesa;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class MPESAConnector extends SwaggerConnector {
   
    protected MPESAConnection createConnection(BrowseContext context) {
        return new MPESAConnection(context);
    }
}