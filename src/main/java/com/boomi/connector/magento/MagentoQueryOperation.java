package com.boomi.connector.magento;

import java.util.ArrayList;
import java.util.List;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.util.CollectionUtil;

public class MagentoQueryOperation extends SwaggerQueryOperation {
//	private static final String PAGINATION_EXPAND_URIPARAM = "expand"; //TODO
	
	protected MagentoQueryOperation(SwaggerConnection conn) {
		super(conn);
	}    	

	@Override
	protected String getNextPageQueryParameterName()
	{
		return "searchCriteria[currentPage]";
	}

	@Override
	protected String getHasMoreElementPath() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	protected String getNextPageElementPath()
	{
		return null;
	}
	
	@Override
	protected String getPageSizeQueryParameterName()
	{
		return "searchCriteria[pageSize]";
	}
	
    //See https://devdocs.magento.com/guides/v2.4/rest/performing-searches.html
	private static final String PAGINATION_SORT_DIRECTION_URIPARAM = "searchCriteria[sortOrders][%d][direction]=%s";//=ASC | DESC";
	private static final String PAGINATION_SORT_URIPARAM = "searchCriteria[sortOrders][%d][field]=%s"; //=<field-name> ";
    @Override
    protected String getSortTermsQueryParam(List<Sort> sortTerms)
    {
    	//sort=name.family(desc),created
    	String sortTermsString="";

    	if (sortTerms!=null)
    	{		            
        	for (int x=0; x<sortTerms.size(); x++)
        	{
        		Sort sort = (Sort)sortTerms.get(x);
        		String sortTerm = sort.getProperty();
        		if (sortTerm != null && sortTerm.length()>0)
        		{
        			if (sortTermsString.length()>0)
        			{
        				sortTermsString+="&";
        			}
        			sortTermsString += String.format(PAGINATION_SORT_URIPARAM,x,sortTerm);
        			if (sort.getSortOrder().contentEquals("DESC"))
        				sortTermsString+=String.format(PAGINATION_SORT_DIRECTION_URIPARAM,x,sort.getSortOrder());
        		}
        	}
    	}
    	return sortTermsString;
    }
    	
	@Override
	protected String buildSimpleExpression(SimpleExpression expr) {
        // this is the name of the queried object's property
    	String term="";
    	List<String> comparableDateFields = new ArrayList<String>();
    	comparableDateFields.add("created");
    	comparableDateFields.add("modified");
    	comparableDateFields.add("lastAccessed");
        String propName = expr.getProperty();
        String operator = expr.getOperator();
        
        if (propName==null || propName.length()==0)
        	throw new ConnectorException("Filter field parameter required");

        // we only support 1 argument operations
        if(CollectionUtil.size(expr.getArguments()) != 1) 
            throw new IllegalStateException("Unexpected number of arguments for operation " + expr.getOperator() + "; found " +
                                            CollectionUtil.size(expr.getArguments()) + ", expected 1");

        // this is the single operation argument
       	String parameter=expr.getArguments().get(0);
        if (parameter==null || parameter.length()==0)
        	throw new ConnectorException(String.format("Filter parameter is required for field: %s ", propName));

        switch (operator)
        {
        case "eq":
        	term = propName + "=" + parameter;
        	break;
        case "lessThan":
        case "greaterOrEqual":
        	if (!comparableDateFields.contains(propName))
        		throw new ConnectorException(String.format("Operation %s not supported for field: % ", operator, propName));
        	term = propName + "=" + parameter + "&" + propName + "Compare" + "=" + operator;
        	break;
        default:
        	throw new ConnectorException(String.format("Unknown filter operator %s for field: %s", operator, propName));
        }

        return term;
    }

	@Override
	protected PaginationType getPaginationType() {
		return PaginationType.PAGINATION_TYPE_PAGE_OFFSET;
	}

}