package com.boomi.connector.magento;

import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.JSONUtil;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;

public class MagentoBrowser extends SwaggerBrowser  implements ConnectionTester {
    public MagentoBrowser(SwaggerConnection conn) {
        super(conn);
    }
	
	@Override
	//Check for pagination query parameters in the request schema. This allows for query operations that end with a path parameter
	protected boolean isQueryOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
	{
		if (!"get".contentEquals(methodName) || queryParameters==null)
			return false;
		for (int i=0; i<queryParameters.length(); i++)
		{	JSONObject parameter = (JSONObject)queryParameters.get(i);
			if (parameter.has("in"))
				if (parameter.getString("in").contentEquals("query"))
					if (parameter.has("name"))
						if (parameter.getString("name").contentEquals("searchCriteria[pageSize]"))
							return true;
		}
		return false;
	}

	@Override
    public MagentoConnection getConnection() {
        return (MagentoConnection) super.getConnection();
    }
}