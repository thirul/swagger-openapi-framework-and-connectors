package com.boomi.connector.magento;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class MagentoConnector extends SwaggerConnector {
   
    protected MagentoConnection createConnection(BrowseContext context) {
        return new MagentoConnection(context);
    }
    
	@Override
    public Browser createBrowser(BrowseContext context) {
		return new MagentoBrowser(createConnection(context));
    }    

    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new MagentoQueryOperation(createConnection(context));
    }
}