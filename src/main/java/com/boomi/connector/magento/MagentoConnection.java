package com.boomi.connector.magento;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.PropertyMap;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.AuthType;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;

public class MagentoConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    public enum CustomAuthProperties {CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, TOKEN_SECRET}
    private static final String HASH_ALGORITHM = "SHA-256";
    private static final int NONCE_LENGTH = 10;
    private static final String ALPHA_NUMERIC_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    
	public MagentoConnection(BrowseContext context) {
		super(context);
	}
	
    public String getQueryPaginationSplitPath()
    {
    	return "/items/*";
    }
    
    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.CUSTOM;
    }
    
    @Override
	protected InputStream insertCustomHeaders(Map<String, String> headers, InputStream data)
    {
    	PropertyMap connProps = this.getConnectionProperties();

    	String accessToken = connProps.getProperty(CustomAuthProperties.ACCESS_TOKEN.name());
    	if (StringUtil.isBlank(accessToken))
    		throw new ConnectorException("Access Token is required");
    	
    	String consumerSecret = connProps.getProperty(CustomAuthProperties.CONSUMER_SECRET.name());
    	if (StringUtil.isBlank(consumerSecret))
    		throw new ConnectorException("Consumer Secret is required");
    	
    	String consumerKey = connProps.getProperty(CustomAuthProperties.CONSUMER_KEY.name());
    	if (StringUtil.isBlank(consumerKey))
    		throw new ConnectorException("Consumer Key is required");
    	
    	String tokenSecret = connProps.getProperty(CustomAuthProperties.TOKEN_SECRET.name());
    	if (StringUtil.isBlank(tokenSecret))
    		throw new ConnectorException("Token Secret is required");
    	
    	String timeStamp = getTimestamp();
		String nonce = null;
		String signature = null;
		
		String header = String.format("OAuth oauth_consumer_key=\"%S\", oauth_token=\"%s\", oauth_signature_method=\"HMAC-SHA256\", oauth_timestamp=\"%s\", oauth_nonce=\"%s\", oauth_version=\"1.0\", oauth_signature=\"%s\"",
    			consumerKey, accessToken, timeStamp, nonce, signature);
    	return data;
    }    
    
    /**
     * Returns UNIX Timestamp as required per
     * https://tools.ietf.org/html/rfc5849#section-3.3
     *
     * @return UNIX timestamp (UTC)
     */
    private static String getTimestamp() {
      return Long.toString(System.currentTimeMillis() / 1000L);
    }
    
    /**
     * Generates a random string for replay protection as per
     * https://tools.ietf.org/html/rfc5849#section-3.3
     *
     * @return random string of 16 characters.
     */
    static String getNonce() {
      SecureRandom rnd = new SecureRandom();
      StringBuilder sb = new StringBuilder(NONCE_LENGTH);
      for (int i = 0; i < NONCE_LENGTH; i++) {
        sb.append(ALPHA_NUMERIC_CHARS.charAt(rnd.nextInt(ALPHA_NUMERIC_CHARS.length())));
      }
      return sb.toString();
    }

}