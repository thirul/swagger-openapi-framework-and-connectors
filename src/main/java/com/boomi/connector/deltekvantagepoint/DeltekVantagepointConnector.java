package com.boomi.connector.deltekvantagepoint;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class DeltekVantagepointConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new DeltekVantagepointBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new DeltekVantagepointQueryOperation(createConnection(context));
    }
    protected DeltekVantagepointConnection createConnection(BrowseContext context) {
        return new DeltekVantagepointConnection(context);
    }
}