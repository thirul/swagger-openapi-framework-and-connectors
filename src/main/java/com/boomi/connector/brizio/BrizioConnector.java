package com.boomi.connector.brizio;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class BrizioConnector extends SwaggerConnector {
   
    protected BrizioConnection createConnection(BrowseContext context) {
        return new BrizioConnection(context);
    }
}