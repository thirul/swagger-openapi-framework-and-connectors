package com.boomi.connector.facebook_marketing;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.sap_concur.SAPConcurBrowser;
import com.boomi.connector.sap_concur.SAPConcurQueryOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class FacebookMarketingConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new FacebookMarketingBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new FacebookMarketingQueryOperation(createConnection(context));
    }
    protected FacebookMarketingConnection createConnection(BrowseContext context) {
        return new FacebookMarketingConnection(context);
    }
}