package com.boomi.connector.cybersource;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class CybersourceConnector extends SwaggerConnector {
   
    protected CybersourceConnection createConnection(BrowseContext context) {
        return new CybersourceConnection(context);
    }
}