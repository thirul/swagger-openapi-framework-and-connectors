package com.boomi.connector.cybersource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.PropertyMap;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerUtil;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.HttpRequest;
import org.apache.http.client.methods.HttpRequestBase;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

//Overrides doGet and Send to implement HTTP Signature Authentication
//https://developer.cybersource.com/api/developer-guides/dita-gettingstarted/authentication/GenerateHeader/httpSignatureAuthentication.html
public class CybersourceConnection extends SwaggerConnection {

	public CybersourceConnection(BrowseContext context) {
		super(context);
	}

	public enum CyberSourceConnectionProperties {MERCHANTID, KEYID, KEYVALUE}
    
    String _host;
	private static final String GET_HEADER = "host (request-target) v-c-merchant-id";
	private static final String POST_HEADER = "host (request-target) digest v-c-merchant-id";
	private static final String algorithm="HmacSHA256";
    
	//String uri="/tss/v2/transactions/5756530710636693503006";
    //path must be the full path excluding only the domain name
    //For post, we need to read the stream...so we rebuilt it and return it
    //For get we just return the stream as is
	//TODO the swagger we have for transactions has a bad basePath(/TCS227/CBSE/1.0.0) so it was removed in file by hand.
    @Override
	protected InputStream insertCustomHeaders(Map<String,String> headers, InputStream data)
	{
    	URL url=null;
    	try {
			url = new URL(this.getUrl());
		} catch (MalformedURLException e1) {
			throw new ConnectorException(e1);
		}
		Logger logger = this.getLogger();
		PropertyMap connProps = this.getConnectionProperties();
		String method=this.getHttpMethod().toLowerCase();
		
		String host = url.getHost();
		String path = url.getPath();
		
		String merchantId = connProps.getProperty("MERCHANTID");
		if (merchantId == null || merchantId.trim().length()==0)
			throw new ConnectorException("A Merchant ID must be provided");
		
		String keyId = connProps.getProperty("KEYID");
		if (keyId == null || keyId.trim().length()==0)
			throw new ConnectorException("A Key ID must be provided");
		
		String keyValue = connProps.getProperty("KEYVALUE");
		if (keyValue == null || keyValue.trim().length()==0)
			throw new ConnectorException("A Key Value must be provided");
		
		String digest=null;
		String signatureParams;
		String header;
		if (method.contentEquals("post"))
		{
		    MessageDigest md;
			try {
				md = MessageDigest.getInstance("SHA-256");
			} catch (NoSuchAlgorithmException e) {
				throw new ConnectorException (e);
			}
			String body;
			try {
				body = SwaggerUtil.inputStreamToString(data);
			} catch (IOException e) {
				throw new ConnectorException(e);
			}
		    md.update(body.getBytes(StandardCharsets.UTF_8));
		    data = new ByteArrayInputStream(body.getBytes());
		    byte[] digestBytes = md.digest();
		    digest = "SHA-256=" + Base64.getEncoder().encodeToString(digestBytes);
			signatureParams=String.format("host: %s\n(request-target): %s %s\ndigest: %s\nv-c-merchant-id: %s", host, method, path, digest, merchantId);
			header = POST_HEADER;
		} else {
			signatureParams=String.format("host: %s\n(request-target): %s %s\nv-c-merchant-id: %s", host, method, path, merchantId);
			header = GET_HEADER;
		}
					
		byte[] decodedKey = Base64.getDecoder().decode(keyValue);
		SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, algorithm);
		Mac hmacSha256;
		
		try {
			hmacSha256 = Mac.getInstance(algorithm);
			hmacSha256.init(originalKey);
		} catch (NoSuchAlgorithmException | InvalidKeyException e) {
			throw new ConnectorException(e);
		}
		
		hmacSha256.update(signatureParams.getBytes());
		byte[] HmachSha256DigestBytes = hmacSha256.doFinal();
		String signature = Base64.getEncoder().encodeToString(HmachSha256DigestBytes);
		String Signature = String.format("keyid=\"%s\", algorithm=\"%s\", headers=\"%s\", signature=\"%s\"", keyId, algorithm, header, signature);
		//Cybersource fails if there are any extra headers
		//TODO We should remove all of them!
//		httpRequest.removeHeaders("Accept");
		headers.put("Host", host);
		String date = (new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")).format(new Date());
		headers.put("Date", date);
		if (digest!=null)
			headers.put("Digest", digest);
		headers.put("v-c-merchant-id", merchantId);
		headers.put("Signature", Signature);
		
		logger.info("Host: \n" + host);
		logger.info("Date: \n" + date);
		logger.info("Digest: \n" + digest);
		logger.info("v-c-merchant-id: \n" + merchantId);
		logger.info("Signature: \n" + Signature);
		logger.info("signatureParams: \n" + signatureParams);		
		return data;
	}

	@Override
	public String getQueryPaginationSplitPath() {
		// TODO Auto-generated method stub
		return null;
	}	
	
	@Override
	protected String getResponseContentType()
	{
		return null;
	}
}