package com.boomi.connector.stripe;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;

public class StripeBrowser extends SwaggerBrowser  implements ConnectionTester {
    public StripeBrowser(SwaggerConnection conn) {
        super(conn);
    }

	//Check for pagination query parameters in the request schema. This allows for query operations that end with a path parameter
	@Override
	protected boolean isQueryOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
	{
		if (!"get".contentEquals(methodName) || queryParameters==null)
			return false;
		for (int i=0; i<queryParameters.length(); i++)
		{	JSONObject parameter = (JSONObject)queryParameters.get(i);
			if (parameter.has("in"))
				if (parameter.getString("in").contentEquals("query"))
					if (parameter.has("name"))
						if (parameter.getString("name").contentEquals("starting_after"))
							return true;
		}
		return false;
	}

	@Override
    public StripeConnection getConnection() {
        return (StripeConnection) super.getConnection();
    }
	
	@Override
	protected void getFilterSortSpecs(SwaggerAPIService swaggerService, List<FieldSpecField> fields) {
		JSONArray queryParams = swaggerService.getQueryParameters();
		for (int i=0; i<queryParams.length(); i++)
		{
			JSONObject param = queryParams.getJSONObject(i);
			if (param.has("name"))
			{
				String name = param.getString("name");
				//TODO need to use schema/type to get integer/epoch datetime type
				String type = "string";
				switch (name)
				{
				case "ending_before":
				case "expand":
				case "limit":
				case "starting_after":
					break;
//created, canceled_at, completed_at, released_at, current_period_end, current_period_start, due_date, status_transitions, available_on, fulfilled, paid, returned, arrival_date
//amount is only integer that allows < >
				default:
					if (param.has("schema"))
					{
						JSONObject schemaObject=param.getJSONObject("schema");
						if (schemaObject.has("anyOf"))
						{
							JSONArray typeArray = schemaObject.getJSONArray("anyOf");
							for (int j=0; j<typeArray.length(); j++){
								if (typeArray.getJSONObject(j).has("title"))
									if ("range_query_specs".contentEquals(typeArray.getJSONObject(j).getString("title")))
									{
										type="number";
										break;
									}											
							}
						}
					}
					FieldSpecField filterable = new FieldSpecField().withName(name).withFilterable(true).withSortable(false).withSelectable(false).withType(type);				
					fields.add(filterable);						
				}
			}
		}
	}
}