package com.boomi.connector.stripe;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class StripeConnector extends SwaggerConnector {
       
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new StripeBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new StripeQueryOperation(createConnection(context));
    }

    protected StripeConnection createConnection(BrowseContext context) {
        return new StripeConnection(context);
    }
}