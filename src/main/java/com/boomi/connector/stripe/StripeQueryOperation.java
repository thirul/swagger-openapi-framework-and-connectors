package com.boomi.connector.stripe;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.json.JSONObject;
import org.json.JSONTokener;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.util.CollectionUtil;

public class StripeQueryOperation extends SwaggerQueryOperation {

	protected StripeQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getNextPageQueryParameterName()
	{
		return "starting_after";
	}

	@Override
	protected String getNextPageElementPath()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getHasMoreElementPath() {
		return "/has_more";
	}

	//TODO move error checking to the super class?
	@Override
	protected String buildSimpleExpression(SimpleExpression expr) {
        // this is the name of the queried object's property
    	String term="";
        String propName = expr.getProperty();
        String operator = expr.getOperator();
        
        if (propName==null || propName.length()==0)
        	throw new ConnectorException("Filter field parameter required");
        // we only support 1 argument operations
        if(CollectionUtil.size(expr.getArguments()) != 1) 
            throw new IllegalStateException("Unexpected number of arguments for operation " + expr.getOperator() + "; found " +
                                            CollectionUtil.size(expr.getArguments()) + ", expected 1");

        // this is the single operation argument
       	String parameter=expr.getArguments().get(0);
        if (parameter==null || parameter.length()==0)
        	throw new ConnectorException(String.format("Filter parameter is required for field: %s ", propName));

        switch (operator)
        {
        case "eq":
        	term = propName + "=" + parameter;
        	break;
        case "lt":
        case "gt":
        case "lte":
        case "gte":
        	//Convert parameter from date to epoch
        	String format = "yyyy-MM-dd'T'HH:mm:ssZ";
        	SimpleDateFormat df = new SimpleDateFormat(format);
        	Date date;
			try {
				if (!"amount".contentEquals(propName)) //if not amount assume date
				{
					date = df.parse(parameter);
		        	long epoch = date.getTime()/1000; //Concert from ms to sec
		        	parameter = ""+epoch;
				}
	        	term = propName + "%5B" + operator + "%5D=" + parameter; //Example: created.gt = <yesterday as epoch integer
			} catch (ParseException e) {
				throw new ConnectorException("Bad date format, expecting: " + format);
			}
        	break;
        default:
        	throw new ConnectorException(String.format("Unknown filter operator %s for field: %s", operator, propName));
        }

        return term;
    }

	@Override
	protected PaginationType getPaginationType() {
		return PaginationType.PAGINATION_TYPE_LAST_ID;
	}
}