package com.boomi.connector.salesforcemarketingcloud;

import java.util.List;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;

public class SalesforceMarketingCloudQueryOperation extends SwaggerQueryOperation {

	protected SalesforceMarketingCloudQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PaginationType getPaginationType() {
		// TODO Auto-generated method stub
		return PaginationType.PAGINATION_TYPE_PAGE_OFFSET;
	}
	
	@Override
	protected String getNextPageQueryParameterName() {
		return "$page";
	}
	
	@Override
	protected String getPageSizeQueryParameterName()
	{
		return "$pageSize";
	}

}