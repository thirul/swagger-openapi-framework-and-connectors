package com.boomi.connector.workday;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.sap_concur.SAPConcurBrowser;
import com.boomi.connector.sap_concur.SAPConcurQueryOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class WorkdayConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new WorkdayBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new WorkdayQueryOperation(createConnection(context));
    }
    protected WorkdayConnection createConnection(BrowseContext context) {
        return new WorkdayConnection(context);
    }
}