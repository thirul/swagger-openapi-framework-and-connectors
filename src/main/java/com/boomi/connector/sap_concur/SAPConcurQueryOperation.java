package com.boomi.connector.sap_concur;

import java.util.List;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;

public class SAPConcurQueryOperation extends SwaggerQueryOperation {

	protected SAPConcurQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getNextPageElementPath()
	{
		return "/NextPage";
	}

	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getHasMoreElementPath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PaginationType getPaginationType() {
		return PaginationType.PAGINATION_TYPE_URL_FIELD;
	}
}