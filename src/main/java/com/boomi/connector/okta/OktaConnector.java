package com.boomi.connector.okta;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class OktaConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
		return new OktaBrowser(createConnection(context));
    }    

    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new OktaQueryOperation(createConnection(context));
    }

    protected OktaConnection createConnection(BrowseContext context) {
        return new OktaConnection(context);
    }
}