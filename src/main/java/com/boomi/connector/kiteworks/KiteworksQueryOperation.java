package com.boomi.connector.kiteworks;

import java.util.List;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;

public class KiteworksQueryOperation extends SwaggerQueryOperation {

	protected KiteworksQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getNextPageElementPath()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
    	String sortTermsString="";

    	if (sortTerms!=null)
    	{		            
        	for (int x=0; x<sortTerms.size(); x++)
        	{
        		Sort sort = (Sort)sortTerms.get(x);
        		String sortTerm = sort.getProperty();
        		if (sortTerm != null && sortTerm.length()>0)
        		{
        			if (sortTermsString.length()==0)
        			{
        				sortTermsString+="orderBy=";
        			} else {
        				sortTermsString+=",";
        			}
        		}
        	}
    	}
    	return sortTermsString;
	}

	@Override
	protected PaginationType getPaginationType() {
		return PaginationType.PAGINATION_TYPE_RECORD_OFFSET;
	}
}