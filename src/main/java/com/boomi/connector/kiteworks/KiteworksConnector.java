package com.boomi.connector.kiteworks;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class KiteworksConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new KiteworksBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new KiteworksQueryOperation(createConnection(context));
    }
    protected KiteworksConnection createConnection(BrowseContext context) {
        return new KiteworksConnection(context);
    }
}