package com.boomi.connector.sage_people;

import java.util.List;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;

//https://developer.sage.com/people/guides/filtering
//TODO we need to implement filtering of nested fields as well as nested expression grouping
public class SageQueryOperation extends SwaggerQueryOperation {

	protected SageQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getNextPageQueryParameterName()
	{
		return "marker";
	}

	@Override
	protected String getNextPageElementPath()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getHasMoreElementPath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PaginationType getPaginationType() {
		return PaginationType.PAGINATION_TYPE_LAST_ID;
	}
}