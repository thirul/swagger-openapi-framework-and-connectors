package com.boomi.connector.sage_people;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.sap_concur.SAPConcurBrowser;
import com.boomi.connector.sap_concur.SAPConcurQueryOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class SageConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new SageBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new SageQueryOperation(createConnection(context));
    }
    protected SageConnection createConnection(BrowseContext context) {
        return new SageConnection(context);
    }
}