package com.boomi.connector.boomi_flow;

import java.util.List;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;

public class BoomiFlowQueryOperation extends SwaggerQueryOperation {

	protected BoomiFlowQueryOperation(SwaggerConnection conn) {
		super(conn);
	}
	
	@Override
	protected String getNextPageElementPath()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	protected String getSelectTermsQueryParam(List<String> selectedFields) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getSortTermsQueryParam(List<Sort> sortTerms) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getHasMoreElementPath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PaginationType getPaginationType() {
		return PaginationType.PAGINATION_TYPE_OTHER;
	}
}