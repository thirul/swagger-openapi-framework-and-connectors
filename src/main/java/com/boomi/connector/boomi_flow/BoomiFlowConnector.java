package com.boomi.connector.boomi_flow;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.sap_concur.SAPConcurBrowser;
import com.boomi.connector.sap_concur.SAPConcurQueryOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class BoomiFlowConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new BoomiFlowBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new BoomiFlowQueryOperation(createConnection(context));
    }
    protected BoomiFlowConnection createConnection(BrowseContext context) {
        return new BoomiFlowConnection(context);
    }
}