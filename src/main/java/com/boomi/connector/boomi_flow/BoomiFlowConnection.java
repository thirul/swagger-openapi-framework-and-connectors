package com.boomi.connector.boomi_flow;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class BoomiFlowConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public BoomiFlowConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/items/*";
    }

    @Override
    protected String getSwaggerUrl()
    {
    	return "resources/boomi_flow/openapi.json";
    }    
}