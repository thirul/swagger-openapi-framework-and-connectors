package com.boomi.connector.aquarius;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class AquariusConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public AquariusConnection(BrowseContext context) {
		super(context);
	}
	
    public String getQueryPaginationSplitPath()
    {
    	return "items/*";
    }
}