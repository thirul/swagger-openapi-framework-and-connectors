package com.boomi.connector.awscloudformation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.client.methods.HttpRequestBase;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.PropertyMap;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerUtil;
import com.boomi.util.StringUtil;


public class AWSCloudFormationConnection extends SwaggerConnection {
    
    public enum AWSConnectionProperties {AWS_REGION, OTHER_AWS_REGION, ACCESS_KEY, SECRET_KEY}
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public AWSCloudFormationConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    protected String getSwaggerUrl()
    {
    	return "resources/awscloudformation/openapi.json";
    } 
    
    @Override 
    protected AuthType getAuthenticationType()
    {
    	return AuthType.NONE;
    }
    
    private String getRegion()
    {
    	String region = this.getConnectionProperties().getProperty(AWSConnectionProperties.OTHER_AWS_REGION.name());
    	if (StringUtil.isEmpty(region))
    		region = this.getConnectionProperties().getProperty(AWSConnectionProperties.AWS_REGION.name());
    	if (StringUtil.isEmpty(region))
    		throw new ConnectorException("AWS Region must be specified");
    	return region;
    }
    
	/**
	 * Returns the service url/host set by the user in the Connections page.
	 * @return the url for the service
	*/
    @Override
    protected String getBaseUrl()
    {
    	String region = getRegion();
    	return "https://cloudformation."+region+".amazonaws.com";  	
    }   

    /**
     * Execute the algorithm here: https://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html
     * @param httpRequest
     * @param data - passed in so it can be parsed for signature generation
     * @return
     */
    @Override
	protected InputStream insertCustomHeaders(Map<String,String> headers, InputStream data)
	{
		Logger logger = this.getLogger();
		String service="cloudformation";
	    String signingAlgorithm = "AWS4-HMAC-SHA256";
		String region = getRegion();
		String method = "POST";
		this.setHttpMethod(method);
		URL url=null;
		try {
			url = new URL(getUrl());
		} catch (MalformedURLException e2) {
			new ConnectorException(e2);
		}
		String host = url.getHost();
		String path = url.getPath();
		String canonicalQueryString = url.getQuery();
		
		logger.info("url " + url.toString());
		logger.info("path " + path);
		logger.info("Query String " + canonicalQueryString);
		PropertyMap connProps = this.getConnectionProperties();
		
		String accessKey = connProps.getProperty(AWSConnectionProperties.ACCESS_KEY.name());
		if (StringUtil.isBlank(accessKey))
			throw new ConnectorException("An Access Key must be provided");
		
		String secretKey = connProps.getProperty(AWSConnectionProperties.SECRET_KEY.name());
		if (StringUtil.isBlank(secretKey))
			throw new ConnectorException("A Secret Key must be provided");
	
		Date now = new Date();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
	    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	    String amzdate = (sdf.format(now)).toString();
	    sdf = new SimpleDateFormat("yyyyMMdd");
	    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		String datestamp = (sdf.format(now)).toString();
		
		logger.info("amzdate: " +amzdate);
		
//		String contentType = "application/x-www-form-urlencoded";

	    String canonicalURI = "/";
	    
	    String signedHeaders = null;
	    String contentType="application/x-www-form-urlencoded";
	    String canonicalHeaders;

String credentialScope = datestamp + "/" + region + "/" + service + "/aws4_request";
logger.info("\n\ncredentialScope:\n"+credentialScope);	    


	    //TODO get input stream into a string
		String body="";
		signedHeaders = "content-type;host;x-amz-date";//;amz-sdk-invocation-id;amz-sdk-request;content-length;content-type";
		canonicalHeaders = "content-type:"+contentType+"\nhost:" + host + '\n' + "x-amz-date:" + amzdate + '\n';
		body=canonicalQueryString;
		canonicalQueryString="";
		String newUrl=this.getUrl();
		newUrl="https://"+host;//+"?"+canonicalQueryString;
	    this.setUrl(newUrl);
		data = new ByteArrayInputStream(body.getBytes());
		logger.info("\n\ncanonicalHeaders:\n"+canonicalHeaders);	    

	    byte[] hashedPayloadBytes = null;
	    try {
			hashedPayloadBytes = HmacSHA256(body.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
			throw new ConnectorException(e1);
		} 
	    String hashedPayload = convertbyte(hashedPayloadBytes);

	    String canonicalRequest = method + "\n" + canonicalURI + "\n" + canonicalQueryString + "\n" + canonicalHeaders + "\n" + signedHeaders + "\n" + hashedPayload;
logger.info("\n\ncanonicalRequest:\n "+canonicalRequest);	    
	    // Calculate String to sign

	    byte[] hashedCanonicalRequestBytes;
		try {
			hashedCanonicalRequestBytes = HmacSHA256(canonicalRequest.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw new ConnectorException(e);
		}
	    String hashedCanonicalRequest = convertbyte(hashedCanonicalRequestBytes);

		String stringToSign = signingAlgorithm +"\n"+ amzdate + "\n" + credentialScope + "\n" + hashedCanonicalRequest;
logger.info("\n\nstringToSign:\n"+stringToSign);	    

	    // Create a signing key.
	    byte[] signingKey;
		try {
			signingKey = getSignatureKey(secretKey, datestamp, region, service);
		} catch (Exception e) {
			throw new ConnectorException(e);
		}
	    // Use the signing key to sign the stringToSign using HMAC-SHA256 signing algorithm.
	    byte[] signatureBytes;
		try {
			signatureBytes = HmacSHA256(stringToSign, signingKey);
		} catch (Exception e) {
			throw new ConnectorException(e);
		}
	    String signature = convertbyte(signatureBytes);
	    logger.info("\n\nsignature:\n"+signature);	    
		
	    //Set authorization
		
	    String authorizationHeader = signingAlgorithm + " Credential=" + accessKey+"/"+credentialScope+", SignedHeaders=" + signedHeaders+", Signature="+signature;
		headers.put("Authorization", authorizationHeader);
	    logger.info("\n\nauthorizationHeader:\n"+authorizationHeader);	    
//		    httpRequest.removeHeaders("Accept");
	    headers.put("Content-Type", contentType);
	    headers.put("Host", host);
	    headers.put("X-Amz-Date", amzdate);
		return data;
	}

    private static byte[] HmacSHA256(byte[] key) throws NoSuchAlgorithmException {
        MessageDigest mac = MessageDigest.getInstance("SHA-256");
        byte[] signatureBytes = mac.digest(key);
        return signatureBytes;
    }
    
    private static String convertbyte(byte[] bytes) {
        StringBuffer hexString = new StringBuffer();
        for (int j=0; j<bytes.length; j++) {
            String hex=Integer.toHexString(0xff & bytes[j]);
            if(hex.length()==1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
    
    private static byte[] HmacSHA256(String data, byte[] key) throws Exception {
        String algorithm="HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(data.getBytes("UTF-8"));
    }

    private static byte[] getSignatureKey(String key, String dateStamp, String regionName, String serviceName) throws Exception {
        byte[] kDate = HmacSHA256(dateStamp, ("AWS4" + key).getBytes("UTF-8"));
        byte[] kRegion = HmacSHA256(regionName, kDate);
        byte[] kService = HmacSHA256(serviceName, kRegion);
        byte[] kSigning = HmacSHA256("aws4_request", kService);
        return kSigning;
    }
 
}