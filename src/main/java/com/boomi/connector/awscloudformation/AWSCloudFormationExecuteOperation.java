package com.boomi.connector.awscloudformation;

import java.net.URLEncoder;
import java.util.Set;

import com.boomi.connector.api.DynamicPropertyMap;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.PropertyMap;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerExecuteOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.util.StringUtil;

public class AWSCloudFormationExecuteOperation extends SwaggerExecuteOperation {

	public AWSCloudFormationExecuteOperation(SwaggerConnection conn) {
		super(conn);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected String getPath(String objectTypeId, PropertyMap opProps, ObjectData input) {
		logger.info("getPath");
		String path = super.getPath(objectTypeId, opProps, input);
		String basePath = SwaggerBrowseUtil.getPath(objectTypeId);
		//Strip off the /#Action=XXXX since there is an ?Action=param&
		path=path.substring(basePath.length()+1);
		path="?"+path;
		//append all the template parameters
		StringBuilder sb = new StringBuilder(path);
		this.appendTemplateParameters(opProps, input.getDynamicOperationProperties(), sb);
		this.appendTemplateFileParameter(opProps, sb);
		return sb.toString();
	}
	
	private void appendTemplateParameters(PropertyMap opProps, DynamicPropertyMap dynamicProperties, StringBuilder path) {
		Set<String> keySet = opProps.keySet();
		int paramIndex=1;
		for (String key: keySet)
		{
			if (key.startsWith(AWSCloudFormationBrowser.TEMPLATE_QUERY_PARAMETER_FIELD_ID_PREFIX))
			{
				String value = dynamicProperties.getProperty(key,opProps.get(key).toString());
				if (StringUtil.isNotEmpty(value))
				{
					String name = key.substring(AWSCloudFormationBrowser.TEMPLATE_QUERY_PARAMETER_FIELD_ID_PREFIX.length());
					path.append("&Parameters.member."+paramIndex+".ParameterKey=");
					path.append(name);
					path.append("&Parameters.member."+paramIndex+".ParameterValue=");
					path.append(URLEncoder.encode(value));
					paramIndex++;
				}
			}
		}
	}

	private void appendTemplateFileParameter(PropertyMap opProps, StringBuilder path) {
		
		String templateBody=opProps.getProperty(AWSCloudFormationBrowser.CFOperationProperties.TEMPLATE_BODY.name());
		if (StringUtil.isNotBlank(templateBody))
		{
			path.append("&TemplateBody=");
			path.append(URLEncoder.encode(templateBody));
		} else {
			String templateUrl=opProps.getProperty(AWSCloudFormationBrowser.CFOperationProperties.TEMPLATE_URL.name());
			if (StringUtil.isNotBlank(templateUrl))
			{
				path.append("&TemplateURL=");
				path.append(URLEncoder.encode(templateUrl));
			}
		}
	}
}
