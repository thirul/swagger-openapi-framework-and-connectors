package com.boomi.connector.guidewire;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class GuidewireConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new GuidewireBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new GuidewireQueryOperation(createConnection(context));
    }
    protected GuidewireConnection createConnection(BrowseContext context) {
        return new GuidewireConnection(context);
    }
}