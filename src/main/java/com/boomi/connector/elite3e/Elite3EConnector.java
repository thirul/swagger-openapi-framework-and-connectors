package com.boomi.connector.elite3e;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class Elite3EConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new Elite3EBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new Elite3EQueryOperation(createConnection(context));
    }
    protected Elite3EConnection createConnection(BrowseContext context) {
        return new Elite3EConnection(context);
    }
}