package com.boomi.connector.flexport;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class FlexportConnector extends SwaggerConnector {
	   
    protected FlexportConnection createConnection(BrowseContext context) {
        return new FlexportConnection(context);
    }
}