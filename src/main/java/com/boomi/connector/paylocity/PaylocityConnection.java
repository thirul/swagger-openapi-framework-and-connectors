package com.boomi.connector.paylocity;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.AuthType;


public class PaylocityConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public PaylocityConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    public AuthType getAuthenticationType()
    {
    	return AuthType.OAUTH;
    }
    
//    @Override
//    public List<String> getExcludedObjectTypeIDs()
//    {
//    	return Arrays.asList("/pet/{petId}___post", "/user/login___get", "/user/logout___get");
//    }
}