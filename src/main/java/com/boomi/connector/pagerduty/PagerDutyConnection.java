package com.boomi.connector.pagerduty;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class PagerDutyConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public PagerDutyConnection(BrowseContext context) {
		super(context);
	}
	
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }
}