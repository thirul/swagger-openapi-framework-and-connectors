package com.boomi.connector.marketo;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class MarketoConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public MarketoConnection(BrowseContext context) {
		super(context);
	}
	
    public String getQueryPaginationSplitPath()
    {
    	return "/result/*";
    }
}