package com.boomi.connector.marketo;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class MarketoConnector extends SwaggerConnector {
   
    protected MarketoConnection createConnection(BrowseContext context) {
        return new MarketoConnection(context);
    }
}