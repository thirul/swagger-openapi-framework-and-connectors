package com.boomi.connector.oraclesaas;

import java.net.URLEncoder;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.api.Sort;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerQueryOperation;
import com.boomi.util.CollectionUtil;
import com.boomi.util.StringUtil;

public class OracleSaaSQueryOperation extends SwaggerQueryOperation {
	private static final String PAGINATION_FILTER_URIPARAM = "q";
	private static final String PAGINATION_SORT_URIPARAM = "orderBy";
//	private static final String PAGINATION_EXPAND_URIPARAM = "expand"; //TODO

	protected OracleSaaSQueryOperation(SwaggerConnection conn) {
		super(conn);
	}

//QUERY Params
//	q: string
//	This query parameter defines the where clause. The resource collection will be queried using the provided expressions. The value of this query parameter is one or more expressions. Example: ?q=Deptno>=10 and <= 30;Loc!=NY
//	Format: ?q=expression1;expression2
	
//PaGINATION
//Request - limit, offset
//Response - hasMore, next link (recommended for performance, offset slow?)
// "links" :[  {
//        "rel": "next",
//        "href": "https://mysite.example.com/services/rest/connect/v1.4/contacts?totalResults=true&limit=20&fromId=21"
//        },
		
//*********************************************
//All below this will be overridden, all above goes into abstract class
		
	//orderBy=lastName:DESC,firstName:ASC
    @Override
    protected String getSortTermsQueryParam(List<Sort> sortTerms)
    {
    	String sortTermsString="";
    	if (sortTerms!=null)
    	{		            
        	for (int x=0; x<sortTerms.size(); x++)
        	{
        		Sort sort = (Sort)sortTerms.get(x);
        		String sortTerm = sort.getProperty();
        		//TODO Deep Sorts Supported?
    			sortTerm=sortTerm.replace('/', '.');
        		if (sortTerm != null && sortTerm.length()>0)
        		{
        			if (sortTermsString.length()==0)
        				sortTermsString+=PAGINATION_SORT_URIPARAM+"=";
        			else
        				sortTermsString+=",";
        			sortTermsString += sortTerm + ":" + sort.getSortOrder();
        		}
        	}
    	}
    	return sortTermsString;
    }
	
    /**
     * Constructs a list of query filter terms from the given filter, may be {@code null} or empty.
     *
     * @param filter query filter from which to construct the terms
     *
     * @return collection of query filter terms for the demo service
     */

	
	//TODO Oracle has varied expression grammars for comparison > vs. gt
	//Some have "finder=" in addition to "q=" Format: ?finder=<finderName>;<variableName>=<variableValue>,<variableName2>=<variableValue2>
	//both AND and ";" seem to be synanomous
	//
	//Oracle Commerce Cloud: https://docs.oracle.com/en/cloud/saas/commerce-cloud/19d/occ-developer/rest-api-query-parameters.html
	//GET /ccadmin/v1/products?q=orderLimit gt 5 and orderLimit lt 10
	//
	//Oracle HCM Cloud https://docs.oracle.com/en/cloud/saas/human-resources/20c/farws/op-emps-get.html
	//q=Deptno>=10 and <= 30;Loc!=NY
	//Format: ?q=expression1;expression2
	//
	//Oracle Finance cloud has weird co and sw operators
	//https://docs.oracle.com/en/cloud/saas/commerce-cloud/19d/occ-developer/rest-api-query-parameters.html
	//ccadmin/v1/products?q=displayName co "shirt"
	///ccadmin/v1/products?q=description sw "pa"
	
    protected String buildSimpleExpression(SimpleExpression expr) {
        // this is the name of the queried object's property
    	String term="";
        String propName = expr.getProperty();
        String operator = expr.getOperator();
        
        if (StringUtil.isBlank(propName))
        	throw new ConnectorException("Filter field parameter required");
        
        // we only support 1 argument operations
        if(CollectionUtil.size(expr.getArguments()) != 1) {
            throw new IllegalStateException("Unexpected number of arguments for operation " + expr.getOperator() + "; found " +
                                            CollectionUtil.size(expr.getArguments()) + ", expected 1");
        }

        // this is the single operation argument
       	String parameter=expr.getArguments().get(0);
        if (parameter==null || parameter.length()==0)
        	throw new ConnectorException(String.format("Filter parameter is required for field: %s ", propName));
        
        //TODO is filtering supported on child items? Otherwise throw error or prevent child items in Browse
        propName=propName.replace('/', '.');
        
        //TODO how do I determin if string or date? probably easiest to force user to quote strings?
        //        String type = _operationCookie.getEdmType("/"+propName);

        if (this.getOperationProperties().getBooleanProperty("SYMBOLIC_FILTER_OPERATIONS"))
        {
        	switch (operator)
        	{
        	case "eq":
        		operator="=";
        		break;
        	case "ne":
        		operator="<>";
        		break;
        	case "gt":
        		operator=">";
        		break;
        	case "lt":
        		operator="<";
        		break;
        	case "ge":
        		operator=">=";
        		break;
        	case "le":
        		operator="<=";
        		break;
        	}
        	// use =, >, >=, <, <=
        }
       	term = propName + " " + operator + " " + parameter; 

        return URLEncoder.encode(term);
    }
    
    @Override
    /*
     *  build the fields=x,y from the selections passed in. 
     *  If child fields are included, substitute . for / and build an &expand=parent1,parent2...
     */
    //TODO We should make this default behavior: expand= , replace / with . , turn expand on/off
    protected String getSelectTermsQueryParam(List<String> selectedFields)
    {
    	StringBuilder expand=new StringBuilder();
    	StringBuilder selection = new StringBuilder();
    	Set<String> expands = new HashSet<String>();
    	
    	boolean buildFieldParameter = getOperationProperties().getBooleanProperty(OperationProperties.ALLOW_FIELD_SELECTION.name(), false);
    	
    	if (selectedFields!=null && selectedFields.size()>0)
    	{
    		selection.append(getFieldSelectionQueryParam()+"=");
    		for (int x=0; x < selectedFields.size(); x++)
    		{
    			if (x!=0) 
    				selection.append(",");
    			//expanded fields use '.' so replace Boomi's '/'
//    			String field=selectedFields.get(x).replace('/', '.');
    			//TODO oracle throws an error when a . used in fields, URL request parameter fields with value DateOfBirth,DisplayName,FirstName,LastName,PersonId,directReports.FullName is not valid.
    			String field = selectedFields.get(x);//.replace('/', '.');
    			if (field.contains("/"))
    			{
    				String elements[]=field.split("/");
    				expands.add(elements[0]);
    			} else {
    	   			selection.append(field);
       			}
    		}
    	}  	
    	
    	//We only want the expand=, not fields=
    	if (!buildFieldParameter)
    		selection.setLength(0);
    	
    	for (String parent : expands)
    	{
    		if (expand.length()==0)
    			expand.append("expand=");
    		else
    			expand.append(",");
    		expand.append(parent);
    	}
       	if (expands.size()>0)
       	{
           	if (buildFieldParameter && selection.length()>0)
        	{
           		selection.append("&");
        	}
           	selection.append(expand);
       	}
    	return selection.toString();
    }
    
	@Override
	protected String getHasMoreElementPath() {
		return "/hasMore";
	}

	@Override
	protected PaginationType getPaginationType() {
		return PaginationType.PAGINATION_TYPE_PAGE_OFFSET;
	}
	
	@Override
	protected String getFilterQueryParameter()
	{
		return PAGINATION_FILTER_URIPARAM;
	}

}