package com.boomi.connector.intapp;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.sap_concur.SAPConcurBrowser;
import com.boomi.connector.sap_concur.SAPConcurQueryOperation;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class IntappConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new IntappBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new IntappQueryOperation(createConnection(context));
    }
    protected IntappConnection createConnection(BrowseContext context) {
        return new IntappConnection(context);
    }
}