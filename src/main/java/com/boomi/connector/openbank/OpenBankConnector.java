package com.boomi.connector.openbank;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class OpenBankConnector extends SwaggerConnector {
   
    protected OpenBankConnection createConnection(BrowseContext context) {
        return new OpenBankConnection(context);
    }
}