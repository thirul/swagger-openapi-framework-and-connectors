package com.boomi.connector.openbank;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;

public class OpenBankConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public OpenBankConnection(BrowseContext context) {
		super(context);
	}
	
	@Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }
}