package com.boomi.connector.boomi_dcp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.util.IOUtil;

public class BoomiDCPConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public BoomiDCPConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/results/*";
    }
    
    @Override
    protected String getSwaggerUrl()
    {
    	return "resources/boomi_dcp/DCP-rest-api-swagger.json";
    }    
    	
    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.CUSTOM;
    }
    
    @Override
    protected String getCustomAuthHeader() throws JSONException, IOException, GeneralSecurityException, UnsupportedOperationException 
    {
    	InputStream is = null;
    	CloseableHttpResponse httpResponse = null;
    	try {
        	String request = String.format("{\"username\":\"%s\",\"password\":\"%s\"}", getUsername(), getPassword());
        	is = new ByteArrayInputStream(request.getBytes());
        	httpResponse = this.doExecuteWithoutAuthentiation("/get-jwt/", is, "POST");
        	JSONObject response = new JSONObject(new JSONTokener(httpResponse.getEntity().getContent()));
        	String header="Bearer ";
        	if (response.has("token"))
        		header+=response.getString("token");
        	return header;
    	} finally {
    		if (is!=null)
    			IOUtil.closeQuietly(is);
    		if (httpResponse!=null)
    			httpResponse.close();
    	}
    }
}