package com.boomi.connector.boomi_dcp;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class BoomiDCPConnector extends SwaggerConnector {
	   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new BoomiDCPBrowser(createConnection(context));
    }    
    		
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new BoomiDCPQueryOperation(createConnection(context));
    }
    
    protected BoomiDCPConnection createConnection(BrowseContext context) {
        return new BoomiDCPConnection(context);
    }
}