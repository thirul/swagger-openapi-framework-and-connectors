package com.boomi.connector.bullhorn;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Logger;

import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;
import com.boomi.execution.ExecutionManager;


public class BullhornConnection extends SwaggerConnection {
	public enum CustomConnectionProperties {CLIENT_ID, CLIENT_SECRET, REGION, OTHER_REGION}
    Logger logger = Logger.getLogger(this.getClass().getName());
    String _BhRestToken = null;
    
 	public BullhornConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/";
    }
    
    @Override
    protected void testConnection()
    {
    	CloseableHttpResponse httpTokenResponse = null;
    	try {
        	httpTokenResponse = doExecute("/ping", null, "GET", null, null);    		
    	} catch (GeneralSecurityException | IOException e) {
    		throw new ConnectorException(e);
    	} finally {
    		IOUtil.closeQuietly(httpTokenResponse);
    	}
    }

    private String getRegion()
    {
    	String region = this.getConnectionProperties().getProperty(CustomConnectionProperties.OTHER_REGION.name());
    	if (StringUtil.isEmpty(region))
    		region = this.getConnectionProperties().getProperty(CustomConnectionProperties.REGION.name());
    	if (StringUtil.isEmpty(region))
    		throw new ConnectorException("Data Center Region must be specified");
    	return region;
    }
    
    private String getAuthURL()
    {
    	return "https://auth-"+getRegion()+".bullhornstaffing.com";
    }
    
    @Override 
    protected InputStream insertCustomHeaders(Map<String,String> headers, InputStream data)
    {
    	headers.put("BhRestToken", _BhRestToken);
    	return data;
    }
    
    @Override
    //We use this call to authenticate and generate a base URL that includes the session path parameter with a valid session id
	protected String getBaseUrl()
	{
    	InputStream is = null;
    	CloseableHttpResponse httpAuthorizeResponse = null;
    	CloseableHttpResponse httpTokenResponse = null;
    	CloseableHttpResponse httpSessionResponse = null;
    	String restUrl=null;
    	
    	//preserve the operation method
    	String originalMethod = this.getHttpMethod();
    	try {
    		String clientId = this.getConnectionProperties().getProperty(CustomConnectionProperties.CLIENT_ID.name());
    		if (StringUtil.isBlank(clientId))
    			throw new ConnectorException("Client ID must be specified");
    		String clientSecret = this.getConnectionProperties().getProperty(CustomConnectionProperties.CLIENT_SECRET.name());
    		if (StringUtil.isBlank(clientSecret))
    			throw new ConnectorException("Client Secret must be specified");
        	
    		//lets cache the token this.getContext().getConnectorCache();
    		ConcurrentMap<Object, Object> cache = this.getContext().getConnectorCache();
    		String executionId=UUID.randomUUID().toString(); //Test Connection doesn't have an executionID
    		if (ExecutionManager.getCurrent()!=null)
    			executionId=ExecutionManager.getCurrent().getExecutionId();
        	String cacheKeyBase = this.getRegion()+this.getUsername()+clientId+clientSecret+this.getPassword()+executionId;
//        	logger.info("Cache Key: " + cacheKeyBase); 
        	String sessionTokenCacheKey = "SESSIONCACHE"+cacheKeyBase;    		
        	String restUrlCacheKey="RESTURL"+cacheKeyBase;
        	this._BhRestToken=(String)cache.get(sessionTokenCacheKey);
        	restUrl=(String)cache.get(restUrlCacheKey);

        	if (StringUtil.isBlank(restUrl) || StringUtil.isBlank(restUrl))
        	{
        		//https://auth-emea.bullhornstaffing.com
            	String path = String.format("/oauth/authorize?action=Login&username=%s&password=%s&client_id=%s&response_type=code&redirect_URI=http://www.bullhorn.com/", getUsername(), getPassword(), clientId);
            	String url = getAuthURL() + path;
            	httpAuthorizeResponse = this.doExecuteWithoutAuthentiation(url, null, "GET");
            	Header[] hdrs = httpAuthorizeResponse.getAllHeaders();
            	//oauth/authorize?action=Login&username=boomiapi.nes&password=1ronMan%23&client_id=fa32f181-670a-4809-88c5-9a866daa9a2d&response_type=code&redirect_URI=http://www.bullhorn.com/
            	String redirect = null;
            	
            	String authorizationCode = null;
            	String accessToken=null;
//            	String refreshToken=null;
//            	String bhRestToken=null;
//            	String corpTokenPathParam=null;
            	
            	if (httpAuthorizeResponse.getFirstHeader("Location")!=null)
            		redirect = httpAuthorizeResponse.getFirstHeader("Location").getValue();
            	if (StringUtil.isNotBlank(redirect))
            	{
                	String target="code=";
                	int codeLocation = redirect.indexOf(target);
                	if (codeLocation>-1)
                	{
                		//&client_id=987b70b6-7e21-4460-942d-8c549a5608cf&client_secret=4xh395KwkkYwvsIkKbLf2uujZ3X81vvd
                		authorizationCode=redirect.substring(codeLocation+target.length());
                		//Remove any trailing URI params
                		codeLocation=authorizationCode.indexOf("&");
                		if (codeLocation>-1)
                			authorizationCode = authorizationCode.substring(0, codeLocation);
                		//https://auth.bullhornstaffing.com
                		path = String.format("/oauth/token?grant_type=authorization_code&code=%s&client_id=%s&client_secret=%s&redirect_URI=http://www.bullhorn.com", authorizationCode, clientId, clientSecret);
                		url = getAuthURL() + path;
                		
                		//TODO Kevin Lam doesn't want bytearrayinput streams but at this moment I can't remember the alternative he wants
                		httpTokenResponse = this.doExecuteWithoutAuthentiation(url, new ByteArrayInputStream("".getBytes()), "POST");
                    	JSONObject tokenResponse = new JSONObject(new JSONTokener(httpTokenResponse.getEntity().getContent()));
                    	if (tokenResponse.has("access_token"))
                    		accessToken=tokenResponse.getString("access_token");
//                    	if (tokenResponse.has("refresh_token"))
//                    		refreshToken=tokenResponse.getString("refresh_token");
                    	if (StringUtil.isNotBlank(accessToken))
                    	{
                    		//POST https://rest.bullhornstaffing.com/rest-services/login?version=*& [rest.bullhornstaffing.com] access_token={xxxxxxxx}
                    		path = String.format("/rest-services/login?version=*&access_token=%s", accessToken);
//                    		uri = new URI("https://auth-"+getRegion()+".bullhornstaffing.com"+path);
                    		url = "https://rest-"+getRegion()+".bullhornstaffing.com"+path;
                    		//TODO does get region even work? Hardcode for now
                    		httpSessionResponse = this.doExecuteWithoutAuthentiation(url, null, "GET");
                        	JSONObject sessionResponse = new JSONObject(new JSONTokener(httpSessionResponse.getEntity().getContent()));
//                        	if (sessionResponse.has("BhRestToken"))
//                        		bhRestToken=tokenResponse.getString("BhRestToken");
                        	if (sessionResponse.has("restUrl"))
                        	{
//                        		/https://rest.bullhornstaffing.com/rest-services/ [rest.bullhornstaffing.com]{corpToken}/"
                        		restUrl=sessionResponse.getString("restUrl");
                        		if (restUrl.endsWith("/"))
                        			restUrl=restUrl.substring(0,restUrl.length()-1);
                        		_BhRestToken = sessionResponse.getString("BhRestToken");
                        	}                		
                    	}                			
                	}
            	}
            	if (StringUtil.isBlank(restUrl))
            		throw new ConnectorException("Login error. Please verify username, password, client ID and endpoint");
            	this.setHttpMethod(originalMethod);
        		//Now cache the items
        		cache.put(sessionTokenCacheKey, _BhRestToken);
        		cache.put(restUrlCacheKey, restUrl);        	
        	} else {
        		logger.info("Session found in cache");
        	}
    	} catch (Exception e) {
    		e.printStackTrace();
    		throw new ConnectorException(e);
    	} finally {
    		if (is!=null)
    			IOUtil.closeQuietly(is);
    		try {
        		if (httpAuthorizeResponse!=null)
        			httpAuthorizeResponse.close();
        		if (httpTokenResponse!=null)
        			httpTokenResponse.close();
        		if (httpSessionResponse!=null)
        			httpSessionResponse.close();
    		} catch (IOException e) {
    			throw new ConnectorException (e);
    		}
    	}
   	return restUrl;
    } 
}