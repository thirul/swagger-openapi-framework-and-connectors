package com.boomi.connector.bullhorn;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collection;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.ui.BrowseField;
import com.boomi.connector.api.ui.DataType;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.swaggerframework.swaggerutil.SwaggerUtil;

public class BullhornBrowser extends SwaggerBrowser  implements ConnectionTester {
    public BullhornBrowser(SwaggerConnection conn) {
        super(conn);
    }

	@Override
    public BullhornConnection getConnection() {
        return (BullhornConnection) super.getConnection();
    }
	
	@Override
	public ObjectTypes getObjectTypes() 
	{
		OperationType operationType = this.getContext().getOperationType();
		String customOperationType = this.getContext().getCustomOperationType();
		ObjectTypes objectTypes = new ObjectTypes();		
		CloseableHttpResponse response = null; 
		try {
			response = this.getConnection().doExecute("/meta", null, "GET", null, null);
			String types = SwaggerUtil.inputStreamToString(response.getEntity().getContent());
			JSONArray typeArray = new JSONArray(types);
		//TODO call https://rest.bullhornstaffing.com/rest-services/e999/meta
//		[
//		    {
//		        "entity": "Appointment",
//		        "metaUrl": "https://rest.bullhornstaffing.com/rest-services/e999/meta/Appointment?fields=*"
//		    }

			for (int i=0; i<typeArray.length(); i++)
			{
				String key = typeArray.getJSONObject(i).getString("entity");
				String methodName = null;
				String path = null;
				switch (operationType)
				{
				case QUERY:
					methodName="get";
					path="/search/"+key;
					break;
				case EXECUTE:
					switch (customOperationType)
					{
					case "GET":
						methodName="get";
						path="/entity/"+key+"/{entityId}";
						break;
					case "DELETE":
						methodName="delete";
						path="/entity/"+key+"/{entityId}";
						break;
					case "CREATE":
						methodName="post";
						path="/entity/"+key;
						break;
					case "UPDATE":
						methodName="put";
						path="/entity/"+key;
						break;
					}
					break;
				default:
					throw new ConnectorException("Operation " + operationType + "not supported");
				}						
				ObjectType objectType = new ObjectType();
				String objectTypeID = path + SwaggerBrowseUtil.PATH_METHOD_OBJECTID_DELIMITER + methodName;
				objectType.setId(objectTypeID);
				objectType.setLabel(entityNameFromObjectTypeID(objectTypeID));
				objectTypes.getTypes().add(objectType);
			}
			SwaggerBrowseUtil.sortObjectTypes(objectTypes);
		} catch (IOException | GeneralSecurityException e) {
			throw new ConnectorException(e);
		}
		return objectTypes;
	}

	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId,
			Collection<ObjectDefinitionRole> roles)		
	{
		String entityName = entityNameFromObjectTypeID(objectTypeId);
		ObjectDefinitions objDefinitions = new ObjectDefinitions();
		OperationType operationType=this.getContext().getOperationType();//.getCustomOperationType();
		String customOperationType = this.getContext().getCustomOperationType();

		CloseableHttpResponse response = null; 
		try {
			String path = "/meta/" + entityName + "?fields=*&meta=full";
			response = this.getConnection().doExecute(path, null, "GET", null, null);
			String entityMetadata = SwaggerUtil.inputStreamToString(response.getEntity().getContent());
			JSONObject metaObject = new JSONObject(entityMetadata);
			String schema = getSchemaFromMetadata(metaObject, entityName);
			for(ObjectDefinitionRole role : roles)
			{
				ObjectDefinition objDefinition = new ObjectDefinition();
				if (ObjectDefinitionRole.INPUT == role)
				{
					if ("DELETE".contentEquals(customOperationType) || "GET".contentEquals(customOperationType))
					{
						//TODO add dynamic operation property entityId
						BrowseField simpleField = new BrowseField();
	
						simpleField.setId(SwaggerBrowseUtil.PATH_PARAMETER_FIELD_ID_PREFIX+"entityId");
						simpleField.setLabel("Entity ID");
						simpleField.setOverrideable(true);
						simpleField.setType(DataType.STRING);
						objDefinitions.getOperationFields().add(simpleField);
						objDefinition.setInputType(ContentType.NONE);
					}
					else
					{
						objDefinition.setInputType(ContentType.JSON);
						objDefinition.setElementName("/"+entityName); //specify root element as schema location
						objDefinition.setJsonSchema(schema);
					}
					objDefinitions.getDefinitions().add(objDefinition);								
				} 
				else 
				{
					if (customOperationType != null && "DELETE".contentEquals(customOperationType))
						objDefinition.setOutputType(ContentType.NONE);
					else
					{
						objDefinition.setOutputType(ContentType.JSON);
						objDefinition.setElementName("/"+entityName); //specify root element as schema location
						objDefinition.setJsonSchema(schema);
						if (operationType==OperationType.QUERY)
						{
							//							this.getPathParameterFilterSpecs(_swaggerService, objDefinition.getFieldSpecFields());
							//							this.getFilterSortSpecs(_swaggerService, objDefinition.getFieldSpecFields());
						}
					}
					objDefinitions.getDefinitions().add(objDefinition);								
				}
			}
		} catch (IOException | GeneralSecurityException e) {
			throw new ConnectorException(e);
		}
		return objDefinitions;
	}
	
private String entityNameFromObjectTypeID(String objectTypeId) {
	String entityName = SwaggerBrowseUtil.getPath(objectTypeId);
	entityName = entityName.replace("/entity/", "");
	entityName = entityName.replace("/search/", "");
//	entityName = entityName.replace("entity/", "");
//	entityName = entityName.replace("search/", "");
	int target = entityName.indexOf("/");
	if (target>-1)
		entityName = entityName.substring(0,target);
	return entityName;
}

	//	{
//		  "entity" : "Candidate",
//		  "entityMetaUrl" : "https://rest.bullhornstaffing.com/rest-services/e999/meta/Candidate?fields=*",
//		  "label" : "Candidate",
//		  "fields" : { {
//		    "name" : "id",
//		    "type" : "ID",
//		    "dataType" : "Integer"
//		  }, {
//		    ...
//		  }]
//		}
	private String getSchemaFromMetadata(JSONObject metadataObject, String entityName) {
		JSONArray fields = metadataObject.getJSONArray("fields");
		StringBuffer sb = new StringBuffer();
		sb.append("{\""+entityName+"\":{\"type\":\"object\",\"properties\":{");
		
		//https://bullhorn.github.io/rest-api-docs/#get-meta-entity
		//TODO composite types, required, other primitives like...
		//Property data type: Integer, BigDecimal, Double, String, Boolean, Timestamp, byte[], Address, Address1, AddressWithoutCountry (these are composite types), and LoginRestrictions (!! may change).
		int fieldCount=0;
		for (int i=0; i < fields.length(); i++)
		{
			JSONObject field=fields.getJSONObject(i);
			if (!field.has("dataType"))
				logger.info("UNSUPPORTED TYPE: "+field.toString());
			else
			{
				if (fieldCount>0)
					sb.append(",");
				fieldCount++;
				sb.append("\""+field.getString("name")+"\":{");
				String type=null;
				String dataType = field.getString("dataType");
				switch (dataType)
				{
					case "String":
						type="string";
						break;
					case "Integer":
						type="integer";
						break;
					case "Boolean":
						type="boolean";
						break;
					case "Double":
						type="number";
						break;
					case "Date":
						type="string";
						sb.append("\"format\":\"date\",");
						break;
					default:
						type="string";
						logger.info("DEFAULTED TYPE: "+field.toString());
//						throw new ConnectorException("Unsupported Type: " + dataType);
				}
				sb.append("\"type\":\""+type+"\"");
				if (field.has("maxLength"))
					sb.append(",\"maxLength\":"+field.getInt("maxLength"));
				if (field.has("required"))
					sb.append(",\"required\":"+field.getBoolean("required"));
				sb.append("}");
			}
			//>{"User GetUser":{"type":"object","properties":{"lastLogin":{"description":"The date and time when the user last logged in to the system.","type":"string"},
		}
		sb.append("}}}");
		return sb.toString();
	}
}