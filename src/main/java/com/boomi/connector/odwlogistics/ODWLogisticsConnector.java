package com.boomi.connector.odwlogistics;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class ODWLogisticsConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new ODWLogisticsBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new ODWLogisticsQueryOperation(createConnection(context));
    }
    protected ODWLogisticsConnection createConnection(BrowseContext context) {
        return new ODWLogisticsConnection(context);
    }
}