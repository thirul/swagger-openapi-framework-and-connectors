package com.boomi.connector.googlebigquery;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.googlebigquery.GoogleBigQueryBrowser.BQOperationTypes;
import com.boomi.swaggerframework.swaggeroperations.SwaggerExecuteOperation;

public class GoogleBigQueryExecuteOperation extends SwaggerExecuteOperation{
	OperationType _operationType;
	String _customOperationType;
	PropertyMap _connProps;
	PropertyMap _opProps;
	public GoogleBigQueryExecuteOperation(GoogleBigQueryConnection conn) {
		super(conn);
		_operationType=this.getContext().getOperationType();//.getCustomOperationType();
		_customOperationType = this.getContext().getCustomOperationType();
		_connProps = getContext().getConnectionProperties();
		_opProps = getContext().getOperationProperties();
	}
        
	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		if (_operationType!=OperationType.EXECUTE || !BQOperationTypes.BATCH_LOAD.name().contentEquals(_customOperationType))		
			super.executeUpdate(request, response);
		//TODO Convert JSON to CSV
		//TODO Create Temp Table
		//TODO Upload to Temp Table
		
		if ("UPSERT".contentEquals(_opProps.getProperty("WRITE_OPTION")))
				; //Execute RunJob/query
		else //INSERT
			; // Execute RunJob Load
		
		return;
	}
}
