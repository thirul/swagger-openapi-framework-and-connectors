package com.boomi.connector.googlebigquery;

import java.util.Collection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.PropertyMap;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;
import com.boomi.util.StringUtil;

public class GoogleBigQueryBrowser extends SwaggerBrowser  implements ConnectionTester {
	enum BQOperationTypes {BATCH_LOAD}
	OperationType _operationType;
	String _customOperationType;
    public GoogleBigQueryBrowser(SwaggerConnection conn) {
        super(conn);
		_operationType=this.getContext().getOperationType();//.getCustomOperationType();
		_customOperationType = this.getContext().getCustomOperationType();
    }
    
    @Override
	public ObjectTypes getObjectTypes()
	{
		PropertyMap opProps = getContext().getOperationProperties();
		String dataSetId = opProps.getProperty("DATASET_ID");
		if (StringUtil.isBlank(dataSetId))
			throw new ConnectorException("A Dataset ID must be specified");
		//TODO Query all tables for dataset
		return null;
	}
    
	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId,
			Collection<ObjectDefinitionRole> roles)		
	{
		if (_operationType!=OperationType.EXECUTE || !BQOperationTypes.BATCH_LOAD.name().contentEquals(_customOperationType))		
			return super.getObjectDefinitions(objectTypeId, roles);
		//TODO Query table/objectId fields
		//TODO generate JSON Profile for fields
		//TODO if Upsert, generate dynamic field with the SQL statement (ideally read only)
		return null;
	}

	//Check for pagination query parameters in the request schema. This allows for query operations that end with a path parameter
    //TODO there is no way to check if a query because we need to check for NextPage in the response schema...not supported...so use default way looking for lack of trailing {}
    //Concur has query operations that do not have offset as a parameter...but DOES have a nextpage response. Seems like a bug in their swagger
	@Override
	protected boolean isQueryOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
	{
		if (!"get".contentEquals(methodName) || queryParameters==null)
			return false;
		for (int i=0; i<queryParameters.length(); i++)
		{	JSONObject parameter = (JSONObject)queryParameters.get(i);
			if (parameter.has("in"))
				if (parameter.getString("in").contentEquals("query"))
					if (parameter.has("name"))
						if (parameter.getString("name").contentEquals("pageToken"))
							return true;
		}
		return false;
	}

	@Override
    public GoogleBigQueryConnection getConnection() {
        return (GoogleBigQueryConnection) super.getConnection();
    }
	
	@Override
	protected void getFilterSortSpecs(SwaggerAPIService swaggerService, List<FieldSpecField> fields) {
		JSONArray queryParams = swaggerService.getQueryParameters();
		for (int i=0; i<queryParams.length(); i++)
		{
			JSONObject param = queryParams.getJSONObject(i);
			if (param.has("name"))
			{
				String name = param.getString("name");
				//TODO need to use schema/type to get integer/epoch datetime type
				String type = "string";
				switch (name)
				{
				case "offset":
				case "limit":
					break;
				default:
					FieldSpecField filterable = new FieldSpecField().withName(name).withFilterable(true).withSortable(false).withSelectable(false).withType(type);				
					fields.add(filterable);						
				}
			}
		}
	}
}