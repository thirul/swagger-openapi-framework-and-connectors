package com.boomi.connector.googlebigquery;

import java.io.InputStream;
import java.util.Map;
import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.AuthType;


public class GoogleBigQueryConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    public enum BQConnectionProperties {API_KEY}
    
	public GoogleBigQueryConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return null;
    }

    @Override
    protected String getSwaggerUrl()
    {
    	return "resources/googlebigquery/openapi.json";
    }    
    
    @Override
    protected AuthType getAuthenticationType()
    {
    	return AuthType.OAUTH;
    }

   	/**
	 * Allows the addition of custom headers to the connection. For example for Visa Cybersource, this is used to implement HTTP Signature Authentication
	 * @param httpRequest the connection for which you can add headers via the addRequestProperty method
	 * @param data the InputStream for the request body 
	 * @return the header value ie. Bearer xxxxxxxxxxxxxxxxxxxxxxxxxx
	*/
	protected InputStream insertCustomHeaders(Map<String,String> headers, InputStream data)
	{
		String url = this.getUrl();
		String apiKey=this.getConnectionProperties().getProperty(BQConnectionProperties.API_KEY.name());
		if (url.contains("?"))
			url+="&";
		else 
			url+="?";
		url+="apiKey="+apiKey;
		this.setUrl(url);
		return data;
	}
}