package com.boomi.connector.googlebigquery;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class GoogleBigQueryConnector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new GoogleBigQueryBrowser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new GoogleBigQueryQueryOperation(createConnection(context));
    }
    @Override
    protected Operation createExecuteOperation(OperationContext context) {
        return new GoogleBigQueryExecuteOperation(createConnection(context));
    }
    protected GoogleBigQueryConnection createConnection(BrowseContext context) {
        return new GoogleBigQueryConnection(context);
    }
}