package com.boomi.connector.docusign;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class DocusignConnector extends SwaggerConnector {
   
    protected DocusignConnection createConnection(BrowseContext context) {
        return new DocusignConnection(context);
    }
}