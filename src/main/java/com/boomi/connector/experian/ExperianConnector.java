package com.boomi.connector.experian;

import com.boomi.connector.api.BrowseContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class ExperianConnector extends SwaggerConnector {
	   
    protected ExperianConnection createConnection(BrowseContext context) {
        return new ExperianConnection(context);
    }
}