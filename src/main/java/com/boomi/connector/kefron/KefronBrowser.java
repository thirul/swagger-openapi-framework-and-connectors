package com.boomi.connector.kefron;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.swaggerframework.swaggeroperations.SwaggerBrowser;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggerutil.JSONSchemaGenerator;
import com.boomi.swaggerframework.swaggerutil.SwaggerAPIService;
import com.boomi.util.StringUtil;

public class KefronBrowser extends SwaggerBrowser  implements ConnectionTester {
    public enum OperationProperties {OBJECT_SAMPLE_PATH}

    public KefronBrowser(SwaggerConnection conn) {
        super(conn);
    }

	//Check for pagination query parameters in the request schema. This allows for query operations that end with a path parameter
    //TODO there is no way to check if a query because we need to check for NextPage in the response schema...not supported...so use default way looking for lack of trailing {}
    //Concur has query operations that do not have offset as a parameter...but DOES have a nextpage response. Seems like a bug in their swagger
	@Override
	protected boolean isQueryOperation(String pathString, String methodName, JSONArray queryParameters, JSONObject methodObject)
	{
		if (!"get".contentEquals(methodName) || queryParameters==null)
			return false;
		for (int i=0; i<queryParameters.length(); i++)
		{	JSONObject parameter = (JSONObject)queryParameters.get(i);
			if (parameter.has("in"))
				if (parameter.getString("in").contentEquals("query"))
					if (parameter.has("name"))
						if (parameter.getString("name").contentEquals("PageNumber"))
							return true;
		}
		return false;
	}

	@Override
    public KefronConnection getConnection() {
        return (KefronConnection) super.getConnection();
    }
	
	private boolean doGetSampleJSON(ObjectDefinitionRole role, OperationType operationType, String customOperationType)
	{
		if (operationType==OperationType.QUERY || "GET".contentEquals(customOperationType))
		{
			return true;
		}
		return false;
	}
	
//	@Override
//	protected String getTypeElementName(ObjectDefinitionRole role, OperationType operationType, String customOperationType) {
//		if (this.doGetSampleJSON(role, operationType, customOperationType))
//			return "Response";
//		return super.getTypeElementName(role, operationType, customOperationType);
//	}
//
//	@Override
//	protected String getTypeJSONSchema(ObjectDefinitionRole role, OperationType operationType, String customOperationType) {
//		if (this.doGetSampleJSON(role, operationType, customOperationType))
//		{
//			String path = this.opProps.getProperty("OBJECT_SAMPLE_PATH");
//			if (StringUtil.isNotBlank(path))
//			{
//			   	CloseableHttpResponse httpResponse = null;
//				try {
//					String objectName = this.getTypeElementName(role, operationType, customOperationType);
//					httpResponse = getConnection().doExecute(path, null, "GET", null, null);
//		        	JSONObject response = new JSONObject(new JSONTokener(httpResponse.getEntity().getContent()));
//		        	String schema = JSONSchemaGenerator.outputAsString("Response", "Generated from JSON Sample " + path, response.toString());
//		        	return "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\""+objectName+"\":"+schema+"}";
//				} catch (IOException | GeneralSecurityException e) {
//					throw new ConnectorException(e);
//				} finally {
//			   		if (httpResponse!=null)
//			   		{
//						try {
//							httpResponse.close();
//						} catch (IOException e) {
//							throw new ConnectorException(e);
//						}					
//			   		}
//				}
//			}
//		}
//		return null;
//	}
	
	@Override
	protected void getFilterSortSpecs(SwaggerAPIService swaggerService, List<FieldSpecField> fields) {
		JSONArray queryParams = swaggerService.getQueryParameters();
		for (int i=0; i<queryParams.length(); i++)
		{
			JSONObject param = queryParams.getJSONObject(i);
			if (param.has("name"))
			{
				String name = param.getString("name");
				//TODO need to use schema/type to get integer/epoch datetime type
				String type = "string";
				switch (name)
				{
				case "PageSize":
				case "PageNumber":
				case "OrderBy":
					break;
				default:
					FieldSpecField filterable = new FieldSpecField().withName(name).withFilterable(true).withSortable(false).withSelectable(false).withType(type);				
					fields.add(filterable);						
				}
			}
		}
	}
}