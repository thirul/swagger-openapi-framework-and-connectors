package com.boomi.connector.kefron;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.logging.Logger;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection.AuthType;
import com.boomi.util.IOUtil;


public class KefronConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public KefronConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/data/*";
    }

    @Override
    protected String getSwaggerUrl()
    {
    	return "resources/kefron/openapi.json";
    }    
    
    @Override
    public AuthType getAuthenticationType()
    {
    	return AuthType.CUSTOM;
    }
    
    @Override
    protected String getTestConnectionUrl()
    {
    	return this.getBaseUrl()+"/comments";
    }
    
    @Override
    protected String getResponseContentType()
    {
    	return "*/*";
    }
  
	/**
	 * Provide a custom Authorization header for AuthType.CUSTOM implementations.
	 * Useful for proprietary API driven authentication. 
	 * @return the header value ie. Bearer xxxxxxxxxxxxxxxxxxxxxxxxxx
	*/
    protected String getCustomAuthHeader() throws JSONException, IOException, GeneralSecurityException
    {
    	String httpMethod = this.getHttpMethod(); //KLUDGE if we let the subclass call doExecute, we have to preserver the method so it doesn't get stomped...GOTTA fix this
    	InputStream is = null;
    	CloseableHttpResponse httpResponse = null;
    	try {
        	String request = String.format("{\"apiKey\":\"%s\",\"password\":\"%s\"}", getUsername(), getPassword());
        	is = new ByteArrayInputStream(request.getBytes());
        	httpResponse = this.doExecuteWithoutAuthentiation(this.getBaseUrl()+"/v1/api", is, "POST");
        	JSONObject response = new JSONObject(new JSONTokener(httpResponse.getEntity().getContent()));
        	String header="Bearer ";
        	if (response.has("token"))
        		header+=response.getString("token");
        	this.setHttpMethod(httpMethod);
        	return header;
    	} finally {
    		if (is!=null)
    			IOUtil.closeQuietly(is);
    		if (httpResponse!=null)
    			httpResponse.close();
    	}
    }    
}