package com.boomi.connector.dell_powerstore;

import java.util.logging.Logger;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnection;


public class DellPowerStoreConnection extends SwaggerConnection {
    
    Logger logger = Logger.getLogger(this.getClass().getName());
    
	public DellPowerStoreConnection(BrowseContext context) {
		super(context);
	}
	
    @Override
    public String getQueryPaginationSplitPath()
    {
    	return "/*";
    }

    @Override
    protected String getSwaggerUrl()
    {
    	return "resources/dell_powerstore/openapi.json";
    }    
}