package com.boomi.swaggerframework.swaggeroperations;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.http.client.methods.CloseableHttpResponse;

import com.boomi.connector.api.DynamicPropertyMap;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;

public class SwaggerExecuteOperation extends BaseUpdateOperation {
	SwaggerOperationCookie _cookie;
    public enum OperationProperties {URL_PATH, EXTRA_URL_PARAMETERS}
	protected Logger logger;

	public SwaggerExecuteOperation(SwaggerConnection conn) {
		super(conn);
	}
        
	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		logger = response.getLogger();
		getConnection().setLogger(logger);
		_cookie = new SwaggerOperationCookie(getContext().getObjectDefinitionCookie(ObjectDefinitionRole.INPUT));
		String objectTypeId = getContext().getObjectTypeId();
		logger.info("cookie: " + _cookie.toString());
		PropertyMap opProps=getContext().getOperationProperties();
        String httpMethod = SwaggerBrowseUtil.getMethod(objectTypeId).toUpperCase();
		for (ObjectData input : request) {
            try {
                // POST the content to the create url
            	InputStream data = input.getData();
            	//URL passed in as overridable? 
 
            	String path = this.getURLPath(opProps, input.getDynamicOperationProperties());
            	if (path==null)
            		path = getPath(objectTypeId, opProps, input);

        		CloseableHttpResponse httpResponse = null;
                InputStream obj = null;
                try {

                	Map<String,String> headers = this.getHeaderParameters(opProps, input.getDynamicOperationProperties());
                	//Need to override the path for things like aws cloudformation to inject other queryparams
                	httpResponse = getConnection().doExecute(path, data, httpMethod, _cookie, headers);
                	if (httpResponse.getEntity()!=null)
                		obj = httpResponse.getEntity().getContent();
                    OperationStatus status = OperationStatus.SUCCESS;
                    int httpResponseCode = httpResponse.getStatusLine().getStatusCode();
                    if (httpResponseCode>=300)
                    {
                    	status = OperationStatus.APPLICATION_ERROR;
                    	//TODO can we put the error payload response in the payload?
//                   		Scanner scanner = new Scanner(obj, StandardCharsets.UTF_8.name());
//                		String responseString = scanner.useDelimiter("\\A").next();
//                		scanner.close();
//                		statusMessage += " " + responseString;
//                		dataOut = new ByteArrayInputStream(responseString.getBytes());

                    	logger.severe("httpResponseCode" + httpResponseCode + " " + httpResponse.getStatusLine().getReasonPhrase());
                    }
                    if (obj != null) {
                    	//TODO write headers
                        response.addResult(input, status, httpResponseCode+"",
                        		httpResponse.getStatusLine().getReasonPhrase(), ResponseUtil.toPayload(obj));
                    }
                    else {
                        response.addEmptyResult(input, status, httpResponseCode+"",
                        		httpResponse.getStatusLine().getReasonPhrase());
                    }
                }
                finally {
                    IOUtil.closeQuietly(obj);
                    IOUtil.closeQuietly(httpResponse);
                }
            }
            catch (Exception e) {
                // make best effort to process every input
//            	e.printStackTrace();
            	logger.severe(e.getMessage());
                ResponseUtil.addExceptionFailure(response, input, e);
            }
        }
	}
	
	/**
	 * This allows the path to be sent into the connector from a self link, etc
	 * @param opProps
	 * @param dynamicProperties
	 * @return
	 */
	private String getURLPath(PropertyMap opProps, DynamicPropertyMap dynamicProperties)
	{
		String urlPath = opProps.getProperty(OperationProperties.URL_PATH.name());
		if (StringUtil.isBlank(urlPath))
			urlPath=dynamicProperties.getProperty(OperationProperties.URL_PATH.name());
		if (StringUtil.isNotBlank(urlPath))
		{
			String baseUrl = getConnection().getBaseUrl();
			if (baseUrl!=null)
				baseUrl = baseUrl.replaceAll(":443", "");
			urlPath = urlPath.replaceAll(":443", "");
			logger.info("baseUrl: " + baseUrl + " urlPath:" + urlPath);
			if (urlPath.startsWith(baseUrl))
			{
   				urlPath=urlPath.substring(baseUrl.length());
    			logger.info("baseUrl: " + baseUrl + " urlPath:" + urlPath);
   				if (urlPath.startsWith(_cookie.getBasePath()))
   				{
   					urlPath=urlPath.substring(_cookie.getBasePath().length());
   				}
   			}  
			return urlPath;
		}
		return null;
	}

	/**
	 * For the odd case, like aws cloud formation, we want to ignore the path and let all URI params handle it
	 * @param objectTypeId
	 * @param opProps
	 * @param dynamicProperties
	 * @param resolvedPath
	 * @return
	 */
	protected String getPath(String objectTypeId, PropertyMap opProps, ObjectData input) {
   		DynamicPropertyMap dynamicProperties = input.getDynamicOperationProperties();
		String path = SwaggerBrowseUtil.getPath(objectTypeId);
		int numPathParams = SwaggerBrowseUtil.getNumberOfPathParameters(path);
		
		if (numPathParams > 0)
    	{
    		path = SwaggerBrowseUtil.getURLPathWithParameterValues(objectTypeId, opProps, dynamicProperties);    
    		logger.info("dynamicProperties:" + dynamicProperties.toString());
    	} 
		StringBuilder sb = new StringBuilder();
    	SwaggerQueryOperation.appendPath(sb,input.getDynamicProperties().get("EXTRA_URL_PARAMETERS"));
    	SwaggerQueryOperation.appendPath(sb, opProps.getProperty("EXTRA_URL_PARAMETERS"));
		this.appendQueryParameters(opProps, dynamicProperties, sb);
		return path+sb.toString();
	}

	private void appendQueryParameters(PropertyMap opProps, DynamicPropertyMap dynamicProperties, StringBuilder path) {
		Set<String> keySet = opProps.keySet();
		for (String key: keySet)
		{
			if (key.startsWith(SwaggerBrowseUtil.QUERY_PARAMETER_FIELD_ID_PREFIX))
			{
				String value = dynamicProperties.getProperty(key);
				if (StringUtil.isBlank(value))
				{
					Object obj = opProps.get(key);
					if (obj!=null)
					{
						value = obj.toString();
					}
				}
				if (StringUtil.isNotBlank(value))
				{
					String name = key.substring(SwaggerBrowseUtil.QUERY_PARAMETER_FIELD_ID_PREFIX.length());
					String term=name+"="+URLEncoder.encode(value);
					SwaggerQueryOperation.appendPath(path, term);
				}
			}
		}
	}
	
	private Map<String,String> getHeaderParameters(PropertyMap opProps, DynamicPropertyMap dynamicProperties) {
		Map<String,String> headers = new HashMap<String,String>();
		Set<String> keySet = opProps.keySet();
		for (String key: keySet)
		{
			if (key.startsWith(SwaggerBrowseUtil.HEADER_PARAMETER_FIELD_ID_PREFIX))
			{
				String value = dynamicProperties.getProperty(key);
				if (StringUtil.isEmpty(value))
				{
					Object obj = opProps.get(key);
					if (obj!=null)
					{
						value = obj.toString();
					}
				}
				if (StringUtil.isNotEmpty(value))
				{
					String name = key.substring(SwaggerBrowseUtil.HEADER_PARAMETER_FIELD_ID_PREFIX.length());
					headers.put(name,value);
				}
			}
		}
		return headers;
	}

	@Override
    public SwaggerConnection getConnection() {
        return (SwaggerConnection) super.getConnection();
    }
}