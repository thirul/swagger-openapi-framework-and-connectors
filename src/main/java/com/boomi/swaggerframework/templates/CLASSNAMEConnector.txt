package com.boomi.connector.!!PACKAGENAME!!;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.swaggerframework.swaggeroperations.SwaggerConnector;

public class !!CLASSNAME!!Connector extends SwaggerConnector {
   
	@Override
    public Browser createBrowser(BrowseContext context) {
        return new !!CLASSNAME!!Browser(createConnection(context));
    }    
    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new !!CLASSNAME!!QueryOperation(createConnection(context));
    }
    protected !!CLASSNAME!!Connection createConnection(BrowseContext context) {
        return new !!CLASSNAME!!Connection(context);
    }
}