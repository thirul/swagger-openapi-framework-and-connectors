package com.boomi.swaggerframework.swaggerutil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.boomi.connector.api.ConnectorException;
import com.boomi.swaggerframework.swaggerutil.SwaggerBrowseUtil;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.github.fge.jsonschema.core.exceptions.ProcessingException;
//import com.github.fge.jsonschema.core.report.ProcessingReport;
//import com.github.fge.jsonschema.examples.Utils;
//import com.github.fge.jsonschema.main.JsonSchema;
//import com.github.fge.jsonschema.main.JsonSchemaFactory;
//import com.github.fge.jsonschema.processors.syntax.SyntaxValidator;
//TODO make abstract?
import com.boomi.util.StringUtil;

//TODO we need a cookie for request mimetype, response mimetype, basePath...and a fixup for bath to remove double //, a problem with cybersource

public class SwaggerAPIService {
	private JSONObject fullSwaggerSchema;
	private JSONObject pathObject;
	private JSONObject methodObject;
	private String pathName;
	private String methodName;
	private int xOperationIndex;
	private static Logger logger = Logger.getLogger(SwaggerAPIService.class.getName());
	private String _queryPaginationSplitPath;
	private long maxBrowseDepth = 30;
	private List<String> _refs;
	
	//Constructor used at Browse/Import time for all paths and operations in the swagger file
	public SwaggerAPIService(String objectTypeId, JSONObject fullSwaggerSchema, long maxBrowseDepth) throws ConnectorException
	{
		this.maxBrowseDepth=maxBrowseDepth;
		this.parseObjectId(objectTypeId);
		this.fullSwaggerSchema = fullSwaggerSchema;
		if (fullSwaggerSchema!=null)
		{
			JSONObject paths = fullSwaggerSchema.getJSONObject("paths");
			this.pathObject=paths.getJSONObject(pathName);
			if (this.pathObject==null)
				throw new ConnectorException("Path not found: " + pathObject);
		} else {
			throw new ConnectorException("swagger schema or swagger path object must be provided");
		}
		this.methodObject=this.pathObject.getJSONObject(methodName);
		if (this.xOperationIndex>=0)
		{
			methodObject=(JSONObject)this.methodObject.getJSONArray("x-operations").get(xOperationIndex);
		}
	}
	
	//********** Browser Import ***********
	//********** Browser Import ***********
	//********** Browser Import ***********	

	//TODO Check all operations for output, not just Get...
	
	private void getParametersItems(JSONObject root, JSONArray parameters)
	{
		if (!root.isNull("parameters"))
		{
			JSONArray rawParameters=root.getJSONArray("parameters");
			for (int i=0; i<rawParameters.length(); i++)
			{
				JSONObject parameter = (JSONObject)rawParameters.get(i);
				boolean isTopRef = parameter.has("$ref");
				this.expandRefs(parameter, 0, maxBrowseDepth);
//TODO Kludge, why is expand $refs returning the object one level down and not at top level?
//TODO this breaks create
//But open banking wants it????
//Open bank doesn't fully resolve {"OBReadConsent1Param":{"schema":{"$ref":"#/definitions/OBReadConsent1"},"in":"body","name":"OBReadConsent1Param","description":"Default","required":true},"type":"object"}
//TODO NOT USED!!!
//				if (isTopRef && !parameter.has("schema"))
//				{
//					Object obj = parameter.get((String)parameter.keys().next());
//					if (obj instanceof JSONObject)
//						parameter = (JSONObject) obj;;
//				}
				parameters.put(parameter);			
			}			
		}		
	}
	
	public String getBasePath()
	{
		String basePath="";
		if (fullSwaggerSchema.has("basePath"))
		{
			basePath = fullSwaggerSchema.getString("basePath");
			if (basePath.endsWith("/"))
			{
				if (basePath.length()==1)
					basePath="";
				else
					basePath=basePath.substring(0,basePath.length()-1); //Some swaggers are errant in having a trailing slash	
			}
		}
		return basePath;
	}
	
	public static String getObjectTypeLabel(JSONObject method)
	{
		String objectLabel="<label not found>";
		if (method.has("operationId"))
		{
			objectLabel=method.getString("operationId");
			//Pretty it up
			objectLabel = objectLabel.replaceAll("_", " ");
			objectLabel = objectLabel.replaceAll("  ", " ").trim();
			if (objectLabel.length()>0)
			{
				//Initial Caps for each word
				StringBuilder sb = new StringBuilder(objectLabel);
				int wordStart = 0;
				while(wordStart>-1)
				{
					char firstChar = (sb.charAt(wordStart)+"").toUpperCase().charAt(0);
					sb.setCharAt(wordStart, firstChar);
					wordStart = sb.indexOf(" ", wordStart+1);
					if (wordStart>-1) wordStart++;
				}
				objectLabel = sb.toString();
			}
		}
		else if (!method.isNull("tags") && method.getJSONArray("tags").length()>0)
		{
			objectLabel=method.getJSONArray("tags").getString(0);
		} 
		
		return objectLabel;
	}
	
	public static String getObjectTypeHelpText(JSONObject method)
	{
		String helpText="";
		
		if (!method.isNull("tags"))
		{
			for (int i=0; i< method.getJSONArray("tags").length(); i++)
			{
				helpText+=method.getJSONArray("tags").getString(i) + " ";
			}
		} 

		if (method.has("description"))
		{
			helpText+=method.getString("description") + " ";
		}
		
		if (method.has("summary"))
		{
			helpText+=method.getString("summary") + " ";
		}

		
		helpText = helpText.replace("\r", "");
		helpText = helpText.replace("\n", " ");
		int maxLength=2000;
		if (helpText.length()>maxLength)
			helpText = helpText.substring(0, maxLength) + "...";
		return helpText;
	}

	//Get a list of the path parameters. These as fields in the input profile for GET, DELETE
	//TODO merge any common parameters for the for all endpoint methods
	public JSONArray getParameters()
	{
		JSONArray parameters = new JSONArray();
		getParametersItems(methodObject, parameters);
		getParametersItems(pathObject, parameters);
		return parameters;
	}
	
	public JSONArray getPathParameters()
	{
		JSONArray pathParameters=new JSONArray();	
		JSONArray allParameters=this.getParameters();
		for (int i=0; i<allParameters.length(); i++)
		{
			JSONObject param = (JSONObject)allParameters.get(i);
			if (param.has("in") && "path".contentEquals(param.getString("in")))
				pathParameters.put(param);
		}
		return pathParameters;
	}

	public JSONArray getQueryParameters()
	{
		JSONArray pathParameters=new JSONArray();	
		JSONArray allParameters=this.getParameters();
		for (int i=0; i<allParameters.length(); i++)
		{
			JSONObject param = (JSONObject)allParameters.get(i);
			if (param.has("in") && "query".contentEquals(param.getString("in")))
				pathParameters.put(param);
		}
		return pathParameters;
	}

	public JSONArray getHeaderParameters()
	{
		JSONArray pathParameters=new JSONArray();	
		JSONArray allParameters=this.getParameters();
		for (int i=0; i<allParameters.length(); i++)
		{
			JSONObject param = (JSONObject)allParameters.get(i);
			if (param.has("in") && "header".contentEquals(param.getString("in")))
				pathParameters.put(param);
		}
		return pathParameters;
	}

	//TODO is there a way to tell boomi profile it is required?
	//Add elements in a request profile to allow user to map the path parameters in the URL
	//Used for EXECUTE GET/DELETE profiles
	//Called by browse object definitions
//  {
//  "name": "grant_type",
//  "in": "path",
//  "required": true,
//  "type": "string",
//  "default": "client_credentials"
//},
	//Build a schema for a request profile for Execute GET/DELETE
		
	//Called during browse from getObjectDefinitions, not used at runtime
	public JSONObject getRequestBodySchema()  
	{		
		JSONObject rawSchema = this.getRawRequestBodySchema();
		JSONObject derefererencedSchema=derefererenceSchema(rawSchema);
		return this.getBodySchema(derefererencedSchema, getTypeElementName());
	}
		
	//paginationItemsPath drills down to the actual paginated item in the schema 
	//getObjectDefinitions
	public JSONObject getResponseBodySchema() 
	{		
		JSONObject rawSchema = this.getRawResponseBodySchema();
		if (rawSchema==null)
			return null;
		JSONObject derefererencedSchema=derefererenceSchema(rawSchema);
		return this.getBodySchema(derefererencedSchema, getTypeElementName());
	}
	
	public JSONObject getQueryPaginationResponseBodySchema(String paginationSplitPath)
	{
		this._queryPaginationSplitPath = paginationSplitPath;
		JSONObject rawSchema = this.getRawResponseBodySchema();
		if (rawSchema==null)
			return null;
		JSONObject derefererencedSchema=derefererenceSchema(rawSchema);
		// Drill down to items if it exists 
		
		if (derefererencedSchema!=null)
		{
			if (StringUtil.isBlank(paginationSplitPath))
				paginationSplitPath = getQueryPaginationSplitPath(derefererencedSchema);
			paginationSplitPath = SwaggerAPIService.pathToSchemaPath(paginationSplitPath);
			if (paginationSplitPath!=null)
			{
				JSONObject pageItemSchema = resolvePath(derefererencedSchema, paginationSplitPath);
				//The Connection.getQueryPaginationItemPath is NG
				if (pageItemSchema==null)
				{
					logger.info(derefererencedSchema.toString(2));
					throw new ConnectorException("Schema resolved path is null. Please verify SwaggerConnection.getQueryPaginationItemPath");
				}
				else
					derefererencedSchema = pageItemSchema;
			}
		}
		return this.getBodySchema(derefererencedSchema, getTypeElementName());
	}
	
	//paginationItemsPath drills down to the actual paginated item in the schema 
	//getObjectDefinitions
	public JSONObject getBodySchema(JSONObject derefererencedSchema, String typeName) 
	{		
		if (derefererencedSchema!=null)
		{
			JSONObject schema = new JSONObject();
			schema.put("$schema", "http://json-schema.org/draft-04/schema#");
			schema.put(typeName, derefererencedSchema);
			return schema;
		}
		return null;
	}
	
		
	//Get the elementName for Browse objDefinition.setElementName for a QUERY operation to resolve the a response item array element type
	public String getTypeElementName()
	{
		String objectName;
		objectName=SwaggerBrowseUtil.getObjectTypeLabel(this.methodObject);
		if (objectName!=null)
			objectName=objectName.replaceAll("/", " ");
		return objectName;		
	}
			
	//the URL of the path ie /Users/{id}
	public String getPathURL()
	{
		return pathName;
	}

	public JSONObject getMethodObject() {
		return methodObject;
	}

	//Called by operation execution when building an http request to execute the REST api
	public String getHTTPMethod() {
		return methodName.toUpperCase();
	}
	
	//**********Operation Execution ***********
	//**********Operation Execution ***********
	//**********Operation Execution ***********
	//TODO these need to go into the cookie
	public String getRequestMimeType()
	{
		if (methodObject.isNull("consumes"))
			return "application/json";
		return methodObject.getJSONArray("consumes").getString(0);
	}
	
	public String getResponseMimeType()
	{
		if (methodObject.isNull("produces"))
			return "application/json";
		return methodObject.getJSONArray("produces").getString(0);
	}

	//*************************END PUBLIC METHODS *********************************************

	//TODO We should not rely on a ref to name the top level item.
	//...resolve the full schema to derive both the name and the body schema
	private String getRefName(JSONObject property)
	{
		String name=null;
		if (property!=null && property.has("$ref"))
		{
			String refItems[] = property.getString("$ref").split("/");
			name = refItems[refItems.length-1];
		} 
		return name;
	}

	//raw response with $refs
	private JSONObject getRawRequestBodySchema() 
	{
		JSONObject requestObject=null;
		//OpenAPI30
		if (!methodObject.isNull("requestBody"))
		{
			try {
				requestObject = methodObject.getJSONObject("requestBody");
				if (requestObject.has("content"))
					requestObject=requestObject.getJSONObject("content");
				if (requestObject.has("application/json"))
					requestObject=requestObject.getJSONObject("application/json");
				//Strip has application/x-www-form-urlencoded yuck we will render json and render object dot notation at runtime
				else if (requestObject.has("application/x-www-form-urlencoded"))
					requestObject=requestObject.getJSONObject("application/x-www-form-urlencoded");
				else 
					requestObject=null;//give up
			} catch (Exception e) {
				throw new ConnectorException("Could not resolve the request body for: " + this.getHTTPMethod() + " " + this.getPathURL());
			}
		} //swagger 20
		else 
		{
			JSONArray parameters = getParameters();
			for(int i=0; i<parameters.length(); i++)
			{
				JSONObject parameter = (JSONObject) parameters.get(i);
				if (!parameter.isNull("in") && parameter.getString("in").contentEquals("body"))
				{
					requestObject = parameter;
					break;
				}
			}

		}

		if (requestObject==null)
			return null;
		if (!requestObject.has("schema"))
		{
			throw new ConnectorException("Request Body Schema not found: " + requestObject.toString());
		}
		return requestObject.getJSONObject("schema");
	}
	
	//Get the json schema of the 200 or default response for the selected method
	private JSONObject getRawResponseBodySchema() 
	{
		JSONObject responseObject=null;
		if (!methodObject.isNull("responses"))
		{
			JSONObject responses = methodObject.getJSONObject("responses");
			if (!responses.isNull("200"))
				responseObject = responses.getJSONObject("200");
			else if (!responses.isNull("201"))
				responseObject = responses.getJSONObject("201");
			else if (!responses.isNull("default"))
				responseObject = responses.getJSONObject("default");
			
			//Openapi30
			if (responseObject!=null && responseObject.has("content"))
			{
				JSONObject content=responseObject.getJSONObject("content");
				if (content.has("application/json"))
					responseObject = content.getJSONObject("application/json");
			}
		}
		//Its OK if no response is found for all but query/get operations.
		if (responseObject==null)
			return null;
		//Response object can be a ref so expand
		if (!responseObject.has("schema")) //has a ref to a schema? Openbank? TODO need a loop here?
		{
			this.expandRefs(responseObject, 0, maxBrowseDepth);
		}
		//TODO MPESA has a response but it isn't a schema so lets not throw? {"description":"Successful response"}		
		if (!responseObject.has("schema"))
			return null;
		return responseObject.getJSONObject("schema");
	}
	
	//Split object ID to get the path and http method for the operation
	//We all for > 2 to accomodate oracle x-operation array indicices
	private void parseObjectId(String objectTypeId)
	{
		String components[] = objectTypeId.split(SwaggerBrowseUtil.PATH_METHOD_OBJECTID_DELIMITER);
		if (components.length<2)
			throw new ConnectorException("Malformed Object ID: " + objectTypeId);
		pathName = components[0];
		methodName = components[1];
		if (components.length<3)
			xOperationIndex=-1;
		else
			xOperationIndex=Integer.parseInt(components[2]);
	}
	
	//Evaluate path as an xpath to drill down in the document to the specified child element
	private JSONObject resolvePath(JSONObject root, String path)
	{
		JSONObject newRoot=root;
		String keys[]=path.split("/");
		for (int i=0; i<keys.length; i++)
		{
			if (keys[i].contentEquals("*"))
			{
				JSONObject node = null;
				Iterator<String> iter = newRoot.keys();
				while (iter.hasNext())
				{
					String key = iter.next();
					if ("$ref".contentEquals(key))
					{
						node=newRoot;
						break;
					}
					Object obj = newRoot.get(key);
					if (obj instanceof JSONObject)
					{
						node = (JSONObject) obj;
						break;
					}
				}
				if (node!=null)
					newRoot = node;
				else 
					return null;
			} 
			else if (!newRoot.isNull(keys[i]))
			{
				Object node = newRoot.get(keys[i]);
				//if it is an array, then the next item will be an ordinal
				if (node instanceof JSONArray)
				{
					JSONArray arr = newRoot.getJSONArray(keys[i]);
					i++;
					newRoot = arr.getJSONObject(Integer.parseInt(keys[i])); 
				}
				else
					newRoot = newRoot.getJSONObject(keys[i]);
			} 
			else 
			{
				System.out.println("ERROR Could not resolve pagination schema path: " + path);
				//TODO throw exception??
//				throw new ConnectorException("Could not resolve pagination schema path: " + path);
				//restore original root and give up
				return null;
			}
		}
//		System.out.println(newRoot);
		return newRoot;
	}
	
    /************************************************************************************/
    /* These methods resolve all $ref's and allOfs to work around Boomi's lack of JSON Type support*/
    /************************************************************************************/
	
	private JSONObject derefererenceSchema(JSONObject objectSchema) 
	{
		if (objectSchema==null)
			return null;
		expandRefs(objectSchema, 0, maxBrowseDepth);
		return objectSchema;
	}
	private void expandRefs(JSONObject parentType, int depth, long maxDepth)
	{
		_refs = new ArrayList<String>();
		this.expandRefsRecursive(parentType, depth, maxDepth);
	}
	
	//TODO Copy enum values to description so the profile UI shows them...also required? format?
	private void expandRefsRecursive(JSONObject parentType, int depth, long maxDepth)
	{
		//Remove all the tags boomi won't use ie required, nullable .. 
		//ORRR should we just remove x- prefixes?

//		//avoid concurrentmodfificationexception by storing keys to delete
//		List<String> keysToDelete = new ArrayList<String>();
//	    Iterator<String> keyIter = parentType.keys();
//	    while (keyIter.hasNext()) {
//	        String key = keyIter.next();

//		if (!parentType.has("type"))
//			System.out.print("");
		String[] names = JSONObject.getNames(parentType);
		for (int i = 0; names!=null && i<names.length; i++)
		{
			//x- elements are ignored by boomi but stripe puts alot of $ref/allof etc in there
			if (names[i].startsWith("x-"))
		    	parentType.remove(names[i]);
//			if (names[i].contentEquals("additionalProperties")) //Fix stripe createcustomer import? NOPE
//		    	parentType.remove(names[i]);
//			switch(names[i])
//			{
//			case "readOnly":
//			case "required":
//			case "nullable":
//			case "enum":
//			case "description":
//			case "properties":
//			case "items":
//			case "format":
//			case "title":
//			case "$ref":
//			case "type":
//				break;
//			default:
//				parentType.remove(names[i]);
//			}
		}
		if (parentType.has("additionalProperties")) //there might be a $ref in there...not used by Boomi Import so whack it
			parentType.remove("additionalProperties");
		if (depth>maxDepth)
		{			
			if (parentType.has("$ref"))
			{
				parentType.remove("$ref");
				parentType.put("type", "object");	
			}
			if (parentType.has("anyOf"))
				parentType.remove("anyOf");
			if (parentType.has("allOf"))
				parentType.remove("allOf");
			if (parentType.has("oneOf"))
				parentType.remove("oneOf");
//			String parentString = parentType.toString();
//TODO If we don't remove child properties/items, we have risk of leaving unresolved items in the schema			
//			if (parentType.has("properties") && (parentString.contains("\"$ref\"") || parentString.contains("\"$anyOf\"") || parentString.contains("\"$allOf\"") || parentString.contains("\"oneOf\"")))
//				parentType.remove("properties");
			if (parentType.has("properties"))
//				if (refPath!=null && _refs.contains(refPath))
					parentType.remove("properties");

			if (parentType.has("items"))
				parentType.remove("items");
			if (parentType.has("type") && parentType.getString("type").contentEquals("array"))
			{
				//TODO isn't it enough to just whack the items, or is boomi puking on arrays without items;
				parentType.put("type", "object");				
			}
			if (!parentType.has("type"))
				parentType.put("type", "object");
			String message = "$ref resolution depth exceeded: "; 
			String existingDescription = "";
			if (parentType.has("description"))
				existingDescription = parentType.getString("description");

			String type = parentType.getString("type");
			if (!existingDescription.startsWith(message) && ("object".contentEquals(type) || "array".contentEquals(type)))
			{
				parentType.put("description", message + depth + " " + existingDescription);
			}

			return;
		}
		
		if (parentType.has("$ref")) 
		{

			String ref = parentType.getString("$ref");
			if (!ref.startsWith("#/"))
				throw new ConnectorException("No external file $ref references are supported");
			parentType.remove("$ref");
//TODO We won't allow refs to be reused???	
//Stripe getting stack overflows due to circular jsonobject references in the final jsonobject
			
			String path = ref.substring(2); //Remove leading #/		
			JSONObject subType = this.resolvePath(fullSwaggerSchema, path);
			//TODO we need a stack
			if (path!=null)
			{
				if (!_refs.contains(path))
					_refs.add(path);
				else 
				{
					subType = new JSONObject(subType.toString()); //CLONE
//					parentType.put("description", "ERROR circular references no allowed");
//					return;
				}
			}
			if (subType==null)
			{
				logger.severe("subtype is null");
				throw new ConnectorException("subtype is null");
			}
			expandRefsRecursive(subType, depth+1, maxDepth);
//			subType = new JSONObject(subType.toString()); //CLONE
			if (subType.has("properties"))// && subType.keySet().size()>0)
			{
				parentType.put("type", "object");
				parentType.put("properties", subType.getJSONObject("properties"));
			}
			else
			{
				//simpleton object
				Iterator<String> iter = subType.keys();
				while (iter.hasNext())
				{				
					String key = (String)iter.next();
					parentType.put(key, subType.get(key));
				}
			}			
		}
		else if (parentType.has("allOf"))
		{
			expandAnyAllOneOf(parentType, "allOf", depth, maxDepth);
		}
		else if (parentType.has("oneOf"))
		{
			expandAnyAllOneOf(parentType, "oneOf", depth, maxDepth);
		}
		else if (parentType.has("anyOf"))
		{
			expandAnyAllOneOf(parentType, "anyOf", depth, maxDepth);
		} 
		else 
		{
			//TODO aka type=object
			if (parentType.has("properties"))
			{
				JSONObject properties = parentType.getJSONObject("properties");
				Iterator<String> iter = properties.keys();
				while (iter.hasNext())
				{				
					String key = (String)iter.next();
//billing_details stripe Create Charge response
//weekly_anchor stripe get account response
//if ("invoice_settings".contentEquals(key)) //stripe get account
//	System.out.println();
					JSONObject property = properties.getJSONObject(key);
					expandRefsRecursive(property, depth, maxDepth);
				}
			}
			//paylocity has a "type": ["string", "null"]] so we have to check if it is a string!
			//TODO this is a kludge, maybe grab the first?
			else if (!parentType.isNull("type") && !(parentType.get("type") instanceof String)) {
				parentType.put("type", "string");
			}
			else if (!parentType.isNull("type") && (parentType.get("type") instanceof String) && parentType.getString("type").contentEquals("array"))
			{
				Iterator<String> iter = parentType.keys();
				while (iter.hasNext())
				{				
					String key = (String)iter.next();
					if (parentType.get(key) instanceof JSONObject)
					{
						JSONObject item = parentType.getJSONObject(key);
						expandRefsRecursive(item, depth, maxDepth);
					}
				}
			} else if (!parentType.has("type"))
			{
				//Some bad profiles, like Okta Application, have no type in their schema but boomi requires one
				parentType.put("type", "object");
			}
		}
//		if (parentType.has("properties") && parentType.getJSONObject("properties").keySet().size()==0)
//			parentType.remove("properties");
	}
	//TODO Should we really be merging oneOf and anyOf?
	//If the type is object, we can merge all the properties
	//If the type is an array, who knows ?? TODO
	//If the type is a simpleton, just take the first
	private void expandAnyAllOneOf(JSONObject parentType, String tag, int depth, long maxDepth)
	{
		if (depth>maxDepth)
		{
			parentType.remove(tag);
			parentType.put("type", "object");
			String description = "anyOf/allOf/oneOf resolution depth exceeded:"+ depth + " ";
			if (parentType.has("description"))
				description += parentType.getString("description");
			parentType.put("description", description);
			return;
		}
		//merge the properties of all
		JSONArray allOf = parentType.getJSONArray(tag);
		parentType.put("type", "object");
		JSONObject properties = new JSONObject();
		parentType.put("properties", properties);
		//build a new properties that is superset of all
		//TODO but what if some are objects and others are simple?
		boolean hasObjectProperties = false;
		JSONObject firstChild=null;

		for (int a=0; a<allOf.length(); a++)
		{
			JSONObject child = allOf.getJSONObject(a);
			expandRefsRecursive(child, depth, maxDepth);
			if (firstChild==null)
				firstChild=child;
	
			if (child.has("properties"))
			{
//				child = new JSONObject(allOf.getJSONObject(a).toString()); //Clone or else Stripe throws a stack overflow
				child = allOf.getJSONObject(a); 
				hasObjectProperties = true;
				JSONObject childProperties = child.getJSONObject("properties");
				for (Object keyObj : childProperties.keySet())
				{
					String key = (String)keyObj;
					properties.put(key, childProperties.get(key));
				}
			}
		}
		//There was no objects within the XXXOf?
		if (!hasObjectProperties)
		{
			//We will default to the first item we found
			if (firstChild!=null && firstChild.has("type"))
			{
				String type = firstChild.getString("type");
				if ("array".contentEquals(type))
				{
					if (firstChild.has("items"))
						parentType.put("items", firstChild.get("items"));
//						parentType.put("items", new JSONObject(firstChild.get("items").toString())); //Clone
				}
				parentType.put("type", type);
			}
		}
		if (properties.keySet().size()==0)
			parentType.remove("properties");
		parentType.remove(tag);
	}
	/*
	 * Search the schema for a child array with the assumption that is the split point for query response pagination
	 */
	private String getQueryPaginationSplitPath(JSONObject dereferencedSchema)
	{
		_queryPaginationSplitPath = getQueryPaginationSplitPathRecursive(dereferencedSchema, "/");
		return _queryPaginationSplitPath;
	}
	
	public String getQueryPaginationSplitPath()
	{
		return _queryPaginationSplitPath;
	}
	
	private static String getQueryPaginationSplitPathRecursive(JSONObject dereferencedSchema, String path)
	{
		if (dereferencedSchema.has("type"))
		{
			String type = dereferencedSchema.getString("type");
			if (type.contentEquals("array"))
			{
				return path + "*";
			}
			if (type.contentEquals("object")) {
				JSONObject properties = dereferencedSchema.getJSONObject("properties");
				Set<String> keys = properties.keySet();
				for (String key: keys)
				{
					String split = getQueryPaginationSplitPathRecursive(properties.getJSONObject(key), path+key+"/");
					if (split!=null)
						return split;
				}
			}
		}
		return null;
	}
	/*
	 * Find the path to a schema element corresponding to the json path of the paginated item
	 */
	private static String pathToSchemaPath(String path)
	{
		String schemaPath="";
		
		if (path!=null)
		{
			String elements[] = path.split("/");
			for (String element : elements)
			{
				if (element.length()>0)
				{
					if (element.contentEquals("*"))
					{
						schemaPath+="*";
					} else  {
						schemaPath+="properties/"+element+"/";
					}
				}
			}	
		}
		logger.info(path + " " + schemaPath);
		return schemaPath;
	}
}

