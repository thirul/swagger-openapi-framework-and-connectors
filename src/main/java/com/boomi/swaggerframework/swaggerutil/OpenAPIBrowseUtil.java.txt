package com.boomi.connector.swaggerutil;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.parser.OpenAPIV3Parser;
import io.swagger.v3.parser.core.models.ParseOptions;
import io.swagger.v3.parser.core.models.SwaggerParseResult;

public class OpenAPIBrowseUtil {
	
	public static ObjectTypes getTypesFromOpenAPIv3(String content, OperationType operationType, OperationType customOperation)
	{
		ObjectTypes objectTypes = new ObjectTypes();		
		ParseOptions parseOptions = new ParseOptions();
		parseOptions.setResolve(true);
		SwaggerParseResult result = new OpenAPIV3Parser().readContents(content, null, parseOptions);
		OpenAPI openAPI = result.getOpenAPI();
		Paths paths = result.getOpenAPI().getPaths();
		for (int i=0; i<paths.size(); i++)
		{
			PathItem path = paths.get(i);
//			paths.
			Operation operation = null;
			String pathString = path.toString();
			Parameter p;
		
			boolean doInclude=false;					
			switch (operationType)
			{
			case QUERY:
				operation = path.getGet();
				if (operation!=null && isStandardQuery("get", pathString))
					doInclude=true;
				break;
			case CREATE:
				operation = path.getPost();
				if (operation!=null && isCreate("post", pathString))
					doInclude=true;
				break;
			case DELETE:
				operation = path.getDelete();
				if (operation!=null && isStandardDelete("delete", pathString))
					doInclude=true;
				break;
			case EXECUTE:
				if (customOperation==OperationType.GET)
				{
					if (isCustomGet("get", pathString) || isStandardGet("get", pathString))
						doInclude=true;
				}
				else if (customOperation==OperationType.DELETE)
				{
					if (isCustomDelete("delete", pathString) || isStandardDelete("delete", pathString))
						doInclude=true;
				}
				break;
			case GET:
				operation = path.getGet();
				if (isStandardGet("get", pathString))
					doInclude=true;
				break;
			case UPDATE:
				operation = path.getPut();
				if (isUpdate("put", pathString))
					doInclude=true;
				break;
			case UPSERT:
				break;
			default:
				break;
			}						
			if (doInclude)
			{
				//TODO we might want an option to choose per connector whether to use tag/summary/description since so much variation between swagger implementations
				String label=operation.getOperationId();

				ObjectType objectType = new ObjectType();
//				objectType.setId(pathString + SwaggerAPIService.PATH_METHOD_OBJECTID_DELIMITER + operation.getOperationId()); 
				objectType.setLabel(label);
				objectType.setHelpText(operation.getDescription());
				objectTypes.getTypes().add(objectType);
			}
		}
		sortObjectTypes(objectTypes);
		return objectTypes;
	}

	//TODO
	// QUERY only for paths with zero path parameters, otherwise EXECUTE - GETMANY with now Query UI
	// GET and DELETE only for paths with ONE path parameter, otherwise EXECUTE GET and EXECUTE DELETE
	// Cosmetic choice, if has some paths with > 1 path parameters, always use CUSTOM GET and CUSTOM DELETE
	
	private static boolean isStandardQuery(String methodString, String pathString)
	{
		boolean result=false;
//TODO Our query operation doesn't accept inputs so can't specify paths		if (!pathString.endsWith("}") && methodString.toLowerCase().contentEquals("get"))
		if (!pathString.contains("}") && methodString.toLowerCase().contentEquals("get"))
			result=true;
		return result;
	}
	
	private static boolean isCustomQuery(String methodString, String pathString)
	{
		boolean result=false;
		if (pathString.contains("}") && methodString.toLowerCase().contentEquals("get") && !pathString.endsWith("}"))
			result=true;
		return result;
	}
	
	//Has can have more than 1 Url path parameters so single ID cannot be passed to a boomi standard GET operation and input Profile is required
	private static boolean isCustomGet(String methodString, String pathString)
	{
		boolean result=false;
		if (pathString.endsWith("}") && methodString.toLowerCase().contentEquals("get")  && numPathParameters(pathString)>1)
			result=true;
		return result;
	}
		
	//Has 1 and only one Url path parameter so ID can be passed to a boomi standard GET operation without an input Profile
	private static boolean isStandardGet(String methodString, String pathString)
	{
		boolean result=false;

		if (pathString.endsWith("}") && methodString.toLowerCase().contentEquals("get")  && numPathParameters(pathString)==1)
			result=true;
		return result;
	}
		
	private static boolean isCreate(String methodString, String pathString)
	{
		boolean result=false;
		if (!pathString.endsWith("}") && methodString.toLowerCase().contentEquals("post"))
			result=true;
		return result;
	}
		
	private static boolean isUpdate(String methodString, String pathString)
	{
		boolean isUpdate=false;
		if (pathString.endsWith("}") && methodString.toLowerCase().contentEquals("patch"))
			isUpdate=true;
		else if (pathString.endsWith("}") && methodString.toLowerCase().contentEquals("post"))
			isUpdate=true;
		else if (pathString.endsWith("}") && methodString.toLowerCase().contentEquals("put"))
			isUpdate=true;
		return isUpdate;
	}
		
	//Has 1 and only one Url path parameter so ID can be passed to a boomi standard GET operation without an input Profile
	private static boolean isStandardDelete(String methodString, String pathString)
	{
		boolean result=false;
		if (methodString.toLowerCase().contentEquals("delete") && numPathParameters(pathString)==1)
			result=true;
		return result;
	}
	
	//Has more than 1 Url path parameters so single ID cannot be passed to a boomi standard GET operation and input Profile is required
	private static boolean isCustomDelete(String methodString, String pathString)
	{
		boolean result=false;
		if (methodString.toLowerCase().contentEquals("delete") && numPathParameters(pathString)>1)
			result=true;
		return result;
	}
	
	private static int numPathParameters(String path)
	{
		return path.split("\\{").length-1;
	}
		
	private static void sortObjectTypes(ObjectTypes objectTypes)
	{
		List<ObjectType> objectTypeList = objectTypes.getTypes();
		//Now sort them
		if (objectTypes!=null)
		{
		    Collections.sort(objectTypeList, new Comparator<ObjectType>() {
		        @Override
		        public int compare(ObjectType a, ObjectType b) {
		            return a.getLabel().compareToIgnoreCase(b.getLabel());
		        }
		    });
		}
	}
		
	//Report on the following conditions that require non-standard implementations
	//1 .GET methods that have paths with parameters that do not end in id, aka candidate query operations that can not be supported by SDK because Query Operations can not accept parameters
	//2. GET and DELETE methods with multiple parameter paths which can not be implemented because platform supports only one id, EXECUTE must be used and ids must be put in the input profile 
	//3. Illegal DELETE, PATCH or PUT with no parameter
	//4. POST methods that have path parameter at the end of the path
	//5. PATCH and PUT methods that do not have an parameter at the end of the path
	public static void analyzeOpenAPI(JSONObject swagger)
	{
		JSONObject paths = swagger.getJSONObject("paths");
		Iterator<String> pathIter = paths.keys();
		
		while(pathIter.hasNext())
		{
			String pathString=pathIter.next();
			JSONObject path = paths.getJSONObject(pathString);
			Iterator<String> methodIter = path.keys();
			while(methodIter.hasNext())
			{
				String methodString = methodIter.next();	
				if (!methodString.contentEquals("parameters"))
				{ 
					//Case 1
					if (isCustomQuery(methodString, pathString))
						System.out.println(String.format("QUERY operation with url parameters is not supported - Path: %s, Method: %s", pathString, methodString));
					//Case 2
					if (isCustomDelete(methodString, pathString))
						System.out.println(String.format("DELETE operations with multiple parameters are not supported by the standard DELETE operation, custom EXECUTE required - Path: %s, Method: %s", pathString, methodString));
					if (isCustomGet(methodString, pathString))
						System.out.println(String.format("GET operations with multiple parameters are not supported by the standard GET operation, custom EXECUTE required - Path: %s, Method: %s", pathString, methodString));
					//Case 3
					if (methodString.toLowerCase().contentEquals("delete") && numPathParameters(pathString)==0)
						System.out.println(String.format("Warning: DELETE method has no parameters - Path: %s, Method: %s", pathString, methodString));
					if (methodString.toLowerCase().contentEquals("patch") && numPathParameters(pathString)==0)
						System.out.println(String.format("Warning: PATCH method has no parameters - Path: %s, Method: %s", pathString, methodString));
					if (methodString.toLowerCase().contentEquals("put") && numPathParameters(pathString)==0)
						System.out.println(String.format("Warning: PUT method has no parameters - Path: %s, Method: %s", pathString, methodString));
					//Case 4
					if (methodString.toLowerCase().contentEquals("post") && pathString.endsWith("}"))
						System.out.println(String.format("Warning POST/CREATE Operation without parameter at end of path - Path: %s, Method: %s", pathString, methodString));
					//Case 5
					if (methodString.toLowerCase().contentEquals("PATCH") && !pathString.endsWith("}"))
						System.out.println(String.format("Warning PATCH Operation with parameter at end of path - Path: %s, Method: %s", pathString, methodString));
					if (methodString.toLowerCase().contentEquals("PUT") && !pathString.endsWith("}"))
						System.out.println(String.format("Warning PUT Operation with parameter at end of path - Path: %s, Method: %s", pathString, methodString));
				}
			}
		}
	}
}
