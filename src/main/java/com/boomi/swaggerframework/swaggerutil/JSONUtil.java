package com.boomi.swaggerframework.swaggerutil;

import org.json.JSONObject;
import org.json.JSONTokener;

public class JSONUtil {
	//Read schema from static resource
	public static JSONObject getJSONFromResource(Class theClass, String path)   {
		return new JSONObject(new JSONTokener(theClass.getClassLoader().getResourceAsStream(path)));
	}
}

