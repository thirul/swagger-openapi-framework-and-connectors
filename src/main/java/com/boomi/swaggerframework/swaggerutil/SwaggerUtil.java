package com.boomi.swaggerframework.swaggerutil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.boomi.connector.api.ConnectorException;

public class SwaggerUtil
{	
    public static String inputStreamToString(InputStream is) throws IOException
    {
    	if (is==null)
    		return null;
    	try (Scanner scanner = new Scanner(is, "UTF-8")) {
    		return scanner.useDelimiter("\\A").next();
    	}
    }
    
    static InputStream getResourceAsStream(String resourcePath, Class theClass) throws Exception
    {
    	InputStream is = null;
		try {
			is = theClass.getClassLoader().getResourceAsStream(resourcePath);			
		} catch (Exception e)
		{
			throw new Exception("Error loading resource: "+resourcePath + " " + e.getMessage());
		}
		if (is==null)
			throw new Exception("Error loading resource: "+resourcePath);
		return is;
    }
	
	public static void handleException(Exception e, Logger logger)
	{
		if (logger!=null)
		{
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
		   	logger.log(Level.SEVERE, e.toString() + " " + sw.toString());
		}
	}
	
	public static InputStream jsonToUrlFormEncodedStream(InputStream jis)
	{
		return new ByteArrayInputStream(jsonToUrlFormEncoded(new JSONObject(new JSONTokener(jis))).getBytes());
	}
	
	public static String jsonToUrlFormEncoded(JSONObject in)
	{
		StringBuilder sb = new StringBuilder();
		
		jsonToUrlFormEncodedRecursive(sb, in, "");
		return sb.toString();
	}
	
	private static void jsonToUrlFormEncodedRecursive(StringBuilder sb, JSONObject in, String parentKey)
	{
		Iterator<String> keys = in.keys();
		while (keys.hasNext())
		{
			String key = keys.next();
			Object obj = in.get(key);
			if (obj instanceof JSONObject)
			{
				String newParentKey="";
				newParentKey = key;
//				if (parentKey.length()>0)
//				{
//					key="["+key+"]";
//				}
//				newParentKey+="["+key+"]";
				jsonToUrlFormEncodedRecursive(sb, (JSONObject) obj, newParentKey);
			} else if (obj instanceof JSONArray) {
				throw new ConnectorException("Form URL Encoding can not support json arrays");
			} else {
				if (sb.length()>0)
					sb.append("&");
				if (parentKey.length()>0)
				{
					key=parentKey+"["+key+"]";
				}
				sb.append(key+"="+obj.toString());
			}
		}
	}
	
//	static ConnectorCookie getInputCookie(OperationContext context)
//	{
//        ObjectMapper mapper = new ObjectMapper();
//    	
//    	ConnectorCookie cookie = new ConnectorCookie();
//		try {
//			cookie = mapper.readValue(context.getObjectDefinitionCookie(ObjectDefinitionRole.INPUT),ConnectorCookie.class);
//		} catch (Exception e1) {
//			//If cookie not there, just keep the defaults
//		}
//    	return cookie;
//	}
//	
//	static ConnectorCookie getOutputCookie(OperationContext context)
//	{
//        ObjectMapper mapper = new ObjectMapper();
//    	
//    	ConnectorCookie cookie = new ConnectorCookie();
//		try {
//			cookie = mapper.readValue(context.getObjectDefinitionCookie(ObjectDefinitionRole.OUTPUT),ConnectorCookie.class);
//		} catch (Exception e1) {
//			//If cookie not there, just keep the defaults
//		}
//    	return cookie;
//	}
}