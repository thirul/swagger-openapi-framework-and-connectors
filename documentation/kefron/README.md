# Kefron API
This connector supports the REST API for the Kefron Document Management platform. For more information please go to https://kefronapi2.developer.azure-api.net/hello-developers

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### Kefron API REST API Service URL

The URL for the Kefron API REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| API - Authenticate | API Authenticate  |
| Comment - Insert a comment | Comment Insert a comment  |
| Currency - Insert a currency | Currency Insert a currency  |
| DocumentAttachment - Insert a Document Attachment | DocumentAttachment Insert a Document Attachment  |
| Glcode - Insert a gl code | Glcode Insert a gl code  |
| History - Insert a history | History Insert a history  |
| LookupDataCustom - Insert a Lookup Data Custom | LookupDataCustom Insert a Lookup Data Custom  |
| PaymentTerm - Insert a payment term | PaymentTerm Insert a payment term  |
| Product - Insert a product | Product Insert a product  |
| Report - Execute a specific report | Report Execute a specific report  |
| Supplier - Insert a supplier | Supplier Insert a supplier  |
| TaxCode - Insert a tax code | TaxCode Insert a tax code  |
| User - Create a new User | User Create a new User  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Currency - Delete a specific currency | Currency Delete a specific currency  |
| Document - Delete a specific document | Document Delete a specific document  |
| DocumentAttachment - Delete a specific Document Attachment | DocumentAttachment Delete a specific Document Attachment  |
| DocumentExtension - Delete all documents by document type id | DocumentExtension Warning: it will delete ALL data from a document type Delete all documents by document type id  |
| Glcode - Delete a specific gl code | Glcode Delete a specific gl code  |
| LookupDataCustom - Delete a specific Lookup Data Custom | LookupDataCustom Delete a specific Lookup Data Custom  |
| PaymentTerm - Delete a specific payment term | PaymentTerm Delete a specific payment term  |
| Product - Delete a specific product | Product Delete a specific product  |
| Supplier - Delete a specific supplier | Supplier Delete a specific supplier  |
| TaxCode - Delete a specific tax code | TaxCode Delete a specific tax code  |
| User - Delete a specific User | User Delete a specific User  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| API - Info | API Info  |
| Comment - Get a specific comment | Comment Get a specific comment  |
| Currency - Get a specific currency | Currency Get a specific currency  |
| Document - Download a specific document | Document Download a specific document  |
| Document - Get a specific document | Document Get a specific document  |
| DocumentAttachment - Download a specific Document Attachment | DocumentAttachment Download a specific Document Attachment  |
| DocumentAttachment - Get a specific Document Attachment | DocumentAttachment Get a specific Document Attachment  |
| DocumentType - Get a specific document type | DocumentType Document type categories:<br /><br /> 0 = AP<br /> 1 = PO<br /> 2 = GRN<br /> 3 = Coding<br /> 4 = Budget<br /> 5 = Supplier Statement Reconciliation<br /> 6 = DM Get a specific document type  |
| Glcode - Get a specific gl code | Glcode Get a specific gl code  |
| History - Get a specific history | History Get a specific history  |
| LookupDataCustom - Get a specific Lookup Data Custom | LookupDataCustom Get a specific Lookup Data Custom  |
| LookupDataTable - Get a specific Lookup Data Table | LookupDataTable Get a specific Lookup Data Table  |
| PaymentTerm - Get a specific payment term | PaymentTerm Get a specific payment term  |
| Product - Get a specific product | Product Get a specific product  |
| Supplier - Get a specific supplier | Supplier Get a specific supplier  |
| TaxCode - Get a specific tax code | TaxCode Get a specific tax code  |
| User - Get a specific User | User Get a specific User  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Comment - Get a list of comments | Comment Get a list of comments  |
| Currency - Get a list of currencies | Currency Get a list of currencies  |
| Document - Get a list of documents | Document Get a list of documents  |
| DocumentAttachment - Get a list of Document Attachments | DocumentAttachment Get a list of Document Attachments  |
| DocumentExtension - Get a list of attachments from a specific document | DocumentExtension Get a list of attachments from a specific document  |
| DocumentExtension - Get a list of comments from a specific document | DocumentExtension Get a list of comments from a specific document  |
| DocumentExtension - Get a list of histories from a specific document | DocumentExtension Get a list of histories from a specific document  |
| DocumentType - Get a list of document indexes | DocumentType Get a list of document indexes  |
| DocumentType - Get a list of document types | DocumentType Document type categories:<br /><br /> 0 = AP<br /> 1 = PO<br /> 2 = GRN<br /> 3 = Coding<br /> 4 = Budget<br /> 5 = Supplier Statement Reconciliation<br /> 6 = DM Get a list of document types  |
| Glcode - Get a list of gl codes | Glcode Get a list of gl codes  |
| History - Get a list of history | History Get a list of history  |
| LookupDataCustom - Get a list of Lookup Data Customs | LookupDataCustom Get a list of Lookup Data Customs  |
| LookupDataTable - Get a list of LookupDataTable | LookupDataTable Get a list of LookupDataTable  |
| PaymentTerm - Get a list of payment terms | PaymentTerm Get a list of payment terms  |
| Product - Get a list of products | Product Get a list of products  |
| Report - Get a list of reports | Report Get a list of reports  |
| Supplier - Get a list of suppliers | Supplier Get a list of suppliers  |
| TaxCode - Get a list of tax codes | TaxCode Get a list of tax codes  |
| User - Get a list of Roles | User Get a list of Roles  |
| User - Get a list of Users | User Get a list of Users  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Currency - Edit a specific currency (PUT) | Currency Edit a specific currency  |
| Currency - Upsert currencies (PUT) | Currency Key = Code Upsert currencies  |
| Document - Update status and header values of documents by id (PUT) | Document Key = Id Update status and header values of documents by id  |
| Document - Update status of documents by id (PUT) | Document Key = Id Update status of documents by id  |
| Document - Upsert documents (PUT) | Document Key = This information is available in the document type GET endpoint under the metadata segment and configKey is GROUP_ON_COLUMNS Upsert documents  |
| Glcode - Edit a specific gl code (PUT) | Glcode Edit a specific gl code  |
| Glcode - Upsert gl codes (PUT) | Glcode Key = DocumentTypeId + Code Upsert gl codes  |
| LookupDataCustom - Edit a specific Lookup Data Custom (PUT) | LookupDataCustom Edit a specific Lookup Data Custom  |
| LookupDataCustom - Upsert Lookup Data Customs (PUT) | LookupDataCustom Key = LookupDataTableId + DocumentTypeId + Value Upsert Lookup Data Customs  |
| PaymentTerm - Edit a specific payment term (PUT) | PaymentTerm Edit a specific payment term  |
| PaymentTerm - Upsert payment terms (PUT) | PaymentTerm Key = DocumentTypeId + Code Upsert payment terms  |
| Product - Edit a specific product (PUT) | Product Edit a specific product  |
| Product - Upsert products (PUT) | Product Key = DocumentTypeId + Code Upsert products  |
| Supplier - Edit a specific supplier (PUT) | Supplier Edit a specific supplier  |
| Supplier - Upsert suppliers (PUT) | Supplier Key = DocumentTypeId + Code Upsert suppliers  |
| TaxCode - Edit a specific tax code (PUT) | TaxCode Edit a specific tax code  |
| TaxCode - Upsert tax codes (PUT) | TaxCode Key = DocumentTypeId + Code Upsert tax codes  |
| User - Edit a specific User (PUT) | User Edit a specific User  |


