
# human-resouces


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Absences - Create an absence record | Absences Create an absence record  |
| Absences/Absence Attachments - Create an absence attachment | Absences/Absence Attachments Create an absence attachment  |
| Absences/Absence Descriptive Flexfields - Create a flexfield | Absences/Absence Descriptive Flexfields Create a flexfield  |
| Absences/Absence Developer Descriptive Flexfields - Create a flexfield | Absences/Absence Developer Descriptive Flexfields Create a flexfield  |
| Absences/Absence Maternity Details - Create a maternity absence record | Absences/Absence Maternity Details Create a maternity absence record  |
| Allocated Checklists - Allocate a checklist | Allocated Checklists Allocates a checklist to a person using the checklist identifier or checklist name, category, person identifier or person number, and allocation date. Allocate a checklist  |
| Allocated Checklists/Allocated Tasks/Attachments - Create a task attachment | Allocated Checklists/Allocated Tasks/Attachments Create a task attachment  |
| Allocated Checklists/Allocated Tasks/Documents - Create a document task type attachment | Allocated Checklists/Allocated Tasks/Documents Create a document task type attachment  |
| Areas of Responsibility - Create an area of responsibility | Areas of Responsibility Create an area of responsibility  |
| Availability Patterns - Create an availability pattern | Availability Patterns Create an availability pattern  |
| Availability Patterns/Shifts - Create a shift | Availability Patterns/Shifts Create a shift  |
| Availability Patterns/Shifts/Breaks - Create a break | Availability Patterns/Shifts/Breaks Create a break  |
| Benefit Groups - Create a benefit group | Benefit Groups Create a benefit group  |
| Candidate Duplicate Checks - Create a candidate duplicate check | Candidate Duplicate Checks Create a candidate duplicate check  |
| Candidate Job Application Drafts - Create an application draft | Candidate Job Application Drafts Create an application draft  |
| Candidate Job Application Drafts/Attachments of the Job Application Drafts - Create attachments for the application drafts | Candidate Job Application Drafts/Attachments of the Job Application Drafts Create attachments for the application drafts  |
| Candidate Responses to Regulatory Questions - Submit regulatory | Candidate Responses to Regulatory Questions Submit regulatory  |
| Channel Messages - Create a channel message | Channel Messages Create a channel message  |
| Channel Messages/Message Attachments - Create an attachment | Channel Messages/Message Attachments Create an attachment  |
| Check-In Documents - Create a document | Check-In Documents Create a document  |
| Check-In Documents/Questionnaire Responses/Question Responses - Create a response | Check-In Documents/Questionnaire Responses/Question Responses Create a response  |
| Check-In Documents/Questionnaire Responses/Question Responses/Attachments - Create an attachment | Check-In Documents/Questionnaire Responses/Question Responses/Attachments Create an attachment  |
| Document Records - Create a document record | Document Records Create a document record  |
| Document Records/Attachments - Create an attachment | Document Records/Attachments Create an attachment  |
| Document Records/Document Records Descriptive Flexfields - Create a flexfield | Document Records/Document Records Descriptive Flexfields Create a flexfield  |
| Document Records/Document Records Developer Descriptive Flexfields - Create a flexfield | Document Records/Document Records Developer Descriptive Flexfields Create a flexfield  |
| Element Entries - Create an element entry | Element Entries Create an element entry  |
| Element Entries/Element Entry Values - Create an element entry value | Element Entries/Element Entry Values Create an element entry value  |
| Eligibility Object Results - Create an eligibility object result | Eligibility Object Results Create an eligibility object result  |
| Eligibility Objects - Create an eligibility object | Eligibility Objects Create an eligibility object  |
| Eligibility Objects/Eligibility Object Profiles - Create an eligibility object profile | Eligibility Objects/Eligibility Object Profiles Create an eligibility object profile  |
| Email Address Migrations - Create an email address migration | Email Address Migrations Create an email address migration  |
| Employees - Create an employee | Employees Create an employee  |
| Employees/Assignments - Create an assignment | Employees/Assignments Create an assignment  |
| Employees/Assignments/Assignment Descriptive Flexfields - Create a flexfield | Employees/Assignments/Assignment Descriptive Flexfields Create a flexfield  |
| Employees/Assignments/Assignment Extra Information Extensible FlexFields - Create a flexfield | Employees/Assignments/Assignment Extra Information Extensible FlexFields Create a flexfield  |
| Employees/Person Descriptive Flexfields - Create a flexfield | Employees/Person Descriptive Flexfields Create a flexfield  |
| Employees/Person Extra Information Extensible FlexFields - Create a flexfield | Employees/Person Extra Information Extensible FlexFields Create a flexfield  |
| Employees/Photos - Create a photo | Employees/Photos Create a photo  |
| Employees/Roles - Create a role | Employees/Roles Create a role  |
| Incident ID - Incident ID | Incident ID Incident ID  |
| Incident ID/Incident ID - Incident ID | Incident ID/Incident ID Incident ID  |
| Incident ID/Incident ID/Attachments - Create an attachment. | Incident ID/Incident ID/Attachments Create an attachment.  |
| Incident Kiosks - Create an incident | Incident Kiosks Create an incident  |
| Incident Kiosks/Incident Detail Kiosks - Create an incident event | Incident Kiosks/Incident Detail Kiosks Create an incident event  |
| Job Applications - Create a job application | Job Applications Create a job application  |
| Job Applications/Candidate Responses to Questions - Create responses for a questionnaire | Job Applications/Candidate Responses to Questions Create responses for a questionnaire  |
| Job Applications/Candidate Responses to Regulatory Questions - Submit regulatory | Job Applications/Candidate Responses to Regulatory Questions Submit regulatory  |
| Job Applications/Preferred Job Locations - Create a preferred location | Job Applications/Preferred Job Locations Create a preferred location  |
| Job Applications/Schedule Job Interviews - Create a scheduled interview | Job Applications/Schedule Job Interviews Create a scheduled interview  |
| Job Applications/Secondary Job Submissions - Create a secondary job application | Job Applications/Secondary Job Submissions Create a secondary job application  |
| Job Applications/Unscheduled Job Interviews - Create an unscheduled interview request | Job Applications/Unscheduled Job Interviews Create an unscheduled interview request  |
| Learner Learning Records - Create an assignment record | Learner Learning Records Create an assignment record  |
| Learner Learning Records/Active Learner Comments - Create an active learner comment | Learner Learning Records/Active Learner Comments Create an active learner comment  |
| Learner Learning Records/Active Learner Comments/Likes - Create a like | Learner Learning Records/Active Learner Comments/Likes Create a like  |
| Learner Learning Records/Active Learner Comments/Replies - Create a reply | Learner Learning Records/Active Learner Comments/Replies Create a reply  |
| Learner Learning Records/Active Learner Comments/Replies/Likes - Create a like | Learner Learning Records/Active Learner Comments/Replies/Likes Create a like  |
| Learner Learning Records/Assignment Descriptive Flexfields - Create an assignment flexfield | Learner Learning Records/Assignment Descriptive Flexfields Create an assignment flexfield  |
| Learner Learning Records/Completion Details - Create a completion detail | Learner Learning Records/Completion Details Create a completion detail  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Assignment Descriptive Flexfields - Create an assignment flexfield | Learner Learning Records/Completion Details/Other Selected Course Offerings/Assignment Descriptive Flexfields Create an assignment flexfield  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Learning Item Rating Details - Create a learning item rating | Learner Learning Records/Completion Details/Other Selected Course Offerings/Learning Item Rating Details Create a learning item rating  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Assignment Descriptive Flexfields - Create an assignment flexfield | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Assignment Descriptive Flexfields Create an assignment flexfield  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Learning Item Rating Details - Create a learning item rating | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Learning Item Rating Details Create a learning item rating  |
| Learner Learning Records/Completion Details/Selected Course Offerings - Create a selected course offering | Learner Learning Records/Completion Details/Selected Course Offerings Create a selected course offering  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments - Create an active learner comment | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments Create an active learner comment  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Likes - Create a like | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Likes Create a like  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies - Create a reply | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies Create a reply  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies/Likes - Create a like | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies/Likes Create a like  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Assignment Descriptive Flexfields - Create an assignment flexfield | Learner Learning Records/Completion Details/Selected Course Offerings/Assignment Descriptive Flexfields Create an assignment flexfield  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Rating Details - Create a learning item rating | Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Rating Details Create a learning item rating  |
| Learner Learning Records/Learning Item Rating Details - Create a learning item rating | Learner Learning Records/Learning Item Rating Details Create a learning item rating  |
| Learner Learning Records/Other Selected Course Offerings/Assignment Descriptive Flexfields - Create an assignment flexfield | Learner Learning Records/Other Selected Course Offerings/Assignment Descriptive Flexfields Create an assignment flexfield  |
| Learner Learning Records/Other Selected Course Offerings/Learning Item Rating Details - Create a learning item rating | Learner Learning Records/Other Selected Course Offerings/Learning Item Rating Details Create a learning item rating  |
| Learner Learning Records/Primary Selected Course Offerings/Assignment Descriptive Flexfields - Create an assignment flexfield | Learner Learning Records/Primary Selected Course Offerings/Assignment Descriptive Flexfields Create an assignment flexfield  |
| Learner Learning Records/Primary Selected Course Offerings/Learning Item Rating Details - Create a learning item rating | Learner Learning Records/Primary Selected Course Offerings/Learning Item Rating Details Create a learning item rating  |
| Learner Learning Records/Selected Course Offerings - Create a selected course offering | Learner Learning Records/Selected Course Offerings Create a selected course offering  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments - Create an active learner comment | Learner Learning Records/Selected Course Offerings/Active Learner Comments Create an active learner comment  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments/Likes - Create a like | Learner Learning Records/Selected Course Offerings/Active Learner Comments/Likes Create a like  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies - Create a reply | Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies Create a reply  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies/Likes - Create a like | Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies/Likes Create a like  |
| Learner Learning Records/Selected Course Offerings/Assignment Descriptive Flexfields - Create an assignment flexfield | Learner Learning Records/Selected Course Offerings/Assignment Descriptive Flexfields Create an assignment flexfield  |
| Learner Learning Records/Selected Course Offerings/Learning Item Rating Details - Create a learning item rating | Learner Learning Records/Selected Course Offerings/Learning Item Rating Details Create a learning item rating  |
| Learning Content Items - Create a learning content item | Learning Content Items Create a learning content item  |
| Locations V2 - Create a location | Locations V2 Create a location  |
| Locations V2/Addresses - Create a location address | Locations V2/Addresses Create a location address  |
| Locations V2/Attachments - Create an attachment | Locations V2/Attachments Create an attachment  |
| Locations V2/Locations Descriptive Flexfields - Create a flexfield | Locations V2/Locations Descriptive Flexfields Create a flexfield  |
| Locations V2/Locations Extensible Flexfields Container - Create a flexfield container | Locations V2/Locations Extensible Flexfields Container Create a flexfield container  |
| Locations V2/Locations Extensible Flexfields Container/Locations Extensible Flexfields - Create a flexfield | Locations V2/Locations Extensible Flexfields Container/Locations Extensible Flexfields Create a flexfield  |
| Locations V2/Locations Legislative Extensible Flexfields - Create a flexfield | Locations V2/Locations Legislative Extensible Flexfields Create a flexfield  |
| Message Designer Mail Services - Submit a messageDesignerMailService | Message Designer Mail Services Submit a messageDesignerMailService  |
| My Job Referrals in Opportunity Marketplace - Create referrals in opportunity marketplace. | My Job Referrals in Opportunity Marketplace Create referrals in opportunity marketplace.  |
| Payroll Relationships/Payroll Assignments/Assigned Payrolls - Create an assigned payroll | Payroll Relationships/Payroll Assignments/Assigned Payrolls Create an assigned payroll  |
| Person Notes - Create a person note | Person Notes Create a person note  |
| Personal Payment Methods - Create a personal payment method | Personal Payment Methods Create a personal payment method  |
| Questionnaires - Create a questionnaire | Questionnaires Create a questionnaire  |
| Questionnaires/Attachments - Create an attachment | Questionnaires/Attachments Create an attachment  |
| Questionnaires/Sections - Create a section | Questionnaires/Sections Create a section  |
| Questionnaires/Sections/Questions - Create a question | Questionnaires/Sections/Questions Create a question  |
| Questions - Create a question | Questions Create a question  |
| Questions/Answers - Create an answer | Questions/Answers Create an answer  |
| Questions/Answers/Attachments - Create an attachment | Questions/Answers/Attachments Create an attachment  |
| Questions/Attachments - Create an attachment | Questions/Attachments Create an attachment  |
| Questions/Job Family Contexts - Create a job family | Questions/Job Family Contexts Create a job family  |
| Questions/Job Function Contexts - Create a job function | Questions/Job Function Contexts Create a job function  |
| Questions/Location Contexts - Create a location | Questions/Location Contexts Create a location  |
| Questions/Organization Contexts - Create an organization | Questions/Organization Contexts Create an organization  |
| Recruiting Assessment Account Packages - Create the details of all recruiting assessment ac... | Recruiting Assessment Account Packages Create the details of all recruiting assessment account packages.  |
| Recruiting Assessment Account Packages/Packages Included in the Recruiting Assessment Account Packages - Create all packages of recruiting assessment accou... | Recruiting Assessment Account Packages/Packages Included in the Recruiting Assessment Account Packages Create all packages of recruiting assessment account packages.  |
| Recruiting Background Check Account Packages - Post all details of the recruiting background chec... | Recruiting Background Check Account Packages Post all details of the recruiting background check account packages  |
| Recruiting Background Check Account Packages/Packages Included in the Recruiting Background Check Account Packages - Create all packages of recruiting background check... | Recruiting Background Check Account Packages/Packages Included in the Recruiting Background Check Account Packages Create all packages of recruiting background check account packages  |
| Recruiting Background Check Candidate Results - Submit all candidate results | Recruiting Background Check Candidate Results Submit all candidate results  |
| Recruiting Background Check Candidate Results/Services About the Candidate Results - Submit all services of candidate results | Recruiting Background Check Candidate Results/Services About the Candidate Results Submit all services of candidate results  |
| Recruiting Background Check Screening Packages - Submit all details of the screening packages | Recruiting Background Check Screening Packages Submit all details of the screening packages  |
| Recruiting Background Check Screening Packages/Services About the Screening Packages. - Submit all services of the screening packages | Recruiting Background Check Screening Packages/Services About the Screening Packages. Submit all services of the screening packages  |
| Recruiting Campaign Details/Campaign Assets/Campaign Asset Channels - Create a CampaignAssetChannel | Recruiting Campaign Details/Campaign Assets/Campaign Asset Channels Create a CampaignAssetChannel  |
| Recruiting Campaign Email Designs - Update a message design | Recruiting Campaign Email Designs Update a message design  |
| Recruiting Campaign Email Designs/Message Design Metadata - Create all message design metadata | Recruiting Campaign Email Designs/Message Design Metadata Create all message design metadata  |
| Recruiting Campaign Email Designs/Message DesignTypes - Create all message design type | Recruiting Campaign Email Designs/Message DesignTypes Create all message design type  |
| Recruiting Candidate Assessment Results - Submit all details of recruiting candidate assessm... | Recruiting Candidate Assessment Results Submit all details of recruiting candidate assessment results  |
| Recruiting Candidate Assessment Results/Packages of Recruiting Candidate Assessment Results - Submit all packages of recruiting candidate assess... | Recruiting Candidate Assessment Results/Packages of Recruiting Candidate Assessment Results Submit all packages of recruiting candidate assessment results  |
| Recruiting Candidate Extra Information Details - Create an extra information details of a candidate | Recruiting Candidate Extra Information Details Create an extra information details of a candidate  |
| Recruiting Candidate Extra Information Details/Person Extra Information Details - Create an extra information details of a Person | Recruiting Candidate Extra Information Details/Person Extra Information Details Create an extra information details of a Person  |
| Recruiting Candidate PII Data - Create details of all candidate PII data | Recruiting Candidate PII Data Create details of all candidate PII data  |
| Recruiting Candidates - Create a candidate | Recruiting Candidates Create a candidate  |
| Recruiting Candidates/Attachments - Create an attachment | Recruiting Candidates/Attachments Create an attachment  |
| Recruiting Candidates/Candidate Addresses - Create an address | Recruiting Candidates/Candidate Addresses Create an address  |
| Recruiting Candidates/Candidate Phones - Create a phone number | Recruiting Candidates/Candidate Phones Create a phone number  |
| Recruiting Candidates/Education Items - Create an education item | Recruiting Candidates/Education Items Create an education item  |
| Recruiting Candidates/Experience Items - Create an experience item | Recruiting Candidates/Experience Items Create an experience item  |
| Recruiting Candidates/Languages - Create a language | Recruiting Candidates/Languages Create a language  |
| Recruiting Candidates/Licenses and Certificates - Create a license and certificate | Recruiting Candidates/Licenses and Certificates Create a license and certificate  |
| Recruiting Candidates/Skills of the Recruiting Candidates - Submit all recruiting candidates skills | Recruiting Candidates/Skills of the Recruiting Candidates Submit all recruiting candidates skills  |
| Recruiting Candidates/Work Preferences - Create a work preference | Recruiting Candidates/Work Preferences Create a work preference  |
| Recruiting CE Candidate Certifications - Submit details of all the certifications | Recruiting CE Candidate Certifications Submit details of all the certifications  |
| Recruiting CE Candidate Details - Post all details of recruitingCECandidates | Recruiting CE Candidate Details Post all details of recruitingCECandidates  |
| Recruiting CE Candidate Details/Candidate Details with Attachments - Post all details of recruitingCECandidates attachm... | Recruiting CE Candidate Details/Candidate Details with Attachments Post all details of recruitingCECandidates attachments  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data - Create all details of recruitingCECandidates perso... | Recruiting CE Candidate Details/Candidate Details with Extra Information Data Create all details of recruitingCECandidates personExtraInformation  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows - Update an extra information row record for a candi... | Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows Update an extra information row record for a candidate.  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows/Candidate Details with Extra Information Attributes - Create all details of recruitingCECandidates perso... | Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows/Candidate Details with Extra Information Attributes Create all details of recruitingCECandidates personExtraInformation row attributes  |
| Recruiting CE Candidate Details/Candidate Details with PII Data - Create all details of recruitingCECandidates Candi... | Recruiting CE Candidate Details/Candidate Details with PII Data Create all details of recruitingCECandidates CandidatePIIData  |
| Recruiting CE Candidate Education - Submit all the details of the education items of t... | Recruiting CE Candidate Education Submit all the details of the education items of the candidate.  |
| Recruiting CE Candidate Interview Actions - Submit a candidate interview action | Recruiting CE Candidate Interview Actions Submit a candidate interview action  |
| Recruiting CE Candidate Previous Experience - Post all the candidate previous experience items. | Recruiting CE Candidate Previous Experience Post all the candidate previous experience items.  |
| Recruiting CE Candidate Responses to Questions - Post details of all responses | Recruiting CE Candidate Responses to Questions Post details of all responses  |
| Recruiting CE Candidate Site Preferences - Submit all candidate site preferences | Recruiting CE Candidate Site Preferences Submit all candidate site preferences  |
| Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate - Submit all details of candidate site TCP preferenc... | Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate Submit all details of candidate site TCP preferences  |
| Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate/Preferred Job Families - Submit all preferred job families of candidate sit... | Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate/Preferred Job Families Submit all preferred job families of candidate site TCP preferences  |
| Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate/Preferred Locations - Submit all preferred locations of candidate site T... | Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate/Preferred Locations Submit all preferred locations of candidate site TCP preferences  |
| Recruiting CE Candidate Work Preferences - Post the details of all the candidate work prefere... | Recruiting CE Candidate Work Preferences Post the details of all the candidate work preferences  |
| Recruiting CE Job Referral Clicks - Submit all details of requisitions clicks | Recruiting CE Job Referral Clicks Submit all details of requisitions clicks  |
| Recruiting CE Job Referrals - Submit all details of candidate referrals | Recruiting CE Job Referrals Submit all details of candidate referrals  |
| Recruiting CE Job Requisitions/Work Locations Facet - Submit all work location facets | Recruiting CE Job Requisitions/Work Locations Facet Submit all work location facets  |
| Recruiting CE Job Requisitions/Work Locations Facet - Submit all work location facets | Recruiting CE Job Requisitions/Work Locations Facet Submit all work location facets  |
| Recruiting CE Languages - Submit all the language items. | Recruiting CE Languages Submit all the language items.  |
| Recruiting CE Offer Details - Create assessment requests for a job application | Recruiting CE Offer Details Create assessment requests for a job application  |
| Recruiting CE Offer Responses - Submit all offer responses | Recruiting CE Offer Responses Submit all offer responses  |
| Recruiting CE Profile Import Attachments - Submit all profile import attachments | Recruiting CE Profile Import Attachments Submit all profile import attachments  |
| Recruiting CE Prospects - Create a prospect | Recruiting CE Prospects Create a prospect  |
| Recruiting CE Regulatory Configurations - Submit all regulatory configurations | Recruiting CE Regulatory Configurations Submit all regulatory configurations  |
| Recruiting CE Sites - Submit all recruitingCESites | Recruiting CE Sites Submit all recruitingCESites  |
| Recruiting CE Sites/Cookie Consent - Submit all cookie consent in recruitingCESites | Recruiting CE Sites/Cookie Consent Submit all cookie consent in recruitingCESites  |
| Recruiting CE Sites/Filters - Submit all filters in recruitingCESites | Recruiting CE Sites/Filters Submit all filters in recruitingCESites  |
| Recruiting CE Sites/Foot Links - Submit all footer links in recruitingCESites | Recruiting CE Sites/Foot Links Submit all footer links in recruitingCESites  |
| Recruiting CE Sites/Foot Links/Header Link Translations - Submit all header translations in recruitingCESite... | Recruiting CE Sites/Foot Links/Header Link Translations Submit all header translations in recruitingCESites  |
| Recruiting CE Sites/Foot Links/Sublinks - Submit all child links used in the recruiting site | Recruiting CE Sites/Foot Links/Sublinks Submit all child links used in the recruiting site  |
| Recruiting CE Sites/Foot Links/Sublinks/Header Link Translations - Submit all header translations in recruitingCESite... | Recruiting CE Sites/Foot Links/Sublinks/Header Link Translations Submit all header translations in recruitingCESites  |
| Recruiting CE Sites/Header Links - Submit all header links in recruitingCESites | Recruiting CE Sites/Header Links Submit all header links in recruitingCESites  |
| Recruiting CE Sites/Header Links/Header Link Translations - Submit all header translations in recruitingCESite... | Recruiting CE Sites/Header Links/Header Link Translations Submit all header translations in recruitingCESites  |
| Recruiting CE Sites/Header Links/Sublinks - Submit all child links used in the recruiting site | Recruiting CE Sites/Header Links/Sublinks Submit all child links used in the recruiting site  |
| Recruiting CE Sites/Header Links/Sublinks/Header Link Translations - Submit all header translations in recruitingCESite... | Recruiting CE Sites/Header Links/Sublinks/Header Link Translations Submit all header translations in recruitingCESites  |
| Recruiting CE Sites/Languages - Submit all languages in recruitingCESites | Recruiting CE Sites/Languages Submit all languages in recruitingCESites  |
| Recruiting CE Sites/Pages - Submit all pages in recruitingCESites | Recruiting CE Sites/Pages Submit all pages in recruitingCESites  |
| Recruiting CE Sites/Pages/Section Parameters - Submit all section parameters in the sections of p... | Recruiting CE Sites/Pages/Section Parameters Submit all section parameters in the sections of pages  |
| Recruiting CE Sites/Pages/Sections - Submit all sections in the pages of recruitingCESi... | Recruiting CE Sites/Pages/Sections Submit all sections in the pages of recruitingCESites  |
| Recruiting CE Sites/Pages/Sections/Components - Submit all components in the sections of pages | Recruiting CE Sites/Pages/Sections/Components Submit all components in the sections of pages  |
| Recruiting CE Sites/Pages/Sections/Components/Section Parameters - Submit all section parameters in the sections of p... | Recruiting CE Sites/Pages/Sections/Components/Section Parameters Submit all section parameters in the sections of pages  |
| Recruiting CE Sites/Pages/Sections/Section Parameters - Submit all section parameters in the sections of p... | Recruiting CE Sites/Pages/Sections/Section Parameters Submit all section parameters in the sections of pages  |
| Recruiting CE Sites/Settings - Submit all settings in recruitingCESites | Recruiting CE Sites/Settings Submit all settings in recruitingCESites  |
| Recruiting CE Sites/Site Custom Fonts - Submit all custom fonts in recruitingCESites | Recruiting CE Sites/Site Custom Fonts Submit all custom fonts in recruitingCESites  |
| Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings - Submit all custom font settings in recruitingCESit... | Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings Submit all custom font settings in recruitingCESites  |
| Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings/Site Custom Font Setting Files - Submit all custom font setting files in recruiting... | Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings/Site Custom Font Setting Files Submit all custom font setting files in recruitingCESites  |
| Recruiting CE Sites/Site Favicon - Create all favorite icons in recruitingCESites | Recruiting CE Sites/Site Favicon Create all favorite icons in recruitingCESites  |
| Recruiting CE Sites/Site Translations - Create all translations in recruitingCESites | Recruiting CE Sites/Site Translations Create all translations in recruitingCESites  |
| Recruiting CE Sites/Talent Community Sign Up - Post all talent community sign up in recruitingCES... | Recruiting CE Sites/Talent Community Sign Up Post all talent community sign up in recruitingCESites  |
| Recruiting CE Skill Items - Submit all the skill items. | Recruiting CE Skill Items Submit all the skill items.  |
| Recruiting CE Source Tracking - Submit all details of recruitingCESourceTrackings | Recruiting CE Source Tracking Submit all details of recruitingCESourceTrackings  |
| Recruiting CE Tax Credits - Create all tax credit requests for a job applicati... | Recruiting CE Tax Credits Create all tax credit requests for a job application  |
| Recruiting CE Themes - Submit details of all recruitingCEThemes | Recruiting CE Themes Submit details of all recruitingCEThemes  |
| Recruiting CE Themes/Brands in Themes - Submit details of all brands in recruitingCEThemes | Recruiting CE Themes/Brands in Themes Submit details of all brands in recruitingCEThemes  |
| Recruiting CE Themes/Brands in Themes/Brand Translations of Themes - Create details of recruitingCEThemes Brand Transla... | Recruiting CE Themes/Brands in Themes/Brand Translations of Themes Create details of recruitingCEThemes Brand Translations  |
| Recruiting CE Themes/Footer Links in Themes - Submit details of all footer links in recruitingCE... | Recruiting CE Themes/Footer Links in Themes Submit details of all footer links in recruitingCEThemes  |
| Recruiting CE Themes/Footer Links in Themes/Translations Used in Brands in Themes - Submit details of all translations in recruitingCE... | Recruiting CE Themes/Footer Links in Themes/Translations Used in Brands in Themes Submit details of all translations in recruitingCEThemes  |
| Recruiting CE Themes/Header Links in Themes - Submit details of all header links in recruitingCE... | Recruiting CE Themes/Header Links in Themes Submit details of all header links in recruitingCEThemes  |
| Recruiting CE Themes/Header Links in Themes/Translations Used in Brands in Themes - Submit details of all translations in recruitingCE... | Recruiting CE Themes/Header Links in Themes/Translations Used in Brands in Themes Submit details of all translations in recruitingCEThemes  |
| Recruiting CE Themes/Styles in Themes - Submit details of all styles in recruitingCEThemes | Recruiting CE Themes/Styles in Themes Submit details of all styles in recruitingCEThemes  |
| Recruiting CE Themes/Translations in a Template - Submit details of all translations in recruitingCE... | Recruiting CE Themes/Translations in a Template Submit details of all translations in recruitingCETemplates  |
| Recruiting CE User Tracking - Submit all the users tracking information | Recruiting CE User Tracking Submit all the users tracking information  |
| Recruiting CE Verification - Submit all details of verification token | Recruiting CE Verification Submit all details of verification token  |
| Recruiting Internal Candidate Job Applications - Submit all internal candidate job applications | Recruiting Internal Candidate Job Applications Submit all internal candidate job applications  |
| Recruiting Job Requisition Distribution Posting Details - Post details of all job postings | Recruiting Job Requisition Distribution Posting Details Post details of all job postings  |
| Recruiting Job Requisition Distribution Posting Details/Job Posting on the Job Boards - Post details of all job postings on job boards | Recruiting Job Requisition Distribution Posting Details/Job Posting on the Job Boards Post details of all job postings on job boards  |
| Recruiting Job Requisitions - Create a requisition | Recruiting Job Requisitions Create a requisition  |
| Recruiting Job Requisitions/Attachments - Create an  attachment | Recruiting Job Requisitions/Attachments Create an  attachment  |
| Recruiting Job Requisitions/Collaborators - Create a collaborator | Recruiting Job Requisitions/Collaborators Create a collaborator  |
| Recruiting Job Requisitions/Languages - Create a language | Recruiting Job Requisitions/Languages Create a language  |
| Recruiting Job Requisitions/Media Links - Create a media link | Recruiting Job Requisitions/Media Links Create a media link  |
| Recruiting Job Requisitions/Media Links/Media Languages - Create a media link language | Recruiting Job Requisitions/Media Links/Media Languages Create a media link language  |
| Recruiting Job Requisitions/Other Locations - Create an other location | Recruiting Job Requisitions/Other Locations Create an other location  |
| Recruiting Job Requisitions/Other Work Locations - Create a work location | Recruiting Job Requisitions/Other Work Locations Create a work location  |
| Recruiting Job Requisitions/Requisition Descriptive Flexfields - Create a requisition descriptive flexfield | Recruiting Job Requisitions/Requisition Descriptive Flexfields Create a requisition descriptive flexfield  |
| Recruiting LinkedIn Event Notifications - Creates an event for the notification received fro... | Recruiting LinkedIn Event Notifications Creates an event for the notification received from LinkedIn  |
| Recruiting Tax Credit Accounts - Create or update details of all tax credits accoun... | Recruiting Tax Credit Accounts Create or update details of all tax credits accounts  |
| Recruiting Tax Credit Accounts/Recruiting Tax Credit Account Packages - Create or update details of tax credits account pa... | Recruiting Tax Credit Accounts/Recruiting Tax Credit Account Packages Create or update details of tax credits account packages  |
| Recruiting Tax Credits Results - Create results for all tax credits screening | Recruiting Tax Credits Results Create results for all tax credits screening  |
| Recruiting Tax Credits Results/Recruiting Tax Credits Package Results - Create or update results for a tax credit package | Recruiting Tax Credits Results/Recruiting Tax Credits Package Results Create or update results for a tax credit package  |
| Recruiting Tax Credits Results/Recruiting Tax Credits Package Results/Other Eligible Credits for Recruiting Tax Credits Package Results - Create results for other eligible credits for a ta... | Recruiting Tax Credits Results/Recruiting Tax Credits Package Results/Other Eligible Credits for Recruiting Tax Credits Package Results Create results for other eligible credits for a tax credit screening  |
| Salaries - Create a salary for a worker's assignment | Salaries Create a salary for a worker's assignment  |
| Salaries/Salary Components - Create a component | Salaries/Salary Components Create a component  |
| Salaries/Salary Rate Components - Create a rate component | Salaries/Salary Rate Components Create a rate component  |
| Salaries/Salary Simple Components - Create a simple component | Salaries/Salary Simple Components Create a simple component  |
| Schedule Requests - Create a schedule request | Schedule Requests Create a schedule request  |
| Schedule Requests/Schedule Events - Create a schedule event | Schedule Requests/Schedule Events Create a schedule event  |
| Schedule Requests/Schedule Events/Schedule Shift Events - Create a schedule shift event | Schedule Requests/Schedule Events/Schedule Shift Events Create a schedule shift event  |
| Schedule Requests/Schedule Events/Schedule Shift Events/Schedule Shift Attributes - Create a schedule shift attribute | Schedule Requests/Schedule Events/Schedule Shift Events/Schedule Shift Attributes Create a schedule shift attribute  |
| Status Change Requests - Create status change requests | Status Change Requests Create status change requests  |
| Status Change Requests/Status Changes - Create a status change record | Status Change Requests/Status Changes Create a status change record  |
| Talent Person Profiles - Create a person profile | Talent Person Profiles Create a person profile  |
| Talent Person Profiles/Accomplishment Sections - Create an accomplishment section | Talent Person Profiles/Accomplishment Sections Create an accomplishment section  |
| Talent Person Profiles/Accomplishment Sections/Accomplishment Items - Create an accomplishment item | Talent Person Profiles/Accomplishment Sections/Accomplishment Items Create an accomplishment item  |
| Talent Person Profiles/Accomplishment Sections/Accomplishment Items/Accomplishment Items Descriptive Flexfields - Create a flexfield | Talent Person Profiles/Accomplishment Sections/Accomplishment Items/Accomplishment Items Descriptive Flexfields Create a flexfield  |
| Talent Person Profiles/Areas of Expertises - Create an area of expertise | Talent Person Profiles/Areas of Expertises Create an area of expertise  |
| Talent Person Profiles/Areas of Interests - Create an area of interest | Talent Person Profiles/Areas of Interests Create an area of interest  |
| Talent Person Profiles/Attachments - Create an attachment | Talent Person Profiles/Attachments Create an attachment  |
| Talent Person Profiles/Career Preference Sections - Create a career preference section | Talent Person Profiles/Career Preference Sections Create a career preference section  |
| Talent Person Profiles/Career Preference Sections/Career Preference Items - Create a career preference item | Talent Person Profiles/Career Preference Sections/Career Preference Items Create a career preference item  |
| Talent Person Profiles/Career Preference Sections/Career Preference Items/Career Preference Items Descriptive Flexfields - Create a flexfield | Talent Person Profiles/Career Preference Sections/Career Preference Items/Career Preference Items Descriptive Flexfields Create a flexfield  |
| Talent Person Profiles/Certification Sections - Create a certification section | Talent Person Profiles/Certification Sections Create a certification section  |
| Talent Person Profiles/Certification Sections/Certification Items - Create a certification item | Talent Person Profiles/Certification Sections/Certification Items Create a certification item  |
| Talent Person Profiles/Certification Sections/Certification Items/Certification Items Descriptive Flexfields - Create a flexfield | Talent Person Profiles/Certification Sections/Certification Items/Certification Items Descriptive Flexfields Create a flexfield  |
| Talent Person Profiles/Competency Sections - Create a competency section | Talent Person Profiles/Competency Sections Create a competency section  |
| Talent Person Profiles/Competency Sections/Competency Items - Create a competency item | Talent Person Profiles/Competency Sections/Competency Items Create a competency item  |
| Talent Person Profiles/Competency Sections/Competency Items/Competency Items Descriptive Flexfields - Create a flexfield | Talent Person Profiles/Competency Sections/Competency Items/Competency Items Descriptive Flexfields Create a flexfield  |
| Talent Person Profiles/Education Sections - Create an education section | Talent Person Profiles/Education Sections Create an education section  |
| Talent Person Profiles/Education Sections/Education Items - Create an education item | Talent Person Profiles/Education Sections/Education Items Create an education item  |
| Talent Person Profiles/Education Sections/Education Items/Education Items Descriptive Flexfields - Create a flexfield | Talent Person Profiles/Education Sections/Education Items/Education Items Descriptive Flexfields Create a flexfield  |
| Talent Person Profiles/Favorite Links - Create a favorite link | Talent Person Profiles/Favorite Links Create a favorite link  |
| Talent Person Profiles/Honor Sections - Create an honor section | Talent Person Profiles/Honor Sections Create an honor section  |
| Talent Person Profiles/Honor Sections/Honor Items - Create an honor item | Talent Person Profiles/Honor Sections/Honor Items Create an honor item  |
| Talent Person Profiles/Honor Sections/Honor Items/Honor Items Descriptive Flexfields - Create a flexfield | Talent Person Profiles/Honor Sections/Honor Items/Honor Items Descriptive Flexfields Create a flexfield  |
| Talent Person Profiles/Language Sections - Create a language section | Talent Person Profiles/Language Sections Create a language section  |
| Talent Person Profiles/Language Sections/Language Items - Create a language item | Talent Person Profiles/Language Sections/Language Items Create a language item  |
| Talent Person Profiles/Language Sections/Language Items/Language Items Descriptive Flexfields - Create a flexfield | Talent Person Profiles/Language Sections/Language Items/Language Items Descriptive Flexfields Create a flexfield  |
| Talent Person Profiles/Membership Sections - Create a membership section | Talent Person Profiles/Membership Sections Create a membership section  |
| Talent Person Profiles/Membership Sections/Membership Items - Create a membership item | Talent Person Profiles/Membership Sections/Membership Items Create a membership item  |
| Talent Person Profiles/Membership Sections/Membership Items/Membership Items Descriptive Flexfields - Create a flexfield | Talent Person Profiles/Membership Sections/Membership Items/Membership Items Descriptive Flexfields Create a flexfield  |
| Talent Person Profiles/Skill Sections - Create a skill section | Talent Person Profiles/Skill Sections Create a skill section  |
| Talent Person Profiles/Skill Sections/Skill Items - Create a skill item | Talent Person Profiles/Skill Sections/Skill Items Create a skill item  |
| Talent Person Profiles/Skill Sections/Skill Items/Skill Items Descriptive Flexfields - Create a flexfield | Talent Person Profiles/Skill Sections/Skill Items/Skill Items Descriptive Flexfields Create a flexfield  |
| Talent Person Profiles/Special Project Sections - Create a special project section | Talent Person Profiles/Special Project Sections Create a special project section  |
| Talent Person Profiles/Special Project Sections/Special Project Items - Create a special project item | Talent Person Profiles/Special Project Sections/Special Project Items Create a special project item  |
| Talent Person Profiles/Special Project Sections/Special Project Items/Special Project Items Descriptive Flexfields - Create a flexfield | Talent Person Profiles/Special Project Sections/Special Project Items/Special Project Items Descriptive Flexfields Create a flexfield  |
| Talent Person Profiles/Tags - Create a tag | Talent Person Profiles/Tags Create a tag  |
| Talent Person Profiles/Work History Sections - Create a work history section | Talent Person Profiles/Work History Sections Create a work history section  |
| Talent Person Profiles/Work History Sections/Work History Items - Create a work history item | Talent Person Profiles/Work History Sections/Work History Items Create a work history item  |
| Talent Person Profiles/Work History Sections/Work History Items/Work History Items Descriptive Flexfields - Create a flexfield | Talent Person Profiles/Work History Sections/Work History Items/Work History Items Descriptive Flexfields Create a flexfield  |
| Talent Person Profiles/Work Preference Sections - Create a work preference section | Talent Person Profiles/Work Preference Sections Create a work preference section  |
| Talent Person Profiles/Work Preference Sections/Work Preference Items - Create a work preference item | Talent Person Profiles/Work Preference Sections/Work Preference Items Create a work preference item  |
| Talent Person Profiles/Work Preference Sections/Work Preference Items/Work Preference Items Descriptive Flexfields - Create a flexfield | Talent Person Profiles/Work Preference Sections/Work Preference Items/Work Preference Items Descriptive Flexfields Create a flexfield  |
| Talent Ratings/Career Potential - Create a career potential | Talent Ratings/Career Potential Create a career potential  |
| Talent Ratings/Career Potential/Career Potential Descriptive Flexfields - Create a flexfield | Talent Ratings/Career Potential/Career Potential Descriptive Flexfields Create a flexfield  |
| Talent Ratings/Impact Of Losses - Create an impact of loss | Talent Ratings/Impact Of Losses Create an impact of loss  |
| Talent Ratings/Performance Ratings - Create a performance rating | Talent Ratings/Performance Ratings Create a performance rating  |
| Talent Ratings/Performance Ratings/Performance Rating Descriptive Flexfields - Create a flexfield | Talent Ratings/Performance Ratings/Performance Rating Descriptive Flexfields Create a flexfield  |
| Talent Ratings/Risk Of Losses - Create a risk of loss | Talent Ratings/Risk Of Losses Create a risk of loss  |
| Talent Ratings/Risk Of Losses/Risk Of Losses Descriptive Flexfields - Create a flexfield | Talent Ratings/Risk Of Losses/Risk Of Losses Descriptive Flexfields Create a flexfield  |
| Talent Ratings/Talent Scores - Create a talent score | Talent Ratings/Talent Scores Create a talent score  |
| Talent Ratings/Talent Scores/Talent Score Descriptive Flexfields - Create a flexfield | Talent Ratings/Talent Scores/Talent Score Descriptive Flexfields Create a flexfield  |
| Target  Date Migrations - Create a target date migration | Target  Date Migrations Create a target date migration  |
| Time Event Requests - Create a time event request | Time Event Requests Create a time event request  |
| Time Event Requests/Time Events - Create a time event | Time Event Requests/Time Events Create a time event  |
| Time Event Requests/Time Events/Time Event Attributes - Create a time event attribute | Time Event Requests/Time Events/Time Event Attributes Create a time event attribute  |
| Time Record Event Requests - Create a time record event request | Time Record Event Requests Create a time record event request Create a time record event request  |
| Time Record Event Requests - Identify a time card as a favorite. | Time Record Event Requests Identify a time card as a favorite for easy copying of time card data. For example, mark the time card for the time card period June 1, 2019 -- June 7, 2019 as a favorite with the name Regular Worked Time. Identify a time card as a favorite.  |
| Time Record Event Requests - Stop identifying the time card as a favorite. | Time Record Event Requests No longer identify the time card as a favorite, including removing the unique name given to the time card when it was identified as a favorite. Stop identifying the time card as a favorite.  |
| Time Record Event Requests/Time Record Events - Create a time record event | Time Record Event Requests/Time Record Events Create a time record event  |
| Time Record Event Requests/Time Record Events/Time Record Event Attributes - Create a time record event attribute | Time Record Event Requests/Time Record Events/Time Record Event Attributes Create a time record event attribute  |
| Time Record Event Requests/Time Record Events/Time Record Event Messages - Create a time record event message | Time Record Event Requests/Time Record Events/Time Record Event Messages Create a time record event message  |
| Tracking Services - Create a tracking service | Tracking Services Create a tracking service  |
| User Accounts - Create a user account | User Accounts Create a user account  |
| User Accounts/User Account Roles - Create a role assignment | User Accounts/User Account Roles Create a role assignment  |
| Web Clock Events - Create a web clock event | Web Clock Events Create a web clock event  |
| Web Clock Events/Time Card Fields - Create a time card field for a web clock event | Web Clock Events/Time Card Fields Create a time card field for a web clock event  |
| Wellness Activities - Create a wellness activity | Wellness Activities Create a wellness activity  |
| Wellness Activities/Activity Measures - Create an activity measure | Wellness Activities/Activity Measures Create an activity measure  |
| Workers - Create a worker | Workers Create a worker  |
| Workers/Addresses - Create a worker address | Workers/Addresses Create a worker address  |
| Workers/Addresses/Address Descriptive Flexfields - Create a flexfield | Workers/Addresses/Address Descriptive Flexfields Create a flexfield  |
| Workers/Citizenships - Create a worker citizenship | Workers/Citizenships Create a worker citizenship  |
| Workers/Citizenships/Citizenship Descriptive Flexfields - Create a flexfield | Workers/Citizenships/Citizenship Descriptive Flexfields Create a flexfield  |
| Workers/Disabilities - Create a worker disability | Workers/Disabilities Create a worker disability  |
| Workers/Disabilities/Attachments - Create an attachment | Workers/Disabilities/Attachments Create an attachment  |
| Workers/Disabilities/Disabilities Developer Flexfields - Create a flexfield | Workers/Disabilities/Disabilities Developer Flexfields Create a flexfield  |
| Workers/Driver Licenses - Create a worker drivers license | Workers/Driver Licenses Create a worker drivers license  |
| Workers/Driver Licenses/Driver License Descriptive Flexfields - Create a flexfield | Workers/Driver Licenses/Driver License Descriptive Flexfields Create a flexfield  |
| Workers/Driver Licenses/Driver License Developer Flexfields - Create a flexfield | Workers/Driver Licenses/Driver License Developer Flexfields Create a flexfield  |
| Workers/Emails - Create a worker email | Workers/Emails Create a worker email  |
| Workers/Emails/Email Descriptive Flexfields - Create a flexfield | Workers/Emails/Email Descriptive Flexfields Create a flexfield  |
| Workers/Ethnicities - Create a worker ethnicity | Workers/Ethnicities Create a worker ethnicity  |
| Workers/Ethnicities/Ethnicity Descriptive Flexfields - Create a flexfield | Workers/Ethnicities/Ethnicity Descriptive Flexfields Create a flexfield  |
| Workers/External Identifiers - Create a worker external identifier | Workers/External Identifiers Create a worker external identifier  |
| Workers/Legislative Information - Create a worker legislative record | Workers/Legislative Information Create a worker legislative record  |
| Workers/Legislative Information/Legislative Information Descriptive Flexfields - Create a flexfield | Workers/Legislative Information/Legislative Information Descriptive Flexfields Create a flexfield  |
| Workers/Legislative Information/Legislative Information Developer Flexfields - Create a flexfield | Workers/Legislative Information/Legislative Information Developer Flexfields Create a flexfield  |
| Workers/Messages - Create a worker message | Workers/Messages Create a worker message  |
| Workers/Names - Create a worker name | Workers/Names Create a worker name  |
| Workers/National Identifiers - Create a worker national identifier | Workers/National Identifiers Create a worker national identifier  |
| Workers/National Identifiers/National Identifier Descriptive Flexfields - Create a flexfield | Workers/National Identifiers/National Identifier Descriptive Flexfields Create a flexfield  |
| Workers/Other Communication Accounts - Create a worker communication account | Workers/Other Communication Accounts Create a worker communication account  |
| Workers/Other Communication Accounts/Other Communication Account Descriptive Flexfields - Create a flexfield | Workers/Other Communication Accounts/Other Communication Account Descriptive Flexfields Create a flexfield  |
| Workers/Passports - Create a worker passport | Workers/Passports Create a worker passport  |
| Workers/Passports/Passport Descriptive Flexfields - Create a flexfield | Workers/Passports/Passport Descriptive Flexfields Create a flexfield  |
| Workers/Phones - Create a worker phone | Workers/Phones Create a worker phone  |
| Workers/Phones/Phone Descriptive Flexfields - Create a flexfield | Workers/Phones/Phone Descriptive Flexfields Create a flexfield  |
| Workers/Photos - Create a worker photo | Workers/Photos Create a worker photo  |
| Workers/Photos/Photo Descriptive Flexfields - Create a flexfield | Workers/Photos/Photo Descriptive Flexfields Create a flexfield  |
| Workers/Religions - Create a worker religion | Workers/Religions Create a worker religion  |
| Workers/Religions/Religion Descriptive Flexfields - Create a flexfield | Workers/Religions/Religion Descriptive Flexfields Create a flexfield  |
| Workers/Visa Permits - Create a worker visa permit | Workers/Visa Permits Create a worker visa permit  |
| Workers/Visa Permits/Visa Permit Descriptive Flexfields - Create a flexfield | Workers/Visa Permits/Visa Permit Descriptive Flexfields Create a flexfield  |
| Workers/Visa Permits/Visa Permit Developer Flexfields - Create a flexfield | Workers/Visa Permits/Visa Permit Developer Flexfields Create a flexfield  |
| Workers/Work Relationships - Create a worker work relationship | Workers/Work Relationships Create a worker work relationship  |
| Workers/Work Relationships/Assignments - Create a worker assignment | Workers/Work Relationships/Assignments Create a worker assignment  |
| Workers/Work Relationships/Assignments/Assignment Descriptive Flexfields - Create a flexfield | Workers/Work Relationships/Assignments/Assignment Descriptive Flexfields Create a flexfield  |
| Workers/Work Relationships/Assignments/Assignment Developer Flexfields - Create a flexfield | Workers/Work Relationships/Assignments/Assignment Developer Flexfields Create a flexfield  |
| Workers/Work Relationships/Assignments/Assignment Extensible FlexFields - Create a flexfield | Workers/Work Relationships/Assignments/Assignment Extensible FlexFields Create a flexfield  |
| Workers/Work Relationships/Assignments/Grade Steps - Create an assignment grade step | Workers/Work Relationships/Assignments/Grade Steps Create an assignment grade step  |
| Workers/Work Relationships/Assignments/Managers - Create an assignment manager | Workers/Work Relationships/Assignments/Managers Create an assignment manager  |
| Workers/Work Relationships/Assignments/Work Measures - Create an assignment work measure | Workers/Work Relationships/Assignments/Work Measures Create an assignment work measure  |
| Workers/Work Relationships/Contracts - Create a worker contract | Workers/Work Relationships/Contracts Create a worker contract  |
| Workers/Work Relationships/Contracts/Contracts Descriptive Flexfields - Create a flexfield | Workers/Work Relationships/Contracts/Contracts Descriptive Flexfields Create a flexfield  |
| Workers/Work Relationships/Contracts/Contracts Developer Flexfields - Create a flexfield | Workers/Work Relationships/Contracts/Contracts Developer Flexfields Create a flexfield  |
| Workers/Work Relationships/Work Relationship Descriptive Flexfields - Create a flexfield | Workers/Work Relationships/Work Relationship Descriptive Flexfields Create a flexfield  |
| Workers/Work Relationships/Work Relationship Developer Flexfields - Create a flexfield | Workers/Work Relationships/Work Relationship Developer Flexfields Create a flexfield  |
| Workers/Worker Descriptive Flexfields - Create a flexfield | Workers/Worker Descriptive Flexfields Create a flexfield  |
| Workers/Worker Extensible Flexfields - Create a flexfield | Workers/Worker Extensible Flexfields Create a flexfield  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Absences/Absence Attachments - Delete an absence attachment | Absences/Absence Attachments Delete an absence attachment  |
| Allocated Checklists - Delete an allocated checklist | Allocated Checklists Delete an allocated checklist  |
| Allocated Checklists/Allocated Tasks - Delete an allocated task | Allocated Checklists/Allocated Tasks Delete an allocated task  |
| Allocated Checklists/Allocated Tasks/Attachments - Delete a task attachment | Allocated Checklists/Allocated Tasks/Attachments Delete a task attachment  |
| Allocated Checklists/Allocated Tasks/Documents - Delete a document task type attachment | Allocated Checklists/Allocated Tasks/Documents Delete a document task type attachment  |
| Areas of Responsibility - Delete an area of responsibility | Areas of Responsibility Delete an area of responsibility  |
| Availability Patterns - Delete an availability pattern | Availability Patterns Delete an availability pattern  |
| Availability Patterns/Shifts - Delete a shift | Availability Patterns/Shifts Delete a shift  |
| Availability Patterns/Shifts/Breaks - Delete a break | Availability Patterns/Shifts/Breaks Delete a break  |
| Benefit Groups - Delete a benefit group | Benefit Groups Delete a benefit group  |
| Candidate Job Application Drafts - Delete an application draft | Candidate Job Application Drafts Delete an application draft  |
| Candidate Job Application Drafts/Attachments of the Job Application Drafts - Delete an attachment | Candidate Job Application Drafts/Attachments of the Job Application Drafts Delete an attachment  |
| Channel Messages - Delete a channel message | Channel Messages Delete a channel message  |
| Channel Messages/Message Attachments - Delete an attachment | Channel Messages/Message Attachments Delete an attachment  |
| Check-In Documents/Questionnaire Responses/Question Responses/Attachments - Delete an attachment | Check-In Documents/Questionnaire Responses/Question Responses/Attachments Delete an attachment  |
| Document Records - Delete a document record | Document Records Delete a document record  |
| Document Records/Attachments - Delete an attachment | Document Records/Attachments Delete an attachment  |
| Element Entries - Delete an element entry | Element Entries Delete an element entry  |
| Eligibility Object Results - Delete an eligibility object result | Eligibility Object Results Delete an eligibility object result  |
| Eligibility Objects - Delete an eligibility object | Eligibility Objects Delete an eligibility object  |
| Eligibility Objects/Eligibility Object Profiles - Delete an eligibility object profile | Eligibility Objects/Eligibility Object Profiles Delete an eligibility object profile  |
| Employees/Photos - Delete a photo | Employees/Photos Delete a photo  |
| Employees/Roles - Delete a role | Employees/Roles Delete a role  |
| Incident ID/Incident ID/Attachments - Delete an attachment. | Incident ID/Incident ID/Attachments Delete an attachment.  |
| Locations V2 - Delete a location | Locations V2 Delete a location  |
| Locations V2/Addresses - Delete a location address | Locations V2/Addresses Delete a location address  |
| Locations V2/Attachments - Delete an attachment | Locations V2/Attachments Delete an attachment  |
| Locations V2/Locations Descriptive Flexfields - Delete a flexfield | Locations V2/Locations Descriptive Flexfields Delete a flexfield  |
| Locations V2/Locations Extensible Flexfields Container/Locations Extensible Flexfields - Delete a flexfield | Locations V2/Locations Extensible Flexfields Container/Locations Extensible Flexfields Delete a flexfield  |
| Locations V2/Locations Legislative Extensible Flexfields - Delete a flexfield | Locations V2/Locations Legislative Extensible Flexfields Delete a flexfield  |
| My Job Referrals in Opportunity Marketplace - Delete a referral in opportunity marketplace. | My Job Referrals in Opportunity Marketplace Delete a referral in opportunity marketplace.  |
| Payroll Relationships/Payroll Assignments/Assigned Payrolls - Delete an assigned payroll | Payroll Relationships/Payroll Assignments/Assigned Payrolls Delete an assigned payroll  |
| Person Notes - Delete a person note | Person Notes Delete a person note  |
| Personal Payment Methods - Delete a personal payment method | Personal Payment Methods Delete a personal payment method  |
| Questionnaires - Delete a questionnaire | Questionnaires Delete a questionnaire  |
| Questionnaires/Attachments - Delete an attachment | Questionnaires/Attachments Delete an attachment  |
| Questionnaires/Sections - Delete a section | Questionnaires/Sections Delete a section  |
| Questionnaires/Sections/Questions - Delete a question | Questionnaires/Sections/Questions Delete a question  |
| Questions - Delete a question | Questions Delete a question  |
| Questions/Answers - Delete an answer | Questions/Answers Delete an answer  |
| Questions/Attachments - Delete an attachment | Questions/Attachments Delete an attachment  |
| Questions/Job Family Contexts - Delete a job family | Questions/Job Family Contexts Delete a job family  |
| Questions/Job Function Contexts - Delete a job function | Questions/Job Function Contexts Delete a job function  |
| Questions/Location Contexts - Delete a location | Questions/Location Contexts Delete a location  |
| Questions/Organization Contexts - Delete an organization | Questions/Organization Contexts Delete an organization  |
| Recruiting Campaign Details/Campaign Assets/Campaign Asset Channels - Delete a Campaign Asset Channel | Recruiting Campaign Details/Campaign Assets/Campaign Asset Channels Delete a Campaign Asset Channel  |
| Recruiting Campaign Email Designs - Get a message design metadata | Recruiting Campaign Email Designs Get a message design metadata  |
| Recruiting Campaign Email Designs/Message Design Metadata - Get a message design type | Recruiting Campaign Email Designs/Message Design Metadata Get a message design type  |
| Recruiting Campaign Email Designs/Message DesignTypes - Get all message designs | Recruiting Campaign Email Designs/Message DesignTypes Get all message designs  |
| Recruiting Candidate Extra Information Details - Delete an extra info data of the candidate | Recruiting Candidate Extra Information Details Delete an extra info data of the candidate  |
| Recruiting Candidate Extra Information Details/Person Extra Information Details - Delete an extra info data of the person | Recruiting Candidate Extra Information Details/Person Extra Information Details Delete an extra info data of the person  |
| Recruiting Candidates - Delete a candidate | Recruiting Candidates Delete a candidate  |
| Recruiting Candidates/Attachments - Delete an attachment | Recruiting Candidates/Attachments Delete an attachment  |
| Recruiting Candidates/Candidate Addresses - Delete an address | Recruiting Candidates/Candidate Addresses Delete an address  |
| Recruiting Candidates/Candidate Phones - Delete a phone number | Recruiting Candidates/Candidate Phones Delete a phone number  |
| Recruiting Candidates/Education Items - Delete an education item | Recruiting Candidates/Education Items Delete an education item  |
| Recruiting Candidates/Experience Items - Delete an experience item | Recruiting Candidates/Experience Items Delete an experience item  |
| Recruiting Candidates/Languages - Delete a language | Recruiting Candidates/Languages Delete a language  |
| Recruiting Candidates/Licenses and Certificates - Delete a license and certificate | Recruiting Candidates/Licenses and Certificates Delete a license and certificate  |
| Recruiting Candidates/Skills of the Recruiting Candidates - Delete recruiting candidates skill | Recruiting Candidates/Skills of the Recruiting Candidates Delete recruiting candidates skill  |
| Recruiting Candidates/Work Preferences - Delete a work preference | Recruiting Candidates/Work Preferences Delete a work preference  |
| Recruiting CE Candidate Certifications - Delete details of the certification | Recruiting CE Candidate Certifications Delete details of the certification  |
| Recruiting CE Candidate Details - Delete details of a candidate | Recruiting CE Candidate Details Delete details of a candidate  |
| Recruiting CE Candidate Details/Candidate Details with Attachments - Delete details of a recruitingCECandidates attachm... | Recruiting CE Candidate Details/Candidate Details with Attachments Delete details of a recruitingCECandidates attachment  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data - Delete a candidate extra information dictionary | Recruiting CE Candidate Details/Candidate Details with Extra Information Data Delete a candidate extra information dictionary  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows - Delete an extra information row for a candidate. | Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows Delete an extra information row for a candidate.  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows/Candidate Details with Extra Information Attributes - Delete an extra information attribute for candidat... | Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows/Candidate Details with Extra Information Attributes Delete an extra information attribute for candidate extra information row  |
| Recruiting CE Candidate Details/Candidate Details with PII Data - Delete details of a recruitingCECandidates Candida... | Recruiting CE Candidate Details/Candidate Details with PII Data Delete details of a recruitingCECandidates CandidatePIIData  |
| Recruiting CE Candidate Education - Delete the details of the education items of the c... | Recruiting CE Candidate Education Delete the details of the education items of the candidate.  |
| Recruiting CE Candidate Previous Experience - Delete the candidate previous experience items. | Recruiting CE Candidate Previous Experience Delete the candidate previous experience items.  |
| Recruiting CE Candidate Work Preferences - Delete details of a candidate work preference | Recruiting CE Candidate Work Preferences Delete details of a candidate work preference  |
| Recruiting CE Job Requisitions/Work Locations Facet - Delete a work location facet | Recruiting CE Job Requisitions/Work Locations Facet Delete a work location facet  |
| Recruiting CE Job Requisitions/Work Locations Facet - Delete a work location facet | Recruiting CE Job Requisitions/Work Locations Facet Delete a work location facet  |
| Recruiting CE Languages - Delete the language items | Recruiting CE Languages Delete the language items  |
| Recruiting CE Regulatory Configurations - Delete a regulatory configuration | Recruiting CE Regulatory Configurations Delete a regulatory configuration  |
| Recruiting CE Sites - Delete a site | Recruiting CE Sites Delete a site  |
| Recruiting CE Sites/Filters - Delete filters in recruitingCESites | Recruiting CE Sites/Filters Delete filters in recruitingCESites  |
| Recruiting CE Sites/Foot Links - Delete footer links in recruitingCESites | Recruiting CE Sites/Foot Links Delete footer links in recruitingCESites  |
| Recruiting CE Sites/Foot Links/Header Link Translations - Delete a header translation in recruitingCESites | Recruiting CE Sites/Foot Links/Header Link Translations Delete a header translation in recruitingCESites  |
| Recruiting CE Sites/Foot Links/Sublinks - Delete a child link used in the recruiting site | Recruiting CE Sites/Foot Links/Sublinks Delete a child link used in the recruiting site  |
| Recruiting CE Sites/Foot Links/Sublinks/Header Link Translations - Delete a header translation in recruitingCESites | Recruiting CE Sites/Foot Links/Sublinks/Header Link Translations Delete a header translation in recruitingCESites  |
| Recruiting CE Sites/Header Links - Delete a header links in recruitingCESites | Recruiting CE Sites/Header Links Delete a header links in recruitingCESites  |
| Recruiting CE Sites/Header Links/Header Link Translations - Delete a header translation in recruitingCESites | Recruiting CE Sites/Header Links/Header Link Translations Delete a header translation in recruitingCESites  |
| Recruiting CE Sites/Header Links/Sublinks - Delete a child link used in the recruiting site | Recruiting CE Sites/Header Links/Sublinks Delete a child link used in the recruiting site  |
| Recruiting CE Sites/Header Links/Sublinks/Header Link Translations - Delete a header translation in recruitingCESites | Recruiting CE Sites/Header Links/Sublinks/Header Link Translations Delete a header translation in recruitingCESites  |
| Recruiting CE Sites/Languages - Delete a languages in recruitingCESites | Recruiting CE Sites/Languages Delete a languages in recruitingCESites  |
| Recruiting CE Sites/Pages - Delete a page in recruitingCESites | Recruiting CE Sites/Pages Delete a page in recruitingCESites  |
| Recruiting CE Sites/Pages/Section Parameters - Delete a section parameter in the sections of page... | Recruiting CE Sites/Pages/Section Parameters Delete a section parameter in the sections of pages  |
| Recruiting CE Sites/Pages/Sections - Delete a section in the pages of recruitingCESites | Recruiting CE Sites/Pages/Sections Delete a section in the pages of recruitingCESites  |
| Recruiting CE Sites/Pages/Sections/Components - Delete a component in the sections of pages | Recruiting CE Sites/Pages/Sections/Components Delete a component in the sections of pages  |
| Recruiting CE Sites/Pages/Sections/Components/Section Parameters - Delete a section parameter in the sections of page... | Recruiting CE Sites/Pages/Sections/Components/Section Parameters Delete a section parameter in the sections of pages  |
| Recruiting CE Sites/Pages/Sections/Section Parameters - Delete a section parameter in the sections of page... | Recruiting CE Sites/Pages/Sections/Section Parameters Delete a section parameter in the sections of pages  |
| Recruiting CE Sites/Settings - Delete a setting in recruitingCESites | Recruiting CE Sites/Settings Delete a setting in recruitingCESites  |
| Recruiting CE Sites/Site Custom Fonts - Delete a custom font | Recruiting CE Sites/Site Custom Fonts Delete a custom font  |
| Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings - Delete a custom font setting | Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings Delete a custom font setting  |
| Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings/Site Custom Font Setting Files - Delete a custom font setting file | Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings/Site Custom Font Setting Files Delete a custom font setting file  |
| Recruiting CE Sites/Site Favicon - Delete a favorite icon in recruitingCESites | Recruiting CE Sites/Site Favicon Delete a favorite icon in recruitingCESites  |
| Recruiting CE Skill Items - Delete the skill items | Recruiting CE Skill Items Delete the skill items  |
| Recruiting CE Themes/Brands in Themes - Delete details of a brand in recruitingCEThemes | Recruiting CE Themes/Brands in Themes Delete details of a brand in recruitingCEThemes  |
| Recruiting CE Themes/Footer Links in Themes - Delete details of a footer link in recruitingCEThe... | Recruiting CE Themes/Footer Links in Themes Delete details of a footer link in recruitingCEThemes  |
| Recruiting CE Themes/Header Links in Themes - Delete details of a header link in recruitingCEThe... | Recruiting CE Themes/Header Links in Themes Delete details of a header link in recruitingCEThemes  |
| Recruiting CE Themes/Styles in Themes - Delete details of styles in recruitingCEThemes | Recruiting CE Themes/Styles in Themes Delete details of styles in recruitingCEThemes  |
| Recruiting CE Themes/Translations in a Template - Delete recruitingCEThemes brand translation | Recruiting CE Themes/Translations in a Template Delete recruitingCEThemes brand translation  |
| Recruiting Job Requisitions - Delete a requisition | Recruiting Job Requisitions Delete a requisition  |
| Recruiting Job Requisitions/Attachments - Delete an attachment | Recruiting Job Requisitions/Attachments Delete an attachment  |
| Recruiting Job Requisitions/Collaborators - Delete a collaborator | Recruiting Job Requisitions/Collaborators Delete a collaborator  |
| Recruiting Job Requisitions/Languages - Delete a language | Recruiting Job Requisitions/Languages Delete a language  |
| Recruiting Job Requisitions/Media Links - Delete a media link | Recruiting Job Requisitions/Media Links Delete a media link  |
| Recruiting Job Requisitions/Media Links/Media Languages - Delete a media link language | Recruiting Job Requisitions/Media Links/Media Languages Delete a media link language  |
| Recruiting Job Requisitions/Other Locations - Delete an other location | Recruiting Job Requisitions/Other Locations Delete an other location  |
| Recruiting Job Requisitions/Other Work Locations - Delete a work location | Recruiting Job Requisitions/Other Work Locations Delete a work location  |
| Salaries - Delete a salary for a worker's assignment | Salaries Delete a salary for a worker's assignment  |
| Salaries/Salary Components - Delete a component | Salaries/Salary Components Delete a component  |
| Salaries/Salary Rate Components - Delete a rate component | Salaries/Salary Rate Components Delete a rate component  |
| Salaries/Salary Simple Components - Delete a simple component | Salaries/Salary Simple Components Delete a simple component  |
| Talent Person Profiles/Accomplishment Sections/Accomplishment Items - Delete an accomplishment item | Talent Person Profiles/Accomplishment Sections/Accomplishment Items Delete an accomplishment item  |
| Talent Person Profiles/Attachments - Delete an attachment | Talent Person Profiles/Attachments Delete an attachment  |
| Talent Person Profiles/Certification Sections/Certification Items - Delete a certification item | Talent Person Profiles/Certification Sections/Certification Items Delete a certification item  |
| Talent Person Profiles/Competency Sections/Competency Items - Delete a competency item | Talent Person Profiles/Competency Sections/Competency Items Delete a competency item  |
| Talent Person Profiles/Education Sections/Education Items - Delete an education item | Talent Person Profiles/Education Sections/Education Items Delete an education item  |
| Talent Person Profiles/Favorite Links - Delete a favorite link | Talent Person Profiles/Favorite Links Delete a favorite link  |
| Talent Person Profiles/Honor Sections/Honor Items - Delete an honor item | Talent Person Profiles/Honor Sections/Honor Items Delete an honor item  |
| Talent Person Profiles/Language Sections/Language Items - Delete a language item | Talent Person Profiles/Language Sections/Language Items Delete a language item  |
| Talent Person Profiles/Membership Sections/Membership Items - Delete a membership item | Talent Person Profiles/Membership Sections/Membership Items Delete a membership item  |
| Talent Person Profiles/Skill Sections/Skill Items - Delete a skill item | Talent Person Profiles/Skill Sections/Skill Items Delete a skill item  |
| Talent Person Profiles/Special Project Sections/Special Project Items - Delete a special project item | Talent Person Profiles/Special Project Sections/Special Project Items Delete a special project item  |
| Talent Person Profiles/Tags - Delete a tag | Talent Person Profiles/Tags Delete a tag  |
| Talent Person Profiles/Work History Sections/Work History Items - Delete a work history item | Talent Person Profiles/Work History Sections/Work History Items Delete a work history item  |
| Tracking Services - Delete a tracking service | Tracking Services Delete a tracking service  |
| Tracking Services/Authorization Artifacts - Delete an authorization artifact | Tracking Services/Authorization Artifacts Delete an authorization artifact  |
| User Accounts - Delete a user account | User Accounts Delete a user account  |
| User Accounts/User Account Roles - Delete a role assignment | User Accounts/User Account Roles Delete a role assignment  |
| Workers/Addresses - Delete a worker address | Workers/Addresses Delete a worker address  |
| Workers/Citizenships - Delete a worker citizenship | Workers/Citizenships Delete a worker citizenship  |
| Workers/Disabilities - Delete a worker disability | Workers/Disabilities Delete a worker disability  |
| Workers/Disabilities/Attachments - Delete an attachment | Workers/Disabilities/Attachments Delete an attachment  |
| Workers/Driver Licenses - Delete a worker drivers license | Workers/Driver Licenses Delete a worker drivers license  |
| Workers/Emails - Delete a worker email | Workers/Emails Delete a worker email  |
| Workers/Ethnicities - Delete a worker ethnicity | Workers/Ethnicities Delete a worker ethnicity  |
| Workers/External Identifiers - Delete a worker external identifier | Workers/External Identifiers Delete a worker external identifier  |
| Workers/Legislative Information - Delete a worker legislative record | Workers/Legislative Information Delete a worker legislative record  |
| Workers/Messages - Delete a worker message | Workers/Messages Delete a worker message  |
| Workers/Names - Delete a worker name | Workers/Names Delete a worker name  |
| Workers/National Identifiers - Delete a worker national identifier | Workers/National Identifiers Delete a worker national identifier  |
| Workers/Other Communication Accounts - Delete a worker communication account | Workers/Other Communication Accounts Delete a worker communication account  |
| Workers/Passports - Delete a worker passport | Workers/Passports Delete a worker passport  |
| Workers/Phones - Delete a worker phone | Workers/Phones Delete a worker phone  |
| Workers/Photos - Delete a worker photo | Workers/Photos Delete a worker photo  |
| Workers/Religions - Delete a worker religion | Workers/Religions Delete a worker religion  |
| Workers/Visa Permits - Delete a worker visa permit | Workers/Visa Permits Delete a worker visa permit  |
| Workers/Work Relationships/Assignments - Delete a worker assignment | Workers/Work Relationships/Assignments Delete a worker assignment  |
| Workers/Work Relationships/Assignments/Assignment Extensible FlexFields - Delete a flexfield | Workers/Work Relationships/Assignments/Assignment Extensible FlexFields Delete a flexfield  |
| Workers/Work Relationships/Assignments/Grade Steps - Delete an assignment grade step | Workers/Work Relationships/Assignments/Grade Steps Delete an assignment grade step  |
| Workers/Work Relationships/Assignments/Managers - Delete an assignment manager | Workers/Work Relationships/Assignments/Managers Delete an assignment manager  |
| Workers/Work Relationships/Assignments/Work Measures - Delete an assignment work measure | Workers/Work Relationships/Assignments/Work Measures Delete an assignment work measure  |
| Workers/Work Relationships/Contracts - Delete a worker contract | Workers/Work Relationships/Contracts Delete a worker contract  |
| Workers/Worker Extensible Flexfields - Delete a flexfield | Workers/Worker Extensible Flexfields Delete a flexfield  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Absence Business Titles List of Values - Get a business title of a worker | Absence Business Titles List of Values Get a business title of a worker  |
| Absence Plans List of Values - Get an absence plan | Absence Plans List of Values Get an absence plan  |
| Absence Type Reasons List of Values - Get an absence reason | Absence Type Reasons List of Values Get an absence reason  |
| Absence Types List of Values - Get an absence type for a worker | Absence Types List of Values Get an absence type for a worker  |
| Absences - Get an absence record | Absences Get an absence record  |
| Absences/Absence Attachments - Get an absence attachment | Absences/Absence Attachments Get an absence attachment  |
| Absences/Absence Descriptive Flexfields - Get a flexfield | Absences/Absence Descriptive Flexfields Get a flexfield  |
| Absences/Absence Developer Descriptive Flexfields - Get a flexfield | Absences/Absence Developer Descriptive Flexfields Get a flexfield  |
| Absences/Absence Entitlements - Get an entitlement summary record | Absences/Absence Entitlements Get an entitlement summary record  |
| Absences/Absence Entitlements/Absence Entitlement Details - Get an entitlement detail record | Absences/Absence Entitlements/Absence Entitlement Details Get an entitlement detail record  |
| Absences/Absence Maternity Details - Get a maternity absence record | Absences/Absence Maternity Details Get a maternity absence record  |
| Action Reasons List of Values - Get an action reason | Action Reasons List of Values Get an action reason  |
| Actions List of Values - Get an action | Actions List of Values Get an action  |
| Allocated Checklists - Get an allocated checklist | Allocated Checklists Get an allocated checklist  |
| Allocated Checklists/Allocated Tasks - Get an allocated task | Allocated Checklists/Allocated Tasks Get an allocated task  |
| Allocated Checklists/Allocated Tasks/Allocated Tasks FlexFields - Get a flexfield | Allocated Checklists/Allocated Tasks/Allocated Tasks FlexFields Get a flexfield  |
| Allocated Checklists/Allocated Tasks/Attachments - Get a task attachment | Allocated Checklists/Allocated Tasks/Attachments Get a task attachment  |
| Allocated Checklists/Allocated Tasks/Documents - Get a document task type attachment | Allocated Checklists/Allocated Tasks/Documents Get a document task type attachment  |
| Application Flows - Get a job application flow | Application Flows Get a job application flow  |
| Application Flows/Requisition Flexfields - Get a requisition flexfield | Application Flows/Requisition Flexfields Get a requisition flexfield  |
| Application Flows/Sections - Get a section | Application Flows/Sections Get a section  |
| Application Flows/Sections/Pages - Get a page | Application Flows/Sections/Pages Get a page  |
| Application Flows/Sections/Pages/Blocks - Get a block | Application Flows/Sections/Pages/Blocks Get a block  |
| Areas of Responsibility - Get an area of responsibility | Areas of Responsibility Get an area of responsibility  |
| Assignment Status Types List of Values - Get a status type | Assignment Status Types List of Values Get a status type  |
| Availability Patterns - Get an availability pattern | Availability Patterns Get an availability pattern  |
| Availability Patterns/Shifts - Get a shift | Availability Patterns/Shifts Get a shift  |
| Availability Patterns/Shifts/Breaks - Get a break | Availability Patterns/Shifts/Breaks Get a break  |
| Bargaining Units List of Values - Get a bargaining unit | Bargaining Units List of Values Get a bargaining unit  |
| Benefit Enrollment Opportunities - Get an enrollment opportunity | Benefit Enrollment Opportunities Get an enrollment opportunity  |
| Benefit Enrollments - Get an enrollment | Benefit Enrollments Get an enrollment  |
| Benefit Enrollments/Costs - Get a cost | Benefit Enrollments/Costs Get a cost  |
| Benefit Enrollments/Dependents - Get a covered dependent | Benefit Enrollments/Dependents Get a covered dependent  |
| Benefit Enrollments/Providers - Get a plan provider | Benefit Enrollments/Providers Get a plan provider  |
| Benefit Enrollments/Providers/Provider Roles - Get a role | Benefit Enrollments/Providers/Provider Roles Get a role  |
| Benefit Groups - Get a benefit group | Benefit Groups Get a benefit group  |
| Benefit Groups List of Values - Get a benefit group | Benefit Groups List of Values Get a benefit group  |
| Benefit Options List of Values - Get a benefit option | Benefit Options List of Values Get a benefit option  |
| Benefit Plan Types List of Values - Get a plan type | Benefit Plan Types List of Values Get a plan type  |
| Benefit Plans List of Values - Get a benefit plan | Benefit Plans List of Values Get a benefit plan  |
| Benefit Programs List of Values - Get a benefit program | Benefit Programs List of Values Get a benefit program  |
| Business Units List of Values - Get a business unit | Business Units List of Values Get a business unit  |
| Candidate Details - Get details about a candidate | Candidate Details Get details about a candidate  |
| Candidate Duplicate Checks - Get a candidate duplicate check | Candidate Duplicate Checks Get a candidate duplicate check  |
| Candidate Job Application Drafts - Get a draft | Candidate Job Application Drafts Get a draft  |
| Candidate Job Application Drafts/Attachments of the Job Application Drafts - Get an attachment for application drafts | Candidate Job Application Drafts/Attachments of the Job Application Drafts Get an attachment for application drafts  |
| Candidate Responses to Regulatory Questions - Get a regulatory response | Candidate Responses to Regulatory Questions Get a regulatory response  |
| Candidates LOV - Get a metadata detail for the candidate | Candidates LOV Get a metadata detail for the candidate  |
| Channel Messages - Get a channel message | Channel Messages Get a channel message  |
| Channel Messages/Message Attachments - Get an attachment | Channel Messages/Message Attachments Get an attachment  |
| Check-In Documents - Get a document | Check-In Documents Get a document  |
| Check-In Documents/Questionnaire Responses - Get a response | Check-In Documents/Questionnaire Responses Get a response  |
| Check-In Documents/Questionnaire Responses/Question Responses - Get a response | Check-In Documents/Questionnaire Responses/Question Responses Get a response  |
| Check-In Documents/Questionnaire Responses/Question Responses/Attachments - Get an attachment | Check-In Documents/Questionnaire Responses/Question Responses/Attachments Get an attachment  |
| Check-In Templates List of Values - Get a template | Check-In Templates List of Values Get a template  |
| Collective Agreements List of Values - Get a collective agreement | Collective Agreements List of Values Get a collective agreement  |
| Competition Participants List of Values - Get a competition participant | Competition Participants List of Values Get a competition participant  |
| Content Item List of Values - Get a content item | Content Item List of Values Get a content item  |
| Contracts List of Values - Get a contract | Contracts List of Values Get a contract  |
| Cost Centers List of Values - Get a cost center | Cost Centers List of Values Get a cost center  |
| Countries List of Values - Get a country | Countries List of Values Get a country  |
| Department Tree Nodes List of Values - Get a department tree node | Department Tree Nodes List of Values Get a department tree node  |
| Departments List of Values - Get a department | Departments List of Values Get a department  |
| Departments List of Values V2 - Get a department | Departments List of Values V2 Get a department  |
| Disability Organizations List of Values - Get a disability organization | Disability Organizations List of Values Get a disability organization  |
| Document Delivery Preferences - Get a delivery preference for a document record | Document Delivery Preferences Get a delivery preference for a document record  |
| Document Records - Get a document record | Document Records Get a document record  |
| Document Records/Attachments - Get an attachment | Document Records/Attachments Get an attachment  |
| Document Records/Document Records Descriptive Flexfields - Get a flexfield | Document Records/Document Records Descriptive Flexfields Get a flexfield  |
| Document Records/Document Records Developer Descriptive Flexfields - Get a flexfield | Document Records/Document Records Developer Descriptive Flexfields Get a flexfield  |
| Document Types List of Values - Get a document type | Document Types List of Values Get a document type  |
| Element Entries - Get an element entry | Element Entries Get an element entry  |
| Element Entries/Element Entry Values - Get an element entry value | Element Entries/Element Entry Values Get an element entry value  |
| Eligibility Object Results - Get an eligibility object result | Eligibility Object Results Get an eligibility object result  |
| Eligibility Objects - Get an eligibility object | Eligibility Objects Get an eligibility object  |
| Eligibility Objects/Eligibility Object Profiles - Get an eligibility object profile | Eligibility Objects/Eligibility Object Profiles Get an eligibility object profile  |
| Eligibility Profiles List of Values - Get an eligibility profile | Eligibility Profiles List of Values Get an eligibility profile  |
| Eligible Contacts List of Values - Get an eligible Contact | Eligible Contacts List of Values Get an eligible Contact  |
| Eligible Options List of Values - Get a plan option | Eligible Options List of Values Get a plan option  |
| Eligible Plans List of Values - Get an individual compensation plan | Eligible Plans List of Values Get an individual compensation plan  |
| Email Address Migrations - Get an email address migration | Email Address Migrations Get an email address migration  |
| Employees - Get an employee | Employees Get an employee  |
| Employees/Assignments - Get an assignment | Employees/Assignments Get an assignment  |
| Employees/Assignments/Assignment Descriptive Flexfields - Get a flexfield | Employees/Assignments/Assignment Descriptive Flexfields Get a flexfield  |
| Employees/Assignments/Assignment Extra Information Extensible FlexFields - Get a flexfield | Employees/Assignments/Assignment Extra Information Extensible FlexFields Get a flexfield  |
| Employees/Assignments/Employee Representatives - Get a representative | Employees/Assignments/Employee Representatives Get a representative  |
| Employees/Assignments/People Group Key Flexfields - Get a flexfield | Employees/Assignments/People Group Key Flexfields Get a flexfield  |
| Employees/Direct Reports - Get a direct report | Employees/Direct Reports Get a direct report  |
| Employees/Person Descriptive Flexfields - Get a flexfield | Employees/Person Descriptive Flexfields Get a flexfield  |
| Employees/Person Extra Information Extensible FlexFields - Get a flexfield | Employees/Person Extra Information Extensible FlexFields Get a flexfield  |
| Employees/Photos - Get a photo | Employees/Photos Get a photo  |
| Employees/Roles - Get a role | Employees/Roles Get a role  |
| Employees/Visas - Get a visa | Employees/Visas Get a visa  |
| Flow Instances List of Values - Get a payroll flow instance | Flow Instances List of Values Get a payroll flow instance  |
| Flow Parameter List of Values - Get a flow parameter value | Flow Parameter List of Values Get a flow parameter value  |
| Flow Patterns List of Values - Get a flow pattern | Flow Patterns List of Values Get a flow pattern  |
| Geographic Hierarchies List of Values - Get a geographic hierarchy | Geographic Hierarchies List of Values Get a geographic hierarchy  |
| Goal Plan Sets List of Values - Get a goal plan set | Goal Plan Sets List of Values Get a goal plan set  |
| Goal Plans List of Values - Get a goal plan | Goal Plans List of Values Get a goal plan  |
| Grade Ladders - Get a grade ladder | Grade Ladders Get a grade ladder  |
| Grade Ladders List of Values - Get a grade ladder | Grade Ladders List of Values Get a grade ladder  |
| Grade Ladders/Grade Ladder Descriptive Flexfields - Get a flexfield | Grade Ladders/Grade Ladder Descriptive Flexfields Get a flexfield  |
| Grade Ladders/Grades - Get a grade | Grade Ladders/Grades Get a grade  |
| Grade Ladders/Step Rates - Get a step rate | Grade Ladders/Step Rates Get a step rate  |
| Grade Ladders/Step Rates/Step Rate Values - Get a value | Grade Ladders/Step Rates/Step Rate Values Get a value  |
| Grade Rates - Get a grade rate | Grade Rates Get a grade rate  |
| Grade Rates/Rate Values - Get a rate value | Grade Rates/Rate Values Get a rate value  |
| Grade Steps List of Values - Get a grade step | Grade Steps List of Values Get a grade step  |
| Grades - Get a grade | Grades Get a grade  |
| Grades List of Values - Get a grade | Grades List of Values Get a grade  |
| Grades/Grade Customer Flexfields - Get a grade flexfield | Grades/Grade Customer Flexfields Get a grade flexfield  |
| Grades/Grade Steps - Get a grade step | Grades/Grade Steps Get a grade step  |
| HCM Groups List of Values - Get a HCM group | HCM Groups List of Values Get a HCM group  |
| HCM Trees List of Values - Get a hierarchy name | HCM Trees List of Values Get a hierarchy name  |
| Incident ID - Incident ID | Incident ID Incident ID  |
| Incident ID/Incident ID - Incident ID | Incident ID/Incident ID Incident ID  |
| Incident ID/Incident ID/Attachments - Get an attachment. | Incident ID/Incident ID/Attachments Get an attachment.  |
| Incident Kiosks - Get an incident | Incident Kiosks Get an incident  |
| Incident Kiosks/Incident Detail Kiosks - Get an incident event | Incident Kiosks/Incident Detail Kiosks Get an incident event  |
| Job Application Filter Autosuggest List of Values - Get an autosuggest value for a job application fil... | Job Application Filter Autosuggest List of Values Get an autosuggest value for a job application filter.  |
| Job Application Grid View Fields - Get a grid view field. | Job Application Grid View Fields Get a grid view field.  |
| Job Applications - Get a job application | Job Applications Get a job application  |
| Job Applications/Candidate Responses to Questions - Get the details of a response for a questionnaire | Job Applications/Candidate Responses to Questions Get the details of a response for a questionnaire  |
| Job Applications/Candidate Responses to Regulatory Questions - Get a regulatory response | Job Applications/Candidate Responses to Regulatory Questions Get a regulatory response  |
| Job Applications/Preferred Job Locations - Get a preferred location | Job Applications/Preferred Job Locations Get a preferred location  |
| Job Applications/Schedule Job Interviews - Get a scheduled interview | Job Applications/Schedule Job Interviews Get a scheduled interview  |
| Job Applications/Secondary Job Submissions - Get a secondary job application | Job Applications/Secondary Job Submissions Get a secondary job application  |
| Job Applications/Unscheduled Job Interviews - Get a request for an unscheduled interview | Job Applications/Unscheduled Job Interviews Get a request for an unscheduled interview  |
| Job Families - Get a job family | Job Families Get a job family  |
| Job Families List of Values - Get a job family | Job Families List of Values Get a job family  |
| Job Families/Job Family Descriptive Flexfields - Get a flexfield | Job Families/Job Family Descriptive Flexfields Get a flexfield  |
| Jobs - Get a job | Jobs Get a job  |
| Jobs List of Values - Get a job | Jobs List of Values Get a job  |
| Jobs List of Values V2 - Get a job | Jobs List of Values V2 Get a job  |
| Jobs/Job Customer Flexfields - Get a job flexfield | Jobs/Job Customer Flexfields Get a job flexfield  |
| Jobs/Valid Grades - Get a valid grade | Jobs/Valid Grades Get a valid grade  |
| Learner Learning Records - Get an assignment record | Learner Learning Records Get an assignment record  |
| Learner Learning Records/Active Learner Comments - Get an active learner comment | Learner Learning Records/Active Learner Comments Get an active learner comment  |
| Learner Learning Records/Active Learner Comments/Likes - Get a like | Learner Learning Records/Active Learner Comments/Likes Get a like  |
| Learner Learning Records/Active Learner Comments/Replies - Get a reply | Learner Learning Records/Active Learner Comments/Replies Get a reply  |
| Learner Learning Records/Active Learner Comments/Replies/Likes - Get a like | Learner Learning Records/Active Learner Comments/Replies/Likes Get a like  |
| Learner Learning Records/Approval Details - Get an approval detail | Learner Learning Records/Approval Details Get an approval detail  |
| Learner Learning Records/Assigned To Person Details - Get an assignee to person details | Learner Learning Records/Assigned To Person Details Get an assignee to person details  |
| Learner Learning Records/Assigner Person Details - Get an assigner person details | Learner Learning Records/Assigner Person Details Get an assigner person details  |
| Learner Learning Records/Assignment Descriptive Flexfields - Get an assignment flexfield | Learner Learning Records/Assignment Descriptive Flexfields Get an assignment flexfield  |
| Learner Learning Records/Completion Details - Get a completion detail | Learner Learning Records/Completion Details Get a completion detail  |
| Learner Learning Records/Completion Details/Activity Completion Predecessor Hints - Get a activity completion predecessor hint | Learner Learning Records/Completion Details/Activity Completion Predecessor Hints Get a activity completion predecessor hint  |
| Learner Learning Records/Completion Details/Activity Content Attempts - Get a content attempt detail | Learner Learning Records/Completion Details/Activity Content Attempts Get a content attempt detail  |
| Learner Learning Records/Completion Details/Activity Section Completion Predecessor Hints - Get a activity section completion predecessor hint | Learner Learning Records/Completion Details/Activity Section Completion Predecessor Hints Get a activity section completion predecessor hint  |
| Learner Learning Records/Completion Details/Classrooms - Get a classroom detail | Learner Learning Records/Completion Details/Classrooms Get a classroom detail  |
| Learner Learning Records/Completion Details/Classrooms/Attachments - Get an attachment | Learner Learning Records/Completion Details/Classrooms/Attachments Get an attachment  |
| Learner Learning Records/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get a classroom flexfield | Learner Learning Records/Completion Details/Classrooms/Classroom Descriptive Flexfields Get a classroom flexfield  |
| Learner Learning Records/Completion Details/Instructors - Get an instructor | Learner Learning Records/Completion Details/Instructors Get an instructor  |
| Learner Learning Records/Completion Details/Instructors/Instructor Descriptive Flexfields - Get an instructor flexfield | Learner Learning Records/Completion Details/Instructors/Instructor Descriptive Flexfields Get an instructor flexfield  |
| Learner Learning Records/Completion Details/Learning Item Talent Profile Summaries - Get a talent profile summary | Learner Learning Records/Completion Details/Learning Item Talent Profile Summaries Get a talent profile summary  |
| Learner Learning Records/Completion Details/Offered Languages - Get an offered language | Learner Learning Records/Completion Details/Offered Languages Get an offered language  |
| Learner Learning Records/Completion Details/Offered Locations - Get an offering location | Learner Learning Records/Completion Details/Offered Locations Get an offering location  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings - Get a single other selected course offering | Learner Learning Records/Completion Details/Other Selected Course Offerings Get a single other selected course offering  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Assignment Descriptive Flexfields - Get an assignment flexfield | Learner Learning Records/Completion Details/Other Selected Course Offerings/Assignment Descriptive Flexfields Get an assignment flexfield  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details - Get a completion detail | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details Get a completion detail  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Activity Content Attempts - Get a content attempt detail | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Activity Content Attempts Get a content attempt detail  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Classrooms - Get a classroom detail | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Classrooms Get a classroom detail  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Classrooms/Attachments - Get an attachment | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Classrooms/Attachments Get an attachment  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get a classroom flexfield | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields Get a classroom flexfield  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Instructors - Get an instructor | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Instructors Get an instructor  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields - Get an instructor flexfield | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields Get an instructor flexfield  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Offered Languages - Get an offered language | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Offered Languages Get an offered language  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Offered Locations - Get an offering location | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Offered Locations Get an offering location  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Related Materials - Get a related material | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Related Materials Get a related material  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Learning Item Descriptive Flexfields - Get a learning item flexfield | Learner Learning Records/Completion Details/Other Selected Course Offerings/Learning Item Descriptive Flexfields Get a learning item flexfield  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Learning Item Rating Details - Get a learning item rating | Learner Learning Records/Completion Details/Other Selected Course Offerings/Learning Item Rating Details Get a learning item rating  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Offering Descriptive Flexfields - Get an offering flexfield | Learner Learning Records/Completion Details/Other Selected Course Offerings/Offering Descriptive Flexfields Get an offering flexfield  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Transaction Histories - Get a transaction history | Learner Learning Records/Completion Details/Other Selected Course Offerings/Transaction Histories Get a transaction history  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings - Get a primary selected course offering | Learner Learning Records/Completion Details/Primary Selected Course Offerings Get a primary selected course offering  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Assignment Descriptive Flexfields - Get an assignment flexfield | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Assignment Descriptive Flexfields Get an assignment flexfield  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details - Get a completion detail | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details Get a completion detail  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Activity Content Attempts - Get a content attempt detail | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Activity Content Attempts Get a content attempt detail  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Classrooms - Get a classroom detail | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Classrooms Get a classroom detail  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Classrooms/Attachments - Get an attachment | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Classrooms/Attachments Get an attachment  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get a classroom flexfield | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields Get a classroom flexfield  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Instructors - Get an instructor | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Instructors Get an instructor  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields - Get an instructor flexfield | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields Get an instructor flexfield  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Offered Languages - Get an offered language | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Offered Languages Get an offered language  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Offered Locations - Get an offering location | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Offered Locations Get an offering location  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Related Materials - Get a related material | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Related Materials Get a related material  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Learning Item Descriptive Flexfields - Get a learning item flexfield | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Learning Item Descriptive Flexfields Get a learning item flexfield  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Learning Item Rating Details - Get a learning item rating | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Learning Item Rating Details Get a learning item rating  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Offering Descriptive Flexfields - Get an offering flexfield | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Offering Descriptive Flexfields Get an offering flexfield  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Transaction Histories - Get a transaction history | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Transaction Histories Get a transaction history  |
| Learner Learning Records/Completion Details/Related Materials - Get a related material | Learner Learning Records/Completion Details/Related Materials Get a related material  |
| Learner Learning Records/Completion Details/Selected Course Offerings - Get a selected course offering | Learner Learning Records/Completion Details/Selected Course Offerings Get a selected course offering  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments - Get an active learner comment | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments Get an active learner comment  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Likes - Get a like | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Likes Get a like  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies - Get a reply | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies Get a reply  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies/Likes - Get a like | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies/Likes Get a like  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Approval Details - Get an approval detail | Learner Learning Records/Completion Details/Selected Course Offerings/Approval Details Get an approval detail  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Assigner Person Details - Get an assigner person details | Learner Learning Records/Completion Details/Selected Course Offerings/Assigner Person Details Get an assigner person details  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Assignment Descriptive Flexfields - Get an assignment flexfield | Learner Learning Records/Completion Details/Selected Course Offerings/Assignment Descriptive Flexfields Get an assignment flexfield  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details - Get a completion detail | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details Get a completion detail  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Activity Content Attempts - Get a content attempt detail | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Activity Content Attempts Get a content attempt detail  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Classrooms - Get a classroom detail | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Classrooms Get a classroom detail  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Classrooms/Attachments - Get an attachment | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Classrooms/Attachments Get an attachment  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get a classroom flexfield | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields Get a classroom flexfield  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Instructors - Get an instructor | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Instructors Get an instructor  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields - Get an instructor flexfield | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields Get an instructor flexfield  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Offered Languages - Get an offered language | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Offered Languages Get an offered language  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Offered Locations - Get an offering location | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Offered Locations Get an offering location  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Related Materials - Get a related material | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Related Materials Get a related material  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Enrollment Related Materials - Get an enrollment related material | Learner Learning Records/Completion Details/Selected Course Offerings/Enrollment Related Materials Get an enrollment related material  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Descriptive Flexfields - Get a learning item flexfield | Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Descriptive Flexfields Get a learning item flexfield  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Rating Details - Get a learning item rating | Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Rating Details Get a learning item rating  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Related Materials - Get a learning item related material | Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Related Materials Get a learning item related material  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Offering Descriptive Flexfields - Get an offering flexfield | Learner Learning Records/Completion Details/Selected Course Offerings/Offering Descriptive Flexfields Get an offering flexfield  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Classrooms - Get an offering primary classroom | Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Classrooms Get an offering primary classroom  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Classrooms/Attachments - Get an attachment | Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Classrooms/Attachments Get an attachment  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Classrooms/Classroom Descriptive Flexfields - Get a classroom flexfield | Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Classrooms/Classroom Descriptive Flexfields Get a classroom flexfield  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Instructors - Get an offering primary instructor | Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Instructors Get an offering primary instructor  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Instructors/Instructor Descriptive Flexfields - Get an instructor flexfield | Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Instructors/Instructor Descriptive Flexfields Get an instructor flexfield  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Transaction Histories - Get a transaction history | Learner Learning Records/Completion Details/Selected Course Offerings/Transaction Histories Get a transaction history  |
| Learner Learning Records/Completion Details/Selected Course Offerings/User Action Hints - Get a user action hint | Learner Learning Records/Completion Details/Selected Course Offerings/User Action Hints Get a user action hint  |
| Learner Learning Records/Completion Details/User Action Hints - Get a user action hint | Learner Learning Records/Completion Details/User Action Hints Get a user action hint  |
| Learner Learning Records/Completion Summaries - Get a completion summary | Learner Learning Records/Completion Summaries Get a completion summary  |
| Learner Learning Records/Course Descriptive Flexfields - Get a course flexfield | Learner Learning Records/Course Descriptive Flexfields Get a course flexfield  |
| Learner Learning Records/Enrollment Related Materials - Get an enrollment related material | Learner Learning Records/Enrollment Related Materials Get an enrollment related material  |
| Learner Learning Records/Learning Item Descriptive Flexfields - Get a learning item flexfield | Learner Learning Records/Learning Item Descriptive Flexfields Get a learning item flexfield  |
| Learner Learning Records/Learning Item Publisher Person Details - Get a learning item publisher person details | Learner Learning Records/Learning Item Publisher Person Details Get a learning item publisher person details  |
| Learner Learning Records/Learning Item Rating Details - Get a learning item rating | Learner Learning Records/Learning Item Rating Details Get a learning item rating  |
| Learner Learning Records/Learning Item Related Materials - Get a learning item related material | Learner Learning Records/Learning Item Related Materials Get a learning item related material  |
| Learner Learning Records/Learning Outcomes - Get a learning outcome | Learner Learning Records/Learning Outcomes Get a learning outcome  |
| Learner Learning Records/Learning Prerequisites - Get a learning prerequisite | Learner Learning Records/Learning Prerequisites Get a learning prerequisite  |
| Learner Learning Records/Other Selected Course Offerings - Get a single other selected course offering | Learner Learning Records/Other Selected Course Offerings Get a single other selected course offering  |
| Learner Learning Records/Other Selected Course Offerings/Assignment Descriptive Flexfields - Get an assignment flexfield | Learner Learning Records/Other Selected Course Offerings/Assignment Descriptive Flexfields Get an assignment flexfield  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details - Get a completion detail | Learner Learning Records/Other Selected Course Offerings/Completion Details Get a completion detail  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Activity Content Attempts - Get a content attempt detail | Learner Learning Records/Other Selected Course Offerings/Completion Details/Activity Content Attempts Get a content attempt detail  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Classrooms - Get a classroom detail | Learner Learning Records/Other Selected Course Offerings/Completion Details/Classrooms Get a classroom detail  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Classrooms/Attachments - Get an attachment | Learner Learning Records/Other Selected Course Offerings/Completion Details/Classrooms/Attachments Get an attachment  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get a classroom flexfield | Learner Learning Records/Other Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields Get a classroom flexfield  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Instructors - Get an instructor | Learner Learning Records/Other Selected Course Offerings/Completion Details/Instructors Get an instructor  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields - Get an instructor flexfield | Learner Learning Records/Other Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields Get an instructor flexfield  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Offered Languages - Get an offered language | Learner Learning Records/Other Selected Course Offerings/Completion Details/Offered Languages Get an offered language  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Offered Locations - Get an offering location | Learner Learning Records/Other Selected Course Offerings/Completion Details/Offered Locations Get an offering location  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Related Materials - Get a related material | Learner Learning Records/Other Selected Course Offerings/Completion Details/Related Materials Get a related material  |
| Learner Learning Records/Other Selected Course Offerings/Learning Item Descriptive Flexfields - Get a learning item flexfield | Learner Learning Records/Other Selected Course Offerings/Learning Item Descriptive Flexfields Get a learning item flexfield  |
| Learner Learning Records/Other Selected Course Offerings/Learning Item Rating Details - Get a learning item rating | Learner Learning Records/Other Selected Course Offerings/Learning Item Rating Details Get a learning item rating  |
| Learner Learning Records/Other Selected Course Offerings/Offering Descriptive Flexfields - Get an offering flexfield | Learner Learning Records/Other Selected Course Offerings/Offering Descriptive Flexfields Get an offering flexfield  |
| Learner Learning Records/Other Selected Course Offerings/Transaction Histories - Get a transaction history | Learner Learning Records/Other Selected Course Offerings/Transaction Histories Get a transaction history  |
| Learner Learning Records/Past Renewals - Get a past renewal | Learner Learning Records/Past Renewals Get a past renewal  |
| Learner Learning Records/Primary Selected Course Offerings - Get a primary selected course offering | Learner Learning Records/Primary Selected Course Offerings Get a primary selected course offering  |
| Learner Learning Records/Primary Selected Course Offerings/Assignment Descriptive Flexfields - Get an assignment flexfield | Learner Learning Records/Primary Selected Course Offerings/Assignment Descriptive Flexfields Get an assignment flexfield  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details - Get a completion detail | Learner Learning Records/Primary Selected Course Offerings/Completion Details Get a completion detail  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Activity Content Attempts - Get a content attempt detail | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Activity Content Attempts Get a content attempt detail  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Classrooms - Get a classroom detail | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Classrooms Get a classroom detail  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Classrooms/Attachments - Get an attachment | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Classrooms/Attachments Get an attachment  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get a classroom flexfield | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields Get a classroom flexfield  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Instructors - Get an instructor | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Instructors Get an instructor  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields - Get an instructor flexfield | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields Get an instructor flexfield  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Offered Languages - Get an offered language | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Offered Languages Get an offered language  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Offered Locations - Get an offering location | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Offered Locations Get an offering location  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Related Materials - Get a related material | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Related Materials Get a related material  |
| Learner Learning Records/Primary Selected Course Offerings/Learning Item Descriptive Flexfields - Get a learning item flexfield | Learner Learning Records/Primary Selected Course Offerings/Learning Item Descriptive Flexfields Get a learning item flexfield  |
| Learner Learning Records/Primary Selected Course Offerings/Learning Item Rating Details - Get a learning item rating | Learner Learning Records/Primary Selected Course Offerings/Learning Item Rating Details Get a learning item rating  |
| Learner Learning Records/Primary Selected Course Offerings/Offering Descriptive Flexfields - Get an offering flexfield | Learner Learning Records/Primary Selected Course Offerings/Offering Descriptive Flexfields Get an offering flexfield  |
| Learner Learning Records/Primary Selected Course Offerings/Transaction Histories - Get a transaction history | Learner Learning Records/Primary Selected Course Offerings/Transaction Histories Get a transaction history  |
| Learner Learning Records/Selected Course Offerings - Get a selected course offering | Learner Learning Records/Selected Course Offerings Get a selected course offering  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments - Get an active learner comment | Learner Learning Records/Selected Course Offerings/Active Learner Comments Get an active learner comment  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments/Likes - Get a like | Learner Learning Records/Selected Course Offerings/Active Learner Comments/Likes Get a like  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies - Get a reply | Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies Get a reply  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies/Likes - Get a like | Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies/Likes Get a like  |
| Learner Learning Records/Selected Course Offerings/Approval Details - Get an approval detail | Learner Learning Records/Selected Course Offerings/Approval Details Get an approval detail  |
| Learner Learning Records/Selected Course Offerings/Assigner Person Details - Get an assigner person details | Learner Learning Records/Selected Course Offerings/Assigner Person Details Get an assigner person details  |
| Learner Learning Records/Selected Course Offerings/Assignment Descriptive Flexfields - Get an assignment flexfield | Learner Learning Records/Selected Course Offerings/Assignment Descriptive Flexfields Get an assignment flexfield  |
| Learner Learning Records/Selected Course Offerings/Completion Details - Get a completion detail | Learner Learning Records/Selected Course Offerings/Completion Details Get a completion detail  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Activity Content Attempts - Get a content attempt detail | Learner Learning Records/Selected Course Offerings/Completion Details/Activity Content Attempts Get a content attempt detail  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Classrooms - Get a classroom detail | Learner Learning Records/Selected Course Offerings/Completion Details/Classrooms Get a classroom detail  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Classrooms/Attachments - Get an attachment | Learner Learning Records/Selected Course Offerings/Completion Details/Classrooms/Attachments Get an attachment  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get a classroom flexfield | Learner Learning Records/Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields Get a classroom flexfield  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Instructors - Get an instructor | Learner Learning Records/Selected Course Offerings/Completion Details/Instructors Get an instructor  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields - Get an instructor flexfield | Learner Learning Records/Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields Get an instructor flexfield  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Offered Languages - Get an offered language | Learner Learning Records/Selected Course Offerings/Completion Details/Offered Languages Get an offered language  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Offered Locations - Get an offering location | Learner Learning Records/Selected Course Offerings/Completion Details/Offered Locations Get an offering location  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Related Materials - Get a related material | Learner Learning Records/Selected Course Offerings/Completion Details/Related Materials Get a related material  |
| Learner Learning Records/Selected Course Offerings/Enrollment Related Materials - Get an enrollment related material | Learner Learning Records/Selected Course Offerings/Enrollment Related Materials Get an enrollment related material  |
| Learner Learning Records/Selected Course Offerings/Learning Item Descriptive Flexfields - Get a learning item flexfield | Learner Learning Records/Selected Course Offerings/Learning Item Descriptive Flexfields Get a learning item flexfield  |
| Learner Learning Records/Selected Course Offerings/Learning Item Rating Details - Get a learning item rating | Learner Learning Records/Selected Course Offerings/Learning Item Rating Details Get a learning item rating  |
| Learner Learning Records/Selected Course Offerings/Learning Item Related Materials - Get a learning item related material | Learner Learning Records/Selected Course Offerings/Learning Item Related Materials Get a learning item related material  |
| Learner Learning Records/Selected Course Offerings/Offering Descriptive Flexfields - Get an offering flexfield | Learner Learning Records/Selected Course Offerings/Offering Descriptive Flexfields Get an offering flexfield  |
| Learner Learning Records/Selected Course Offerings/Offering Primary Classrooms - Get an offering primary classroom | Learner Learning Records/Selected Course Offerings/Offering Primary Classrooms Get an offering primary classroom  |
| Learner Learning Records/Selected Course Offerings/Offering Primary Classrooms/Attachments - Get an attachment | Learner Learning Records/Selected Course Offerings/Offering Primary Classrooms/Attachments Get an attachment  |
| Learner Learning Records/Selected Course Offerings/Offering Primary Classrooms/Classroom Descriptive Flexfields - Get a classroom flexfield | Learner Learning Records/Selected Course Offerings/Offering Primary Classrooms/Classroom Descriptive Flexfields Get a classroom flexfield  |
| Learner Learning Records/Selected Course Offerings/Offering Primary Instructors - Get an offering primary instructor | Learner Learning Records/Selected Course Offerings/Offering Primary Instructors Get an offering primary instructor  |
| Learner Learning Records/Selected Course Offerings/Offering Primary Instructors/Instructor Descriptive Flexfields - Get an instructor flexfield | Learner Learning Records/Selected Course Offerings/Offering Primary Instructors/Instructor Descriptive Flexfields Get an instructor flexfield  |
| Learner Learning Records/Selected Course Offerings/Transaction Histories - Get a transaction history | Learner Learning Records/Selected Course Offerings/Transaction Histories Get a transaction history  |
| Learner Learning Records/Selected Course Offerings/User Action Hints - Get a user action hint | Learner Learning Records/Selected Course Offerings/User Action Hints Get a user action hint  |
| Learner Learning Records/Specialization Descriptive Flexfields - Get a specialization flexfield | Learner Learning Records/Specialization Descriptive Flexfields Get a specialization flexfield  |
| Learner Learning Records/Transaction Histories - Get a transaction history | Learner Learning Records/Transaction Histories Get a transaction history  |
| Learner Learning Records/User Action Hints - Get a user action hint | Learner Learning Records/User Action Hints Get a user action hint  |
| Learning Content Items - Get a learning content item | Learning Content Items Get a learning content item  |
| Legal Employers List of Values - Get a legal employer | Legal Employers List of Values Get a legal employer  |
| Legislative Data Groups List of Values - Get a legislative data group | Legislative Data Groups List of Values Get a legislative data group  |
| Life Events List of Values - Get a life event | Life Events List of Values Get a life event  |
| Locations - Get a location | Locations Get a location  |
| Locations List of Values - Get a location | Locations List of Values Get a location  |
| Locations List of Values V2 - Get a location | Locations List of Values V2 Get a location  |
| Locations V2 - Get a location | Locations V2 Get a location  |
| Locations V2/Addresses - Get a location address | Locations V2/Addresses Get a location address  |
| Locations V2/Attachments - Get an attachment | Locations V2/Attachments Get an attachment  |
| Locations V2/Locations Descriptive Flexfields - Get a flexfield | Locations V2/Locations Descriptive Flexfields Get a flexfield  |
| Locations V2/Locations Extensible Flexfields Container - Get a flexfield | Locations V2/Locations Extensible Flexfields Container Get a flexfield  |
| Locations V2/Locations Extensible Flexfields Container/Locations Extensible Flexfields - Get a flexfield | Locations V2/Locations Extensible Flexfields Container/Locations Extensible Flexfields Get a flexfield  |
| Locations V2/Locations Legislative Extensible Flexfields - Get a flexfield | Locations V2/Locations Legislative Extensible Flexfields Get a flexfield  |
| Locations/Location Descriptive Flexfields - Get a flexfield | Locations/Location Descriptive Flexfields Get a flexfield  |
| Majors List of Values - Get a major | Majors List of Values Get a major  |
| Message Designer Mail Services - Get a messageDesignerMailService | Message Designer Mail Services Get a messageDesignerMailService  |
| Metadata for Sensitive Personal Information Block - Get a metadata detail for the sensitive personal i... | Metadata for Sensitive Personal Information Block Get a metadata detail for the sensitive personal information block  |
| My Job Referrals in Opportunity Marketplace - Get a referral as displayed in opportunity marketp... | My Job Referrals in Opportunity Marketplace Get a referral as displayed in opportunity marketplace.  |
| Organization Payment Methods List of Values - Get an organization payment method | Organization Payment Methods List of Values Get an organization payment method  |
| Organization Tree Nodes List of Values - Get an organization tree node | Organization Tree Nodes List of Values Get an organization tree node  |
| Organizations - Get an organization | Organizations Get an organization  |
| Organizations/Organization Descriptive Flexfields - Get a flexfield | Organizations/Organization Descriptive Flexfields Get a flexfield  |
| Organizations/Organization Extra Information - Get a flexfield | Organizations/Organization Extra Information Get a flexfield  |
| Payroll Balance Definitions List of Values - Get a payroll balance definition | Payroll Balance Definitions List of Values Get a payroll balance definition  |
| Payroll Definitions List of Values - Get a payroll definition | Payroll Definitions List of Values Get a payroll definition  |
| Payroll Element Definitions List of Values - Get an element definition | Payroll Element Definitions List of Values Get an element definition  |
| Payroll Input Values List of Values - Get a payroll input value | Payroll Input Values List of Values Get a payroll input value  |
| Payroll Relationships - Get a payroll relationship | Payroll Relationships Get a payroll relationship  |
| Payroll Relationships/Payroll Assignments - Get a payroll assignment | Payroll Relationships/Payroll Assignments Get a payroll assignment  |
| Payroll Relationships/Payroll Assignments/Assigned Payrolls - Get an assigned payroll | Payroll Relationships/Payroll Assignments/Assigned Payrolls Get an assigned payroll  |
| Payroll Relationships/Payroll Assignments/Assigned Payrolls/Assigned Payroll Dates - Get an assigned payroll date | Payroll Relationships/Payroll Assignments/Assigned Payrolls/Assigned Payroll Dates Get an assigned payroll date  |
| Payroll Relationships/Payroll Assignments/Payroll Assignment Dates - Get a payroll assignment date | Payroll Relationships/Payroll Assignments/Payroll Assignment Dates Get a payroll assignment date  |
| Payroll Relationships/Payroll Relationship Dates - Get a payroll relationship date | Payroll Relationships/Payroll Relationship Dates Get a payroll relationship date  |
| Payroll Statutory Units List of Values - Get a payroll statutory unit | Payroll Statutory Units List of Values Get a payroll statutory unit  |
| Payroll Time Definitions List of Values - Get a payroll time definition | Payroll Time Definitions List of Values Get a payroll time definition  |
| Payroll Time Periods List of Values - Get a time period | Payroll Time Periods List of Values Get a time period  |
| Payslips - Get a payslip | Payslips Get a payslip  |
| Payslips/Documents - Get a document | Payslips/Documents Get a document  |
| Performance Evaluations - Get a performance document | Performance Evaluations Get a performance document  |
| Performance Evaluations/Roles - Get a role | Performance Evaluations/Roles Get a role  |
| Performance Evaluations/Roles/Participants - Get a participant | Performance Evaluations/Roles/Participants Get a participant  |
| Performance Evaluations/Roles/Participants/Tasks - Get a task | Performance Evaluations/Roles/Participants/Tasks Get a task  |
| Performance Goals - Get a goal | Performance Goals Get a goal  |
| Performance Template Document Names List Of Values - Get a document name | Performance Template Document Names List Of Values Get a document name  |
| Performance Template Task Names List Of Values - Get a task name | Performance Template Task Names List Of Values Get a task name  |
| Person Notes - Get a person note | Person Notes Get a person note  |
| Person Notes Visibility Options - Get a visibility option | Person Notes Visibility Options Get a visibility option  |
| Person Notes Visibility Options List of Values - Get a visibility option | Person Notes Visibility Options List of Values Get a visibility option  |
| Person Types List of Values - Get a person type | Person Types List of Values Get a person type  |
| Personal Payment Methods - Get a personal payment method | Personal Payment Methods Get a personal payment method  |
| Plan Balances - Get a plan balance | Plan Balances Get a plan balance  |
| Plan Balances/Plan Balance Details - Get a plan balance detail | Plan Balances/Plan Balance Details Get a plan balance detail  |
| Plan Balances/Plan Balance Summaries - Get a plan balance summary | Plan Balances/Plan Balance Summaries Get a plan balance summary  |
| Position Tree Nodes List of Values - Get a position tree node | Position Tree Nodes List of Values Get a position tree node  |
| Positions - Get a position | Positions Get a position  |
| Positions List of Values - Get a position | Positions List of Values Get a position  |
| Positions List of Values V2 - Get a position | Positions List of Values V2 Get a position  |
| Positions/Position Descriptive Flexfields - Get a flexfield | Positions/Position Descriptive Flexfields Get a flexfield  |
| Positions/Position Extra Information - Get a flexfield | Positions/Position Extra Information Get a flexfield  |
| Positions/Position Extra Legislative Information - Get a flexfield | Positions/Position Extra Legislative Information Get a flexfield  |
| Positions/Valid Grades - Get a valid grade | Positions/Valid Grades Get a valid grade  |
| Profile Type Sections List of Values - Get a profile type section | Profile Type Sections List of Values Get a profile type section  |
| Profiles List of Values - Get a profile | Profiles List of Values Get a profile  |
| Public Workers - Get a worker | Public Workers Get a worker  |
| Public Workers/Assignments - Get a worker assignment | Public Workers/Assignments Get a worker assignment  |
| Public Workers/Assignments/All Reports - Get a worker report | Public Workers/Assignments/All Reports Get a worker report  |
| Public Workers/Assignments/AllReportsDepartments - Get a department | Public Workers/Assignments/AllReportsDepartments Get a department  |
| Public Workers/Assignments/AllReportsLocations - Get a location | Public Workers/Assignments/AllReportsLocations Get a location  |
| Public Workers/Assignments/DirectReports - Get a direct report | Public Workers/Assignments/DirectReports Get a direct report  |
| Public Workers/Assignments/Employment History - Get a worker employment history | Public Workers/Assignments/Employment History Get a worker employment history  |
| Public Workers/Assignments/Managers - Get a worker manager | Public Workers/Assignments/Managers Get a worker manager  |
| Public Workers/Assignments/Representatives - Get a worker representative | Public Workers/Assignments/Representatives Get a worker representative  |
| Public Workers/Messages - Get a worker message | Public Workers/Messages Get a worker message  |
| Public Workers/Other Communication Accounts - Get a worker communication account | Public Workers/Other Communication Accounts Get a worker communication account  |
| Public Workers/Phones - Get a worker phone | Public Workers/Phones Get a worker phone  |
| Public Workers/Photos - Get a worker photo | Public Workers/Photos Get a worker photo  |
| Question Answers List of Values - Get an answer choice | Question Answers List of Values Get an answer choice  |
| Questionnaire Folders List of Values - Get a questionnaire folder | Questionnaire Folders List of Values Get a questionnaire folder  |
| Questionnaire Questions List of Values - Get a question | Questionnaire Questions List of Values Get a question  |
| Questionnaire Response Types List of Values - Get a response type | Questionnaire Response Types List of Values Get a response type  |
| Questionnaire Subscribers List of Values - Get a subscriber | Questionnaire Subscribers List of Values Get a subscriber  |
| Questionnaire Templates List of Values - Get a questionnaire template | Questionnaire Templates List of Values Get a questionnaire template  |
| Questionnaires - Get a questionnaire | Questionnaires Get a questionnaire  |
| Questionnaires List of Values - Get a questionnaire | Questionnaires List of Values Get a questionnaire  |
| Questionnaires/Attachments - Get an attachment | Questionnaires/Attachments Get an attachment  |
| Questionnaires/Sections - Get a section | Questionnaires/Sections Get a section  |
| Questionnaires/Sections/Questions - Get a question | Questionnaires/Sections/Questions Get a question  |
| Questionnaires/Sections/Questions/Answers - Get an answer choice | Questionnaires/Sections/Questions/Answers Get an answer choice  |
| Questions - Get a question | Questions Get a question  |
| Questions List of Values - Get a question | Questions List of Values Get a question  |
| Questions/Answers - Get an answer | Questions/Answers Get an answer  |
| Questions/Answers/Attachments - Get an attachment | Questions/Answers/Attachments Get an attachment  |
| Questions/Attachments - Get an attachment | Questions/Attachments Get an attachment  |
| Questions/Job Family Contexts - Get a job family | Questions/Job Family Contexts Get a job family  |
| Questions/Job Function Contexts - Get a job function | Questions/Job Function Contexts Get a job function  |
| Questions/Location Contexts - Get a location | Questions/Location Contexts Get a location  |
| Questions/Organization Contexts - Get an organization | Questions/Organization Contexts Get an organization  |
| Rating Levels List of Values - Get a rating level | Rating Levels List of Values Get a rating level  |
| Rating Models List of Values - Get a rating model | Rating Models List of Values Get a rating model  |
| Reasons List of Values - Get a reason | Reasons List of Values Get a reason  |
| Recruiting Agency Candidates - Get a candidate from the recruiting agency. | Recruiting Agency Candidates Get a candidate from the recruiting agency.  |
| Recruiting Agents - Get a recruiting agent | Recruiting Agents Get a recruiting agent  |
| Recruiting Assessment Account Packages - Get details of a recruiting assessment account pac... | Recruiting Assessment Account Packages Get details of a recruiting assessment account package.  |
| Recruiting Assessment Account Packages/Packages Included in the Recruiting Assessment Account Packages - Get a package of the recruiting assessment account... | Recruiting Assessment Account Packages/Packages Included in the Recruiting Assessment Account Packages Get a package of the recruiting assessment account packages.  |
| Recruiting Assessment Partner Candidate Details - Get a recruiting partner candidate details | Recruiting Assessment Partner Candidate Details Get a recruiting partner candidate details  |
| Recruiting Assessment Partner Candidate Details/Address Details in the Recruiting Assessment Partner Candidate Details - Get an address details of the recruiting partner c... | Recruiting Assessment Partner Candidate Details/Address Details in the Recruiting Assessment Partner Candidate Details Get an address details of the recruiting partner candidate  |
| Recruiting Assessment Partner Candidate Details/Address Format in the Recruiting Assessment Partner Candidate Details - Get an address format of the recruiting partner ca... | Recruiting Assessment Partner Candidate Details/Address Format in the Recruiting Assessment Partner Candidate Details Get an address format of the recruiting partner candidate  |
| Recruiting Assessment Partner Candidate Details/Certification Related Information in the Recruiting Partner Candidate Details - Get a certification of the recruiting partner cand... | Recruiting Assessment Partner Candidate Details/Certification Related Information in the Recruiting Partner Candidate Details Get a certification of the recruiting partner candidate  |
| Recruiting Assessment Partner Candidate Details/Educational Qualifications Mentioned in the Recruiting Partner Candidate Details - Get a education of the recruiting partner candidat... | Recruiting Assessment Partner Candidate Details/Educational Qualifications Mentioned in the Recruiting Partner Candidate Details Get a education of the recruiting partner candidate  |
| Recruiting Assessment Partner Candidate Details/National Identifiers of the Candidate in the Recruiting Partner Candidate Details - Get a national identifier of the recruiting partne... | Recruiting Assessment Partner Candidate Details/National Identifiers of the Candidate in the Recruiting Partner Candidate Details Get a national identifier of the recruiting partner candidate  |
| Recruiting Assessment Partner Candidate Details/Previous Employments Recorded in the Recruiting Partner Candidate Details - Get a previous employment of the recruiting partne... | Recruiting Assessment Partner Candidate Details/Previous Employments Recorded in the Recruiting Partner Candidate Details Get a previous employment of the recruiting partner candidate  |
| Recruiting Background Check Account Packages - Get details of a recruiting background check accou... | Recruiting Background Check Account Packages Get details of a recruiting background check account package  |
| Recruiting Background Check Account Packages/Packages Included in the Recruiting Background Check Account Packages - Get packages of a recruiting background check acco... | Recruiting Background Check Account Packages/Packages Included in the Recruiting Background Check Account Packages Get packages of a recruiting background check account package  |
| Recruiting Background Check Candidate Results - Get a candidate result | Recruiting Background Check Candidate Results Get a candidate result  |
| Recruiting Background Check Candidate Results/Services About the Candidate Results - Get services of candidate results | Recruiting Background Check Candidate Results/Services About the Candidate Results Get services of candidate results  |
| Recruiting Background Check Screening Packages - Get details of a screening package | Recruiting Background Check Screening Packages Get details of a screening package  |
| Recruiting Background Check Screening Packages List - Get a background check screening package | Recruiting Background Check Screening Packages List Get a background check screening package  |
| Recruiting Background Check Screening Packages/Services About the Screening Packages. - Get details of a service of the screening package | Recruiting Background Check Screening Packages/Services About the Screening Packages. Get details of a service of the screening package  |
| Recruiting Campaign Details - Get a Campaign Details | Recruiting Campaign Details Get a Campaign Details  |
| Recruiting Campaign Details/Campaign Assets - Get a Campaign Asset | Recruiting Campaign Details/Campaign Assets Get a Campaign Asset  |
| Recruiting Campaign Details/Campaign Assets/Campaign Asset Channels - Get a Campaign Asset Channel | Recruiting Campaign Details/Campaign Assets/Campaign Asset Channels Get a Campaign Asset Channel  |
| Recruiting Campaign Details/Campaign Assets/Campaign Asset Segments - Get campaignAssetSegment | Recruiting Campaign Details/Campaign Assets/Campaign Asset Segments Get campaignAssetSegment  |
| Recruiting Campaign Details/Campaign Goal Responses - Get campaignGoalResponse | Recruiting Campaign Details/Campaign Goal Responses Get campaignGoalResponse  |
| Recruiting Campaign Email Designs - Get a message design | Recruiting Campaign Email Designs Get a message design  |
| Recruiting Campaign Email Designs/Message Design Metadata - Update a message design metadata | Recruiting Campaign Email Designs/Message Design Metadata Update a message design metadata  |
| Recruiting Campaign Email Designs/Message DesignTypes - Update a message design type | Recruiting Campaign Email Designs/Message DesignTypes Update a message design type  |
| Recruiting Candidate Assessment Results - Get a recruiting candidate assessment results | Recruiting Candidate Assessment Results Get a recruiting candidate assessment results  |
| Recruiting Candidate Assessment Results/Packages of Recruiting Candidate Assessment Results - Get a package of recruiting candidate assessment r... | Recruiting Candidate Assessment Results/Packages of Recruiting Candidate Assessment Results Get a package of recruiting candidate assessment results  |
| Recruiting Candidate Experience Extra Information Metadata - Get an extensible flexfield metadata detail | Recruiting Candidate Experience Extra Information Metadata Get an extensible flexfield metadata detail  |
| Recruiting Candidate Experience Extra Information Metadata/Candidate Extra Information Metadata with Attributes - Get an extensible flexfield attribute metadata det... | Recruiting Candidate Experience Extra Information Metadata/Candidate Extra Information Metadata with Attributes Get an extensible flexfield attribute metadata detail  |
| Recruiting Candidate Extra Information Details - Get extra information of a candidate | Recruiting Candidate Extra Information Details Get extra information of a candidate  |
| Recruiting Candidate Extra Information Details/Person Extra Information Details - Get all extra information of a person | Recruiting Candidate Extra Information Details/Person Extra Information Details Get all extra information of a person  |
| Recruiting Candidate PII Data - Get the details of a candidate PII data | Recruiting Candidate PII Data Get the details of a candidate PII data  |
| Recruiting Candidate Pools LOV - Get a candidate pool LOV | Recruiting Candidate Pools LOV Get a candidate pool LOV  |
| Recruiting Candidates - Get a candidate | Recruiting Candidates Get a candidate  |
| Recruiting Candidates/Attachments - Get a attachment | Recruiting Candidates/Attachments Get a attachment  |
| Recruiting Candidates/Candidate Addresses - Get an address | Recruiting Candidates/Candidate Addresses Get an address  |
| Recruiting Candidates/Candidate Phones - Get a phone number | Recruiting Candidates/Candidate Phones Get a phone number  |
| Recruiting Candidates/Education Items - Get an education item | Recruiting Candidates/Education Items Get an education item  |
| Recruiting Candidates/Experience Items - Get an experience item | Recruiting Candidates/Experience Items Get an experience item  |
| Recruiting Candidates/Languages - Get a language | Recruiting Candidates/Languages Get a language  |
| Recruiting Candidates/Licenses and Certificates - Get a license and certificate | Recruiting Candidates/Licenses and Certificates Get a license and certificate  |
| Recruiting Candidates/Skills of the Recruiting Candidates - Get recruiting candidates skill | Recruiting Candidates/Skills of the Recruiting Candidates Get recruiting candidates skill  |
| Recruiting Candidates/Work Preferences - Get a work preference | Recruiting Candidates/Work Preferences Get a work preference  |
| Recruiting Career Site Contact Information Metadata - Get candidate contact information to be entered on... | Recruiting Career Site Contact Information Metadata Get candidate contact information to be entered on the recruiting career site  |
| Recruiting CE Address Formats - Get an address format of countries | Recruiting CE Address Formats Get an address format of countries  |
| Recruiting CE Address Formats/Address Format Details - Get an address format details | Recruiting CE Address Formats/Address Format Details Get an address format details  |
| Recruiting CE Address Formats/Geographical Hierarchies - Get a geographical hierarchy | Recruiting CE Address Formats/Geographical Hierarchies Get a geographical hierarchy  |
| Recruiting CE Address Formats/State Provinces - Get a state and province | Recruiting CE Address Formats/State Provinces Get a state and province  |
| Recruiting CE Auto Suggestions on Search. - Get details of an auto suggestion on search | Recruiting CE Auto Suggestions on Search. Get details of an auto suggestion on search  |
| Recruiting CE Candidate Certifications - Get details of a certification | Recruiting CE Candidate Certifications Get details of a certification  |
| Recruiting CE Candidate Details - Get the details of a candidate | Recruiting CE Candidate Details Get the details of a candidate  |
| Recruiting CE Candidate Details/Candidate Details with Attachments - Get details of a recruitingCECandidates attachment... | Recruiting CE Candidate Details/Candidate Details with Attachments Get details of a recruitingCECandidates attachments  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data - Get extra information dictionary detail of candida... | Recruiting CE Candidate Details/Candidate Details with Extra Information Data Get extra information dictionary detail of candidate  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows - Get an extra information row detail for a candidat... | Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows Get an extra information row detail for a candidate  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows/Candidate Details with Extra Information Attributes - Get an extra information attribute detail defined ... | Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows/Candidate Details with Extra Information Attributes Get an extra information attribute detail defined for the candidate extra information row  |
| Recruiting CE Candidate Details/Candidate Details with PII Data - Get details of a recruitingCECandidates CandidateP... | Recruiting CE Candidate Details/Candidate Details with PII Data Get details of a recruitingCECandidates CandidatePIIData  |
| Recruiting CE Candidate Education - Get an education item of the candidate. | Recruiting CE Candidate Education Get an education item of the candidate.  |
| Recruiting CE Candidate Extra Information Dictionary - Get details of the extra information dictionary | Recruiting CE Candidate Extra Information Dictionary Get details of the extra information dictionary  |
| Recruiting CE Candidate Extra Information Dictionary/Recruiting CE Candidate Extra Information Dictionary List of Values - Get details of the extra information dictionary li... | Recruiting CE Candidate Extra Information Dictionary/Recruiting CE Candidate Extra Information Dictionary List of Values Get details of the extra information dictionary list  |
| Recruiting CE Candidate Interview Actions - Get a candidate interview action | Recruiting CE Candidate Interview Actions Get a candidate interview action  |
| Recruiting CE Candidate Preferences - Get a candidate preference | Recruiting CE Candidate Preferences Get a candidate preference  |
| Recruiting CE Candidate Previous Experience - Get the candidate previous experience item. | Recruiting CE Candidate Previous Experience Get the candidate previous experience item.  |
| Recruiting CE Candidate Profile Blocks Meta Data. - Get a candidate profile blocks meta data | Recruiting CE Candidate Profile Blocks Meta Data. Get a candidate profile blocks meta data  |
| Recruiting CE Candidate Responses to Questions - Get the details of a response | Recruiting CE Candidate Responses to Questions Get the details of a response  |
| Recruiting CE Candidate Resume Parser - Get a single candidate resume | Recruiting CE Candidate Resume Parser Get a single candidate resume  |
| Recruiting CE Candidate Resume Parser - Get a single candidate resume | Recruiting CE Candidate Resume Parser Get a single candidate resume  |
| Recruiting CE Candidate Resume Parser/Attachments - Get a single attachment of the candidate resume | Recruiting CE Candidate Resume Parser/Attachments Get a single attachment of the candidate resume  |
| Recruiting CE Candidate Resume Parser/Attachments - Get a single attachment of the candidate resume | Recruiting CE Candidate Resume Parser/Attachments Get a single attachment of the candidate resume  |
| Recruiting CE Candidate Resume Parser/Educations - Get a single educational details of the candidate ... | Recruiting CE Candidate Resume Parser/Educations Get a single educational details of the candidate resume  |
| Recruiting CE Candidate Resume Parser/Educations - Get a single educational details of the candidate ... | Recruiting CE Candidate Resume Parser/Educations Get a single educational details of the candidate resume  |
| Recruiting CE Candidate Resume Parser/Languages - Get a single language of the candidate resume | Recruiting CE Candidate Resume Parser/Languages Get a single language of the candidate resume  |
| Recruiting CE Candidate Resume Parser/Languages - Get a single language of the candidate resume | Recruiting CE Candidate Resume Parser/Languages Get a single language of the candidate resume  |
| Recruiting CE Candidate Resume Parser/Licenses and Certifications - Get a single license and certification of the cand... | Recruiting CE Candidate Resume Parser/Licenses and Certifications Get a single license and certification of the candidate resume  |
| Recruiting CE Candidate Resume Parser/Licenses and Certifications - Get a single license and certification of the cand... | Recruiting CE Candidate Resume Parser/Licenses and Certifications Get a single license and certification of the candidate resume  |
| Recruiting CE Candidate Resume Parser/Skills - Get a single skill of the candidate resume | Recruiting CE Candidate Resume Parser/Skills Get a single skill of the candidate resume  |
| Recruiting CE Candidate Resume Parser/Skills - Get a single skill of the candidate resume | Recruiting CE Candidate Resume Parser/Skills Get a single skill of the candidate resume  |
| Recruiting CE Candidate Resume Parser/Work Experiences - Get a single work experiences of the candidate res... | Recruiting CE Candidate Resume Parser/Work Experiences Get a single work experiences of the candidate resume  |
| Recruiting CE Candidate Resume Parser/Work Experiences - Get a single work experiences of the candidate res... | Recruiting CE Candidate Resume Parser/Work Experiences Get a single work experiences of the candidate resume  |
| Recruiting CE Candidate Resume Parser/Work Preferences - Get a single work preference of the candidate resu... | Recruiting CE Candidate Resume Parser/Work Preferences Get a single work preference of the candidate resume  |
| Recruiting CE Candidate Resume Parser/Work Preferences - Get a single work preference of the candidate resu... | Recruiting CE Candidate Resume Parser/Work Preferences Get a single work preference of the candidate resume  |
| Recruiting CE Candidate Site Preferences - Get a candidate site preference | Recruiting CE Candidate Site Preferences Get a candidate site preference  |
| Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate - Get a candidate site TCP preference | Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate Get a candidate site TCP preference  |
| Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate/Preferred Job Families - Get a preferred job families of candidate site TCP... | Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate/Preferred Job Families Get a preferred job families of candidate site TCP preferences  |
| Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate/Preferred Locations - Get a preferred locations of candidate site TCP pr... | Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate/Preferred Locations Get a preferred locations of candidate site TCP preferences  |
| Recruiting CE Candidate Work Preferences - Get the details of a candidate work preference | Recruiting CE Candidate Work Preferences Get the details of a candidate work preference  |
| Recruiting CE Dictionaries - Get a recruiting CE dictionary | Recruiting CE Dictionaries Get a recruiting CE dictionary  |
| Recruiting CE Interview Schedules - Get an interview schedule | Recruiting CE Interview Schedules Get an interview schedule  |
| Recruiting CE Interview Schedules/Recruiting CE Interviews - Get details of a scheduled interview | Recruiting CE Interview Schedules/Recruiting CE Interviews Get details of a scheduled interview  |
| Recruiting CE Interviews - Get details of a scheduled interview | Recruiting CE Interviews Get details of a scheduled interview  |
| Recruiting CE Interviews/Schedule Interview Participants - Get the interview schedule for a participant | Recruiting CE Interviews/Schedule Interview Participants Get the interview schedule for a participant  |
| Recruiting CE Job Referral Clicks - Get details of a requisitions click | Recruiting CE Job Referral Clicks Get details of a requisitions click  |
| Recruiting CE Job Referrals - Get a candidate referral | Recruiting CE Job Referrals Get a candidate referral  |
| Recruiting CE Job Referrals/Attachments in Referrals - Get an attachment for candidate referral | Recruiting CE Job Referrals/Attachments in Referrals Get an attachment for candidate referral  |
| Recruiting CE Job Requisition Details - Get a job requisition details pertaining to a publ... | Recruiting CE Job Requisition Details Get a job requisition details pertaining to a published job  |
| Recruiting CE Job Requisition Details - Get a job requisition details pertaining to a publ... | Recruiting CE Job Requisition Details Get a job requisition details pertaining to a published job  |
| Recruiting CE Job Requisition Details/Job Requisition Flexfields - Get a requisition flexfield for the requisition de... | Recruiting CE Job Requisition Details/Job Requisition Flexfields Get a requisition flexfield for the requisition details  |
| Recruiting CE Job Requisition Details/Job Requisition Flexfields - Get a requisition flexfield for the requisition de... | Recruiting CE Job Requisition Details/Job Requisition Flexfields Get a requisition flexfield for the requisition details  |
| Recruiting CE Job Requisition Details/Media - Get a media used in the requisition template detai... | Recruiting CE Job Requisition Details/Media Get a media used in the requisition template details preview  |
| Recruiting CE Job Requisition Details/Media - Get a media used in the requisition template detai... | Recruiting CE Job Requisition Details/Media Get a media used in the requisition template details preview  |
| Recruiting CE Job Requisition Details/Other Work Locations - Get other work location for the requisition templa... | Recruiting CE Job Requisition Details/Other Work Locations Get other work location for the requisition template details previews  |
| Recruiting CE Job Requisition Details/Other Work Locations - Get other work location for the requisition templa... | Recruiting CE Job Requisition Details/Other Work Locations Get other work location for the requisition template details previews  |
| Recruiting CE Job Requisition Details/Primary Location Coordinates - Get a primary location coordinates for the requisi... | Recruiting CE Job Requisition Details/Primary Location Coordinates Get a primary location coordinates for the requisition details  |
| Recruiting CE Job Requisition Details/Secondary Locations - Get a secondary location for the requisition templ... | Recruiting CE Job Requisition Details/Secondary Locations Get a secondary location for the requisition template details previews  |
| Recruiting CE Job Requisition Details/Secondary Locations - Get a secondary location for the requisition templ... | Recruiting CE Job Requisition Details/Secondary Locations Get a secondary location for the requisition template details previews  |
| Recruiting CE Job Requisition Details/Work Locations - Get a work location for the requisition template d... | Recruiting CE Job Requisition Details/Work Locations Get a work location for the requisition template details previews  |
| Recruiting CE Job Requisition Details/Work Locations - Get a work location for the requisition template d... | Recruiting CE Job Requisition Details/Work Locations Get a work location for the requisition template details previews  |
| Recruiting CE Job Requisitions - Get a job requisition | Recruiting CE Job Requisitions Get a job requisition  |
| Recruiting CE Job Requisitions - Get a job requisition | Recruiting CE Job Requisitions Get a job requisition  |
| Recruiting CE Job Requisitions/Categories Facet - Get a categories facet | Recruiting CE Job Requisitions/Categories Facet Get a categories facet  |
| Recruiting CE Job Requisitions/Categories Facet - Get a categories facet | Recruiting CE Job Requisitions/Categories Facet Get a categories facet  |
| Recruiting CE Job Requisitions/Flexfields Facet - Get a flexfields facet | Recruiting CE Job Requisitions/Flexfields Facet Get a flexfields facet  |
| Recruiting CE Job Requisitions/Flexfields Facet/Flexfield Facet Values - Get a flexfields facet values | Recruiting CE Job Requisitions/Flexfields Facet/Flexfield Facet Values Get a flexfields facet values  |
| Recruiting CE Job Requisitions/Locations Facet - Get a location facet | Recruiting CE Job Requisitions/Locations Facet Get a location facet  |
| Recruiting CE Job Requisitions/Locations Facet - Get a location facet | Recruiting CE Job Requisitions/Locations Facet Get a location facet  |
| Recruiting CE Job Requisitions/Organizations Facet - Get an organization facet | Recruiting CE Job Requisitions/Organizations Facet Get an organization facet  |
| Recruiting CE Job Requisitions/Posting Dates Facet - Get a posting date facet | Recruiting CE Job Requisitions/Posting Dates Facet Get a posting date facet  |
| Recruiting CE Job Requisitions/Posting Dates Facet - Get a posting date facet | Recruiting CE Job Requisitions/Posting Dates Facet Get a posting date facet  |
| Recruiting CE Job Requisitions/Requisition List - Get a requisition list | Recruiting CE Job Requisitions/Requisition List Get a requisition list  |
| Recruiting CE Job Requisitions/Requisition List - Get a requisition list | Recruiting CE Job Requisitions/Requisition List Get a requisition list  |
| Recruiting CE Job Requisitions/Requisition List/Other Work Locations - Get other work location for the requisition templa... | Recruiting CE Job Requisitions/Requisition List/Other Work Locations Get other work location for the requisition template details previews  |
| Recruiting CE Job Requisitions/Requisition List/Other Work Locations - Get other work location for the requisition templa... | Recruiting CE Job Requisitions/Requisition List/Other Work Locations Get other work location for the requisition template details previews  |
| Recruiting CE Job Requisitions/Requisition List/Primary Location Coordinates - Get a primary location coordinates for the requisi... | Recruiting CE Job Requisitions/Requisition List/Primary Location Coordinates Get a primary location coordinates for the requisition details  |
| Recruiting CE Job Requisitions/Requisition List/Secondary Locations - Get a secondary location for the requisition templ... | Recruiting CE Job Requisitions/Requisition List/Secondary Locations Get a secondary location for the requisition template details previews  |
| Recruiting CE Job Requisitions/Requisition List/Secondary Locations - Get a secondary location for the requisition templ... | Recruiting CE Job Requisitions/Requisition List/Secondary Locations Get a secondary location for the requisition template details previews  |
| Recruiting CE Job Requisitions/Requisition List/Work Locations - Get a work location for the requisition template d... | Recruiting CE Job Requisitions/Requisition List/Work Locations Get a work location for the requisition template details previews  |
| Recruiting CE Job Requisitions/Requisition List/Work Locations - Get a work location for the requisition template d... | Recruiting CE Job Requisitions/Requisition List/Work Locations Get a work location for the requisition template details previews  |
| Recruiting CE Job Requisitions/Requisition Locations Coordinates - Get a requisition locations coordinates | Recruiting CE Job Requisitions/Requisition Locations Coordinates Get a requisition locations coordinates  |
| Recruiting CE Job Requisitions/Requisition Locations Coordinates/Location Coordinates - Get a location coordinates | Recruiting CE Job Requisitions/Requisition Locations Coordinates/Location Coordinates Get a location coordinates  |
| Recruiting CE Job Requisitions/Titles Facet - Get a titles facet | Recruiting CE Job Requisitions/Titles Facet Get a titles facet  |
| Recruiting CE Job Requisitions/Titles Facet - Get a titles facet | Recruiting CE Job Requisitions/Titles Facet Get a titles facet  |
| Recruiting CE Job Requisitions/Work Locations Facet - Get a work location facet | Recruiting CE Job Requisitions/Work Locations Facet Get a work location facet  |
| Recruiting CE Job Requisitions/Work Locations Facet - Get a work location facet | Recruiting CE Job Requisitions/Work Locations Facet Get a work location facet  |
| Recruiting CE Languages - Get a language item for the candidate. | Recruiting CE Languages Get a language item for the candidate.  |
| Recruiting CE Name Styles - Get a recruiting CE name style | Recruiting CE Name Styles Get a recruiting CE name style  |
| Recruiting CE Offer Details - Get details of a job offer | Recruiting CE Offer Details Get details of a job offer  |
| Recruiting CE Offer Details - Get the details of the recruiting candidate assess... | Recruiting CE Offer Details Get the details of the recruiting candidate assessment  |
| Recruiting CE Offer Details/Attachments - Get details of an offer attachment | Recruiting CE Offer Details/Attachments Get details of an offer attachment  |
| Recruiting CE Offer Details/Candidate Reasons to Decline the Job Offer - Get a reason that candidates can provide while dec... | Recruiting CE Offer Details/Candidate Reasons to Decline the Job Offer Get a reason that candidates can provide while declining the job offer.  |
| Recruiting CE Offer Details/Job Offer Assignments - Get the type of job assignment | Recruiting CE Offer Details/Job Offer Assignments Get the type of job assignment  |
| Recruiting CE Offer Details/Job Offer Letters - Get a job offer letter | Recruiting CE Offer Details/Job Offer Letters Get a job offer letter  |
| Recruiting CE Offer Details/Media Links in the Job Offers - Get the media link URL | Recruiting CE Offer Details/Media Links in the Job Offers Get the media link URL  |
| Recruiting CE Offer Responses - Get an offer response | Recruiting CE Offer Responses Get an offer response  |
| Recruiting CE Profile Import Attachments - Get a profile import attachment | Recruiting CE Profile Import Attachments Get a profile import attachment  |
| Recruiting CE Profile Import Configurations - Get details of a profile import configuration | Recruiting CE Profile Import Configurations Get details of a profile import configuration  |
| Recruiting CE Prospects - Get a prospect | Recruiting CE Prospects Get a prospect  |
| Recruiting CE Questions - Get details of a question | Recruiting CE Questions Get details of a question  |
| Recruiting CE Questions/Answers to the Questions - Get details of a answer to the question | Recruiting CE Questions/Answers to the Questions Get details of a answer to the question  |
| Recruiting CE Regulatory Configurations - Get a secured regulatory configuration | Recruiting CE Regulatory Configurations Get a secured regulatory configuration  |
| Recruiting CE Regulatory Configurations - Get a secured regulatory configuration | Recruiting CE Regulatory Configurations Get a secured regulatory configuration  |
| Recruiting CE Sites - Get a site | Recruiting CE Sites Get a site  |
| Recruiting CE Sites/Cookie Consent - Get a cookie consent in recruitingCESites | Recruiting CE Sites/Cookie Consent Get a cookie consent in recruitingCESites  |
| Recruiting CE Sites/Cookie Consent/Translations - Get translations of cookie consent in recruitingCE... | Recruiting CE Sites/Cookie Consent/Translations Get translations of cookie consent in recruitingCESites  |
| Recruiting CE Sites/Filters - Get a filter in recruitingCESites | Recruiting CE Sites/Filters Get a filter in recruitingCESites  |
| Recruiting CE Sites/Foot Links - Get a footer links in recruitingCESites | Recruiting CE Sites/Foot Links Get a footer links in recruitingCESites  |
| Recruiting CE Sites/Foot Links/Header Link Translations - Get a header translation in recruitingCESites | Recruiting CE Sites/Foot Links/Header Link Translations Get a header translation in recruitingCESites  |
| Recruiting CE Sites/Foot Links/Sublinks - Get a child link used in the recruiting site | Recruiting CE Sites/Foot Links/Sublinks Get a child link used in the recruiting site  |
| Recruiting CE Sites/Foot Links/Sublinks/Header Link Translations - Get a header translation in recruitingCESites | Recruiting CE Sites/Foot Links/Sublinks/Header Link Translations Get a header translation in recruitingCESites  |
| Recruiting CE Sites/Header Links - Get a header link in recruitingCESites | Recruiting CE Sites/Header Links Get a header link in recruitingCESites  |
| Recruiting CE Sites/Header Links/Header Link Translations - Get a header translation in recruitingCESites | Recruiting CE Sites/Header Links/Header Link Translations Get a header translation in recruitingCESites  |
| Recruiting CE Sites/Header Links/Sublinks - Get a child link used in the recruiting site | Recruiting CE Sites/Header Links/Sublinks Get a child link used in the recruiting site  |
| Recruiting CE Sites/Header Links/Sublinks/Header Link Translations - Get a header translation in recruitingCESites | Recruiting CE Sites/Header Links/Sublinks/Header Link Translations Get a header translation in recruitingCESites  |
| Recruiting CE Sites/Languages - Get a language in recruitingCESites | Recruiting CE Sites/Languages Get a language in recruitingCESites  |
| Recruiting CE Sites/Pages - Get a page in recruitingCESites | Recruiting CE Sites/Pages Get a page in recruitingCESites  |
| Recruiting CE Sites/Pages/Page Translations - Get a page translation in recruitingCESites | Recruiting CE Sites/Pages/Page Translations Get a page translation in recruitingCESites  |
| Recruiting CE Sites/Pages/Section Parameters - Get a section parameter in the sections of pages | Recruiting CE Sites/Pages/Section Parameters Get a section parameter in the sections of pages  |
| Recruiting CE Sites/Pages/Sections - Get a section in the pages of recruitingCESites | Recruiting CE Sites/Pages/Sections Get a section in the pages of recruitingCESites  |
| Recruiting CE Sites/Pages/Sections/Components - Get a component in the sections of pages | Recruiting CE Sites/Pages/Sections/Components Get a component in the sections of pages  |
| Recruiting CE Sites/Pages/Sections/Components/Component Translations - Get a component translation in the sections of pag... | Recruiting CE Sites/Pages/Sections/Components/Component Translations Get a component translation in the sections of pages  |
| Recruiting CE Sites/Pages/Sections/Components/Section Parameters - Get a section parameter in the sections of pages | Recruiting CE Sites/Pages/Sections/Components/Section Parameters Get a section parameter in the sections of pages  |
| Recruiting CE Sites/Pages/Sections/Section Parameters - Get a section parameter in the sections of pages | Recruiting CE Sites/Pages/Sections/Section Parameters Get a section parameter in the sections of pages  |
| Recruiting CE Sites/Settings - Get a setting in recruitingCESites | Recruiting CE Sites/Settings Get a setting in recruitingCESites  |
| Recruiting CE Sites/Site Custom Fonts - Get a custom font in recruitingCESites | Recruiting CE Sites/Site Custom Fonts Get a custom font in recruitingCESites  |
| Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings - Get a custom font setting in recruitingCESites | Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings Get a custom font setting in recruitingCESites  |
| Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings/Site Custom Font Setting Files - Get a custom font setting file in recruitingCESite... | Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings/Site Custom Font Setting Files Get a custom font setting file in recruitingCESites  |
| Recruiting CE Sites/Site Favicon - Get a favorite icon in recruitingCESites | Recruiting CE Sites/Site Favicon Get a favorite icon in recruitingCESites  |
| Recruiting CE Sites/Site Translations - Get a translation in recruitingCESites | Recruiting CE Sites/Site Translations Get a translation in recruitingCESites  |
| Recruiting CE Sites/Talent Community Sign Up - Get talent community sign up in recruitingCESites | Recruiting CE Sites/Talent Community Sign Up Get talent community sign up in recruitingCESites  |
| Recruiting CE Sites/Talent Community Sign Up/Translations for the Talent Community Sign Up - Get a talent community enrollment translation in r... | Recruiting CE Sites/Talent Community Sign Up/Translations for the Talent Community Sign Up Get a talent community enrollment translation in recruitingCESites  |
| Recruiting CE Skill Items - Get a skill item for the candidate. | Recruiting CE Skill Items Get a skill item for the candidate.  |
| Recruiting CE Source Tracking - Get details of a tracking | Recruiting CE Source Tracking Get details of a tracking  |
| Recruiting CE Tax Credits - Get tax credits partner candidate URL for a job re... | Recruiting CE Tax Credits Get tax credits partner candidate URL for a job requisition  |
| Recruiting CE Themes - Get details of a theme in recruitingCETemplates | Recruiting CE Themes Get details of a theme in recruitingCETemplates  |
| Recruiting CE Themes/Brands in Themes - Get details of a brand in recruitingCEThemes | Recruiting CE Themes/Brands in Themes Get details of a brand in recruitingCEThemes  |
| Recruiting CE Themes/Brands in Themes/Brand Translations of Themes - Get details of recruitingCEThemes Brand Translatio... | Recruiting CE Themes/Brands in Themes/Brand Translations of Themes Get details of recruitingCEThemes Brand Translation  |
| Recruiting CE Themes/Footer Links in Themes - Get details of footer links in recruitingCEThemes | Recruiting CE Themes/Footer Links in Themes Get details of footer links in recruitingCEThemes  |
| Recruiting CE Themes/Footer Links in Themes/Translations Used in Brands in Themes - Get details of a translation in recruitingCEThemes | Recruiting CE Themes/Footer Links in Themes/Translations Used in Brands in Themes Get details of a translation in recruitingCEThemes  |
| Recruiting CE Themes/Header Links in Themes - Get details of a header link in recruitingCEThemes | Recruiting CE Themes/Header Links in Themes Get details of a header link in recruitingCEThemes  |
| Recruiting CE Themes/Header Links in Themes/Translations Used in Brands in Themes - Get details of a translation in recruitingCEThemes | Recruiting CE Themes/Header Links in Themes/Translations Used in Brands in Themes Get details of a translation in recruitingCEThemes  |
| Recruiting CE Themes/Styles in Themes - Get details of a style in recruitingCEThemes | Recruiting CE Themes/Styles in Themes Get details of a style in recruitingCEThemes  |
| Recruiting CE Themes/Translations in a Template - Get details of a translation in recruitingCETempla... | Recruiting CE Themes/Translations in a Template Get details of a translation in recruitingCETemplates  |
| Recruiting CE User Tracking - Get a user tracking information | Recruiting CE User Tracking Get a user tracking information  |
| Recruiting CE Verification - Get details of a verification token | Recruiting CE Verification Get details of a verification token  |
| Recruiting Content Library Items LOV - Get a content library item LOV | Recruiting Content Library Items LOV Get a content library item LOV  |
| Recruiting Document Format Converter - Get a document format converter | Recruiting Document Format Converter Get a document format converter  |
| Recruiting Geography Hierarchy Locations LOV - Get the details of a geography hierarchy location ... | Recruiting Geography Hierarchy Locations LOV Get the details of a geography hierarchy location LOV  |
| Recruiting Hierarchy Locations - Get a hierarchy locations | Recruiting Hierarchy Locations Get a hierarchy locations  |
| Recruiting Internal Candidate Job Applications - Get an internal candidate job application | Recruiting Internal Candidate Job Applications Get an internal candidate job application  |
| Recruiting Job Applications - Get a job application | Recruiting Job Applications Get a job application  |
| Recruiting Job Applications/Attachments - Get an attachment | Recruiting Job Applications/Attachments Get an attachment  |
| Recruiting Job Applications/Education Items - Get an education item | Recruiting Job Applications/Education Items Get an education item  |
| Recruiting Job Applications/Experience Items - Get an experience item | Recruiting Job Applications/Experience Items Get an experience item  |
| Recruiting Job Applications/Languages - Get a language item | Recruiting Job Applications/Languages Get a language item  |
| Recruiting Job Applications/Licenses And Certificates - Get a license or certificate | Recruiting Job Applications/Licenses And Certificates Get a license or certificate  |
| Recruiting Job Applications/WorkPreferences - Get a work preference | Recruiting Job Applications/WorkPreferences Get a work preference  |
| Recruiting Job Families - Get a recruiting job family | Recruiting Job Families Get a recruiting job family  |
| Recruiting Job Offers - Get an offer | Recruiting Job Offers Get an offer  |
| Recruiting Job Offers/Attachments - Get an attachment | Recruiting Job Offers/Attachments Get an attachment  |
| Recruiting Job Offers/Collaborators - Get a collaborator | Recruiting Job Offers/Collaborators Get a collaborator  |
| Recruiting Job Offers/Offer Descriptive Flexfields - Get an offer descriptive flexfield | Recruiting Job Offers/Offer Descriptive Flexfields Get an offer descriptive flexfield  |
| Recruiting Job Requisition Distribution - Get the details of a job distribution | Recruiting Job Requisition Distribution Get the details of a job distribution  |
| Recruiting Job Requisition Distribution Posting Details - Get details of a job posting | Recruiting Job Requisition Distribution Posting Details Get details of a job posting  |
| Recruiting Job Requisition Distribution Posting Details/Job Posting on the Job Boards - Get details of a job posting on job boards | Recruiting Job Requisition Distribution Posting Details/Job Posting on the Job Boards Get details of a job posting on job boards  |
| Recruiting Job Requisition Distribution/Requisition Languages - Get the details of the required language | Recruiting Job Requisition Distribution/Requisition Languages Get the details of the required language  |
| Recruiting Job Requisition Distribution/Requisition Languages/Secondary Locations - Get the details of a secondary location | Recruiting Job Requisition Distribution/Requisition Languages/Secondary Locations Get the details of a secondary location  |
| Recruiting Job Requisition Distribution/Requisition Languages/Secondary Locations - Get the details of the media | Recruiting Job Requisition Distribution/Requisition Languages/Secondary Locations Get the details of the media  |
| Recruiting Job Requisition Template Details Preview - Get a requisition template details preview | Recruiting Job Requisition Template Details Preview Get a requisition template details preview  |
| Recruiting Job Requisition Template Details Preview/Job Requisition Flexfields - Get a requisition flexfield for the requisition de... | Recruiting Job Requisition Template Details Preview/Job Requisition Flexfields Get a requisition flexfield for the requisition details  |
| Recruiting Job Requisition Template Details Preview/Media - Get a media used in the requisition template detai... | Recruiting Job Requisition Template Details Preview/Media Get a media used in the requisition template details preview  |
| Recruiting Job Requisition Template Details Preview/Other Work Locations - Get other work location for the requisition templa... | Recruiting Job Requisition Template Details Preview/Other Work Locations Get other work location for the requisition template details previews  |
| Recruiting Job Requisition Template Details Preview/Secondary Locations - Get a secondary location for the requisition templ... | Recruiting Job Requisition Template Details Preview/Secondary Locations Get a secondary location for the requisition template details previews  |
| Recruiting Job Requisition Template Details Preview/Work Locations - Get a work location for the requisition template d... | Recruiting Job Requisition Template Details Preview/Work Locations Get a work location for the requisition template details previews  |
| Recruiting Job Requisition Template LOV - Get details of a job requisitions template LOV | Recruiting Job Requisition Template LOV Get details of a job requisitions template LOV  |
| Recruiting Job Requisitions - Get a requisition | Recruiting Job Requisitions Get a requisition  |
| Recruiting Job Requisitions LOV - Get details of a job requisitions LOV | Recruiting Job Requisitions LOV Get details of a job requisitions LOV  |
| Recruiting Job Requisitions/Attachments - Get an attachment | Recruiting Job Requisitions/Attachments Get an attachment  |
| Recruiting Job Requisitions/Collaborators - Get a collaborator | Recruiting Job Requisitions/Collaborators Get a collaborator  |
| Recruiting Job Requisitions/Languages - Get a language | Recruiting Job Requisitions/Languages Get a language  |
| Recruiting Job Requisitions/Media Links - Get a media link | Recruiting Job Requisitions/Media Links Get a media link  |
| Recruiting Job Requisitions/Media Links/Media Languages - Get a media language of a media link | Recruiting Job Requisitions/Media Links/Media Languages Get a media language of a media link  |
| Recruiting Job Requisitions/Other Locations - Get an other location | Recruiting Job Requisitions/Other Locations Get an other location  |
| Recruiting Job Requisitions/Other Work Locations - Get an other work location | Recruiting Job Requisitions/Other Work Locations Get an other work location  |
| Recruiting Job Requisitions/Published Jobs - Get a published job | Recruiting Job Requisitions/Published Jobs Get a published job  |
| Recruiting Job Requisitions/Requisition Descriptive Flexfields - Get a requisition descriptive flexfield | Recruiting Job Requisitions/Requisition Descriptive Flexfields Get a requisition descriptive flexfield  |
| Recruiting LinkedIn Event Notifications - Get details about a notification received from Lin... | Recruiting LinkedIn Event Notifications Get details about a notification received from LinkedIn  |
| Recruiting Locations LOV - Get a recruitingLocation | Recruiting Locations LOV Get a recruitingLocation  |
| Recruiting Organizations LOV - Get a recruiting organizations LOV | Recruiting Organizations LOV Get a recruiting organizations LOV  |
| Recruiting Representatives LOV - Get a representative in the list | Recruiting Representatives LOV Get a representative in the list  |
| Recruiting Requisition Questions LOV - Get a requisition question | Recruiting Requisition Questions LOV Get a requisition question  |
| Recruiting SecondaryApply Flows - Get a recruiting secondary application flow | Recruiting SecondaryApply Flows Get a recruiting secondary application flow  |
| Recruiting SecondaryApply Flows/Requisition Flexfields - Get a requisition flexfield | Recruiting SecondaryApply Flows/Requisition Flexfields Get a requisition flexfield  |
| Recruiting SecondaryApply Flows/Sections - Get a section of a recruiting secondary applicatio... | Recruiting SecondaryApply Flows/Sections Get a section of a recruiting secondary application flow  |
| Recruiting SecondaryApply Flows/Sections/Pages - Get a page of a recruiting secondary application f... | Recruiting SecondaryApply Flows/Sections/Pages Get a page of a recruiting secondary application flow  |
| Recruiting SecondaryApply Flows/Sections/Pages/Blocks - Get a block of a recruiting secondary application ... | Recruiting SecondaryApply Flows/Sections/Pages/Blocks Get a block of a recruiting secondary application flow  |
| Recruiting Tax Credit Accounts - Get details of tax credits account | Recruiting Tax Credit Accounts Get details of tax credits account  |
| Recruiting Tax Credit Accounts/Recruiting Tax Credit Account Packages - Get details of tax credits account package | Recruiting Tax Credit Accounts/Recruiting Tax Credit Account Packages Get details of tax credits account package  |
| Recruiting Tax Credits Candidate Details - Gets details for a job application | Recruiting Tax Credits Candidate Details Gets details for a job application  |
| Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Address Details - Get the address details of the candidate | Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Address Details Get the address details of the candidate  |
| Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Address Format - Get an address format of the candidate | Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Address Format Get an address format of the candidate  |
| Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Candidate National Identifiers - Get the candidate national identifier | Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Candidate National Identifiers Get the candidate national identifier  |
| Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Other Work Locations - Gets the job requisition work location | Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Other Work Locations Gets the job requisition work location  |
| Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Other Work Locations - Gets the job requisition work location | Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Other Work Locations Gets the job requisition work location  |
| Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Other Work Locations - Gets the job requisition work location | Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Other Work Locations Gets the job requisition work location  |
| Recruiting Tax Credits Results - Get details of a tax credit screening result | Recruiting Tax Credits Results Get details of a tax credit screening result  |
| Recruiting Tax Credits Results/Recruiting Tax Credits Package Results - Get details of a tax credit result for a package | Recruiting Tax Credits Results/Recruiting Tax Credits Package Results Get details of a tax credit result for a package  |
| Recruiting Tax Credits Results/Recruiting Tax Credits Package Results/Other Eligible Credits for Recruiting Tax Credits Package Results - Get details of other eligible credit for a tax cre... | Recruiting Tax Credits Results/Recruiting Tax Credits Package Results/Other Eligible Credits for Recruiting Tax Credits Package Results Get details of other eligible credit for a tax credit screening  |
| Reporting Establishments List of Values - Get a reporting establishment | Reporting Establishments List of Values Get a reporting establishment  |
| Responsibility Templates List of Values - Get a responsibility template | Responsibility Templates List of Values Get a responsibility template  |
| Review Periods List of Values - Get a review period | Review Periods List of Values Get a review period  |
| Roles List of Values - Get a role | Roles List of Values Get a role  |
| Salaries - Get a salary for a worker's assignment | Salaries Get a salary for a worker's assignment  |
| Salaries/Salary Components - Get a component | Salaries/Salary Components Get a component  |
| Salaries/Salary Rate Components - Get a rate component | Salaries/Salary Rate Components Get a rate component  |
| Salaries/Salary Simple Components - Get a simple component | Salaries/Salary Simple Components Get a simple component  |
| Salary Basis List of Values - Get a salary basis | Salary Basis List of Values Get a salary basis  |
| Salary Basis List of Values/Salary Basis Components - Get a component | Salary Basis List of Values/Salary Basis Components Get a component  |
| Schedule Requests - Get a schedule request | Schedule Requests Get a schedule request  |
| Schedule Requests/Schedule Events - Get a schedule event | Schedule Requests/Schedule Events Get a schedule event  |
| Schedule Requests/Schedule Events/Schedule Shift Events - Get a schedule shift event | Schedule Requests/Schedule Events/Schedule Shift Events Get a schedule shift event  |
| Schedule Requests/Schedule Events/Schedule Shift Events/Schedule Shift Attributes - Get a schedule shift attribute | Schedule Requests/Schedule Events/Schedule Shift Events/Schedule Shift Attributes Get a schedule shift attribute  |
| Scheduling Shifts List of Values - Get a shift | Scheduling Shifts List of Values Get a shift  |
| Self Details - Get a self detail | Self Details Get a self detail  |
| Sets List of Values - Get a set | Sets List of Values Get a set  |
| Shared Performance Goals List of Values - Get a shared goal | Shared Performance Goals List of Values Get a shared goal  |
| Status Change Requests - Get a status change request | Status Change Requests Get a status change request  |
| Status Change Requests/Status Changes - Get a status change record | Status Change Requests/Status Changes Get a status change record  |
| Succession Plans List of Values - Get a plan | Succession Plans List of Values Get a plan  |
| Talent External Candidates List of Values - Get an external candidate | Talent External Candidates List of Values Get an external candidate  |
| Talent Person Profiles - Get a person profile | Talent Person Profiles Get a person profile  |
| Talent Person Profiles/Accomplishment Sections - Get an accomplishment section | Talent Person Profiles/Accomplishment Sections Get an accomplishment section  |
| Talent Person Profiles/Accomplishment Sections/Accomplishment Items - Get an accomplishment item | Talent Person Profiles/Accomplishment Sections/Accomplishment Items Get an accomplishment item  |
| Talent Person Profiles/Accomplishment Sections/Accomplishment Items/Accomplishment Items Descriptive Flexfields - Get a flexfield | Talent Person Profiles/Accomplishment Sections/Accomplishment Items/Accomplishment Items Descriptive Flexfields Get a flexfield  |
| Talent Person Profiles/Areas of Expertises - Get areas of expertise | Talent Person Profiles/Areas of Expertises Get areas of expertise  |
| Talent Person Profiles/Areas of Interests - Get an area of interest | Talent Person Profiles/Areas of Interests Get an area of interest  |
| Talent Person Profiles/Attachments - Get an attachment | Talent Person Profiles/Attachments Get an attachment  |
| Talent Person Profiles/Career Preference Sections - Get career preference section | Talent Person Profiles/Career Preference Sections Get career preference section  |
| Talent Person Profiles/Career Preference Sections/Career Preference Items - Get career preference item | Talent Person Profiles/Career Preference Sections/Career Preference Items Get career preference item  |
| Talent Person Profiles/Career Preference Sections/Career Preference Items/Career Preference Items Descriptive Flexfields - Get a flexfield | Talent Person Profiles/Career Preference Sections/Career Preference Items/Career Preference Items Descriptive Flexfields Get a flexfield  |
| Talent Person Profiles/Certification Sections - Get a certification section | Talent Person Profiles/Certification Sections Get a certification section  |
| Talent Person Profiles/Certification Sections/Certification Items - Get a certification item | Talent Person Profiles/Certification Sections/Certification Items Get a certification item  |
| Talent Person Profiles/Certification Sections/Certification Items/Certification Items Descriptive Flexfields - Get a flexfield | Talent Person Profiles/Certification Sections/Certification Items/Certification Items Descriptive Flexfields Get a flexfield  |
| Talent Person Profiles/Competency Sections - Get a competency section | Talent Person Profiles/Competency Sections Get a competency section  |
| Talent Person Profiles/Competency Sections/Competency Items - Get a competency item | Talent Person Profiles/Competency Sections/Competency Items Get a competency item  |
| Talent Person Profiles/Competency Sections/Competency Items/Competency Items Descriptive Flexfields - Get a flexfield | Talent Person Profiles/Competency Sections/Competency Items/Competency Items Descriptive Flexfields Get a flexfield  |
| Talent Person Profiles/Education Sections - Get an education section | Talent Person Profiles/Education Sections Get an education section  |
| Talent Person Profiles/Education Sections/Education Items - Get an education item | Talent Person Profiles/Education Sections/Education Items Get an education item  |
| Talent Person Profiles/Education Sections/Education Items/Education Items Descriptive Flexfields - Get a flexfield | Talent Person Profiles/Education Sections/Education Items/Education Items Descriptive Flexfields Get a flexfield  |
| Talent Person Profiles/Favorite Links - Get a favorite link | Talent Person Profiles/Favorite Links Get a favorite link  |
| Talent Person Profiles/Honor Sections - Get honor section | Talent Person Profiles/Honor Sections Get honor section  |
| Talent Person Profiles/Honor Sections/Honor Items - Get honor items | Talent Person Profiles/Honor Sections/Honor Items Get honor items  |
| Talent Person Profiles/Honor Sections/Honor Items/Honor Items Descriptive Flexfields - Get a flexfield | Talent Person Profiles/Honor Sections/Honor Items/Honor Items Descriptive Flexfields Get a flexfield  |
| Talent Person Profiles/Language Sections - Get a language section | Talent Person Profiles/Language Sections Get a language section  |
| Talent Person Profiles/Language Sections/Language Items - Get a language item | Talent Person Profiles/Language Sections/Language Items Get a language item  |
| Talent Person Profiles/Language Sections/Language Items/Language Items Descriptive Flexfields - Get a flexfield | Talent Person Profiles/Language Sections/Language Items/Language Items Descriptive Flexfields Get a flexfield  |
| Talent Person Profiles/Membership Sections - Get membership section | Talent Person Profiles/Membership Sections Get membership section  |
| Talent Person Profiles/Membership Sections/Membership Items - Get membership item | Talent Person Profiles/Membership Sections/Membership Items Get membership item  |
| Talent Person Profiles/Membership Sections/Membership Items/Membership Items Descriptive Flexfields - Get a flexfield | Talent Person Profiles/Membership Sections/Membership Items/Membership Items Descriptive Flexfields Get a flexfield  |
| Talent Person Profiles/Skill Sections - Get a skill section | Talent Person Profiles/Skill Sections Get a skill section  |
| Talent Person Profiles/Skill Sections/Skill Items - Get a skill item | Talent Person Profiles/Skill Sections/Skill Items Get a skill item  |
| Talent Person Profiles/Skill Sections/Skill Items/Skill Items Descriptive Flexfields - Get a flexfield | Talent Person Profiles/Skill Sections/Skill Items/Skill Items Descriptive Flexfields Get a flexfield  |
| Talent Person Profiles/Special Project Sections - Get a special project section | Talent Person Profiles/Special Project Sections Get a special project section  |
| Talent Person Profiles/Special Project Sections/Special Project Items - Get a special project item | Talent Person Profiles/Special Project Sections/Special Project Items Get a special project item  |
| Talent Person Profiles/Special Project Sections/Special Project Items/Special Project Items Descriptive Flexfields - Get a flexfield | Talent Person Profiles/Special Project Sections/Special Project Items/Special Project Items Descriptive Flexfields Get a flexfield  |
| Talent Person Profiles/Tags - Get a tag | Talent Person Profiles/Tags Get a tag  |
| Talent Person Profiles/Work History Sections - Get work history section | Talent Person Profiles/Work History Sections Get work history section  |
| Talent Person Profiles/Work History Sections/Work History Items - Get work history item | Talent Person Profiles/Work History Sections/Work History Items Get work history item  |
| Talent Person Profiles/Work History Sections/Work History Items/Work History Items Descriptive Flexfields - Get a flexfield | Talent Person Profiles/Work History Sections/Work History Items/Work History Items Descriptive Flexfields Get a flexfield  |
| Talent Person Profiles/Work Preference Sections - Get work preference section | Talent Person Profiles/Work Preference Sections Get work preference section  |
| Talent Person Profiles/Work Preference Sections/Work Preference Items - Get work preference item | Talent Person Profiles/Work Preference Sections/Work Preference Items Get work preference item  |
| Talent Person Profiles/Work Preference Sections/Work Preference Items/Work Preference Items Descriptive Flexfields - Get a flexfield | Talent Person Profiles/Work Preference Sections/Work Preference Items/Work Preference Items Descriptive Flexfields Get a flexfield  |
| Talent Pools List of Values - Get a talent pool | Talent Pools List of Values Get a talent pool  |
| Talent Ratings - Get a talent rating | Talent Ratings Get a talent rating  |
| Talent Ratings/Career Potential - Get a career potential | Talent Ratings/Career Potential Get a career potential  |
| Talent Ratings/Career Potential/Career Potential Descriptive Flexfields - Get a flexfield | Talent Ratings/Career Potential/Career Potential Descriptive Flexfields Get a flexfield  |
| Talent Ratings/Impact Of Losses - Get an impact of loss | Talent Ratings/Impact Of Losses Get an impact of loss  |
| Talent Ratings/nBox Cell Assignments - Get an nBox cell assignment | Talent Ratings/nBox Cell Assignments Get an nBox cell assignment  |
| Talent Ratings/Performance Ratings - Get a performance rating | Talent Ratings/Performance Ratings Get a performance rating  |
| Talent Ratings/Performance Ratings/Performance Rating Descriptive Flexfields - Get a flexfield | Talent Ratings/Performance Ratings/Performance Rating Descriptive Flexfields Get a flexfield  |
| Talent Ratings/Risk Of Losses - Get a risk of loss | Talent Ratings/Risk Of Losses Get a risk of loss  |
| Talent Ratings/Risk Of Losses/Risk Of Losses Descriptive Flexfields - Get a flexfield | Talent Ratings/Risk Of Losses/Risk Of Losses Descriptive Flexfields Get a flexfield  |
| Talent Ratings/Talent Scores - Get a talent score | Talent Ratings/Talent Scores Get a talent score  |
| Talent Ratings/Talent Scores/Talent Score Descriptive Flexfields - Get a flexfield | Talent Ratings/Talent Scores/Talent Score Descriptive Flexfields Get a flexfield  |
| Talent Review Managers List of Values - Get a manager | Talent Review Managers List of Values Get a manager  |
| Target  Date Migrations - Get a target date migration | Target  Date Migrations Get a target date migration  |
| Tasks - Get a task | Tasks Get a task  |
| Tasks/Changed Attributes - Get a changed attribute | Tasks/Changed Attributes Get a changed attribute  |
| Tax Reporting Units List of Values - Get a tax reporting unit | Tax Reporting Units List of Values Get a tax reporting unit  |
| Team Members List of Values - Get a team member | Team Members List of Values Get a team member  |
| Time Attribute Values - Get a time attribute value | Time Attribute Values Get a time attribute value  |
| Time Attributes - Get a time attribute | Time Attributes Get a time attribute  |
| Time Attributes/Data Source Usages - Get the data source associated with the time attri... | Time Attributes/Data Source Usages Get the data source associated with the time attribute  |
| Time Attributes/Data Source Usages/Data Source Criteria Binds - Get the data source criterion bind | Time Attributes/Data Source Usages/Data Source Criteria Binds Get the data source criterion bind  |
| Time Card Fields List of Values - Get a time card field value | Time Card Fields List of Values Get a time card field value  |
| Time Cards List of Values - Get a time card | Time Cards List of Values Get a time card  |
| Time Event Requests - Get a time event request | Time Event Requests Get a time event request  |
| Time Event Requests/Time Events - Get a time event | Time Event Requests/Time Events Get a time event  |
| Time Event Requests/Time Events/Time Event Attributes - Get a time event attribute | Time Event Requests/Time Events/Time Event Attributes Get a time event attribute  |
| Time Layout Sets - Get a time layout set | Time Layout Sets Get a time layout set  |
| Time Layout Sets/Web Clock Layouts - Get a web clock layout | Time Layout Sets/Web Clock Layouts Get a web clock layout  |
| Time Layout Sets/Web Clock Layouts/Buttons - Get a web clock button | Time Layout Sets/Web Clock Layouts/Buttons Get a web clock button  |
| Time Layout Sets/Web Clock Layouts/Buttons/Time Attributes - Get a time attribute for a button | Time Layout Sets/Web Clock Layouts/Buttons/Time Attributes Get a time attribute for a button  |
| Time Layout Sets/Web Clock Layouts/Buttons/Time Card Fields - Get a time card field for a button | Time Layout Sets/Web Clock Layouts/Buttons/Time Card Fields Get a time card field for a button  |
| Time Record Event Requests - Get a time record event request | Time Record Event Requests Get a time record event request  |
| Time Record Event Requests/Time Record Events - Get a time record event | Time Record Event Requests/Time Record Events Get a time record event  |
| Time Record Event Requests/Time Record Events/Time Record Event Attributes - Get a time record event attribute | Time Record Event Requests/Time Record Events/Time Record Event Attributes Get a time record event attribute  |
| Time Record Event Requests/Time Record Events/Time Record Event Messages - Get a time record event message | Time Record Event Requests/Time Record Events/Time Record Event Messages Get a time record event message  |
| Time Record Groups - Get a time record group | Time Record Groups Get a time record group  |
| Time Record Groups/Time Attributes - Get a time record group attribute | Time Record Groups/Time Attributes Get a time record group attribute  |
| Time Record Groups/Time Messages - Get a time record group message | Time Record Groups/Time Messages Get a time record group message  |
| Time Record Groups/Time Messages/Time Message Tokens - Get a time record group message token | Time Record Groups/Time Messages/Time Message Tokens Get a time record group message token  |
| Time Record Groups/Time Record Groups - Get a time record group | Time Record Groups/Time Record Groups Get a time record group  |
| Time Record Groups/Time Record Groups/Time Attributes - Get a time record group attribute | Time Record Groups/Time Record Groups/Time Attributes Get a time record group attribute  |
| Time Record Groups/Time Record Groups/Time Messages - Get a time record group message | Time Record Groups/Time Record Groups/Time Messages Get a time record group message  |
| Time Record Groups/Time Record Groups/Time Messages/Time Message Tokens - Get a time record group message token | Time Record Groups/Time Record Groups/Time Messages/Time Message Tokens Get a time record group message token  |
| Time Record Groups/Time Record Groups/Time Record Groups - Get a time record group | Time Record Groups/Time Record Groups/Time Record Groups Get a time record group  |
| Time Record Groups/Time Record Groups/Time Records - Get a time record | Time Record Groups/Time Record Groups/Time Records Get a time record  |
| Time Record Groups/Time Record Groups/Time Records/Time Attributes - Get a time record group attribute | Time Record Groups/Time Record Groups/Time Records/Time Attributes Get a time record group attribute  |
| Time Record Groups/Time Record Groups/Time Records/Time Messages - Get a time record group message | Time Record Groups/Time Record Groups/Time Records/Time Messages Get a time record group message  |
| Time Record Groups/Time Record Groups/Time Records/Time Messages/Time Message Tokens - Get a time record group message token | Time Record Groups/Time Record Groups/Time Records/Time Messages/Time Message Tokens Get a time record group message token  |
| Time Record Groups/Time Record Groups/Time Records/Time Statuses - Get a time record group status | Time Record Groups/Time Record Groups/Time Records/Time Statuses Get a time record group status  |
| Time Record Groups/Time Record Groups/Time Statuses - Get a time record group status | Time Record Groups/Time Record Groups/Time Statuses Get a time record group status  |
| Time Record Groups/Time Records - Get a time record | Time Record Groups/Time Records Get a time record  |
| Time Record Groups/Time Records/Time Attributes - Get a time record group attribute | Time Record Groups/Time Records/Time Attributes Get a time record group attribute  |
| Time Record Groups/Time Records/Time Messages - Get a time record group message | Time Record Groups/Time Records/Time Messages Get a time record group message  |
| Time Record Groups/Time Records/Time Messages/Time Message Tokens - Get a time record group message token | Time Record Groups/Time Records/Time Messages/Time Message Tokens Get a time record group message token  |
| Time Record Groups/Time Records/Time Statuses - Get a time record group status | Time Record Groups/Time Records/Time Statuses Get a time record group status  |
| Time Record Groups/Time Statuses - Get a time record group status | Time Record Groups/Time Statuses Get a time record group status  |
| Time Records - Get a time record | Time Records Get a time record  |
| Time Records/Time Attributes - Get a time record attribute | Time Records/Time Attributes Get a time record attribute  |
| Time Records/Time Messages - Get a time record message | Time Records/Time Messages Get a time record message  |
| Time Records/Time Messages/Time Message Tokens - Get a time record message token | Time Records/Time Messages/Time Message Tokens Get a time record message token  |
| Time Records/Time Statuses - Get a time record status | Time Records/Time Statuses Get a time record status  |
| Tracking Services - Get a tracking service | Tracking Services Get a tracking service  |
| Tracking Services/Authorization Artifacts - Get an authorization artifact | Tracking Services/Authorization Artifacts Get an authorization artifact  |
| Unions List of Values - Get a union | Unions List of Values Get a union  |
| User Accounts - Get a user account | User Accounts Get a user account  |
| User Accounts List of Values - Get a user account | User Accounts List of Values Get a user account  |
| User Accounts/User Account Roles - Get a role assignment | User Accounts/User Account Roles Get a role assignment  |
| User Roles List of Values - Get a user role | User Roles List of Values Get a user role  |
| Web Clock Events - Get a web clock event | Web Clock Events Get a web clock event  |
| Web Clock Events/Time Card Fields - Get a time card field for a web clock event | Web Clock Events/Time Card Fields Get a time card field for a web clock event  |
| Wellness Activities - GET action not supported | Wellness Activities GET action not supported  |
| Wellness Activities/Activity Measures - GET action not supported | Wellness Activities/Activity Measures GET action not supported  |
| Workers - Get a worker | Workers Get a worker  |
| Workers/Addresses - Get a worker address | Workers/Addresses Get a worker address  |
| Workers/Addresses/Address Descriptive Flexfields - Get a flexfield | Workers/Addresses/Address Descriptive Flexfields Get a flexfield  |
| Workers/Citizenships - Get a worker citizenship | Workers/Citizenships Get a worker citizenship  |
| Workers/Citizenships/Citizenship Descriptive Flexfields - Get a flexfield | Workers/Citizenships/Citizenship Descriptive Flexfields Get a flexfield  |
| Workers/Disabilities - Get a worker disability | Workers/Disabilities Get a worker disability  |
| Workers/Disabilities/Attachments - Get an attachment | Workers/Disabilities/Attachments Get an attachment  |
| Workers/Disabilities/Disabilities Developer Flexfields - Get a flexfield | Workers/Disabilities/Disabilities Developer Flexfields Get a flexfield  |
| Workers/Driver Licenses - Get a worker drivers license | Workers/Driver Licenses Get a worker drivers license  |
| Workers/Driver Licenses/Driver License Descriptive Flexfields - Get a flexfield | Workers/Driver Licenses/Driver License Descriptive Flexfields Get a flexfield  |
| Workers/Driver Licenses/Driver License Developer Flexfields - Get a flexfield | Workers/Driver Licenses/Driver License Developer Flexfields Get a flexfield  |
| Workers/Emails - Get a worker email | Workers/Emails Get a worker email  |
| Workers/Emails/Email Descriptive Flexfields - Get a flexfield | Workers/Emails/Email Descriptive Flexfields Get a flexfield  |
| Workers/Ethnicities - Get a worker ethnicity | Workers/Ethnicities Get a worker ethnicity  |
| Workers/Ethnicities/Ethnicity Descriptive Flexfields - Get a flexfield | Workers/Ethnicities/Ethnicity Descriptive Flexfields Get a flexfield  |
| Workers/External Identifiers - Get a worker external identifier | Workers/External Identifiers Get a worker external identifier  |
| Workers/Legislative Information - Get a worker legislative record | Workers/Legislative Information Get a worker legislative record  |
| Workers/Legislative Information/Legislative Information Descriptive Flexfields - Get a flexfield | Workers/Legislative Information/Legislative Information Descriptive Flexfields Get a flexfield  |
| Workers/Legislative Information/Legislative Information Developer Flexfields - Get a flexfield | Workers/Legislative Information/Legislative Information Developer Flexfields Get a flexfield  |
| Workers/Messages - Get a worker message | Workers/Messages Get a worker message  |
| Workers/Names - Get a worker name | Workers/Names Get a worker name  |
| Workers/National Identifiers - Get a worker national identifier | Workers/National Identifiers Get a worker national identifier  |
| Workers/National Identifiers/National Identifier Descriptive Flexfields - Get a flexfield | Workers/National Identifiers/National Identifier Descriptive Flexfields Get a flexfield  |
| Workers/Other Communication Accounts - Get a worker communication account | Workers/Other Communication Accounts Get a worker communication account  |
| Workers/Other Communication Accounts/Other Communication Account Descriptive Flexfields - Get a flexfield | Workers/Other Communication Accounts/Other Communication Account Descriptive Flexfields Get a flexfield  |
| Workers/Passports - Get a worker passport | Workers/Passports Get a worker passport  |
| Workers/Passports/Passport Descriptive Flexfields - Get a flexfield | Workers/Passports/Passport Descriptive Flexfields Get a flexfield  |
| Workers/Phones - Get a worker phone | Workers/Phones Get a worker phone  |
| Workers/Phones/Phone Descriptive Flexfields - Get a flexfield | Workers/Phones/Phone Descriptive Flexfields Get a flexfield  |
| Workers/Photos - Get a worker photo | Workers/Photos Get a worker photo  |
| Workers/Photos/Photo Descriptive Flexfields - Get a flexfield | Workers/Photos/Photo Descriptive Flexfields Get a flexfield  |
| Workers/Religions - Get a worker religion | Workers/Religions Get a worker religion  |
| Workers/Religions/Religion Descriptive Flexfields - Get a flexfield | Workers/Religions/Religion Descriptive Flexfields Get a flexfield  |
| Workers/Visa Permits - Get a worker visa permit | Workers/Visa Permits Get a worker visa permit  |
| Workers/Visa Permits/Visa Permit Descriptive Flexfields - Get a flexfield | Workers/Visa Permits/Visa Permit Descriptive Flexfields Get a flexfield  |
| Workers/Visa Permits/Visa Permit Developer Flexfields - Get a flexfield | Workers/Visa Permits/Visa Permit Developer Flexfields Get a flexfield  |
| Workers/Work Relationships - Get a worker work relationship | Workers/Work Relationships Get a worker work relationship  |
| Workers/Work Relationships/Assignments - Get a worker assignment | Workers/Work Relationships/Assignments Get a worker assignment  |
| Workers/Work Relationships/Assignments/All Reports - Get a worker report | Workers/Work Relationships/Assignments/All Reports Get a worker report  |
| Workers/Work Relationships/Assignments/Assignment Descriptive Flexfields - Get a flexfield | Workers/Work Relationships/Assignments/Assignment Descriptive Flexfields Get a flexfield  |
| Workers/Work Relationships/Assignments/Assignment Developer Flexfields - Get a flexfield | Workers/Work Relationships/Assignments/Assignment Developer Flexfields Get a flexfield  |
| Workers/Work Relationships/Assignments/Assignment Extensible FlexFields - Get a flexfield | Workers/Work Relationships/Assignments/Assignment Extensible FlexFields Get a flexfield  |
| Workers/Work Relationships/Assignments/Grade Steps - Get an assignment grade step | Workers/Work Relationships/Assignments/Grade Steps Get an assignment grade step  |
| Workers/Work Relationships/Assignments/Managers - Get an assignment manager | Workers/Work Relationships/Assignments/Managers Get an assignment manager  |
| Workers/Work Relationships/Assignments/Representatives - Get a worker representative | Workers/Work Relationships/Assignments/Representatives Get a worker representative  |
| Workers/Work Relationships/Assignments/Work Measures - Get an assignment work measure | Workers/Work Relationships/Assignments/Work Measures Get an assignment work measure  |
| Workers/Work Relationships/Contracts - Get a worker contract | Workers/Work Relationships/Contracts Get a worker contract  |
| Workers/Work Relationships/Contracts/Contracts Descriptive Flexfields - Get a flexfield | Workers/Work Relationships/Contracts/Contracts Descriptive Flexfields Get a flexfield  |
| Workers/Work Relationships/Contracts/Contracts Developer Flexfields - Get a flexfield | Workers/Work Relationships/Contracts/Contracts Developer Flexfields Get a flexfield  |
| Workers/Work Relationships/Work Relationship Descriptive Flexfields - Get a flexfield | Workers/Work Relationships/Work Relationship Descriptive Flexfields Get a flexfield  |
| Workers/Work Relationships/Work Relationship Developer Flexfields - Get a flexfield | Workers/Work Relationships/Work Relationship Developer Flexfields Get a flexfield  |
| Workers/Worker Descriptive Flexfields - Get a flexfield | Workers/Worker Descriptive Flexfields Get a flexfield  |
| Workers/Worker Extensible Flexfields - Get a flexfield | Workers/Worker Extensible Flexfields Get a flexfield  |
| Worklife Teams List of Values - Get a team | Worklife Teams List of Values Get a team  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Absence Business Titles List of Values - Get all business titles of a worker | Absence Business Titles List of Values Get all business titles of a worker  |
| Absence Plans List of Values - Get all absence plans | Absence Plans List of Values Get all absence plans  |
| Absence Type Reasons List of Values - Get all absence reasons | Absence Type Reasons List of Values Get all absence reasons  |
| Absence Types List of Values - Get all absence types for a worker | Absence Types List of Values Get all absence types for a worker  |
| Absences - Get all absence records | Absences Get all absence records  |
| Absences/Absence Attachments - Get all absence attachments | Absences/Absence Attachments Get all absence attachments  |
| Absences/Absence Descriptive Flexfields - Get all flexfields | Absences/Absence Descriptive Flexfields Get all flexfields  |
| Absences/Absence Developer Descriptive Flexfields - Get all flexfields | Absences/Absence Developer Descriptive Flexfields Get all flexfields  |
| Absences/Absence Entitlements - Get all entitlement summary records | Absences/Absence Entitlements Get all entitlement summary records  |
| Absences/Absence Entitlements/Absence Entitlement Details - Get all entitlement detail records | Absences/Absence Entitlements/Absence Entitlement Details Get all entitlement detail records  |
| Absences/Absence Maternity Details - Get all maternity absence records | Absences/Absence Maternity Details Get all maternity absence records  |
| Action Reasons List of Values - Get all action reasons | Action Reasons List of Values Get all action reasons  |
| Actions List of Values - Get all actions | Actions List of Values Get all actions  |
| Allocated Checklists - Get all allocated checklists | Allocated Checklists Get all allocated checklists  |
| Allocated Checklists/Allocated Tasks - Get all allocated tasks | Allocated Checklists/Allocated Tasks Get all allocated tasks  |
| Allocated Checklists/Allocated Tasks/Allocated Tasks FlexFields - Get all flexfields | Allocated Checklists/Allocated Tasks/Allocated Tasks FlexFields Get all flexfields  |
| Allocated Checklists/Allocated Tasks/Attachments - Get all task attachments | Allocated Checklists/Allocated Tasks/Attachments Get all task attachments  |
| Allocated Checklists/Allocated Tasks/Documents - Get all document task type attachments | Allocated Checklists/Allocated Tasks/Documents Get all document task type attachments  |
| Application Flows - Get all job application flows | Application Flows Get all job application flows  |
| Application Flows/Requisition Flexfields - Get all requisition flexfields | Application Flows/Requisition Flexfields Get all requisition flexfields  |
| Application Flows/Sections - Get all sections | Application Flows/Sections Get all sections  |
| Application Flows/Sections/Pages - Get all pages | Application Flows/Sections/Pages Get all pages  |
| Application Flows/Sections/Pages/Blocks - Get all blocks | Application Flows/Sections/Pages/Blocks Get all blocks  |
| Areas of Responsibility - Get all areas of responsibility | Areas of Responsibility Get all areas of responsibility  |
| Assignment Status Types List of Values - Get all status types | Assignment Status Types List of Values Get all status types  |
| Availability Patterns - Get all availability patterns | Availability Patterns Get all availability patterns  |
| Availability Patterns/Shifts - Get all shifts | Availability Patterns/Shifts Get all shifts  |
| Availability Patterns/Shifts/Breaks - Get all breaks | Availability Patterns/Shifts/Breaks Get all breaks  |
| Bargaining Units List of Values - Get all bargaining units | Bargaining Units List of Values Get all bargaining units  |
| Benefit Enrollment Opportunities - Get all enrollment opportunities | Benefit Enrollment Opportunities Get all enrollment opportunities  |
| Benefit Enrollments - Get all enrollments | Benefit Enrollments Get all enrollments  |
| Benefit Enrollments/Costs - Get all costs | Benefit Enrollments/Costs Get all costs  |
| Benefit Enrollments/Dependents - Get all covered dependents | Benefit Enrollments/Dependents Get all covered dependents  |
| Benefit Enrollments/Providers - Get all plan providers | Benefit Enrollments/Providers Get all plan providers  |
| Benefit Enrollments/Providers/Provider Roles - Get all roles | Benefit Enrollments/Providers/Provider Roles Get all roles  |
| Benefit Groups - Get all benefit groups | Benefit Groups Get all benefit groups  |
| Benefit Groups List of Values - Get all benefit groups | Benefit Groups List of Values Get all benefit groups  |
| Benefit Options List of Values - Get all benefit options | Benefit Options List of Values Get all benefit options  |
| Benefit Plan Types List of Values - Get all plan types | Benefit Plan Types List of Values Get all plan types  |
| Benefit Plans List of Values - Get all benefit plans | Benefit Plans List of Values Get all benefit plans  |
| Benefit Programs List of Values - Get all benefit programs | Benefit Programs List of Values Get all benefit programs  |
| Business Units List of Values - Get all business units | Business Units List of Values Get all business units  |
| Candidate Details - Get details about all candidates | Candidate Details Get details about all candidates  |
| Candidate Duplicate Checks - Get all candidate duplicate checks | Candidate Duplicate Checks Get all candidate duplicate checks  |
| Candidate Job Application Drafts - Get all application drafts | Candidate Job Application Drafts Get all application drafts  |
| Candidate Job Application Drafts/Attachments of the Job Application Drafts - Get all attachments for application drafts | Candidate Job Application Drafts/Attachments of the Job Application Drafts Get all attachments for application drafts  |
| Candidate Responses to Regulatory Questions - Get all regulatory responses | Candidate Responses to Regulatory Questions Get all regulatory responses  |
| Candidates LOV - Get all metadata details for the candidates | Candidates LOV Get all metadata details for the candidates  |
| Channel Messages - Get all channel messages | Channel Messages Get all channel messages  |
| Channel Messages/Message Attachments - Get all attachments | Channel Messages/Message Attachments Get all attachments  |
| Check-In Documents - Get all documents | Check-In Documents Get all documents  |
| Check-In Documents/Questionnaire Responses - Get all responses | Check-In Documents/Questionnaire Responses Get all responses  |
| Check-In Documents/Questionnaire Responses/Question Responses - Get all responses | Check-In Documents/Questionnaire Responses/Question Responses Get all responses  |
| Check-In Documents/Questionnaire Responses/Question Responses/Attachments - Get all attachments | Check-In Documents/Questionnaire Responses/Question Responses/Attachments Get all attachments  |
| Check-In Templates List of Values - Get all templates | Check-In Templates List of Values Get all templates  |
| Collective Agreements List of Values - Get all collective agreements | Collective Agreements List of Values Get all collective agreements  |
| Competition Participants List of Values - Get all competition participants | Competition Participants List of Values Get all competition participants  |
| Content Item List of Values - Get all contet items | Content Item List of Values Get all contet items  |
| Contracts List of Values - Get all contracts | Contracts List of Values Get all contracts  |
| Cost Centers List of Values - Get all cost centers | Cost Centers List of Values Get all cost centers  |
| Countries List of Values - Get all countries | Countries List of Values Get all countries  |
| Department Tree Nodes List of Values - Get all department tree nodes | Department Tree Nodes List of Values Get all department tree nodes  |
| Departments List of Values - Get all departments | Departments List of Values Get all departments  |
| Departments List of Values V2 - Get all departments | Departments List of Values V2 Get all departments  |
| Disability Organizations List of Values - Get all disability organizations | Disability Organizations List of Values Get all disability organizations  |
| Document Delivery Preferences - Get all delivery preferences for document records | Document Delivery Preferences Get all delivery preferences for document records  |
| Document Records - Get all document records | Document Records Get all document records  |
| Document Records/Attachments - Get all attachments | Document Records/Attachments Get all attachments  |
| Document Records/Document Records Descriptive Flexfields - Get all flexfields | Document Records/Document Records Descriptive Flexfields Get all flexfields  |
| Document Records/Document Records Developer Descriptive Flexfields - Get all flexfields | Document Records/Document Records Developer Descriptive Flexfields Get all flexfields  |
| Document Types List of Values - Get all document types | Document Types List of Values Get all document types  |
| Element Entries - Get all element entries | Element Entries Get all element entries  |
| Element Entries/Element Entry Values - Get all element entry values | Element Entries/Element Entry Values Get all element entry values  |
| Eligibility Object Results - Get all eligibility object results | Eligibility Object Results Get all eligibility object results  |
| Eligibility Objects - Get all eligibility objects | Eligibility Objects Get all eligibility objects  |
| Eligibility Objects/Eligibility Object Profiles - Get all eligibility object profiles | Eligibility Objects/Eligibility Object Profiles Get all eligibility object profiles  |
| Eligibility Profiles List of Values - Get all eligibility profiles | Eligibility Profiles List of Values Get all eligibility profiles  |
| Eligible Contacts List of Values - Get all eligible Contacts | Eligible Contacts List of Values Get all eligible Contacts  |
| Eligible Options List of Values - Get all plan options | Eligible Options List of Values Get all plan options  |
| Eligible Plans List of Values - Get all individual compensation plans | Eligible Plans List of Values Get all individual compensation plans  |
| Email Address Migrations - Get all email address migrations | Email Address Migrations Get all email address migrations  |
| Employees - Get all employees | Employees Get all employees  |
| Employees/Assignments - Get all assignments | Employees/Assignments Get all assignments  |
| Employees/Assignments/Assignment Descriptive Flexfields - Get all flexfields | Employees/Assignments/Assignment Descriptive Flexfields Get all flexfields  |
| Employees/Assignments/Assignment Extra Information Extensible FlexFields - Get all flexfields | Employees/Assignments/Assignment Extra Information Extensible FlexFields Get all flexfields  |
| Employees/Assignments/Employee Representatives - Get all representatives | Employees/Assignments/Employee Representatives Get all representatives  |
| Employees/Assignments/People Group Key Flexfields - Get all flexfields | Employees/Assignments/People Group Key Flexfields Get all flexfields  |
| Employees/Direct Reports - Get all direct reports | Employees/Direct Reports Get all direct reports  |
| Employees/Person Descriptive Flexfields - Get all flexfields | Employees/Person Descriptive Flexfields Get all flexfields  |
| Employees/Person Extra Information Extensible FlexFields - Get all flexfields | Employees/Person Extra Information Extensible FlexFields Get all flexfields  |
| Employees/Photos - Get all photos | Employees/Photos Get all photos  |
| Employees/Roles - Get all roles | Employees/Roles Get all roles  |
| Employees/Visas - Get all visas | Employees/Visas Get all visas  |
| Flow Instances List of Values - Get all payroll flow instances | Flow Instances List of Values Get all payroll flow instances  |
| Flow Parameter List of Values - Get all flow parameter values | Flow Parameter List of Values Get all flow parameter values  |
| Flow Patterns List of Values - Get all flow patterns | Flow Patterns List of Values Get all flow patterns  |
| Geographic Hierarchies List of Values - Get all geographic hierarchies | Geographic Hierarchies List of Values Get all geographic hierarchies  |
| Goal Plan Sets List of Values - Get all goal plan sets | Goal Plan Sets List of Values Get all goal plan sets  |
| Goal Plans List of Values - Get all goal plans | Goal Plans List of Values Get all goal plans  |
| Grade Ladders - Get all grade ladders | Grade Ladders Get all grade ladders  |
| Grade Ladders List of Values - Get all grade ladders | Grade Ladders List of Values Get all grade ladders  |
| Grade Ladders/Grade Ladder Descriptive Flexfields - Get all flexfields | Grade Ladders/Grade Ladder Descriptive Flexfields Get all flexfields  |
| Grade Ladders/Grades - Get all grades | Grade Ladders/Grades Get all grades  |
| Grade Ladders/Step Rates - Get all step rates | Grade Ladders/Step Rates Get all step rates  |
| Grade Ladders/Step Rates/Step Rate Values - Get all values | Grade Ladders/Step Rates/Step Rate Values Get all values  |
| Grade Rates - Get all grade rates | Grade Rates Get all grade rates  |
| Grade Rates/Rate Values - Get all rate values | Grade Rates/Rate Values Get all rate values  |
| Grade Steps List of Values - Get all grade steps | Grade Steps List of Values Get all grade steps  |
| Grades - Get all grades | Grades Get all grades  |
| Grades List of Values - Get all grades | Grades List of Values Get all grades  |
| Grades/Grade Customer Flexfields - Get all grade flexfields | Grades/Grade Customer Flexfields Get all grade flexfields  |
| Grades/Grade Steps - Get all grade steps | Grades/Grade Steps Get all grade steps  |
| HCM Groups List of Values - Get all HCM groups | HCM Groups List of Values Get all HCM groups  |
| HCM Trees List of Values - Get all hierarchy names | HCM Trees List of Values Get all hierarchy names  |
| Incident ID - Incident ID | Incident ID Incident ID  |
| Incident ID/Incident ID - Incident ID | Incident ID/Incident ID Incident ID  |
| Incident ID/Incident ID/Attachments - Get all attachments. | Incident ID/Incident ID/Attachments Get all attachments.  |
| Incident Kiosks - Get all incidents | Incident Kiosks Get all incidents  |
| Incident Kiosks/Incident Detail Kiosks - Get all incident events | Incident Kiosks/Incident Detail Kiosks Get all incident events  |
| Job Application Filter Autosuggest List of Values - Get all autosuggest values for job application fil... | Job Application Filter Autosuggest List of Values Get all autosuggest values for job application filters.  |
| Job Application Grid View Fields - Get all grid view fields. | Job Application Grid View Fields Get all grid view fields.  |
| Job Applications - Get all job applications | Job Applications Get all job applications  |
| Job Applications/Candidate Responses to Questions - Get the details of all responses for a questionnai... | Job Applications/Candidate Responses to Questions Get the details of all responses for a questionnaire  |
| Job Applications/Candidate Responses to Regulatory Questions - Get all regulatory responses | Job Applications/Candidate Responses to Regulatory Questions Get all regulatory responses  |
| Job Applications/Preferred Job Locations - Get all preferred locations | Job Applications/Preferred Job Locations Get all preferred locations  |
| Job Applications/Schedule Job Interviews - Get all the scheduled interviews | Job Applications/Schedule Job Interviews Get all the scheduled interviews  |
| Job Applications/Secondary Job Submissions - Get all the secondary job applications | Job Applications/Secondary Job Submissions Get all the secondary job applications  |
| Job Applications/Unscheduled Job Interviews - Get all the unscheduled interview requests | Job Applications/Unscheduled Job Interviews Get all the unscheduled interview requests  |
| Job Families - Get all job families | Job Families Get all job families  |
| Job Families List of Values - Get all job families | Job Families List of Values Get all job families  |
| Job Families/Job Family Descriptive Flexfields - Get all flexfields | Job Families/Job Family Descriptive Flexfields Get all flexfields  |
| Jobs - Get all jobs | Jobs Get all jobs  |
| Jobs List of Values - Get all jobs | Jobs List of Values Get all jobs  |
| Jobs List of Values V2 - Get all jobs | Jobs List of Values V2 Get all jobs  |
| Jobs/Job Customer Flexfields - Get all job flexfields | Jobs/Job Customer Flexfields Get all job flexfields  |
| Jobs/Valid Grades - Get all valid grades | Jobs/Valid Grades Get all valid grades  |
| Learner Learning Records - Get all assignment records | Learner Learning Records Get all assignment records  |
| Learner Learning Records/Active Learner Comments - Get all active learner comments | Learner Learning Records/Active Learner Comments Get all active learner comments  |
| Learner Learning Records/Active Learner Comments/Likes - Get all likes | Learner Learning Records/Active Learner Comments/Likes Get all likes  |
| Learner Learning Records/Active Learner Comments/Replies - Get all replies | Learner Learning Records/Active Learner Comments/Replies Get all replies  |
| Learner Learning Records/Active Learner Comments/Replies/Likes - Get all likes | Learner Learning Records/Active Learner Comments/Replies/Likes Get all likes  |
| Learner Learning Records/Approval Details - Get all approval details | Learner Learning Records/Approval Details Get all approval details  |
| Learner Learning Records/Assigned To Person Details - Get all assignee to person details | Learner Learning Records/Assigned To Person Details Get all assignee to person details  |
| Learner Learning Records/Assigner Person Details - Get all assigner person details | Learner Learning Records/Assigner Person Details Get all assigner person details  |
| Learner Learning Records/Assignment Descriptive Flexfields - Get all assignment flexfields | Learner Learning Records/Assignment Descriptive Flexfields Get all assignment flexfields  |
| Learner Learning Records/Completion Details - Get all completion details | Learner Learning Records/Completion Details Get all completion details  |
| Learner Learning Records/Completion Details/Activity Completion Predecessor Hints - Get all activity completion predecessor hints | Learner Learning Records/Completion Details/Activity Completion Predecessor Hints Get all activity completion predecessor hints  |
| Learner Learning Records/Completion Details/Activity Content Attempts - Get all content attempt details | Learner Learning Records/Completion Details/Activity Content Attempts Get all content attempt details  |
| Learner Learning Records/Completion Details/Activity Section Completion Predecessor Hints - Get all activity section completion predecessor hi... | Learner Learning Records/Completion Details/Activity Section Completion Predecessor Hints Get all activity section completion predecessor hints  |
| Learner Learning Records/Completion Details/Classrooms - Get all classroom details | Learner Learning Records/Completion Details/Classrooms Get all classroom details  |
| Learner Learning Records/Completion Details/Classrooms/Attachments - Get all attachments | Learner Learning Records/Completion Details/Classrooms/Attachments Get all attachments  |
| Learner Learning Records/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get all classroom flexfields | Learner Learning Records/Completion Details/Classrooms/Classroom Descriptive Flexfields Get all classroom flexfields  |
| Learner Learning Records/Completion Details/Instructors - Get all instructors | Learner Learning Records/Completion Details/Instructors Get all instructors  |
| Learner Learning Records/Completion Details/Instructors/Instructor Descriptive Flexfields - Get all instructor flexfields | Learner Learning Records/Completion Details/Instructors/Instructor Descriptive Flexfields Get all instructor flexfields  |
| Learner Learning Records/Completion Details/Learning Item Talent Profile Summaries - Get all talent profile summaries | Learner Learning Records/Completion Details/Learning Item Talent Profile Summaries Get all talent profile summaries  |
| Learner Learning Records/Completion Details/Offered Languages - Get all offered languages | Learner Learning Records/Completion Details/Offered Languages Get all offered languages  |
| Learner Learning Records/Completion Details/Offered Locations - Get all offering locations | Learner Learning Records/Completion Details/Offered Locations Get all offering locations  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings - Get all other selected course offerings | Learner Learning Records/Completion Details/Other Selected Course Offerings Get all other selected course offerings  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Assignment Descriptive Flexfields - Get all assignment flexfields | Learner Learning Records/Completion Details/Other Selected Course Offerings/Assignment Descriptive Flexfields Get all assignment flexfields  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details - Get all completion details | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details Get all completion details  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Activity Content Attempts - Get all content attempt details | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Activity Content Attempts Get all content attempt details  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Classrooms - Get all classroom details | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Classrooms Get all classroom details  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Classrooms/Attachments - Get all attachments | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Classrooms/Attachments Get all attachments  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get all classroom flexfields | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields Get all classroom flexfields  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Instructors - Get all instructors | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Instructors Get all instructors  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields - Get all instructor flexfields | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields Get all instructor flexfields  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Offered Languages - Get all offered languages | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Offered Languages Get all offered languages  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Offered Locations - Get all offering locations | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Offered Locations Get all offering locations  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Related Materials - Get all related materials | Learner Learning Records/Completion Details/Other Selected Course Offerings/Completion Details/Related Materials Get all related materials  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Learning Item Descriptive Flexfields - Get all learning item flexfields | Learner Learning Records/Completion Details/Other Selected Course Offerings/Learning Item Descriptive Flexfields Get all learning item flexfields  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Learning Item Rating Details - Get all learning item ratings | Learner Learning Records/Completion Details/Other Selected Course Offerings/Learning Item Rating Details Get all learning item ratings  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Offering Descriptive Flexfields - Get all offering flexfields | Learner Learning Records/Completion Details/Other Selected Course Offerings/Offering Descriptive Flexfields Get all offering flexfields  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Transaction Histories - Get all past transaction history | Learner Learning Records/Completion Details/Other Selected Course Offerings/Transaction Histories Get all past transaction history  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings - Get all primary selected course offerings | Learner Learning Records/Completion Details/Primary Selected Course Offerings Get all primary selected course offerings  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Assignment Descriptive Flexfields - Get all assignment flexfields | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Assignment Descriptive Flexfields Get all assignment flexfields  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details - Get all completion details | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details Get all completion details  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Activity Content Attempts - Get all content attempt details | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Activity Content Attempts Get all content attempt details  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Classrooms - Get all classroom details | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Classrooms Get all classroom details  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Classrooms/Attachments - Get all attachments | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Classrooms/Attachments Get all attachments  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get all classroom flexfields | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields Get all classroom flexfields  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Instructors - Get all instructors | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Instructors Get all instructors  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields - Get all instructor flexfields | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields Get all instructor flexfields  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Offered Languages - Get all offered languages | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Offered Languages Get all offered languages  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Offered Locations - Get all offering locations | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Offered Locations Get all offering locations  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Related Materials - Get all related materials | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Completion Details/Related Materials Get all related materials  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Learning Item Descriptive Flexfields - Get all learning item flexfields | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Learning Item Descriptive Flexfields Get all learning item flexfields  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Learning Item Rating Details - Get all learning item ratings | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Learning Item Rating Details Get all learning item ratings  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Offering Descriptive Flexfields - Get all offering flexfields | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Offering Descriptive Flexfields Get all offering flexfields  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Transaction Histories - Get all past transaction history | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Transaction Histories Get all past transaction history  |
| Learner Learning Records/Completion Details/Related Materials - Get all related materials | Learner Learning Records/Completion Details/Related Materials Get all related materials  |
| Learner Learning Records/Completion Details/Selected Course Offerings - Get all selected course offerings | Learner Learning Records/Completion Details/Selected Course Offerings Get all selected course offerings  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments - Get all active learner comments | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments Get all active learner comments  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Likes - Get all likes | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Likes Get all likes  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies - Get all replies | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies Get all replies  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies/Likes - Get all likes | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies/Likes Get all likes  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Approval Details - Get all approval details | Learner Learning Records/Completion Details/Selected Course Offerings/Approval Details Get all approval details  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Assigner Person Details - Get all assigner person details | Learner Learning Records/Completion Details/Selected Course Offerings/Assigner Person Details Get all assigner person details  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Assignment Descriptive Flexfields - Get all assignment flexfields | Learner Learning Records/Completion Details/Selected Course Offerings/Assignment Descriptive Flexfields Get all assignment flexfields  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details - Get all completion details | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details Get all completion details  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Activity Content Attempts - Get all content attempt details | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Activity Content Attempts Get all content attempt details  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Classrooms - Get all classroom details | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Classrooms Get all classroom details  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Classrooms/Attachments - Get all attachments | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Classrooms/Attachments Get all attachments  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get all classroom flexfields | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields Get all classroom flexfields  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Instructors - Get all instructors | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Instructors Get all instructors  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields - Get all instructor flexfields | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields Get all instructor flexfields  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Offered Languages - Get all offered languages | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Offered Languages Get all offered languages  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Offered Locations - Get all offering locations | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Offered Locations Get all offering locations  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Related Materials - Get all related materials | Learner Learning Records/Completion Details/Selected Course Offerings/Completion Details/Related Materials Get all related materials  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Enrollment Related Materials - Get all enrollment related materials | Learner Learning Records/Completion Details/Selected Course Offerings/Enrollment Related Materials Get all enrollment related materials  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Descriptive Flexfields - Get all learning item flexfields | Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Descriptive Flexfields Get all learning item flexfields  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Rating Details - Get all learning item ratings | Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Rating Details Get all learning item ratings  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Related Materials - Get all learning item related materials | Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Related Materials Get all learning item related materials  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Offering Descriptive Flexfields - Get all offering flexfields | Learner Learning Records/Completion Details/Selected Course Offerings/Offering Descriptive Flexfields Get all offering flexfields  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Classrooms - Get all offering primary classrooms | Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Classrooms Get all offering primary classrooms  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Classrooms/Attachments - Get all attachments | Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Classrooms/Attachments Get all attachments  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Classrooms/Classroom Descriptive Flexfields - Get all classroom flexfields | Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Classrooms/Classroom Descriptive Flexfields Get all classroom flexfields  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Instructors - Get all offering primary instructors | Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Instructors Get all offering primary instructors  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Instructors/Instructor Descriptive Flexfields - Get all instructor flexfields | Learner Learning Records/Completion Details/Selected Course Offerings/Offering Primary Instructors/Instructor Descriptive Flexfields Get all instructor flexfields  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Transaction Histories - Get all past transaction history | Learner Learning Records/Completion Details/Selected Course Offerings/Transaction Histories Get all past transaction history  |
| Learner Learning Records/Completion Details/Selected Course Offerings/User Action Hints - Get all user action hints | Learner Learning Records/Completion Details/Selected Course Offerings/User Action Hints Get all user action hints  |
| Learner Learning Records/Completion Details/User Action Hints - Get all user action hints | Learner Learning Records/Completion Details/User Action Hints Get all user action hints  |
| Learner Learning Records/Completion Summaries - Get all completion summaries | Learner Learning Records/Completion Summaries Get all completion summaries  |
| Learner Learning Records/Course Descriptive Flexfields - Get all course flexfields | Learner Learning Records/Course Descriptive Flexfields Get all course flexfields  |
| Learner Learning Records/Enrollment Related Materials - Get all enrollment related materials | Learner Learning Records/Enrollment Related Materials Get all enrollment related materials  |
| Learner Learning Records/Learning Item Descriptive Flexfields - Get all learning item flexfields | Learner Learning Records/Learning Item Descriptive Flexfields Get all learning item flexfields  |
| Learner Learning Records/Learning Item Publisher Person Details - Get all learning item publisher person details | Learner Learning Records/Learning Item Publisher Person Details Get all learning item publisher person details  |
| Learner Learning Records/Learning Item Rating Details - Get all learning item ratings | Learner Learning Records/Learning Item Rating Details Get all learning item ratings  |
| Learner Learning Records/Learning Item Related Materials - Get all learning item related materials | Learner Learning Records/Learning Item Related Materials Get all learning item related materials  |
| Learner Learning Records/Learning Outcomes - Get all learning outcomes | Learner Learning Records/Learning Outcomes Get all learning outcomes  |
| Learner Learning Records/Learning Prerequisites - Get all learning prerequisites | Learner Learning Records/Learning Prerequisites Get all learning prerequisites  |
| Learner Learning Records/Other Selected Course Offerings - Get all other selected course offerings | Learner Learning Records/Other Selected Course Offerings Get all other selected course offerings  |
| Learner Learning Records/Other Selected Course Offerings/Assignment Descriptive Flexfields - Get all assignment flexfields | Learner Learning Records/Other Selected Course Offerings/Assignment Descriptive Flexfields Get all assignment flexfields  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details - Get all completion details | Learner Learning Records/Other Selected Course Offerings/Completion Details Get all completion details  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Activity Content Attempts - Get all content attempt details | Learner Learning Records/Other Selected Course Offerings/Completion Details/Activity Content Attempts Get all content attempt details  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Classrooms - Get all classroom details | Learner Learning Records/Other Selected Course Offerings/Completion Details/Classrooms Get all classroom details  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Classrooms/Attachments - Get all attachments | Learner Learning Records/Other Selected Course Offerings/Completion Details/Classrooms/Attachments Get all attachments  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get all classroom flexfields | Learner Learning Records/Other Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields Get all classroom flexfields  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Instructors - Get all instructors | Learner Learning Records/Other Selected Course Offerings/Completion Details/Instructors Get all instructors  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields - Get all instructor flexfields | Learner Learning Records/Other Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields Get all instructor flexfields  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Offered Languages - Get all offered languages | Learner Learning Records/Other Selected Course Offerings/Completion Details/Offered Languages Get all offered languages  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Offered Locations - Get all offering locations | Learner Learning Records/Other Selected Course Offerings/Completion Details/Offered Locations Get all offering locations  |
| Learner Learning Records/Other Selected Course Offerings/Completion Details/Related Materials - Get all related materials | Learner Learning Records/Other Selected Course Offerings/Completion Details/Related Materials Get all related materials  |
| Learner Learning Records/Other Selected Course Offerings/Learning Item Descriptive Flexfields - Get all learning item flexfields | Learner Learning Records/Other Selected Course Offerings/Learning Item Descriptive Flexfields Get all learning item flexfields  |
| Learner Learning Records/Other Selected Course Offerings/Learning Item Rating Details - Get all learning item ratings | Learner Learning Records/Other Selected Course Offerings/Learning Item Rating Details Get all learning item ratings  |
| Learner Learning Records/Other Selected Course Offerings/Offering Descriptive Flexfields - Get all offering flexfields | Learner Learning Records/Other Selected Course Offerings/Offering Descriptive Flexfields Get all offering flexfields  |
| Learner Learning Records/Other Selected Course Offerings/Transaction Histories - Get all past transaction history | Learner Learning Records/Other Selected Course Offerings/Transaction Histories Get all past transaction history  |
| Learner Learning Records/Past Renewals - Get all past renewals | Learner Learning Records/Past Renewals Get all past renewals  |
| Learner Learning Records/Primary Selected Course Offerings - Get all primary selected course offerings | Learner Learning Records/Primary Selected Course Offerings Get all primary selected course offerings  |
| Learner Learning Records/Primary Selected Course Offerings/Assignment Descriptive Flexfields - Get all assignment flexfields | Learner Learning Records/Primary Selected Course Offerings/Assignment Descriptive Flexfields Get all assignment flexfields  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details - Get all completion details | Learner Learning Records/Primary Selected Course Offerings/Completion Details Get all completion details  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Activity Content Attempts - Get all content attempt details | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Activity Content Attempts Get all content attempt details  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Classrooms - Get all classroom details | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Classrooms Get all classroom details  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Classrooms/Attachments - Get all attachments | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Classrooms/Attachments Get all attachments  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get all classroom flexfields | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields Get all classroom flexfields  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Instructors - Get all instructors | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Instructors Get all instructors  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields - Get all instructor flexfields | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields Get all instructor flexfields  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Offered Languages - Get all offered languages | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Offered Languages Get all offered languages  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Offered Locations - Get all offering locations | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Offered Locations Get all offering locations  |
| Learner Learning Records/Primary Selected Course Offerings/Completion Details/Related Materials - Get all related materials | Learner Learning Records/Primary Selected Course Offerings/Completion Details/Related Materials Get all related materials  |
| Learner Learning Records/Primary Selected Course Offerings/Learning Item Descriptive Flexfields - Get all learning item flexfields | Learner Learning Records/Primary Selected Course Offerings/Learning Item Descriptive Flexfields Get all learning item flexfields  |
| Learner Learning Records/Primary Selected Course Offerings/Learning Item Rating Details - Get all learning item ratings | Learner Learning Records/Primary Selected Course Offerings/Learning Item Rating Details Get all learning item ratings  |
| Learner Learning Records/Primary Selected Course Offerings/Offering Descriptive Flexfields - Get all offering flexfields | Learner Learning Records/Primary Selected Course Offerings/Offering Descriptive Flexfields Get all offering flexfields  |
| Learner Learning Records/Primary Selected Course Offerings/Transaction Histories - Get all past transaction history | Learner Learning Records/Primary Selected Course Offerings/Transaction Histories Get all past transaction history  |
| Learner Learning Records/Selected Course Offerings - Get all selected course offerings | Learner Learning Records/Selected Course Offerings Get all selected course offerings  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments - Get all active learner comments | Learner Learning Records/Selected Course Offerings/Active Learner Comments Get all active learner comments  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments/Likes - Get all likes | Learner Learning Records/Selected Course Offerings/Active Learner Comments/Likes Get all likes  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies - Get all replies | Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies Get all replies  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies/Likes - Get all likes | Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies/Likes Get all likes  |
| Learner Learning Records/Selected Course Offerings/Approval Details - Get all approval details | Learner Learning Records/Selected Course Offerings/Approval Details Get all approval details  |
| Learner Learning Records/Selected Course Offerings/Assigner Person Details - Get all assigner person details | Learner Learning Records/Selected Course Offerings/Assigner Person Details Get all assigner person details  |
| Learner Learning Records/Selected Course Offerings/Assignment Descriptive Flexfields - Get all assignment flexfields | Learner Learning Records/Selected Course Offerings/Assignment Descriptive Flexfields Get all assignment flexfields  |
| Learner Learning Records/Selected Course Offerings/Completion Details - Get all completion details | Learner Learning Records/Selected Course Offerings/Completion Details Get all completion details  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Activity Content Attempts - Get all content attempt details | Learner Learning Records/Selected Course Offerings/Completion Details/Activity Content Attempts Get all content attempt details  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Classrooms - Get all classroom details | Learner Learning Records/Selected Course Offerings/Completion Details/Classrooms Get all classroom details  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Classrooms/Attachments - Get all attachments | Learner Learning Records/Selected Course Offerings/Completion Details/Classrooms/Attachments Get all attachments  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields - Get all classroom flexfields | Learner Learning Records/Selected Course Offerings/Completion Details/Classrooms/Classroom Descriptive Flexfields Get all classroom flexfields  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Instructors - Get all instructors | Learner Learning Records/Selected Course Offerings/Completion Details/Instructors Get all instructors  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields - Get all instructor flexfields | Learner Learning Records/Selected Course Offerings/Completion Details/Instructors/Instructor Descriptive Flexfields Get all instructor flexfields  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Offered Languages - Get all offered languages | Learner Learning Records/Selected Course Offerings/Completion Details/Offered Languages Get all offered languages  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Offered Locations - Get all offering locations | Learner Learning Records/Selected Course Offerings/Completion Details/Offered Locations Get all offering locations  |
| Learner Learning Records/Selected Course Offerings/Completion Details/Related Materials - Get all related materials | Learner Learning Records/Selected Course Offerings/Completion Details/Related Materials Get all related materials  |
| Learner Learning Records/Selected Course Offerings/Enrollment Related Materials - Get all enrollment related materials | Learner Learning Records/Selected Course Offerings/Enrollment Related Materials Get all enrollment related materials  |
| Learner Learning Records/Selected Course Offerings/Learning Item Descriptive Flexfields - Get all learning item flexfields | Learner Learning Records/Selected Course Offerings/Learning Item Descriptive Flexfields Get all learning item flexfields  |
| Learner Learning Records/Selected Course Offerings/Learning Item Rating Details - Get all learning item ratings | Learner Learning Records/Selected Course Offerings/Learning Item Rating Details Get all learning item ratings  |
| Learner Learning Records/Selected Course Offerings/Learning Item Related Materials - Get all learning item related materials | Learner Learning Records/Selected Course Offerings/Learning Item Related Materials Get all learning item related materials  |
| Learner Learning Records/Selected Course Offerings/Offering Descriptive Flexfields - Get all offering flexfields | Learner Learning Records/Selected Course Offerings/Offering Descriptive Flexfields Get all offering flexfields  |
| Learner Learning Records/Selected Course Offerings/Offering Primary Classrooms - Get all offering primary classrooms | Learner Learning Records/Selected Course Offerings/Offering Primary Classrooms Get all offering primary classrooms  |
| Learner Learning Records/Selected Course Offerings/Offering Primary Classrooms/Attachments - Get all attachments | Learner Learning Records/Selected Course Offerings/Offering Primary Classrooms/Attachments Get all attachments  |
| Learner Learning Records/Selected Course Offerings/Offering Primary Classrooms/Classroom Descriptive Flexfields - Get all classroom flexfields | Learner Learning Records/Selected Course Offerings/Offering Primary Classrooms/Classroom Descriptive Flexfields Get all classroom flexfields  |
| Learner Learning Records/Selected Course Offerings/Offering Primary Instructors - Get all offering primary instructors | Learner Learning Records/Selected Course Offerings/Offering Primary Instructors Get all offering primary instructors  |
| Learner Learning Records/Selected Course Offerings/Offering Primary Instructors/Instructor Descriptive Flexfields - Get all instructor flexfields | Learner Learning Records/Selected Course Offerings/Offering Primary Instructors/Instructor Descriptive Flexfields Get all instructor flexfields  |
| Learner Learning Records/Selected Course Offerings/Transaction Histories - Get all past transaction history | Learner Learning Records/Selected Course Offerings/Transaction Histories Get all past transaction history  |
| Learner Learning Records/Selected Course Offerings/User Action Hints - Get all user action hints | Learner Learning Records/Selected Course Offerings/User Action Hints Get all user action hints  |
| Learner Learning Records/Specialization Descriptive Flexfields - Get all specialization flexfields | Learner Learning Records/Specialization Descriptive Flexfields Get all specialization flexfields  |
| Learner Learning Records/Transaction Histories - Get all past transaction history | Learner Learning Records/Transaction Histories Get all past transaction history  |
| Learner Learning Records/User Action Hints - Get all user action hints | Learner Learning Records/User Action Hints Get all user action hints  |
| Learning Content Items - GET action not supported | Learning Content Items GET action not supported  |
| Legal Employers List of Values - Get all legal employers | Legal Employers List of Values Get all legal employers  |
| Legislative Data Groups List of Values - Get all legislative data groups | Legislative Data Groups List of Values Get all legislative data groups  |
| Life Events List of Values - Get all life events | Life Events List of Values Get all life events  |
| Locations - Get all locations | Locations Get all locations  |
| Locations List of Values - Get all locations | Locations List of Values Get all locations  |
| Locations List of Values V2 - Get all locations | Locations List of Values V2 Get all locations  |
| Locations V2 - Get all locations | Locations V2 Get all locations  |
| Locations V2/Addresses - Get all location addresses | Locations V2/Addresses Get all location addresses  |
| Locations V2/Attachments - Get all attachments | Locations V2/Attachments Get all attachments  |
| Locations V2/Locations Descriptive Flexfields - Get all flexfields | Locations V2/Locations Descriptive Flexfields Get all flexfields  |
| Locations V2/Locations Extensible Flexfields Container - Get all flexfield containers | Locations V2/Locations Extensible Flexfields Container Get all flexfield containers  |
| Locations V2/Locations Extensible Flexfields Container/Locations Extensible Flexfields - Get all flexfields | Locations V2/Locations Extensible Flexfields Container/Locations Extensible Flexfields Get all flexfields  |
| Locations V2/Locations Legislative Extensible Flexfields - Get all flexfields | Locations V2/Locations Legislative Extensible Flexfields Get all flexfields  |
| Locations/Location Descriptive Flexfields - Get all flexfields | Locations/Location Descriptive Flexfields Get all flexfields  |
| Majors List of Values - Get all majors | Majors List of Values Get all majors  |
| Message Designer Mail Services - Get all recruitingMessageDesignerMailServices | Message Designer Mail Services Get all recruitingMessageDesignerMailServices  |
| Metadata for Sensitive Personal Information Block - Get all metadata detail for the sensitive personal... | Metadata for Sensitive Personal Information Block Get all metadata detail for the sensitive personal information block  |
| My Job Referrals in Opportunity Marketplace - Get all referrals in opportunity marketplace. | My Job Referrals in Opportunity Marketplace Get all referrals in opportunity marketplace.  |
| Organization Payment Methods List of Values - Get all organization payment methods | Organization Payment Methods List of Values Get all organization payment methods  |
| Organization Tree Nodes List of Values - Get all organization tree nodes | Organization Tree Nodes List of Values Get all organization tree nodes  |
| Organizations - Get all organizations | Organizations Get all organizations  |
| Organizations/Organization Descriptive Flexfields - Get all flexfields | Organizations/Organization Descriptive Flexfields Get all flexfields  |
| Organizations/Organization Extra Information - Get all flexfields | Organizations/Organization Extra Information Get all flexfields  |
| Payroll Balance Definitions List of Values - Get all payroll balance definitions | Payroll Balance Definitions List of Values Get all payroll balance definitions  |
| Payroll Definitions List of Values - Get all payroll definitions | Payroll Definitions List of Values Get all payroll definitions  |
| Payroll Element Definitions List of Values - Get all element definitions | Payroll Element Definitions List of Values Get all element definitions  |
| Payroll Input Values List of Values - Get all payroll input values | Payroll Input Values List of Values Get all payroll input values  |
| Payroll Relationships - Get all payroll relationships | Payroll Relationships Get all payroll relationships  |
| Payroll Relationships/Payroll Assignments - Get all payroll assignments | Payroll Relationships/Payroll Assignments Get all payroll assignments  |
| Payroll Relationships/Payroll Assignments/Assigned Payrolls - Get all assigned payrolls | Payroll Relationships/Payroll Assignments/Assigned Payrolls Get all assigned payrolls  |
| Payroll Relationships/Payroll Assignments/Assigned Payrolls/Assigned Payroll Dates - Get all assigned payroll dates | Payroll Relationships/Payroll Assignments/Assigned Payrolls/Assigned Payroll Dates Get all assigned payroll dates  |
| Payroll Relationships/Payroll Assignments/Payroll Assignment Dates - Get all payroll assignment dates | Payroll Relationships/Payroll Assignments/Payroll Assignment Dates Get all payroll assignment dates  |
| Payroll Relationships/Payroll Relationship Dates - Get all payroll relationship dates | Payroll Relationships/Payroll Relationship Dates Get all payroll relationship dates  |
| Payroll Statutory Units List of Values - Get all payroll statutory units | Payroll Statutory Units List of Values Get all payroll statutory units  |
| Payroll Time Definitions List of Values - Get all payroll time definitions | Payroll Time Definitions List of Values Get all payroll time definitions  |
| Payroll Time Periods List of Values - Get all time periods | Payroll Time Periods List of Values Get all time periods  |
| Payslips - Get all payslips | Payslips Get all payslips  |
| Payslips/Documents - Get all documents | Payslips/Documents Get all documents  |
| Performance Evaluations - Get all performance documents | Performance Evaluations Get all performance documents  |
| Performance Evaluations/Roles - Get all roles | Performance Evaluations/Roles Get all roles  |
| Performance Evaluations/Roles/Participants - Get all participants | Performance Evaluations/Roles/Participants Get all participants  |
| Performance Evaluations/Roles/Participants/Tasks - Get all tasks | Performance Evaluations/Roles/Participants/Tasks Get all tasks  |
| Performance Goals - Get all goals | Performance Goals Get all goals  |
| Performance Template Document Names List Of Values - Get all document names | Performance Template Document Names List Of Values Get all document names  |
| Performance Template Task Names List Of Values - Get all task names | Performance Template Task Names List Of Values Get all task names  |
| Person Notes - Get all person notes | Person Notes Get all person notes  |
| Person Notes Visibility Options - Get all visibility options | Person Notes Visibility Options Get all visibility options  |
| Person Notes Visibility Options List of Values - Get all visibility options | Person Notes Visibility Options List of Values Get all visibility options  |
| Person Types List of Values - Get all person types | Person Types List of Values Get all person types  |
| Personal Payment Methods - Get all personal payment methods | Personal Payment Methods Get all personal payment methods  |
| Plan Balances - Get all plan balances | Plan Balances Get all plan balances  |
| Plan Balances/Plan Balance Details - Get all plan balance details | Plan Balances/Plan Balance Details Get all plan balance details  |
| Plan Balances/Plan Balance Summaries - Get all plan balance summaries | Plan Balances/Plan Balance Summaries Get all plan balance summaries  |
| Position Tree Nodes List of Values - Get all position tree nodes | Position Tree Nodes List of Values Get all position tree nodes  |
| Positions - Get all positions | Positions Get all positions  |
| Positions List of Values - Get all positions | Positions List of Values Get all positions  |
| Positions List of Values V2 - Get all positions | Positions List of Values V2 Get all positions  |
| Positions/Position Descriptive Flexfields - Get all flexfields | Positions/Position Descriptive Flexfields Get all flexfields  |
| Positions/Position Extra Information - Get all flexfields | Positions/Position Extra Information Get all flexfields  |
| Positions/Position Extra Legislative Information - Get all flexfields | Positions/Position Extra Legislative Information Get all flexfields  |
| Positions/Valid Grades - Get all valid grades | Positions/Valid Grades Get all valid grades  |
| Profile Type Sections List of Values - Get all profile type sections | Profile Type Sections List of Values Get all profile type sections  |
| Profiles List of Values - Get all profiles | Profiles List of Values Get all profiles  |
| Public Workers - Get all workers | Public Workers Get all workers  |
| Public Workers/Assignments - Get all workers assignments | Public Workers/Assignments Get all workers assignments  |
| Public Workers/Assignments/All Reports - Get all worker reports | Public Workers/Assignments/All Reports Get all worker reports  |
| Public Workers/Assignments/AllReportsDepartments - Get all departments | Public Workers/Assignments/AllReportsDepartments Get all departments  |
| Public Workers/Assignments/AllReportsLocations - Get all locations | Public Workers/Assignments/AllReportsLocations Get all locations  |
| Public Workers/Assignments/DirectReports - Get all direct reports | Public Workers/Assignments/DirectReports Get all direct reports  |
| Public Workers/Assignments/Employment History - Get all worker employment history | Public Workers/Assignments/Employment History Get all worker employment history  |
| Public Workers/Assignments/Managers - Get all worker managers | Public Workers/Assignments/Managers Get all worker managers  |
| Public Workers/Assignments/Representatives - Get all workers representatives | Public Workers/Assignments/Representatives Get all workers representatives  |
| Public Workers/Messages - Get all worker messages | Public Workers/Messages Get all worker messages  |
| Public Workers/Other Communication Accounts - Get all worker communication accounts | Public Workers/Other Communication Accounts Get all worker communication accounts  |
| Public Workers/Phones - Get all worker phones | Public Workers/Phones Get all worker phones  |
| Public Workers/Photos - Get all worker photos | Public Workers/Photos Get all worker photos  |
| Question Answers List of Values - Get all answer choices | Question Answers List of Values Get all answer choices  |
| Questionnaire Folders List of Values - Get all questionnaire folders | Questionnaire Folders List of Values Get all questionnaire folders  |
| Questionnaire Questions List of Values - Get all questions | Questionnaire Questions List of Values Get all questions  |
| Questionnaire Response Types List of Values - Get all response types | Questionnaire Response Types List of Values Get all response types  |
| Questionnaire Subscribers List of Values - Get all subscribers | Questionnaire Subscribers List of Values Get all subscribers  |
| Questionnaire Templates List of Values - Get all questionnaire templates | Questionnaire Templates List of Values Get all questionnaire templates  |
| Questionnaires - Get all questionnaires | Questionnaires Get all questionnaires  |
| Questionnaires List of Values - Get all questionnaires | Questionnaires List of Values Get all questionnaires  |
| Questionnaires/Attachments - Get all attachments | Questionnaires/Attachments Get all attachments  |
| Questionnaires/Sections - Get all sections | Questionnaires/Sections Get all sections  |
| Questionnaires/Sections/Questions - Get all questions | Questionnaires/Sections/Questions Get all questions  |
| Questionnaires/Sections/Questions/Answers - Get all answer choices | Questionnaires/Sections/Questions/Answers Get all answer choices  |
| Questions - Get all questions | Questions Get all questions  |
| Questions List of Values - Get all questions | Questions List of Values Get all questions  |
| Questions/Answers - Get all answers | Questions/Answers Get all answers  |
| Questions/Answers/Attachments - Get all attachments | Questions/Answers/Attachments Get all attachments  |
| Questions/Attachments - Get all attachments | Questions/Attachments Get all attachments  |
| Questions/Job Family Contexts - Get all  job families | Questions/Job Family Contexts Get all  job families  |
| Questions/Job Function Contexts - Get all job functions | Questions/Job Function Contexts Get all job functions  |
| Questions/Location Contexts - Get all locations | Questions/Location Contexts Get all locations  |
| Questions/Organization Contexts - Get all organization | Questions/Organization Contexts Get all organization  |
| Rating Levels List of Values - Get all rating levels | Rating Levels List of Values Get all rating levels  |
| Rating Models List of Values - Get all rating models | Rating Models List of Values Get all rating models  |
| Reasons List of Values - Get all reasons | Reasons List of Values Get all reasons  |
| Recruiting Agency Candidates - Get all candidates from the recruiting agency. | Recruiting Agency Candidates Get all candidates from the recruiting agency.  |
| Recruiting Agents - Get all recruiting agents | Recruiting Agents Get all recruiting agents  |
| Recruiting Assessment Account Packages - Get details of all recruiting assessment account p... | Recruiting Assessment Account Packages Get details of all recruiting assessment account packages.  |
| Recruiting Assessment Account Packages/Packages Included in the Recruiting Assessment Account Packages - Get all packages of recruiting assessment account ... | Recruiting Assessment Account Packages/Packages Included in the Recruiting Assessment Account Packages Get all packages of recruiting assessment account packages.  |
| Recruiting Assessment Partner Candidate Details - Get all the recruiting partner candidate details | Recruiting Assessment Partner Candidate Details Get all the recruiting partner candidate details  |
| Recruiting Assessment Partner Candidate Details/Address Details in the Recruiting Assessment Partner Candidate Details - Get all address details of the recruiting partner ... | Recruiting Assessment Partner Candidate Details/Address Details in the Recruiting Assessment Partner Candidate Details Get all address details of the recruiting partner candidate  |
| Recruiting Assessment Partner Candidate Details/Address Format in the Recruiting Assessment Partner Candidate Details - Get all address formats of the recruiting partner ... | Recruiting Assessment Partner Candidate Details/Address Format in the Recruiting Assessment Partner Candidate Details Get all address formats of the recruiting partner candidate  |
| Recruiting Assessment Partner Candidate Details/Certification Related Information in the Recruiting Partner Candidate Details - Get all certifications of the recruiting partner c... | Recruiting Assessment Partner Candidate Details/Certification Related Information in the Recruiting Partner Candidate Details Get all certifications of the recruiting partner candidate  |
| Recruiting Assessment Partner Candidate Details/Educational Qualifications Mentioned in the Recruiting Partner Candidate Details - Get all educations of the recruiting partner candi... | Recruiting Assessment Partner Candidate Details/Educational Qualifications Mentioned in the Recruiting Partner Candidate Details Get all educations of the recruiting partner candidate  |
| Recruiting Assessment Partner Candidate Details/National Identifiers of the Candidate in the Recruiting Partner Candidate Details - Get all national identifiers of the recruiting par... | Recruiting Assessment Partner Candidate Details/National Identifiers of the Candidate in the Recruiting Partner Candidate Details Get all national identifiers of the recruiting partner candidate  |
| Recruiting Assessment Partner Candidate Details/Previous Employments Recorded in the Recruiting Partner Candidate Details - Get all the previous employments of the recruiting... | Recruiting Assessment Partner Candidate Details/Previous Employments Recorded in the Recruiting Partner Candidate Details Get all the previous employments of the recruiting partner candidate  |
| Recruiting Background Check Account Packages - Get all details of the recruiting background check... | Recruiting Background Check Account Packages Get all details of the recruiting background check account packages  |
| Recruiting Background Check Account Packages/Packages Included in the Recruiting Background Check Account Packages - Get all packages of recruiting background check ac... | Recruiting Background Check Account Packages/Packages Included in the Recruiting Background Check Account Packages Get all packages of recruiting background check account packages  |
| Recruiting Background Check Candidate Results - Get all candidate results | Recruiting Background Check Candidate Results Get all candidate results  |
| Recruiting Background Check Candidate Results/Services About the Candidate Results - Get all services of candidate results | Recruiting Background Check Candidate Results/Services About the Candidate Results Get all services of candidate results  |
| Recruiting Background Check Screening Packages - Get details of all the screening packages | Recruiting Background Check Screening Packages Get details of all the screening packages  |
| Recruiting Background Check Screening Packages List - Get all background check screening packages | Recruiting Background Check Screening Packages List Get all background check screening packages  |
| Recruiting Background Check Screening Packages/Services About the Screening Packages. - Get details of all services of the screening packa... | Recruiting Background Check Screening Packages/Services About the Screening Packages. Get details of all services of the screening packages  |
| Recruiting Campaign Details - Get all recruitingCampaignDetails | Recruiting Campaign Details Get all recruitingCampaignDetails  |
| Recruiting Campaign Details/Campaign Assets - Get all campaignAssets | Recruiting Campaign Details/Campaign Assets Get all campaignAssets  |
| Recruiting Campaign Details/Campaign Assets/Campaign Asset Channels - Get all CampaignAssetChannels | Recruiting Campaign Details/Campaign Assets/Campaign Asset Channels Get all CampaignAssetChannels  |
| Recruiting Campaign Details/Campaign Assets/Campaign Asset Segments - Get all campaignAssetSegments | Recruiting Campaign Details/Campaign Assets/Campaign Asset Segments Get all campaignAssetSegments  |
| Recruiting Campaign Details/Campaign Goal Responses - Get all campaignGoalResponse | Recruiting Campaign Details/Campaign Goal Responses Get all campaignGoalResponse  |
| Recruiting Campaign Email Designs - Delete a message design | Recruiting Campaign Email Designs Delete a message design  |
| Recruiting Campaign Email Designs/Message Design Metadata - Get all message design metadata | Recruiting Campaign Email Designs/Message Design Metadata Get all message design metadata  |
| Recruiting Campaign Email Designs/Message DesignTypes - Get all message design type | Recruiting Campaign Email Designs/Message DesignTypes Get all message design type  |
| Recruiting Candidate Assessment Results - Get all recruiting candidate assessment results | Recruiting Candidate Assessment Results Get all recruiting candidate assessment results  |
| Recruiting Candidate Assessment Results/Packages of Recruiting Candidate Assessment Results - Get all packages of recruiting candidate assessmen... | Recruiting Candidate Assessment Results/Packages of Recruiting Candidate Assessment Results Get all packages of recruiting candidate assessment results  |
| Recruiting Candidate Experience Extra Information Metadata - Get all extensible flexfield metadata details | Recruiting Candidate Experience Extra Information Metadata Get all extensible flexfield metadata details  |
| Recruiting Candidate Experience Extra Information Metadata/Candidate Extra Information Metadata with Attributes - Get all extensible flexfield attribute metadata de... | Recruiting Candidate Experience Extra Information Metadata/Candidate Extra Information Metadata with Attributes Get all extensible flexfield attribute metadata details  |
| Recruiting Candidate Extra Information Details - Get all extra information of a candidate | Recruiting Candidate Extra Information Details Get all extra information of a candidate  |
| Recruiting Candidate Extra Information Details/Person Extra Information Details - Get all extra information of a person | Recruiting Candidate Extra Information Details/Person Extra Information Details Get all extra information of a person  |
| Recruiting Candidate PII Data - Get the details of all the candidate PII data | Recruiting Candidate PII Data Get the details of all the candidate PII data  |
| Recruiting Candidate Pools LOV - Get all the candidate pools LOV | Recruiting Candidate Pools LOV Get all the candidate pools LOV  |
| Recruiting Candidates - Get all candidates | Recruiting Candidates Get all candidates  |
| Recruiting Candidates/Attachments - Get all attachments | Recruiting Candidates/Attachments Get all attachments  |
| Recruiting Candidates/Candidate Addresses - Get all addresses | Recruiting Candidates/Candidate Addresses Get all addresses  |
| Recruiting Candidates/Candidate Phones - Get all phone numbers | Recruiting Candidates/Candidate Phones Get all phone numbers  |
| Recruiting Candidates/Education Items - Get all education items | Recruiting Candidates/Education Items Get all education items  |
| Recruiting Candidates/Experience Items - Get all experience items | Recruiting Candidates/Experience Items Get all experience items  |
| Recruiting Candidates/Languages - Get all languages | Recruiting Candidates/Languages Get all languages  |
| Recruiting Candidates/Licenses and Certificates - Get all licenses and certificates | Recruiting Candidates/Licenses and Certificates Get all licenses and certificates  |
| Recruiting Candidates/Skills of the Recruiting Candidates - Get all recruiting candidates skills | Recruiting Candidates/Skills of the Recruiting Candidates Get all recruiting candidates skills  |
| Recruiting Candidates/Work Preferences - Get all work preferences | Recruiting Candidates/Work Preferences Get all work preferences  |
| Recruiting Career Site Contact Information Metadata - Get all candidate contact information to be entere... | Recruiting Career Site Contact Information Metadata Get all candidate contact information to be entered on the recruiting career site  |
| Recruiting CE Address Formats - Get all address formats of countries | Recruiting CE Address Formats Get all address formats of countries  |
| Recruiting CE Address Formats/Address Format Details - Get all address formats details | Recruiting CE Address Formats/Address Format Details Get all address formats details  |
| Recruiting CE Address Formats/Geographical Hierarchies - Get all geographical hierarchies | Recruiting CE Address Formats/Geographical Hierarchies Get all geographical hierarchies  |
| Recruiting CE Address Formats/State Provinces - Get all states and provinces | Recruiting CE Address Formats/State Provinces Get all states and provinces  |
| Recruiting CE Auto Suggestions on Search. - Get details of all auto suggestions on search | Recruiting CE Auto Suggestions on Search. Get details of all auto suggestions on search  |
| Recruiting CE Candidate Certifications - Get details of all the certifications | Recruiting CE Candidate Certifications Get details of all the certifications  |
| Recruiting CE Candidate Details - Get all details of recruitingCECandidates | Recruiting CE Candidate Details Get all details of recruitingCECandidates  |
| Recruiting CE Candidate Details/Candidate Details with Attachments - Get all details of recruitingCECandidates attachme... | Recruiting CE Candidate Details/Candidate Details with Attachments Get all details of recruitingCECandidates attachments  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data - Get all extra information dictionary details of ca... | Recruiting CE Candidate Details/Candidate Details with Extra Information Data Get all extra information dictionary details of candidate  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows - Get all extra information row details for a candid... | Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows Get all extra information row details for a candidate  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows/Candidate Details with Extra Information Attributes - Get all extra information attribute details define... | Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows/Candidate Details with Extra Information Attributes Get all extra information attribute details defined for candidate extra information row  |
| Recruiting CE Candidate Details/Candidate Details with PII Data - Get all details of recruitingCECandidates Candidat... | Recruiting CE Candidate Details/Candidate Details with PII Data Get all details of recruitingCECandidates CandidatePIIData  |
| Recruiting CE Candidate Education - Get all the education items of the candidate. | Recruiting CE Candidate Education Get all the education items of the candidate.  |
| Recruiting CE Candidate Extra Information Dictionary - Get all details of the extra information dictionar... | Recruiting CE Candidate Extra Information Dictionary Get all details of the extra information dictionary  |
| Recruiting CE Candidate Extra Information Dictionary/Recruiting CE Candidate Extra Information Dictionary List of Values - Get all details of the extra information dictionar... | Recruiting CE Candidate Extra Information Dictionary/Recruiting CE Candidate Extra Information Dictionary List of Values Get all details of the extra information dictionary list  |
| Recruiting CE Candidate Interview Actions - Get all candidate interview actions | Recruiting CE Candidate Interview Actions Get all candidate interview actions  |
| Recruiting CE Candidate Preferences - Get all candidate preferences | Recruiting CE Candidate Preferences Get all candidate preferences  |
| Recruiting CE Candidate Previous Experience - Get all the candidate previous experience items. | Recruiting CE Candidate Previous Experience Get all the candidate previous experience items.  |
| Recruiting CE Candidate Profile Blocks Meta Data. - Get all the candidate profile blocks meta data | Recruiting CE Candidate Profile Blocks Meta Data. Get all the candidate profile blocks meta data  |
| Recruiting CE Candidate Responses to Questions - Get the details of all responses | Recruiting CE Candidate Responses to Questions Get the details of all responses  |
| Recruiting CE Candidate Resume Parser - Get all the candidate resumes in a collection | Recruiting CE Candidate Resume Parser Get all the candidate resumes in a collection  |
| Recruiting CE Candidate Resume Parser - Get all the candidate resumes in a collection | Recruiting CE Candidate Resume Parser Get all the candidate resumes in a collection  |
| Recruiting CE Candidate Resume Parser/Attachments - Get all the attachments of the candidate resume in... | Recruiting CE Candidate Resume Parser/Attachments Get all the attachments of the candidate resume in a collection.  |
| Recruiting CE Candidate Resume Parser/Attachments - Get all the attachments of the candidate resume in... | Recruiting CE Candidate Resume Parser/Attachments Get all the attachments of the candidate resume in a collection.  |
| Recruiting CE Candidate Resume Parser/Educations - Get all the educational details of the candidate r... | Recruiting CE Candidate Resume Parser/Educations Get all the educational details of the candidate resume in a collection  |
| Recruiting CE Candidate Resume Parser/Educations - Get all the educational details of the candidate r... | Recruiting CE Candidate Resume Parser/Educations Get all the educational details of the candidate resume in a collection  |
| Recruiting CE Candidate Resume Parser/Languages - Get all the languages of the candidate resume in a... | Recruiting CE Candidate Resume Parser/Languages Get all the languages of the candidate resume in a collection  |
| Recruiting CE Candidate Resume Parser/Languages - Get all the languages of the candidate resume in a... | Recruiting CE Candidate Resume Parser/Languages Get all the languages of the candidate resume in a collection  |
| Recruiting CE Candidate Resume Parser/Licenses and Certifications - Get all the licenses and certifications of the can... | Recruiting CE Candidate Resume Parser/Licenses and Certifications Get all the licenses and certifications of the candidate resume in a collection  |
| Recruiting CE Candidate Resume Parser/Licenses and Certifications - Get all the licenses and certifications of the can... | Recruiting CE Candidate Resume Parser/Licenses and Certifications Get all the licenses and certifications of the candidate resume in a collection  |
| Recruiting CE Candidate Resume Parser/Skills - Get all the skills of the candidate resume in a co... | Recruiting CE Candidate Resume Parser/Skills Get all the skills of the candidate resume in a collection  |
| Recruiting CE Candidate Resume Parser/Skills - Get all the skills of the candidate resume in a co... | Recruiting CE Candidate Resume Parser/Skills Get all the skills of the candidate resume in a collection  |
| Recruiting CE Candidate Resume Parser/Work Experiences - Get all the work experiences of the candidate resu... | Recruiting CE Candidate Resume Parser/Work Experiences Get all the work experiences of the candidate resume in a collection  |
| Recruiting CE Candidate Resume Parser/Work Experiences - Get all the work experiences of the candidate resu... | Recruiting CE Candidate Resume Parser/Work Experiences Get all the work experiences of the candidate resume in a collection  |
| Recruiting CE Candidate Resume Parser/Work Preferences - Get all the work preferences of the candidate resu... | Recruiting CE Candidate Resume Parser/Work Preferences Get all the work preferences of the candidate resume in a collection  |
| Recruiting CE Candidate Resume Parser/Work Preferences - Get all the work preferences of the candidate resu... | Recruiting CE Candidate Resume Parser/Work Preferences Get all the work preferences of the candidate resume in a collection  |
| Recruiting CE Candidate Site Preferences - Get all candidate site preferences | Recruiting CE Candidate Site Preferences Get all candidate site preferences  |
| Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate - Get all candidate site TCP preferences | Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate Get all candidate site TCP preferences  |
| Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate/Preferred Job Families - Get all preferred job families of candidate site T... | Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate/Preferred Job Families Get all preferred job families of candidate site TCP preferences  |
| Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate/Preferred Locations - Get all preferred locations of candidate site TCP ... | Recruiting CE Candidate Site Preferences/Preferred Site TC in the Site Preferences of the Recruiting Candidate/Preferred Locations Get all preferred locations of candidate site TCP preferences  |
| Recruiting CE Candidate Work Preferences - Get the details of all the candidate work preferen... | Recruiting CE Candidate Work Preferences Get the details of all the candidate work preferences  |
| Recruiting CE Dictionaries - Get all recruiting CE dictionaries | Recruiting CE Dictionaries Get all recruiting CE dictionaries  |
| Recruiting CE Interview Schedules - Get all interview schedules | Recruiting CE Interview Schedules Get all interview schedules  |
| Recruiting CE Interview Schedules/Recruiting CE Interviews - Gets details of all scheduled interviews | Recruiting CE Interview Schedules/Recruiting CE Interviews Gets details of all scheduled interviews  |
| Recruiting CE Interviews - Gets details of all scheduled interviews | Recruiting CE Interviews Gets details of all scheduled interviews  |
| Recruiting CE Interviews/Schedule Interview Participants - Get all interview schedules for participants | Recruiting CE Interviews/Schedule Interview Participants Get all interview schedules for participants  |
| Recruiting CE Job Referral Clicks - Get details of all requisitions clicks | Recruiting CE Job Referral Clicks Get details of all requisitions clicks  |
| Recruiting CE Job Referrals - Get all candidate referrals | Recruiting CE Job Referrals Get all candidate referrals  |
| Recruiting CE Job Referrals/Attachments in Referrals - Get all attachments for candidate referral | Recruiting CE Job Referrals/Attachments in Referrals Get all attachments for candidate referral  |
| Recruiting CE Job Requisition Details - Get all job requisition details pertaining to a pu... | Recruiting CE Job Requisition Details Get all job requisition details pertaining to a published job  |
| Recruiting CE Job Requisition Details - Get all job requisition details pertaining to a pu... | Recruiting CE Job Requisition Details Get all job requisition details pertaining to a published job  |
| Recruiting CE Job Requisition Details/Job Requisition Flexfields - Get all requisition flexfields for the requisition... | Recruiting CE Job Requisition Details/Job Requisition Flexfields Get all requisition flexfields for the requisition details  |
| Recruiting CE Job Requisition Details/Job Requisition Flexfields - Get all requisition flexfields for the requisition... | Recruiting CE Job Requisition Details/Job Requisition Flexfields Get all requisition flexfields for the requisition details  |
| Recruiting CE Job Requisition Details/Media - Get all media used in the requisition template det... | Recruiting CE Job Requisition Details/Media Get all media used in the requisition template details previews  |
| Recruiting CE Job Requisition Details/Media - Get all media used in the requisition template det... | Recruiting CE Job Requisition Details/Media Get all media used in the requisition template details previews  |
| Recruiting CE Job Requisition Details/Other Work Locations - Get all other work location for the requisition te... | Recruiting CE Job Requisition Details/Other Work Locations Get all other work location for the requisition template details previews  |
| Recruiting CE Job Requisition Details/Other Work Locations - Get all other work location for the requisition te... | Recruiting CE Job Requisition Details/Other Work Locations Get all other work location for the requisition template details previews  |
| Recruiting CE Job Requisition Details/Primary Location Coordinates - Get all primary location coordinates for the requi... | Recruiting CE Job Requisition Details/Primary Location Coordinates Get all primary location coordinates for the requisition details  |
| Recruiting CE Job Requisition Details/Secondary Locations - Get all secondary locations for the requisition te... | Recruiting CE Job Requisition Details/Secondary Locations Get all secondary locations for the requisition template details previews  |
| Recruiting CE Job Requisition Details/Secondary Locations - Get all secondary locations for the requisition te... | Recruiting CE Job Requisition Details/Secondary Locations Get all secondary locations for the requisition template details previews  |
| Recruiting CE Job Requisition Details/Work Locations - Get all work locations for the requisition templat... | Recruiting CE Job Requisition Details/Work Locations Get all work locations for the requisition template details previews  |
| Recruiting CE Job Requisition Details/Work Locations - Get all work locations for the requisition templat... | Recruiting CE Job Requisition Details/Work Locations Get all work locations for the requisition template details previews  |
| Recruiting CE Job Requisitions - Get all job requisitions | Recruiting CE Job Requisitions Get all job requisitions  |
| Recruiting CE Job Requisitions - Get all job requisitions | Recruiting CE Job Requisitions Get all job requisitions  |
| Recruiting CE Job Requisitions/Categories Facet - Get all categories facets | Recruiting CE Job Requisitions/Categories Facet Get all categories facets  |
| Recruiting CE Job Requisitions/Categories Facet - Get all categories facets | Recruiting CE Job Requisitions/Categories Facet Get all categories facets  |
| Recruiting CE Job Requisitions/Flexfields Facet - Get all flexfields facets | Recruiting CE Job Requisitions/Flexfields Facet Get all flexfields facets  |
| Recruiting CE Job Requisitions/Flexfields Facet/Flexfield Facet Values - Get all flexfields facets values | Recruiting CE Job Requisitions/Flexfields Facet/Flexfield Facet Values Get all flexfields facets values  |
| Recruiting CE Job Requisitions/Locations Facet - Get all location facets | Recruiting CE Job Requisitions/Locations Facet Get all location facets  |
| Recruiting CE Job Requisitions/Locations Facet - Get all location facets | Recruiting CE Job Requisitions/Locations Facet Get all location facets  |
| Recruiting CE Job Requisitions/Organizations Facet - Get all organization facets | Recruiting CE Job Requisitions/Organizations Facet Get all organization facets  |
| Recruiting CE Job Requisitions/Posting Dates Facet - Get all posting date facets | Recruiting CE Job Requisitions/Posting Dates Facet Get all posting date facets  |
| Recruiting CE Job Requisitions/Posting Dates Facet - Get all posting date facets | Recruiting CE Job Requisitions/Posting Dates Facet Get all posting date facets  |
| Recruiting CE Job Requisitions/Requisition List - Get all requisition lists | Recruiting CE Job Requisitions/Requisition List Get all requisition lists  |
| Recruiting CE Job Requisitions/Requisition List - Get all requisition lists | Recruiting CE Job Requisitions/Requisition List Get all requisition lists  |
| Recruiting CE Job Requisitions/Requisition List/Other Work Locations - Get all other work location for the requisition te... | Recruiting CE Job Requisitions/Requisition List/Other Work Locations Get all other work location for the requisition template details previews  |
| Recruiting CE Job Requisitions/Requisition List/Other Work Locations - Get all other work location for the requisition te... | Recruiting CE Job Requisitions/Requisition List/Other Work Locations Get all other work location for the requisition template details previews  |
| Recruiting CE Job Requisitions/Requisition List/Primary Location Coordinates - Get all primary location coordinates for the requi... | Recruiting CE Job Requisitions/Requisition List/Primary Location Coordinates Get all primary location coordinates for the requisition details  |
| Recruiting CE Job Requisitions/Requisition List/Secondary Locations - Get all secondary locations for the requisition te... | Recruiting CE Job Requisitions/Requisition List/Secondary Locations Get all secondary locations for the requisition template details previews  |
| Recruiting CE Job Requisitions/Requisition List/Secondary Locations - Get all secondary locations for the requisition te... | Recruiting CE Job Requisitions/Requisition List/Secondary Locations Get all secondary locations for the requisition template details previews  |
| Recruiting CE Job Requisitions/Requisition List/Work Locations - Get all work locations for the requisition templat... | Recruiting CE Job Requisitions/Requisition List/Work Locations Get all work locations for the requisition template details previews  |
| Recruiting CE Job Requisitions/Requisition List/Work Locations - Get all work locations for the requisition templat... | Recruiting CE Job Requisitions/Requisition List/Work Locations Get all work locations for the requisition template details previews  |
| Recruiting CE Job Requisitions/Requisition Locations Coordinates - Get all requisitions locations coordinates | Recruiting CE Job Requisitions/Requisition Locations Coordinates Get all requisitions locations coordinates  |
| Recruiting CE Job Requisitions/Requisition Locations Coordinates/Location Coordinates - Get all locations coordinates | Recruiting CE Job Requisitions/Requisition Locations Coordinates/Location Coordinates Get all locations coordinates  |
| Recruiting CE Job Requisitions/Titles Facet - Get all titles facets | Recruiting CE Job Requisitions/Titles Facet Get all titles facets  |
| Recruiting CE Job Requisitions/Titles Facet - Get all titles facets | Recruiting CE Job Requisitions/Titles Facet Get all titles facets  |
| Recruiting CE Job Requisitions/Work Locations Facet - Get all work location facets | Recruiting CE Job Requisitions/Work Locations Facet Get all work location facets  |
| Recruiting CE Job Requisitions/Work Locations Facet - Get all work location facets | Recruiting CE Job Requisitions/Work Locations Facet Get all work location facets  |
| Recruiting CE Languages - Get all the language items for the candidate. | Recruiting CE Languages Get all the language items for the candidate.  |
| Recruiting CE Name Styles - Get all recruiting CE name style | Recruiting CE Name Styles Get all recruiting CE name style  |
| Recruiting CE Offer Details - Get all details of job offers | Recruiting CE Offer Details Get all details of job offers  |
| Recruiting CE Offer Details - Get the details of all recruiting candidate assess... | Recruiting CE Offer Details Get the details of all recruiting candidate assessments  |
| Recruiting CE Offer Details/Attachments - Get all details of all offer attachments | Recruiting CE Offer Details/Attachments Get all details of all offer attachments  |
| Recruiting CE Offer Details/Candidate Reasons to Decline the Job Offer - Get all the reasons that candidates can provide wh... | Recruiting CE Offer Details/Candidate Reasons to Decline the Job Offer Get all the reasons that candidates can provide while declining the job offer.  |
| Recruiting CE Offer Details/Job Offer Assignments - Get all the job assignment types | Recruiting CE Offer Details/Job Offer Assignments Get all the job assignment types  |
| Recruiting CE Offer Details/Job Offer Letters - Get all the job offer letters | Recruiting CE Offer Details/Job Offer Letters Get all the job offer letters  |
| Recruiting CE Offer Details/Media Links in the Job Offers - Get all media link URLs | Recruiting CE Offer Details/Media Links in the Job Offers Get all media link URLs  |
| Recruiting CE Offer Responses - Get all offer responses | Recruiting CE Offer Responses Get all offer responses  |
| Recruiting CE Profile Import Attachments - Get all profile import attachments | Recruiting CE Profile Import Attachments Get all profile import attachments  |
| Recruiting CE Profile Import Configurations - Get details of all the profile import configuratio... | Recruiting CE Profile Import Configurations Get details of all the profile import configuration  |
| Recruiting CE Prospects - Get all recruitingCEProspects | Recruiting CE Prospects Get all recruitingCEProspects  |
| Recruiting CE Questions - Get details of all questions | Recruiting CE Questions Get details of all questions  |
| Recruiting CE Questions/Answers to the Questions - Get details of all answers to the question | Recruiting CE Questions/Answers to the Questions Get details of all answers to the question  |
| Recruiting CE Regulatory Configurations - Get all secured regulatory configurations | Recruiting CE Regulatory Configurations Get all secured regulatory configurations  |
| Recruiting CE Regulatory Configurations - Get all secured regulatory configurations | Recruiting CE Regulatory Configurations Get all secured regulatory configurations  |
| Recruiting CE Sites - Get all recruitingCESites | Recruiting CE Sites Get all recruitingCESites  |
| Recruiting CE Sites/Cookie Consent - Get all cookie consent in recruitingCESites | Recruiting CE Sites/Cookie Consent Get all cookie consent in recruitingCESites  |
| Recruiting CE Sites/Cookie Consent/Translations - Get all translations of cookie consent in recruiti... | Recruiting CE Sites/Cookie Consent/Translations Get all translations of cookie consent in recruitingCESites  |
| Recruiting CE Sites/Filters - Get all filters in recruitingCESites | Recruiting CE Sites/Filters Get all filters in recruitingCESites  |
| Recruiting CE Sites/Foot Links - Get all footer links in recruitingCESites | Recruiting CE Sites/Foot Links Get all footer links in recruitingCESites  |
| Recruiting CE Sites/Foot Links/Header Link Translations - Get all header translations in recruitingCESites | Recruiting CE Sites/Foot Links/Header Link Translations Get all header translations in recruitingCESites  |
| Recruiting CE Sites/Foot Links/Sublinks - Get all child links used in the recruiting sites | Recruiting CE Sites/Foot Links/Sublinks Get all child links used in the recruiting sites  |
| Recruiting CE Sites/Foot Links/Sublinks/Header Link Translations - Get all header translations in recruitingCESites | Recruiting CE Sites/Foot Links/Sublinks/Header Link Translations Get all header translations in recruitingCESites  |
| Recruiting CE Sites/Header Links - Get all header links in recruitingCESites | Recruiting CE Sites/Header Links Get all header links in recruitingCESites  |
| Recruiting CE Sites/Header Links/Header Link Translations - Get all header translations in recruitingCESites | Recruiting CE Sites/Header Links/Header Link Translations Get all header translations in recruitingCESites  |
| Recruiting CE Sites/Header Links/Sublinks - Get all child links used in the recruiting sites | Recruiting CE Sites/Header Links/Sublinks Get all child links used in the recruiting sites  |
| Recruiting CE Sites/Header Links/Sublinks/Header Link Translations - Get all header translations in recruitingCESites | Recruiting CE Sites/Header Links/Sublinks/Header Link Translations Get all header translations in recruitingCESites  |
| Recruiting CE Sites/Languages - Get all languages in recruitingCESites | Recruiting CE Sites/Languages Get all languages in recruitingCESites  |
| Recruiting CE Sites/Pages - Get all pages in recruitingCESites | Recruiting CE Sites/Pages Get all pages in recruitingCESites  |
| Recruiting CE Sites/Pages/Page Translations - Get all page translations in recruitingCESites | Recruiting CE Sites/Pages/Page Translations Get all page translations in recruitingCESites  |
| Recruiting CE Sites/Pages/Section Parameters - Get all section parameters in the sections of page... | Recruiting CE Sites/Pages/Section Parameters Get all section parameters in the sections of pages  |
| Recruiting CE Sites/Pages/Sections - Get all sections in the pages of recruitingCESites | Recruiting CE Sites/Pages/Sections Get all sections in the pages of recruitingCESites  |
| Recruiting CE Sites/Pages/Sections/Components - Get all components in the sections of pages | Recruiting CE Sites/Pages/Sections/Components Get all components in the sections of pages  |
| Recruiting CE Sites/Pages/Sections/Components/Component Translations - Get all component translations in the sections of ... | Recruiting CE Sites/Pages/Sections/Components/Component Translations Get all component translations in the sections of pages  |
| Recruiting CE Sites/Pages/Sections/Components/Section Parameters - Get all section parameters in the sections of page... | Recruiting CE Sites/Pages/Sections/Components/Section Parameters Get all section parameters in the sections of pages  |
| Recruiting CE Sites/Pages/Sections/Section Parameters - Get all section parameters in the sections of page... | Recruiting CE Sites/Pages/Sections/Section Parameters Get all section parameters in the sections of pages  |
| Recruiting CE Sites/Settings - Get all settings in recruitingCESites | Recruiting CE Sites/Settings Get all settings in recruitingCESites  |
| Recruiting CE Sites/Site Custom Fonts - Get all custom fonts in recruitingCESites | Recruiting CE Sites/Site Custom Fonts Get all custom fonts in recruitingCESites  |
| Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings - Get all custom font settings in recruitingCESites | Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings Get all custom font settings in recruitingCESites  |
| Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings/Site Custom Font Setting Files - Get all custom font setting files in recruitingCES... | Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings/Site Custom Font Setting Files Get all custom font setting files in recruitingCESites  |
| Recruiting CE Sites/Site Favicon - Get all favorite icons in recruitingCESites | Recruiting CE Sites/Site Favicon Get all favorite icons in recruitingCESites  |
| Recruiting CE Sites/Site Translations - Get all translations in recruitingCESites | Recruiting CE Sites/Site Translations Get all translations in recruitingCESites  |
| Recruiting CE Sites/Talent Community Sign Up - Get all talent community sign up in recruitingCESi... | Recruiting CE Sites/Talent Community Sign Up Get all talent community sign up in recruitingCESites  |
| Recruiting CE Sites/Talent Community Sign Up/Translations for the Talent Community Sign Up - Get all talent community enrollment translations i... | Recruiting CE Sites/Talent Community Sign Up/Translations for the Talent Community Sign Up Get all talent community enrollment translations in recruitingCESites  |
| Recruiting CE Skill Items - Get all the skill items for the candidate. | Recruiting CE Skill Items Get all the skill items for the candidate.  |
| Recruiting CE Source Tracking - Get details of all recruitingCESourceTrackings | Recruiting CE Source Tracking Get details of all recruitingCESourceTrackings  |
| Recruiting CE Tax Credits - Get all tax credits candidate URLs | Recruiting CE Tax Credits Get all tax credits candidate URLs  |
| Recruiting CE Themes - Get details of all themes in recruitingCETemplates | Recruiting CE Themes Get details of all themes in recruitingCETemplates  |
| Recruiting CE Themes/Brands in Themes - Get details of all brands in recruitingCEThemes | Recruiting CE Themes/Brands in Themes Get details of all brands in recruitingCEThemes  |
| Recruiting CE Themes/Brands in Themes/Brand Translations of Themes - Get all details of recruitingCEThemes Brand Transl... | Recruiting CE Themes/Brands in Themes/Brand Translations of Themes Get all details of recruitingCEThemes Brand Translation  |
| Recruiting CE Themes/Footer Links in Themes - Get details of all footer links in recruitingCEThe... | Recruiting CE Themes/Footer Links in Themes Get details of all footer links in recruitingCEThemes  |
| Recruiting CE Themes/Footer Links in Themes/Translations Used in Brands in Themes - Get details of all translations in recruitingCEThe... | Recruiting CE Themes/Footer Links in Themes/Translations Used in Brands in Themes Get details of all translations in recruitingCEThemes  |
| Recruiting CE Themes/Header Links in Themes - Get details of all header links in recruitingCEThe... | Recruiting CE Themes/Header Links in Themes Get details of all header links in recruitingCEThemes  |
| Recruiting CE Themes/Header Links in Themes/Translations Used in Brands in Themes - Get details of all translations in recruitingCEThe... | Recruiting CE Themes/Header Links in Themes/Translations Used in Brands in Themes Get details of all translations in recruitingCEThemes  |
| Recruiting CE Themes/Styles in Themes - Get details of all styles in recruitingCEThemes | Recruiting CE Themes/Styles in Themes Get details of all styles in recruitingCEThemes  |
| Recruiting CE Themes/Translations in a Template - Get details of all translations in recruitingCETem... | Recruiting CE Themes/Translations in a Template Get details of all translations in recruitingCETemplates  |
| Recruiting CE User Tracking - Get all users tracking information | Recruiting CE User Tracking Get all users tracking information  |
| Recruiting CE Verification - Get details of all verification token | Recruiting CE Verification Get details of all verification token  |
| Recruiting Content Library Items LOV - Get all content library items LOV | Recruiting Content Library Items LOV Get all content library items LOV  |
| Recruiting Document Format Converter - Get all the document format converters | Recruiting Document Format Converter Get all the document format converters  |
| Recruiting Geography Hierarchy Locations LOV - Get the details of all geography hierarchy locatio... | Recruiting Geography Hierarchy Locations LOV Get the details of all geography hierarchy locations LOV  |
| Recruiting Hierarchy Locations - Get all hierarchy locations | Recruiting Hierarchy Locations Get all hierarchy locations  |
| Recruiting Internal Candidate Job Applications - Get all the internal candidate job applications | Recruiting Internal Candidate Job Applications Get all the internal candidate job applications  |
| Recruiting Job Applications - Get all job applications | Recruiting Job Applications Get all job applications  |
| Recruiting Job Applications/Attachments - Get all attachments | Recruiting Job Applications/Attachments Get all attachments  |
| Recruiting Job Applications/Education Items - Get all education items | Recruiting Job Applications/Education Items Get all education items  |
| Recruiting Job Applications/Experience Items - Get all experience items | Recruiting Job Applications/Experience Items Get all experience items  |
| Recruiting Job Applications/Languages - Get all language items | Recruiting Job Applications/Languages Get all language items  |
| Recruiting Job Applications/Licenses And Certificates - Get all licenses and certificates | Recruiting Job Applications/Licenses And Certificates Get all licenses and certificates  |
| Recruiting Job Applications/WorkPreferences - Get all work preferences | Recruiting Job Applications/WorkPreferences Get all work preferences  |
| Recruiting Job Families - Get all recruiting job families | Recruiting Job Families Get all recruiting job families  |
| Recruiting Job Offers - Get all offers | Recruiting Job Offers Get all offers  |
| Recruiting Job Offers/Attachments - Get all attachments | Recruiting Job Offers/Attachments Get all attachments  |
| Recruiting Job Offers/Collaborators - Get all collaborators | Recruiting Job Offers/Collaborators Get all collaborators  |
| Recruiting Job Offers/Offer Descriptive Flexfields - Get all offer descriptive flexfields | Recruiting Job Offers/Offer Descriptive Flexfields Get all offer descriptive flexfields  |
| Recruiting Job Requisition Distribution - Get the details of all job distributions | Recruiting Job Requisition Distribution Get the details of all job distributions  |
| Recruiting Job Requisition Distribution Posting Details - Get details of all job postings | Recruiting Job Requisition Distribution Posting Details Get details of all job postings  |
| Recruiting Job Requisition Distribution Posting Details/Job Posting on the Job Boards - Get details of all job postings on job boards | Recruiting Job Requisition Distribution Posting Details/Job Posting on the Job Boards Get details of all job postings on job boards  |
| Recruiting Job Requisition Distribution/Requisition Languages - Get the details of all the required languages | Recruiting Job Requisition Distribution/Requisition Languages Get the details of all the required languages  |
| Recruiting Job Requisition Distribution/Requisition Languages/Secondary Locations - Get the details of all the media | Recruiting Job Requisition Distribution/Requisition Languages/Secondary Locations Get the details of all the media  |
| Recruiting Job Requisition Distribution/Requisition Languages/Secondary Locations - Get the details of all the secondary locations | Recruiting Job Requisition Distribution/Requisition Languages/Secondary Locations Get the details of all the secondary locations  |
| Recruiting Job Requisition Template Details Preview - Get all requisition template details previews | Recruiting Job Requisition Template Details Preview Get all requisition template details previews  |
| Recruiting Job Requisition Template Details Preview/Job Requisition Flexfields - Get all requisition flexfields for the requisition... | Recruiting Job Requisition Template Details Preview/Job Requisition Flexfields Get all requisition flexfields for the requisition details  |
| Recruiting Job Requisition Template Details Preview/Media - Get all media used in the requisition template det... | Recruiting Job Requisition Template Details Preview/Media Get all media used in the requisition template details previews  |
| Recruiting Job Requisition Template Details Preview/Other Work Locations - Get all other work location for the requisition te... | Recruiting Job Requisition Template Details Preview/Other Work Locations Get all other work location for the requisition template details previews  |
| Recruiting Job Requisition Template Details Preview/Secondary Locations - Get all secondary locations for the requisition te... | Recruiting Job Requisition Template Details Preview/Secondary Locations Get all secondary locations for the requisition template details previews  |
| Recruiting Job Requisition Template Details Preview/Work Locations - Get all work locations for the requisition templat... | Recruiting Job Requisition Template Details Preview/Work Locations Get all work locations for the requisition template details previews  |
| Recruiting Job Requisition Template LOV - Get details of all job requisitions templates LOV | Recruiting Job Requisition Template LOV Get details of all job requisitions templates LOV  |
| Recruiting Job Requisitions - Get all requisitions | Recruiting Job Requisitions Get all requisitions  |
| Recruiting Job Requisitions LOV - Get details of all the job requisitions LOV | Recruiting Job Requisitions LOV Get details of all the job requisitions LOV  |
| Recruiting Job Requisitions/Attachments - Get all attachments | Recruiting Job Requisitions/Attachments Get all attachments  |
| Recruiting Job Requisitions/Collaborators - Get all collaborators | Recruiting Job Requisitions/Collaborators Get all collaborators  |
| Recruiting Job Requisitions/Languages - Get all languages | Recruiting Job Requisitions/Languages Get all languages  |
| Recruiting Job Requisitions/Media Links - Get all media links | Recruiting Job Requisitions/Media Links Get all media links  |
| Recruiting Job Requisitions/Media Links/Media Languages - Get all media languages of a media link | Recruiting Job Requisitions/Media Links/Media Languages Get all media languages of a media link  |
| Recruiting Job Requisitions/Other Locations - Get all other locations | Recruiting Job Requisitions/Other Locations Get all other locations  |
| Recruiting Job Requisitions/Other Work Locations - Get all other work locations | Recruiting Job Requisitions/Other Work Locations Get all other work locations  |
| Recruiting Job Requisitions/Published Jobs - Get all published jobs | Recruiting Job Requisitions/Published Jobs Get all published jobs  |
| Recruiting Job Requisitions/Requisition Descriptive Flexfields - Get all requisition descriptive flexfields | Recruiting Job Requisitions/Requisition Descriptive Flexfields Get all requisition descriptive flexfields  |
| Recruiting LinkedIn Event Notifications - Get details about all notifications received from ... | Recruiting LinkedIn Event Notifications Get details about all notifications received from LinkedIn  |
| Recruiting Locations LOV - Get all recruitingLocations | Recruiting Locations LOV Get all recruitingLocations  |
| Recruiting Organizations LOV - Get all recruiting organizations LOV | Recruiting Organizations LOV Get all recruiting organizations LOV  |
| Recruiting Representatives LOV - Get all representatives in the list | Recruiting Representatives LOV Get all representatives in the list  |
| Recruiting Requisition Questions LOV - Get all the requisition questions | Recruiting Requisition Questions LOV Get all the requisition questions  |
| Recruiting SecondaryApply Flows - Get all recruiting secondary application flows | Recruiting SecondaryApply Flows Get all recruiting secondary application flows  |
| Recruiting SecondaryApply Flows/Requisition Flexfields - Get all requisition flexfields | Recruiting SecondaryApply Flows/Requisition Flexfields Get all requisition flexfields  |
| Recruiting SecondaryApply Flows/Sections - Get all sections of a recruiting secondary applica... | Recruiting SecondaryApply Flows/Sections Get all sections of a recruiting secondary application flow  |
| Recruiting SecondaryApply Flows/Sections/Pages - Get all pages of a recruiting secondary applicatio... | Recruiting SecondaryApply Flows/Sections/Pages Get all pages of a recruiting secondary application flow  |
| Recruiting SecondaryApply Flows/Sections/Pages/Blocks - Get all blocks of a recruiting secondary applicati... | Recruiting SecondaryApply Flows/Sections/Pages/Blocks Get all blocks of a recruiting secondary application flow  |
| Recruiting Tax Credit Accounts - Get details of all tax credits accounts | Recruiting Tax Credit Accounts Get details of all tax credits accounts  |
| Recruiting Tax Credit Accounts/Recruiting Tax Credit Account Packages - Get details of all tax credits account packages | Recruiting Tax Credit Accounts/Recruiting Tax Credit Account Packages Get details of all tax credits account packages  |
| Recruiting Tax Credits Candidate Details - Gets details for all job applications | Recruiting Tax Credits Candidate Details Gets details for all job applications  |
| Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Address Details - Get all the address details of the candidate | Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Address Details Get all the address details of the candidate  |
| Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Address Format - Get all address formats of the candidate | Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Address Format Get all address formats of the candidate  |
| Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Candidate National Identifiers - Get all candidate national identifiers | Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Candidate National Identifiers Get all candidate national identifiers  |
| Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Other Work Locations - Gets all the job requisition work locations | Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Other Work Locations Gets all the job requisition work locations  |
| Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Other Work Locations - Gets all the job requisition work locations | Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Other Work Locations Gets all the job requisition work locations  |
| Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Other Work Locations - Gets all the job requisition work locations | Recruiting Tax Credits Candidate Details/Recruiting Tax Credits Other Work Locations Gets all the job requisition work locations  |
| Recruiting Tax Credits Results - Get details of all tax credit screening results | Recruiting Tax Credits Results Get details of all tax credit screening results  |
| Recruiting Tax Credits Results/Recruiting Tax Credits Package Results - Get details of a tax credit result for all package... | Recruiting Tax Credits Results/Recruiting Tax Credits Package Results Get details of a tax credit result for all packages  |
| Recruiting Tax Credits Results/Recruiting Tax Credits Package Results/Other Eligible Credits for Recruiting Tax Credits Package Results - Get details of all other eligible credits for a ta... | Recruiting Tax Credits Results/Recruiting Tax Credits Package Results/Other Eligible Credits for Recruiting Tax Credits Package Results Get details of all other eligible credits for a tax credit screening  |
| Reporting Establishments List of Values - Get all reporting establishments | Reporting Establishments List of Values Get all reporting establishments  |
| Responsibility Templates List of Values - Get all responsibility templates | Responsibility Templates List of Values Get all responsibility templates  |
| Review Periods List of Values - Get all review periods | Review Periods List of Values Get all review periods  |
| Roles List of Values - Get all roles | Roles List of Values Get all roles  |
| Salaries - Get all salaries for a worker's assignment | Salaries Get all salaries for a worker's assignment  |
| Salaries/Salary Components - Get all components | Salaries/Salary Components Get all components  |
| Salaries/Salary Rate Components - Get all rate components | Salaries/Salary Rate Components Get all rate components  |
| Salaries/Salary Simple Components - Get all simple components | Salaries/Salary Simple Components Get all simple components  |
| Salary Basis List of Values - Get all salary bases | Salary Basis List of Values Get all salary bases  |
| Salary Basis List of Values/Salary Basis Components - Get all components | Salary Basis List of Values/Salary Basis Components Get all components  |
| Schedule Requests - Get all schedule requests | Schedule Requests Get all schedule requests  |
| Schedule Requests/Schedule Events - Get all schedule events | Schedule Requests/Schedule Events Get all schedule events  |
| Schedule Requests/Schedule Events/Schedule Shift Events - Get all schedule shift events | Schedule Requests/Schedule Events/Schedule Shift Events Get all schedule shift events  |
| Schedule Requests/Schedule Events/Schedule Shift Events/Schedule Shift Attributes - Get all schedule shift attributes | Schedule Requests/Schedule Events/Schedule Shift Events/Schedule Shift Attributes Get all schedule shift attributes  |
| Scheduling Shifts List of Values - Get all shifts | Scheduling Shifts List of Values Get all shifts  |
| Self Details - Get all self details | Self Details Get all self details  |
| Sets List of Values - Get all sets | Sets List of Values Get all sets  |
| Shared Performance Goals List of Values - Get all shared goals | Shared Performance Goals List of Values Get all shared goals  |
| Status Change Requests - Get all status change  requests | Status Change Requests Get all status change  requests  |
| Status Change Requests/Status Changes - Get all status change  records | Status Change Requests/Status Changes Get all status change  records  |
| Succession Plans List of Values - Get all plans | Succession Plans List of Values Get all plans  |
| Talent External Candidates List of Values - Get all external candidates | Talent External Candidates List of Values Get all external candidates  |
| Talent Person Profiles - Get all person profiles | Talent Person Profiles Get all person profiles  |
| Talent Person Profiles/Accomplishment Sections - Get all accomplishment sections | Talent Person Profiles/Accomplishment Sections Get all accomplishment sections  |
| Talent Person Profiles/Accomplishment Sections/Accomplishment Items - Get all accomplishment items | Talent Person Profiles/Accomplishment Sections/Accomplishment Items Get all accomplishment items  |
| Talent Person Profiles/Accomplishment Sections/Accomplishment Items/Accomplishment Items Descriptive Flexfields - Get all flexfields | Talent Person Profiles/Accomplishment Sections/Accomplishment Items/Accomplishment Items Descriptive Flexfields Get all flexfields  |
| Talent Person Profiles/Areas of Expertises - Get all areas of expertise | Talent Person Profiles/Areas of Expertises Get all areas of expertise  |
| Talent Person Profiles/Areas of Interests - Get all areas of interest | Talent Person Profiles/Areas of Interests Get all areas of interest  |
| Talent Person Profiles/Attachments - Get all attachments | Talent Person Profiles/Attachments Get all attachments  |
| Talent Person Profiles/Career Preference Sections - Get all career preference sections | Talent Person Profiles/Career Preference Sections Get all career preference sections  |
| Talent Person Profiles/Career Preference Sections/Career Preference Items - Get all career preference items | Talent Person Profiles/Career Preference Sections/Career Preference Items Get all career preference items  |
| Talent Person Profiles/Career Preference Sections/Career Preference Items/Career Preference Items Descriptive Flexfields - Get all flexfields | Talent Person Profiles/Career Preference Sections/Career Preference Items/Career Preference Items Descriptive Flexfields Get all flexfields  |
| Talent Person Profiles/Certification Sections - Get all certification sections | Talent Person Profiles/Certification Sections Get all certification sections  |
| Talent Person Profiles/Certification Sections/Certification Items - Get all certification items | Talent Person Profiles/Certification Sections/Certification Items Get all certification items  |
| Talent Person Profiles/Certification Sections/Certification Items/Certification Items Descriptive Flexfields - Get all flexfields | Talent Person Profiles/Certification Sections/Certification Items/Certification Items Descriptive Flexfields Get all flexfields  |
| Talent Person Profiles/Competency Sections - Get all competency sections | Talent Person Profiles/Competency Sections Get all competency sections  |
| Talent Person Profiles/Competency Sections/Competency Items - Get all competency items | Talent Person Profiles/Competency Sections/Competency Items Get all competency items  |
| Talent Person Profiles/Competency Sections/Competency Items/Competency Items Descriptive Flexfields - Get all flexfields | Talent Person Profiles/Competency Sections/Competency Items/Competency Items Descriptive Flexfields Get all flexfields  |
| Talent Person Profiles/Education Sections - Get all education sections | Talent Person Profiles/Education Sections Get all education sections  |
| Talent Person Profiles/Education Sections/Education Items - Get all education items | Talent Person Profiles/Education Sections/Education Items Get all education items  |
| Talent Person Profiles/Education Sections/Education Items/Education Items Descriptive Flexfields - Get all flexfields | Talent Person Profiles/Education Sections/Education Items/Education Items Descriptive Flexfields Get all flexfields  |
| Talent Person Profiles/Favorite Links - Get all favorite links | Talent Person Profiles/Favorite Links Get all favorite links  |
| Talent Person Profiles/Honor Sections - Get all honor sections | Talent Person Profiles/Honor Sections Get all honor sections  |
| Talent Person Profiles/Honor Sections/Honor Items - Get all honor items | Talent Person Profiles/Honor Sections/Honor Items Get all honor items  |
| Talent Person Profiles/Honor Sections/Honor Items/Honor Items Descriptive Flexfields - Get all flexfields | Talent Person Profiles/Honor Sections/Honor Items/Honor Items Descriptive Flexfields Get all flexfields  |
| Talent Person Profiles/Language Sections - Get all language sections | Talent Person Profiles/Language Sections Get all language sections  |
| Talent Person Profiles/Language Sections/Language Items - Get all language items | Talent Person Profiles/Language Sections/Language Items Get all language items  |
| Talent Person Profiles/Language Sections/Language Items/Language Items Descriptive Flexfields - Get all flexfields | Talent Person Profiles/Language Sections/Language Items/Language Items Descriptive Flexfields Get all flexfields  |
| Talent Person Profiles/Membership Sections - Get all membership sections | Talent Person Profiles/Membership Sections Get all membership sections  |
| Talent Person Profiles/Membership Sections/Membership Items - Get all membership items | Talent Person Profiles/Membership Sections/Membership Items Get all membership items  |
| Talent Person Profiles/Membership Sections/Membership Items/Membership Items Descriptive Flexfields - Get all flexfields | Talent Person Profiles/Membership Sections/Membership Items/Membership Items Descriptive Flexfields Get all flexfields  |
| Talent Person Profiles/Skill Sections - Get all skill sections | Talent Person Profiles/Skill Sections Get all skill sections  |
| Talent Person Profiles/Skill Sections/Skill Items - Get all skill items | Talent Person Profiles/Skill Sections/Skill Items Get all skill items  |
| Talent Person Profiles/Skill Sections/Skill Items/Skill Items Descriptive Flexfields - Get all flexfields | Talent Person Profiles/Skill Sections/Skill Items/Skill Items Descriptive Flexfields Get all flexfields  |
| Talent Person Profiles/Special Project Sections - Get all special project sections | Talent Person Profiles/Special Project Sections Get all special project sections  |
| Talent Person Profiles/Special Project Sections/Special Project Items - Get all special project items | Talent Person Profiles/Special Project Sections/Special Project Items Get all special project items  |
| Talent Person Profiles/Special Project Sections/Special Project Items/Special Project Items Descriptive Flexfields - Get all flexfields | Talent Person Profiles/Special Project Sections/Special Project Items/Special Project Items Descriptive Flexfields Get all flexfields  |
| Talent Person Profiles/Tags - Get all tags | Talent Person Profiles/Tags Get all tags  |
| Talent Person Profiles/Work History Sections - Get all work history sections | Talent Person Profiles/Work History Sections Get all work history sections  |
| Talent Person Profiles/Work History Sections/Work History Items - Get all work history items | Talent Person Profiles/Work History Sections/Work History Items Get all work history items  |
| Talent Person Profiles/Work History Sections/Work History Items/Work History Items Descriptive Flexfields - Get all flexfields | Talent Person Profiles/Work History Sections/Work History Items/Work History Items Descriptive Flexfields Get all flexfields  |
| Talent Person Profiles/Work Preference Sections - Get all work preference sections | Talent Person Profiles/Work Preference Sections Get all work preference sections  |
| Talent Person Profiles/Work Preference Sections/Work Preference Items - Get all work preference items | Talent Person Profiles/Work Preference Sections/Work Preference Items Get all work preference items  |
| Talent Person Profiles/Work Preference Sections/Work Preference Items/Work Preference Items Descriptive Flexfields - Get all flexfields | Talent Person Profiles/Work Preference Sections/Work Preference Items/Work Preference Items Descriptive Flexfields Get all flexfields  |
| Talent Pools List of Values - Get all talent pools | Talent Pools List of Values Get all talent pools  |
| Talent Ratings - Get all talent ratings | Talent Ratings Get all talent ratings  |
| Talent Ratings/Career Potential - Get all career potentials | Talent Ratings/Career Potential Get all career potentials  |
| Talent Ratings/Career Potential/Career Potential Descriptive Flexfields - Get all flexfields | Talent Ratings/Career Potential/Career Potential Descriptive Flexfields Get all flexfields  |
| Talent Ratings/Impact Of Losses - Get all impacts of losses | Talent Ratings/Impact Of Losses Get all impacts of losses  |
| Talent Ratings/nBox Cell Assignments - Get all nBox cell assignments | Talent Ratings/nBox Cell Assignments Get all nBox cell assignments  |
| Talent Ratings/Performance Ratings - Get all performance ratings | Talent Ratings/Performance Ratings Get all performance ratings  |
| Talent Ratings/Performance Ratings/Performance Rating Descriptive Flexfields - Get all flexfields | Talent Ratings/Performance Ratings/Performance Rating Descriptive Flexfields Get all flexfields  |
| Talent Ratings/Risk Of Losses - Get all risks of losses | Talent Ratings/Risk Of Losses Get all risks of losses  |
| Talent Ratings/Risk Of Losses/Risk Of Losses Descriptive Flexfields - Get all flexfields | Talent Ratings/Risk Of Losses/Risk Of Losses Descriptive Flexfields Get all flexfields  |
| Talent Ratings/Talent Scores - Get all talent scores | Talent Ratings/Talent Scores Get all talent scores  |
| Talent Ratings/Talent Scores/Talent Score Descriptive Flexfields - Get all flexfields | Talent Ratings/Talent Scores/Talent Score Descriptive Flexfields Get all flexfields  |
| Talent Review Managers List of Values - Get all managers | Talent Review Managers List of Values Get all managers  |
| Target  Date Migrations - Get all target date migrations | Target  Date Migrations Get all target date migrations  |
| Tasks - Get all tasks | Tasks Get all tasks  |
| Tasks/Changed Attributes - Get all changed attributes | Tasks/Changed Attributes Get all changed attributes  |
| Tax Reporting Units List of Values - Get all tax reporting units | Tax Reporting Units List of Values Get all tax reporting units  |
| Team Members List of Values - Get all team members | Team Members List of Values Get all team members  |
| Time Attribute Values - Get all time attribute values | Time Attribute Values Get all time attribute values  |
| Time Attributes - Get all time attributes | Time Attributes Get all time attributes  |
| Time Attributes/Data Source Usages - Get all data sources associated with the time attr... | Time Attributes/Data Source Usages Get all data sources associated with the time attribute  |
| Time Attributes/Data Source Usages/Data Source Criteria Binds - Get all data source criteria binds | Time Attributes/Data Source Usages/Data Source Criteria Binds Get all data source criteria binds  |
| Time Card Fields List of Values - Get all time card field values | Time Card Fields List of Values Get all time card field values  |
| Time Cards List of Values - Get all time cards | Time Cards List of Values Get all time cards  |
| Time Event Requests - Get all time event requests | Time Event Requests Get all time event requests  |
| Time Event Requests/Time Events - Get all time events | Time Event Requests/Time Events Get all time events  |
| Time Event Requests/Time Events/Time Event Attributes - Get all time event attributes | Time Event Requests/Time Events/Time Event Attributes Get all time event attributes  |
| Time Layout Sets - Get all time layout sets | Time Layout Sets Get all time layout sets  |
| Time Layout Sets/Web Clock Layouts - Get all web clock layouts for a time layout set | Time Layout Sets/Web Clock Layouts Get all web clock layouts for a time layout set  |
| Time Layout Sets/Web Clock Layouts/Buttons - Get all buttons for a web clock layout | Time Layout Sets/Web Clock Layouts/Buttons Get all buttons for a web clock layout  |
| Time Layout Sets/Web Clock Layouts/Buttons/Time Attributes - Get all time attributes for a button | Time Layout Sets/Web Clock Layouts/Buttons/Time Attributes Get all time attributes for a button  |
| Time Layout Sets/Web Clock Layouts/Buttons/Time Card Fields - Get all time card fields for a button | Time Layout Sets/Web Clock Layouts/Buttons/Time Card Fields Get all time card fields for a button  |
| Time Record Event Requests - Get all time record event requests | Time Record Event Requests Get all time record event requests  |
| Time Record Event Requests/Time Record Events - Get all time record events | Time Record Event Requests/Time Record Events Get all time record events  |
| Time Record Event Requests/Time Record Events/Time Record Event Attributes - Get all time record event attributes | Time Record Event Requests/Time Record Events/Time Record Event Attributes Get all time record event attributes  |
| Time Record Event Requests/Time Record Events/Time Record Event Messages - Get all time record event messages | Time Record Event Requests/Time Record Events/Time Record Event Messages Get all time record event messages  |
| Time Record Groups - Get all time record groups | Time Record Groups Get all time record groups  |
| Time Record Groups/Time Attributes - Get all time record group attributes | Time Record Groups/Time Attributes Get all time record group attributes  |
| Time Record Groups/Time Messages - Get all time record group messages | Time Record Groups/Time Messages Get all time record group messages  |
| Time Record Groups/Time Messages/Time Message Tokens - Get all time record group message tokens | Time Record Groups/Time Messages/Time Message Tokens Get all time record group message tokens  |
| Time Record Groups/Time Record Groups - Get all time record groups | Time Record Groups/Time Record Groups Get all time record groups  |
| Time Record Groups/Time Record Groups/Time Attributes - Get all time record group attributes | Time Record Groups/Time Record Groups/Time Attributes Get all time record group attributes  |
| Time Record Groups/Time Record Groups/Time Messages - Get all time record group messages | Time Record Groups/Time Record Groups/Time Messages Get all time record group messages  |
| Time Record Groups/Time Record Groups/Time Messages/Time Message Tokens - Get all time record group message tokens | Time Record Groups/Time Record Groups/Time Messages/Time Message Tokens Get all time record group message tokens  |
| Time Record Groups/Time Record Groups/Time Record Groups - Get all time record groups | Time Record Groups/Time Record Groups/Time Record Groups Get all time record groups  |
| Time Record Groups/Time Record Groups/Time Records - Get all time records | Time Record Groups/Time Record Groups/Time Records Get all time records  |
| Time Record Groups/Time Record Groups/Time Records/Time Attributes - Get all time record group attributes | Time Record Groups/Time Record Groups/Time Records/Time Attributes Get all time record group attributes  |
| Time Record Groups/Time Record Groups/Time Records/Time Messages - Get all time record group messages | Time Record Groups/Time Record Groups/Time Records/Time Messages Get all time record group messages  |
| Time Record Groups/Time Record Groups/Time Records/Time Messages/Time Message Tokens - Get all time record group message tokens | Time Record Groups/Time Record Groups/Time Records/Time Messages/Time Message Tokens Get all time record group message tokens  |
| Time Record Groups/Time Record Groups/Time Records/Time Statuses - Get all time record group statuses | Time Record Groups/Time Record Groups/Time Records/Time Statuses Get all time record group statuses  |
| Time Record Groups/Time Record Groups/Time Statuses - Get all time record group statuses | Time Record Groups/Time Record Groups/Time Statuses Get all time record group statuses  |
| Time Record Groups/Time Records - Get all time records | Time Record Groups/Time Records Get all time records  |
| Time Record Groups/Time Records/Time Attributes - Get all time record group attributes | Time Record Groups/Time Records/Time Attributes Get all time record group attributes  |
| Time Record Groups/Time Records/Time Messages - Get all time record group messages | Time Record Groups/Time Records/Time Messages Get all time record group messages  |
| Time Record Groups/Time Records/Time Messages/Time Message Tokens - Get all time record group message tokens | Time Record Groups/Time Records/Time Messages/Time Message Tokens Get all time record group message tokens  |
| Time Record Groups/Time Records/Time Statuses - Get all time record group statuses | Time Record Groups/Time Records/Time Statuses Get all time record group statuses  |
| Time Record Groups/Time Statuses - Get all time record group statuses | Time Record Groups/Time Statuses Get all time record group statuses  |
| Time Records - Get all time records | Time Records Get all time records  |
| Time Records/Time Attributes - Get all time record attributes | Time Records/Time Attributes Get all time record attributes  |
| Time Records/Time Messages - Get all time record messages | Time Records/Time Messages Get all time record messages  |
| Time Records/Time Messages/Time Message Tokens - Get all time record message tokens | Time Records/Time Messages/Time Message Tokens Get all time record message tokens  |
| Time Records/Time Statuses - Get all time record statuses | Time Records/Time Statuses Get all time record statuses  |
| Tracking Services - Get all tracking services | Tracking Services Get all tracking services  |
| Tracking Services/Authorization Artifacts - Get all authorization artifacts | Tracking Services/Authorization Artifacts Get all authorization artifacts  |
| Unions List of Values - Get all unions | Unions List of Values Get all unions  |
| User Accounts - Get all user accounts | User Accounts Get all user accounts  |
| User Accounts List of Values - Get all user accounts | User Accounts List of Values Get all user accounts  |
| User Accounts/User Account Roles - Get all role assignments | User Accounts/User Account Roles Get all role assignments  |
| User Roles List of Values - Get all user roles | User Roles List of Values Get all user roles  |
| Web Clock Events - Get all web clock events | Web Clock Events Get all web clock events  |
| Web Clock Events/Time Card Fields - Get all time card fields for a web clock event | Web Clock Events/Time Card Fields Get all time card fields for a web clock event  |
| Wellness Activities - GET action not supported | Wellness Activities GET action not supported  |
| Wellness Activities/Activity Measures - GET action not supported | Wellness Activities/Activity Measures GET action not supported  |
| Workers - Get all workers | Workers Get all workers  |
| Workers/Addresses - Get all worker addresses | Workers/Addresses Get all worker addresses  |
| Workers/Addresses/Address Descriptive Flexfields - Get all flexfields | Workers/Addresses/Address Descriptive Flexfields Get all flexfields  |
| Workers/Citizenships - Get all worker citizenships | Workers/Citizenships Get all worker citizenships  |
| Workers/Citizenships/Citizenship Descriptive Flexfields - Get all flexfields | Workers/Citizenships/Citizenship Descriptive Flexfields Get all flexfields  |
| Workers/Disabilities - Get all worker disabilities | Workers/Disabilities Get all worker disabilities  |
| Workers/Disabilities/Attachments - Get all attachments | Workers/Disabilities/Attachments Get all attachments  |
| Workers/Disabilities/Disabilities Developer Flexfields - Get all flexfields | Workers/Disabilities/Disabilities Developer Flexfields Get all flexfields  |
| Workers/Driver Licenses - Get all worker drivers licenses | Workers/Driver Licenses Get all worker drivers licenses  |
| Workers/Driver Licenses/Driver License Descriptive Flexfields - Get all flexfields | Workers/Driver Licenses/Driver License Descriptive Flexfields Get all flexfields  |
| Workers/Driver Licenses/Driver License Developer Flexfields - Get all flexfields | Workers/Driver Licenses/Driver License Developer Flexfields Get all flexfields  |
| Workers/Emails - Get all worker emails | Workers/Emails Get all worker emails  |
| Workers/Emails/Email Descriptive Flexfields - Get all flexfields | Workers/Emails/Email Descriptive Flexfields Get all flexfields  |
| Workers/Ethnicities - Get all worker ethnicities | Workers/Ethnicities Get all worker ethnicities  |
| Workers/Ethnicities/Ethnicity Descriptive Flexfields - Get all flexfields | Workers/Ethnicities/Ethnicity Descriptive Flexfields Get all flexfields  |
| Workers/External Identifiers - Get all worker external identifiers | Workers/External Identifiers Get all worker external identifiers  |
| Workers/Legislative Information - Get all worker legislative records | Workers/Legislative Information Get all worker legislative records  |
| Workers/Legislative Information/Legislative Information Descriptive Flexfields - Get all flexfields | Workers/Legislative Information/Legislative Information Descriptive Flexfields Get all flexfields  |
| Workers/Legislative Information/Legislative Information Developer Flexfields - Get all flexfields | Workers/Legislative Information/Legislative Information Developer Flexfields Get all flexfields  |
| Workers/Messages - Get all worker messages | Workers/Messages Get all worker messages  |
| Workers/Names - Get all worker names | Workers/Names Get all worker names  |
| Workers/National Identifiers - Get all worker national identifiers | Workers/National Identifiers Get all worker national identifiers  |
| Workers/National Identifiers/National Identifier Descriptive Flexfields - Get all flexfields | Workers/National Identifiers/National Identifier Descriptive Flexfields Get all flexfields  |
| Workers/Other Communication Accounts - Get all worker communication accounts | Workers/Other Communication Accounts Get all worker communication accounts  |
| Workers/Other Communication Accounts/Other Communication Account Descriptive Flexfields - Get all flexfields | Workers/Other Communication Accounts/Other Communication Account Descriptive Flexfields Get all flexfields  |
| Workers/Passports - Get all worker passports | Workers/Passports Get all worker passports  |
| Workers/Passports/Passport Descriptive Flexfields - Get all flexfields | Workers/Passports/Passport Descriptive Flexfields Get all flexfields  |
| Workers/Phones - Get all worker phones | Workers/Phones Get all worker phones  |
| Workers/Phones/Phone Descriptive Flexfields - Get all flexfields | Workers/Phones/Phone Descriptive Flexfields Get all flexfields  |
| Workers/Photos - Get all worker photos | Workers/Photos Get all worker photos  |
| Workers/Photos/Photo Descriptive Flexfields - Get all flexfields | Workers/Photos/Photo Descriptive Flexfields Get all flexfields  |
| Workers/Religions - Get all worker religions | Workers/Religions Get all worker religions  |
| Workers/Religions/Religion Descriptive Flexfields - Get all flexfields | Workers/Religions/Religion Descriptive Flexfields Get all flexfields  |
| Workers/Visa Permits - Get all worker visas permits | Workers/Visa Permits Get all worker visas permits  |
| Workers/Visa Permits/Visa Permit Descriptive Flexfields - Get all flexfields | Workers/Visa Permits/Visa Permit Descriptive Flexfields Get all flexfields  |
| Workers/Visa Permits/Visa Permit Developer Flexfields - Get all flexfields | Workers/Visa Permits/Visa Permit Developer Flexfields Get all flexfields  |
| Workers/Work Relationships - Get all worker work relationships | Workers/Work Relationships Get all worker work relationships  |
| Workers/Work Relationships/Assignments - Get all worker assignments | Workers/Work Relationships/Assignments Get all worker assignments  |
| Workers/Work Relationships/Assignments/All Reports - Get all worker reports | Workers/Work Relationships/Assignments/All Reports Get all worker reports  |
| Workers/Work Relationships/Assignments/Assignment Descriptive Flexfields - Get all flexfields | Workers/Work Relationships/Assignments/Assignment Descriptive Flexfields Get all flexfields  |
| Workers/Work Relationships/Assignments/Assignment Developer Flexfields - Get all flexfields | Workers/Work Relationships/Assignments/Assignment Developer Flexfields Get all flexfields  |
| Workers/Work Relationships/Assignments/Assignment Extensible FlexFields - Get all flexfields | Workers/Work Relationships/Assignments/Assignment Extensible FlexFields Get all flexfields  |
| Workers/Work Relationships/Assignments/Grade Steps - Get all assignment grade steps | Workers/Work Relationships/Assignments/Grade Steps Get all assignment grade steps  |
| Workers/Work Relationships/Assignments/Managers - Get all assignment managers | Workers/Work Relationships/Assignments/Managers Get all assignment managers  |
| Workers/Work Relationships/Assignments/Representatives - Get all worker representatives | Workers/Work Relationships/Assignments/Representatives Get all worker representatives  |
| Workers/Work Relationships/Assignments/Work Measures - Get all assignment work measures | Workers/Work Relationships/Assignments/Work Measures Get all assignment work measures  |
| Workers/Work Relationships/Contracts - Get all worker contracts | Workers/Work Relationships/Contracts Get all worker contracts  |
| Workers/Work Relationships/Contracts/Contracts Descriptive Flexfields - Get all flexfields | Workers/Work Relationships/Contracts/Contracts Descriptive Flexfields Get all flexfields  |
| Workers/Work Relationships/Contracts/Contracts Developer Flexfields - Get all flexfields | Workers/Work Relationships/Contracts/Contracts Developer Flexfields Get all flexfields  |
| Workers/Work Relationships/Work Relationship Descriptive Flexfields - Get all flexfields | Workers/Work Relationships/Work Relationship Descriptive Flexfields Get all flexfields  |
| Workers/Work Relationships/Work Relationship Developer Flexfields - Get all flexfields | Workers/Work Relationships/Work Relationship Developer Flexfields Get all flexfields  |
| Workers/Worker Descriptive Flexfields - Get all flexfields | Workers/Worker Descriptive Flexfields Get all flexfields  |
| Workers/Worker Extensible Flexfields - Get all flexfields | Workers/Worker Extensible Flexfields Get all flexfields  |
| Worklife Teams List of Values - Get all teams | Worklife Teams List of Values Get all teams  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Absences - Update an absence record (PATCH) | Absences Update an absence record  |
| Absences/Absence Attachments - Update an absence attachment (PATCH) | Absences/Absence Attachments Update an absence attachment  |
| Absences/Absence Descriptive Flexfields - Update a flexfield (PATCH) | Absences/Absence Descriptive Flexfields Update a flexfield  |
| Absences/Absence Developer Descriptive Flexfields - Update a flexfield (PATCH) | Absences/Absence Developer Descriptive Flexfields Update a flexfield  |
| Absences/Absence Maternity Details - Update a maternity absence record (PATCH) | Absences/Absence Maternity Details Update a maternity absence record  |
| Allocated Checklists - Allocate a new task (POST) | Allocated Checklists Allocates a task to a person using task name, task description, task type, action url, mandatory flag. Allocate a new task  |
| Allocated Checklists - Allocate a task from the task library (POST) | Allocated Checklists Allocates a task from the task library to an existing checklist. Allocate a task from the task library  |
| Allocated Checklists - Force close an incomplete checklist (POST) | Allocated Checklists Force closes an incomplete checklist. Force close an incomplete checklist  |
| Allocated Checklists - Update an allocated checklist (PATCH) | Allocated Checklists Update an allocated checklist  |
| Allocated Checklists/Allocated Tasks - Reopen a task (POST) | Allocated Checklists/Allocated Tasks Reopens an already completed or not applicable task. Reopen a task  |
| Allocated Checklists/Allocated Tasks - Update an allocated task (PATCH) | Allocated Checklists/Allocated Tasks Update an allocated task  |
| Allocated Checklists/Allocated Tasks - Update the task status (POST) | Allocated Checklists/Allocated Tasks Updates the existing task status to complete or not applicable. Update the task status  |
| Allocated Checklists/Allocated Tasks/Attachments - Update a task attachment (PATCH) | Allocated Checklists/Allocated Tasks/Attachments Update a task attachment  |
| Allocated Checklists/Allocated Tasks/Documents - Update a document task type attachment (PATCH) | Allocated Checklists/Allocated Tasks/Documents Update a document task type attachment  |
| Areas of Responsibility - Reassign an area of responsibility (POST) | Areas of Responsibility Reassigns an area of responsibility to a different worker. Reassign an area of responsibility  |
| Areas of Responsibility - Update an area of responsibility (PATCH) | Areas of Responsibility Update an area of responsibility  |
| Availability Patterns - Update an availability pattern (PATCH) | Availability Patterns Update an availability pattern  |
| Availability Patterns/Shifts - Update a shift (PATCH) | Availability Patterns/Shifts Update a shift  |
| Availability Patterns/Shifts/Breaks - Update a break (PATCH) | Availability Patterns/Shifts/Breaks Update a break  |
| Benefit Groups - Update a benefit group (PATCH) | Benefit Groups Update a benefit group  |
| Candidate Duplicate Checks - Update a candidate duplicate check (PATCH) | Candidate Duplicate Checks Update a candidate duplicate check  |
| Candidate Job Application Drafts - Update an application draft (PATCH) | Candidate Job Application Drafts Update an application draft  |
| Candidate Job Application Drafts/Attachments of the Job Application Drafts - Update an attachment (PATCH) | Candidate Job Application Drafts/Attachments of the Job Application Drafts Update an attachment  |
| Channel Messages - Update a channel message (PATCH) | Channel Messages Update a channel message  |
| Channel Messages/Message Attachments - Update an attachment (PATCH) | Channel Messages/Message Attachments Update an attachment  |
| Check-In Documents - Update a document (PATCH) | Check-In Documents Update a document  |
| Check-In Documents/Questionnaire Responses/Question Responses - Update a response (PATCH) | Check-In Documents/Questionnaire Responses/Question Responses Update a response  |
| Check-In Documents/Questionnaire Responses/Question Responses/Attachments - Update an attachment (PATCH) | Check-In Documents/Questionnaire Responses/Question Responses/Attachments Update an attachment  |
| Document Records - Update a document record (PATCH) | Document Records Update a document record  |
| Document Records/Attachments - Update an attachment (PATCH) | Document Records/Attachments Update an attachment  |
| Document Records/Document Records Descriptive Flexfields - Update a flexfield (PATCH) | Document Records/Document Records Descriptive Flexfields Update a flexfield  |
| Document Records/Document Records Developer Descriptive Flexfields - Update a flexfield (PATCH) | Document Records/Document Records Developer Descriptive Flexfields Update a flexfield  |
| Element Entries - Update an element entry (PATCH) | Element Entries Update an element entry  |
| Element Entries/Element Entry Values - Update an element entry value (PATCH) | Element Entries/Element Entry Values Update an element entry value  |
| Eligibility Object Results - Update an eligibility object result (PATCH) | Eligibility Object Results Update an eligibility object result  |
| Eligibility Objects - Update an eligibility object (PATCH) | Eligibility Objects Update an eligibility object  |
| Eligibility Objects/Eligibility Object Profiles - Update an eligibility object profile (PATCH) | Eligibility Objects/Eligibility Object Profiles Update an eligibility object profile  |
| Email Address Migrations - Update an email address migration (PATCH) | Email Address Migrations Update an email address migration  |
| Employees - Update an employee (PATCH) | Employees Update an employee  |
| Employees/Assignments - Update an assignment (PATCH) | Employees/Assignments Update an assignment  |
| Employees/Assignments/Assignment Descriptive Flexfields - Update a flexfield (PATCH) | Employees/Assignments/Assignment Descriptive Flexfields Update a flexfield  |
| Employees/Assignments/Assignment Extra Information Extensible FlexFields - Update a flexfield (PATCH) | Employees/Assignments/Assignment Extra Information Extensible FlexFields Update a flexfield  |
| Employees/Person Descriptive Flexfields - Update a flexfield (PATCH) | Employees/Person Descriptive Flexfields Update a flexfield  |
| Employees/Person Extra Information Extensible FlexFields - Update a flexfield (PATCH) | Employees/Person Extra Information Extensible FlexFields Update a flexfield  |
| Employees/Photos - Update a photo (PATCH) | Employees/Photos Update a photo  |
| Employees/Roles - Update a role (PATCH) | Employees/Roles Update a role  |
| Incident ID/Incident ID/Attachments - Update an attachment. (PATCH) | Incident ID/Incident ID/Attachments Update an attachment.  |
| Job Applications/Schedule Job Interviews - Update a scheduled interview (PATCH) | Job Applications/Schedule Job Interviews Update a scheduled interview  |
| Job Applications/Secondary Job Submissions - Update a secondary job application (PATCH) | Job Applications/Secondary Job Submissions Update a secondary job application  |
| Job Applications/Unscheduled Job Interviews - Update an unscheduled interview request (PATCH) | Job Applications/Unscheduled Job Interviews Update an unscheduled interview request  |
| Learner Learning Records - Update an assignment record (PATCH) | Learner Learning Records Update an assignment record  |
| Learner Learning Records/Active Learner Comments - Update an active learner comment (PATCH) | Learner Learning Records/Active Learner Comments Update an active learner comment  |
| Learner Learning Records/Active Learner Comments/Likes - Update a like (PATCH) | Learner Learning Records/Active Learner Comments/Likes Update a like  |
| Learner Learning Records/Active Learner Comments/Replies - Update a reply (PATCH) | Learner Learning Records/Active Learner Comments/Replies Update a reply  |
| Learner Learning Records/Active Learner Comments/Replies/Likes - Update a like (PATCH) | Learner Learning Records/Active Learner Comments/Replies/Likes Update a like  |
| Learner Learning Records/Assignment Descriptive Flexfields - Update an assignment flexfield (PATCH) | Learner Learning Records/Assignment Descriptive Flexfields Update an assignment flexfield  |
| Learner Learning Records/Completion Details - Update a completion detail (PATCH) | Learner Learning Records/Completion Details Update a completion detail  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings - Update single other selected course offering (PATCH) | Learner Learning Records/Completion Details/Other Selected Course Offerings Update single other selected course offering  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Assignment Descriptive Flexfields - Update an assignment flexfield (PATCH) | Learner Learning Records/Completion Details/Other Selected Course Offerings/Assignment Descriptive Flexfields Update an assignment flexfield  |
| Learner Learning Records/Completion Details/Other Selected Course Offerings/Learning Item Rating Details - Update a learning item rating (PATCH) | Learner Learning Records/Completion Details/Other Selected Course Offerings/Learning Item Rating Details Update a learning item rating  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings - Update a primary selected course offering (PATCH) | Learner Learning Records/Completion Details/Primary Selected Course Offerings Update a primary selected course offering  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Assignment Descriptive Flexfields - Update an assignment flexfield (PATCH) | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Assignment Descriptive Flexfields Update an assignment flexfield  |
| Learner Learning Records/Completion Details/Primary Selected Course Offerings/Learning Item Rating Details - Update a learning item rating (PATCH) | Learner Learning Records/Completion Details/Primary Selected Course Offerings/Learning Item Rating Details Update a learning item rating  |
| Learner Learning Records/Completion Details/Selected Course Offerings - Update a selected course offering (PATCH) | Learner Learning Records/Completion Details/Selected Course Offerings Update a selected course offering  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments - Update an active learner comment (PATCH) | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments Update an active learner comment  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Likes - Update a like (PATCH) | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Likes Update a like  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies - Update a reply (PATCH) | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies Update a reply  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies/Likes - Update a like (PATCH) | Learner Learning Records/Completion Details/Selected Course Offerings/Active Learner Comments/Replies/Likes Update a like  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Assignment Descriptive Flexfields - Update an assignment flexfield (PATCH) | Learner Learning Records/Completion Details/Selected Course Offerings/Assignment Descriptive Flexfields Update an assignment flexfield  |
| Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Rating Details - Update a learning item rating (PATCH) | Learner Learning Records/Completion Details/Selected Course Offerings/Learning Item Rating Details Update a learning item rating  |
| Learner Learning Records/Learning Item Rating Details - Update a learning item rating (PATCH) | Learner Learning Records/Learning Item Rating Details Update a learning item rating  |
| Learner Learning Records/Other Selected Course Offerings - Update single other selected course offering (PATCH) | Learner Learning Records/Other Selected Course Offerings Update single other selected course offering  |
| Learner Learning Records/Other Selected Course Offerings/Assignment Descriptive Flexfields - Update an assignment flexfield (PATCH) | Learner Learning Records/Other Selected Course Offerings/Assignment Descriptive Flexfields Update an assignment flexfield  |
| Learner Learning Records/Other Selected Course Offerings/Learning Item Rating Details - Update a learning item rating (PATCH) | Learner Learning Records/Other Selected Course Offerings/Learning Item Rating Details Update a learning item rating  |
| Learner Learning Records/Primary Selected Course Offerings - Update a primary selected course offering (PATCH) | Learner Learning Records/Primary Selected Course Offerings Update a primary selected course offering  |
| Learner Learning Records/Primary Selected Course Offerings/Assignment Descriptive Flexfields - Update an assignment flexfield (PATCH) | Learner Learning Records/Primary Selected Course Offerings/Assignment Descriptive Flexfields Update an assignment flexfield  |
| Learner Learning Records/Primary Selected Course Offerings/Learning Item Rating Details - Update a learning item rating (PATCH) | Learner Learning Records/Primary Selected Course Offerings/Learning Item Rating Details Update a learning item rating  |
| Learner Learning Records/Selected Course Offerings - Update a selected course offering (PATCH) | Learner Learning Records/Selected Course Offerings Update a selected course offering  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments - Update an active learner comment (PATCH) | Learner Learning Records/Selected Course Offerings/Active Learner Comments Update an active learner comment  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments/Likes - Update a like (PATCH) | Learner Learning Records/Selected Course Offerings/Active Learner Comments/Likes Update a like  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies - Update a reply (PATCH) | Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies Update a reply  |
| Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies/Likes - Update a like (PATCH) | Learner Learning Records/Selected Course Offerings/Active Learner Comments/Replies/Likes Update a like  |
| Learner Learning Records/Selected Course Offerings/Assignment Descriptive Flexfields - Update an assignment flexfield (PATCH) | Learner Learning Records/Selected Course Offerings/Assignment Descriptive Flexfields Update an assignment flexfield  |
| Learner Learning Records/Selected Course Offerings/Learning Item Rating Details - Update a learning item rating (PATCH) | Learner Learning Records/Selected Course Offerings/Learning Item Rating Details Update a learning item rating  |
| Learning Content Items - Update a learning content item (PATCH) | Learning Content Items Update a learning content item  |
| Locations V2 - Update a location (PATCH) | Locations V2 Update a location  |
| Locations V2/Addresses - Update a location address (PATCH) | Locations V2/Addresses Update a location address  |
| Locations V2/Attachments - Update an attachment (PATCH) | Locations V2/Attachments Update an attachment  |
| Locations V2/Locations Descriptive Flexfields - Update a flexfield (PATCH) | Locations V2/Locations Descriptive Flexfields Update a flexfield  |
| Locations V2/Locations Extensible Flexfields Container/Locations Extensible Flexfields - Update a flexfield (PATCH) | Locations V2/Locations Extensible Flexfields Container/Locations Extensible Flexfields Update a flexfield  |
| Locations V2/Locations Legislative Extensible Flexfields - Update a flexfield (PATCH) | Locations V2/Locations Legislative Extensible Flexfields Update a flexfield  |
| Message Designer Mail Services - Update a messageDesignerMailService (PATCH) | Message Designer Mail Services Update a messageDesignerMailService  |
| My Job Referrals in Opportunity Marketplace - Update a referral in opportunity marketplace. (PATCH) | My Job Referrals in Opportunity Marketplace Update a referral in opportunity marketplace.  |
| Payroll Relationships - Update a payroll relationship (PATCH) | Payroll Relationships Update a payroll relationship  |
| Payroll Relationships/Payroll Assignments - Update a payroll assignment (PATCH) | Payroll Relationships/Payroll Assignments Update a payroll assignment  |
| Payroll Relationships/Payroll Assignments - Update a tax reporting unit (POST) | Payroll Relationships/Payroll Assignments Updates the tax reporting unit for a payroll assignment such that the card associations get generated. Update a tax reporting unit  |
| Payroll Relationships/Payroll Assignments/Assigned Payrolls - Update an assigned payroll (PATCH) | Payroll Relationships/Payroll Assignments/Assigned Payrolls Update an assigned payroll  |
| Payroll Relationships/Payroll Assignments/Assigned Payrolls/Assigned Payroll Dates - Update an assigned payroll date (PATCH) | Payroll Relationships/Payroll Assignments/Assigned Payrolls/Assigned Payroll Dates Update an assigned payroll date  |
| Payroll Relationships/Payroll Assignments/Payroll Assignment Dates - Update a payroll assignment date (PATCH) | Payroll Relationships/Payroll Assignments/Payroll Assignment Dates Update a payroll assignment date  |
| Person Notes - Update a person note (PATCH) | Person Notes Update a person note  |
| Personal Payment Methods - Update a personal payment method (PATCH) | Personal Payment Methods Update a personal payment method  |
| Questionnaires - Update a questionnaire (PATCH) | Questionnaires Update a questionnaire  |
| Questionnaires/Attachments - Update an attachment (PATCH) | Questionnaires/Attachments Update an attachment  |
| Questionnaires/Sections - Update a section (PATCH) | Questionnaires/Sections Update a section  |
| Questionnaires/Sections/Questions - Update a question (PATCH) | Questionnaires/Sections/Questions Update a question  |
| Questionnaires/Sections/Questions/Answers - Update an answer choice (PATCH) | Questionnaires/Sections/Questions/Answers Update an answer choice  |
| Questions - Update a question (PATCH) | Questions Update a question  |
| Questions/Answers - Update an answer (PATCH) | Questions/Answers Update an answer  |
| Recruiting Campaign Details/Campaign Assets - Get all campaignAssets (PATCH) | Recruiting Campaign Details/Campaign Assets Get all campaignAssets  |
| Recruiting Campaign Details/Campaign Assets/Campaign Asset Channels - Update a CampaignAssetChannel (PATCH) | Recruiting Campaign Details/Campaign Assets/Campaign Asset Channels Update a CampaignAssetChannel  |
| Recruiting Campaign Email Designs - Delete a message design metadata (PATCH) | Recruiting Campaign Email Designs Delete a message design metadata  |
| Recruiting Campaign Email Designs/Message Design Metadata - Delete a message design type (PATCH) | Recruiting Campaign Email Designs/Message Design Metadata Delete a message design type  |
| Recruiting Campaign Email Designs/Message DesignTypes - Create all message designs (PATCH) | Recruiting Campaign Email Designs/Message DesignTypes Create all message designs  |
| Recruiting Candidate Extra Information Details - Update an extra information details of a candidate (PATCH) | Recruiting Candidate Extra Information Details Update an extra information details of a candidate  |
| Recruiting Candidate Extra Information Details/Person Extra Information Details - Update an extra information details of a person (PATCH) | Recruiting Candidate Extra Information Details/Person Extra Information Details Update an extra information details of a person  |
| Recruiting Candidates - Update a candidate (PATCH) | Recruiting Candidates Update a candidate  |
| Recruiting Candidates/Attachments - Update an attachment (PATCH) | Recruiting Candidates/Attachments Update an attachment  |
| Recruiting Candidates/Candidate Addresses - Update an address (PATCH) | Recruiting Candidates/Candidate Addresses Update an address  |
| Recruiting Candidates/Candidate Phones - Update a number (PATCH) | Recruiting Candidates/Candidate Phones Update a number  |
| Recruiting Candidates/Education Items - Update an education item (PATCH) | Recruiting Candidates/Education Items Update an education item  |
| Recruiting Candidates/Experience Items - Update an  experience item (PATCH) | Recruiting Candidates/Experience Items Update an  experience item  |
| Recruiting Candidates/Languages - Update a language (PATCH) | Recruiting Candidates/Languages Update a language  |
| Recruiting Candidates/Licenses and Certificates - Update a license and certificate (PATCH) | Recruiting Candidates/Licenses and Certificates Update a license and certificate  |
| Recruiting Candidates/Skills of the Recruiting Candidates - Update recruiting candidates skill (PATCH) | Recruiting Candidates/Skills of the Recruiting Candidates Update recruiting candidates skill  |
| Recruiting Candidates/Work Preferences - Update a work preference (PATCH) | Recruiting Candidates/Work Preferences Update a work preference  |
| Recruiting CE Candidate Certifications - Submit a patch for the certification. (PATCH) | Recruiting CE Candidate Certifications Submit a patch for the certification.  |
| Recruiting CE Candidate Details - Patch details of a candidate (PATCH) | Recruiting CE Candidate Details Patch details of a candidate  |
| Recruiting CE Candidate Details/Candidate Details with Attachments - Patch details of a recruitingCECandidates attachme... (PATCH) | Recruiting CE Candidate Details/Candidate Details with Attachments Patch details of a recruitingCECandidates attachment  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data - Update all details of recruitingCECandidates perso... (PATCH) | Recruiting CE Candidate Details/Candidate Details with Extra Information Data Update all details of recruitingCECandidates personExtraInformation  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows - Update all details of recruitingCECandidates perso... (PATCH) | Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows Update all details of recruitingCECandidates personExtraInformation rows  |
| Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows/Candidate Details with Extra Information Attributes - Update all details of recruitingCECandidates perso... (PATCH) | Recruiting CE Candidate Details/Candidate Details with Extra Information Data/Candidate Details with Extra Information Rows/Candidate Details with Extra Information Attributes Update all details of recruitingCECandidates personExtraInformation row attributes  |
| Recruiting CE Candidate Details/Candidate Details with PII Data - Update details of a recruitingCECandidates Candida... (PATCH) | Recruiting CE Candidate Details/Candidate Details with PII Data Update details of a recruitingCECandidates CandidatePIIData  |
| Recruiting CE Candidate Education - Update the details of the education items of the c... (PATCH) | Recruiting CE Candidate Education Update the details of the education items of the candidate.  |
| Recruiting CE Candidate Preferences - Update a candidate preference (PATCH) | Recruiting CE Candidate Preferences Update a candidate preference  |
| Recruiting CE Candidate Previous Experience - Patch the candidate previous experience items. (PATCH) | Recruiting CE Candidate Previous Experience Patch the candidate previous experience items.  |
| Recruiting CE Candidate Work Preferences - Patch details of a candidate work preference (PATCH) | Recruiting CE Candidate Work Preferences Patch details of a candidate work preference  |
| Recruiting CE Job Requisitions/Work Locations Facet - Update a work location facet (PATCH) | Recruiting CE Job Requisitions/Work Locations Facet Update a work location facet  |
| Recruiting CE Job Requisitions/Work Locations Facet - Update a work location facet (PATCH) | Recruiting CE Job Requisitions/Work Locations Facet Update a work location facet  |
| Recruiting CE Languages - Patch the language items (PATCH) | Recruiting CE Languages Patch the language items  |
| Recruiting CE Regulatory Configurations - Update a regulatory configuration (PATCH) | Recruiting CE Regulatory Configurations Update a regulatory configuration  |
| Recruiting CE Sites - Update a site (PATCH) | Recruiting CE Sites Update a site  |
| Recruiting CE Sites/Cookie Consent - Update a cookie consent in recruitingCESites (PATCH) | Recruiting CE Sites/Cookie Consent Update a cookie consent in recruitingCESites  |
| Recruiting CE Sites/Cookie Consent/Translations - Patch translations of cookie consent in recruiting... (PATCH) | Recruiting CE Sites/Cookie Consent/Translations Patch translations of cookie consent in recruitingCESites  |
| Recruiting CE Sites/Filters - Update a filter in recruitingCESites (PATCH) | Recruiting CE Sites/Filters Update a filter in recruitingCESites  |
| Recruiting CE Sites/Foot Links - Patch footer links in recruitingCESites (PATCH) | Recruiting CE Sites/Foot Links Patch footer links in recruitingCESites  |
| Recruiting CE Sites/Foot Links/Header Link Translations - Update a header translation in recruitingCESites (PATCH) | Recruiting CE Sites/Foot Links/Header Link Translations Update a header translation in recruitingCESites  |
| Recruiting CE Sites/Foot Links/Sublinks - Update a child link used in the recruiting site (PATCH) | Recruiting CE Sites/Foot Links/Sublinks Update a child link used in the recruiting site  |
| Recruiting CE Sites/Foot Links/Sublinks/Header Link Translations - Update a header translation in recruitingCESites (PATCH) | Recruiting CE Sites/Foot Links/Sublinks/Header Link Translations Update a header translation in recruitingCESites  |
| Recruiting CE Sites/Header Links - Update a header links in recruitingCESites (PATCH) | Recruiting CE Sites/Header Links Update a header links in recruitingCESites  |
| Recruiting CE Sites/Header Links/Header Link Translations - Update a header translation in recruitingCESites (PATCH) | Recruiting CE Sites/Header Links/Header Link Translations Update a header translation in recruitingCESites  |
| Recruiting CE Sites/Header Links/Sublinks - Update a child link used in the recruiting site (PATCH) | Recruiting CE Sites/Header Links/Sublinks Update a child link used in the recruiting site  |
| Recruiting CE Sites/Header Links/Sublinks/Header Link Translations - Update a header translation in recruitingCESites (PATCH) | Recruiting CE Sites/Header Links/Sublinks/Header Link Translations Update a header translation in recruitingCESites  |
| Recruiting CE Sites/Languages - Update a languages in recruitingCESites (PATCH) | Recruiting CE Sites/Languages Update a languages in recruitingCESites  |
| Recruiting CE Sites/Pages - Update a page in recruitingCESites (PATCH) | Recruiting CE Sites/Pages Update a page in recruitingCESites  |
| Recruiting CE Sites/Pages/Page Translations - Update a page translation in recruitingCESites (PATCH) | Recruiting CE Sites/Pages/Page Translations Update a page translation in recruitingCESites  |
| Recruiting CE Sites/Pages/Section Parameters - Update a section parameter in the sections of page... (PATCH) | Recruiting CE Sites/Pages/Section Parameters Update a section parameter in the sections of pages  |
| Recruiting CE Sites/Pages/Sections - Update a section in the pages of recruitingCESites (PATCH) | Recruiting CE Sites/Pages/Sections Update a section in the pages of recruitingCESites  |
| Recruiting CE Sites/Pages/Sections/Components - Update a component in the sections of pages (PATCH) | Recruiting CE Sites/Pages/Sections/Components Update a component in the sections of pages  |
| Recruiting CE Sites/Pages/Sections/Components/Component Translations - Update a component translation in the sections of ... (PATCH) | Recruiting CE Sites/Pages/Sections/Components/Component Translations Update a component translation in the sections of pages  |
| Recruiting CE Sites/Pages/Sections/Components/Section Parameters - Update a section parameter in the sections of page... (PATCH) | Recruiting CE Sites/Pages/Sections/Components/Section Parameters Update a section parameter in the sections of pages  |
| Recruiting CE Sites/Pages/Sections/Section Parameters - Update a section parameter in the sections of page... (PATCH) | Recruiting CE Sites/Pages/Sections/Section Parameters Update a section parameter in the sections of pages  |
| Recruiting CE Sites/Settings - Update a setting in recruitingCESites (PATCH) | Recruiting CE Sites/Settings Update a setting in recruitingCESites  |
| Recruiting CE Sites/Site Custom Fonts - Update a custom font in recruitingCESites (PATCH) | Recruiting CE Sites/Site Custom Fonts Update a custom font in recruitingCESites  |
| Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings - Update a custom font setting in recruitingCESites (PATCH) | Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings Update a custom font setting in recruitingCESites  |
| Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings/Site Custom Font Setting Files - Update a custom font setting file in recruitingCES... (PATCH) | Recruiting CE Sites/Site Custom Fonts/Site Custom Font Settings/Site Custom Font Setting Files Update a custom font setting file in recruitingCESites  |
| Recruiting CE Sites/Site Favicon - Update a favorite icon in recruitingCESites (PATCH) | Recruiting CE Sites/Site Favicon Update a favorite icon in recruitingCESites  |
| Recruiting CE Sites/Site Translations - Update a translation in recruitingCESites (PATCH) | Recruiting CE Sites/Site Translations Update a translation in recruitingCESites  |
| Recruiting CE Sites/Talent Community Sign Up - Patch talent community sign up in recruitingCESite... (PATCH) | Recruiting CE Sites/Talent Community Sign Up Patch talent community sign up in recruitingCESites  |
| Recruiting CE Sites/Talent Community Sign Up/Translations for the Talent Community Sign Up - Update a talent community enrollment translation i... (PATCH) | Recruiting CE Sites/Talent Community Sign Up/Translations for the Talent Community Sign Up Update a talent community enrollment translation in recruitingCESites  |
| Recruiting CE Skill Items - Update the skill items (PATCH) | Recruiting CE Skill Items Update the skill items  |
| Recruiting CE Themes - Update details of recruitingCEThemes (PATCH) | Recruiting CE Themes Update details of recruitingCEThemes  |
| Recruiting CE Themes/Brands in Themes - Update details of a brand in recruitingCEThemes (PATCH) | Recruiting CE Themes/Brands in Themes Update details of a brand in recruitingCEThemes  |
| Recruiting CE Themes/Brands in Themes/Brand Translations of Themes - Update details of recruitingCEThemes Brands transl... (PATCH) | Recruiting CE Themes/Brands in Themes/Brand Translations of Themes Update details of recruitingCEThemes Brands translations  |
| Recruiting CE Themes/Footer Links in Themes - Update details of a footer link in recruitingCEThe... (PATCH) | Recruiting CE Themes/Footer Links in Themes Update details of a footer link in recruitingCEThemes  |
| Recruiting CE Themes/Footer Links in Themes/Translations Used in Brands in Themes - Update details of a translation in recruitingCEThe... (PATCH) | Recruiting CE Themes/Footer Links in Themes/Translations Used in Brands in Themes Update details of a translation in recruitingCEThemes  |
| Recruiting CE Themes/Header Links in Themes - Update details of a header link in recruitingCEThe... (PATCH) | Recruiting CE Themes/Header Links in Themes Update details of a header link in recruitingCEThemes  |
| Recruiting CE Themes/Header Links in Themes/Translations Used in Brands in Themes - Update details of a translation in recruitingCEThe... (PATCH) | Recruiting CE Themes/Header Links in Themes/Translations Used in Brands in Themes Update details of a translation in recruitingCEThemes  |
| Recruiting CE Themes/Styles in Themes - Update details of styles in recruitingCEThemes (PATCH) | Recruiting CE Themes/Styles in Themes Update details of styles in recruitingCEThemes  |
| Recruiting CE Themes/Translations in a Template - Update details of a translation in recruitingCETem... (PATCH) | Recruiting CE Themes/Translations in a Template Update details of a translation in recruitingCETemplates  |
| Recruiting CE User Tracking - Update the user tracking information (PATCH) | Recruiting CE User Tracking Update the user tracking information  |
| Recruiting Job Applications - Allows a candidate to move an application to an sp... (POST) | Recruiting Job Applications Allows a candidate to move an application to an specific state and phase Allows a candidate to move an application to an specific state and phase  |
| Recruiting Job Applications - Allows a recruiter to confirm a job application on... (POST) | Recruiting Job Applications Allows a recruiter to confirm a job application on behalf of a candidate. Allows a recruiter to confirm a job application on behalf of a candidate.  |
| Recruiting Job Applications - Allows a recruiter to resend a notification to a c... (POST) | Recruiting Job Applications Allows a recruiter to resend a notification to a candidate requesting them to confirm their job application. Allows a recruiter to resend a notification to a candidate requesting them to confirm their job application.  |
| Recruiting Job Applications - Allows a recruiter to withdraw a job application o... (POST) | Recruiting Job Applications Allows a recruiter to withdraw a job application on behalf of the candidate. Allows a recruiter to withdraw a job application on behalf of the candidate.  |
| Recruiting Job Requisitions - Update a requisition (PATCH) | Recruiting Job Requisitions Update a requisition  |
| Recruiting Job Requisitions/Attachments - Update an attachment (PATCH) | Recruiting Job Requisitions/Attachments Update an attachment  |
| Recruiting Job Requisitions/Media Links - Update a media link (PATCH) | Recruiting Job Requisitions/Media Links Update a media link  |
| Recruiting Job Requisitions/Requisition Descriptive Flexfields - Update a requisition descriptive flexfield (PATCH) | Recruiting Job Requisitions/Requisition Descriptive Flexfields Update a requisition descriptive flexfield  |
| Salaries - Update a salary for a worker's assignment (PATCH) | Salaries Update a salary for a worker's assignment  |
| Salaries/Salary Components - Update a component (PATCH) | Salaries/Salary Components Update a component  |
| Salaries/Salary Rate Components - Update a rate component (PATCH) | Salaries/Salary Rate Components Update a rate component  |
| Salaries/Salary Simple Components - Update a simple component (PATCH) | Salaries/Salary Simple Components Update a simple component  |
| Talent Person Profiles - Update a person profile (PATCH) | Talent Person Profiles Update a person profile  |
| Talent Person Profiles/Accomplishment Sections/Accomplishment Items - Update an accomplishment item (PATCH) | Talent Person Profiles/Accomplishment Sections/Accomplishment Items Update an accomplishment item  |
| Talent Person Profiles/Accomplishment Sections/Accomplishment Items/Accomplishment Items Descriptive Flexfields - Update a flexfield (PATCH) | Talent Person Profiles/Accomplishment Sections/Accomplishment Items/Accomplishment Items Descriptive Flexfields Update a flexfield  |
| Talent Person Profiles/Areas of Expertises - Update an area of expertise (PATCH) | Talent Person Profiles/Areas of Expertises Update an area of expertise  |
| Talent Person Profiles/Areas of Interests - Update an area of interest (PATCH) | Talent Person Profiles/Areas of Interests Update an area of interest  |
| Talent Person Profiles/Career Preference Sections/Career Preference Items - Update a career preference item (PATCH) | Talent Person Profiles/Career Preference Sections/Career Preference Items Update a career preference item  |
| Talent Person Profiles/Career Preference Sections/Career Preference Items/Career Preference Items Descriptive Flexfields - Update a flexfield (PATCH) | Talent Person Profiles/Career Preference Sections/Career Preference Items/Career Preference Items Descriptive Flexfields Update a flexfield  |
| Talent Person Profiles/Certification Sections/Certification Items - Update a certification item (PATCH) | Talent Person Profiles/Certification Sections/Certification Items Update a certification item  |
| Talent Person Profiles/Certification Sections/Certification Items/Certification Items Descriptive Flexfields - Update a flexfield (PATCH) | Talent Person Profiles/Certification Sections/Certification Items/Certification Items Descriptive Flexfields Update a flexfield  |
| Talent Person Profiles/Competency Sections/Competency Items - Update a competency item (PATCH) | Talent Person Profiles/Competency Sections/Competency Items Update a competency item  |
| Talent Person Profiles/Competency Sections/Competency Items/Competency Items Descriptive Flexfields - Update a flexfield (PATCH) | Talent Person Profiles/Competency Sections/Competency Items/Competency Items Descriptive Flexfields Update a flexfield  |
| Talent Person Profiles/Education Sections/Education Items - Update an education item (PATCH) | Talent Person Profiles/Education Sections/Education Items Update an education item  |
| Talent Person Profiles/Education Sections/Education Items/Education Items Descriptive Flexfields - Update a flexfield (PATCH) | Talent Person Profiles/Education Sections/Education Items/Education Items Descriptive Flexfields Update a flexfield  |
| Talent Person Profiles/Favorite Links - Update a favorite link (PATCH) | Talent Person Profiles/Favorite Links Update a favorite link  |
| Talent Person Profiles/Honor Sections/Honor Items - Update an honor item (PATCH) | Talent Person Profiles/Honor Sections/Honor Items Update an honor item  |
| Talent Person Profiles/Honor Sections/Honor Items/Honor Items Descriptive Flexfields - Update a flexfield (PATCH) | Talent Person Profiles/Honor Sections/Honor Items/Honor Items Descriptive Flexfields Update a flexfield  |
| Talent Person Profiles/Language Sections/Language Items - Update a language item (PATCH) | Talent Person Profiles/Language Sections/Language Items Update a language item  |
| Talent Person Profiles/Language Sections/Language Items/Language Items Descriptive Flexfields - Update a flexfield (PATCH) | Talent Person Profiles/Language Sections/Language Items/Language Items Descriptive Flexfields Update a flexfield  |
| Talent Person Profiles/Membership Sections/Membership Items - Update a membership item (PATCH) | Talent Person Profiles/Membership Sections/Membership Items Update a membership item  |
| Talent Person Profiles/Membership Sections/Membership Items/Membership Items Descriptive Flexfields - Update a flexfield (PATCH) | Talent Person Profiles/Membership Sections/Membership Items/Membership Items Descriptive Flexfields Update a flexfield  |
| Talent Person Profiles/Skill Sections/Skill Items - Update a skill item (PATCH) | Talent Person Profiles/Skill Sections/Skill Items Update a skill item  |
| Talent Person Profiles/Skill Sections/Skill Items/Skill Items Descriptive Flexfields - Update a flexfield (PATCH) | Talent Person Profiles/Skill Sections/Skill Items/Skill Items Descriptive Flexfields Update a flexfield  |
| Talent Person Profiles/Special Project Sections/Special Project Items - Update a special project item (PATCH) | Talent Person Profiles/Special Project Sections/Special Project Items Update a special project item  |
| Talent Person Profiles/Special Project Sections/Special Project Items/Special Project Items Descriptive Flexfields - Update a flexfield (PATCH) | Talent Person Profiles/Special Project Sections/Special Project Items/Special Project Items Descriptive Flexfields Update a flexfield  |
| Talent Person Profiles/Tags - Update a tag (PATCH) | Talent Person Profiles/Tags Update a tag  |
| Talent Person Profiles/Work History Sections/Work History Items - Update a work history item (PATCH) | Talent Person Profiles/Work History Sections/Work History Items Update a work history item  |
| Talent Person Profiles/Work History Sections/Work History Items/Work History Items Descriptive Flexfields - Update a flexfield (PATCH) | Talent Person Profiles/Work History Sections/Work History Items/Work History Items Descriptive Flexfields Update a flexfield  |
| Talent Person Profiles/Work Preference Sections/Work Preference Items - Update a work preference item (PATCH) | Talent Person Profiles/Work Preference Sections/Work Preference Items Update a work preference item  |
| Talent Person Profiles/Work Preference Sections/Work Preference Items/Work Preference Items Descriptive Flexfields - Update a flexfield (PATCH) | Talent Person Profiles/Work Preference Sections/Work Preference Items/Work Preference Items Descriptive Flexfields Update a flexfield  |
| Talent Ratings/Career Potential - Update a career potential (PATCH) | Talent Ratings/Career Potential Update a career potential  |
| Talent Ratings/Career Potential/Career Potential Descriptive Flexfields - Update a flexfield (PATCH) | Talent Ratings/Career Potential/Career Potential Descriptive Flexfields Update a flexfield  |
| Talent Ratings/Impact Of Losses - Update an impact of loss (PATCH) | Talent Ratings/Impact Of Losses Update an impact of loss  |
| Talent Ratings/Performance Ratings - Update a performance rating (PATCH) | Talent Ratings/Performance Ratings Update a performance rating  |
| Talent Ratings/Performance Ratings/Performance Rating Descriptive Flexfields - Update a flexfield (PATCH) | Talent Ratings/Performance Ratings/Performance Rating Descriptive Flexfields Update a flexfield  |
| Talent Ratings/Risk Of Losses - Update a risk of loss (PATCH) | Talent Ratings/Risk Of Losses Update a risk of loss  |
| Talent Ratings/Risk Of Losses/Risk Of Losses Descriptive Flexfields - Update a flexfield (PATCH) | Talent Ratings/Risk Of Losses/Risk Of Losses Descriptive Flexfields Update a flexfield  |
| Talent Ratings/Talent Scores - Update a talent score (PATCH) | Talent Ratings/Talent Scores Update a talent score  |
| Talent Ratings/Talent Scores/Talent Score Descriptive Flexfields - Update a flexfield (PATCH) | Talent Ratings/Talent Scores/Talent Score Descriptive Flexfields Update a flexfield  |
| Target  Date Migrations - Update a target date migration (PATCH) | Target  Date Migrations Update a target date migration  |
| Tracking Services - Update a tracking service (PATCH) | Tracking Services Update a tracking service  |
| User Accounts - Reset a user account password (POST) | User Accounts Resets the password to a user account. Initiating this custom action on the selected user account must trigger the password reset flow. Reset a user account password  |
| User Accounts - Trigger roles autoprovisioning process (POST) | User Accounts Triggers the roles autoprovisioning process on the selected user account. Trigger roles autoprovisioning process  |
| User Accounts - Update a user account (PATCH) | User Accounts Update a user account  |
| User Accounts - Update a user account password (POST) | User Accounts Updates the password to a user account. Initiating the custom action on the  selected user account must trigger the password update flow. Update a user account password  |
| Workers - Update a worker (PATCH) | Workers Update a worker  |
| Workers/Addresses - Update a worker address (PATCH) | Workers/Addresses Update a worker address  |
| Workers/Addresses/Address Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Addresses/Address Descriptive Flexfields Update a flexfield  |
| Workers/Citizenships - Update a worker citizenship (PATCH) | Workers/Citizenships Update a worker citizenship  |
| Workers/Citizenships/Citizenship Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Citizenships/Citizenship Descriptive Flexfields Update a flexfield  |
| Workers/Disabilities - Update a worker disability (PATCH) | Workers/Disabilities Update a worker disability  |
| Workers/Disabilities/Attachments - Update an attachment (PATCH) | Workers/Disabilities/Attachments Update an attachment  |
| Workers/Disabilities/Disabilities Developer Flexfields - Update a flexfield (PATCH) | Workers/Disabilities/Disabilities Developer Flexfields Update a flexfield  |
| Workers/Driver Licenses - Update a worker drivers license (PATCH) | Workers/Driver Licenses Update a worker drivers license  |
| Workers/Driver Licenses/Driver License Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Driver Licenses/Driver License Descriptive Flexfields Update a flexfield  |
| Workers/Driver Licenses/Driver License Developer Flexfields - Update a flexfield (PATCH) | Workers/Driver Licenses/Driver License Developer Flexfields Update a flexfield  |
| Workers/Emails - Update a worker email (PATCH) | Workers/Emails Update a worker email  |
| Workers/Emails/Email Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Emails/Email Descriptive Flexfields Update a flexfield  |
| Workers/Ethnicities - Update a worker ethnicity (PATCH) | Workers/Ethnicities Update a worker ethnicity  |
| Workers/Ethnicities/Ethnicity Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Ethnicities/Ethnicity Descriptive Flexfields Update a flexfield  |
| Workers/External Identifiers - Update a worker external identifier (PATCH) | Workers/External Identifiers Update a worker external identifier  |
| Workers/Legislative Information - Update a worker legislative record (PATCH) | Workers/Legislative Information Update a worker legislative record  |
| Workers/Legislative Information/Legislative Information Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Legislative Information/Legislative Information Descriptive Flexfields Update a flexfield  |
| Workers/Legislative Information/Legislative Information Developer Flexfields - Update a flexfield (PATCH) | Workers/Legislative Information/Legislative Information Developer Flexfields Update a flexfield  |
| Workers/Messages - Update a worker message (PATCH) | Workers/Messages Update a worker message  |
| Workers/Names - Update a worker name (PATCH) | Workers/Names Update a worker name  |
| Workers/National Identifiers - Update a worker national identifier (PATCH) | Workers/National Identifiers Update a worker national identifier  |
| Workers/National Identifiers/National Identifier Descriptive Flexfields - Update a flexfield (PATCH) | Workers/National Identifiers/National Identifier Descriptive Flexfields Update a flexfield  |
| Workers/Other Communication Accounts - Update a worker communication account (PATCH) | Workers/Other Communication Accounts Update a worker communication account  |
| Workers/Other Communication Accounts/Other Communication Account Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Other Communication Accounts/Other Communication Account Descriptive Flexfields Update a flexfield  |
| Workers/Passports - Update a worker passport (PATCH) | Workers/Passports Update a worker passport  |
| Workers/Passports/Passport Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Passports/Passport Descriptive Flexfields Update a flexfield  |
| Workers/Phones - Update a worker phone (PATCH) | Workers/Phones Update a worker phone  |
| Workers/Phones/Phone Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Phones/Phone Descriptive Flexfields Update a flexfield  |
| Workers/Photos - Update a worker photo (PATCH) | Workers/Photos Update a worker photo  |
| Workers/Photos/Photo Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Photos/Photo Descriptive Flexfields Update a flexfield  |
| Workers/Religions - Update a worker religion (PATCH) | Workers/Religions Update a worker religion  |
| Workers/Religions/Religion Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Religions/Religion Descriptive Flexfields Update a flexfield  |
| Workers/Visa Permits - Update a worker visa permit (PATCH) | Workers/Visa Permits Update a worker visa permit  |
| Workers/Visa Permits/Visa Permit Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Visa Permits/Visa Permit Descriptive Flexfields Update a flexfield  |
| Workers/Visa Permits/Visa Permit Developer Flexfields - Update a flexfield (PATCH) | Workers/Visa Permits/Visa Permit Developer Flexfields Update a flexfield  |
| Workers/Work Relationships - Add a temporary assignment (POST) | Workers/Work Relationships Adds a temporary assignment for a worker. Add a temporary assignment  |
| Workers/Work Relationships - Cancel a work relationship (POST) | Workers/Work Relationships Cancels the work relationship of a worker. Cancel a work relationship  |
| Workers/Work Relationships - Change the legal employer or create a temporary as... (POST) | Workers/Work Relationships Changes the legal employer of a worker or creates a global temporary assignment for a worker. Change the legal employer or create a temporary assignment  |
| Workers/Work Relationships - Change the primary flag of a work relationship (POST) | Workers/Work Relationships Changes the primary flag of a work relationship of a worker. Change the primary flag of a work relationship  |
| Workers/Work Relationships - Change the start date of a work relationship (POST) | Workers/Work Relationships Changes the start date of a work relationship of a worker. Change the start date of a work relationship  |
| Workers/Work Relationships - Correct a worker termination (POST) | Workers/Work Relationships Corrects the termination of a worker. Correct a worker termination  |
| Workers/Work Relationships - Reverse a worker termination (POST) | Workers/Work Relationships Reverses the termination of a worker. Reverse a worker termination  |
| Workers/Work Relationships - Terminate a worker (POST) | Workers/Work Relationships Terminates a worker. Terminate a worker  |
| Workers/Work Relationships - Update a worker work relationship (PATCH) | Workers/Work Relationships Update a worker work relationship  |
| Workers/Work Relationships/Assignments - End a worker assignment (POST) | Workers/Work Relationships/Assignments Ends an assignment of a worker. End a worker assignment  |
| Workers/Work Relationships/Assignments - Update a worker assignment (PATCH) | Workers/Work Relationships/Assignments Update a worker assignment  |
| Workers/Work Relationships/Assignments/Assignment Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Work Relationships/Assignments/Assignment Descriptive Flexfields Update a flexfield  |
| Workers/Work Relationships/Assignments/Assignment Developer Flexfields - Update a flexfield (PATCH) | Workers/Work Relationships/Assignments/Assignment Developer Flexfields Update a flexfield  |
| Workers/Work Relationships/Assignments/Assignment Extensible FlexFields - Update a flexfield (PATCH) | Workers/Work Relationships/Assignments/Assignment Extensible FlexFields Update a flexfield  |
| Workers/Work Relationships/Assignments/Grade Steps - Update an assignment grade step (PATCH) | Workers/Work Relationships/Assignments/Grade Steps Update an assignment grade step  |
| Workers/Work Relationships/Assignments/Managers - Update an assignment manager (PATCH) | Workers/Work Relationships/Assignments/Managers Update an assignment manager  |
| Workers/Work Relationships/Assignments/Work Measures - Update an assignment work measure (PATCH) | Workers/Work Relationships/Assignments/Work Measures Update an assignment work measure  |
| Workers/Work Relationships/Contracts - Update a worker contract (PATCH) | Workers/Work Relationships/Contracts Update a worker contract  |
| Workers/Work Relationships/Contracts/Contracts Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Work Relationships/Contracts/Contracts Descriptive Flexfields Update a flexfield  |
| Workers/Work Relationships/Contracts/Contracts Developer Flexfields - Update a flexfield (PATCH) | Workers/Work Relationships/Contracts/Contracts Developer Flexfields Update a flexfield  |
| Workers/Work Relationships/Work Relationship Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Work Relationships/Work Relationship Descriptive Flexfields Update a flexfield  |
| Workers/Work Relationships/Work Relationship Developer Flexfields - Update a flexfield (PATCH) | Workers/Work Relationships/Work Relationship Developer Flexfields Update a flexfield  |
| Workers/Worker Descriptive Flexfields - Update a flexfield (PATCH) | Workers/Worker Descriptive Flexfields Update a flexfield  |
| Workers/Worker Extensible Flexfields - Update a flexfield (PATCH) | Workers/Worker Extensible Flexfields Update a flexfield  |


