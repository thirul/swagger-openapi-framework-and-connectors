
# common-features


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| AddTaskAttachments - Add a Task Attachment | Manage Tasks  Adds an attachment to a task. If Oracle Content and Experience (documents) is enabled for this task, attachments do not apply. Add a Task Attachment  |
| AddTaskComment - Add a Task Comment | Manage Tasks  Adds a comment to a task. Add a Task Comment  |
| Announcements - Create an announcement | Announcements Create an announcement  |
| ATK Popup Items - Create a popup items | ATK Popup Items Create a popup items  |
| ATK Themes - Create a theme | ATK Themes Create a theme  |
| Check-auditstatus - Get the audit status of an object | Audit Setup Get the audit status of an object  |
| Create - Create a user | Users Create a user  |
| Create SetupOfferingCSVExports - Create a setup offering CSV export | Setup Offering CSV Exports Create a setup offering CSV export  |
| Create SetupOfferingCSVExports-SetupOfferingCSVExportCriteria - Create an export criteria | Setup Offering CSV Exports/Export Criteria Create an export criteria  |
| Create ValueSets-values - Create values of a value set | Value Sets/Values Create values of a value set  |
| Get-auditlevel - Get the audit level for products | Audit Setup Get the audit level for products  |
| Get-auditsetup - Get all objects for a product | Audit Setup Get all objects for a product  |
| Getaudithistory - Get an audit report | Audit Report Get an audit report  |
| Persons - Create a person | Persons Create a person  |
| Post - Bulk operation | Bulk Bulk operation  |
| Set-auditlevel - Set the audit level for products | Audit Setup Set the audit level for products  |
| Setup Offering CSV Exports/Export Process - Create an export process | Setup Offering CSV Exports/Export Process Create an export process  |
| Setup Offering CSV Imports - Create a setup offering CSV import | Setup Offering CSV Imports Create a setup offering CSV import  |
| Setup Offering CSV Imports/Import Process - Create an import process | Setup Offering CSV Imports/Import Process Create an import process  |
| Setup Offering CSV Template Exports - Create a setup offering CSV template export | Setup Offering CSV Template Exports Create a setup offering CSV template export  |
| Setup Offering CSV Template Exports/Export Process - Create an template export process | Setup Offering CSV Template Exports/Export Process Create an template export process  |
| Setup Task CSV Exports - Create a setup task CSV export | Setup Task CSV Exports Create a setup task CSV export  |
| Setup Task CSV Exports/Export Criteria - Create an export criteria | Setup Task CSV Exports/Export Criteria Create an export criteria  |
| Setup Task CSV Exports/Export Process - Create an export process | Setup Task CSV Exports/Export Process Create an export process  |
| Setup Task CSV Imports - Create a setup task CSV import | Setup Task CSV Imports Create a setup task CSV import  |
| Setup Task CSV Imports/Import Process - Create an import process | Setup Task CSV Imports/Import Process Create an import process  |
| Setup Task CSV Template Exports - Create a setup task CSV template export | Setup Task CSV Template Exports Create a setup task CSV template export  |
| Setup Task CSV Template Exports/Export Process - Create an template export process | Setup Task CSV Template Exports/Export Process Create an template export process  |
| Update-auditstatus - Set the audit status for an object | Audit Setup Set the audit status for an object  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Announcements - Delete an announcement | Announcements Delete an announcement  |
| ATK Popup Items - Delete a popup item | ATK Popup Items Delete a popup item  |
| ATK Themes - Delete a theme | ATK Themes Delete a theme  |
| Delete - Delete a user | Users Delete a user  |
| Delete ValueSets-values - Delete values of a value set | Value Sets/Values Delete values of a value set  |
| DeleteTaskAttachment - Delete a Task Attachment | Manage Tasks  Deletes an attachment by name. If Oracle Content and Experience (documents) is enabled for this task, attachments do not apply. Delete a Task Attachment  |
| Persons - Delete a person | Persons Delete a person  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Announcements - Get an announcement | Announcements Get an announcement  |
| ATK Help Center UAP Keys - Get  a UAP key | ATK Help Center UAP Keys Get  a UAP key  |
| ATK Help Center UAP Keys/ATK Help Center UAP Topics - Get a topic | ATK Help Center UAP Keys/ATK Help Center UAP Topics Get a topic  |
| ATK Popup Items - Get a popup item | ATK Popup Items Get a popup item  |
| ATK Themes - Get a theme | ATK Themes Get a theme  |
| Get CommonLookupsLOV - Get a common lookup | Common Lookups List of Values Get a common lookup  |
| Get CurrenciesLOV - Get a currency | Currencies List of Values Get a currency  |
| Get GenericLookupsLOV - Get a generic lookup | Generic Lookups List of Values Get a generic lookup  |
| Get ProfileValues - Get a profile value | Profile Values  Get a profile value  |
| Get SetEnabledLookupsLOV - Get a set-enabled lookup | Set-Enabled Lookups List of Values Get a set-enabled lookup  |
| Get StandardLookupsLOV - Get a standard lookup | Standard Lookups List of Values Get a standard lookup  |
| Get TerritoriesLOV - Get a territory | Territories List of Values Get a territory  |
| Get ValueSets - Get a value set | Value Sets Get a value set  |
| Get ValueSets-validationTable - Get a table-validated value set | Value Sets/Table-Validated Value Sets Get a table-validated value set  |
| Get ValueSets-values - Get a value of a value set | Value Sets/Values Get a value of a value set  |
| GetTask - Retrieve a Task | Manage Tasks  Retrieves a task by task ID (The task number).  Retrieve a Task  |
| GetTaskAttachment - Retrieve a Task Attachment by Name | Manage Tasks  Retrieves an attachment by name. If Oracle Content and Experience (documents) is enabled for this task, attachments do not apply. Retrieve a Task Attachment by Name  |
| GetViewBasedTasks - Retrieve a Custom Task List | Manage Tasks  Retrieves a custom task list by name. Retrieve a Custom Task List  |
| Persons - Get a person | Persons Get a person  |
| Read - Get a resource type | Resource Types Get a resource type  |
| Read - Get a role | Roles Get a role  |
| Read - Get a schema | Schemas Get a schema  |
| Read - Get a user | Users Get a user  |
| Read - Get a user request | User Requests Get a user request  |
| Setup Offering CSV Exports - Get a setup offering CSV export | Setup Offering CSV Exports Get a setup offering CSV export  |
| Setup Offering CSV Exports/Export Criteria - GET action not supported | Setup Offering CSV Exports/Export Criteria GET action not supported  |
| Setup Offering CSV Exports/Export Process - Get an export process | Setup Offering CSV Exports/Export Process Get an export process  |
| Setup Offering CSV Exports/Export Process/Export Process Result - Get an export process result | Setup Offering CSV Exports/Export Process/Export Process Result Get an export process result  |
| Setup Offering CSV Imports - Get a setup offering CSV import | Setup Offering CSV Imports Get a setup offering CSV import  |
| Setup Offering CSV Imports/Import Process - Get an import process | Setup Offering CSV Imports/Import Process Get an import process  |
| Setup Offering CSV Imports/Import Process/Import Process Result - Get an import process result | Setup Offering CSV Imports/Import Process/Import Process Result Get an import process result  |
| Setup Offering CSV Template Exports - Get a setup offering CSV template export | Setup Offering CSV Template Exports Get a setup offering CSV template export  |
| Setup Offering CSV Template Exports/Export Process - Get a template export process | Setup Offering CSV Template Exports/Export Process Get a template export process  |
| Setup Offering CSV Template Exports/Export Process/Export Process Result - Get a template export process result | Setup Offering CSV Template Exports/Export Process/Export Process Result Get a template export process result  |
| Setup Task CSV Exports - Get a setup task CSV export | Setup Task CSV Exports Get a setup task CSV export  |
| Setup Task CSV Exports/Export Criteria - GET action not supported | Setup Task CSV Exports/Export Criteria GET action not supported  |
| Setup Task CSV Exports/Export Process - Get an export process | Setup Task CSV Exports/Export Process Get an export process  |
| Setup Task CSV Exports/Export Process/Export Process Result - Get an export process result | Setup Task CSV Exports/Export Process/Export Process Result Get an export process result  |
| Setup Task CSV Imports - Get a setup task CSV import | Setup Task CSV Imports Get a setup task CSV import  |
| Setup Task CSV Imports/Import Process - Get an import process | Setup Task CSV Imports/Import Process Get an import process  |
| Setup Task CSV Imports/Import Process/Import Process Result - Get an import process result | Setup Task CSV Imports/Import Process/Import Process Result Get an import process result  |
| Setup Task CSV Template Exports - Get a setup task CSV template export | Setup Task CSV Template Exports Get a setup task CSV template export  |
| Setup Task CSV Template Exports/Export Process - Get a template export process | Setup Task CSV Template Exports/Export Process Get a template export process  |
| Setup Task CSV Template Exports/Export Process/Export Process Result - Get a template export process result | Setup Task CSV Template Exports/Export Process/Export Process Result Get a template export process result  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Announcements - Get all announcements | Announcements Get all announcements  |
| ATK Help Center UAP Keys - Get all UAP keys | ATK Help Center UAP Keys Get all UAP keys  |
| ATK Help Center UAP Keys/ATK Help Center UAP Topics - Get all topics | ATK Help Center UAP Keys/ATK Help Center UAP Topics Get all topics  |
| ATK Popup Items - Get all popup items | ATK Popup Items Get all popup items  |
| ATK Themes - Get all themes | ATK Themes Get all themes  |
| Getall CommonLookupsLOV - Get all common lookups | Common Lookups List of Values Get all common lookups  |
| Getall CurrenciesLOV - Get all currencies | Currencies List of Values Get all currencies  |
| Getall GenericLookupsLOV - Get all generic lookups | Generic Lookups List of Values Get all generic lookups  |
| Getall ProfileValues - Get all profile values | Profile Values  Get all profile values  |
| Getall SetEnabledLookupsLOV - Get all set-enabled lookups | Set-Enabled Lookups List of Values Get all set-enabled lookups  |
| Getall SetupOfferingCSVExports-SetupOfferingCSVExportProcess - GET all action not supported | Setup Offering CSV Exports/Export Process GET all action not supported  |
| Getall StandardLookupsLOV - Get all standard lookups | Standard Lookups List of Values Get all standard lookups  |
| Getall TerritoriesLOV - Get all territorries | Territories List of Values Get all territorries  |
| Getall ValueSets - Get all value sets | Value Sets Get all value sets  |
| Getall ValueSets-validationTable - Get all table-validated of a value set | Value Sets/Table-Validated Value Sets get Get all table-validated of a value set  |
| Getall ValueSets-values - Get all values of a value sets | Value Sets/Values Get all values of a value sets  |
| GetTaskAttachments - Retrieve Task Attachments | Manage Tasks  Retrieves task attachments. If Oracle Content and Experience (documents) is enabled for this task, attachments do not apply. Retrieve Task Attachments  |
| GetTaskAttachmentStream - Retrieve a Task Attachment as a Stream | Manage Tasks  Retrieves an attachment by name as a stream. If Oracle Content and Experience (documents) is enabled for this task, attachments do not apply. Retrieve a Task Attachment as a Stream  |
| GetTaskComments - Retrieve Task Comments | Manage Tasks Retrieves task comments. Retrieve Task Comments  |
| GetTaskForm - Retrieve a Task Form Uri | Manage Tasks Retrieves the task form uri. Retrieve a Task Form Uri  |
| GetTaskHistory - Retrieve Task History | Manage Tasks  Retrieves the history of a task. Retrieve Task History  |
| GetTaskPayload - Retrieve Task Payload | Manage Tasks  Retrieves the payload of a task. Retrieve Task Payload  |
| GetTasks - Retrieve a Task List | Manage Tasks  Retrieves a task list for the current user. Retrieve a Task List  |
| GetViews - Retrieve Custom Task Lists | Manage Tasks  Retrieves a list of custom task lists (views) that the current user has defined. Retrieve Custom Task Lists  |
| Persons - Get all persons | Persons Get all persons  |
| Search - Get all resource types | Resource Types Get all resource types  |
| Search - Get all roles | Roles Get all roles  |
| Search - Get all schemas | Schemas Get all schemas  |
| Search - Get all user requests | User Requests Get all user requests  |
| Search - Get all users | Users Get all users  |
| Setup Offering CSV Exports - GET all action not supported | Setup Offering CSV Exports GET all action not supported  |
| Setup Offering CSV Exports/Export Criteria - GET all action not supported | Setup Offering CSV Exports/Export Criteria GET all action not supported  |
| Setup Offering CSV Exports/Export Process/Export Process Result - GET all action not supported | Setup Offering CSV Exports/Export Process/Export Process Result GET all action not supported  |
| Setup Offering CSV Exports/Export Process/Export Process Result - Get an export process CSV template report | Setup Offering CSV Exports/Export Process/Export Process Result Get an export process CSV template report  |
| Setup Offering CSV Exports/Export Process/Export Process Result - Get an export process file content | Setup Offering CSV Exports/Export Process/Export Process Result Get an export process file content  |
| Setup Offering CSV Exports/Export Process/Export Process Result - Get an export process log | Setup Offering CSV Exports/Export Process/Export Process Result Get an export process log  |
| Setup Offering CSV Exports/Export Process/Export Process Result - Get an export process result report | Setup Offering CSV Exports/Export Process/Export Process Result Get an export process result report  |
| Setup Offering CSV Exports/Export Process/Export Process Result - Get an export process result summary report | Setup Offering CSV Exports/Export Process/Export Process Result Get an export process result summary report  |
| Setup Offering CSV Imports - GET all action not supported | Setup Offering CSV Imports GET all action not supported  |
| Setup Offering CSV Imports/Import Process - GET action not supported | Setup Offering CSV Imports/Import Process GET action not supported  |
| Setup Offering CSV Imports/Import Process - GET all action not supported | Setup Offering CSV Imports/Import Process GET all action not supported  |
| Setup Offering CSV Imports/Import Process/Import Process Result - GET all action not supported | Setup Offering CSV Imports/Import Process/Import Process Result GET all action not supported  |
| Setup Offering CSV Imports/Import Process/Import Process Result - Get an import process log | Setup Offering CSV Imports/Import Process/Import Process Result Get an import process log  |
| Setup Offering CSV Imports/Import Process/Import Process Result - Get an import process result report | Setup Offering CSV Imports/Import Process/Import Process Result Get an import process result report  |
| Setup Offering CSV Imports/Import Process/Import Process Result - Get an import process result summary report | Setup Offering CSV Imports/Import Process/Import Process Result Get an import process result summary report  |
| Setup Offering CSV Template Exports - GET all action not supported | Setup Offering CSV Template Exports GET all action not supported  |
| Setup Offering CSV Template Exports/Export Process - GET all action not supported | Setup Offering CSV Template Exports/Export Process GET all action not supported  |
| Setup Offering CSV Template Exports/Export Process/Export Process Result - Get a template export process file content | Setup Offering CSV Template Exports/Export Process/Export Process Result Get a template export process file content  |
| Setup Offering CSV Template Exports/Export Process/Export Process Result - Get a template export process log | Setup Offering CSV Template Exports/Export Process/Export Process Result Get a template export process log  |
| Setup Offering CSV Template Exports/Export Process/Export Process Result - GET all action not supported | Setup Offering CSV Template Exports/Export Process/Export Process Result GET all action not supported  |
| Setup Offering CSV Template Exports/Export Process/Export Process Result - Get an export process CSV template report | Setup Offering CSV Template Exports/Export Process/Export Process Result Get an export process CSV template report  |
| Setup Offering CSV Template Exports/Export Process/Export Process Result - Get an export process result report | Setup Offering CSV Template Exports/Export Process/Export Process Result Get an export process result report  |
| Setup Offering CSV Template Exports/Export Process/Export Process Result - Get an export process result summary report | Setup Offering CSV Template Exports/Export Process/Export Process Result Get an export process result summary report  |
| Setup Task CSV Exports - GET all action not supported | Setup Task CSV Exports GET all action not supported  |
| Setup Task CSV Exports/Export Criteria - GET all action not supported | Setup Task CSV Exports/Export Criteria GET all action not supported  |
| Setup Task CSV Exports/Export Process - GET all action not supported | Setup Task CSV Exports/Export Process GET all action not supported  |
| Setup Task CSV Exports/Export Process/Export Process Result - GET all action not supported | Setup Task CSV Exports/Export Process/Export Process Result GET all action not supported  |
| Setup Task CSV Exports/Export Process/Export Process Result - Get an export process CSV template report | Setup Task CSV Exports/Export Process/Export Process Result Get an export process CSV template report  |
| Setup Task CSV Exports/Export Process/Export Process Result - Get an export process file content | Setup Task CSV Exports/Export Process/Export Process Result Get an export process file content  |
| Setup Task CSV Exports/Export Process/Export Process Result - Get an export process log | Setup Task CSV Exports/Export Process/Export Process Result Get an export process log  |
| Setup Task CSV Exports/Export Process/Export Process Result - Get an export process result report | Setup Task CSV Exports/Export Process/Export Process Result Get an export process result report  |
| Setup Task CSV Exports/Export Process/Export Process Result - Get an export process result summary report | Setup Task CSV Exports/Export Process/Export Process Result Get an export process result summary report  |
| Setup Task CSV Imports - GET all action not supported | Setup Task CSV Imports GET all action not supported  |
| Setup Task CSV Imports/Import Process - GET action not supported | Setup Task CSV Imports/Import Process GET action not supported  |
| Setup Task CSV Imports/Import Process - GET all action not supported | Setup Task CSV Imports/Import Process GET all action not supported  |
| Setup Task CSV Imports/Import Process/Import Process Result - GET all action not supported | Setup Task CSV Imports/Import Process/Import Process Result GET all action not supported  |
| Setup Task CSV Imports/Import Process/Import Process Result - Get an import process log | Setup Task CSV Imports/Import Process/Import Process Result Get an import process log  |
| Setup Task CSV Imports/Import Process/Import Process Result - Get an import process result report | Setup Task CSV Imports/Import Process/Import Process Result Get an import process result report  |
| Setup Task CSV Imports/Import Process/Import Process Result - Get an import process result summary report | Setup Task CSV Imports/Import Process/Import Process Result Get an import process result summary report  |
| Setup Task CSV Template Exports - GET all action not supported | Setup Task CSV Template Exports GET all action not supported  |
| Setup Task CSV Template Exports/Export Process - GET all action not supported | Setup Task CSV Template Exports/Export Process GET all action not supported  |
| Setup Task CSV Template Exports/Export Process/Export Process Result - Get a template export process file content | Setup Task CSV Template Exports/Export Process/Export Process Result Get a template export process file content  |
| Setup Task CSV Template Exports/Export Process/Export Process Result - Get a template export process log | Setup Task CSV Template Exports/Export Process/Export Process Result Get a template export process log  |
| Setup Task CSV Template Exports/Export Process/Export Process Result - GET all action not supported | Setup Task CSV Template Exports/Export Process/Export Process Result GET all action not supported  |
| Setup Task CSV Template Exports/Export Process/Export Process Result - Get an export process CSV template report | Setup Task CSV Template Exports/Export Process/Export Process Result Get an export process CSV template report  |
| Setup Task CSV Template Exports/Export Process/Export Process Result - Get an export process result report | Setup Task CSV Template Exports/Export Process/Export Process Result Get an export process result report  |
| Setup Task CSV Template Exports/Export Process/Export Process Result - Get an export process result summary report | Setup Task CSV Template Exports/Export Process/Export Process Result Get an export process result summary report  |
| Sign In and Sign Out Audit - Get the recorded events | Sign In and Sign Out Audit Returns the recorded events for the specified time period. Get the recorded events  |
| Sign In and Sign Out Audit - Get the recorded events for a period | Sign In and Sign Out Audit Returns a count of the total number of recorded events for the specified time period. Get the recorded events for a period  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Announcements - Update an announcement (PATCH) | Announcements Update an announcement  |
| ATK Popup Items - Approve or reject a popup item (POST) | ATK Popup Items Approve or Reject actions on the popup Item Approve or reject a popup item  |
| ATK Popup Items - Update a popup item (PATCH) | ATK Popup Items Update a popup item  |
| ATK Themes - Update a theme (PATCH) | ATK Themes Update a theme  |
| PerformBulkActions - Perform Bulk Action on Tasks (PUT) | Manage Tasks Performs the action on multiple tasks based on the payload. The following actions are available and the actions depend on the application: APPROVE, REJECT, REASSIGN, DELEGATE, WITHDRAW, SUSPEND, RESUME, DELETE, PURGE and ESCALATE. Custom Actions depend on the task definition. Perform Bulk Action on Tasks  |
| PerformTaskAction - Perform Action on Task (PUT) | Manage Tasks Performs the action on a specific task identifier, based on the payload. The following actions are available and the actions depend on the application: APPROVE, REJECT, REASSIGN, DELEGATE, WITHDRAW, SUSPEND, RESUME, DELETE, PURGE, ACQUIRE, RELEASE, PUSHBACK, ERROR, ADHOC_ROUTE, INFO_REQUEST, INFO_SUBMIT, SKIP_CURRENT_ASSIGNMENT, SAVE and ESCALATE. Custom Actions depend on the task definition. Perform Action on Task  |
| Persons - Update a person (PATCH) | Persons Update a person  |
| Replace - Replace a user (PUT) | Users Replace a user  |
| Update - Update a role (PATCH) | Roles Update a role  |
| Update - Update a user (PATCH) | Users Update a user  |
| Update ValueSets-values - Update values of a value set (PATCH) | Value Sets/Values Update values of a value set  |
| UpdateTaskPayload - Update a Task Payload (PUT) | Manage Tasks  Updates the payload of a task. Update a Task Payload  |


