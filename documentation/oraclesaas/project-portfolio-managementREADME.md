
# project-portfolio-management


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Award Budgets - Create an award budget version | Award Budgets Create an award budget version Create an award budget version  |
| Award Budgets - Reprocess all failed award budgets | Award Budgets This action will reprocess all the award budget versions which are showing as failed and bring them back to working state. Reprocess all failed award budgets  |
| Award Budgets/Planning Options/Planning Options Descriptive Flexfields - Create descriptive flexfields for planning options | Award Budgets/Planning Options/Planning Options Descriptive Flexfields Create descriptive flexfields for planning options  |
| Award Budgets/Planning Resources - Create a planning resource for an award budget ver... | Award Budgets/Planning Resources Create a planning resource for an award budget version  |
| Award Budgets/Planning Resources/Planning Amounts - Create a plan line for a planning resource | Award Budgets/Planning Resources/Planning Amounts Create a plan line for a planning resource  |
| Award Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields - Create a descriptive flexfield for summary plannin... | Award Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields Create a descriptive flexfield for summary planning amounts  |
| Award Budgets/Planning Resources/Planning Amounts/Planning Amount Details - Create planning amount details for a period for a ... | Award Budgets/Planning Resources/Planning Amounts/Planning Amount Details Create planning amount details for a period for a plan line  |
| Awards - Create an award | Awards Create an award  |
| Awards/Award Attachments - Create award attachments | Awards/Award Attachments Create award attachments  |
| Awards/Award Budget Periods - Create an award budget period | Awards/Award Budget Periods Create an award budget period  |
| Awards/Award Certifications - Create an award certification | Awards/Award Certifications Create an award certification  |
| Awards/Award CFDAs - Create an award CFDA | Awards/Award CFDAs Create an award CFDA  |
| Awards/Award Department Credits - Create an award department credit | Awards/Award Department Credits Create an award department credit  |
| Awards/Award Descriptive Flexfields - Create award descriptive flexfields | Awards/Award Descriptive Flexfields Create award descriptive flexfields  |
| Awards/Award Funding Sources - Create an award funding source | Awards/Award Funding Sources Create an award funding source  |
| Awards/Award Fundings - Create award funding | Awards/Award Fundings Create award funding  |
| Awards/Award Fundings/Award Project Fundings - Create award project funding | Awards/Award Fundings/Award Project Fundings Create award project funding  |
| Awards/Award Keywords - Create an award keyword | Awards/Award Keywords Create an award keyword  |
| Awards/Award Personnel - Create an award personnel | Awards/Award Personnel Create an award personnel  |
| Awards/Award Personnel/Award Personnel Descriptive Flexfields - Create award personnel descriptive flexfields | Awards/Award Personnel/Award Personnel Descriptive Flexfields Create award personnel descriptive flexfields  |
| Awards/Award Projects - Create an award project | Awards/Award Projects Create an award project  |
| Awards/Award Projects/Award Project Attachments - Create award project attachments | Awards/Award Projects/Award Project Attachments Create award project attachments  |
| Awards/Award Projects/Award Project Certifications - Create an award project certification | Awards/Award Projects/Award Project Certifications Create an award project certification  |
| Awards/Award Projects/Award Project Descriptive Flexfields - Create award project descriptive flexfields | Awards/Award Projects/Award Project Descriptive Flexfields Create award project descriptive flexfields  |
| Awards/Award Projects/Award Project Funding Sources - Create an award project funding source | Awards/Award Projects/Award Project Funding Sources Create an award project funding source  |
| Awards/Award Projects/Award Project Keywords - Create an award project keyword | Awards/Award Projects/Award Project Keywords Create an award project keyword  |
| Awards/Award Projects/Award Project Organization Credits - Create an award project department credit | Awards/Award Projects/Award Project Organization Credits Create an award project department credit  |
| Awards/Award Projects/Award Project Override Burden Schedules - Create a burden schedule for an award project. | Awards/Award Projects/Award Project Override Burden Schedules Create a burden schedule for an award project.  |
| Awards/Award Projects/Award Project Override Burden Schedules/Versions - Create a version in burden schedule. | Awards/Award Projects/Award Project Override Burden Schedules/Versions Create a version in burden schedule.  |
| Awards/Award Projects/Award Project Override Burden Schedules/Versions/Multipliers - Create a multiplier in burden schedule version. | Awards/Award Projects/Award Project Override Burden Schedules/Versions/Multipliers Create a multiplier in burden schedule version.  |
| Awards/Award Projects/Award Project Personnel - Create an award project personnel | Awards/Award Projects/Award Project Personnel Create an award project personnel  |
| Awards/Award Projects/Award Project Personnel/Award Project Personnel Descriptive Flexfields - Create award project personnel descriptive flexfie... | Awards/Award Projects/Award Project Personnel/Award Project Personnel Descriptive Flexfields Create award project personnel descriptive flexfields  |
| Awards/Award Projects/Award Project Reference Types - Create an award project reference | Awards/Award Projects/Award Project Reference Types Create an award project reference  |
| Awards/Award Projects/Award Project Task Burden Schedules - Create an award project task burden schedule | Awards/Award Projects/Award Project Task Burden Schedules Create an award project task burden schedule  |
| Awards/Award References - Create an award reference | Awards/Award References Create an award reference  |
| Awards/Award Terms - Create an award term | Awards/Award Terms Create an award term  |
| Change Orders - Create a change order | Change Orders Create a change order  |
| Change Orders/Change Impacts - Create impacts for a change order | Change Orders/Change Impacts Create impacts for a change order  |
| Change Orders/Change Participants - Create participants for a change order | Change Orders/Change Participants Create participants for a change order  |
| Deliverables - Create deliverables | Deliverables Create deliverables  |
| Deliverables/Attachment to a Deliverable - Create attachments for a deliverable | Deliverables/Attachment to a Deliverable Create attachments for a deliverable  |
| Deliverables/Deliverable and Project Task Associations - Create associations between a deliverable and proj... | Deliverables/Deliverable and Project Task Associations Create associations between a deliverable and project tasks  |
| Deliverables/Deliverable and Requirement Associations - Create associations between a deliverable and requ... | Deliverables/Deliverable and Requirement Associations Create associations between a deliverable and requirements  |
| Financial Project Plans - Create a resource assignment for a financial proje... | Financial Project Plans Create a resource assignment for a financial project plan version using the DeferPlanning flag in UPSERT mode Create a resource assignment for a financial project plan version using the DeferPlanning flag in UPSERT mode  |
| Financial Project Plans - Reprocess all failed financial project plan versio... | Financial Project Plans This action will reprocess all the financial project plan versions that failed and places them back in the Current working state. Reprocess all failed financial project plan versions  |
| Financial Project Plans - Set financial project plan baseline | Financial Project Plans Capture project progress in draft status. Set financial project plan baseline  |
| Financial Project Plans/Resource Assignments - Create a resource assignments for a financial proj... | Financial Project Plans/Resource Assignments Create a resource assignments for a financial project plan version  |
| Financial Project Plans/Resource Assignments/Planning Amounts - Create a planning amounts for a resource assignmen... | Financial Project Plans/Resource Assignments/Planning Amounts Create a planning amounts for a resource assignment  |
| Financial Project Plans/Resource Assignments/Planning Amounts/Plan Lines Descriptive Flexfields - Create a descriptive flexfield for summary plannin... | Financial Project Plans/Resource Assignments/Planning Amounts/Plan Lines Descriptive Flexfields Create a descriptive flexfield for summary planning amounts  |
| Financial Project Plans/Resource Assignments/Planning Amounts/Planning Amount Details - Create a periodic planning amounts for a resource ... | Financial Project Plans/Resource Assignments/Planning Amounts/Planning Amount Details Create a periodic planning amounts for a resource assignment  |
| Grants Personnel - Create grants personnel | Grants Personnel Create grants personnel  |
| Grants Personnel/Grants Personnel  Descriptive Flexfields - Create grants personnel descriptive flexfields | Grants Personnel/Grants Personnel  Descriptive Flexfields Create grants personnel descriptive flexfields  |
| Grants Personnel/Grants Personnel Keywords - Create grants personnel keywords | Grants Personnel/Grants Personnel Keywords Create grants personnel keywords  |
| Person Assignment Labor Schedules - Create many Person Assignment Labor Schedule heade... | Person Assignment Labor Schedules Create many Person Assignment Labor Schedule headers.  |
| Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions - Create many versions within a Person Assignment La... | Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions Create many versions within a Person Assignment Labor Schedule header.  |
| Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions/Person Assignment Labor Schedule Version Rules - Create many distribution rules within a version | Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions/Person Assignment Labor Schedule Version Rules Create many distribution rules within a version  |
| Planning Resource Breakdown Structures for Projects - Create a planning resource breakdown structure ass... | Planning Resource Breakdown Structures for Projects Create a planning resource breakdown structure assignment to a project  |
| Planning Resource Breakdown Structures for Projects/Elements - Create a resource in a planning resource breakdown... | Planning Resource Breakdown Structures for Projects/Elements Create a resource in a planning resource breakdown structure  |
| Project Assets - Create project assets | Project Assets Create project assets  |
| Project Assets/Project Asset Assignments - Create project asset assignments | Project Assets/Project Asset Assignments Create project asset assignments  |
| Project Assets/Project Asset Descriptive Flexfields - Create project assets - key flexfields | Project Assets/Project Asset Descriptive Flexfields Create project assets - key flexfields  |
| Project Billing Events - Create a project billing event | Project Billing Events Create a project billing event  |
| Project Billing Events/Project Billing Event Descriptive Flexfields - Create a project billing event descriptive flexfie... | Project Billing Events/Project Billing Event Descriptive Flexfields Create a project billing event descriptive flexfield  |
| Project Budgets - Adjust for range of projects | Project Budgets Adjusts the project budget version for the range of projects using the adjustment type and adjustment percentage. Adjust for range of projects  |
| Project Budgets - Create a project budget version | Project Budgets Create a project budget version Create a project budget version  |
| Project Budgets - Refresh rates for range of projects | Project Budgets Refresh rates for the project budget version for the range of projects from the rate schedules. Refresh rates for range of projects  |
| Project Budgets - Reprocess all failed project budgets | Project Budgets This action will reprocess all the project budget versions which are showing as failed and bring them back to working state. Reprocess all failed project budgets  |
| Project Budgets/Planning Options/Planning Options Descriptive Flexfields - Create descriptive flexfields for planning options | Project Budgets/Planning Options/Planning Options Descriptive Flexfields Create descriptive flexfields for planning options  |
| Project Budgets/Planning Resources - Create a planning resource for a project budget ve... | Project Budgets/Planning Resources Create a planning resource for a project budget version  |
| Project Budgets/Planning Resources/Planning Amounts - Create a plan line for a planning resource | Project Budgets/Planning Resources/Planning Amounts Create a plan line for a planning resource  |
| Project Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields - Create a descriptive flexfield for summary plannin... | Project Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields Create a descriptive flexfield for summary planning amounts  |
| Project Budgets/Planning Resources/Planning Amounts/Planning Amount Details - Create planning amount details for a period for a ... | Project Budgets/Planning Resources/Planning Amounts/Planning Amount Details Create planning amount details for a period for a plan line  |
| Project Commitments - Create a project commitment | Project Commitments Create a project commitment  |
| Project Enterprise Expense Resources - Create a project enterprise expense resource | Project Enterprise Expense Resources Create a project enterprise expense resource  |
| Project Enterprise Labor Resources - Create a project enterprise labor resource | Project Enterprise Labor Resources Create a project enterprise labor resource  |
| Project Expense Resources - Create a project expense resource | Project Expense Resources Create a project expense resource  |
| Project Forecasts - Create a project forecast version | Project Forecasts Create a project forecast version Create a project forecast version  |
| Project Forecasts - Reprocess all failed project forecasts | Project Forecasts This action will reprocess all the project forecast versions which are showing as failed and bring them back to working state. Reprocess all failed project forecasts  |
| Project Forecasts/Planning Options/Planning Options Descriptive Flexfields - Create a descriptive flexfield for planning option... | Project Forecasts/Planning Options/Planning Options Descriptive Flexfields Create a descriptive flexfield for planning options  |
| Project Forecasts/Planning Resources - Create a planning resource for a project forecast ... | Project Forecasts/Planning Resources Create a planning resource for a project forecast version  |
| Project Forecasts/Planning Resources/Planning Amounts - Create a summary planning amount for a planning re... | Project Forecasts/Planning Resources/Planning Amounts Create a summary planning amount for a planning resource  |
| Project Forecasts/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields - Create a descriptive flexfield for summary plannin... | Project Forecasts/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields Create a descriptive flexfield for summary planning amounts  |
| Project Forecasts/Planning Resources/Planning Amounts/Planning Amount Details - Create details for a period for a planning amount | Project Forecasts/Planning Resources/Planning Amounts/Planning Amount Details Create details for a period for a planning amount  |
| Project Issues - Create issues | Project Issues Create issues  |
| Project Issues/Project Issue Action Items - Create action items | Project Issues/Project Issue Action Items Create action items  |
| Project Labor Resources - Create a project labor resource | Project Labor Resources Create a project labor resource  |
| Project Numbering Configurations/Project Numbering Configuration Details - Create a project numbering configuration detail | Project Numbering Configurations/Project Numbering Configuration Details Create a project numbering configuration detail  |
| Project Plan Resource Requests - Create one or more project resource requests | Project Plan Resource Requests Create one or more project resource requests  |
| Project Plan Resource Requests/Project Plan Resource Request Descriptive Flexfields - Create one or more descriptive flexfields for a pr... | Project Plan Resource Requests/Project Plan Resource Request Descriptive Flexfields Create one or more descriptive flexfields for a project resource request  |
| Project Plan Resource Requests/Project Plan Resource Request Qualifications - Create one or more qualifications for a project re... | Project Plan Resource Requests/Project Plan Resource Request Qualifications Create one or more qualifications for a project resource request  |
| Project Plans/Baselines - Create a baseline | Project Plans/Baselines Create a baseline  |
| Project Plans/Project Descriptive Flexfields - Create descriptive flexfields for a project | Project Plans/Project Descriptive Flexfields Create descriptive flexfields for a project  |
| Project Plans/Task Dependencies - Create a dependency between two tasks of a project | Project Plans/Task Dependencies Create a dependency between two tasks of a project  |
| Project Plans/Task Expense Resource Assignments - Create an expense resource assignment for a task | Project Plans/Task Expense Resource Assignments Create an expense resource assignment for a task  |
| Project Plans/Task Labor Resource Assignments - Create a labor resource assignment for a task | Project Plans/Task Labor Resource Assignments Create a labor resource assignment for a task  |
| Project Plans/Task Work Items - Create a work item for a task | Project Plans/Task Work Items Create a work item for a task  |
| Project Plans/Tasks - Create a task in a project | Project Plans/Tasks Create a task in a project  |
| Project Plans/Tasks/Gate Approvers - Create an approver for a gate in a project | Project Plans/Tasks/Gate Approvers Create an approver for a gate in a project  |
| Project Plans/Tasks/Tasks Descriptive Flexfields - Create descriptive flexfields for a task | Project Plans/Tasks/Tasks Descriptive Flexfields Create descriptive flexfields for a task  |
| Project Process Configurators - Create a project process configurator | Project Process Configurators Create a project process configurator  |
| Project Process Configurators/Source Assignments - Create sources for a configurator | Project Process Configurators/Source Assignments Create sources for a configurator  |
| Project Progress - Capture project progress | Project Progress Capture project progress in draft status. Capture project progress  |
| Project Progress - Publish Project Progress | Project Progress Publish project progress so that the progress can be viewed by others. Publish Project Progress  |
| Project Resource Actual Hours - Create one or more project resource actual records... | Project Resource Actual Hours Create one or more project resource actual records.  |
| Project Resource Assignments - Create a project resource assignment | Project Resource Assignments Create a project resource assignment  |
| Project Resource Pools - Create one or more project resource pools. | Project Resource Pools Create one or more project resource pools.  |
| Project Resource Pools/Project Resource Pool Managers - Create one or more project resource pool managers ... | Project Resource Pools/Project Resource Pool Managers Create one or more project resource pool managers for a project resource pool.  |
| Project Resource Pools/Project Resource Pool Members - Create one or more project resource pool members u... | Project Resource Pools/Project Resource Pool Members Create one or more project resource pool members under a project resource pool.  |
| Project Resource Requests - Create one or more project resource requests | Project Resource Requests Create one or more project resource requests  |
| Project Resource Requests/Project Resource Request Descriptive Flexfields - Create one or more descriptive flexfields for a pr... | Project Resource Requests/Project Resource Request Descriptive Flexfields Create one or more descriptive flexfields for a project resource request  |
| Project Resource Requests/Project Resource Request Lines - Create one or more nominated resources for a proje... | Project Resource Requests/Project Resource Request Lines Create one or more nominated resources for a project resource request  |
| Project Resource Requests/Project Resource Request Qualifications - Create one or more qualifications for a project re... | Project Resource Requests/Project Resource Request Qualifications Create one or more qualifications for a project resource request  |
| Project Templates - Create a project template | Project Templates Create a project template  |
| Project Templates/Project Classifications - Create a project classification | Project Templates/Project Classifications Create a project classification  |
| Project Templates/Project Customers - Create a project customer | Project Templates/Project Customers Create a project customer  |
| Project Templates/Project Team Members - Create a project team member | Project Templates/Project Team Members Create a project team member  |
| Project Templates/Project Transaction Controls - Create a project transaction control | Project Templates/Project Transaction Controls Create a project transaction control  |
| Project Templates/Quick Entries - Create a quick entry | Project Templates/Quick Entries Create a quick entry  |
| Project Templates/Setup Options - Create a project template setup option | Project Templates/Setup Options Create a project template setup option  |
| Project Templates/Tasks - Create a project task | Project Templates/Tasks Create a project task  |
| Project Templates/Tasks/Task Transaction Controls - Create a task transaction control | Project Templates/Tasks/Task Transaction Controls Create a task transaction control  |
| Projects - Create a project | Projects Create a project  |
| Projects Users - Create signed in project enterprise users | Projects Users Create signed in project enterprise users  |
| Projects Users/To Do Tasks - Create  to do tasks for a user | Projects Users/To Do Tasks Create  to do tasks for a user  |
| Projects/Project Classifications - Create a project classification | Projects/Project Classifications Create a project classification  |
| Projects/Project Customers - Create a project customer | Projects/Project Customers Create a project customer  |
| Projects/Project Descriptive Flexfields - Create a project descriptive flexfield | Projects/Project Descriptive Flexfields Create a project descriptive flexfield  |
| Projects/Project Opportunities - Create a project opportunity | Projects/Project Opportunities Create a project opportunity  |
| Projects/Project Team Members - Create a project team member | Projects/Project Team Members Create a project team member  |
| Projects/Project Transaction Controls - Create a project transaction control | Projects/Project Transaction Controls Create a project transaction control  |
| Projects/Task Dependencies - Create a task dependency | Projects/Task Dependencies Create a task dependency  |
| Projects/Tasks - Create a project task | Projects/Tasks Create a project task  |
| Projects/Tasks/Expense Resource Assignments - Create an expense resource assignment for a projec... | Projects/Tasks/Expense Resource Assignments Create an expense resource assignment for a project task  |
| Projects/Tasks/Labor Resource Assignments - Create a labor resource assignment for a project t... | Projects/Tasks/Labor Resource Assignments Create a labor resource assignment for a project task  |
| Projects/Tasks/Task Transaction Controls - Create a task transaction control | Projects/Tasks/Task Transaction Controls Create a task transaction control  |
| Projects/Tasks/Tasks Descriptive Flexfields - Create a descriptive flexfield for a project task | Projects/Tasks/Tasks Descriptive Flexfields Create a descriptive flexfield for a project task  |
| Rate Schedules - Create a rate schedule | Rate Schedules Create a rate schedule  |
| Rate Schedules/Rate Schedule Descriptive Flexfields - Create rate schedule descriptive flexfields | Rate Schedules/Rate Schedule Descriptive Flexfields Create rate schedule descriptive flexfields  |
| Requirements - Create requirements | Requirements Create requirements  |
| Requirements/BacklogItems - Create backlog items | Requirements/BacklogItems Create backlog items  |
| Requirements/BacklogItems/AcceptanceCriterions - Create acceptance criteria | Requirements/BacklogItems/AcceptanceCriterions Create acceptance criteria  |
| Requirements/ChildRequirements - Create child requirements | Requirements/ChildRequirements Create child requirements  |
| Requirements/ChildRequirements/BacklogItems - Create backlog items | Requirements/ChildRequirements/BacklogItems Create backlog items  |
| Requirements/ChildRequirements/BacklogItems/AcceptanceCriterions - Create acceptance criteria | Requirements/ChildRequirements/BacklogItems/AcceptanceCriterions Create acceptance criteria  |
| Requirements/ChildRequirements/ChildRequirements - Create child requirements | Requirements/ChildRequirements/ChildRequirements Create child requirements  |
| Sponsors - Create one or many sponsors | Sponsors Create one or many sponsors  |
| Sponsors/Sponsor Reference Types - Create one or many sponsor reference types | Sponsors/Sponsor Reference Types Create one or many sponsor reference types  |
| Unprocessed Project Costs - Create an unprocessed project cost | Unprocessed Project Costs Create an unprocessed project cost  |
| Unprocessed Project Costs/Project Standard Cost Collection Flexfields - Create standard cost collection information for un... | Unprocessed Project Costs/Project Standard Cost Collection Flexfields Create standard cost collection information for unprocessed project costs  |
| Unprocessed Project Costs/Unprocessed Project Cost Descriptive Flexfields - Create additional information for unprocessed proj... | Unprocessed Project Costs/Unprocessed Project Cost Descriptive Flexfields Create additional information for unprocessed project costs  |
| Work Plan Templates - Create a work plan template | Work Plan Templates Create a work plan template  |
| Work Plan Templates/Task Dependencies - Create a dependency between two tasks | Work Plan Templates/Task Dependencies Create a dependency between two tasks  |
| Work Plan Templates/Task Expense Resource Assignments - Create an expense resource assignment for a task | Work Plan Templates/Task Expense Resource Assignments Create an expense resource assignment for a task  |
| Work Plan Templates/Task Labor Resource Assignments - Create a labor resource assignment for a task | Work Plan Templates/Task Labor Resource Assignments Create a labor resource assignment for a task  |
| Work Plan Templates/Tasks - Create a task | Work Plan Templates/Tasks Create a task  |
| Work Plan Templates/Tasks/Gate Approvers - Create an approver for a gate | Work Plan Templates/Tasks/Gate Approvers Create an approver for a gate  |
| Work Plan Templates/Tasks/Task Descriptive Flexfields - Create task descriptive flexfields | Work Plan Templates/Tasks/Task Descriptive Flexfields Create task descriptive flexfields  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Award Budgets - Delete an award budget version | Award Budgets Delete an award budget version  |
| Award Budgets/Planning Resources - Delete a planning resource for an award budget ver... | Award Budgets/Planning Resources Delete a planning resource for an award budget version  |
| Award Budgets/Planning Resources/Planning Amounts - Delete a plan line for a planning resource | Award Budgets/Planning Resources/Planning Amounts Delete a plan line for a planning resource  |
| Award Budgets/Planning Resources/Planning Amounts/Planning Amount Details - Delete planning amount details for a period for a ... | Award Budgets/Planning Resources/Planning Amounts/Planning Amount Details Delete planning amount details for a period for a plan line  |
| Awards/Award Attachments - Delete an award attachment | Awards/Award Attachments Delete an award attachment  |
| Awards/Award Budget Periods - Delete an award budget period | Awards/Award Budget Periods Delete an award budget period  |
| Awards/Award Certifications - Delete an award certification | Awards/Award Certifications Delete an award certification  |
| Awards/Award CFDAs - Delete an award CFDA | Awards/Award CFDAs Delete an award CFDA  |
| Awards/Award Department Credits - Delete an award department credit | Awards/Award Department Credits Delete an award department credit  |
| Awards/Award Funding Sources - Delete an award funding source | Awards/Award Funding Sources Delete an award funding source  |
| Awards/Award Fundings - Delete award funding | Awards/Award Fundings Delete award funding  |
| Awards/Award Fundings/Award Project Fundings - Delete award project funding | Awards/Award Fundings/Award Project Fundings Delete award project funding  |
| Awards/Award Keywords - Delete an award keyword | Awards/Award Keywords Delete an award keyword  |
| Awards/Award Personnel - Delete an award personnel | Awards/Award Personnel Delete an award personnel  |
| Awards/Award Projects - Delete an award project | Awards/Award Projects Delete an award project  |
| Awards/Award Projects/Award Project Attachments - Delete an award project attachment | Awards/Award Projects/Award Project Attachments Delete an award project attachment  |
| Awards/Award Projects/Award Project Certifications - Delete an award project certification | Awards/Award Projects/Award Project Certifications Delete an award project certification  |
| Awards/Award Projects/Award Project Funding Sources - Delete an award project funding source | Awards/Award Projects/Award Project Funding Sources Delete an award project funding source  |
| Awards/Award Projects/Award Project Keywords - Delete an award project keyword | Awards/Award Projects/Award Project Keywords Delete an award project keyword  |
| Awards/Award Projects/Award Project Organization Credits - Delete an award project department credit | Awards/Award Projects/Award Project Organization Credits Delete an award project department credit  |
| Awards/Award Projects/Award Project Personnel - Delete an award project personnel | Awards/Award Projects/Award Project Personnel Delete an award project personnel  |
| Awards/Award Projects/Award Project Reference Types - Delete an award project reference | Awards/Award Projects/Award Project Reference Types Delete an award project reference  |
| Awards/Award Projects/Award Project Task Burden Schedules - Delete an award project task burden schedule | Awards/Award Projects/Award Project Task Burden Schedules Delete an award project task burden schedule  |
| Awards/Award References - Delete an award reference | Awards/Award References Delete an award reference  |
| Awards/Award Terms - Delete an award term | Awards/Award Terms Delete an award term  |
| Change Orders - Delete a change order | Change Orders Delete a change order  |
| Change Orders/Change Impacts - Delete an impact for a change order | Change Orders/Change Impacts Delete an impact for a change order  |
| Change Orders/Change Participants - Delete a participant for a change order | Change Orders/Change Participants Delete a participant for a change order  |
| Deliverables - Delete a deliverable | Deliverables Delete a deliverable  |
| Deliverables/Attachment to a Deliverable - Delete an attachment to a deliverable | Deliverables/Attachment to a Deliverable Delete an attachment to a deliverable  |
| Deliverables/Deliverable and Project Task Associations - Delete an association between a deliverable and a ... | Deliverables/Deliverable and Project Task Associations Delete an association between a deliverable and a project task  |
| Deliverables/Deliverable and Requirement Associations - Delete an association between a deliverable and a ... | Deliverables/Deliverable and Requirement Associations Delete an association between a deliverable and a requirement  |
| Financial Project Plans/Resource Assignments - Delete a resource assignment for a financial proje... | Financial Project Plans/Resource Assignments Delete a resource assignment for a financial project plan version  |
| Grants Personnel - Delete a grants personnel | Grants Personnel Delete a grants personnel  |
| Grants Personnel/Grants Personnel Keywords - Delete a keyword associated to a grants personnel | Grants Personnel/Grants Personnel Keywords Delete a keyword associated to a grants personnel  |
| Person Assignment Labor Schedules - Delete a Person Assignment Labor Schedule header. | Person Assignment Labor Schedules Delete a Person Assignment Labor Schedule header.  |
| Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions - Delete a version within a Person Assignment Labor ... | Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions Delete a version within a Person Assignment Labor Schedule header.  |
| Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions/Person Assignment Labor Schedule Version Rules - Delete a distribution rule within a version | Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions/Person Assignment Labor Schedule Version Rules Delete a distribution rule within a version  |
| Project Assets - Delete a project asset | Project Assets Delete a project asset  |
| Project Assets/Project Asset Assignments - Delete an asset assignment for a project asset | Project Assets/Project Asset Assignments Delete an asset assignment for a project asset  |
| Project Billing Events - Delete a project billing event | Project Billing Events Delete a project billing event  |
| Project Budgets - Delete a project budget version | Project Budgets Delete a project budget version  |
| Project Budgets/Planning Resources - Delete a planning resource for a project budget ve... | Project Budgets/Planning Resources Delete a planning resource for a project budget version  |
| Project Budgets/Planning Resources/Planning Amounts - Delete a plan line for a planning resource | Project Budgets/Planning Resources/Planning Amounts Delete a plan line for a planning resource  |
| Project Budgets/Planning Resources/Planning Amounts/Planning Amount Details - Delete planning amount details for a period for a ... | Project Budgets/Planning Resources/Planning Amounts/Planning Amount Details Delete planning amount details for a period for a plan line  |
| Project Commitments - Delete a project commitment | Project Commitments Delete a project commitment  |
| Project Enterprise Expense Resources - Delete a project enterprise expense resource | Project Enterprise Expense Resources Delete a project enterprise expense resource  |
| Project Enterprise Labor Resources - Delete a project enterprise labor resource | Project Enterprise Labor Resources Delete a project enterprise labor resource  |
| Project Expense Resources - Delete a project expense resource | Project Expense Resources Delete a project expense resource  |
| Project Forecasts - Delete a project forecast version | Project Forecasts Delete a project forecast version  |
| Project Forecasts/Planning Resources - Delete a planning resource for a project forecast ... | Project Forecasts/Planning Resources Delete a planning resource for a project forecast version  |
| Project Forecasts/Planning Resources/Planning Amounts - Delete a summary planning amount for a planning re... | Project Forecasts/Planning Resources/Planning Amounts Delete a summary planning amount for a planning resource  |
| Project Forecasts/Planning Resources/Planning Amounts/Planning Amount Details - Delete details for a period for a planning amount | Project Forecasts/Planning Resources/Planning Amounts/Planning Amount Details Delete details for a period for a planning amount  |
| Project Issues - Delete an issue | Project Issues Delete an issue  |
| Project Issues/Project Issue Action Items - Delete an action item | Project Issues/Project Issue Action Items Delete an action item  |
| Project Labor Resources - Delete a project labor resource | Project Labor Resources Delete a project labor resource  |
| Project Numbering Configurations/Project Numbering Configuration Details - Delete a project numbering configuration detail | Project Numbering Configurations/Project Numbering Configuration Details Delete a project numbering configuration detail  |
| Project Plan Resource Requests - Delete a project resource request | Project Plan Resource Requests Delete a project resource request  |
| Project Plan Resource Requests/Project Plan Resource Request Qualifications - Delete a qualification for a project resource requ... | Project Plan Resource Requests/Project Plan Resource Request Qualifications Delete a qualification for a project resource request  |
| Project Plans/Baselines - Delete a baseline | Project Plans/Baselines Delete a baseline  |
| Project Plans/Task Dependencies - Delete a task dependency | Project Plans/Task Dependencies Delete a task dependency  |
| Project Plans/Task Expense Resource Assignments - Delete an expense resource assignment for a task | Project Plans/Task Expense Resource Assignments Delete an expense resource assignment for a task  |
| Project Plans/Task Labor Resource Assignments - Delete a labor resource assignment for a task | Project Plans/Task Labor Resource Assignments Delete a labor resource assignment for a task  |
| Project Plans/Task Work Items - Delete a work item for a task | Project Plans/Task Work Items Delete a work item for a task  |
| Project Plans/Tasks - Delete a task of a project | Project Plans/Tasks Delete a task of a project  |
| Project Plans/Tasks/Gate Approvers - Delete an approver of a gate in a project | Project Plans/Tasks/Gate Approvers Delete an approver of a gate in a project  |
| Project Process Configurators - Delete a project process configurator | Project Process Configurators Delete a project process configurator  |
| Project Resource Pools - Delete a project resource pool. | Project Resource Pools Delete a project resource pool.  |
| Project Resource Pools/Project Resource Pool Managers - Delete a project resource pool manager for a proje... | Project Resource Pools/Project Resource Pool Managers Delete a project resource pool manager for a project resource pool.  |
| Project Resource Requests - Delete a project resource request | Project Resource Requests Delete a project resource request  |
| Project Resource Requests/Project Resource Request Qualifications - Delete a qualification for a project resource requ... | Project Resource Requests/Project Resource Request Qualifications Delete a qualification for a project resource request  |
| Projects Users/To Do Tasks - Delete a to do task of a user | Projects Users/To Do Tasks Delete a to do task of a user  |
| Projects/Project Classifications - Delete a project classification | Projects/Project Classifications Delete a project classification  |
| Projects/Project Customers - Delete a project customer | Projects/Project Customers Delete a project customer  |
| Projects/Project Team Members - Delete a project team member | Projects/Project Team Members Delete a project team member  |
| Projects/Project Transaction Controls - Delete a project transaction control | Projects/Project Transaction Controls Delete a project transaction control  |
| Projects/Task Dependencies - Delete a task dependency | Projects/Task Dependencies Delete a task dependency  |
| Projects/Tasks - Delete a project task | Projects/Tasks Delete a project task  |
| Projects/Tasks/Expense Resource Assignments - Delete an expense resource assignment for a projec... | Projects/Tasks/Expense Resource Assignments Delete an expense resource assignment for a project task  |
| Projects/Tasks/Labor Resource Assignments - Delete a labor resource assignment for a project t... | Projects/Tasks/Labor Resource Assignments Delete a labor resource assignment for a project task  |
| Projects/Tasks/Task Transaction Controls - Delete a task transaction control | Projects/Tasks/Task Transaction Controls Delete a task transaction control  |
| Rate Schedules - Delete a rate schedule | Rate Schedules Delete a rate schedule  |
| Requirements - Delete a requirement | Requirements Delete a requirement  |
| Requirements/BacklogItems - Delete a backlog item | Requirements/BacklogItems Delete a backlog item  |
| Requirements/BacklogItems/AcceptanceCriterions - Delete an acceptance criteria | Requirements/BacklogItems/AcceptanceCriterions Delete an acceptance criteria  |
| Requirements/ChildRequirements - Delete a child requirement | Requirements/ChildRequirements Delete a child requirement  |
| Requirements/ChildRequirements/BacklogItems - Delete a backlog item | Requirements/ChildRequirements/BacklogItems Delete a backlog item  |
| Requirements/ChildRequirements/BacklogItems/AcceptanceCriterions - Delete an acceptance criteria | Requirements/ChildRequirements/BacklogItems/AcceptanceCriterions Delete an acceptance criteria  |
| Requirements/ChildRequirements/ChildRequirements - Delete a child requirement | Requirements/ChildRequirements/ChildRequirements Delete a child requirement  |
| Sponsors/Sponsor Reference Types - Delete a sponsor reference type | Sponsors/Sponsor Reference Types Delete a sponsor reference type  |
| Unprocessed Project Costs - Delete an unprocessed project cost | Unprocessed Project Costs Delete an unprocessed project cost  |
| Work Plan Templates - Delete a work plan template | Work Plan Templates Delete a work plan template  |
| Work Plan Templates/Task Dependencies - Delete a task dependency | Work Plan Templates/Task Dependencies Delete a task dependency  |
| Work Plan Templates/Task Expense Resource Assignments - Delete an expense resource assignment for a task | Work Plan Templates/Task Expense Resource Assignments Delete an expense resource assignment for a task  |
| Work Plan Templates/Task Labor Resource Assignments - Delete a labor resource assignment for a task | Work Plan Templates/Task Labor Resource Assignments Delete a labor resource assignment for a task  |
| Work Plan Templates/Tasks - Delete a task | Work Plan Templates/Tasks Delete a task  |
| Work Plan Templates/Tasks/Gate Approvers - Delete an approver of a gate | Work Plan Templates/Tasks/Gate Approvers Delete an approver of a gate  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Award Budget Periods - Get an award budget period | Award Budget Periods Get an award budget period  |
| Award Budget Summary - Get an award budget version | Award Budget Summary Get an award budget version  |
| Award Budget Summary/Resources - Get summary amounts for a resource for an award bu... | Award Budget Summary/Resources Get summary amounts for a resource for an award budget version  |
| Award Budget Summary/Resources/Budget Lines - Get a budget line for a resource | Award Budget Summary/Resources/Budget Lines Get a budget line for a resource  |
| Award Budget Summary/Resources/Budget Lines/Planning Amount Details - Get planning amount details for a period for a bud... | Award Budget Summary/Resources/Budget Lines/Planning Amount Details Get planning amount details for a period for a budget line  |
| Award Budget Summary/Tasks - Get summary amounts for a task for an award budget... | Award Budget Summary/Tasks Get summary amounts for a task for an award budget version  |
| Award Budget Summary/Tasks/Budget Lines - Get a budget line for a resource | Award Budget Summary/Tasks/Budget Lines Get a budget line for a resource  |
| Award Budget Summary/Tasks/Budget Lines/Planning Amount Details - Get planning amount details for a period for a bud... | Award Budget Summary/Tasks/Budget Lines/Planning Amount Details Get planning amount details for a period for a budget line  |
| Award Budget Summary/Version Errors - Get an error for an award budget version | Award Budget Summary/Version Errors Get an error for an award budget version  |
| Award Budgets - Get an award budget version | Award Budgets Get an award budget version  |
| Award Budgets/Planning Options - Get a planning option | Award Budgets/Planning Options Get a planning option  |
| Award Budgets/Planning Options/Planning Options Descriptive Flexfields - Get all descriptive flexfields for planning option... | Award Budgets/Planning Options/Planning Options Descriptive Flexfields Get all descriptive flexfields for planning options  |
| Award Budgets/Planning Resources - Get a planning resource for an award budget versio... | Award Budgets/Planning Resources Get a planning resource for an award budget version  |
| Award Budgets/Planning Resources/Planning Amounts - Get a plan line for a planning resource | Award Budgets/Planning Resources/Planning Amounts Get a plan line for a planning resource  |
| Award Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields - Get a descriptive flexfield for summary planning a... | Award Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields Get a descriptive flexfield for summary planning amounts  |
| Award Budgets/Planning Resources/Planning Amounts/Planning Amount Details - Get planning amount details for a period for a pla... | Award Budgets/Planning Resources/Planning Amounts/Planning Amount Details Get planning amount details for a period for a plan line  |
| Award Budgets/Version Errors - Get an error for an award budget version | Award Budgets/Version Errors Get an error for an award budget version  |
| Award Funding Sources - Get an award funding source | Award Funding Sources Get an award funding source  |
| Award Projects - Get an award project | Award Projects Get an award project  |
| Awards - Get an award | Awards Get an award  |
| Awards/Award Attachments - Get an award attachment | Awards/Award Attachments Get an award attachment  |
| Awards/Award Budget Periods - Get an award budget period | Awards/Award Budget Periods Get an award budget period  |
| Awards/Award Certifications - Get an award certification | Awards/Award Certifications Get an award certification  |
| Awards/Award CFDAs - Get an award CFDA | Awards/Award CFDAs Get an award CFDA  |
| Awards/Award Department Credits - Get an award department credit | Awards/Award Department Credits Get an award department credit  |
| Awards/Award Descriptive Flexfields - Get an award descriptive flexfield | Awards/Award Descriptive Flexfields Get an award descriptive flexfield  |
| Awards/Award Errors - Get an award error | Awards/Award Errors Get an award error  |
| Awards/Award Funding Sources - Get an award funding source | Awards/Award Funding Sources Get an award funding source  |
| Awards/Award Fundings - Get award funding | Awards/Award Fundings Get award funding  |
| Awards/Award Fundings/Award Project Fundings - Get award project funding | Awards/Award Fundings/Award Project Fundings Get award project funding  |
| Awards/Award Keywords - Get an award keyword | Awards/Award Keywords Get an award keyword  |
| Awards/Award Personnel - Get an award personnel | Awards/Award Personnel Get an award personnel  |
| Awards/Award Personnel/Award Personnel Descriptive Flexfields - Get an award personnel descriptive flexfield | Awards/Award Personnel/Award Personnel Descriptive Flexfields Get an award personnel descriptive flexfield  |
| Awards/Award Projects - Get an award project | Awards/Award Projects Get an award project  |
| Awards/Award Projects/Award Project Attachments - Get an award project attachment | Awards/Award Projects/Award Project Attachments Get an award project attachment  |
| Awards/Award Projects/Award Project Certifications - Get an award project certification | Awards/Award Projects/Award Project Certifications Get an award project certification  |
| Awards/Award Projects/Award Project Descriptive Flexfields - Get an award project descriptive flexfield | Awards/Award Projects/Award Project Descriptive Flexfields Get an award project descriptive flexfield  |
| Awards/Award Projects/Award Project Funding Sources - Get an award project funding source | Awards/Award Projects/Award Project Funding Sources Get an award project funding source  |
| Awards/Award Projects/Award Project Keywords - Get an award project keyword | Awards/Award Projects/Award Project Keywords Get an award project keyword  |
| Awards/Award Projects/Award Project Organization Credits - Get an award project department credit | Awards/Award Projects/Award Project Organization Credits Get an award project department credit  |
| Awards/Award Projects/Award Project Override Burden Schedules - Get the specific burden schedule override for the ... | Awards/Award Projects/Award Project Override Burden Schedules Get the specific burden schedule override for the award project.  |
| Awards/Award Projects/Award Project Override Burden Schedules/Versions - Get a specific version of the burden schedule. | Awards/Award Projects/Award Project Override Burden Schedules/Versions Get a specific version of the burden schedule.  |
| Awards/Award Projects/Award Project Override Burden Schedules/Versions/Multipliers - Get a specific multiplier of the burden schedule v... | Awards/Award Projects/Award Project Override Burden Schedules/Versions/Multipliers Get a specific multiplier of the burden schedule version.  |
| Awards/Award Projects/Award Project Personnel - Get an award project personnel | Awards/Award Projects/Award Project Personnel Get an award project personnel  |
| Awards/Award Projects/Award Project Personnel/Award Project Personnel Descriptive Flexfields - Get an award project personnel descriptive flexfie... | Awards/Award Projects/Award Project Personnel/Award Project Personnel Descriptive Flexfields Get an award project personnel descriptive flexfield  |
| Awards/Award Projects/Award Project Reference Types - Get an award project reference | Awards/Award Projects/Award Project Reference Types Get an award project reference  |
| Awards/Award Projects/Award Project Task Burden Schedules - Get an award project task burden schedule | Awards/Award Projects/Award Project Task Burden Schedules Get an award project task burden schedule  |
| Awards/Award References - Get an award reference | Awards/Award References Get an award reference  |
| Awards/Award Terms - Get an award term | Awards/Award Terms Get an award term  |
| Change Orders - Get a change order | Change Orders Get a change order  |
| Change Orders/Change Impacts - Get an impact for change order | Change Orders/Change Impacts Get an impact for change order  |
| Change Orders/Change Participants - Get a participant for a change order | Change Orders/Change Participants Get a participant for a change order  |
| Deliverables - Get a deliverable | Deliverables Get a deliverable  |
| Deliverables/Attachment to a Deliverable - Get an attachment for a deliverable | Deliverables/Attachment to a Deliverable Get an attachment for a deliverable  |
| Deliverables/Deliverable and Project Task Associations - Get an association between a deliverable and a pro... | Deliverables/Deliverable and Project Task Associations Get an association between a deliverable and a project task  |
| Deliverables/Deliverable and Requirement Associations - Get an association between a deliverable and a req... | Deliverables/Deliverable and Requirement Associations Get an association between a deliverable and a requirement  |
| Enterprise Project and Task Codes - Get an enterprise project or task code | Enterprise Project and Task Codes Get an enterprise project or task code  |
| Enterprise Project and Task Codes/Accepted Values - Get an accepted value | Enterprise Project and Task Codes/Accepted Values Get an accepted value  |
| Expenditure Types - Get an expenditure type | Expenditure Types Get an expenditure type  |
| Financial Project Plans - Get a financial project plan version | Financial Project Plans Get a financial project plan version  |
| Financial Project Plans/Plan Version Errors - Get an error for a financial project plan | Financial Project Plans/Plan Version Errors Get an error for a financial project plan  |
| Financial Project Plans/Resource Assignments - Get a resource assignment for a financial project ... | Financial Project Plans/Resource Assignments Get a resource assignment for a financial project plan version  |
| Financial Project Plans/Resource Assignments/Planning Amounts - Get a planning amount for a resource assignment | Financial Project Plans/Resource Assignments/Planning Amounts Get a planning amount for a resource assignment  |
| Financial Project Plans/Resource Assignments/Planning Amounts/Plan Lines Descriptive Flexfields - Get a descriptive flexfield for summary planning a... | Financial Project Plans/Resource Assignments/Planning Amounts/Plan Lines Descriptive Flexfields Get a descriptive flexfield for summary planning amounts  |
| Financial Project Plans/Resource Assignments/Planning Amounts/Planning Amount Details - Get a periodic detail for a planning amount | Financial Project Plans/Resource Assignments/Planning Amounts/Planning Amount Details Get a periodic detail for a planning amount  |
| Grants Keywords - Get a grants keyword. | Grants Keywords Get a grants keyword.  |
| Grants Personnel - Get a grants personnel | Grants Personnel Get a grants personnel  |
| Grants Personnel/Grants Personnel  Descriptive Flexfields - Get a grants personnel descriptive flexfield | Grants Personnel/Grants Personnel  Descriptive Flexfields Get a grants personnel descriptive flexfield  |
| Grants Personnel/Grants Personnel Keywords - Get a keyword associated to a grants personnel | Grants Personnel/Grants Personnel Keywords Get a keyword associated to a grants personnel  |
| Labor Schedule Cost Distributions - Get labor schedule cost distributions. | Labor Schedule Cost Distributions Get labor schedule cost distributions.  |
| Labor Schedule Cost Distributions/Labor Schedule Cost Distribution Errors - Get labor schedule cost distribution errors | Labor Schedule Cost Distributions/Labor Schedule Cost Distribution Errors Get labor schedule cost distribution errors  |
| Labor Schedule Costs - Get a labor schedule cost. | Labor Schedule Costs Get a labor schedule cost.  |
| Labor Schedule Costs Total Errors - Get labor schedule costs total errors | Labor Schedule Costs Total Errors Get labor schedule costs total errors  |
| Labor Schedule Costs Total Errors/Labor Schedule Costs Total Errors by Period - Get labor schedule cost errors by period | Labor Schedule Costs Total Errors/Labor Schedule Costs Total Errors by Period Get labor schedule cost errors by period  |
| Labor Schedule Costs/Labor Schedule Cost Distributions - Get labor schedule cost distributions. | Labor Schedule Costs/Labor Schedule Cost Distributions Get labor schedule cost distributions.  |
| Labor Schedule Costs/Labor Schedule Cost Distributions/Labor Schedule Cost Distribution Errors - Get labor schedule cost distribution errors | Labor Schedule Costs/Labor Schedule Cost Distributions/Labor Schedule Cost Distribution Errors Get labor schedule cost distribution errors  |
| LOV for Award Project Funding Sources - Get an award-project funding source | LOV for Award Project Funding Sources Get an award-project funding source  |
| LOV for Award Templates - Get an award template | LOV for Award Templates Get an award template  |
| LOV for Awards by Projects - Get an award-project mapping | LOV for Awards by Projects Get an award-project mapping  |
| LOV for Burden Schedules - Get a burden schedule | LOV for Burden Schedules Get a burden schedule  |
| LOV for Deliverable and Work Item Types - Get a deliverable or work item type | LOV for Deliverable and Work Item Types Get a deliverable or work item type  |
| LOV for Expenditure Types - Get an expenditure type | LOV for Expenditure Types Get an expenditure type  |
| LOV for Expenditure Types/LOV for Expenditure Type Classes - Get an expenditure type class for an expenditure t... | LOV for Expenditure Types/LOV for Expenditure Type Classes Get an expenditure type class for an expenditure type  |
| LOV for Funding Sources - Get a funding source | LOV for Funding Sources Get a funding source  |
| LOV for Project Billing Event Types - Get a project billing event type | LOV for Project Billing Event Types Get a project billing event type  |
| LOV for Project Business Units - Get a project business unit | LOV for Project Business Units Get a project business unit  |
| LOV for Project Class Codes - Get a project class code | LOV for Project Class Codes Get a project class code  |
| LOV for Project Organizations - Get a project organization | LOV for Project Organizations Get a project organization  |
| LOV for Project Roles - Get a project role | LOV for Project Roles Get a project role  |
| LOV for Project Tasks - Get a project task | LOV for Project Tasks Get a project task  |
| LOV for Project Templates - Get a project template | LOV for Project Templates Get a project template  |
| LOV for Project Types - Get a project type | LOV for Project Types Get a project type  |
| LOV for Projects - Get a project | LOV for Projects Get a project  |
| LOV for Rate Schedules - Get a rate schedule | LOV for Rate Schedules Get a rate schedule  |
| LOV for Work Types - Get a work type | LOV for Work Types Get a work type  |
| Person Assignment Labor Schedules - Get a Person Assignment Labor Schedule header. | Person Assignment Labor Schedules Get a Person Assignment Labor Schedule header.  |
| Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions - Get a version within a Person Assignment Labor Sch... | Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions Get a version within a Person Assignment Labor Schedule header.  |
| Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions/Person Assignment Labor Schedule Version Rules - Get a distribution rule within a version | Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions/Person Assignment Labor Schedule Version Rules Get a distribution rule within a version  |
| Planning Resource Breakdown Structures for Projects - Get a planning resource breakdown structure assign... | Planning Resource Breakdown Structures for Projects Get a planning resource breakdown structure assigned to a project  |
| Planning Resource Breakdown Structures for Projects/Elements - Get a resource in a planning resource breakdown st... | Planning Resource Breakdown Structures for Projects/Elements Get a resource in a planning resource breakdown structure  |
| Planning Resource Breakdown Structures for Projects/Formats - Get a resource format supported by a planning reso... | Planning Resource Breakdown Structures for Projects/Formats Get a resource format supported by a planning resource breakdown structure  |
| Project Assets - Get a project asset | Project Assets Get a project asset  |
| Project Assets/Project Asset Assignments - Get an asset assignment for a project asset | Project Assets/Project Asset Assignments Get an asset assignment for a project asset  |
| Project Assets/Project Asset Descriptive Flexfields - Get additional information for a project asset | Project Assets/Project Asset Descriptive Flexfields Get additional information for a project asset  |
| Project Billing Events - Get a project billing event | Project Billing Events Get a project billing event  |
| Project Billing Events/Project Billing Event Descriptive Flexfields - Get a project billing event descriptive flexfield | Project Billing Events/Project Billing Event Descriptive Flexfields Get a project billing event descriptive flexfield  |
| Project Budget Summary - Get a project budget version | Project Budget Summary Get a project budget version  |
| Project Budget Summary/Resources - Get a resource summary amounts for a project budge... | Project Budget Summary/Resources Get a resource summary amounts for a project budget version  |
| Project Budget Summary/Resources/Budget Lines - Get a budget line | Project Budget Summary/Resources/Budget Lines Get a budget line  |
| Project Budget Summary/Resources/Budget Lines/Planning Amount Details - Get planning amount details for a period for a bud... | Project Budget Summary/Resources/Budget Lines/Planning Amount Details Get planning amount details for a period for a budget line  |
| Project Budget Summary/Tasks - Get a project task summary amounts for a project b... | Project Budget Summary/Tasks Get a project task summary amounts for a project budget version  |
| Project Budget Summary/Tasks/Budget Lines - Get a budget line | Project Budget Summary/Tasks/Budget Lines Get a budget line  |
| Project Budget Summary/Tasks/Budget Lines/Planning Amount Details - Get planning amount details for a period for a bud... | Project Budget Summary/Tasks/Budget Lines/Planning Amount Details Get planning amount details for a period for a budget line  |
| Project Budget Summary/Version Errors - Get an error for a budget version | Project Budget Summary/Version Errors Get an error for a budget version  |
| Project Budgets - Get a project budget version | Project Budgets Get a project budget version  |
| Project Budgets/Planning Options - Get a planning option | Project Budgets/Planning Options Get a planning option  |
| Project Budgets/Planning Options/Planning Options Descriptive Flexfields - Get all descriptive flexfields for planning option... | Project Budgets/Planning Options/Planning Options Descriptive Flexfields Get all descriptive flexfields for planning options  |
| Project Budgets/Planning Resources - Get a planning resource for a project budget versi... | Project Budgets/Planning Resources Get a planning resource for a project budget version  |
| Project Budgets/Planning Resources/Planning Amounts - Get a plan line for a planning resource | Project Budgets/Planning Resources/Planning Amounts Get a plan line for a planning resource  |
| Project Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields - Get a descriptive flexfield for summary planning a... | Project Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields Get a descriptive flexfield for summary planning amounts  |
| Project Budgets/Planning Resources/Planning Amounts/Planning Amount Details - Get planning amount details for a period for a bud... | Project Budgets/Planning Resources/Planning Amounts/Planning Amount Details Get planning amount details for a period for a budget line  |
| Project Budgets/Version Errors - Get an error for a budget version | Project Budgets/Version Errors Get an error for a budget version  |
| Project Commitments - Get a project commitment | Project Commitments Get a project commitment  |
| Project Contract Invoices - Get a project contract invoice | Project Contract Invoices Get a project contract invoice  |
| Project Contract Invoices/Invoice Header Descriptive Flexfields - Get a project contract invoice descriptive flexfie... | Project Contract Invoices/Invoice Header Descriptive Flexfields Get a project contract invoice descriptive flexfield  |
| Project Contract Invoices/Invoice Lines - Get an invoice line | Project Contract Invoices/Invoice Lines Get an invoice line  |
| Project Contract Invoices/Invoice Lines/Invoice Line Descriptive Flexfields - Get an invoice line descriptive flexfield | Project Contract Invoices/Invoice Lines/Invoice Line Descriptive Flexfields Get an invoice line descriptive flexfield  |
| Project Contract Invoices/Invoice Lines/Invoice Line Distributions - Get an invoice line distribution | Project Contract Invoices/Invoice Lines/Invoice Line Distributions Get an invoice line distribution  |
| Project Contract Revenue - Get a project contract revenue | Project Contract Revenue Get a project contract revenue  |
| Project Costs - Get a project cost | Project Costs Get a project cost  |
| Project Costs/Adjustments - Get an adjustment for a project cost | Project Costs/Adjustments Get an adjustment for a project cost  |
| Project Costs/Project Costs Descriptive Flexfields - Get a descriptive flexfield for a project cost | Project Costs/Project Costs Descriptive Flexfields Get a descriptive flexfield for a project cost  |
| Project Costs/Project Standard Cost Collection Flexfields - Get standard cost collection information for a pro... | Project Costs/Project Standard Cost Collection Flexfields Get standard cost collection information for a project cost  |
| Project Enterprise Expense Resources - Get a project enterprise expense resource | Project Enterprise Expense Resources Get a project enterprise expense resource  |
| Project Enterprise Labor Resources - Get a project enterprise labor resource | Project Enterprise Labor Resources Get a project enterprise labor resource  |
| Project Enterprise Labor Resources/Project Enterprise Resource Image - Get an image of the project enterprise  resource | Project Enterprise Labor Resources/Project Enterprise Resource Image Get an image of the project enterprise  resource  |
| Project Enterprise Resources - Get a project enterprise resource | Project Enterprise Resources Get a project enterprise resource  |
| Project Events - Get a project calendar event | Project Events Get a project calendar event  |
| Project Expenditure Batches - Get a project expenditure batch | Project Expenditure Batches Get a project expenditure batch  |
| Project Expenditure Items - Get an expenditure item | Project Expenditure Items Get an expenditure item  |
| Project Expenditure Items/Project Expenditure Items Descriptive Flexfields - Get a descriptive flexfield for a project expendit... | Project Expenditure Items/Project Expenditure Items Descriptive Flexfields Get a descriptive flexfield for a project expenditure item  |
| Project Expense Resources - Get a project expense resource | Project Expense Resources Get a project expense resource  |
| Project Financial Tasks - Get a financial task | Project Financial Tasks Get a financial task  |
| Project Forecasts - Get a project forecast version | Project Forecasts Get a project forecast version  |
| Project Forecasts/Errors - Get an error for a project forecast version | Project Forecasts/Errors Get an error for a project forecast version  |
| Project Forecasts/Planning Options - Get a planning option | Project Forecasts/Planning Options Get a planning option  |
| Project Forecasts/Planning Options/Planning Options Descriptive Flexfields - Get a descriptive flexfield for planning options | Project Forecasts/Planning Options/Planning Options Descriptive Flexfields Get a descriptive flexfield for planning options  |
| Project Forecasts/Planning Resources - Get a planning resource for a project forecast ver... | Project Forecasts/Planning Resources Get a planning resource for a project forecast version  |
| Project Forecasts/Planning Resources/Planning Amounts - Get a summary planning amount for a planning resou... | Project Forecasts/Planning Resources/Planning Amounts Get a summary planning amount for a planning resource  |
| Project Forecasts/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields - Get a descriptive flexfield for summary planning a... | Project Forecasts/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields Get a descriptive flexfield for summary planning amounts  |
| Project Forecasts/Planning Resources/Planning Amounts/Planning Amount Details - Get details for a period for a planning amount | Project Forecasts/Planning Resources/Planning Amounts/Planning Amount Details Get details for a period for a planning amount  |
| Project Insights - Get insights for a project | Project Insights Get insights for a project  |
| Project Insights/Assignments - Get an assignment | Project Insights/Assignments Get an assignment  |
| Project Insights/Missing Time Cards - Get a missing time card | Project Insights/Missing Time Cards Get a missing time card  |
| Project Insights/Pending Invoices - Get a pending invoice | Project Insights/Pending Invoices Get a pending invoice  |
| Project Insights/Task Exceptions - Get a task exception | Project Insights/Task Exceptions Get a task exception  |
| Project Insights/Team Allocations - Get allocations for a resource | Project Insights/Team Allocations Get allocations for a resource  |
| Project Issues - Get an issue | Project Issues Get an issue  |
| Project Issues/Project Issue Action Items - Get an action item | Project Issues/Project Issue Action Items Get an action item  |
| Project Labor Resources - Get a project labor resource | Project Labor Resources Get a project labor resource  |
| Project Numbering Configurations - Get a project numbering configuration | Project Numbering Configurations Get a project numbering configuration  |
| Project Numbering Configurations/Project Numbering Configuration Details - Get a project numbering configuration detail | Project Numbering Configurations/Project Numbering Configuration Details Get a project numbering configuration detail  |
| Project Performance - Get performance data for a project | Project Performance Get performance data for a project  |
| Project Performance/Periodic Project Performance - Get periodic performance data for a project | Project Performance/Periodic Project Performance Get periodic performance data for a project  |
| Project Plan Details - Get a project that I can view | Project Plan Details Get a project that I can view  |
| Project Plan Details/Project Calendars - Get a single shift project calendar | Project Plan Details/Project Calendars Get a single shift project calendar  |
| Project Plan Details/Project Calendars/Calendar Exceptions - Get a calendar exception | Project Plan Details/Project Calendars/Calendar Exceptions Get a calendar exception  |
| Project Plan Details/Task Deliverables - Get a deliverable for a task | Project Plan Details/Task Deliverables Get a deliverable for a task  |
| Project Plan Details/Task Dependencies - Get one dependency between two tasks of a project | Project Plan Details/Task Dependencies Get one dependency between two tasks of a project  |
| Project Plan Details/Task Expense Resource Assignments - Get an expense resource assignment for a task | Project Plan Details/Task Expense Resource Assignments Get an expense resource assignment for a task  |
| Project Plan Details/Task Labor Resource Assignments - Get a labor resource assignment for a task | Project Plan Details/Task Labor Resource Assignments Get a labor resource assignment for a task  |
| Project Plan Details/Task Work Items - Get a work item for a task | Project Plan Details/Task Work Items Get a work item for a task  |
| Project Plan Details/Tasks - Get a task of a project | Project Plan Details/Tasks Get a task of a project  |
| Project Plan Details/Tasks/Gate Approvers - Get an approver of a gate in a project | Project Plan Details/Tasks/Gate Approvers Get an approver of a gate in a project  |
| Project Plan Resource Requests - Get a project resource request | Project Plan Resource Requests Get a project resource request  |
| Project Plan Resource Requests/Project Plan Resource Request Descriptive Flexfields - Get a descriptive flexfield associated to the proj... | Project Plan Resource Requests/Project Plan Resource Request Descriptive Flexfields Get a descriptive flexfield associated to the project resource request  |
| Project Plan Resource Requests/Project Plan Resource Request Lines - Get a  request line associated to the project reso... | Project Plan Resource Requests/Project Plan Resource Request Lines Get a  request line associated to the project resource request  |
| Project Plan Resource Requests/Project Plan Resource Request Qualifications - Get a qualification associated to the project reso... | Project Plan Resource Requests/Project Plan Resource Request Qualifications Get a qualification associated to the project resource request  |
| Project Plans - Get a project that I manage | Project Plans Get a project that I manage  |
| Project Plans/Baselines - Get a baseline | Project Plans/Baselines Get a baseline  |
| Project Plans/Project Calendars - Get a single shift project calendar | Project Plans/Project Calendars Get a single shift project calendar  |
| Project Plans/Project Calendars/Calendar Exceptions - Get a calendar exception | Project Plans/Project Calendars/Calendar Exceptions Get a calendar exception  |
| Project Plans/Project Descriptive Flexfields - Get all the descriptive flexfields for a project | Project Plans/Project Descriptive Flexfields Get all the descriptive flexfields for a project  |
| Project Plans/Task Deliverables - Get a deliverable for a task | Project Plans/Task Deliverables Get a deliverable for a task  |
| Project Plans/Task Deliverables/Attachments - Get an attachment for a deliverable of the project... | Project Plans/Task Deliverables/Attachments Get an attachment for a deliverable of the project task  |
| Project Plans/Task Dependencies - Get one dependency between two tasks of a project | Project Plans/Task Dependencies Get one dependency between two tasks of a project  |
| Project Plans/Task Expense Resource Assignments - Get an expense resource assignment for a task | Project Plans/Task Expense Resource Assignments Get an expense resource assignment for a task  |
| Project Plans/Task Labor Resource Assignments - Get a labor resource assignment for a task | Project Plans/Task Labor Resource Assignments Get a labor resource assignment for a task  |
| Project Plans/Task Work Items - Get a work item for a task | Project Plans/Task Work Items Get a work item for a task  |
| Project Plans/Tasks - Get a task of a project | Project Plans/Tasks Get a task of a project  |
| Project Plans/Tasks/Gate Approvers - Get an approver of a gate in a project | Project Plans/Tasks/Gate Approvers Get an approver of a gate in a project  |
| Project Plans/Tasks/Tasks Descriptive Flexfields - Get all the descriptive flexfields for a task of a... | Project Plans/Tasks/Tasks Descriptive Flexfields Get all the descriptive flexfields for a task of a project  |
| Project Process Configurators - Get a project process configurator | Project Process Configurators Get a project process configurator  |
| Project Process Configurators/Source Assignments - Get the source for a configurator | Project Process Configurators/Source Assignments Get the source for a configurator  |
| Project Progress - Get the progress of a draft project | Project Progress Get the progress of a draft project  |
| Project Progress/Task Progress - Get the progress of a project task | Project Progress/Task Progress Get the progress of a project task  |
| Project Resource Actual Hours - Get a project resource actual hour record. | Project Resource Actual Hours Get a project resource actual hour record.  |
| Project Resource Assignments - Get a project resource assignment | Project Resource Assignments Get a project resource assignment  |
| Project Resource Pools - Get a project resource pool. | Project Resource Pools Get a project resource pool.  |
| Project Resource Pools/Project Resource Pool Managers - Get a project resource pool manager associated to ... | Project Resource Pools/Project Resource Pool Managers Get a project resource pool manager associated to a project resource pool.  |
| Project Resource Pools/Project Resource Pool Members - Get a project resource pool member assigned to a p... | Project Resource Pools/Project Resource Pool Members Get a project resource pool member assigned to a project resource pool.  |
| Project Resource Request Matches - Not Applicable | Project Resource Request Matches Not Applicable  |
| Project Resource Requests - Get a project resource request | Project Resource Requests Get a project resource request  |
| Project Resource Requests/Project Resource Request Descriptive Flexfields - Get a descriptive flexfield associated to the proj... | Project Resource Requests/Project Resource Request Descriptive Flexfields Get a descriptive flexfield associated to the project resource request  |
| Project Resource Requests/Project Resource Request Lines - Get a  request line associated to the project reso... | Project Resource Requests/Project Resource Request Lines Get a  request line associated to the project resource request  |
| Project Resource Requests/Project Resource Request Qualifications - Get a qualification associated to the project reso... | Project Resource Requests/Project Resource Request Qualifications Get a qualification associated to the project resource request  |
| Project Templates - Get a project template | Project Templates Get a project template  |
| Project Templates/Project Classifications - Get a project classification | Project Templates/Project Classifications Get a project classification  |
| Project Templates/Project Customers - Get a project customer | Project Templates/Project Customers Get a project customer  |
| Project Templates/Project Team Members - Get a project team member | Project Templates/Project Team Members Get a project team member  |
| Project Templates/Project Transaction Controls - Get a project transaction control | Project Templates/Project Transaction Controls Get a project transaction control  |
| Project Templates/Quick Entries - Get a quick entry | Project Templates/Quick Entries Get a quick entry  |
| Project Templates/Setup Options - Get a project template setup option | Project Templates/Setup Options Get a project template setup option  |
| Project Templates/Tasks - Get a project task | Project Templates/Tasks Get a project task  |
| Project Templates/Tasks/Task Transaction Controls - Get a task transaction control | Project Templates/Tasks/Task Transaction Controls Get a task transaction control  |
| Projects - Get a project | Projects Get a project  |
| Projects Users - Get a projects user | Projects Users Get a projects user  |
| Projects Users/Chargeable Projects - Get a project for which the projects user can char... | Projects Users/Chargeable Projects Get a project for which the projects user can charge expenditures  |
| Projects Users/Chargeable Tasks - Get a task for which the projects user can charge ... | Projects Users/Chargeable Tasks Get a task for which the projects user can charge expenditures  |
| Projects Users/Followed Project Tasks - Get project tasks followed by a user | Projects Users/Followed Project Tasks Get project tasks followed by a user  |
| Projects Users/Followed To Do Tasks - Get to do tasks followed by a user | Projects Users/Followed To Do Tasks Get to do tasks followed by a user  |
| Projects Users/Project Tasks - Get a project task assigned to the user | Projects Users/Project Tasks Get a project task assigned to the user  |
| Projects Users/Project Tasks/Labor Resource Assignments - Get a labor resource assignment of a project task | Projects Users/Project Tasks/Labor Resource Assignments Get a labor resource assignment of a project task  |
| Projects Users/Project Tasks/Project Task Followers - Get a follower of a project task | Projects Users/Project Tasks/Project Task Followers Get a follower of a project task  |
| Projects Users/To Do Tasks - Get a to do task assigned to a user | Projects Users/To Do Tasks Get a to do task assigned to a user  |
| Projects Users/To Do Tasks/To Do Task Followers - Get a task follower of a to do project task | Projects Users/To Do Tasks/To Do Task Followers Get a task follower of a to do project task  |
| Projects/Project Classifications - Get a project classification | Projects/Project Classifications Get a project classification  |
| Projects/Project Customers - Get a project customer | Projects/Project Customers Get a project customer  |
| Projects/Project Descriptive Flexfields - Get a project descriptive flexfield | Projects/Project Descriptive Flexfields Get a project descriptive flexfield  |
| Projects/Project Opportunities - Get a project opportunity | Projects/Project Opportunities Get a project opportunity  |
| Projects/Project Team Members - Get a project team member | Projects/Project Team Members Get a project team member  |
| Projects/Project Transaction Controls - Get a project transaction control | Projects/Project Transaction Controls Get a project transaction control  |
| Projects/Task Dependencies - Get a task dependency | Projects/Task Dependencies Get a task dependency  |
| Projects/Tasks - Get a project task | Projects/Tasks Get a project task  |
| Projects/Tasks/Expense Resource Assignments - Get an expense resource assignment for a project t... | Projects/Tasks/Expense Resource Assignments Get an expense resource assignment for a project task  |
| Projects/Tasks/Labor Resource Assignments - Get a labor resource assignment for a project task | Projects/Tasks/Labor Resource Assignments Get a labor resource assignment for a project task  |
| Projects/Tasks/Task Transaction Controls - Get a task transaction control | Projects/Tasks/Task Transaction Controls Get a task transaction control  |
| Projects/Tasks/Tasks Descriptive Flexfields - Get a descriptive flexfield for a project task | Projects/Tasks/Tasks Descriptive Flexfields Get a descriptive flexfield for a project task  |
| Rate Schedules - Get a rate schedule | Rate Schedules Get a rate schedule  |
| Rate Schedules/Rate Schedule Descriptive Flexfields - Get a rate schedule descriptive flexfield | Rate Schedules/Rate Schedule Descriptive Flexfields Get a rate schedule descriptive flexfield  |
| Requirements - Get a requirement | Requirements Get a requirement  |
| Requirements/BacklogItems - Get a backlog item | Requirements/BacklogItems Get a backlog item  |
| Requirements/BacklogItems/AcceptanceCriterions - Get an acceptance criteria | Requirements/BacklogItems/AcceptanceCriterions Get an acceptance criteria  |
| Requirements/ChildRequirements - Get a child requirement | Requirements/ChildRequirements Get a child requirement  |
| Requirements/ChildRequirements/BacklogItems - Get a backlog item | Requirements/ChildRequirements/BacklogItems Get a backlog item  |
| Requirements/ChildRequirements/BacklogItems/AcceptanceCriterions - Get an acceptance criteria | Requirements/ChildRequirements/BacklogItems/AcceptanceCriterions Get an acceptance criteria  |
| Requirements/ChildRequirements/ChildRequirements - Get a child requirement | Requirements/ChildRequirements/ChildRequirements Get a child requirement  |
| Resource Events - Get a resource calendar event | Resource Events Get a resource calendar event  |
| Resource Performance - Get performance data for a resource | Resource Performance Get performance data for a resource  |
| Sponsors - Get a sponsor | Sponsors Get a sponsor  |
| Sponsors/Sponsor Reference Types - Get a sponsor reference type | Sponsors/Sponsor Reference Types Get a sponsor reference type  |
| Sprints - Get a sprint | Sprints Get a sprint  |
| Task Performance - Get performance data for a task | Task Performance Get performance data for a task  |
| Unprocessed Project Costs - Get an unprocessed project cost | Unprocessed Project Costs Get an unprocessed project cost  |
| Unprocessed Project Costs/Errors - Get an error for an unprocessed project cost | Unprocessed Project Costs/Errors Get an error for an unprocessed project cost  |
| Unprocessed Project Costs/Project Standard Cost Collection Flexfields - Get standard cost collection information for an un... | Unprocessed Project Costs/Project Standard Cost Collection Flexfields Get standard cost collection information for an unprocessed project cost  |
| Unprocessed Project Costs/Unprocessed Project Cost Descriptive Flexfields - Get additional information for an unprocessed proj... | Unprocessed Project Costs/Unprocessed Project Cost Descriptive Flexfields Get additional information for an unprocessed project cost  |
| Work Plan Templates - Get a work plan template | Work Plan Templates Get a work plan template  |
| Work Plan Templates/Task Deliverables - Get a deliverable for a task | Work Plan Templates/Task Deliverables Get a deliverable for a task  |
| Work Plan Templates/Task Dependencies - Get a dependency between two tasks in a work plan ... | Work Plan Templates/Task Dependencies Get a dependency between two tasks in a work plan template  |
| Work Plan Templates/Task Expense Resource Assignments - Get an expense resource assignment for a task | Work Plan Templates/Task Expense Resource Assignments Get an expense resource assignment for a task  |
| Work Plan Templates/Task Labor Resource Assignments - Get a labor resource assignment for a task | Work Plan Templates/Task Labor Resource Assignments Get a labor resource assignment for a task  |
| Work Plan Templates/Tasks - Get a task | Work Plan Templates/Tasks Get a task  |
| Work Plan Templates/Tasks/Gate Approvers - Get an approver of a gate | Work Plan Templates/Tasks/Gate Approvers Get an approver of a gate  |
| Work Plan Templates/Tasks/Task Descriptive Flexfields - Get all the descriptive flexfields for a task | Work Plan Templates/Tasks/Task Descriptive Flexfields Get all the descriptive flexfields for a task  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Award Budget Periods - Get all award budget periods | Award Budget Periods Get all award budget periods  |
| Award Budget Summary - Get all award budget versions | Award Budget Summary Get all award budget versions  |
| Award Budget Summary/Resources - Get summary amounts for all resources for an award... | Award Budget Summary/Resources Get summary amounts for all resources for an award budget version  |
| Award Budget Summary/Resources/Budget Lines - Get all budget lines for a resource | Award Budget Summary/Resources/Budget Lines Get all budget lines for a resource  |
| Award Budget Summary/Resources/Budget Lines/Planning Amount Details - Get planning amount details for all periods for a ... | Award Budget Summary/Resources/Budget Lines/Planning Amount Details Get planning amount details for all periods for a budget line  |
| Award Budget Summary/Tasks - Get summary amounts for all tasks for an award bud... | Award Budget Summary/Tasks Get summary amounts for all tasks for an award budget version  |
| Award Budget Summary/Tasks/Budget Lines - Get all budget lines for a resource | Award Budget Summary/Tasks/Budget Lines Get all budget lines for a resource  |
| Award Budget Summary/Tasks/Budget Lines/Planning Amount Details - Get planning amount details for all periods for a ... | Award Budget Summary/Tasks/Budget Lines/Planning Amount Details Get planning amount details for all periods for a budget line  |
| Award Budget Summary/Version Errors - Get all errors for an award budget version | Award Budget Summary/Version Errors Get all errors for an award budget version  |
| Award Budgets - Get all award budget versions | Award Budgets Get all award budget versions  |
| Award Budgets/Planning Options - Get all planning options | Award Budgets/Planning Options Get all planning options  |
| Award Budgets/Planning Options/Planning Options Descriptive Flexfields - Get all descriptive flexfields for planning option... | Award Budgets/Planning Options/Planning Options Descriptive Flexfields Get all descriptive flexfields for planning options as a collection  |
| Award Budgets/Planning Resources - Get all planning resources for an award budget ver... | Award Budgets/Planning Resources Get all planning resources for an award budget version  |
| Award Budgets/Planning Resources/Planning Amounts - Get all plan lines for a planning resource | Award Budgets/Planning Resources/Planning Amounts Get all plan lines for a planning resource  |
| Award Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields - Get all descriptive flexfields for summary plannin... | Award Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields Get all descriptive flexfields for summary planning amounts  |
| Award Budgets/Planning Resources/Planning Amounts/Planning Amount Details - Get planning amount details for all periods for a ... | Award Budgets/Planning Resources/Planning Amounts/Planning Amount Details Get planning amount details for all periods for a plan line  |
| Award Budgets/Version Errors - Get all errors for an award budget version | Award Budgets/Version Errors Get all errors for an award budget version  |
| Award Funding Sources - Get all award funding sources | Award Funding Sources Get all award funding sources  |
| Award Projects - Get all award projects | Award Projects Get all award projects  |
| Awards - Get all awards | Awards Get all awards  |
| Awards/Award Attachments - Get all award attachments | Awards/Award Attachments Get all award attachments  |
| Awards/Award Budget Periods - Get all award budget periods | Awards/Award Budget Periods Get all award budget periods  |
| Awards/Award Certifications - Get all award certifications | Awards/Award Certifications Get all award certifications  |
| Awards/Award CFDAs - Get all award CFDAs | Awards/Award CFDAs Get all award CFDAs  |
| Awards/Award Department Credits - Get all award department credits | Awards/Award Department Credits Get all award department credits  |
| Awards/Award Descriptive Flexfields - Get all award descriptive flexfields | Awards/Award Descriptive Flexfields Get all award descriptive flexfields  |
| Awards/Award Errors - Get all award errors | Awards/Award Errors Get all award errors  |
| Awards/Award Funding Sources - Get all award funding sources | Awards/Award Funding Sources Get all award funding sources  |
| Awards/Award Fundings - Get all award funding details | Awards/Award Fundings Get all award funding details  |
| Awards/Award Fundings/Award Project Fundings - Get all award project funding details | Awards/Award Fundings/Award Project Fundings Get all award project funding details  |
| Awards/Award Keywords - Get all award keywords | Awards/Award Keywords Get all award keywords  |
| Awards/Award Personnel - Get all award personnels | Awards/Award Personnel Get all award personnels  |
| Awards/Award Personnel/Award Personnel Descriptive Flexfields - Get all award personnel descriptive flexfields | Awards/Award Personnel/Award Personnel Descriptive Flexfields Get all award personnel descriptive flexfields  |
| Awards/Award Projects - Get all award projects | Awards/Award Projects Get all award projects  |
| Awards/Award Projects/Award Project Attachments - Get all award project attachments | Awards/Award Projects/Award Project Attachments Get all award project attachments  |
| Awards/Award Projects/Award Project Certifications - Get all award project certifications | Awards/Award Projects/Award Project Certifications Get all award project certifications  |
| Awards/Award Projects/Award Project Descriptive Flexfields - Get all award project descriptive flexfields | Awards/Award Projects/Award Project Descriptive Flexfields Get all award project descriptive flexfields  |
| Awards/Award Projects/Award Project Funding Sources - Get all award project funding sources | Awards/Award Projects/Award Project Funding Sources Get all award project funding sources  |
| Awards/Award Projects/Award Project Keywords - Get all award project keywords | Awards/Award Projects/Award Project Keywords Get all award project keywords  |
| Awards/Award Projects/Award Project Organization Credits - Get all award project department credits | Awards/Award Projects/Award Project Organization Credits Get all award project department credits  |
| Awards/Award Projects/Award Project Override Burden Schedules - Get the burden schedule override for the award pro... | Awards/Award Projects/Award Project Override Burden Schedules Get the burden schedule override for the award project.  |
| Awards/Award Projects/Award Project Override Burden Schedules/Versions - Get all versions of the burden schedule. | Awards/Award Projects/Award Project Override Burden Schedules/Versions Get all versions of the burden schedule.  |
| Awards/Award Projects/Award Project Override Burden Schedules/Versions/Multipliers - Get all multipliers in the burden schedule version... | Awards/Award Projects/Award Project Override Burden Schedules/Versions/Multipliers Get all multipliers in the burden schedule version.  |
| Awards/Award Projects/Award Project Personnel - Get all award project personnel | Awards/Award Projects/Award Project Personnel Get all award project personnel  |
| Awards/Award Projects/Award Project Personnel/Award Project Personnel Descriptive Flexfields - Get all award project personnel descriptive flexfi... | Awards/Award Projects/Award Project Personnel/Award Project Personnel Descriptive Flexfields Get all award project personnel descriptive flexfields  |
| Awards/Award Projects/Award Project Reference Types - Get all award project references | Awards/Award Projects/Award Project Reference Types Get all award project references  |
| Awards/Award Projects/Award Project Task Burden Schedules - Get all award project task burden schedules | Awards/Award Projects/Award Project Task Burden Schedules Get all award project task burden schedules  |
| Awards/Award References - Get all award references | Awards/Award References Get all award references  |
| Awards/Award Terms - Get all award terms | Awards/Award Terms Get all award terms  |
| Change Orders - Get all change orders | Change Orders Get all change orders  |
| Change Orders/Change Impacts - Get all impacts for a change order | Change Orders/Change Impacts Get all impacts for a change order  |
| Change Orders/Change Participants - Get all participants for change order | Change Orders/Change Participants Get all participants for change order  |
| Deliverables - Get all deliverables | Deliverables Get all deliverables  |
| Deliverables/Attachment to a Deliverable - Get all attachments for a deliverable | Deliverables/Attachment to a Deliverable Get all attachments for a deliverable  |
| Deliverables/Deliverable and Project Task Associations - Get all associations between a deliverable and pro... | Deliverables/Deliverable and Project Task Associations Get all associations between a deliverable and project tasks  |
| Deliverables/Deliverable and Requirement Associations - Get all associations between a deliverable and req... | Deliverables/Deliverable and Requirement Associations Get all associations between a deliverable and requirements  |
| Enterprise Project and Task Codes - Get all enterprise project and task codes | Enterprise Project and Task Codes Get all enterprise project and task codes  |
| Enterprise Project and Task Codes/Accepted Values - Get all accepted values | Enterprise Project and Task Codes/Accepted Values Get all accepted values  |
| Expenditure Types - Get all expenditure types | Expenditure Types Get all expenditure types  |
| Financial Project Plans - Get all financial project plan versions | Financial Project Plans Get all financial project plan versions  |
| Financial Project Plans/Plan Version Errors - Get all errors for a financial project plan | Financial Project Plans/Plan Version Errors Get all errors for a financial project plan  |
| Financial Project Plans/Resource Assignments - Get all resource assignments for a financial proje... | Financial Project Plans/Resource Assignments Get all resource assignments for a financial project plan version  |
| Financial Project Plans/Resource Assignments/Planning Amounts - Get all planning amounts for a resource assignment | Financial Project Plans/Resource Assignments/Planning Amounts Get all planning amounts for a resource assignment  |
| Financial Project Plans/Resource Assignments/Planning Amounts/Plan Lines Descriptive Flexfields - Get all descriptive flexfields for summary plannin... | Financial Project Plans/Resource Assignments/Planning Amounts/Plan Lines Descriptive Flexfields Get all descriptive flexfields for summary planning amounts  |
| Financial Project Plans/Resource Assignments/Planning Amounts/Planning Amount Details - Get all periodic details for a planning amount | Financial Project Plans/Resource Assignments/Planning Amounts/Planning Amount Details Get all periodic details for a planning amount  |
| Grants Keywords - Get all grants keywords. | Grants Keywords Get all grants keywords.  |
| Grants Personnel - Get all grants personnel | Grants Personnel Get all grants personnel  |
| Grants Personnel/Grants Personnel  Descriptive Flexfields - Get all grants personnel descriptive flexfields | Grants Personnel/Grants Personnel  Descriptive Flexfields Get all grants personnel descriptive flexfields  |
| Grants Personnel/Grants Personnel Keywords - Get all keywords associated to a grants personnel | Grants Personnel/Grants Personnel Keywords Get all keywords associated to a grants personnel  |
| Labor Schedule Cost Distributions - Get many labor schedule cost distributions. | Labor Schedule Cost Distributions Get many labor schedule cost distributions.  |
| Labor Schedule Cost Distributions/Labor Schedule Cost Distribution Errors - Get many labor schedule cost distribution errors | Labor Schedule Cost Distributions/Labor Schedule Cost Distribution Errors Get many labor schedule cost distribution errors  |
| Labor Schedule Costs - Get many labor schedule costs. | Labor Schedule Costs Get many labor schedule costs.  |
| Labor Schedule Costs Total Errors - Get many labor schedule costs total errors | Labor Schedule Costs Total Errors Get many labor schedule costs total errors  |
| Labor Schedule Costs Total Errors/Labor Schedule Costs Total Errors by Period - Get many labor schedule cost errors by period | Labor Schedule Costs Total Errors/Labor Schedule Costs Total Errors by Period Get many labor schedule cost errors by period  |
| Labor Schedule Costs/Labor Schedule Cost Distributions - Get many labor schedule cost distributions. | Labor Schedule Costs/Labor Schedule Cost Distributions Get many labor schedule cost distributions.  |
| Labor Schedule Costs/Labor Schedule Cost Distributions/Labor Schedule Cost Distribution Errors - Get many labor schedule cost distribution errors | Labor Schedule Costs/Labor Schedule Cost Distributions/Labor Schedule Cost Distribution Errors Get many labor schedule cost distribution errors  |
| LOV for Award Project Funding Sources - Get all award-project funding sources | LOV for Award Project Funding Sources Get all award-project funding sources  |
| LOV for Award Templates - Get all award templates | LOV for Award Templates Get all award templates  |
| LOV for Awards by Projects - Get all award-project mappings | LOV for Awards by Projects Get all award-project mappings  |
| LOV for Burden Schedules - Get all burden schedules | LOV for Burden Schedules Get all burden schedules  |
| LOV for Deliverable and Work Item Types - Get all deliverable and work item types | LOV for Deliverable and Work Item Types Get all deliverable and work item types  |
| LOV for Expenditure Types - Get all expenditure types | LOV for Expenditure Types Get all expenditure types  |
| LOV for Expenditure Types/LOV for Expenditure Type Classes - Get all expenditure type classes for an expenditur... | LOV for Expenditure Types/LOV for Expenditure Type Classes Get all expenditure type classes for an expenditure type  |
| LOV for Funding Sources - Get all funding sources | LOV for Funding Sources Get all funding sources  |
| LOV for Project Billing Event Types - Get all project billing event types | LOV for Project Billing Event Types Get all project billing event types  |
| LOV for Project Business Units - Get all project business units | LOV for Project Business Units Get all project business units  |
| LOV for Project Class Codes - Get all project class codes | LOV for Project Class Codes Get all project class codes  |
| LOV for Project Organizations - Get all project organizations | LOV for Project Organizations Get all project organizations  |
| LOV for Project Roles - Get all project roles | LOV for Project Roles Get all project roles  |
| LOV for Project Tasks - Get all project tasks | LOV for Project Tasks Get all project tasks  |
| LOV for Project Templates - Get all project templates | LOV for Project Templates Get all project templates  |
| LOV for Project Types - Get all project types | LOV for Project Types Get all project types  |
| LOV for Projects - Get all projects | LOV for Projects Get all projects  |
| LOV for Rate Schedules - Get all rate schedules | LOV for Rate Schedules Get all rate schedules  |
| LOV for Work Types - Get all work types | LOV for Work Types Get all work types  |
| Person Assignment Labor Schedules - Get many Person Assignment Labor Schedule headers. | Person Assignment Labor Schedules Get many Person Assignment Labor Schedule headers.  |
| Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions - Get many versions within Person Assignment Labor S... | Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions Get many versions within Person Assignment Labor Schedule header.  |
| Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions/Person Assignment Labor Schedule Version Rules - Get many distribution rules within a version | Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions/Person Assignment Labor Schedule Version Rules Get many distribution rules within a version  |
| Planning Resource Breakdown Structures for Projects - Get all planning resource breakdown structures ass... | Planning Resource Breakdown Structures for Projects Get all planning resource breakdown structures assigned to projects  |
| Planning Resource Breakdown Structures for Projects/Elements - Get all resources in a planning resource breakdown... | Planning Resource Breakdown Structures for Projects/Elements Get all resources in a planning resource breakdown structure  |
| Planning Resource Breakdown Structures for Projects/Formats - Get all resource formats supported by a planning r... | Planning Resource Breakdown Structures for Projects/Formats Get all resource formats supported by a planning resource breakdown structure  |
| Project Assets - Get all project assets | Project Assets Get all project assets  |
| Project Assets/Project Asset Assignments - Get all asset assignments for a project asset | Project Assets/Project Asset Assignments Get all asset assignments for a project asset  |
| Project Assets/Project Asset Descriptive Flexfields - Get all additional information for project assets | Project Assets/Project Asset Descriptive Flexfields Get all additional information for project assets  |
| Project Billing Events - Get all project billing events | Project Billing Events Get all project billing events  |
| Project Billing Events/Project Billing Event Descriptive Flexfields - Get all project billing event descriptive flexfiel... | Project Billing Events/Project Billing Event Descriptive Flexfields Get all project billing event descriptive flexfields  |
| Project Budget Summary - Get all project budget versions | Project Budget Summary Get all project budget versions  |
| Project Budget Summary/Resources - Get all resource summary amounts for a project bud... | Project Budget Summary/Resources Get all resource summary amounts for a project budget version  |
| Project Budget Summary/Resources/Budget Lines - Get all budget lines | Project Budget Summary/Resources/Budget Lines Get all budget lines  |
| Project Budget Summary/Resources/Budget Lines/Planning Amount Details - Get planning amount details for all periods for a ... | Project Budget Summary/Resources/Budget Lines/Planning Amount Details Get planning amount details for all periods for a budget line  |
| Project Budget Summary/Tasks - Get all project tasks summary amounts for a projec... | Project Budget Summary/Tasks Get all project tasks summary amounts for a project budget version  |
| Project Budget Summary/Tasks/Budget Lines - Get all budget lines | Project Budget Summary/Tasks/Budget Lines Get all budget lines  |
| Project Budget Summary/Tasks/Budget Lines/Planning Amount Details - Get planning amount details for all periods for a ... | Project Budget Summary/Tasks/Budget Lines/Planning Amount Details Get planning amount details for all periods for a budget line  |
| Project Budget Summary/Version Errors - Get all errors for a budget version | Project Budget Summary/Version Errors Get all errors for a budget version  |
| Project Budgets - Get all project budget versions | Project Budgets Get all project budget versions  |
| Project Budgets/Planning Options - Get all planning options | Project Budgets/Planning Options Get all planning options  |
| Project Budgets/Planning Options/Planning Options Descriptive Flexfields - Get all descriptive flexfields for planning option... | Project Budgets/Planning Options/Planning Options Descriptive Flexfields Get all descriptive flexfields for planning options as a collection  |
| Project Budgets/Planning Resources - Get all planning resources for a project budget ve... | Project Budgets/Planning Resources Get all planning resources for a project budget version  |
| Project Budgets/Planning Resources/Planning Amounts - Get all plan lines for a planning resource | Project Budgets/Planning Resources/Planning Amounts Get all plan lines for a planning resource  |
| Project Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields - Get all descriptive flexfields for summary plannin... | Project Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields Get all descriptive flexfields for summary planning amounts  |
| Project Budgets/Planning Resources/Planning Amounts/Planning Amount Details - Get planning amount details for all periods for a ... | Project Budgets/Planning Resources/Planning Amounts/Planning Amount Details Get planning amount details for all periods for a budget line  |
| Project Budgets/Version Errors - Get all errors for a budget version | Project Budgets/Version Errors Get all errors for a budget version  |
| Project Commitments - Get all project commitments | Project Commitments Get all project commitments  |
| Project Contract Invoices - Get all project contract invoices | Project Contract Invoices Get all project contract invoices  |
| Project Contract Invoices/Invoice Header Descriptive Flexfields - Get all project contract invoice descriptive flexf... | Project Contract Invoices/Invoice Header Descriptive Flexfields Get all project contract invoice descriptive flexfields  |
| Project Contract Invoices/Invoice Lines - Get all invoice lines | Project Contract Invoices/Invoice Lines Get all invoice lines  |
| Project Contract Invoices/Invoice Lines/Invoice Line Descriptive Flexfields - Get all invoice line descriptive flexfields | Project Contract Invoices/Invoice Lines/Invoice Line Descriptive Flexfields Get all invoice line descriptive flexfields  |
| Project Contract Invoices/Invoice Lines/Invoice Line Distributions - Get all invoice line distributions | Project Contract Invoices/Invoice Lines/Invoice Line Distributions Get all invoice line distributions  |
| Project Contract Revenue - Get all project contract revenues | Project Contract Revenue Get all project contract revenues  |
| Project Costs - Get all project costs | Project Costs Get all project costs  |
| Project Costs/Adjustments - Get all adjustments for a project cost | Project Costs/Adjustments Get all adjustments for a project cost  |
| Project Costs/Project Costs Descriptive Flexfields - Get all descriptive flexfields for a project cost | Project Costs/Project Costs Descriptive Flexfields Get all descriptive flexfields for a project cost  |
| Project Costs/Project Standard Cost Collection Flexfields - Get all standard cost collection information for a... | Project Costs/Project Standard Cost Collection Flexfields Get all standard cost collection information for a project cost  |
| Project Enterprise Expense Resources - Get all project enterprise expense resources | Project Enterprise Expense Resources Get all project enterprise expense resources  |
| Project Enterprise Labor Resources - Get all project enterprise labor resources | Project Enterprise Labor Resources Get all project enterprise labor resources  |
| Project Enterprise Labor Resources/Project Enterprise Resource Image - Get all images of the project enterprise resource | Project Enterprise Labor Resources/Project Enterprise Resource Image Get all images of the project enterprise resource  |
| Project Enterprise Resources - Get all project enterprise resources | Project Enterprise Resources Get all project enterprise resources  |
| Project Events - Get all project calendar events | Project Events Get all project calendar events  |
| Project Expenditure Batches - Get all project expenditure batches | Project Expenditure Batches Get all project expenditure batches  |
| Project Expenditure Items - Get all expenditure items | Project Expenditure Items Get all expenditure items  |
| Project Expenditure Items/Project Expenditure Items Descriptive Flexfields - Get all descriptive flexfields for a project expen... | Project Expenditure Items/Project Expenditure Items Descriptive Flexfields Get all descriptive flexfields for a project expenditure item  |
| Project Expense Resources - Get all project expense resources | Project Expense Resources Get all project expense resources  |
| Project Financial Tasks - Get all financial tasks | Project Financial Tasks Get all financial tasks  |
| Project Forecasts - Get all project forecast versions | Project Forecasts Get all project forecast versions  |
| Project Forecasts/Errors - Get all errors for a project forecast version | Project Forecasts/Errors Get all errors for a project forecast version  |
| Project Forecasts/Planning Options - Get all planning options | Project Forecasts/Planning Options Get all planning options  |
| Project Forecasts/Planning Options/Planning Options Descriptive Flexfields - Get all descriptive flexfields for planning option... | Project Forecasts/Planning Options/Planning Options Descriptive Flexfields Get all descriptive flexfields for planning options  |
| Project Forecasts/Planning Resources - Get all planning resources for a project forecast ... | Project Forecasts/Planning Resources Get all planning resources for a project forecast version  |
| Project Forecasts/Planning Resources/Planning Amounts - Get all summary planning amounts for a planning re... | Project Forecasts/Planning Resources/Planning Amounts Get all summary planning amounts for a planning resource  |
| Project Forecasts/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields - Get all descriptive flexfields for summary plannin... | Project Forecasts/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields Get all descriptive flexfields for summary planning amounts  |
| Project Forecasts/Planning Resources/Planning Amounts/Planning Amount Details - Get details for all periods for a planning amount | Project Forecasts/Planning Resources/Planning Amounts/Planning Amount Details Get details for all periods for a planning amount  |
| Project Insights - Get insights for all projects | Project Insights Get insights for all projects  |
| Project Insights/Assignments - Get all assignments for a project | Project Insights/Assignments Get all assignments for a project  |
| Project Insights/Missing Time Cards - Get all missing time cards for a project | Project Insights/Missing Time Cards Get all missing time cards for a project  |
| Project Insights/Pending Invoices - Get all pending invoices for a project | Project Insights/Pending Invoices Get all pending invoices for a project  |
| Project Insights/Task Exceptions - Get all task exceptions for a project | Project Insights/Task Exceptions Get all task exceptions for a project  |
| Project Insights/Team Allocations - Get all allocations for a project | Project Insights/Team Allocations Get all allocations for a project  |
| Project Issues - Get all issues | Project Issues Get all issues  |
| Project Issues/Project Issue Action Items - Get all action items for a project issue | Project Issues/Project Issue Action Items Get all action items for a project issue  |
| Project Labor Resources - Get all project labor resources | Project Labor Resources Get all project labor resources  |
| Project Numbering Configurations - Get all project numbering configurations | Project Numbering Configurations Get all project numbering configurations  |
| Project Numbering Configurations/Project Numbering Configuration Details - Get all project numbering configuration details | Project Numbering Configurations/Project Numbering Configuration Details Get all project numbering configuration details  |
| Project Performance - Get performance data for all projects | Project Performance Get performance data for all projects  |
| Project Performance/Periodic Project Performance - Get periodic performance data for all projects | Project Performance/Periodic Project Performance Get periodic performance data for all projects  |
| Project Plan Details - Get all projects that I can view | Project Plan Details Get all projects that I can view  |
| Project Plan Details/Project Calendars - Get all single shift project calendars used to sch... | Project Plan Details/Project Calendars Get all single shift project calendars used to schedule a project  |
| Project Plan Details/Project Calendars/Calendar Exceptions - Get all calendar exceptions | Project Plan Details/Project Calendars/Calendar Exceptions Get all calendar exceptions  |
| Project Plan Details/Task Deliverables - Get all deliverables for all the tasks of a projec... | Project Plan Details/Task Deliverables Get all deliverables for all the tasks of a project  |
| Project Plan Details/Task Dependencies - Get all dependencies between the tasks of a projec... | Project Plan Details/Task Dependencies Get all dependencies between the tasks of a project  |
| Project Plan Details/Task Expense Resource Assignments - Get all expense resource assignments for all the t... | Project Plan Details/Task Expense Resource Assignments Get all expense resource assignments for all the tasks of a project  |
| Project Plan Details/Task Labor Resource Assignments - Get all labor resource assignments for all the tas... | Project Plan Details/Task Labor Resource Assignments Get all labor resource assignments for all the tasks of a project  |
| Project Plan Details/Task Work Items - Get all work items for all the tasks of a project | Project Plan Details/Task Work Items Get all work items for all the tasks of a project  |
| Project Plan Details/Tasks - Get all tasks of a project | Project Plan Details/Tasks Get all tasks of a project  |
| Project Plan Details/Tasks/Gate Approvers - Get all the approvers of a gate in a project | Project Plan Details/Tasks/Gate Approvers Get all the approvers of a gate in a project  |
| Project Plan Resource Requests - Get all project resource requests | Project Plan Resource Requests Get all project resource requests  |
| Project Plan Resource Requests/Project Plan Resource Request Descriptive Flexfields - Get all descriptive flexfields associated to the p... | Project Plan Resource Requests/Project Plan Resource Request Descriptive Flexfields Get all descriptive flexfields associated to the project resource request  |
| Project Plan Resource Requests/Project Plan Resource Request Lines - Get all request lines associated to the project re... | Project Plan Resource Requests/Project Plan Resource Request Lines Get all request lines associated to the project resource request  |
| Project Plan Resource Requests/Project Plan Resource Request Qualifications - Get all qualifications associated to the project r... | Project Plan Resource Requests/Project Plan Resource Request Qualifications Get all qualifications associated to the project resource request  |
| Project Plans - Get all projects that I manage | Project Plans Get all projects that I manage  |
| Project Plans/Baselines - Get all baselines | Project Plans/Baselines Get all baselines  |
| Project Plans/Project Calendars - Get all single shift project calendars used to sch... | Project Plans/Project Calendars Get all single shift project calendars used to schedule a project  |
| Project Plans/Project Calendars/Calendar Exceptions - Get all calendar exceptions | Project Plans/Project Calendars/Calendar Exceptions Get all calendar exceptions  |
| Project Plans/Project Descriptive Flexfields - Get all the descriptive flexfields for a project | Project Plans/Project Descriptive Flexfields Get all the descriptive flexfields for a project  |
| Project Plans/Task Deliverables - Get all deliverables for all the tasks of a projec... | Project Plans/Task Deliverables Get all deliverables for all the tasks of a project  |
| Project Plans/Task Deliverables/Attachments - Get all attachments for a deliverable of the proje... | Project Plans/Task Deliverables/Attachments Get all attachments for a deliverable of the project task  |
| Project Plans/Task Dependencies - Get all dependencies between the tasks of a projec... | Project Plans/Task Dependencies Get all dependencies between the tasks of a project  |
| Project Plans/Task Expense Resource Assignments - Get all expense resource assignments for all the t... | Project Plans/Task Expense Resource Assignments Get all expense resource assignments for all the tasks of a project  |
| Project Plans/Task Labor Resource Assignments - Get all labor resource assignments for all the tas... | Project Plans/Task Labor Resource Assignments Get all labor resource assignments for all the tasks of a project  |
| Project Plans/Task Work Items - Get all work items for all the tasks of a project | Project Plans/Task Work Items Get all work items for all the tasks of a project  |
| Project Plans/Tasks - Get all tasks of a project | Project Plans/Tasks Get all tasks of a project  |
| Project Plans/Tasks/Gate Approvers - Get all the approvers of a gate in a project | Project Plans/Tasks/Gate Approvers Get all the approvers of a gate in a project  |
| Project Plans/Tasks/Tasks Descriptive Flexfields - Get all the descriptive flexfields for all of the ... | Project Plans/Tasks/Tasks Descriptive Flexfields Get all the descriptive flexfields for all of the tasks of a project  |
| Project Process Configurators - Get all project process configurators | Project Process Configurators Get all project process configurators  |
| Project Process Configurators/Source Assignments - Get all sources for a configurator | Project Process Configurators/Source Assignments Get all sources for a configurator  |
| Project Progress - Get the progress of all projects | Project Progress Get the progress of all projects  |
| Project Progress/Task Progress - Get the progress of all tasks of a project | Project Progress/Task Progress Get the progress of all tasks of a project  |
| Project Resource Actual Hours - Get all project resource actual hour records. | Project Resource Actual Hours Get all project resource actual hour records.  |
| Project Resource Assignments - Get all project resource assignments | Project Resource Assignments Get all project resource assignments  |
| Project Resource Pools - Get all project resource pool. | Project Resource Pools Get all project resource pool.  |
| Project Resource Pools/Project Resource Pool Managers - Get all project resource pool managers associated ... | Project Resource Pools/Project Resource Pool Managers Get all project resource pool managers associated to a project resource pool.  |
| Project Resource Pools/Project Resource Pool Members - Get all project resource pool members assigned to ... | Project Resource Pools/Project Resource Pool Members Get all project resource pool members assigned to a project resource pool.  |
| Project Resource Request Matches - Get all resources that match the project resource ... | Project Resource Request Matches Get all resources that match the project resource request criteria  |
| Project Resource Requests - Get all project resource requests | Project Resource Requests Get all project resource requests  |
| Project Resource Requests/Project Resource Request Descriptive Flexfields - Get all descriptive flexfields associated to the p... | Project Resource Requests/Project Resource Request Descriptive Flexfields Get all descriptive flexfields associated to the project resource request  |
| Project Resource Requests/Project Resource Request Lines - Get all request lines associated to the project re... | Project Resource Requests/Project Resource Request Lines Get all request lines associated to the project resource request  |
| Project Resource Requests/Project Resource Request Qualifications - Get all qualifications associated to the project r... | Project Resource Requests/Project Resource Request Qualifications Get all qualifications associated to the project resource request  |
| Project Templates - Get all project templates | Project Templates Get all project templates  |
| Project Templates/Project Classifications - Get all classifications for a project | Project Templates/Project Classifications Get all classifications for a project  |
| Project Templates/Project Customers - Get all customers for a project | Project Templates/Project Customers Get all customers for a project  |
| Project Templates/Project Team Members - Get all team members for a project template | Project Templates/Project Team Members Get all team members for a project template  |
| Project Templates/Project Transaction Controls - Get all transaction controls for a project | Project Templates/Project Transaction Controls Get all transaction controls for a project  |
| Project Templates/Quick Entries - Get all quick entries for a project template | Project Templates/Quick Entries Get all quick entries for a project template  |
| Project Templates/Setup Options - Get all setup options for a project template | Project Templates/Setup Options Get all setup options for a project template  |
| Project Templates/Tasks - Get all tasks for a project | Project Templates/Tasks Get all tasks for a project  |
| Project Templates/Tasks/Task Transaction Controls - Get all transaction controls for a project task | Project Templates/Tasks/Task Transaction Controls Get all transaction controls for a project task  |
| Projects - Get all projects | Projects Get all projects  |
| Projects Users - Get signed in projects users | Projects Users Get signed in projects users  |
| Projects Users/Chargeable Projects - Get all the projects for which the projects user c... | Projects Users/Chargeable Projects Get all the projects for which the projects user can charge expenditures  |
| Projects Users/Chargeable Tasks - Get all the tasks for which the projects user can ... | Projects Users/Chargeable Tasks Get all the tasks for which the projects user can charge expenditures  |
| Projects Users/Followed Project Tasks - Get all followed project tasks | Projects Users/Followed Project Tasks Get all followed project tasks  |
| Projects Users/Followed To Do Tasks - Get all followed to do tasks | Projects Users/Followed To Do Tasks Get all followed to do tasks  |
| Projects Users/Project Tasks - Get all project tasks assigned to the user | Projects Users/Project Tasks Get all project tasks assigned to the user  |
| Projects Users/Project Tasks/Labor Resource Assignments - Get all labor resource assignments of a project ta... | Projects Users/Project Tasks/Labor Resource Assignments Get all labor resource assignments of a project task  |
| Projects Users/Project Tasks/Project Task Followers - Get all the followers of a project task | Projects Users/Project Tasks/Project Task Followers Get all the followers of a project task  |
| Projects Users/To Do Tasks - Get all to do tasks assigned to a user | Projects Users/To Do Tasks Get all to do tasks assigned to a user  |
| Projects Users/To Do Tasks/To Do Task Followers - Get all the task followers of a to do project task | Projects Users/To Do Tasks/To Do Task Followers Get all the task followers of a to do project task  |
| Projects/Project Classifications - Get all classifications for a project | Projects/Project Classifications Get all classifications for a project  |
| Projects/Project Customers - Get all customers for a project | Projects/Project Customers Get all customers for a project  |
| Projects/Project Descriptive Flexfields - Get all project descriptive flexfields | Projects/Project Descriptive Flexfields Get all project descriptive flexfields  |
| Projects/Project Opportunities - Get all opportunities for a project | Projects/Project Opportunities Get all opportunities for a project  |
| Projects/Project Team Members - Get all team members for a project | Projects/Project Team Members Get all team members for a project  |
| Projects/Project Transaction Controls - Get all transaction controls for a project | Projects/Project Transaction Controls Get all transaction controls for a project  |
| Projects/Task Dependencies - Get all task dependencies for a project | Projects/Task Dependencies Get all task dependencies for a project  |
| Projects/Tasks - Get all tasks for a project | Projects/Tasks Get all tasks for a project  |
| Projects/Tasks/Expense Resource Assignments - Get all expense resource assignments for a project... | Projects/Tasks/Expense Resource Assignments Get all expense resource assignments for a project task  |
| Projects/Tasks/Labor Resource Assignments - Get all labor resource assignments for a project t... | Projects/Tasks/Labor Resource Assignments Get all labor resource assignments for a project task  |
| Projects/Tasks/Task Transaction Controls - Get all transaction controls for a project task | Projects/Tasks/Task Transaction Controls Get all transaction controls for a project task  |
| Projects/Tasks/Tasks Descriptive Flexfields - Get all descriptive flexfields for a project task | Projects/Tasks/Tasks Descriptive Flexfields Get all descriptive flexfields for a project task  |
| Rate Schedules - Get all rate schedules | Rate Schedules Get all rate schedules  |
| Rate Schedules/Rate Schedule Descriptive Flexfields - Get all rate schedule descriptive flexfields | Rate Schedules/Rate Schedule Descriptive Flexfields Get all rate schedule descriptive flexfields  |
| Requirements - Get all requirements | Requirements Get all requirements  |
| Requirements/BacklogItems - Get all backlog items | Requirements/BacklogItems Get all backlog items  |
| Requirements/BacklogItems/AcceptanceCriterions - Get all acceptance criteria | Requirements/BacklogItems/AcceptanceCriterions Get all acceptance criteria  |
| Requirements/ChildRequirements - Get all child requirements | Requirements/ChildRequirements Get all child requirements  |
| Requirements/ChildRequirements/BacklogItems - Get all backlog items | Requirements/ChildRequirements/BacklogItems Get all backlog items  |
| Requirements/ChildRequirements/BacklogItems/AcceptanceCriterions - Get all acceptance criteria | Requirements/ChildRequirements/BacklogItems/AcceptanceCriterions Get all acceptance criteria  |
| Requirements/ChildRequirements/ChildRequirements - Get all child requirements | Requirements/ChildRequirements/ChildRequirements Get all child requirements  |
| Resource Events - Get all resource calendar events | Resource Events Get all resource calendar events  |
| Resource Performance - Get performance data for all resources | Resource Performance Get performance data for all resources  |
| Sponsors - Get many sponsors | Sponsors Get many sponsors  |
| Sponsors/Sponsor Reference Types - Get many sponsor reference types | Sponsors/Sponsor Reference Types Get many sponsor reference types  |
| Sprints - Get all sprints | Sprints Get all sprints  |
| Task Performance - Get performance data for all tasks | Task Performance Get performance data for all tasks  |
| Unprocessed Project Costs - Get all unprocessed project costs | Unprocessed Project Costs Get all unprocessed project costs  |
| Unprocessed Project Costs/Errors - Get all errors for an unprocessed project cost | Unprocessed Project Costs/Errors Get all errors for an unprocessed project cost  |
| Unprocessed Project Costs/Project Standard Cost Collection Flexfields - Get standard cost collection information for unpro... | Unprocessed Project Costs/Project Standard Cost Collection Flexfields Get standard cost collection information for unprocessed project costs  |
| Unprocessed Project Costs/Unprocessed Project Cost Descriptive Flexfields - Get additional information for unprocessed project... | Unprocessed Project Costs/Unprocessed Project Cost Descriptive Flexfields Get additional information for unprocessed project costs  |
| Work Plan Templates - Get all the work plan templates | Work Plan Templates Get all the work plan templates  |
| Work Plan Templates/Task Deliverables - Get all deliverables for all the tasks | Work Plan Templates/Task Deliverables Get all deliverables for all the tasks  |
| Work Plan Templates/Task Dependencies - Get all dependencies in a work plan template | Work Plan Templates/Task Dependencies Get all dependencies in a work plan template  |
| Work Plan Templates/Task Expense Resource Assignments - Get all expense resource assignments for all the t... | Work Plan Templates/Task Expense Resource Assignments Get all expense resource assignments for all the tasks  |
| Work Plan Templates/Task Labor Resource Assignments - Get all labor resource assignments for all the tas... | Work Plan Templates/Task Labor Resource Assignments Get all labor resource assignments for all the tasks  |
| Work Plan Templates/Tasks - Get all tasks | Work Plan Templates/Tasks Get all tasks  |
| Work Plan Templates/Tasks/Gate Approvers - Get all the approvers of a gate | Work Plan Templates/Tasks/Gate Approvers Get all the approvers of a gate  |
| Work Plan Templates/Tasks/Task Descriptive Flexfields - Get all the descriptive flexfields for a task as a... | Work Plan Templates/Tasks/Task Descriptive Flexfields Get all the descriptive flexfields for a task as a collection  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Award Budgets - Update an award budget version (PATCH) | Award Budgets Update an award budget version  |
| Award Budgets/Planning Options/Planning Options Descriptive Flexfields - Update a descriptive flexfield for planning option... (PATCH) | Award Budgets/Planning Options/Planning Options Descriptive Flexfields Update a descriptive flexfield for planning options  |
| Award Budgets/Planning Resources - Update a planning resource for an award budget ver... (PATCH) | Award Budgets/Planning Resources Update a planning resource for an award budget version  |
| Award Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields - Update a descriptive flexfield for summary plannin... (PATCH) | Award Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields Update a descriptive flexfield for summary planning amounts  |
| Award Budgets/Planning Resources/Planning Amounts/Planning Amount Details - Update planning amount details for a period for a ... (PATCH) | Award Budgets/Planning Resources/Planning Amounts/Planning Amount Details Update planning amount details for a period for a plan line  |
| Awards - Update an award (PATCH) | Awards Update an award  |
| Awards/Award Attachments - Update an award attachment (PATCH) | Awards/Award Attachments Update an award attachment  |
| Awards/Award Budget Periods - Update an award budget period (PATCH) | Awards/Award Budget Periods Update an award budget period  |
| Awards/Award Certifications - Update an award certification (PATCH) | Awards/Award Certifications Update an award certification  |
| Awards/Award CFDAs - Update an award CFDA (PATCH) | Awards/Award CFDAs Update an award CFDA  |
| Awards/Award Department Credits - Update an award department credit (PATCH) | Awards/Award Department Credits Update an award department credit  |
| Awards/Award Descriptive Flexfields - Update an award descriptive flexfield (PATCH) | Awards/Award Descriptive Flexfields Update an award descriptive flexfield  |
| Awards/Award Funding Sources - Update an award funding source (PATCH) | Awards/Award Funding Sources Update an award funding source  |
| Awards/Award Fundings - Update award funding (PATCH) | Awards/Award Fundings Update award funding  |
| Awards/Award Fundings/Award Project Fundings - Update award project funding (PATCH) | Awards/Award Fundings/Award Project Fundings Update award project funding  |
| Awards/Award Keywords - Update an award keyword (PATCH) | Awards/Award Keywords Update an award keyword  |
| Awards/Award Personnel - Update an award personnel (PATCH) | Awards/Award Personnel Update an award personnel  |
| Awards/Award Personnel/Award Personnel Descriptive Flexfields - Update an award project personnel descriptive flex... (PATCH) | Awards/Award Personnel/Award Personnel Descriptive Flexfields Update an award project personnel descriptive flexfield  |
| Awards/Award Projects - Update an award project (PATCH) | Awards/Award Projects Update an award project  |
| Awards/Award Projects/Award Project Attachments - Update an award project attachment (PATCH) | Awards/Award Projects/Award Project Attachments Update an award project attachment  |
| Awards/Award Projects/Award Project Certifications - Update an award project certification (PATCH) | Awards/Award Projects/Award Project Certifications Update an award project certification  |
| Awards/Award Projects/Award Project Descriptive Flexfields - Update an award project descriptive flexfield (PATCH) | Awards/Award Projects/Award Project Descriptive Flexfields Update an award project descriptive flexfield  |
| Awards/Award Projects/Award Project Funding Sources - Update an award project funding source (PATCH) | Awards/Award Projects/Award Project Funding Sources Update an award project funding source  |
| Awards/Award Projects/Award Project Keywords - Update an award project keyword (PATCH) | Awards/Award Projects/Award Project Keywords Update an award project keyword  |
| Awards/Award Projects/Award Project Organization Credits - Update an award project department credit (PATCH) | Awards/Award Projects/Award Project Organization Credits Update an award project department credit  |
| Awards/Award Projects/Award Project Override Burden Schedules - Update a burden schedule for an award project. (PATCH) | Awards/Award Projects/Award Project Override Burden Schedules Update a burden schedule for an award project.  |
| Awards/Award Projects/Award Project Override Burden Schedules/Versions - Update a version in burden schedule. (PATCH) | Awards/Award Projects/Award Project Override Burden Schedules/Versions Update a version in burden schedule.  |
| Awards/Award Projects/Award Project Override Burden Schedules/Versions/Multipliers - Update a multiplier in burden schedule version. (PATCH) | Awards/Award Projects/Award Project Override Burden Schedules/Versions/Multipliers Update a multiplier in burden schedule version.  |
| Awards/Award Projects/Award Project Personnel - Update an award project personnel (PATCH) | Awards/Award Projects/Award Project Personnel Update an award project personnel  |
| Awards/Award Projects/Award Project Personnel/Award Project Personnel Descriptive Flexfields - Create an award project personnel descriptive flex... (PATCH) | Awards/Award Projects/Award Project Personnel/Award Project Personnel Descriptive Flexfields Create an award project personnel descriptive flexfield  |
| Awards/Award Projects/Award Project Reference Types - Update an award project reference (PATCH) | Awards/Award Projects/Award Project Reference Types Update an award project reference  |
| Awards/Award Projects/Award Project Task Burden Schedules - Update an award project task burden schedule (PATCH) | Awards/Award Projects/Award Project Task Burden Schedules Update an award project task burden schedule  |
| Awards/Award References - Update an award reference (PATCH) | Awards/Award References Update an award reference  |
| Awards/Award Terms - Update an award term (PATCH) | Awards/Award Terms Update an award term  |
| Change Orders - Update a change order (PATCH) | Change Orders Update a change order  |
| Change Orders/Change Impacts - Update an impact for a change order (PATCH) | Change Orders/Change Impacts Update an impact for a change order  |
| Change Orders/Change Participants - Update a participant for change order (PATCH) | Change Orders/Change Participants Update a participant for change order  |
| Deliverables - Update a deliverable (PATCH) | Deliverables Update a deliverable  |
| Deliverables/Attachment to a Deliverable - Update an attachment to a deliverable (PATCH) | Deliverables/Attachment to a Deliverable Update an attachment to a deliverable  |
| Deliverables/Deliverable and Project Task Associations - Update an association between a deliverable and a ... (PATCH) | Deliverables/Deliverable and Project Task Associations Update an association between a deliverable and a project task  |
| Deliverables/Deliverable and Requirement Associations - Update an association between a deliverable and a ... (PATCH) | Deliverables/Deliverable and Requirement Associations Update an association between a deliverable and a requirement  |
| Financial Project Plans - Update a resource assignment for a financial proje... (PATCH) | Financial Project Plans Update a resource assignment for a financial project plan version using the DeferPlanning flag in UPSERT mode  |
| Financial Project Plans/Resource Assignments - Update a resource assignment for a financial proje... (PATCH) | Financial Project Plans/Resource Assignments Update a resource assignment for a financial project plan version  |
| Financial Project Plans/Resource Assignments/Planning Amounts - Update a planning amount for a resource assignment (PATCH) | Financial Project Plans/Resource Assignments/Planning Amounts Update a planning amount for a resource assignment  |
| Financial Project Plans/Resource Assignments/Planning Amounts/Plan Lines Descriptive Flexfields - Update a descriptive flexfield for summary plannin... (PATCH) | Financial Project Plans/Resource Assignments/Planning Amounts/Plan Lines Descriptive Flexfields Update a descriptive flexfield for summary planning amounts  |
| Financial Project Plans/Resource Assignments/Planning Amounts/Planning Amount Details - Update a periodic planning amount for a resource a... (PATCH) | Financial Project Plans/Resource Assignments/Planning Amounts/Planning Amount Details Update a periodic planning amount for a resource assignment  |
| Grants Personnel - Update a grants personnel (PATCH) | Grants Personnel Update a grants personnel  |
| Grants Personnel/Grants Personnel  Descriptive Flexfields - Update a grants personnel descriptive flexfield (PATCH) | Grants Personnel/Grants Personnel  Descriptive Flexfields Update a grants personnel descriptive flexfield  |
| Grants Personnel/Grants Personnel Keywords - Update a grants personnel keyword (PATCH) | Grants Personnel/Grants Personnel Keywords Update a grants personnel keyword  |
| Person Assignment Labor Schedules - Update a Person Assignment Labor Schedule header. (PATCH) | Person Assignment Labor Schedules Update a Person Assignment Labor Schedule header.  |
| Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions - Update a version within a Person Assignment Labor ... (PATCH) | Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions Update a version within a Person Assignment Labor Schedule header.  |
| Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions/Person Assignment Labor Schedule Version Rules - Update a distribution rule within a version (PATCH) | Person Assignment Labor Schedules/Person Assignment Labor Schedule Versions/Person Assignment Labor Schedule Version Rules Update a distribution rule within a version  |
| Planning Resource Breakdown Structures for Projects - Update a planning resource breakdown structure ass... (PATCH) | Planning Resource Breakdown Structures for Projects Update a planning resource breakdown structure assignment to a project  |
| Project Assets - Update a project asset (PATCH) | Project Assets Update a project asset  |
| Project Assets/Project Asset Descriptive Flexfields - Update a project assets - description flexfield (PATCH) | Project Assets/Project Asset Descriptive Flexfields Update a project assets - description flexfield  |
| Project Billing Events - Update a project billing event (PATCH) | Project Billing Events Update a project billing event  |
| Project Billing Events/Project Billing Event Descriptive Flexfields - Update a project billing event descriptive flexfie... (PATCH) | Project Billing Events/Project Billing Event Descriptive Flexfields Update a project billing event descriptive flexfield  |
| Project Budgets - Adjust the budget version (POST) | Project Budgets Adjusts the project budget version using the adjustment parameters. Adjust the budget version  |
| Project Budgets - Refresh rates for the budget version (POST) | Project Budgets Refresh rates for the project budget version. Refresh rates for the budget version  |
| Project Budgets - Update a project budget version (PATCH) | Project Budgets Update a project budget version  |
| Project Budgets/Planning Options/Planning Options Descriptive Flexfields - Update a descriptive flexfield for planning option... (PATCH) | Project Budgets/Planning Options/Planning Options Descriptive Flexfields Update a descriptive flexfield for planning options  |
| Project Budgets/Planning Resources - Update a planning resource for a project budget ve... (PATCH) | Project Budgets/Planning Resources Update a planning resource for a project budget version  |
| Project Budgets/Planning Resources/Planning Amounts - Adjust the budget line (POST) | Project Budgets/Planning Resources/Planning Amounts Adjusts the project budget line using the adjustment parameters. Adjust the budget line  |
| Project Budgets/Planning Resources/Planning Amounts - Refresh rates for the budget line (POST) | Project Budgets/Planning Resources/Planning Amounts Refresh rates for the project budget line. Refresh rates for the budget line  |
| Project Budgets/Planning Resources/Planning Amounts - Update a plan line for a planning resource (PATCH) | Project Budgets/Planning Resources/Planning Amounts Update a plan line for a planning resource  |
| Project Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields - Update a descriptive flexfield for summary plannin... (PATCH) | Project Budgets/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields Update a descriptive flexfield for summary planning amounts  |
| Project Budgets/Planning Resources/Planning Amounts/Planning Amount Details - Adjust the budget line period (POST) | Project Budgets/Planning Resources/Planning Amounts/Planning Amount Details Adjusts the project budget line period using the adjustment parameters. Adjust the budget line period  |
| Project Budgets/Planning Resources/Planning Amounts/Planning Amount Details - Update planning amount details for a period for a ... (PATCH) | Project Budgets/Planning Resources/Planning Amounts/Planning Amount Details Update planning amount details for a period for a plan line  |
| Project Contract Invoices - Approval of a single submitted invoice (POST) | Project Contract Invoices Use this action to approve a submitted invoice. Approval of a single submitted invoice  |
| Project Contract Invoices - Rejection of a single submitted invoice (POST) | Project Contract Invoices Use this action to reject a submitted invoice. Rejection of a single submitted invoice  |
| Project Contract Invoices - Release of a single approved invoice (POST) | Project Contract Invoices Use this action to release invoices that have been approved and are ready to be released. Release of a single approved invoice  |
| Project Contract Invoices - Return of a single approved or approval rejected i... (POST) | Project Contract Invoices Use this action to return an approved or approval rejected invoice to draft status. Return of a single approved or approval rejected invoice to draft status  |
| Project Contract Invoices - Submit of a single draft invoice (POST) | Project Contract Invoices Use this action to submit invoices for approval. Submit of a single draft invoice  |
| Project Contract Invoices - Unrelease of a single released or transfer rejecte... (POST) | Project Contract Invoices Use this action to unrelease invoices that are either in released status or transfer rejected status. To unrelease an invoice that isn't the last in sequence, you must first unrelease and delete all subsequent invoices. Invoices that are in transferred or accepted statuses cannot be unreleased. Unrelease of a single released or transfer rejected invoice  |
| Project Costs - Adjust project costs (POST) | Project Costs Adjust existing project costs after reviewing them. Adjust project costs  |
| Project Costs - Update a project cost (PATCH) | Project Costs Update a project cost  |
| Project Costs/Project Costs Descriptive Flexfields - Update a descriptive flexfield for a project cost (PATCH) | Project Costs/Project Costs Descriptive Flexfields Update a descriptive flexfield for a project cost  |
| Project Costs/Project Standard Cost Collection Flexfields - Update standard cost collection information for a ... (PATCH) | Project Costs/Project Standard Cost Collection Flexfields Update standard cost collection information for a project cost  |
| Project Enterprise Expense Resources - Update a project enterprise expense resource (PATCH) | Project Enterprise Expense Resources Update a project enterprise expense resource  |
| Project Enterprise Labor Resources - Update a project enterprise labor resource (PATCH) | Project Enterprise Labor Resources Update a project enterprise labor resource  |
| Project Expenditure Batches - Update a project expenditure batch (PATCH) | Project Expenditure Batches Update a project expenditure batch  |
| Project Expenditure Items - Update an expenditure item (PATCH) | Project Expenditure Items Update an expenditure item  |
| Project Expenditure Items/Project Expenditure Items Descriptive Flexfields - Update a descriptive flexfield for a project expen... (PATCH) | Project Expenditure Items/Project Expenditure Items Descriptive Flexfields Update a descriptive flexfield for a project expenditure item  |
| Project Expense Resources - Update a project expense resource (PATCH) | Project Expense Resources Update a project expense resource  |
| Project Forecasts - Adjust the forecast version (POST) | Project Forecasts Adjusts the project forecast version using the adjustment parameters. Adjust the forecast version  |
| Project Forecasts - Refresh rates for the forecast version (POST) | Project Forecasts Refresh rates for the project forecast version. Refresh rates for the forecast version  |
| Project Forecasts - Refreshes actual amounts for project forecast vers... (POST) | Project Forecasts Refresh actual amounts for project forecast version. Performs a refresh for the entire forecast version if you don't specify the refreshActualAmountsFrom and refreshActualAmountsTo dates. If you specify these dates, the actual amounts that are incurred outside these dates aren't considered for the forecast version. Refreshes actual amounts for project forecast version  |
| Project Forecasts - Update a project forecast version (PATCH) | Project Forecasts Update a project forecast version  |
| Project Forecasts/Planning Options/Planning Options Descriptive Flexfields - Update a descriptive flexfield for planning option... (PATCH) | Project Forecasts/Planning Options/Planning Options Descriptive Flexfields Update a descriptive flexfield for planning options  |
| Project Forecasts/Planning Resources - Update a planning resource for a project forecast ... (PATCH) | Project Forecasts/Planning Resources Update a planning resource for a project forecast version  |
| Project Forecasts/Planning Resources/Planning Amounts - Adjust the forecast line (POST) | Project Forecasts/Planning Resources/Planning Amounts Adjusts the project forecast line using the adjustment parameters. Adjust the forecast line  |
| Project Forecasts/Planning Resources/Planning Amounts - Refresh rates for the Forecast line (POST) | Project Forecasts/Planning Resources/Planning Amounts Refresh rates for the project forecast line. Refresh rates for the Forecast line  |
| Project Forecasts/Planning Resources/Planning Amounts - Update a summary planning amount for a planning re... (PATCH) | Project Forecasts/Planning Resources/Planning Amounts Update a summary planning amount for a planning resource  |
| Project Forecasts/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields - Update a descriptive flexfield for summary plannin... (PATCH) | Project Forecasts/Planning Resources/Planning Amounts/Plan Lines Descriptive Flexfields Update a descriptive flexfield for summary planning amounts  |
| Project Forecasts/Planning Resources/Planning Amounts/Planning Amount Details - Adjust the forecast line period (POST) | Project Forecasts/Planning Resources/Planning Amounts/Planning Amount Details Adjusts the project forecast line period using the adjustment parameters. Adjust the forecast line period  |
| Project Forecasts/Planning Resources/Planning Amounts/Planning Amount Details - Update details for a period for a planning amount (PATCH) | Project Forecasts/Planning Resources/Planning Amounts/Planning Amount Details Update details for a period for a planning amount  |
| Project Issues - Update an issue (PATCH) | Project Issues Update an issue  |
| Project Issues/Project Issue Action Items - Update an action item (PATCH) | Project Issues/Project Issue Action Items Update an action item  |
| Project Labor Resources - Update a project labor resource (PATCH) | Project Labor Resources Update a project labor resource  |
| Project Numbering Configurations - Update a project numbering configuration (PATCH) | Project Numbering Configurations Update a project numbering configuration  |
| Project Numbering Configurations/Project Numbering Configuration Details - Update a project numbering configuration detail (PATCH) | Project Numbering Configurations/Project Numbering Configuration Details Update a project numbering configuration detail  |
| Project Plan Resource Requests - Approve project resource request. (POST) | Project Plan Resource Requests Approve project resource request via this action. The request must be in Pending Adjustment status to allow this action. Approve project resource request.  |
| Project Plan Resource Requests - Cancel project resource request. (POST) | Project Plan Resource Requests Cancel project resource request via this action. The request must be in Open status to allow this action. Cancel project resource request.  |
| Project Plan Resource Requests - Reject project resource request. (POST) | Project Plan Resource Requests Reject project resource request via this action. The request must be in Pending Adjustment status to allow this action. Reject project resource request.  |
| Project Plan Resource Requests - Update a project resource request (PATCH) | Project Plan Resource Requests Update a project resource request  |
| Project Plan Resource Requests/Project Plan Resource Request Descriptive Flexfields - Update a descriptive flexfield for a project resou... (PATCH) | Project Plan Resource Requests/Project Plan Resource Request Descriptive Flexfields Update a descriptive flexfield for a project resource request  |
| Project Plan Resource Requests/Project Plan Resource Request Lines - Update a nominated resource on a project resource ... (PATCH) | Project Plan Resource Requests/Project Plan Resource Request Lines Update a nominated resource on a project resource request  |
| Project Plans/Baselines - Update a baseline (PATCH) | Project Plans/Baselines Update a baseline  |
| Project Plans/Project Descriptive Flexfields - Update the descriptive flexfields for a project (PATCH) | Project Plans/Project Descriptive Flexfields Update the descriptive flexfields for a project  |
| Project Plans/Task Dependencies - Update a dependency between two tasks of a project (PATCH) | Project Plans/Task Dependencies Update a dependency between two tasks of a project  |
| Project Plans/Task Expense Resource Assignments - Update an expense resource assignment for a task (PATCH) | Project Plans/Task Expense Resource Assignments Update an expense resource assignment for a task  |
| Project Plans/Task Labor Resource Assignments - Update a labor resource assignment for a task (PATCH) | Project Plans/Task Labor Resource Assignments Update a labor resource assignment for a task  |
| Project Plans/Task Work Items - Update a work item for a task (PATCH) | Project Plans/Task Work Items Update a work item for a task  |
| Project Plans/Tasks - Update a task in a project or update a project by ... (PATCH) | Project Plans/Tasks Update a task in a project or update a project by creating tasks from a work plan template  |
| Project Plans/Tasks/Gate Approvers - Update an approver of a gate in a project (PATCH) | Project Plans/Tasks/Gate Approvers Update an approver of a gate in a project  |
| Project Plans/Tasks/Tasks Descriptive Flexfields - Update descriptive flexfields for a task (PATCH) | Project Plans/Tasks/Tasks Descriptive Flexfields Update descriptive flexfields for a task  |
| Project Process Configurators - Update a project process configurator (PATCH) | Project Process Configurators Update a project process configurator  |
| Project Process Configurators/Source Assignments - Update the source for a configurator (PATCH) | Project Process Configurators/Source Assignments Update the source for a configurator  |
| Project Progress - Update the progress of a project (PATCH) | Project Progress Update the progress of a project  |
| Project Progress/Task Progress - Update the progress of a project task (PATCH) | Project Progress/Task Progress Update the progress of a project task  |
| Project Resource Actual Hours - Update a project resource actual hour record (PATCH) | Project Resource Actual Hours Update a project resource actual hour record  |
| Project Resource Assignments - Adjust project resource assignment schedule (POST) | Project Resource Assignments Perform project resource assignment schedule change via this action. The schedule change can happen due to assignment date changes and/or changes in assignment hours per day.  The assignment must be in Confirmed or Reserved status to perform this action. Adjust project resource assignment schedule  |
| Project Resource Assignments - Cancel project resource assignment (POST) | Project Resource Assignments Cancel a project resource assignment. Cancel project resource assignment  |
| Project Resource Assignments - Update a project resource assignment (PATCH) | Project Resource Assignments Update a project resource assignment  |
| Project Resource Pools - Update a project resource pool. (PATCH) | Project Resource Pools Update a project resource pool.  |
| Project Resource Pools/Project Resource Pool Members - Update a project resource member assigned to a pro... (PATCH) | Project Resource Pools/Project Resource Pool Members Update a project resource member assigned to a project resource pool.  |
| Project Resource Requests - Approve project resource request. (POST) | Project Resource Requests Approve project resource request via this action. The request must be in Pending Adjustment status to allow this action. Approve project resource request.  |
| Project Resource Requests - Cancel project resource request. (POST) | Project Resource Requests Cancel project resource request via this action. The request must be in Open status to allow this action. Cancel project resource request.  |
| Project Resource Requests - Reject project resource request. (POST) | Project Resource Requests Reject project resource request via this action. The request must be in Pending Adjustment status to allow this action. Reject project resource request.  |
| Project Resource Requests - Update a project resource request (PATCH) | Project Resource Requests Update a project resource request  |
| Project Resource Requests/Project Resource Request Descriptive Flexfields - Update a descriptive flexfield for a project resou... (PATCH) | Project Resource Requests/Project Resource Request Descriptive Flexfields Update a descriptive flexfield for a project resource request  |
| Project Resource Requests/Project Resource Request Lines - Update a nominated resource on a project resource ... (PATCH) | Project Resource Requests/Project Resource Request Lines Update a nominated resource on a project resource request  |
| Project Templates/Setup Options - Update a project template setup option (PATCH) | Project Templates/Setup Options Update a project template setup option  |
| Projects - Add tasks from work plan template to the project (POST) | Projects Use this action to add tasks from a work plan template to the Project. If no task reference is provided, tasks from work plan template are created directly under the project node. Provide either parent task reference or the peer task reference based on where you want to create tasks from work plan template. Add tasks from work plan template to the project  |
| Projects - Update a project (PATCH) | Projects Update a project  |
| Projects Users/Project Tasks - Update a project task (PATCH) | Projects Users/Project Tasks Update a project task  |
| Projects Users/To Do Tasks - Update a to do task of a user (PATCH) | Projects Users/To Do Tasks Update a to do task of a user  |
| Projects/Project Classifications - Update a project classification (PATCH) | Projects/Project Classifications Update a project classification  |
| Projects/Project Descriptive Flexfields - Update a project descriptive flexfield (PATCH) | Projects/Project Descriptive Flexfields Update a project descriptive flexfield  |
| Projects/Project Opportunities - Update a project opportunity (PATCH) | Projects/Project Opportunities Update a project opportunity  |
| Projects/Project Team Members - Update a project team member (PATCH) | Projects/Project Team Members Update a project team member  |
| Projects/Project Transaction Controls - Update a project transaction control (PATCH) | Projects/Project Transaction Controls Update a project transaction control  |
| Projects/Task Dependencies - Update a task dependency (PATCH) | Projects/Task Dependencies Update a task dependency  |
| Projects/Tasks - Update a project task (PATCH) | Projects/Tasks Update a project task  |
| Projects/Tasks/Expense Resource Assignments - Update an expense resource assignment for a projec... (PATCH) | Projects/Tasks/Expense Resource Assignments Update an expense resource assignment for a project task  |
| Projects/Tasks/Labor Resource Assignments - Update a labor resource assignment for a project t... (PATCH) | Projects/Tasks/Labor Resource Assignments Update a labor resource assignment for a project task  |
| Projects/Tasks/Task Transaction Controls - Update a task transaction control (PATCH) | Projects/Tasks/Task Transaction Controls Update a task transaction control  |
| Projects/Tasks/Tasks Descriptive Flexfields - Update a descriptive flexfield for a project task (PATCH) | Projects/Tasks/Tasks Descriptive Flexfields Update a descriptive flexfield for a project task  |
| Rate Schedules - Update a Rate Schedule (PATCH) | Rate Schedules Update a Rate Schedule  |
| Rate Schedules/Rate Schedule Descriptive Flexfields - Update rate schedule descriptive flexfields (PATCH) | Rate Schedules/Rate Schedule Descriptive Flexfields Update rate schedule descriptive flexfields  |
| Requirements - Update a requirement (PATCH) | Requirements Update a requirement  |
| Requirements/BacklogItems - Update a backlog item (PATCH) | Requirements/BacklogItems Update a backlog item  |
| Requirements/BacklogItems/AcceptanceCriterions - Update an acceptance criteria (PATCH) | Requirements/BacklogItems/AcceptanceCriterions Update an acceptance criteria  |
| Requirements/ChildRequirements - Update a child requirement (PATCH) | Requirements/ChildRequirements Update a child requirement  |
| Requirements/ChildRequirements/BacklogItems - Update a backlog item (PATCH) | Requirements/ChildRequirements/BacklogItems Update a backlog item  |
| Requirements/ChildRequirements/BacklogItems/AcceptanceCriterions - Update an acceptance criteria (PATCH) | Requirements/ChildRequirements/BacklogItems/AcceptanceCriterions Update an acceptance criteria  |
| Requirements/ChildRequirements/ChildRequirements - Update a child requirement (PATCH) | Requirements/ChildRequirements/ChildRequirements Update a child requirement  |
| Sponsors - Update a sponsor (PATCH) | Sponsors Update a sponsor  |
| Sponsors/Sponsor Reference Types - Update a sponsor reference type (PATCH) | Sponsors/Sponsor Reference Types Update a sponsor reference type  |
| Unprocessed Project Costs - Update an unprocessed project cost (PATCH) | Unprocessed Project Costs Update an unprocessed project cost  |
| Unprocessed Project Costs/Project Standard Cost Collection Flexfields - Update standard cost collection information for an... (PATCH) | Unprocessed Project Costs/Project Standard Cost Collection Flexfields Update standard cost collection information for an unprocessed project cost  |
| Unprocessed Project Costs/Unprocessed Project Cost Descriptive Flexfields - Update additional information for an unprocessed p... (PATCH) | Unprocessed Project Costs/Unprocessed Project Cost Descriptive Flexfields Update additional information for an unprocessed project cost  |
| Work Plan Templates - Update a work plan template (PATCH) | Work Plan Templates Update a work plan template  |
| Work Plan Templates/Task Dependencies - Update a dependency between two tasks (PATCH) | Work Plan Templates/Task Dependencies Update a dependency between two tasks  |
| Work Plan Templates/Task Expense Resource Assignments - Update an expense resource assignment for a task (PATCH) | Work Plan Templates/Task Expense Resource Assignments Update an expense resource assignment for a task  |
| Work Plan Templates/Task Labor Resource Assignments - Update a labor resource assignment for a task (PATCH) | Work Plan Templates/Task Labor Resource Assignments Update a labor resource assignment for a task  |
| Work Plan Templates/Tasks - Update a task (PATCH) | Work Plan Templates/Tasks Update a task  |
| Work Plan Templates/Tasks/Gate Approvers - Update an approver of a gate (PATCH) | Work Plan Templates/Tasks/Gate Approvers Update an approver of a gate  |


