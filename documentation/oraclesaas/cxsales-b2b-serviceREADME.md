
# cxsales-b2b-service


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Access Groups - Create an access group | Access Groups Create an access group  |
| Access Groups/Access Group Members - Create an access group member | Access Groups/Access Group Members Create an access group member  |
| Accounts - Assign accounts | Accounts Assign an account. Assign accounts  |
| Accounts - Create an account | Accounts Create an account Create an account  |
| Accounts - Find duplicates | Accounts Use Data Quality Duplicate Identification to find duplicate accounts. The method returns a list of duplicate accounts. Find duplicates  |
| Accounts/Account Attachments - Upload an attachment | Accounts/Account Attachments Upload an attachment  |
| Accounts/Account Extension Bases - Create an account extension base | Accounts/Account Extension Bases Create an account extension base  |
| Accounts/Account Resources - Create a sales team member | Accounts/Account Resources Create a sales team member  |
| Accounts/Additional Identifier - Create an additional identifier | Accounts/Additional Identifier Create an additional identifier  |
| Accounts/Additional Names - Create an additional name | Accounts/Additional Names Create an additional name  |
| Accounts/Addresses - Create an address | Accounts/Addresses Create an address  |
| Accounts/Addresses/Address Locales - Create an address locale | Accounts/Addresses/Address Locales Create an address locale  |
| Accounts/Addresses/Address Purposes - Create an address purpose | Accounts/Addresses/Address Purposes Create an address purpose  |
| Accounts/Aux Classifications - Create a customer classification | Accounts/Aux Classifications Create a customer classification  |
| Accounts/Contact Points - Create a contact point | Accounts/Contact Points Create a contact point  |
| Accounts/Notes - Create a note | Accounts/Notes Create a note  |
| Accounts/Organization Contacts - Create an account contact | Accounts/Organization Contacts Create an account contact  |
| Accounts/Primary Addresses - Create a primary address | Accounts/Primary Addresses Create a primary address  |
| Accounts/Relationships - Create a relationship | Accounts/Relationships Create a relationship  |
| Accounts/Source System References - Create a source system reference | Accounts/Source System References Create a source system reference  |
| Action Events - Create an action event | Action Events Create an action event  |
| Action Plans - Adds adhoc actions to action plans | Action Plans Adds adhoc actions to a single action plan or multiple action plans. The actions and action plans are specified by a given list of action plan numbers and action numbers. Adds adhoc actions to action plans  |
| Action Plans - Create an Action Plan | Action Plans Create an Action Plan Create an Action Plan  |
| Action Plans - Recreates the underlying business object | Action Plans Recreates the underlying business object of the corresponding action plan action such as SR, Task, or Appointment, that has failed during previous orchestration. Recreates the underlying business object  |
| Action Plans - Refresh an action plan | Action Plans Refreshes an action plan that has already been initiated. Refresh an action plan  |
| Action Plans - Refresh an action plan action | Action Plans Refreshes an action plan action that has already been initiated. Refresh an action plan action  |
| Action Plans/Action Plan Actions - Create an action plan action | Action Plans/Action Plan Actions Create an action plan action  |
| Action Plans/Action Plan Actions/Action Plan Action Relations - Create an action plan action relation | Action Plans/Action Plan Actions/Action Plan Action Relations Create an action plan action relation  |
| Action Templates - Create an Action Plan Template | Action Templates Create an Action Plan Template  |
| Action Templates/Template Actions - Create a Template Action | Action Templates/Template Actions Create a Template Action  |
| Action Templates/Template Actions/Action Relations - Create an Action Relation | Action Templates/Template Actions/Action Relations Create an Action Relation  |
| Actions - Create an Action | Actions Create an Action  |
| Actions/Action Attributes - Create an Action Attribute | Actions/Action Attributes Create an Action Attribute  |
| Actions/Action Conditions - Create an Action Condition | Actions/Action Conditions Create an Action Condition  |
| Activities - Create an activity | Activities Create an activity  |
| Activities/Activity Assignees - Create an activity assignee | Activities/Activity Assignees Create an activity assignee  |
| Activities/Activity Attachments - Create an activity attachment | Activities/Activity Attachments Create an activity attachment  |
| Activities/Activity Contacts - Create an activity contact | Activities/Activity Contacts Create an activity contact  |
| Activities/Activity Objectives - Create an activity objective | Activities/Activity Objectives Create an activity objective  |
| Activities/Notes - Create an activity note | Activities/Notes Create an activity note  |
| Assets - Create an asset | Assets Create an asset  |
| Assets/Asset Contacts - Create an asset contact | Assets/Asset Contacts Create an asset contact  |
| Assets/Asset Resources - Create an asset resource | Assets/Asset Resources Create an asset resource  |
| Assets/Attachments - Upload an attachment | Assets/Attachments Upload an attachment  |
| Attribute Predictions - Create an attribute prediction | Attribute Predictions Create an attribute prediction  |
| Billing Adjustments - Create a billing adjustment | Billing Adjustments Create a billing adjustment  |
| Billing Adjustments/Billing Adjustment Events - Create a billing adjustment event | Billing Adjustments/Billing Adjustment Events Create a billing adjustment event  |
| Budgets - Create a MDF budget | Budgets Create a MDF budget  |
| Budgets/Budget Countries - Create a MDF budget country | Budgets/Budget Countries Create a MDF budget country  |
| Budgets/Claims - Create a MDF claims against the budget | Budgets/Claims Create a MDF claims against the budget  |
| Budgets/Fund Requests - Create a MDF request for the budget | Budgets/Fund Requests Create a MDF request for the budget  |
| Budgets/MDF Budget Teams - Create a MDF budget team member | Budgets/MDF Budget Teams Create a MDF budget team member  |
| Budgets/Notes - Create a MDF budget note | Budgets/Notes Create a MDF budget note  |
| Business Plans - Create a business plan | Business Plans Create a business plan  |
| Business Plans/Business Plan Resources - Create a business plan resource | Business Plans/Business Plan Resources Create a business plan resource  |
| Business Plans/Notes - Create a business plan note | Business Plans/Notes Create a business plan note  |
| Business Plans/SWOTs - Create a business plan SWOT | Business Plans/SWOTs Create a business plan SWOT  |
| Calculation Simulations - Create a calculation simulation | Calculation Simulations Create a calculation simulation  |
| Calculation Simulations/Simulation Transactions - Create a simulation transaction | Calculation Simulations/Simulation Transactions Create a simulation transaction  |
| Calculation Simulations/Simulation Transactions/Simulation Credits - Create a simulation credit | Calculation Simulations/Simulation Transactions/Simulation Credits Create a simulation credit  |
| Calculation Simulations/Simulation Transactions/Simulation Transaction Descriptive Flex Fields - Create a simulation transaction descriptive flex f... | Calculation Simulations/Simulation Transactions/Simulation Transaction Descriptive Flex Fields Create a simulation transaction descriptive flex field  |
| Campaign Members - Create a campaign member | Campaign Members Create a campaign member  |
| Campaigns - Create a campaign | Campaigns Create a campaign  |
| Cases - Create a case | Cases Create a case  |
| Cases/Case Contacts - Create a case contact | Cases/Case Contacts Create a case contact  |
| Cases/Case Households - Create a case household | Cases/Case Households Create a case household  |
| Cases/Case Messages - Create a case message | Cases/Case Messages Create a case message  |
| Cases/Case Opportunities - Create an opportunity | Cases/Case Opportunities Create an opportunity  |
| Cases/Case Resources - Create a case resource | Cases/Case Resources Create a case resource  |
| Catalog Product Groups - Create a product group (Not Supported) | Catalog Product Groups Create a product group (Not Supported)  |
| Catalog Product Groups/Attachments - Create an attachment | Catalog Product Groups/Attachments Create an attachment  |
| Catalog Product Groups/Translations - Create a product group translation (Not Supported) | Catalog Product Groups/Translations Create a product group translation (Not Supported)  |
| Catalog Products Items - Create a product item (Not Supported) | Catalog Products Items Create a product item (Not Supported)  |
| Catalog Products Items/Attachments - Create an attachment | Catalog Products Items/Attachments Create an attachment  |
| Catalog Products Items/Item Translation - Create an item translation (Not Supported) | Catalog Products Items/Item Translation Create an item translation (Not Supported)  |
| Categories - Create a category | Categories Create a category  |
| Channels - Create a channel | Channels Create a channel  |
| Channels/Channel Resources - Create a channel resource member | Channels/Channel Resources Create a channel resource member  |
| Channels/Sender Identification Priorities - Create a sender priority sequence | Channels/Sender Identification Priorities Create a sender priority sequence  |
| Chat Authentications - Create a chat authentication | Chat Authentications Create a chat authentication  |
| Chat Interactions - Create a chat interaction (Not supported) | Chat Interactions Create a chat interaction (Not supported)  |
| Chat Interactions/Transcripts - Create a transcript (Not supported) | Chat Interactions/Transcripts Create a transcript (Not supported)  |
| Claims - Create an MDF claim | Claims Create an MDF claim  |
| Claims/Claim Settlements - Create a claim settlement | Claims/Claim Settlements Create a claim settlement  |
| Claims/Claim Team Members - Create a claim team member | Claims/Claim Team Members Create a claim team member  |
| Claims/Notes - Create a note | Claims/Notes Create a note  |
| Collaboration Actions - Create a collaboration action | Collaboration Actions Create a collaboration action  |
| Collaboration Actions/Action Attributes - Add an attributes to a collaboration action | Collaboration Actions/Action Attributes Add an attributes to a collaboration action  |
| Compensation Plans - Create a compensation plan | Compensation Plans Create a compensation plan  |
| Compensation Plans/Compensation Plan - Descriptive Flex Fields - Create a descriptive flex field | Compensation Plans/Compensation Plan - Descriptive Flex Fields Create a descriptive flex field  |
| Compensation Plans/Compensation Plan - Plan Components - Create a plan component | Compensation Plans/Compensation Plan - Plan Components Create a plan component  |
| Compensation Plans/Compensation Plan - Roles - Create a role | Compensation Plans/Compensation Plan - Roles Create a role  |
| Compensation Plans/Direct Assignment Requests - Create a direct assignment request | Compensation Plans/Direct Assignment Requests Create a direct assignment request  |
| Competencies - Create a competency | Competencies Create a competency  |
| Competitors - Create a competitor | Competitors Create a competitor  |
| Contacts - Create a contact | Contacts Create a contact Create a contact  |
| Contacts - Find duplicates | Contacts Finds the duplicate. Find duplicates  |
| Contacts/Additional Identifiers - Create an additional identifier | Contacts/Additional Identifiers Create an additional identifier  |
| Contacts/Additional Names - Create an additional name | Contacts/Additional Names Create an additional name  |
| Contacts/Attachments - Upload a contact's picture | Contacts/Attachments Upload a contact's picture  |
| Contacts/Aux Classifications - Create a customer classification | Contacts/Aux Classifications Create a customer classification  |
| Contacts/Contact Addresses - Create an address | Contacts/Contact Addresses Create an address  |
| Contacts/Contact Addresses/Address Locales - Create an address locale | Contacts/Contact Addresses/Address Locales Create an address locale  |
| Contacts/Contact Addresses/Contact Address Purposes - Create an address purpose | Contacts/Contact Addresses/Contact Address Purposes Create an address purpose  |
| Contacts/Contact Attachments - Upload an attachment | Contacts/Contact Attachments Upload an attachment  |
| Contacts/Contact Points - Create a contact point | Contacts/Contact Points Create a contact point  |
| Contacts/Contact Primary Addresses - Create a primary address | Contacts/Contact Primary Addresses Create a primary address  |
| Contacts/Notes - Create a note | Contacts/Notes Create a note  |
| Contacts/Relationships - Create a relationship | Contacts/Relationships Create a relationship  |
| Contacts/Sales Account Resources - Create a sales team member | Contacts/Sales Account Resources Create a sales team member  |
| Contacts/Source System References - Create a source system reference | Contacts/Source System References Create a source system reference  |
| Contests - Create a contest | Contests Create a contest  |
| Contests/Contest History - Create a contest history record | Contests/Contest History Create a contest history record  |
| Contests/Contest Resources - Create a contest participant | Contests/Contest Resources Create a contest participant  |
| Contests/Contest Scores - Create a contest score | Contests/Contest Scores Create a contest score  |
| Contract Asset Transactions - Create a contract asset transaction | Contract Asset Transactions Create a contract asset transaction  |
| Contract Asset Transactions/Contract Asset Transaction Details - Create a contract asset transaction detail | Contract Asset Transactions/Contract Asset Transaction Details Create a contract asset transaction detail  |
| Contracts - Create contracts | Contracts Create contracts  |
| Contracts/Bill Plans - Create a bill plan | Contracts/Bill Plans Create a bill plan  |
| Contracts/Bill Plans/Job Assignment Overrides - Create job assignment overrides | Contracts/Bill Plans/Job Assignment Overrides Create job assignment overrides  |
| Contracts/Bill Plans/Job Rate Overrides - Create job rate overrides | Contracts/Bill Plans/Job Rate Overrides Create job rate overrides  |
| Contracts/Bill Plans/Job Title Overrides - Create job title overrides | Contracts/Bill Plans/Job Title Overrides Create job title overrides  |
| Contracts/Bill Plans/Labor Multiplier Overrides - Create labor multiplier overrides | Contracts/Bill Plans/Labor Multiplier Overrides Create labor multiplier overrides  |
| Contracts/Bill Plans/Non Labor Rate Overrides - Create non labor rate overrides | Contracts/Bill Plans/Non Labor Rate Overrides Create non labor rate overrides  |
| Contracts/Bill Plans/Person Rate Overrides - Create person rate overrides | Contracts/Bill Plans/Person Rate Overrides Create person rate overrides  |
| Contracts/Billing Controls - Create a billing control | Contracts/Billing Controls Create a billing control  |
| Contracts/Contract Documents - Create contract documents | Contracts/Contract Documents Create contract documents  |
| Contracts/Contract Header Flexfields - Create contract header flexfields | Contracts/Contract Header Flexfields Create contract header flexfields  |
| Contracts/Contract Lines - Create contract lines | Contracts/Contract Lines Create contract lines  |
| Contracts/Contract Lines/Associated Projects - Create an associated project | Contracts/Contract Lines/Associated Projects Create an associated project  |
| Contracts/Contract Lines/Billing Controls - Create a billing control | Contracts/Contract Lines/Billing Controls Create a billing control  |
| Contracts/Contract Lines/Contract Line Flexfields - Create contract line flexfields | Contracts/Contract Lines/Contract Line Flexfields Create contract line flexfields  |
| Contracts/Contract Lines/Sales Credits - Create sales credits | Contracts/Contract Lines/Sales Credits Create sales credits  |
| Contracts/Contract Parties - Create contract parties | Contracts/Contract Parties Create contract parties  |
| Contracts/Contract Parties/Contract Party Contacts - Create contract party contacts | Contracts/Contract Parties/Contract Party Contacts Create contract party contacts  |
| Contracts/Contract Parties/Contract Party Contacts/Contract Party Contact Flexfields - Create contract party contact flexfields | Contracts/Contract Parties/Contract Party Contacts/Contract Party Contact Flexfields Create contract party contact flexfields  |
| Contracts/Contract Parties/Contract Party Flexfields - Create contract party flexfields | Contracts/Contract Parties/Contract Party Flexfields Create contract party flexfields  |
| Contracts/Related Documents - Create related documents | Contracts/Related Documents Create related documents  |
| Contracts/Revenue Plans - Create revenue plans | Contracts/Revenue Plans Create revenue plans  |
| Contracts/Revenue Plans/Job Assignment Overrides - Create job assignment overrides | Contracts/Revenue Plans/Job Assignment Overrides Create job assignment overrides  |
| Contracts/Revenue Plans/Job Rate Overrides - Create job rate overrides | Contracts/Revenue Plans/Job Rate Overrides Create job rate overrides  |
| Contracts/Revenue Plans/Labor Multiplier Overrides - Create labor multiplier overrides | Contracts/Revenue Plans/Labor Multiplier Overrides Create labor multiplier overrides  |
| Contracts/Revenue Plans/Non Labor Rate Overrides - Create non labor rate overrides | Contracts/Revenue Plans/Non Labor Rate Overrides Create non labor rate overrides  |
| Contracts/Revenue Plans/Person Rate Overrides - Create person rate overrides | Contracts/Revenue Plans/Person Rate Overrides Create person rate overrides  |
| Contracts/Sales Credits - Create sales credits | Contracts/Sales Credits Create sales credits  |
| Contracts/Supporting Documents - Create supporting documents | Contracts/Supporting Documents Create supporting documents  |
| Conversation Messages - Create a conversation message | Conversation Messages Create a conversation message Create a conversation message  |
| Conversation Messages - Create a conversation message for a business objec... | Conversation Messages Creates a conversation, conversation message, conversation message recipients, and associates the conversation with one or more business objects. Create a conversation message for a business object  |
| Conversation Messages/Attachments - Create an attachment | Conversation Messages/Attachments Create an attachment  |
| Conversation Messages/Conversation Message Recipients - Create a conversation message recipient | Conversation Messages/Conversation Message Recipients Create a conversation message recipient  |
| Conversations/Conversation References - Create a conversation reference | Conversations/Conversation References Create a conversation reference  |
| Credit Categories - Create a credit category | Credit Categories Create a credit category  |
| DaaS Smart Data - Create smart data | DaaS Smart Data Create smart data Create smart data  |
| DaaS Smart Data - Get accounts by account ID and address fields | DaaS Smart Data Get all account data from DaaS for the account Id. Get accounts by account ID and address fields  |
| DaaS Smart Data - Get address data | DaaS Smart Data Get all address data from Loqate for the address Id. Get address data  |
| Deal Registrations - Create a deal registration | Deal Registrations Create a deal registration  |
| Deal Registrations/Deal Products - Create a deal product | Deal Registrations/Deal Products Create a deal product  |
| Deal Registrations/Deal Team Members - Create a deal team member | Deal Registrations/Deal Team Members Create a deal team member  |
| Deal Registrations/Notes - Create a note | Deal Registrations/Notes Create a note  |
| Deal Registrations/Opportunities - Assign matched opportunity | Deal Registrations/Opportunities Assigns a matched opportunity. Assign matched opportunity  |
| Deal Registrations/Opportunities - Copy matched opportunity | Deal Registrations/Opportunities Copies a matched opportunity. Copy matched opportunity  |
| Deal Registrations/Product Groups - Set product group usage mode | Deal Registrations/Product Groups Set the product group usage mode Set product group usage mode  |
| Deal Registrations/Product Groups - Set product usage code | Deal Registrations/Product Groups Set a product usage code. Set product usage code  |
| Deal Registrations/Products - Set product usage code | Deal Registrations/Products Set a product usage code. Set product usage code  |
| Deal Registrations/Products - Set product usage mode | Deal Registrations/Products Set the product usage mode Set product usage mode  |
| Device Tokens - Create a device token | Device Tokens Create a device token  |
| Dynamic Link Patterns - Create a dynamic link pattern | Dynamic Link Patterns Create a dynamic link pattern  |
| Email Verifications - Create an email verification | Email Verifications Create an email verification  |
| Entitlements - Create a subscription entitlement | Entitlements Create a subscription entitlement  |
| Events - Get events | Third Party Routing Confirms the successful processing of a number of previously delivered routing events - either there are agents status updates or work objects. We will delete those messages from our queue and they will never be sent to the partner again. All events that are not confirmed will be returned again. This method is a POST so it is not SAFE and it is not IDEMPOTENT. This method modifies data on the Fusion side and  returns different data each time it is called. When the call completes, the partner should call back immediately. The response may be delayed for a max of 30 milliseconds, if no data is available on our side. The response will be sent immediately as data becomes available, regardless of the amount of data. This is a mechanism of minimizing the latency. The username is the one that the customer used to register this partner in the authentication server and it should be sent automatically.<br/> In case of backward incompatible changes, a new method will be added, with a new version number. Get events  |
| Export Activities - Create a bulk export activity | Export Activities Create a bulk export activity  |
| Export Activities/Export Child Object Activities - Create a child bulk export activity | Export Activities/Export Child Object Activities Create a child bulk export activity  |
| Export Activities/Export Child Object Activities/Export Child Object Activities - Create a child bulk export activity | Export Activities/Export Child Object Activities/Export Child Object Activities Create a child bulk export activity  |
| Goals - Create a goal | Goals Create a goal  |
| Goals/Goal History - Create a goal history record | Goals/Goal History Create a goal history record  |
| Goals/Goal Metric - Create a goal | Goals/Goal Metric Create a goal  |
| Goals/Goal Metric/Goal Metric Breakdowns - Create goal metric breakdowns | Goals/Goal Metric/Goal Metric Breakdowns Create goal metric breakdowns  |
| Goals/Goal Metric/Goal Metric Detail - Create a goal metric detail record | Goals/Goal Metric/Goal Metric Detail Create a goal metric detail record  |
| Goals/Goal Participants - Create a goal participant | Goals/Goal Participants Create a goal participant  |
| HCM Service Requests - Create a service request | HCM Service Requests Create a service request  |
| HCM Service Requests/Attachments - Create an attachment | HCM Service Requests/Attachments Create an attachment  |
| HCM Service Requests/Channel Communications - Create a channel communication | HCM Service Requests/Channel Communications Create a channel communication  |
| HCM Service Requests/Contact Members - Create a contact member | HCM Service Requests/Contact Members Create a contact member  |
| HCM Service Requests/Messages - Create a message | HCM Service Requests/Messages Create a message  |
| HCM Service Requests/Messages/Attachments - Create an attachment | HCM Service Requests/Messages/Attachments Create an attachment  |
| HCM Service Requests/Messages/Message Channels - Create a channel communication message | HCM Service Requests/Messages/Message Channels Create a channel communication message  |
| HCM Service Requests/Resources - Create a resource member | HCM Service Requests/Resources Create a resource member  |
| HCM Service Requests/Service Request References - Create a service request reference | HCM Service Requests/Service Request References Create a service request reference  |
| HCM Service Requests/Tags - Create a tag | HCM Service Requests/Tags Create a tag  |
| Households - Create a household | Households Create a household  |
| Households/Additional Identifiers - Create an additional identifier | Households/Additional Identifiers Create an additional identifier  |
| Households/Additional Names - Create an additional name | Households/Additional Names Create an additional name  |
| Households/Addresses - Create an address | Households/Addresses Create an address  |
| Households/Addresses/Address Locales - Create an address locale | Households/Addresses/Address Locales Create an address locale  |
| Households/Addresses/Address Purposes - Create an address purpose | Households/Addresses/Address Purposes Create an address purpose  |
| Households/Attachments - Upload an attachment | Households/Attachments Upload an attachment  |
| Households/Aux Classifications - Create a customer classification | Households/Aux Classifications Create a customer classification  |
| Households/Contact Points - Create a contact point | Households/Contact Points Create a contact point  |
| Households/Notes - Create a note | Households/Notes Create a note  |
| Households/Primary Addresses - Create a primary address | Households/Primary Addresses Create a primary address  |
| Households/Relationships - Create a relationship | Households/Relationships Create a relationship  |
| Households/Sales Account Resources - Create a sales team member | Households/Sales Account Resources Create a sales team member  |
| Households/Source System References - Create a source system reference | Households/Source System References Create a source system reference  |
| Hub Organizations - Create an organization | Hub Organizations Create an organization Create an organization  |
| Hub Organizations - Find duplicates | Hub Organizations Use Data Quality Duplicate Identification to find duplicate hub organizations. The method returns a list of duplicate hub organizations. Find duplicates  |
| Hub Organizations/Additional Identifiers - Create an additional identifier | Hub Organizations/Additional Identifiers Create an additional identifier  |
| Hub Organizations/Additional Names - Create an additional name | Hub Organizations/Additional Names Create an additional name  |
| Hub Organizations/Addresses - Create an address | Hub Organizations/Addresses Create an address  |
| Hub Organizations/Contact Points - Create a contact point | Hub Organizations/Contact Points Create a contact point  |
| Hub Organizations/Customer Classifications - Create a customer classification | Hub Organizations/Customer Classifications Create a customer classification  |
| Hub Organizations/Notes - Create a note | Hub Organizations/Notes Create a note  |
| Hub Organizations/Organization Attachments - Create an attachment | Hub Organizations/Organization Attachments Create an attachment  |
| Hub Organizations/Party Usage Assignments - Create a usage assignment | Hub Organizations/Party Usage Assignments Create a usage assignment  |
| Hub Organizations/Relationships - Create a relationship | Hub Organizations/Relationships Create a relationship  |
| Hub Organizations/Source System References - Create a source system reference | Hub Organizations/Source System References Create a source system reference  |
| Hub Persons - Create a person | Hub Persons Create a person Create a person  |
| Hub Persons - Find duplicates | Hub Persons Use Data Quality Duplicate Identification to find duplicate hub persons. The method returns a list of duplicate hub persons. Find duplicates  |
| Hub Persons/Additional Identifiers - Create an additional identifier | Hub Persons/Additional Identifiers Create an additional identifier  |
| Hub Persons/Additional Names - Create an additional name | Hub Persons/Additional Names Create an additional name  |
| Hub Persons/Addresses - Create an address | Hub Persons/Addresses Create an address  |
| Hub Persons/Attachments - Create an attachment | Hub Persons/Attachments Create an attachment  |
| Hub Persons/Contact Points - Create a contact point | Hub Persons/Contact Points Create a contact point  |
| Hub Persons/Customer Classifications - Create a customer classification | Hub Persons/Customer Classifications Create a customer classification  |
| Hub Persons/Notes - Create a note | Hub Persons/Notes Create a note  |
| Hub Persons/Relationships - Create a relationship | Hub Persons/Relationships Create a relationship  |
| Hub Persons/Source System References - Create a source system reference | Hub Persons/Source System References Create a source system reference  |
| Hub Persons/Usage Assignments - Create a usage assignment | Hub Persons/Usage Assignments Create a usage assignment  |
| Import Activities - Create a file import activity | Import Activities Create a file import activity  |
| Import Activities/Import Activity Data Files - Create a file import activity data file | Import Activities/Import Activity Data Files Create a file import activity data file  |
| Import Activity Maps - Create a file import map | Import Activity Maps Create a file import map  |
| Import Activity Maps/Import Activity Map Columns - Create a file import map column | Import Activity Maps/Import Activity Map Columns Create a file import map column  |
| Inbound Message Filters - Create an inbound message filter | Inbound Message Filters Create an inbound message filter  |
| Inbound Messages - Create an inbound message | Inbound Messages Create an inbound message Create an inbound message  |
| Inbound Messages - Process inbound messages | Inbound Messages Process inbound messages sent by customers. Process inbound messages  |
| Inbound Messages/Attachments - Create an attachment | Inbound Messages/Attachments Create an attachment  |
| Inbound Messages/Inbound Message Parts - Create an inbound message part | Inbound Messages/Inbound Message Parts Create an inbound message part  |
| Incentive Compensation Expressions - Create an expression | Incentive Compensation Expressions Create an expression  |
| Incentive Compensation Expressions/Expression Details - Create an expression detail | Incentive Compensation Expressions/Expression Details Create an expression detail  |
| Incentive Compensation Rule Hierarchies - Create a rule hierarchy | Incentive Compensation Rule Hierarchies Create a rule hierarchy  |
| Incentive Compensation Rule Hierarchies/Qualifying Criteria - Create a qualifying criterion | Incentive Compensation Rule Hierarchies/Qualifying Criteria Create a qualifying criterion  |
| Incentive Compensation Rule Hierarchies/Qualifying Criteria/Qualifying Attribute Values - Create a qualifying attribute value | Incentive Compensation Rule Hierarchies/Qualifying Criteria/Qualifying Attribute Values Create a qualifying attribute value  |
| Incentive Compensation Rule Hierarchies/Rule Assignments - Create a rule assignment | Incentive Compensation Rule Hierarchies/Rule Assignments Create a rule assignment  |
| Incentive Compensation Rule Hierarchies/Rules - Create a rule | Incentive Compensation Rule Hierarchies/Rules Create a rule  |
| Incentive Compensation Transactions - Create a transaction | Incentive Compensation Transactions Create a transaction  |
| Incentive Compensation Transactions/Credits - Create a credit | Incentive Compensation Transactions/Credits Create a credit  |
| Incentive Compensation Transactions/Transaction Descriptive Flex Fields - Create a transaction descriptive flex field | Incentive Compensation Transactions/Transaction Descriptive Flex Fields Create a transaction descriptive flex field  |
| Interactions - Create an interaction | Interactions Create an interaction  |
| Interactions/Child Interactions - Create a child interaction | Interactions/Child Interactions Create a child interaction  |
| Interactions/Interaction Participants - Create an interaction participant | Interactions/Interaction Participants Create an interaction participant  |
| Interactions/Interaction References - Create an interaction reference | Interactions/Interaction References Create an interaction reference  |
| Internal Service Requests - Create a service request | Internal Service Requests Create a service request  |
| Internal Service Requests/Attachments - Create an attachment | Internal Service Requests/Attachments Create an attachment  |
| Internal Service Requests/Channel Communications - Create a channel communication | Internal Service Requests/Channel Communications Create a channel communication  |
| Internal Service Requests/Contact Members - Create a contact member | Internal Service Requests/Contact Members Create a contact member  |
| Internal Service Requests/Messages - Create a message | Internal Service Requests/Messages Create a message  |
| Internal Service Requests/Messages/Attachments - Create an attachment | Internal Service Requests/Messages/Attachments Create an attachment  |
| Internal Service Requests/Messages/Message Channels - Create a channel communication message | Internal Service Requests/Messages/Message Channels Create a channel communication message  |
| Internal Service Requests/Resources - Create a resource member | Internal Service Requests/Resources Create a resource member  |
| Internal Service Requests/Service Request References - Create a service request reference | Internal Service Requests/Service Request References Create a service request reference  |
| Internal Service Requests/Tags - Create a tag | Internal Service Requests/Tags Create a tag  |
| Lightbox Documents - Create a Lightbox document | Lightbox Documents Create a Lightbox document  |
| Lightbox Documents/Document-viewed-outside-Lightbox Signals - Create a document-viewed-outside-Lightbox signal f... | Lightbox Documents/Document-viewed-outside-Lightbox Signals Create a document-viewed-outside-Lightbox signal for a document  |
| Lightbox Documents/Lightbox Document Pages/Page-viewed-outside-Lightbox Signals - Create a page-viewed-outside-Lightbox signal for a... | Lightbox Documents/Lightbox Document Pages/Page-viewed-outside-Lightbox Signals Create a page-viewed-outside-Lightbox signal for a page  |
| Lightbox Documents/Shared-person Instances - Create a shared-person instance for a document | Lightbox Documents/Shared-person Instances Create a shared-person instance for a document  |
| Lightbox Presentation Session Feedback - Create a lightbox presentation session feedback it... | Lightbox Presentation Session Feedback Create a lightbox presentation session feedback item  |
| Lightbox Presentation Sessions - Create a Lightbox presentation session | Lightbox Presentation Sessions Create a Lightbox presentation session  |
| Lightbox Presentation Sessions/Presentation Session Attendance Events - Create a Lightbox presentation session attendance ... | Lightbox Presentation Sessions/Presentation Session Attendance Events Create a Lightbox presentation session attendance event  |
| Lightbox Presentation Sessions/Presentation Session Display Events - Create a Lightbox presentation session display eve... | Lightbox Presentation Sessions/Presentation Session Display Events Create a Lightbox presentation session display event  |
| List of Values/Action Status Conditions - Create a status condition | List of Values/Action Status Conditions Create a status condition  |
| List of Values/Collaboration Recipients - Create a collaboration recipient | List of Values/Collaboration Recipients Create a collaboration recipient  |
| List of Values/Dynamic Link Values - Create a reference | List of Values/Dynamic Link Values Create a reference  |
| List of Values/KPI - Create a KPI | List of Values/KPI Create a KPI  |
| List of Values/KPI/KPI History Metadata - Create a KPI history metadata | List of Values/KPI/KPI History Metadata Create a KPI history metadata  |
| List of Values/Tags - Create a tag | List of Values/Tags Create a tag  |
| MDF Requests - Create an MDF request | MDF Requests Create an MDF request  |
| MDF Requests/Claims - Create a claim | MDF Requests/Claims Create a claim  |
| MDF Requests/MDF Request Teams - Create a fund request resource | MDF Requests/MDF Request Teams Create a fund request resource  |
| MDF Requests/Notes - Create a note | MDF Requests/Notes Create a note  |
| Milestones - Create a milestone | Milestones Create a milestone  |
| Multi Channel Adapter Events - Create an event | Multi Channel Adapter Events Create an event Create an event  |
| Multi Channel Adapter Events - Fetch toolbar configuration | Multi Channel Adapter Events Fetches the toolbar configuration for the current user used to process multi-channel adapter events.  Information being fetched includes values for debugMode, faTrustToken, AGENT_CONFIG_DATA, agentPartyId, agentId, profileMcaDisableJwt, NLS_LANG, and NLS_LOCALE Fetch toolbar configuration  |
| Multi Channel Adapter Events - Process a multi-channel adapter event | Multi Channel Adapter Events This action accepts a jsonString with the necessary data to process the event request which should include the input tokens. The response of the processed event will include the output tokens and list of applicable actions to take on the client side. Process a multi-channel adapter event  |
| Multi-Channel Adapter Toolbars - Create all toolbars | Multi-Channel Adapter Toolbars Create all toolbars  |
| Multi-Channel Adapter Toolbars/Multi-Channel Adapter Toolbar Additions - Create all toolbar additions | Multi-Channel Adapter Toolbars/Multi-Channel Adapter Toolbar Additions Create all toolbar additions  |
| My Self-Service Roles - Create a self-service role | My Self-Service Roles Create a self-service role  |
| Non-Duplicate Records - Create a non-duplicate record | Non-Duplicate Records Create a non-duplicate record  |
| Object Capacities - Create an object capacity | Object Capacities Create an object capacity  |
| Objectives - Create an objective | Objectives Create an objective  |
| Objectives/Objective Splits - Create an objective split | Objectives/Objective Splits Create an objective split  |
| Omnichannel Properties - Create an omnichannel property | Omnichannel Properties Create an omnichannel property  |
| Opportunities - Assign matched opportunity | Opportunities Assigns a matched opportunity. Assign matched opportunity  |
| Opportunities - Copy matched opportunity | Opportunities Copies a matched opportunity. Copy matched opportunity  |
| Opportunities - Create an opportunity | Opportunities Create an opportunity Create an opportunity  |
| Opportunities/Assessments - Create an assessment | Opportunities/Assessments Create an assessment  |
| Opportunities/Notes - Create a note | Opportunities/Notes Create a note  |
| Opportunities/Opportunity Competitors - Create an opportunity competitor | Opportunities/Opportunity Competitors Create an opportunity competitor  |
| Opportunities/Opportunity Contacts - Create an opportunity contact | Opportunities/Opportunity Contacts Create an opportunity contact  |
| Opportunities/Opportunity Leads - Create an opportunity lead | Opportunities/Opportunity Leads Create an opportunity lead  |
| Opportunities/Opportunity Partners - Create an opportunity revenue partner | Opportunities/Opportunity Partners Create an opportunity revenue partner  |
| Opportunities/Opportunity Sources - Create an opportunity source | Opportunities/Opportunity Sources Create an opportunity source  |
| Opportunities/Opportunity Team Members - Create an opportunity team member | Opportunities/Opportunity Team Members Create an opportunity team member  |
| Opportunities/Revenue Items - Create an opportunity revenue | Opportunities/Revenue Items Create an opportunity revenue  |
| Opportunities/Revenue Items/Child Split Revenues - Create a child split revenue | Opportunities/Revenue Items/Child Split Revenues Create a child split revenue  |
| Opportunities/Revenue Items/Opportunity Revenue Territories - Create a territory for an opportunity revenue | Opportunities/Revenue Items/Opportunity Revenue Territories Create a territory for an opportunity revenue  |
| Opportunities/Revenue Items/Recurring Revenues - Create a recurring revenue | Opportunities/Revenue Items/Recurring Revenues Create a recurring revenue  |
| Outbound Messages - Create an outbound message | Outbound Messages Create an outbound message Create an outbound message  |
| Outbound Messages - Create an outbound message for a business object | Outbound Messages Create an outbound message and outbound message recipients Create an outbound message for a business object  |
| Outbound Messages/Outbound Message Parts - Create an outbound message parts | Outbound Messages/Outbound Message Parts Create an outbound message parts  |
| Participant Compensation Plans/Plan Components/Performance Measures/Scorecards/Scorecard Rates - Create a scorecard rate | Participant Compensation Plans/Plan Components/Performance Measures/Scorecards/Scorecard Rates Create a scorecard rate  |
| Participant Compensation Plans/Plan Components/Rate Tables/Rate Table Rates - Create a rate table rate | Participant Compensation Plans/Plan Components/Rate Tables/Rate Table Rates Create a rate table rate  |
| Participants/Participant Details - Create a participant detail | Participants/Participant Details Create a participant detail  |
| Participants/Participant Details/Participant Details Descriptive Flex Fields - Create a descriptive flex field | Participants/Participant Details/Participant Details Descriptive Flex Fields Create a descriptive flex field  |
| Participants/Participant Roles - Create a participant role | Participants/Participant Roles Create a participant role  |
| Participants/Participants Descriptive Flex Fields - Create a descriptive flex field | Participants/Participants Descriptive Flex Fields Create a descriptive flex field  |
| Partner Contacts - Create a partner contact | Partner Contacts Create a partner contact  |
| Partner Contacts/Attachments - Create an attachment | Partner Contacts/Attachments Create an attachment  |
| Partner Contacts/Partner Contact User Details - Create an user detail | Partner Contacts/Partner Contact User Details Create an user detail  |
| Partner Programs - Create a partner program | Partner Programs Create a partner program  |
| Partner Programs/Countries - Create a country for a partner program | Partner Programs/Countries Create a country for a partner program  |
| Partner Programs/Program Benefit Details - Create a program benefit for a partner program | Partner Programs/Program Benefit Details Create a program benefit for a partner program  |
| Partner Programs/Tiers - Create a tier for a partner program | Partner Programs/Tiers Create a tier for a partner program  |
| Partner Tiers - Create a partner tier | Partner Tiers Create a partner tier  |
| Partners - Create a partner | Partners Create a partner  |
| Partners/Addresses - Create an address | Partners/Addresses Create an address  |
| Partners/Attachments - Create a partner attachment | Partners/Attachments Create a partner attachment  |
| Partners/Expertises - Create an expertise for a partner | Partners/Expertises Create an expertise for a partner  |
| Partners/Focus Areas - Create a product specialty for a partner | Partners/Focus Areas Create a product specialty for a partner  |
| Partners/Geographies - Create a geography for a partner | Partners/Geographies Create a geography for a partner  |
| Partners/Industries - Create an industry for a partner | Partners/Industries Create an industry for a partner  |
| Partners/Notes - Create a note for a partner | Partners/Notes Create a note for a partner  |
| Partners/Partner Account Team Members - Create a partner account team member for a partner | Partners/Partner Account Team Members Create a partner account team member for a partner  |
| Partners/Partner Certifications - Create a certification for a partner | Partners/Partner Certifications Create a certification for a partner  |
| Partners/Partner Contacts - Create a partner contact | Partners/Partner Contacts Create a partner contact  |
| Partners/Partner Contacts/Attachments - Create a partner attachment | Partners/Partner Contacts/Attachments Create a partner attachment  |
| Partners/Partner Contacts/User Account Details - Create a partner contact user detail | Partners/Partner Contacts/User Account Details Create a partner contact user detail  |
| Partners/Partner Types - Create a partner type | Partners/Partner Types Create a partner type  |
| Pay Groups - Create a pay group | Pay Groups Create a pay group  |
| Pay Groups/Pay Group Assignments - Create a pay group assignments | Pay Groups/Pay Group Assignments Create a pay group assignments  |
| Pay Groups/Pay Group Roles - Create a pay group role | Pay Groups/Pay Group Roles Create a pay group role  |
| Payment Batches - Create a payment batch | Payment Batches Create a payment batch  |
| Payment Batches/Payment Transactions - Create a manual adjustment | Payment Batches/Payment Transactions Create a manual adjustment  |
| Payment Batches/Paysheets/Payment Transactions - Create a manual adjustment | Payment Batches/Paysheets/Payment Transactions Create a manual adjustment  |
| Payment Plans - Create a payment plan | Payment Plans Create a payment plan  |
| Payment Plans/Payment Plans Assignments - Create a payment plan assignment | Payment Plans/Payment Plans Assignments Create a payment plan assignment  |
| Payment Plans/Payment Plans Roles - Create a payment plan role | Payment Plans/Payment Plans Roles Create a payment plan role  |
| Payment Transactions - Create a manual adjustment | Payment Transactions Create a manual adjustment  |
| Paysheets/Payment Transactions - Create a manual adjustment for a paysheet | Paysheets/Payment Transactions Create a manual adjustment for a paysheet  |
| Performance Measures - Create a performance measure | Performance Measures Create a performance measure  |
| Performance Measures/Credit Categories - Create a credit category | Performance Measures/Credit Categories Create a credit category  |
| Performance Measures/Credit Categories/Credit Factors - Create a credit factor | Performance Measures/Credit Categories/Credit Factors Create a credit factor  |
| Performance Measures/Credit Categories/Transaction Factors - Create a transaction factor | Performance Measures/Credit Categories/Transaction Factors Create a transaction factor  |
| Performance Measures/Descriptive Flex fields - Create a descriptive flex field | Performance Measures/Descriptive Flex fields Create a descriptive flex field  |
| Performance Measures/Scorecards - Create a scorecard | Performance Measures/Scorecards Create a scorecard  |
| Phone Verifications - Create a phone verification | Phone Verifications Create a phone verification  |
| Plan Components - Create a plan component | Plan Components Create a plan component  |
| Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Tables - Create a rate table | Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Tables Create a rate table  |
| Plan Components/Plan Component - Performance Measures - Create a performance measure | Plan Components/Plan Component - Performance Measures Create a performance measure  |
| Plan Components/Plan Component Descriptive Flex Fields - Create a descriptive flex field | Plan Components/Plan Component Descriptive Flex Fields Create a descriptive flex field  |
| Price Book Headers - Create a pricebook | Price Book Headers Create a pricebook  |
| Price Book Headers/Price Book Items - Create a pricebook item | Price Book Headers/Price Book Items Create a pricebook item  |
| Process Metadatas - Create a process metadata | Process Metadatas Create a process metadata  |
| Product Groups - Create a product group | Product Groups Create a product group Create a product group  |
| Product Groups - Lock a product group for editing | Product Groups The action locks a product group for editing, and sends the product group identifier in request body. Lock a product group for editing  |
| Product Groups - Publish product groups locked or edited by the use... | Product Groups The action publishes a list of product groups that have been locked or edited by the user. Publish product groups locked or edited by the user  |
| Product Groups/Attachments - Upload an attachment to the product group | Product Groups/Attachments Upload an attachment to the product group  |
| Product Groups/Filter Attributes - Create an attribute for the product group | Product Groups/Filter Attributes Create an attribute for the product group  |
| Product Groups/Filter Attributes/Filter Attribute Values - Create an attribute value | Product Groups/Filter Attributes/Filter Attribute Values Create an attribute value  |
| Product Groups/Products - Create a product for the product group | Product Groups/Products Create a product for the product group  |
| Product Groups/Products/EligibilityRules - Create an eligibility rule | Product Groups/Products/EligibilityRules Create an eligibility rule  |
| Product Groups/Related Groups - Create a subgroup relationship | Product Groups/Related Groups Create a subgroup relationship  |
| Product Groups/Subgroups - Create a subgroup | Product Groups/Subgroups Create a subgroup  |
| Products - Create a product | Products Create a product  |
| Products/Default Prices - Create a default price | Products/Default Prices Create a default price  |
| Products/Product Attachments - Create an attachment for a product | Products/Product Attachments Create an attachment for a product  |
| Products/Product Image Attachments - Create an image attachment for a product | Products/Product Image Attachments Create an image attachment for a product  |
| Program Benefits - Create a program benefit | Program Benefits Create a program benefit  |
| Program Benefits/Benefit List Values - Create a benefit list type value | Program Benefits/Benefit List Values Create a benefit list type value  |
| Program Enrollments - Create an enrollment | Program Enrollments Create an enrollment  |
| Program Enrollments/Notes - Create a note for an enrollment | Program Enrollments/Notes Create a note for an enrollment  |
| ProposeAgents - Propose agents for work | Third Party Routing Gets agent proposals for work items. Receives a list of work object identifier and agent identifier pairs, each pair representing an agent proposal for a work item. These agent proposals will then be handled as if coming from the internal routing system. This method modifies data on the Fusion side. In case of backward incompatible changes, a new method will be added, with a new version number. The username is the one that the customer used to register this partner in the authentication server and it should be sent automatically. Propose agents for work  |
| Queues - Create a queue | Queues Create a queue  |
| Queues/Overflow Queues - Create an overflow queue | Queues/Overflow Queues Create an overflow queue  |
| Queues/Queue Resource Members - Create a resource member | Queues/Queue Resource Members Create a resource member  |
| Queues/Queue Resource Teams - Create a resource team | Queues/Queue Resource Teams Create a resource team  |
| Quote and Order Lines - Create a sales order line | Quote and Order Lines Create a sales order line  |
| Rate Dimensions - Create a rate dimension | Rate Dimensions Create a rate dimension  |
| Rate Dimensions/Rate Dimension - Tiers - Create a rate dimension tier | Rate Dimensions/Rate Dimension - Tiers Create a rate dimension tier  |
| Rate Tables - Create a rate table | Rate Tables Create a rate table  |
| Rate Tables/Rate Table Dimensions - Create a rate dimension association with a rate ta... | Rate Tables/Rate Table Dimensions Create a rate dimension association with a rate table  |
| Rate Tables/Rate Table Rates - Create a rate table rate | Rate Tables/Rate Table Rates Create a rate table rate  |
| Register - Register partner | Third Party Routing Provides a mechanism to register a new Third Party Routing system. You only need to call this method once. The provided name will be associated with your login username and will show up in the Fusion Application Queue edit page in order to be associated with externally routed queues. Register partner  |
| Resolution Links - Create a link | Resolution Links Create a link  |
| Resolution Links/Link Members - Create a link member for the link | Resolution Links/Link Members Create a link member for the link  |
| Resolution Requests - Create a resolution request | Resolution Requests Create a resolution request  |
| Resolution Requests/Duplicate Parties - Create a duplicate party | Resolution Requests/Duplicate Parties Create a duplicate party  |
| Resolution Requests/Resolution Details - Create a detailed record for a duplicate party res... | Resolution Requests/Resolution Details Create a detailed record for a duplicate party resolution  |
| Resource Capacities - Create a resource capacity | Resource Capacities Create a resource capacity  |
| Resource Users - Create a resource user | Resource Users Create a resource user  |
| Roles - Create an incentive compensation role | Roles Create an incentive compensation role  |
| Roles/Role - Participants - Create a participant for a role | Roles/Role - Participants Create a participant for a role  |
| Sales Leads - Accept Lead | Sales Leads Accept a lead. Accept Lead  |
| Sales Leads - Create a sales lead | Sales Leads Create a sales lead Create a sales lead  |
| Sales Leads - Rank Lead | Sales Leads Rank a lead. Rank Lead  |
| Sales Leads - Reject Lead | Sales Leads Reject a lead. Reject Lead  |
| Sales Leads - Score Lead | Sales Leads Score a lead. Score Lead  |
| Sales Leads/Lead Qualifications - Create a lead qualification | Sales Leads/Lead Qualifications Create a lead qualification  |
| Sales Leads/Notes - Create a note | Sales Leads/Notes Create a note  |
| Sales Leads/Product Groups - Set product usage code | Sales Leads/Product Groups Sets the usage code for the product. Set product usage code  |
| Sales Leads/Product Groups - Set product usage mode | Sales Leads/Product Groups Sets the usage mode for the product. Set product usage mode  |
| Sales Leads/Products - Set product usage code | Sales Leads/Products Sets the usage code for the product. Set product usage code  |
| Sales Leads/Products - Set product usage mode | Sales Leads/Products Sets the usage mode for the product. Set product usage mode  |
| Sales Leads/Sales Lead Contacts - Create a sales lead contact | Sales Leads/Sales Lead Contacts Create a sales lead contact  |
| Sales Leads/Sales Lead Products - Create a sales lead product | Sales Leads/Sales Lead Products Create a sales lead product  |
| Sales Leads/Sales Lead Resources - Create a sales lead resource | Sales Leads/Sales Lead Resources Create a sales lead resource  |
| Sales Orders - Create a quote | Sales Orders Create a quote  |
| Sales Orders/Quote and Order Lines - Create a sales order line | Sales Orders/Quote and Order Lines Create a sales order line  |
| Sales Promotions - Create a sales promotion | Sales Promotions Create a sales promotion  |
| Sales Territories - Create a territory | Sales Territories Create a territory  |
| Sales Territories/Line of Business - Create a line of business for a territory | Sales Territories/Line of Business Create a line of business for a territory  |
| Sales Territories/Resources - Create a resource for a territory | Sales Territories/Resources Create a resource for a territory  |
| Sales Territories/Rules - Create a rule for a territory | Sales Territories/Rules Create a rule for a territory  |
| Sales Territories/Rules/Rule Boundaries - Create a rule boundary for a territory | Sales Territories/Rules/Rule Boundaries Create a rule boundary for a territory  |
| Sales Territories/Rules/Rule Boundaries/Rule Boundary Values - Create a rule boundary value for a territory | Sales Territories/Rules/Rule Boundaries/Rule Boundary Values Create a rule boundary value for a territory  |
| Sales Territory Proposals - Create a proposal | Sales Territory Proposals Create a proposal  |
| Self-Service Registrations - Create a self-service registration request | Self-Service Registrations Create a self-service registration request  |
| Self-Service Roles - Create a role for a self-service user | Self-Service Roles Create a role for a self-service user  |
| Self-Service Users/Self-Service Roles - Create a self-service role | Self-Service Users/Self-Service Roles Create a self-service role  |
| Self-Service Users/Self-Service Roles - Create a self-service role | Self-Service Users/Self-Service Roles Create a self-service role  |
| Service Providers - Create a service provider | Service Providers Create a service provider  |
| Service Providers/Services - Create a service | Service Providers/Services Create a service  |
| Service Requests - Create a service request | Service Requests Create a service request  |
| Service Requests/Activities - Create an activity | Service Requests/Activities Create an activity  |
| Service Requests/Attachments - Create an attachment | Service Requests/Attachments Create an attachment  |
| Service Requests/Channel Communications - Create a channel communication | Service Requests/Channel Communications Create a channel communication  |
| Service Requests/Contact Members - Create a contact | Service Requests/Contact Members Create a contact  |
| Service Requests/Messages - Create a message | Service Requests/Messages Create a message  |
| Service Requests/Messages/Attachments - Create an attachment | Service Requests/Messages/Attachments Create an attachment  |
| Service Requests/Messages/Channel Communications - Create a channel communication | Service Requests/Messages/Channel Communications Create a channel communication  |
| Service Requests/Resources - Create a resource member | Service Requests/Resources Create a resource member  |
| Service Requests/Service Request References - Create a service request reference | Service Requests/Service Request References Create a service request reference  |
| Service Requests/Tags - Create a tag | Service Requests/Tags Create a tag  |
| Setup Assistants/Accounting Calendars - Create an accounting calendar | Setup Assistants/Accounting Calendars Create an accounting calendar  |
| Setup Assistants/Company Profiles - Create a company profile | Setup Assistants/Company Profiles Create a company profile  |
| Setup Assistants/Competitors - Create a competitor | Setup Assistants/Competitors Create a competitor  |
| Setup Assistants/Geographies - Create geographies setup data | Setup Assistants/Geographies Create geographies setup data  |
| Setup Assistants/Opportunities - Create opportunities setup data | Setup Assistants/Opportunities Create opportunities setup data  |
| Setup Assistants/Opportunities/Sales Stages - Create sales stages for the selected sales method | Setup Assistants/Opportunities/Sales Stages Create sales stages for the selected sales method  |
| Setup Assistants/Product Groups - Create product group setup details | Setup Assistants/Product Groups Create product group setup details  |
| Setup Assistants/Role Mappings - Create resource roles and role mappings | Setup Assistants/Role Mappings Create resource roles and role mappings  |
| Setup Assistants/Sales Forecasts - Create sales forecasting details | Setup Assistants/Sales Forecasts Create sales forecasting details  |
| Setup Assistants/Sales Stages - Create sales stages for the selected sales method | Setup Assistants/Sales Stages Create sales stages for the selected sales method  |
| Setup Assistants/Setup Users - Create setup users details | Setup Assistants/Setup Users Create setup users details  |
| Setup Assistants/Top Sales Users - Create top sales users details | Setup Assistants/Top Sales Users Create top sales users details  |
| Skill Resources - Create a skill resource | Skill Resources Create a skill resource  |
| Skills - Create a skill | Skills Create a skill  |
| Skills/Competencies - Create a competency | Skills/Competencies Create a competency  |
| Skills/Competencies/Competency Values - Create a value | Skills/Competencies/Competency Values Create a value  |
| Smart Text Folders - Create a smart text folder | Smart Text Folders Create a smart text folder  |
| Smart Text Folders/Smart Text Child Folders - Create smart text child folders | Smart Text Folders/Smart Text Child Folders Create smart text child folders  |
| Smart Text User Variables - Create a smart text user variable | Smart Text User Variables Create a smart text user variable  |
| Smart Texts - Create a smart text | Smart Texts Create a smart text Create a smart text  |
| Smart Texts - Get Smart Text Variable Values | Smart Texts This action accepts a jsonString with the primary keys of the Smart Text and the referenced object. The response contains a collection of variables with their derived values and a collection of prompt variables with their respective display names. Get Smart Text Variable Values  |
| Smart Texts - Process smart text variable substitution by the un... | Smart Texts This action accepts a jsonString with the primary keys of the smart text and the referenced object. The action then substitutes the variables in the Smart Text with the data obtained from the referenced object. Alternatively, you can use the PUID instead of the primary key to reference the Smart Text. Process smart text variable substitution by the unique identifier  |
| Social Posts - Create a social post | Social Posts Create a social post  |
| Social Posts/Social Post Tags - Create a social post tag | Social Posts/Social Post Tags Create a social post tag  |
| Social Posts/Social Post URLs - Add a social post URL | Social Posts/Social Post URLs Add a social post URL  |
| Social Users - Create a social user | Social Users Create a social user  |
| Source System References - Create a source system reference | Source System References Create a source system reference  |
| Standard Coverages - Create a standard coverage | Standard Coverages Create a standard coverage  |
| Standard Coverages/Adjustments - Create an adjustment | Standard Coverages/Adjustments Create an adjustment  |
| Subscription Accounts - Create a subscription account | Subscription Accounts Create a subscription account  |
| Subscription Accounts Roles - Create a subscription account role | Subscription Accounts Roles Create a subscription account role  |
| Subscription Accounts/Attachments - Create an attachment | Subscription Accounts/Attachments Create an attachment  |
| Subscription Accounts/Billing Profiles - Create a billing profile | Subscription Accounts/Billing Profiles Create a billing profile  |
| Subscription Accounts/Notes - Create a note | Subscription Accounts/Notes Create a note  |
| Subscription Accounts/Subscription Account Addresses - Create an address | Subscription Accounts/Subscription Account Addresses Create an address  |
| Subscription Accounts/Subscription Account Relationships - Create a subscription account relationship | Subscription Accounts/Subscription Account Relationships Create a subscription account relationship  |
| Subscription Accounts/Subscription Account Roles - Create a subscription account role | Subscription Accounts/Subscription Account Roles Create a subscription account role  |
| Subscription AI Features - Create a product churn feature | Subscription AI Features Create a product churn feature  |
| Subscription Asset Transactions - Create a subscription asset transaction | Subscription Asset Transactions Create a subscription asset transaction  |
| Subscription Asset Transactions/Asset Fulfillment Lines - Create a fulfillment line | Subscription Asset Transactions/Asset Fulfillment Lines Create a fulfillment line  |
| Subscription Asset Transactions/Asset Split Lines - Create a split line | Subscription Asset Transactions/Asset Split Lines Create a split line  |
| Subscription Products - Create a subscription product | Subscription Products Create a subscription product  |
| Subscription Products/Bill Lines - Create a bill line | Subscription Products/Bill Lines Create a bill line  |
| Subscription Products/Bill Lines/Bill Adjustments - Create a bill adjustment | Subscription Products/Bill Lines/Bill Adjustments Create a bill adjustment  |
| Subscription Products/Charges - Create a charge | Subscription Products/Charges Create a charge  |
| Subscription Products/Charges/Adjustments - Create an adjustment | Subscription Products/Charges/Adjustments Create an adjustment  |
| Subscription Products/Charges/Charge Tiers - Create a charge tier | Subscription Products/Charges/Charge Tiers Create a charge tier  |
| Subscription Products/Covered Levels - Create a covered level | Subscription Products/Covered Levels Create a covered level  |
| Subscription Products/Covered Levels/Bill Lines - Create a bill line | Subscription Products/Covered Levels/Bill Lines Create a bill line  |
| Subscription Products/Covered Levels/Bill Lines/Bill Adjustments - Create a bill adjustment | Subscription Products/Covered Levels/Bill Lines/Bill Adjustments Create a bill adjustment  |
| Subscription Products/Covered Levels/Charges - Create a charge | Subscription Products/Covered Levels/Charges Create a charge  |
| Subscription Products/Covered Levels/Charges/Adjustments - Create an adjustment | Subscription Products/Covered Levels/Charges/Adjustments Create an adjustment  |
| Subscription Products/Covered Levels/Charges/Charge Tiers - Create a charge tier | Subscription Products/Covered Levels/Charges/Charge Tiers Create a charge tier  |
| Subscription Products/Covered Levels/Relationships - Create a relationship | Subscription Products/Covered Levels/Relationships Create a relationship  |
| Subscription Products/Credit Cards - Create a credit card | Subscription Products/Credit Cards Create a credit card  |
| Subscription Products/Descriptive Flexfields - Create a descriptive flexfield | Subscription Products/Descriptive Flexfields Create a descriptive flexfield  |
| Subscription Products/Relationships - Create a relationship | Subscription Products/Relationships Create a relationship  |
| Subscription Products/Sales Credits - Create a sales credit | Subscription Products/Sales Credits Create a sales credit  |
| Subscriptions - Create a subscription | Subscriptions Create a subscription  |
| Subscriptions/Credit Cards - Create a credit card | Subscriptions/Credit Cards Create a credit card  |
| Subscriptions/Descriptive Flexfields - Create a descriptive flexfield | Subscriptions/Descriptive Flexfields Create a descriptive flexfield  |
| Subscriptions/Parties - Create subscription parties | Subscriptions/Parties Create subscription parties  |
| Subscriptions/Parties/Contacts - Create subscription contacts | Subscriptions/Parties/Contacts Create subscription contacts  |
| Subscriptions/Products - Create a subscription product | Subscriptions/Products Create a subscription product  |
| Subscriptions/Products/Bill Lines - Create subscription bill lines | Subscriptions/Products/Bill Lines Create subscription bill lines  |
| Subscriptions/Products/Bill Lines/Bill Adjustments - Create subscription bill adjustments | Subscriptions/Products/Bill Lines/Bill Adjustments Create subscription bill adjustments  |
| Subscriptions/Products/Charges - Create subscription charges | Subscriptions/Products/Charges Create subscription charges  |
| Subscriptions/Products/Charges/Adjustments - Create subscription adjustments | Subscriptions/Products/Charges/Adjustments Create subscription adjustments  |
| Subscriptions/Products/Charges/Charge Tiers - Create subscription charge tiers | Subscriptions/Products/Charges/Charge Tiers Create subscription charge tiers  |
| Subscriptions/Products/Covered Levels - Create a covered level | Subscriptions/Products/Covered Levels Create a covered level  |
| Subscriptions/Products/Covered Levels/Bill Lines - Create subscription bill lines | Subscriptions/Products/Covered Levels/Bill Lines Create subscription bill lines  |
| Subscriptions/Products/Covered Levels/Bill Lines/Bill Adjustments - Create subscription bill adjustments | Subscriptions/Products/Covered Levels/Bill Lines/Bill Adjustments Create subscription bill adjustments  |
| Subscriptions/Products/Covered Levels/Charges - Create subscription charges | Subscriptions/Products/Covered Levels/Charges Create subscription charges  |
| Subscriptions/Products/Covered Levels/Charges/Adjustments - Create subscription adjustments | Subscriptions/Products/Covered Levels/Charges/Adjustments Create subscription adjustments  |
| Subscriptions/Products/Covered Levels/Charges/Charge Tiers - Create subscription charge tiers | Subscriptions/Products/Covered Levels/Charges/Charge Tiers Create subscription charge tiers  |
| Subscriptions/Products/Covered Levels/Child Covered Levels - Create a child covered level | Subscriptions/Products/Covered Levels/Child Covered Levels Create a child covered level  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines - Create subscription bill lines | Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines Create subscription bill lines  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines/Bill Adjustments - Create subscription bill adjustments | Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines/Bill Adjustments Create subscription bill adjustments  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges - Create subscription charges | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges Create subscription charges  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Adjustments - Create subscription adjustments | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Adjustments Create subscription adjustments  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Charge Tiers - Create subscription charge tiers | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Charge Tiers Create subscription charge tiers  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Relationships - Create a subscription relationship | Subscriptions/Products/Covered Levels/Child Covered Levels/Relationships Create a subscription relationship  |
| Subscriptions/Products/Covered Levels/Relationships - Create a subscription relationship | Subscriptions/Products/Covered Levels/Relationships Create a subscription relationship  |
| Subscriptions/Products/Credit Cards - Create a credit card | Subscriptions/Products/Credit Cards Create a credit card  |
| Subscriptions/Products/Descriptive Flexfields - Create a descriptive flexfield | Subscriptions/Products/Descriptive Flexfields Create a descriptive flexfield  |
| Subscriptions/Products/Relationships - Create a subscription relationship | Subscriptions/Products/Relationships Create a subscription relationship  |
| Subscriptions/Products/Sales Credits - Create a sales credit | Subscriptions/Products/Sales Credits Create a sales credit  |
| Subscriptions/Sales Credits - Create a sales credit | Subscriptions/Sales Credits Create a sales credit  |
| Subscriptions/Validate Subscriptions - Create a subscription validation | Subscriptions/Validate Subscriptions Create a subscription validation  |
| Survey Configurations - Create a survey configuration | Survey Configurations Create a survey configuration  |
| Survey Configurations/Survey Configuration Attributes - Create a survey configuration attribute | Survey Configurations/Survey Configuration Attributes Create a survey configuration attribute  |
| Surveys - Create a survey | Surveys Create a survey  |
| Surveys/Survey Questions - Create a survey question | Surveys/Survey Questions Create a survey question  |
| Surveys/Survey Questions/Survey Answer Choices - Create a survey answer choice | Surveys/Survey Questions/Survey Answer Choices Create a survey answer choice  |
| Surveys/Survey Requests - Create a survey request | Surveys/Survey Requests Create a survey request  |
| Surveys/Survey Requests/Survey Responses - Create a survey response | Surveys/Survey Requests/Survey Responses Create a survey response  |
| Territories for Sales - Create a territory | Territories for Sales Create a territory  |
| Territories for Sales/Territory Business - Create a territory line of business | Territories for Sales/Territory Business Create a territory line of business  |
| Territories for Sales/Territory Coverages - Create a territory coverage | Territories for Sales/Territory Coverages Create a territory coverage  |
| Territories for Sales/Territory Resources - Create a territory resource | Territories for Sales/Territory Resources Create a territory resource  |
| Work Orders - Create a work order | Work Orders Create a work order  |
| Work Orders/Attachments - Create an attachment | Work Orders/Attachments Create an attachment  |
| Wrap Ups - Create a wrap up | Wrap Ups Create a wrap up  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Access Groups - Delete an access group | Access Groups Delete an access group  |
| Access Groups/Access Group Members - Delete an access group member | Access Groups/Access Group Members Delete an access group member  |
| Accounts - Delete an account | Accounts Delete an account  |
| Accounts/Account Attachments - Remove an attachment | Accounts/Account Attachments Remove an attachment  |
| Accounts/Account Extension Bases - Delete an account extension base | Accounts/Account Extension Bases Delete an account extension base  |
| Accounts/Account Resources - Delete a sales team member | Accounts/Account Resources Delete a sales team member  |
| Accounts/Additional Identifier - Delete an additional identifier | Accounts/Additional Identifier Delete an additional identifier  |
| Accounts/Additional Names - Delete an additional name | Accounts/Additional Names Delete an additional name  |
| Accounts/Addresses - Delete an address | Accounts/Addresses Delete an address  |
| Accounts/Addresses/Address Locales - Delete an address locale | Accounts/Addresses/Address Locales Delete an address locale  |
| Accounts/Addresses/Address Purposes - Delete an address purpose | Accounts/Addresses/Address Purposes Delete an address purpose  |
| Accounts/Aux Classifications - Delete a customer classification | Accounts/Aux Classifications Delete a customer classification  |
| Accounts/Contact Points - Delete a contact point | Accounts/Contact Points Delete a contact point  |
| Accounts/Notes - Delete a note | Accounts/Notes Delete a note  |
| Accounts/Organization Contacts - Delete an account contact | Accounts/Organization Contacts Delete an account contact  |
| Accounts/Primary Addresses - Delete a primary address | Accounts/Primary Addresses Delete a primary address  |
| Accounts/Relationships - Delete a relationship | Accounts/Relationships Delete a relationship  |
| Accounts/Source System References - Delete a source system reference | Accounts/Source System References Delete a source system reference  |
| Action Plans - Delete an Action Plan | Action Plans Delete an Action Plan  |
| Action Plans/Action Plan Actions - Delete an Action Plan Action | Action Plans/Action Plan Actions Delete an Action Plan Action  |
| Action Plans/Action Plan Actions/Action Plan Action Relations - Delete an action plan action relation | Action Plans/Action Plan Actions/Action Plan Action Relations Delete an action plan action relation  |
| Action Templates - Delete an Action Plan Template | Action Templates Delete an Action Plan Template  |
| Action Templates/Template Actions - Delete a Template Action | Action Templates/Template Actions Delete a Template Action  |
| Action Templates/Template Actions/Action Relations - Delete an Action Relation | Action Templates/Template Actions/Action Relations Delete an Action Relation  |
| Actions - Delete an Action | Actions Delete an Action  |
| Actions/Action Attributes - Delete an Action Attribute | Actions/Action Attributes Delete an Action Attribute  |
| Actions/Action Conditions - Delete an Action Condition | Actions/Action Conditions Delete an Action Condition  |
| Activities - Delete an activity | Activities Delete an activity  |
| Activities/Activity Assignees - Delete an activity assignee | Activities/Activity Assignees Delete an activity assignee  |
| Activities/Activity Attachments - Delete an activity attachment | Activities/Activity Attachments Delete an activity attachment  |
| Activities/Activity Contacts - Delete an activity contact | Activities/Activity Contacts Delete an activity contact  |
| Activities/Activity Objectives - Delete an activity objective | Activities/Activity Objectives Delete an activity objective  |
| Activities/Notes - Delete an activity note | Activities/Notes Delete an activity note  |
| Assets - Delete an asset | Assets Delete an asset  |
| Assets/Asset Contacts - Delete an asset contact | Assets/Asset Contacts Delete an asset contact  |
| Assets/Asset Resources - Delete an asset resource | Assets/Asset Resources Delete an asset resource  |
| Assets/Attachments - Delete an attachment | Assets/Attachments Delete an attachment  |
| Attribute Predictions - Delete an attribute prediction | Attribute Predictions Delete an attribute prediction  |
| Billing Adjustments - Delete a billing adjustment | Billing Adjustments Delete a billing adjustment  |
| Billing Adjustments/Billing Adjustment Events - Delete a billing adjustment event | Billing Adjustments/Billing Adjustment Events Delete a billing adjustment event  |
| Budgets - Delete a MDF budget | Budgets Delete a MDF budget  |
| Budgets/Budget Countries - Delete a MDF budget country | Budgets/Budget Countries Delete a MDF budget country  |
| Budgets/Claims - Delete a MDF claim associated to the budget | Budgets/Claims Delete a MDF claim associated to the budget  |
| Budgets/Fund Requests - Delete a MDF request associated to the budget | Budgets/Fund Requests Delete a MDF request associated to the budget  |
| Budgets/MDF Budget Teams - Delete a MDF budget team member | Budgets/MDF Budget Teams Delete a MDF budget team member  |
| Budgets/Notes - Delete a MDF budget note | Budgets/Notes Delete a MDF budget note  |
| Business Plans - Delete a business plan | Business Plans Delete a business plan  |
| Business Plans/Business Plan Resources - Delete a business plan resource | Business Plans/Business Plan Resources Delete a business plan resource  |
| Business Plans/Notes - Delete a business plan note | Business Plans/Notes Delete a business plan note  |
| Business Plans/SWOTs - Delete a business plan SWOT | Business Plans/SWOTs Delete a business plan SWOT  |
| Calculation Simulations - Delete a calculation simulation | Calculation Simulations Delete a calculation simulation  |
| Calculation Simulations/Simulation Transactions - Delete a simulation transaction | Calculation Simulations/Simulation Transactions Delete a simulation transaction  |
| Calculation Simulations/Simulation Transactions/Simulation Credits - Delete a simulation credit | Calculation Simulations/Simulation Transactions/Simulation Credits Delete a simulation credit  |
| Campaign Members - Delete a campaign member | Campaign Members Delete a campaign member  |
| Campaigns - Delete a campaign | Campaigns Delete a campaign  |
| Cases - Delete a case | Cases Delete a case  |
| Cases/Case Contacts - Delete a case contact | Cases/Case Contacts Delete a case contact  |
| Cases/Case Households - Delete a case household | Cases/Case Households Delete a case household  |
| Cases/Case Messages - Delete a case message | Cases/Case Messages Delete a case message  |
| Cases/Case Opportunities - Delete an opportunity | Cases/Case Opportunities Delete an opportunity  |
| Cases/Case Resources - Delete a case resource | Cases/Case Resources Delete a case resource  |
| Catalog Product Groups - Delete a product group (Not Supported) | Catalog Product Groups Delete a product group (Not Supported)  |
| Catalog Product Groups/Attachments - Delete an attachment | Catalog Product Groups/Attachments Delete an attachment  |
| Catalog Product Groups/Translations - Delete a product group translation (Not Supported) | Catalog Product Groups/Translations Delete a product group translation (Not Supported)  |
| Catalog Products Items - Delete a product item (Not Supported) | Catalog Products Items Delete a product item (Not Supported)  |
| Catalog Products Items/Attachments - Delete an attachment | Catalog Products Items/Attachments Delete an attachment  |
| Catalog Products Items/Item Translation - Delete an item translation (Not Supported) | Catalog Products Items/Item Translation Delete an item translation (Not Supported)  |
| Categories - Delete a category | Categories Delete a category  |
| Channels - Delete a channel | Channels Delete a channel  |
| Channels/Channel Resources - Delete a channel resource member | Channels/Channel Resources Delete a channel resource member  |
| Channels/Sender Identification Priorities - Delete a sender  priority sequence | Channels/Sender Identification Priorities Delete a sender  priority sequence  |
| Chat Interactions - Delete a chat interaction (Not supported) | Chat Interactions Delete a chat interaction (Not supported)  |
| Chat Interactions/Transcripts - Delete a transcript (Not supported) | Chat Interactions/Transcripts Delete a transcript (Not supported)  |
| Claims - Delete an MDF claim | Claims Delete an MDF claim  |
| Claims/Claim Settlements - Delete a claim settlement | Claims/Claim Settlements Delete a claim settlement  |
| Claims/Claim Team Members - Delete a claim team member | Claims/Claim Team Members Delete a claim team member  |
| Claims/Notes - Delete a note | Claims/Notes Delete a note  |
| Collaboration Actions - Delete a collaboration action | Collaboration Actions Delete a collaboration action  |
| Collaboration Actions/Action Attributes - Delete a collaboration action attribute | Collaboration Actions/Action Attributes Delete a collaboration action attribute  |
| Compensation Plans - Delete a compensation plan | Compensation Plans Delete a compensation plan  |
| Compensation Plans/Compensation Plan - Plan Components - Delete a plan component | Compensation Plans/Compensation Plan - Plan Components Delete a plan component  |
| Compensation Plans/Compensation Plan - Roles - Delete a role | Compensation Plans/Compensation Plan - Roles Delete a role  |
| Compensation Plans/Direct Assignment Requests - Delete a direct assignment request | Compensation Plans/Direct Assignment Requests Delete a direct assignment request  |
| Competencies - Delete a competency | Competencies Delete a competency  |
| Contacts - Delete a contact | Contacts Delete a contact  |
| Contacts/Additional Identifiers - Delete an additional identifier | Contacts/Additional Identifiers Delete an additional identifier  |
| Contacts/Additional Names - Delete an additional name | Contacts/Additional Names Delete an additional name  |
| Contacts/Attachments - Remove a contact's picture | Contacts/Attachments Remove a contact's picture  |
| Contacts/Aux Classifications - Delete a customer classification | Contacts/Aux Classifications Delete a customer classification  |
| Contacts/Contact Addresses - Delete an address | Contacts/Contact Addresses Delete an address  |
| Contacts/Contact Addresses/Address Locales - Delete an address locale | Contacts/Contact Addresses/Address Locales Delete an address locale  |
| Contacts/Contact Addresses/Contact Address Purposes - Delete an address purpose | Contacts/Contact Addresses/Contact Address Purposes Delete an address purpose  |
| Contacts/Contact Attachments - Remove an attachment | Contacts/Contact Attachments Remove an attachment  |
| Contacts/Contact Points - Delete a contact point | Contacts/Contact Points Delete a contact point  |
| Contacts/Contact Primary Addresses - Delete a primary address | Contacts/Contact Primary Addresses Delete a primary address  |
| Contacts/Notes - Delete a note | Contacts/Notes Delete a note  |
| Contacts/Relationships - Delete a relationship | Contacts/Relationships Delete a relationship  |
| Contacts/Sales Account Resources - Delete a sales team member | Contacts/Sales Account Resources Delete a sales team member  |
| Contacts/Source System References - Delete a source system reference | Contacts/Source System References Delete a source system reference  |
| Contests - Delete a contest | Contests Delete a contest  |
| Contests/Contest History - Delete a contest history record | Contests/Contest History Delete a contest history record  |
| Contests/Contest Resources - Delete a contest participant | Contests/Contest Resources Delete a contest participant  |
| Contests/Contest Scores - Delete a contest score | Contests/Contest Scores Delete a contest score  |
| Contracts - Delete a contract | Contracts Delete a contract  |
| Contracts/Bill Plans/Job Assignment Overrides - Delete a job assignment override | Contracts/Bill Plans/Job Assignment Overrides Delete a job assignment override  |
| Contracts/Bill Plans/Job Rate Overrides - Delete a job rate override | Contracts/Bill Plans/Job Rate Overrides Delete a job rate override  |
| Contracts/Bill Plans/Job Title Overrides - Delete a job title override | Contracts/Bill Plans/Job Title Overrides Delete a job title override  |
| Contracts/Bill Plans/Labor Multiplier Overrides - Delete a labor multiplier override | Contracts/Bill Plans/Labor Multiplier Overrides Delete a labor multiplier override  |
| Contracts/Bill Plans/Non Labor Rate Overrides - Delete a non labor rate override | Contracts/Bill Plans/Non Labor Rate Overrides Delete a non labor rate override  |
| Contracts/Bill Plans/Person Rate Overrides - Delete a person rate override | Contracts/Bill Plans/Person Rate Overrides Delete a person rate override  |
| Contracts/Billing Controls - Delete a billing control | Contracts/Billing Controls Delete a billing control  |
| Contracts/Contract Documents - Delete a contract document | Contracts/Contract Documents Delete a contract document  |
| Contracts/Contract Header Flexfields - Delete a contract header flexfield | Contracts/Contract Header Flexfields Delete a contract header flexfield  |
| Contracts/Contract Lines - Delete a contract line | Contracts/Contract Lines Delete a contract line  |
| Contracts/Contract Lines/Associated Projects - Delete an associated project | Contracts/Contract Lines/Associated Projects Delete an associated project  |
| Contracts/Contract Lines/Billing Controls - Delete a billing control | Contracts/Contract Lines/Billing Controls Delete a billing control  |
| Contracts/Contract Lines/Contract Line Flexfields - Delete a contract line flexfield | Contracts/Contract Lines/Contract Line Flexfields Delete a contract line flexfield  |
| Contracts/Contract Lines/Sales Credits - Delete a sales credit | Contracts/Contract Lines/Sales Credits Delete a sales credit  |
| Contracts/Contract Parties - Delete a contract party | Contracts/Contract Parties Delete a contract party  |
| Contracts/Contract Parties/Contract Party Contacts - Delete a contract party contact | Contracts/Contract Parties/Contract Party Contacts Delete a contract party contact  |
| Contracts/Contract Parties/Contract Party Contacts/Contract Party Contact Flexfields - Delete a contract party contact flexfield | Contracts/Contract Parties/Contract Party Contacts/Contract Party Contact Flexfields Delete a contract party contact flexfield  |
| Contracts/Contract Parties/Contract Party Flexfields - Delete a contract party flexfield | Contracts/Contract Parties/Contract Party Flexfields Delete a contract party flexfield  |
| Contracts/Related Documents - Delete a related document | Contracts/Related Documents Delete a related document  |
| Contracts/Revenue Plans - Delete a revenue plan | Contracts/Revenue Plans Delete a revenue plan  |
| Contracts/Revenue Plans/Job Assignment Overrides - Delete a job assignment override | Contracts/Revenue Plans/Job Assignment Overrides Delete a job assignment override  |
| Contracts/Revenue Plans/Job Rate Overrides - Delete a job rate override | Contracts/Revenue Plans/Job Rate Overrides Delete a job rate override  |
| Contracts/Revenue Plans/Labor Multiplier Overrides - Delete a labor multiplier override | Contracts/Revenue Plans/Labor Multiplier Overrides Delete a labor multiplier override  |
| Contracts/Revenue Plans/Non Labor Rate Overrides - Delete a non labor rate override | Contracts/Revenue Plans/Non Labor Rate Overrides Delete a non labor rate override  |
| Contracts/Revenue Plans/Person Rate Overrides - Delete a person rate override | Contracts/Revenue Plans/Person Rate Overrides Delete a person rate override  |
| Contracts/Sales Credits - Delete a sales credit | Contracts/Sales Credits Delete a sales credit  |
| Contracts/Supporting Documents - Delete a supporting document | Contracts/Supporting Documents Delete a supporting document  |
| Conversation Messages - Delete a conversation message | Conversation Messages Delete a conversation message  |
| Conversation Messages/Attachments - Delete an attachment | Conversation Messages/Attachments Delete an attachment  |
| Conversation Messages/Conversation Message Recipients - Delete a conversation message recipient | Conversation Messages/Conversation Message Recipients Delete a conversation message recipient  |
| Conversations - Delete a conversation | Conversations Delete a conversation  |
| Conversations/Conversation References - Delete a conversation reference | Conversations/Conversation References Delete a conversation reference  |
| Credit Categories - Delete a credit category | Credit Categories Delete a credit category  |
| DaaS Smart Data - Delete smart data | DaaS Smart Data Delete smart data  |
| Deal Registrations/Deal Products - Delete a deal product | Deal Registrations/Deal Products Delete a deal product  |
| Deal Registrations/Deal Team Members - Delete a deal team member | Deal Registrations/Deal Team Members Delete a deal team member  |
| Deal Registrations/Notes - Delete a note | Deal Registrations/Notes Delete a note  |
| Device Tokens - Delete a device token | Device Tokens Delete a device token  |
| Dynamic Link Patterns - Delete a dynamic link pattern | Dynamic Link Patterns Delete a dynamic link pattern  |
| Email Verifications - Delete an email verification | Email Verifications Delete an email verification  |
| Goals - Delete a goal | Goals Delete a goal  |
| Goals/Goal History - Delete a goal history record | Goals/Goal History Delete a goal history record  |
| Goals/Goal Metric - Get a Goal Metric | Goals/Goal Metric Get a Goal Metric  |
| Goals/Goal Metric/Goal Metric Breakdowns - Delete a goal metric breakdown record | Goals/Goal Metric/Goal Metric Breakdowns Delete a goal metric breakdown record  |
| Goals/Goal Metric/Goal Metric Detail - Delete a goal metric detail record | Goals/Goal Metric/Goal Metric Detail Delete a goal metric detail record  |
| Goals/Goal Participants - Delete a goal participant | Goals/Goal Participants Delete a goal participant  |
| HCM Service Requests - Delete a service request | HCM Service Requests Delete a service request  |
| HCM Service Requests/Attachments - Delete an attachment | HCM Service Requests/Attachments Delete an attachment  |
| HCM Service Requests/Channel Communications - Delete a channel communication | HCM Service Requests/Channel Communications Delete a channel communication  |
| HCM Service Requests/Contact Members - Delete a contact member | HCM Service Requests/Contact Members Delete a contact member  |
| HCM Service Requests/Messages - Delete a message | HCM Service Requests/Messages Delete a message  |
| HCM Service Requests/Messages/Attachments - Delete an attachment | HCM Service Requests/Messages/Attachments Delete an attachment  |
| HCM Service Requests/Messages/Message Channels - Delete a channel communication message | HCM Service Requests/Messages/Message Channels Delete a channel communication message  |
| HCM Service Requests/Resources - Delete a resource member | HCM Service Requests/Resources Delete a resource member  |
| HCM Service Requests/Service Request References - Delete a service request reference | HCM Service Requests/Service Request References Delete a service request reference  |
| HCM Service Requests/Tags - Delete a tag | HCM Service Requests/Tags Delete a tag  |
| Households - Delete a household | Households Delete a household  |
| Households/Additional Identifiers - Delete an additional identifier | Households/Additional Identifiers Delete an additional identifier  |
| Households/Additional Names - Delete an additional name | Households/Additional Names Delete an additional name  |
| Households/Addresses - Delete an address | Households/Addresses Delete an address  |
| Households/Addresses/Address Locales - Delete an address locale | Households/Addresses/Address Locales Delete an address locale  |
| Households/Addresses/Address Purposes - Delete an address purpose | Households/Addresses/Address Purposes Delete an address purpose  |
| Households/Attachments - Remove an attachment | Households/Attachments Remove an attachment  |
| Households/Aux Classifications - Delete a customer classification | Households/Aux Classifications Delete a customer classification  |
| Households/Contact Points - Delete a contact point | Households/Contact Points Delete a contact point  |
| Households/Notes - Delete a note | Households/Notes Delete a note  |
| Households/Primary Addresses - Delete a primary address | Households/Primary Addresses Delete a primary address  |
| Households/Relationships - Delete a relationship | Households/Relationships Delete a relationship  |
| Households/Sales Account Resources - Delete a sales team member | Households/Sales Account Resources Delete a sales team member  |
| Households/Source System References - Delete a source system reference | Households/Source System References Delete a source system reference  |
| Hub Organizations - Delete an organization | Hub Organizations Delete an organization  |
| Hub Organizations/Additional Identifiers - Delete an additional identifier | Hub Organizations/Additional Identifiers Delete an additional identifier  |
| Hub Organizations/Additional Names - Delete an additional name | Hub Organizations/Additional Names Delete an additional name  |
| Hub Organizations/Addresses - Delete an address | Hub Organizations/Addresses Delete an address  |
| Hub Organizations/Contact Points - Delete a contact point | Hub Organizations/Contact Points Delete a contact point  |
| Hub Organizations/Customer Classifications - Delete a customer classification | Hub Organizations/Customer Classifications Delete a customer classification  |
| Hub Organizations/Notes - Delete a note | Hub Organizations/Notes Delete a note  |
| Hub Organizations/Organization Attachments - Delete an attachment | Hub Organizations/Organization Attachments Delete an attachment  |
| Hub Organizations/Party Usage Assignments - Delete a usage assignment | Hub Organizations/Party Usage Assignments Delete a usage assignment  |
| Hub Organizations/Relationships - Delete a relationship | Hub Organizations/Relationships Delete a relationship  |
| Hub Organizations/Source System References - Delete a source system reference | Hub Organizations/Source System References Delete a source system reference  |
| Hub Persons - Delete a person | Hub Persons Delete a person  |
| Hub Persons/Additional Identifiers - Delete an additional identifier | Hub Persons/Additional Identifiers Delete an additional identifier  |
| Hub Persons/Additional Names - Delete an additional name | Hub Persons/Additional Names Delete an additional name  |
| Hub Persons/Addresses - Delete an address | Hub Persons/Addresses Delete an address  |
| Hub Persons/Attachments - Delete an attachment | Hub Persons/Attachments Delete an attachment  |
| Hub Persons/Contact Points - Delete a contact point | Hub Persons/Contact Points Delete a contact point  |
| Hub Persons/Customer Classifications - Delete a customer classification | Hub Persons/Customer Classifications Delete a customer classification  |
| Hub Persons/Notes - Delete a note | Hub Persons/Notes Delete a note  |
| Hub Persons/Relationships - Delete a relationship | Hub Persons/Relationships Delete a relationship  |
| Hub Persons/Source System References - Delete a source system reference | Hub Persons/Source System References Delete a source system reference  |
| Hub Persons/Usage Assignments - Delete a usage assignment | Hub Persons/Usage Assignments Delete a usage assignment  |
| Import Activity Maps - Delete a file import map | Import Activity Maps Delete a file import map  |
| Import Activity Maps/Import Activity Map Columns - Delete a file import map column | Import Activity Maps/Import Activity Map Columns Delete a file import map column  |
| Inbound Message Filters - Delete an inbound message filter | Inbound Message Filters Delete an inbound message filter  |
| Inbound Messages - Delete an inbound message | Inbound Messages Delete an inbound message  |
| Inbound Messages/Attachments - Delete an attachment | Inbound Messages/Attachments Delete an attachment  |
| Inbound Messages/Inbound Message Parts - Delete an inbound message part | Inbound Messages/Inbound Message Parts Delete an inbound message part  |
| Incentive Compensation Expressions - Delete an expression | Incentive Compensation Expressions Delete an expression  |
| Incentive Compensation Expressions/Expression Details - Delete an expression detail | Incentive Compensation Expressions/Expression Details Delete an expression detail  |
| Incentive Compensation Rule Hierarchies - Delete a rule hierarchy | Incentive Compensation Rule Hierarchies Delete a rule hierarchy  |
| Incentive Compensation Rule Hierarchies/Qualifying Criteria - Delete a qualifying criterion | Incentive Compensation Rule Hierarchies/Qualifying Criteria Delete a qualifying criterion  |
| Incentive Compensation Rule Hierarchies/Qualifying Criteria/Qualifying Attribute Values - Delete a qualifying attribute value | Incentive Compensation Rule Hierarchies/Qualifying Criteria/Qualifying Attribute Values Delete a qualifying attribute value  |
| Incentive Compensation Rule Hierarchies/Rule Assignments - Delete a rule assignment | Incentive Compensation Rule Hierarchies/Rule Assignments Delete a rule assignment  |
| Incentive Compensation Rule Hierarchies/Rules - Delete a rule and all its child rules | Incentive Compensation Rule Hierarchies/Rules Delete a rule and all its child rules  |
| Internal Service Requests - Delete a service request | Internal Service Requests Delete a service request  |
| Internal Service Requests/Attachments - Delete an attachment | Internal Service Requests/Attachments Delete an attachment  |
| Internal Service Requests/Channel Communications - Delete a channel communication | Internal Service Requests/Channel Communications Delete a channel communication  |
| Internal Service Requests/Contact Members - Delete a contact member | Internal Service Requests/Contact Members Delete a contact member  |
| Internal Service Requests/Messages - Delete a message | Internal Service Requests/Messages Delete a message  |
| Internal Service Requests/Messages/Attachments - Delete an attachment | Internal Service Requests/Messages/Attachments Delete an attachment  |
| Internal Service Requests/Messages/Message Channels - Delete a channel communication message | Internal Service Requests/Messages/Message Channels Delete a channel communication message  |
| Internal Service Requests/Resources - Delete a resource member | Internal Service Requests/Resources Delete a resource member  |
| Internal Service Requests/Service Request References - Delete a service request reference | Internal Service Requests/Service Request References Delete a service request reference  |
| Internal Service Requests/Tags - Delete a tag | Internal Service Requests/Tags Delete a tag  |
| Lightbox Documents - Delete a Lightbox document | Lightbox Documents Delete a Lightbox document  |
| Lightbox Documents/Shared-person Instances - Delete a shared-person instance for a document | Lightbox Documents/Shared-person Instances Delete a shared-person instance for a document  |
| Lightbox Presentation Session Feedback - Delete a lightbox presentation session feedback it... | Lightbox Presentation Session Feedback Delete a lightbox presentation session feedback item  |
| Lightbox Presentation Sessions - Delete a Lightbox presentation session | Lightbox Presentation Sessions Delete a Lightbox presentation session  |
| List of Values/Action Status Conditions - Delete a status condition | List of Values/Action Status Conditions Delete a status condition  |
| List of Values/Collaboration Recipients - Delete a collaboration recipient | List of Values/Collaboration Recipients Delete a collaboration recipient  |
| List of Values/Dynamic Link Values - Delete a reference | List of Values/Dynamic Link Values Delete a reference  |
| List of Values/KPI - Delete a KPI | List of Values/KPI Delete a KPI  |
| List of Values/KPI/KPI History Metadata - Delete a KPI history metadata | List of Values/KPI/KPI History Metadata Delete a KPI history metadata  |
| List of Values/Tags - Delete a tag | List of Values/Tags Delete a tag  |
| MDF Requests - Delete an MDF request | MDF Requests Delete an MDF request  |
| MDF Requests/Claims - Delete a claim | MDF Requests/Claims Delete a claim  |
| MDF Requests/MDF Request Teams - Delete a fund request resource | MDF Requests/MDF Request Teams Delete a fund request resource  |
| MDF Requests/Notes - Delete a note | MDF Requests/Notes Delete a note  |
| Milestones - Delete a milestone | Milestones Delete a milestone  |
| Multi Channel Adapter Events - Delete an event | Multi Channel Adapter Events Delete an event  |
| Multi-Channel Adapter Toolbars - Delete a toolbar | Multi-Channel Adapter Toolbars Delete a toolbar  |
| Multi-Channel Adapter Toolbars/Multi-Channel Adapter Toolbar Additions - Delete a toolbar addition | Multi-Channel Adapter Toolbars/Multi-Channel Adapter Toolbar Additions Delete a toolbar addition  |
| My Self-Service Roles - Delete a self-service role | My Self-Service Roles Delete a self-service role  |
| Object Capacities - Delete an object capacity | Object Capacities Delete an object capacity  |
| Objectives - Delete an objective | Objectives Delete an objective  |
| Objectives/Objective Splits - Delete an objective split | Objectives/Objective Splits Delete an objective split  |
| Omnichannel Properties - Delete an omnichannel property | Omnichannel Properties Delete an omnichannel property  |
| Opportunities - Delete an opportunity | Opportunities Delete an opportunity  |
| Opportunities/Assessments - Delete an assessment | Opportunities/Assessments Delete an assessment  |
| Opportunities/Notes - Delete a note | Opportunities/Notes Delete a note  |
| Opportunities/Opportunity Competitors - Delete an opportunity competitor | Opportunities/Opportunity Competitors Delete an opportunity competitor  |
| Opportunities/Opportunity Contacts - Delete an opportunity contact | Opportunities/Opportunity Contacts Delete an opportunity contact  |
| Opportunities/Opportunity Leads - Delete an opportunity lead | Opportunities/Opportunity Leads Delete an opportunity lead  |
| Opportunities/Opportunity Partners - Delete an opportunity revenue partner | Opportunities/Opportunity Partners Delete an opportunity revenue partner  |
| Opportunities/Opportunity Sources - Delete an opportunity source | Opportunities/Opportunity Sources Delete an opportunity source  |
| Opportunities/Opportunity Team Members - Delete an opportunity team member | Opportunities/Opportunity Team Members Delete an opportunity team member  |
| Opportunities/Revenue Items - Delete an opportunity revenue | Opportunities/Revenue Items Delete an opportunity revenue  |
| Opportunities/Revenue Items/Child Split Revenues - Delete a child split revenue | Opportunities/Revenue Items/Child Split Revenues Delete a child split revenue  |
| Opportunities/Revenue Items/Opportunity Revenue Territories - Delete an opportunity revenue territory | Opportunities/Revenue Items/Opportunity Revenue Territories Delete an opportunity revenue territory  |
| Opportunities/Revenue Items/Recurring Revenues - Delete a recurring revenue | Opportunities/Revenue Items/Recurring Revenues Delete a recurring revenue  |
| Outbound Messages - Delete an outbound message | Outbound Messages Delete an outbound message  |
| Outbound Messages/Outbound Message Parts - Delete an outbound message part | Outbound Messages/Outbound Message Parts Delete an outbound message part  |
| Participants/Participant Roles - Delete a participant role | Participants/Participant Roles Delete a participant role  |
| Partner Contacts - Delete a partner contact | Partner Contacts Delete a partner contact  |
| Partner Contacts/Attachments - Delete an attachment | Partner Contacts/Attachments Delete an attachment  |
| Partner Contacts/Partner Contact User Details - Delete an user detail | Partner Contacts/Partner Contact User Details Delete an user detail  |
| Partner Programs - Delete a partner program | Partner Programs Delete a partner program  |
| Partner Programs/Countries - Delete a country from a partner program | Partner Programs/Countries Delete a country from a partner program  |
| Partner Programs/Program Benefit Details - Delete a program benefit from a partner program | Partner Programs/Program Benefit Details Delete a program benefit from a partner program  |
| Partner Programs/Tiers - Delete a tier from a partner program | Partner Programs/Tiers Delete a tier from a partner program  |
| Partner Tiers - Delete a partner tier | Partner Tiers Delete a partner tier  |
| Partners/Addresses - Delete an address | Partners/Addresses Delete an address  |
| Partners/Attachments - Delete a partner attachment | Partners/Attachments Delete a partner attachment  |
| Partners/Expertises - Delete an expertise for a partner | Partners/Expertises Delete an expertise for a partner  |
| Partners/Focus Areas - Delete a product specialty for a partner | Partners/Focus Areas Delete a product specialty for a partner  |
| Partners/Geographies - Delete a geography for a partner | Partners/Geographies Delete a geography for a partner  |
| Partners/Industries - Delete an industry for a partner | Partners/Industries Delete an industry for a partner  |
| Partners/Notes - Delete a note for a partner | Partners/Notes Delete a note for a partner  |
| Partners/Partner Account Team Members - Delete a partner account team member for a partner | Partners/Partner Account Team Members Delete a partner account team member for a partner  |
| Partners/Partner Certifications - Delete a certification for a partner | Partners/Partner Certifications Delete a certification for a partner  |
| Partners/Partner Contacts - Delete a partner contact | Partners/Partner Contacts Delete a partner contact  |
| Partners/Partner Contacts/Attachments - Delete a partner attachment | Partners/Partner Contacts/Attachments Delete a partner attachment  |
| Partners/Partner Contacts/User Account Details - Delete a partner contact user detail | Partners/Partner Contacts/User Account Details Delete a partner contact user detail  |
| Partners/Partner Types - Delete a partner type | Partners/Partner Types Delete a partner type  |
| Pay Groups - Delete a pay group | Pay Groups Delete a pay group  |
| Pay Groups/Pay Group Assignments - Delete a pay group assignment | Pay Groups/Pay Group Assignments Delete a pay group assignment  |
| Pay Groups/Pay Group Roles - Delete a pay group role | Pay Groups/Pay Group Roles Delete a pay group role  |
| Payment Batches/Payment Transactions - Delete a manual adjustment | Payment Batches/Payment Transactions Delete a manual adjustment  |
| Payment Batches/Paysheets/Payment Transactions - Delete a manual adjustment | Payment Batches/Paysheets/Payment Transactions Delete a manual adjustment  |
| Payment Plans - Delete a payment plan | Payment Plans Delete a payment plan  |
| Payment Plans/Payment Plans Assignments - Delete a payment plan assignment | Payment Plans/Payment Plans Assignments Delete a payment plan assignment  |
| Payment Plans/Payment Plans Roles - Delete a payment plan role | Payment Plans/Payment Plans Roles Delete a payment plan role  |
| Payment Transactions - Delete a manual adjustment | Payment Transactions Delete a manual adjustment  |
| Paysheets/Payment Transactions - Delete a manual adjustment for a paysheet | Paysheets/Payment Transactions Delete a manual adjustment for a paysheet  |
| Performance Measures - Delete a performance measure | Performance Measures Delete a performance measure  |
| Performance Measures/Credit Categories - Delete a credit category | Performance Measures/Credit Categories Delete a credit category  |
| Performance Measures/Credit Categories/Credit Factors - Delete a credit factor | Performance Measures/Credit Categories/Credit Factors Delete a credit factor  |
| Performance Measures/Credit Categories/Transaction Factors - Delete a transaction factor | Performance Measures/Credit Categories/Transaction Factors Delete a transaction factor  |
| Performance Measures/Scorecards - Delete a scorecard | Performance Measures/Scorecards Delete a scorecard  |
| Phone Verifications - Delete a phone verification | Phone Verifications Delete a phone verification  |
| Plan Components - Delete a plan component | Plan Components Delete a plan component  |
| Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Tables - Delete a rate table | Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Tables Delete a rate table  |
| Plan Components/Plan Component - Performance Measures - Delete a performance measure | Plan Components/Plan Component - Performance Measures Delete a performance measure  |
| Price Book Headers - Delete a pricebook | Price Book Headers Delete a pricebook  |
| Price Book Headers/Price Book Items - Delete a pricebook item | Price Book Headers/Price Book Items Delete a pricebook item  |
| Process Metadatas - Delete a process metadata | Process Metadatas Delete a process metadata  |
| Product Groups - Delete a product group | Product Groups Delete a product group  |
| Product Groups/Attachments - Delete an attachment on the product group | Product Groups/Attachments Delete an attachment on the product group  |
| Product Groups/Filter Attributes - Delete an attribute from a product group | Product Groups/Filter Attributes Delete an attribute from a product group  |
| Product Groups/Filter Attributes/Filter Attribute Values - Delete an attribute value | Product Groups/Filter Attributes/Filter Attribute Values Delete an attribute value  |
| Product Groups/Products - Delete a product on a product group | Product Groups/Products Delete a product on a product group  |
| Product Groups/Products/EligibilityRules - Delete an eligibility rule | Product Groups/Products/EligibilityRules Delete an eligibility rule  |
| Product Groups/Related Groups - Delete a subgroup relationship | Product Groups/Related Groups Delete a subgroup relationship  |
| Products/Default Prices - Delete a default price | Products/Default Prices Delete a default price  |
| Products/Product Attachments - Delete an attachment for a product | Products/Product Attachments Delete an attachment for a product  |
| Products/Product Image Attachments - Delete an image attachment for a product | Products/Product Image Attachments Delete an image attachment for a product  |
| Program Benefits - Delete a program benefit | Program Benefits Delete a program benefit  |
| Program Benefits/Benefit List Values - Delete a benefit list type value | Program Benefits/Benefit List Values Delete a benefit list type value  |
| Program Enrollments/Notes - Delete a note for an enrollment | Program Enrollments/Notes Delete a note for an enrollment  |
| Queues - Delete a queue | Queues Delete a queue  |
| Queues/Overflow Queues - Delete an overflow queue | Queues/Overflow Queues Delete an overflow queue  |
| Queues/Queue Resource Members - Delete a resource member | Queues/Queue Resource Members Delete a resource member  |
| Queues/Queue Resource Teams - Delete a resource team | Queues/Queue Resource Teams Delete a resource team  |
| Quote and Order Lines - Delete a sales order line | Quote and Order Lines Delete a sales order line  |
| Rate Dimensions - Delete a rate dimension | Rate Dimensions Delete a rate dimension  |
| Rate Dimensions/Rate Dimension - Tiers - Delete a rate dimension tier | Rate Dimensions/Rate Dimension - Tiers Delete a rate dimension tier  |
| Rate Tables - Delete a rate table | Rate Tables Delete a rate table  |
| Rate Tables/Rate Table Dimensions - Delete a rate dimension association with a rate ta... | Rate Tables/Rate Table Dimensions Delete a rate dimension association with a rate table  |
| Resource Capacities - Delete a resource capacity | Resource Capacities Delete a resource capacity  |
| Resource Users - Delete a resource user | Resource Users Delete a resource user  |
| Roles - Delete an incentive compensation role | Roles Delete an incentive compensation role  |
| Roles/Role - Participants - Delete a participant from a role | Roles/Role - Participants Delete a participant from a role  |
| Sales Leads - Delete a sales lead | Sales Leads Delete a sales lead  |
| Sales Leads/Lead Qualifications - Delete a lead qualification | Sales Leads/Lead Qualifications Delete a lead qualification  |
| Sales Leads/Notes - Delete a note | Sales Leads/Notes Delete a note  |
| Sales Leads/Sales Lead Contacts - Delete a sales lead contact | Sales Leads/Sales Lead Contacts Delete a sales lead contact  |
| Sales Leads/Sales Lead Products - Delete a sales lead product | Sales Leads/Sales Lead Products Delete a sales lead product  |
| Sales Leads/Sales Lead Resources - Delete a sales lead resource | Sales Leads/Sales Lead Resources Delete a sales lead resource  |
| Sales Orders - Delete a quote | Sales Orders Delete a quote  |
| Sales Orders/Quote and Order Lines - Delete a sales order line | Sales Orders/Quote and Order Lines Delete a sales order line  |
| Sales Promotions - Delete a sales promotion | Sales Promotions Delete a sales promotion  |
| Sales Territories - Delete a territory | Sales Territories Delete a territory  |
| Sales Territories/Line of Business - Delete a line of business for a territory | Sales Territories/Line of Business Delete a line of business for a territory  |
| Sales Territories/Resources - Delete a resource for a territory | Sales Territories/Resources Delete a resource for a territory  |
| Sales Territories/Rules - Delete a rule for a territory | Sales Territories/Rules Delete a rule for a territory  |
| Sales Territories/Rules/Rule Boundaries - Delete a rule boundary for a territory | Sales Territories/Rules/Rule Boundaries Delete a rule boundary for a territory  |
| Sales Territories/Rules/Rule Boundaries/Rule Boundary Values - Delete a rule boundary value for a territory | Sales Territories/Rules/Rule Boundaries/Rule Boundary Values Delete a rule boundary value for a territory  |
| Sales Territory Proposals - Delete a proposal | Sales Territory Proposals Delete a proposal  |
| Self-Service Roles - Delete a role from a self-service user | Self-Service Roles Delete a role from a self-service user  |
| Self-Service Users/Self-Service Roles - Delete a self-service role | Self-Service Users/Self-Service Roles Delete a self-service role  |
| Self-Service Users/Self-Service Roles - Delete a self-service role | Self-Service Users/Self-Service Roles Delete a self-service role  |
| Service Providers - Delete a service provider | Service Providers Delete a service provider  |
| Service Providers/Services - Delete a service | Service Providers/Services Delete a service  |
| Service Requests - Delete a service request | Service Requests Delete a service request  |
| Service Requests/Activities - Delete an activity | Service Requests/Activities Delete an activity  |
| Service Requests/Attachments - Delete an attachment | Service Requests/Attachments Delete an attachment  |
| Service Requests/Channel Communications - Delete a channel communication | Service Requests/Channel Communications Delete a channel communication  |
| Service Requests/Contact Members - Delete a contact | Service Requests/Contact Members Delete a contact  |
| Service Requests/Messages - Delete a message | Service Requests/Messages Delete a message  |
| Service Requests/Messages/Attachments - Delete an attachment | Service Requests/Messages/Attachments Delete an attachment  |
| Service Requests/Messages/Channel Communications - Delete a channel communication | Service Requests/Messages/Channel Communications Delete a channel communication  |
| Service Requests/Resources - Delete a resource member | Service Requests/Resources Delete a resource member  |
| Service Requests/Service Request References - Delete a service request reference | Service Requests/Service Request References Delete a service request reference  |
| Service Requests/Tags - Delete a tag | Service Requests/Tags Delete a tag  |
| Setup Assistants/Competitors - Delete a competitor | Setup Assistants/Competitors Delete a competitor  |
| Setup Assistants/Opportunities/Sales Stages - Delete a user defined sales stage for the selected... | Setup Assistants/Opportunities/Sales Stages Delete a user defined sales stage for the selected sales method  |
| Setup Assistants/Role Mappings - Delete a resource role and role mapping | Setup Assistants/Role Mappings Delete a resource role and role mapping  |
| Setup Assistants/Sales Stages - Delete a user defined sales stage for the selected... | Setup Assistants/Sales Stages Delete a user defined sales stage for the selected sales method  |
| Setup Assistants/Setup Users - Delete a setup user details | Setup Assistants/Setup Users Delete a setup user details  |
| Skill Resources - Delete a skill resource | Skill Resources Delete a skill resource  |
| Skills - Delete a skill | Skills Delete a skill  |
| Skills/Competencies - Delete a competency | Skills/Competencies Delete a competency  |
| Skills/Competencies/Competency Values - Delete a value | Skills/Competencies/Competency Values Delete a value  |
| Smart Text Folders - Delete a smart text folder | Smart Text Folders Delete a smart text folder  |
| Smart Text Folders/Smart Text Child Folders - Delete a smart text child folder | Smart Text Folders/Smart Text Child Folders Delete a smart text child folder  |
| Smart Text User Variables - Delete a smart text user variable | Smart Text User Variables Delete a smart text user variable  |
| Smart Texts - Delete a smart text | Smart Texts Delete a smart text  |
| Social Posts - Delete a social post | Social Posts Delete a social post  |
| Social Posts/Social Post Tags - Delete a social post tag | Social Posts/Social Post Tags Delete a social post tag  |
| Social Posts/Social Post URLs - Delete a social post URL | Social Posts/Social Post URLs Delete a social post URL  |
| Source System References - Delete a source system reference | Source System References Delete a source system reference  |
| Standard Coverages - Delete a standard coverage | Standard Coverages Delete a standard coverage  |
| Subscription Accounts - Delete a subscription account | Subscription Accounts Delete a subscription account  |
| Subscription Accounts Roles - Delete a subscription account role | Subscription Accounts Roles Delete a subscription account role  |
| Subscription Accounts/Attachments - Delete an attachment | Subscription Accounts/Attachments Delete an attachment  |
| Subscription Accounts/Billing Profiles - Delete a billing profile | Subscription Accounts/Billing Profiles Delete a billing profile  |
| Subscription Accounts/Notes - Delete a note | Subscription Accounts/Notes Delete a note  |
| Subscription Accounts/Subscription Account Addresses - Delete an address | Subscription Accounts/Subscription Account Addresses Delete an address  |
| Subscription Accounts/Subscription Account Relationships - Delete a subscription account relationship | Subscription Accounts/Subscription Account Relationships Delete a subscription account relationship  |
| Subscription Accounts/Subscription Account Roles - Delete a subscription account role | Subscription Accounts/Subscription Account Roles Delete a subscription account role  |
| Subscription AI Features - Delete a product churn feature | Subscription AI Features Delete a product churn feature  |
| Subscription Products - Delete a subscription product | Subscription Products Delete a subscription product  |
| Subscription Products/Bill Lines - Delete a bill line | Subscription Products/Bill Lines Delete a bill line  |
| Subscription Products/Bill Lines/Bill Adjustments - Delete a bill adjustment | Subscription Products/Bill Lines/Bill Adjustments Delete a bill adjustment  |
| Subscription Products/Charges - Delete a charge | Subscription Products/Charges Delete a charge  |
| Subscription Products/Charges/Adjustments - Delete an adjustment | Subscription Products/Charges/Adjustments Delete an adjustment  |
| Subscription Products/Charges/Charge Tiers - Delete a charge tier | Subscription Products/Charges/Charge Tiers Delete a charge tier  |
| Subscription Products/Covered Levels - Delete a covered level | Subscription Products/Covered Levels Delete a covered level  |
| Subscription Products/Covered Levels/Bill Lines - Delete a bill line | Subscription Products/Covered Levels/Bill Lines Delete a bill line  |
| Subscription Products/Covered Levels/Bill Lines/Bill Adjustments - Delete a bill adjustment | Subscription Products/Covered Levels/Bill Lines/Bill Adjustments Delete a bill adjustment  |
| Subscription Products/Covered Levels/Charges - Delete a charge | Subscription Products/Covered Levels/Charges Delete a charge  |
| Subscription Products/Covered Levels/Charges/Adjustments - Delete an adjustment | Subscription Products/Covered Levels/Charges/Adjustments Delete an adjustment  |
| Subscription Products/Covered Levels/Charges/Charge Tiers - Delete a charge tier | Subscription Products/Covered Levels/Charges/Charge Tiers Delete a charge tier  |
| Subscription Products/Covered Levels/Relationships - Delete a relationship | Subscription Products/Covered Levels/Relationships Delete a relationship  |
| Subscription Products/Relationships - Delete a relationship | Subscription Products/Relationships Delete a relationship  |
| Subscription Products/Sales Credits - Delete a sales credit | Subscription Products/Sales Credits Delete a sales credit  |
| Subscriptions - Delete a subscription | Subscriptions Delete a subscription  |
| Subscriptions/Parties - Delete a subscription party | Subscriptions/Parties Delete a subscription party  |
| Subscriptions/Parties/Contacts - Delete a subscription contact | Subscriptions/Parties/Contacts Delete a subscription contact  |
| Subscriptions/Products - Delete a subscription product | Subscriptions/Products Delete a subscription product  |
| Subscriptions/Products/Bill Lines - Delete a subscription bill line | Subscriptions/Products/Bill Lines Delete a subscription bill line  |
| Subscriptions/Products/Bill Lines/Bill Adjustments - Delete a subscription bill adjustment | Subscriptions/Products/Bill Lines/Bill Adjustments Delete a subscription bill adjustment  |
| Subscriptions/Products/Charges - Delete a subscription charge | Subscriptions/Products/Charges Delete a subscription charge  |
| Subscriptions/Products/Charges/Adjustments - Delete a subscription adjustment | Subscriptions/Products/Charges/Adjustments Delete a subscription adjustment  |
| Subscriptions/Products/Charges/Charge Tiers - Delete a subscription charge tier | Subscriptions/Products/Charges/Charge Tiers Delete a subscription charge tier  |
| Subscriptions/Products/Covered Levels - Delete a covered level | Subscriptions/Products/Covered Levels Delete a covered level  |
| Subscriptions/Products/Covered Levels/Bill Lines - Delete a subscription bill line | Subscriptions/Products/Covered Levels/Bill Lines Delete a subscription bill line  |
| Subscriptions/Products/Covered Levels/Bill Lines/Bill Adjustments - Delete a subscription bill adjustment | Subscriptions/Products/Covered Levels/Bill Lines/Bill Adjustments Delete a subscription bill adjustment  |
| Subscriptions/Products/Covered Levels/Charges - Delete a subscription charge | Subscriptions/Products/Covered Levels/Charges Delete a subscription charge  |
| Subscriptions/Products/Covered Levels/Charges/Adjustments - Delete a subscription adjustment | Subscriptions/Products/Covered Levels/Charges/Adjustments Delete a subscription adjustment  |
| Subscriptions/Products/Covered Levels/Charges/Charge Tiers - Delete a subscription charge tier | Subscriptions/Products/Covered Levels/Charges/Charge Tiers Delete a subscription charge tier  |
| Subscriptions/Products/Covered Levels/Child Covered Levels - Delete a child covered level | Subscriptions/Products/Covered Levels/Child Covered Levels Delete a child covered level  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines - Delete a subscription bill line | Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines Delete a subscription bill line  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines/Bill Adjustments - Delete a subscription bill adjustment | Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines/Bill Adjustments Delete a subscription bill adjustment  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges - Delete a subscription charge | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges Delete a subscription charge  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Adjustments - Delete a subscription adjustment | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Adjustments Delete a subscription adjustment  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Charge Tiers - Delete a subscription charge tier | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Charge Tiers Delete a subscription charge tier  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Relationships - Delete a subscription relationship | Subscriptions/Products/Covered Levels/Child Covered Levels/Relationships Delete a subscription relationship  |
| Subscriptions/Products/Covered Levels/Relationships - Delete a subscription relationship | Subscriptions/Products/Covered Levels/Relationships Delete a subscription relationship  |
| Subscriptions/Products/Relationships - Delete a subscription relationship | Subscriptions/Products/Relationships Delete a subscription relationship  |
| Subscriptions/Products/Sales Credits - Delete a sales credit | Subscriptions/Products/Sales Credits Delete a sales credit  |
| Subscriptions/Sales Credits - Delete a sales credit | Subscriptions/Sales Credits Delete a sales credit  |
| Subscriptions/Validate Subscriptions - Delete a subscription validation | Subscriptions/Validate Subscriptions Delete a subscription validation  |
| Survey Configurations - Delete a survey configuration | Survey Configurations Delete a survey configuration  |
| Survey Configurations/Survey Configuration Attributes - Delete a survey configuration attribute | Survey Configurations/Survey Configuration Attributes Delete a survey configuration attribute  |
| Surveys - Delete a survey | Surveys Delete a survey  |
| Surveys/Survey Questions - Delete a survey question | Surveys/Survey Questions Delete a survey question  |
| Surveys/Survey Questions/Survey Answer Choices - Delete a survey answer choice | Surveys/Survey Questions/Survey Answer Choices Delete a survey answer choice  |
| Territories for Sales - Delete a territory | Territories for Sales Delete a territory  |
| Territories for Sales/Territory Business - Delete a territory line of business | Territories for Sales/Territory Business Delete a territory line of business  |
| Territories for Sales/Territory Coverages - Delete a territory coverage | Territories for Sales/Territory Coverages Delete a territory coverage  |
| Territories for Sales/Territory Resources - Delete a territory resource | Territories for Sales/Territory Resources Delete a territory resource  |
| Work Orders - Delete a work order | Work Orders Delete a work order  |
| Work Orders/Attachments - Delete an attachment | Work Orders/Attachments Delete an attachment  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Access Groups - Get an access group | Access Groups Get an access group  |
| Access Groups/Access Group Members - Get an access group member | Access Groups/Access Group Members Get an access group member  |
| Accounts - Get an account | Accounts Get an account  |
| Accounts/Account Attachments - Get an attachment | Accounts/Account Attachments Get an attachment  |
| Accounts/Account Extension Bases - Get an account extension base | Accounts/Account Extension Bases Get an account extension base  |
| Accounts/Account Resources - Get a sales team member | Accounts/Account Resources Get a sales team member  |
| Accounts/Account Rollups - Get an account rollup | Accounts/Account Rollups Get an account rollup  |
| Accounts/Additional Identifier - Get an additional identifier | Accounts/Additional Identifier Get an additional identifier  |
| Accounts/Additional Names - Get an additional name | Accounts/Additional Names Get an additional name  |
| Accounts/Addresses - Get an address | Accounts/Addresses Get an address  |
| Accounts/Addresses/Address Locales - Get an address locale | Accounts/Addresses/Address Locales Get an address locale  |
| Accounts/Addresses/Address Purposes - Get an address purpose | Accounts/Addresses/Address Purposes Get an address purpose  |
| Accounts/Aux Classifications - Get a customer classification | Accounts/Aux Classifications Get a customer classification  |
| Accounts/Contact Points - Get a contact point | Accounts/Contact Points Get a contact point  |
| Accounts/Notes - Get a note | Accounts/Notes Get a note  |
| Accounts/Organization Contacts - Get an account contact | Accounts/Organization Contacts Get an account contact  |
| Accounts/Primary Addresses - Get a primary address | Accounts/Primary Addresses Get a primary address  |
| Accounts/Relationships - Get a relationship | Accounts/Relationships Get a relationship  |
| Accounts/Smart Actions - Get a smart action (Not Supported) | Accounts/Smart Actions Get a smart action (Not Supported)  |
| Accounts/Smart Actions/Smart Action REST Path Parameter Definitions - Get an action URL binding (Not Supported) | Accounts/Smart Actions/Smart Action REST Path Parameter Definitions Get an action URL binding (Not Supported)  |
| Accounts/Smart Actions/Smart Action REST Payload Definitions - Get an action request payload (Not Supported) | Accounts/Smart Actions/Smart Action REST Payload Definitions Get an action request payload (Not Supported)  |
| Accounts/Smart Actions/Smart Action User Interface Definitions - Get an action navigation (Not Supported) | Accounts/Smart Actions/Smart Action User Interface Definitions Get an action navigation (Not Supported)  |
| Accounts/Source System References - Get a source system reference | Accounts/Source System References Get a source system reference  |
| Action Events - Get an action event | Action Events Get an action event  |
| Action Plans - Get an Action Plan | Action Plans Get an Action Plan  |
| Action Plans/Action Plan Actions - Get an Action Plan Action | Action Plans/Action Plan Actions Get an Action Plan Action  |
| Action Plans/Action Plan Actions/Action Plan Action Relations - Get an action plan action relation | Action Plans/Action Plan Actions/Action Plan Action Relations Get an action plan action relation  |
| Action Templates - Get an Action Plan Template | Action Templates Get an Action Plan Template  |
| Action Templates/Template Actions - Get a Template Action | Action Templates/Template Actions Get a Template Action  |
| Action Templates/Template Actions/Action Relations - Get an Action Relation | Action Templates/Template Actions/Action Relations Get an Action Relation  |
| Actions - Get an Action | Actions Get an Action  |
| Actions/Action Attributes - Get an Action Attribute | Actions/Action Attributes Get an Action Attribute  |
| Actions/Action Conditions - Get an Action Condition | Actions/Action Conditions Get an Action Condition  |
| Activities - Get an activity | Activities Get an activity  |
| Activities/Activity Assignees - Get an activity assignee | Activities/Activity Assignees Get an activity assignee  |
| Activities/Activity Attachments - Get an activity attachment | Activities/Activity Attachments Get an activity attachment  |
| Activities/Activity Contacts - Get an activity contact | Activities/Activity Contacts Get an activity contact  |
| Activities/Activity Objectives - Get an activity objective | Activities/Activity Objectives Get an activity objective  |
| Activities/Notes - Get an activity note | Activities/Notes Get an activity note  |
| Activities/Smart Actions - Get a smart action (Not Supported) | Activities/Smart Actions Get a smart action (Not Supported)  |
| Activities/Smart Actions/Smart Action REST Path Parameter Definitions - GET an action URL binding (Not Supported) | Activities/Smart Actions/Smart Action REST Path Parameter Definitions GET an action URL binding (Not Supported)  |
| Activities/Smart Actions/Smart Action REST Payload Definitions - GET an action request payload (Not Supported) | Activities/Smart Actions/Smart Action REST Payload Definitions GET an action request payload (Not Supported)  |
| Activities/Smart Actions/Smart Action User Interface Definitions - GET an action navigation (Not Supported) | Activities/Smart Actions/Smart Action User Interface Definitions GET an action navigation (Not Supported)  |
| Address Style Formats - Get an address style format | Address Style Formats Get an address style format  |
| Address Style Formats/Address Style Format Layouts - Get an address format layout | Address Style Formats/Address Style Format Layouts Get an address format layout  |
| Assets - Get an asset | Assets Get an asset  |
| Assets/Asset Contacts - Get an asset contact | Assets/Asset Contacts Get an asset contact  |
| Assets/Asset Resources - Get an asset resource | Assets/Asset Resources Get an asset resource  |
| Assets/Attachments - Get an attachment | Assets/Attachments Get an attachment  |
| Assets/Smart Actions - Get a smart action (Not Supported) | Assets/Smart Actions Get a smart action (Not Supported)  |
| Assets/Smart Actions/Smart Action REST Path Parameter Definitions - GET an action URL binding (Not Supported) | Assets/Smart Actions/Smart Action REST Path Parameter Definitions GET an action URL binding (Not Supported)  |
| Assets/Smart Actions/Smart Action REST Payload Definitions - GET an action request payload (Not Supported) | Assets/Smart Actions/Smart Action REST Payload Definitions GET an action request payload (Not Supported)  |
| Assets/Smart Actions/Smart Action User Interface Definitions - GET an action navigation (Not Supported) | Assets/Smart Actions/Smart Action User Interface Definitions GET an action navigation (Not Supported)  |
| Attribute Predictions - Get an attribute prediction | Attribute Predictions Get an attribute prediction  |
| Attributes Values LOVs - Get a rule attribute value | Attributes Values LOVs Get a rule attribute value  |
| Billing Adjustments - Get a billing adjustment | Billing Adjustments Get a billing adjustment  |
| Billing Adjustments/Billing Adjustment Events - Get a billing adjustment event | Billing Adjustments/Billing Adjustment Events Get a billing adjustment event  |
| Budgets - Get a MDF budget | Budgets Get a MDF budget  |
| Budgets/Budget Countries - Get a MDF budget country | Budgets/Budget Countries Get a MDF budget country  |
| Budgets/Claims - Get a MDF claim associated to the budget | Budgets/Claims Get a MDF claim associated to the budget  |
| Budgets/Fund Requests - Get a MDF request associated to the budget | Budgets/Fund Requests Get a MDF request associated to the budget  |
| Budgets/MDF Budget Teams - Get a MDF budget team member | Budgets/MDF Budget Teams Get a MDF budget team member  |
| Budgets/Notes - Get a MDF budget note | Budgets/Notes Get a MDF budget note  |
| Business Plans - Get a business plan | Business Plans Get a business plan  |
| Business Plans/Business Plan Resources - Get a business plan resource | Business Plans/Business Plan Resources Get a business plan resource  |
| Business Plans/Notes - Get a business plan note | Business Plans/Notes Get a business plan note  |
| Business Plans/SWOTs - Get a business plan SWOT | Business Plans/SWOTs Get a business plan SWOT  |
| Business Units List of Values - Get a business unit | Business Units List of Values Get a business unit  |
| Calculation Simulations - Get a calculation simulation | Calculation Simulations Get a calculation simulation  |
| Calculation Simulations/Simulation Earnings - Get a simulation earning | Calculation Simulations/Simulation Earnings Get a simulation earning  |
| Calculation Simulations/Simulation Meause Results - Get a simulation measure result | Calculation Simulations/Simulation Meause Results Get a simulation measure result  |
| Calculation Simulations/Simulation Transactions - Get a simulation transaction | Calculation Simulations/Simulation Transactions Get a simulation transaction  |
| Calculation Simulations/Simulation Transactions/Simulation Credits - Get a simulation credit | Calculation Simulations/Simulation Transactions/Simulation Credits Get a simulation credit  |
| Calculation Simulations/Simulation Transactions/Simulation Transaction Descriptive Flex Fields - Get a simulation transaction descriptive flex fiel... | Calculation Simulations/Simulation Transactions/Simulation Transaction Descriptive Flex Fields Get a simulation transaction descriptive flex field  |
| Campaign Members - Get a campaign member | Campaign Members Get a campaign member  |
| Campaigns - Get a campaign | Campaigns Get a campaign  |
| Campaigns/Smart Actions - Get a smart action (Not Supported) | Campaigns/Smart Actions Get a smart action (Not Supported)  |
| Campaigns/Smart Actions/Smart Action REST Path Parameter Definitions - Get an action URL binding (Not Supported) | Campaigns/Smart Actions/Smart Action REST Path Parameter Definitions Get an action URL binding (Not Supported)  |
| Campaigns/Smart Actions/Smart Action REST Payload Definitions - Get an action request payload (Not Supported) | Campaigns/Smart Actions/Smart Action REST Payload Definitions Get an action request payload (Not Supported)  |
| Campaigns/Smart Actions/Smart Action User Interface Definitions - Get an action navigation (Not Supported) | Campaigns/Smart Actions/Smart Action User Interface Definitions Get an action navigation (Not Supported)  |
| Cases - Get a case | Cases Get a case  |
| Cases/Case Contacts - Get a case contact | Cases/Case Contacts Get a case contact  |
| Cases/Case Households - Get a case household | Cases/Case Households Get a case household  |
| Cases/Case Messages - Get a case message | Cases/Case Messages Get a case message  |
| Cases/Case Opportunities - Get an opportunity | Cases/Case Opportunities Get an opportunity  |
| Cases/Case Resources - Get a case resource | Cases/Case Resources Get a case resource  |
| Cases/Smart Actions - Get a smart action (Not Supported) | Cases/Smart Actions Get a smart action (Not Supported)  |
| Cases/Smart Actions/Path Parameter Definitions - Get an action URL binding (Not Supported) | Cases/Smart Actions/Path Parameter Definitions Get an action URL binding (Not Supported)  |
| Cases/Smart Actions/Payload Definitions - Get an action request payload (Not Supported) | Cases/Smart Actions/Payload Definitions Get an action request payload (Not Supported)  |
| Cases/Smart Actions/User Interface Definitions - Get an action navigation (Not Supported) | Cases/Smart Actions/User Interface Definitions Get an action navigation (Not Supported)  |
| Catalog Product Groups - Get a catalog product group | Catalog Product Groups Get a catalog product group  |
| Catalog Product Groups/Attachments - Get an attachment | Catalog Product Groups/Attachments Get an attachment  |
| Catalog Product Groups/Translations - Get a product group translation | Catalog Product Groups/Translations Get a product group translation  |
| Catalog Products Items - Get a product item | Catalog Products Items Get a product item  |
| Catalog Products Items/Attachments - Get an attachment | Catalog Products Items/Attachments Get an attachment  |
| Catalog Products Items/Item Translation - Get an item translation | Catalog Products Items/Item Translation Get an item translation  |
| Categories - Get a category | Categories Get a category  |
| Channels - Get a channel | Channels Get a channel  |
| Channels/Channel Resources - Get a channel resource member | Channels/Channel Resources Get a channel resource member  |
| Channels/Sender Identification Priorities - Get details of a sender priority sequence | Channels/Sender Identification Priorities Get details of a sender priority sequence  |
| Chat Authentications - Get a chat authentication (Not Supported) | Chat Authentications Get a chat authentication (Not Supported)  |
| Chat Interactions - Get a chat interaction (Not supported) | Chat Interactions Get a chat interaction (Not supported)  |
| Chat Interactions/Transcripts - Get a transcript (Not supported) | Chat Interactions/Transcripts Get a transcript (Not supported)  |
| Claims - Get an MDF claim | Claims Get an MDF claim  |
| Claims/Claim Settlements - Get a claim settlement | Claims/Claim Settlements Get a claim settlement  |
| Claims/Claim Team Members - Get a claim team member | Claims/Claim Team Members Get a claim team member  |
| Claims/Notes - Get a note | Claims/Notes Get a note  |
| Collaboration Actions - Get a collaboration action | Collaboration Actions Get a collaboration action  |
| Collaboration Actions/Action Attributes - Get a collaboration action attribute | Collaboration Actions/Action Attributes Get a collaboration action attribute  |
| Compensation Plans - Get a compensation plan | Compensation Plans Get a compensation plan  |
| Compensation Plans/Assigned Participants - Get an assigned participant | Compensation Plans/Assigned Participants Get an assigned participant  |
| Compensation Plans/Compensation Plan - Descriptive Flex Fields - Get a descriptive flex field | Compensation Plans/Compensation Plan - Descriptive Flex Fields Get a descriptive flex field  |
| Compensation Plans/Compensation Plan - Plan Components - Get a plan component | Compensation Plans/Compensation Plan - Plan Components Get a plan component  |
| Compensation Plans/Compensation Plan - Roles - Get a role | Compensation Plans/Compensation Plan - Roles Get a role  |
| Compensation Plans/Direct Assignment Requests - Get a direct assignment request | Compensation Plans/Direct Assignment Requests Get a direct assignment request  |
| Competencies - Get a competency | Competencies Get a competency  |
| Competitors - Get a competitor | Competitors Get a competitor  |
| Competitors - Get a competitor | Competitors Get a competitor  |
| Contacts - Get a contact | Contacts Get a contact  |
| Contacts/Additional Identifiers - Get an additional identifier | Contacts/Additional Identifiers Get an additional identifier  |
| Contacts/Additional Names - Get an additional name | Contacts/Additional Names Get an additional name  |
| Contacts/Attachments - Get a contact's picture | Contacts/Attachments Get a contact's picture  |
| Contacts/Aux Classifications - Get a customer classification | Contacts/Aux Classifications Get a customer classification  |
| Contacts/Contact Addresses - Get an address | Contacts/Contact Addresses Get an address  |
| Contacts/Contact Addresses/Address Locales - Get an address locale | Contacts/Contact Addresses/Address Locales Get an address locale  |
| Contacts/Contact Addresses/Contact Address Purposes - Get an address purpose | Contacts/Contact Addresses/Contact Address Purposes Get an address purpose  |
| Contacts/Contact Attachments - Get an attachment | Contacts/Contact Attachments Get an attachment  |
| Contacts/Contact Points - Get a contact point | Contacts/Contact Points Get a contact point  |
| Contacts/Contact Primary Addresses - Get a primary address | Contacts/Contact Primary Addresses Get a primary address  |
| Contacts/Notes - Get a note | Contacts/Notes Get a note  |
| Contacts/Relationships - Get a relationship | Contacts/Relationships Get a relationship  |
| Contacts/Sales Account Resources - Get a sales team member | Contacts/Sales Account Resources Get a sales team member  |
| Contacts/Smart Actions - Get a smart action (Not Supported) | Contacts/Smart Actions Get a smart action (Not Supported)  |
| Contacts/Smart Actions/Smart Action REST Path Parameter Definitions - Get an action URL binding (Not Supported) | Contacts/Smart Actions/Smart Action REST Path Parameter Definitions Get an action URL binding (Not Supported)  |
| Contacts/Smart Actions/Smart Action REST Payload Definitions - Get an action request payload (Not Supported) | Contacts/Smart Actions/Smart Action REST Payload Definitions Get an action request payload (Not Supported)  |
| Contacts/Smart Actions/Smart Action User Interface Definitions - Get an action navigation (Not Supported) | Contacts/Smart Actions/Smart Action User Interface Definitions Get an action navigation (Not Supported)  |
| Contacts/Source System References - Get a source system reference | Contacts/Source System References Get a source system reference  |
| Contest Scores - Get a contest score | Contest Scores Get a contest score  |
| Contest Scores/Contest Score Details - Get a contest score detail record | Contest Scores/Contest Score Details Get a contest score detail record  |
| Contests - Get a contest | Contests Get a contest  |
| Contests/Contest History - Get a contest history | Contests/Contest History Get a contest history  |
| Contests/Contest Resources - Get a contest participant | Contests/Contest Resources Get a contest participant  |
| Contests/Contest Scores - Get a contest score | Contests/Contest Scores Get a contest score  |
| Contests/Contest Scores/Contest Score Details - Get a contest score detail record | Contests/Contest Scores/Contest Score Details Get a contest score detail record  |
| Contract Asset Transactions - Get contract asset transaction | Contract Asset Transactions Get contract asset transaction  |
| Contract Asset Transactions/Contract Asset Transaction Details - Get a contract asset transaction detail | Contract Asset Transactions/Contract Asset Transaction Details Get a contract asset transaction detail  |
| Contracts - Get a contract | Contracts Get a contract  |
| Contracts/Bill Plans - Get a bill plan | Contracts/Bill Plans Get a bill plan  |
| Contracts/Bill Plans/Job Assignment Overrides - Get a job assignment override | Contracts/Bill Plans/Job Assignment Overrides Get a job assignment override  |
| Contracts/Bill Plans/Job Rate Overrides - Get a job rate override | Contracts/Bill Plans/Job Rate Overrides Get a job rate override  |
| Contracts/Bill Plans/Job Title Overrides - Get a job title override | Contracts/Bill Plans/Job Title Overrides Get a job title override  |
| Contracts/Bill Plans/Labor Multiplier Overrides - Get a labor multiplier override | Contracts/Bill Plans/Labor Multiplier Overrides Get a labor multiplier override  |
| Contracts/Bill Plans/Non Labor Rate Overrides - Get a non labor rate override | Contracts/Bill Plans/Non Labor Rate Overrides Get a non labor rate override  |
| Contracts/Bill Plans/Person Rate Overrides - Get a person rate override | Contracts/Bill Plans/Person Rate Overrides Get a person rate override  |
| Contracts/Billing Controls - Get a billing control | Contracts/Billing Controls Get a billing control  |
| Contracts/Contract Documents - Get a contract document | Contracts/Contract Documents Get a contract document  |
| Contracts/Contract Header Flexfields - Get a contract header flexfield | Contracts/Contract Header Flexfields Get a contract header flexfield  |
| Contracts/Contract Header Translations - Get all contract header translation | Contracts/Contract Header Translations Get all contract header translation  |
| Contracts/Contract Lines - Get a contract line | Contracts/Contract Lines Get a contract line  |
| Contracts/Contract Lines/Associated Projects - Get an associated project | Contracts/Contract Lines/Associated Projects Get an associated project  |
| Contracts/Contract Lines/Billing Controls - Get a billing control | Contracts/Contract Lines/Billing Controls Get a billing control  |
| Contracts/Contract Lines/Contract Line Flexfields - Get a contract line flexfield | Contracts/Contract Lines/Contract Line Flexfields Get a contract line flexfield  |
| Contracts/Contract Lines/Contract Line Translations - Get a contract line translation | Contracts/Contract Lines/Contract Line Translations Get a contract line translation  |
| Contracts/Contract Lines/Sales Credits - Get a sales credit | Contracts/Contract Lines/Sales Credits Get a sales credit  |
| Contracts/Contract Parties - Get a contract party | Contracts/Contract Parties Get a contract party  |
| Contracts/Contract Parties/Contract Party Contacts - Get a contract party contact | Contracts/Contract Parties/Contract Party Contacts Get a contract party contact  |
| Contracts/Contract Parties/Contract Party Contacts/Contract Party Contact Flexfields - Get a contract party contact flexfield | Contracts/Contract Parties/Contract Party Contacts/Contract Party Contact Flexfields Get a contract party contact flexfield  |
| Contracts/Contract Parties/Contract Party Flexfields - Get a contract party flexfield | Contracts/Contract Parties/Contract Party Flexfields Get a contract party flexfield  |
| Contracts/Related Documents - Get a related document | Contracts/Related Documents Get a related document  |
| Contracts/Revenue Plans - Get a revenue plan | Contracts/Revenue Plans Get a revenue plan  |
| Contracts/Revenue Plans/Job Assignment Overrides - Get a job assignment override | Contracts/Revenue Plans/Job Assignment Overrides Get a job assignment override  |
| Contracts/Revenue Plans/Job Rate Overrides - Get a job rate override | Contracts/Revenue Plans/Job Rate Overrides Get a job rate override  |
| Contracts/Revenue Plans/Labor Multiplier Overrides - Get a labor multiplier override | Contracts/Revenue Plans/Labor Multiplier Overrides Get a labor multiplier override  |
| Contracts/Revenue Plans/Non Labor Rate Overrides - Get a non labor rate override | Contracts/Revenue Plans/Non Labor Rate Overrides Get a non labor rate override  |
| Contracts/Revenue Plans/Person Rate Overrides - Get a person rate override | Contracts/Revenue Plans/Person Rate Overrides Get a person rate override  |
| Contracts/Sales Credits - Get a sales credit | Contracts/Sales Credits Get a sales credit  |
| Contracts/Sections - Get a section | Contracts/Sections Get a section  |
| Contracts/Sections/Clauses - Get a clause | Contracts/Sections/Clauses Get a clause  |
| Contracts/Sections/Sub Sections - Get sub section | Contracts/Sections/Sub Sections Get sub section  |
| Contracts/Supporting Documents - Get a supporting document | Contracts/Supporting Documents Get a supporting document  |
| Conversation Messages - Get a conversation message | Conversation Messages Get a conversation message  |
| Conversation Messages/Attachments - Get an attachment | Conversation Messages/Attachments Get an attachment  |
| Conversation Messages/Conversation Message Recipients - Get a conversation message recipient | Conversation Messages/Conversation Message Recipients Get a conversation message recipient  |
| Conversation Messages/Smart Actions - Get an action | Conversation Messages/Smart Actions Get an action  |
| Conversation Messages/Smart Actions/Smart Action REST Payload Definitions - Get an action request payload definition | Conversation Messages/Smart Actions/Smart Action REST Payload Definitions Get an action request payload definition  |
| Conversation Messages/Smart Actions/Smart Action User Interface Definitions - Get an action navigation definition | Conversation Messages/Smart Actions/Smart Action User Interface Definitions Get an action navigation definition  |
| Conversation Messages/Smart Actions/Smart Action User Interface Definitions - Get an action path parameter | Conversation Messages/Smart Actions/Smart Action User Interface Definitions Get an action path parameter  |
| Conversations - Get a conversation | Conversations Get a conversation  |
| Conversations/Conversation References - Get a conversation reference | Conversations/Conversation References Get a conversation reference  |
| Copy Maps - Get a copy map | Copy Maps Get a copy map  |
| Credit Categories - Get a credit category | Credit Categories Get a credit category  |
| Currencies - Get a currency | Currencies Get a currency  |
| DaaS Smart Data - Get smart data | DaaS Smart Data Get smart data  |
| Deal Registrations - Get a deal registration | Deal Registrations Get a deal registration  |
| Deal Registrations/Deal Products - Get a deal product | Deal Registrations/Deal Products Get a deal product  |
| Deal Registrations/Deal Team Members - Get a deal team member | Deal Registrations/Deal Team Members Get a deal team member  |
| Deal Registrations/Notes - Get a note | Deal Registrations/Notes Get a note  |
| Deal Registrations/Opportunities - Get an opportunity | Deal Registrations/Opportunities Get an opportunity  |
| Deal Registrations/Product Groups - Get a product group | Deal Registrations/Product Groups Get a product group  |
| Deal Registrations/Products - Get a product | Deal Registrations/Products Get a product  |
| Deal Registrations/Smart Actions - Get a smart action (Not Supported) | Deal Registrations/Smart Actions Get a smart action (Not Supported)  |
| Deal Registrations/Smart Actions/Smart Action REST Path Parameter Definitions - Get an action URL binding (Not Supported) | Deal Registrations/Smart Actions/Smart Action REST Path Parameter Definitions Get an action URL binding (Not Supported)  |
| Deal Registrations/Smart Actions/Smart Action REST Payload Definitions - Get an action request payload (Not Supported) | Deal Registrations/Smart Actions/Smart Action REST Payload Definitions Get an action request payload (Not Supported)  |
| Deal Registrations/Smart Actions/Smart Action User Interface Definitions - Get an action navigation (Not Supported) | Deal Registrations/Smart Actions/Smart Action User Interface Definitions Get an action navigation (Not Supported)  |
| Device Tokens - Get a device token | Device Tokens Get a device token  |
| Dynamic Link Patterns - Get a dynamic link pattern | Dynamic Link Patterns Get a dynamic link pattern  |
| Eligible Pay Groups List of Values - Get an eligible pay group | Eligible Pay Groups List of Values Get an eligible pay group  |
| Email Verifications - Get an email verification | Email Verifications Get an email verification  |
| End Periods - Get an end period for the period type | End Periods Get an end period for the period type  |
| Entitlements - Get a subscription entitlement | Entitlements Get a subscription entitlement  |
| Entitlements/Entitlement Details - Get a subscription entitlement detail | Entitlements/Entitlement Details Get a subscription entitlement detail  |
| Entitlements/Entitlement Details/Charge Adjustments - Get a subscription coverage adjustments | Entitlements/Entitlement Details/Charge Adjustments Get a subscription coverage adjustments  |
| Entitlements/Entitlement Details/Entitlement Results - Get a subscription entitlement result | Entitlements/Entitlement Details/Entitlement Results Get a subscription entitlement result  |
| Entitlements/Entitlement Details/Standard Coverages - Get a standard coverage | Entitlements/Entitlement Details/Standard Coverages Get a standard coverage  |
| Export Activities - Get a bulk export activity | Export Activities Get a bulk export activity  |
| Export Activities/Export Attachments - Get an attachment detail | Export Activities/Export Attachments Get an attachment detail  |
| Export Activities/Export Child Object Activities - Get a bulk export activity child object detail | Export Activities/Export Child Object Activities Get a bulk export activity child object detail  |
| Export Activities/Export Child Object Activities/Export Child Object Activities - Get a bulk export activity child object detail | Export Activities/Export Child Object Activities/Export Child Object Activities Get a bulk export activity child object detail  |
| Expression Detail Items List of Values - Get an expression detail item | Expression Detail Items List of Values Get an expression detail item  |
| Expression Detail Items List of Values/Expression Detail Basic Attribute Groups - Get a basic attribute group | Expression Detail Items List of Values/Expression Detail Basic Attribute Groups Get a basic attribute group  |
| Expression Detail Items List of Values/Expression Detail Basic Attribute Groups/Expression Detail Basic Attribute Names - Get a basic attribute name | Expression Detail Items List of Values/Expression Detail Basic Attribute Groups/Expression Detail Basic Attribute Names Get a basic attribute name  |
| Expression Detail Items List of Values/Expression Detail Function Groups - Get a function group | Expression Detail Items List of Values/Expression Detail Function Groups Get a function group  |
| Expression Detail Items List of Values/Expression Detail Function Groups/Expression Detail Function Names - Get a function name | Expression Detail Items List of Values/Expression Detail Function Groups/Expression Detail Function Names Get a function name  |
| Expression Detail Items List of Values/Expression Detail Operators - Get an operator | Expression Detail Items List of Values/Expression Detail Operators Get an operator  |
| Expression Detail Items List of Values/Expression Detail Performance Measures - Get a performance measure | Expression Detail Items List of Values/Expression Detail Performance Measures Get a performance measure  |
| Expression Detail Items List of Values/Expression Detail Performance Measures/Expression Detail Performance Measure Attributes - Get a performance measure attribute | Expression Detail Items List of Values/Expression Detail Performance Measures/Expression Detail Performance Measure Attributes Get a performance measure attribute  |
| Expression Detail Items List of Values/Expression Detail Plan Components - Get a plan component | Expression Detail Items List of Values/Expression Detail Plan Components Get a plan component  |
| Expression Detail Items List of Values/Expression Detail Plan Components/Expression Detail Plan Component Attributes - Get a plan component attribute | Expression Detail Items List of Values/Expression Detail Plan Components/Expression Detail Plan Component Attributes Get a plan component attribute  |
| Expression Detail Items List of Values/Expression Detail Plan Components/Expression Detail Plan Component Attributes/Expression Detail Plan Component Measures - Get a plan component performance measure | Expression Detail Items List of Values/Expression Detail Plan Components/Expression Detail Plan Component Attributes/Expression Detail Plan Component Measures Get a plan component performance measure  |
| Expression Detail Items List of Values/Expression Detail Plan Components/Expression Detail Plan Component Attributes/Expression Detail Plan Component Measures/Expression Detail Plan Component Measure Result Attributes - Get a plan component measure result attribute | Expression Detail Items List of Values/Expression Detail Plan Components/Expression Detail Plan Component Attributes/Expression Detail Plan Component Measures/Expression Detail Plan Component Measure Result Attributes Get a plan component measure result attribute  |
| Expression Detail Items List of Values/Expression Detail User Defined Functions - Get a user defined function | Expression Detail Items List of Values/Expression Detail User Defined Functions Get a user defined function  |
| Expression Detail Items List of Values/Expression Detail User Defined Query Value Sets - Get a user defined query | Expression Detail Items List of Values/Expression Detail User Defined Query Value Sets Get a user defined query  |
| Expression Usages List of Values - Get an expression usage | Expression Usages List of Values Get an expression usage  |
| Forecasts - Get a territory forecast | Forecasts Get a territory forecast  |
| Forecasts/Adjustment Periods - Get a period adjustment for a forecast | Forecasts/Adjustment Periods Get a period adjustment for a forecast  |
| Forecasts/Forecast Items - Get a forecast item | Forecasts/Forecast Items Get a forecast item  |
| Forecasts/Forecast Products - Get a forecast product | Forecasts/Forecast Products Get a forecast product  |
| Forecasts/Forecast Products/Product Adjustment Periods - Get a period adjustment for a forecast product | Forecasts/Forecast Products/Product Adjustment Periods Get a period adjustment for a forecast product  |
| Forecasts/Revenue Items - Get an unforecasted revenue item | Forecasts/Revenue Items Get an unforecasted revenue item  |
| Geographies List of Values - Get a geography | Geographies List of Values Get a geography  |
| GetAgentDetails - Get an agent | Agents The method returns an JSON Object with details about the registered agent's presence, availability, and capacity load. If the work type parameter is specified, then it also indicates if the agent can take work for a given work type. Get an agent  |
| Goal Metric - Get a Goal Metric | Goal Metric Get a Goal Metric  |
| Goal Metric/Goal Metric Breakdowns - Get a goal metric breakdown | Goal Metric/Goal Metric Breakdowns Get a goal metric breakdown  |
| Goal Metric/Goal Metric Detail - Get a Goal Metric Detail | Goal Metric/Goal Metric Detail Get a Goal Metric Detail  |
| Goals - Get a goal | Goals Get a goal  |
| Goals/Goal History - Get a goal history record | Goals/Goal History Get a goal history record  |
| Goals/Goal Metric - Get a Goal Metric | Goals/Goal Metric Get a Goal Metric  |
| Goals/Goal Metric/Goal Metric Breakdowns - Get a goal metric breakdown | Goals/Goal Metric/Goal Metric Breakdowns Get a goal metric breakdown  |
| Goals/Goal Metric/Goal Metric Detail - Get a Goal Metric Detail | Goals/Goal Metric/Goal Metric Detail Get a Goal Metric Detail  |
| Goals/Goal Participants - Get a goal participant | Goals/Goal Participants Get a goal participant  |
| HCM Service Requests - Get a service request | HCM Service Requests Get a service request  |
| HCM Service Requests/Attachments - Get an attachment | HCM Service Requests/Attachments Get an attachment  |
| HCM Service Requests/Channel Communications - Get a channel communication | HCM Service Requests/Channel Communications Get a channel communication  |
| HCM Service Requests/Contact Members - Get a contact member | HCM Service Requests/Contact Members Get a contact member  |
| HCM Service Requests/Interaction References - Get a service request interaction | HCM Service Requests/Interaction References Get a service request interaction  |
| HCM Service Requests/Messages - Get a message | HCM Service Requests/Messages Get a message  |
| HCM Service Requests/Messages/Attachments - Get an attachment | HCM Service Requests/Messages/Attachments Get an attachment  |
| HCM Service Requests/Messages/Message Channels - Get a channel communication message | HCM Service Requests/Messages/Message Channels Get a channel communication message  |
| HCM Service Requests/Milestones - Get a service request milestone | HCM Service Requests/Milestones Get a service request milestone  |
| HCM Service Requests/Milestones/Milestones History - Get a service request milestone history | HCM Service Requests/Milestones/Milestones History Get a service request milestone history  |
| HCM Service Requests/Resources - Get a resource member | HCM Service Requests/Resources Get a resource member  |
| HCM Service Requests/Service Request References - Get a service request reference | HCM Service Requests/Service Request References Get a service request reference  |
| HCM Service Requests/Smart Actions - Get an action | HCM Service Requests/Smart Actions Get an action  |
| HCM Service Requests/Smart Actions/Smart Action REST Payload Definitions - Get an action request payload definition | HCM Service Requests/Smart Actions/Smart Action REST Payload Definitions Get an action request payload definition  |
| HCM Service Requests/Smart Actions/Smart Action User Interface Definitions - Get an action navigation definition | HCM Service Requests/Smart Actions/Smart Action User Interface Definitions Get an action navigation definition  |
| HCM Service Requests/Smart Actions/Smart Action User Interface Definitions - Get an action path parameter | HCM Service Requests/Smart Actions/Smart Action User Interface Definitions Get an action path parameter  |
| HCM Service Requests/Tags - Get a tag | HCM Service Requests/Tags Get a tag  |
| Households - Get a household | Households Get a household  |
| Households/Additional Identifiers - Get an additional identifier | Households/Additional Identifiers Get an additional identifier  |
| Households/Additional Names - Get an additional name | Households/Additional Names Get an additional name  |
| Households/Addresses - Get an address | Households/Addresses Get an address  |
| Households/Addresses/Address Locales - Get an address locale | Households/Addresses/Address Locales Get an address locale  |
| Households/Addresses/Address Purposes - Get an address purpose | Households/Addresses/Address Purposes Get an address purpose  |
| Households/Attachments - Get an attachment | Households/Attachments Get an attachment  |
| Households/Aux Classifications - Get a customer classification | Households/Aux Classifications Get a customer classification  |
| Households/Contact Points - Get a contact point | Households/Contact Points Get a contact point  |
| Households/Notes - Get a note | Households/Notes Get a note  |
| Households/Primary Addresses - Get a primary address | Households/Primary Addresses Get a primary address  |
| Households/Relationships - Get a relationship | Households/Relationships Get a relationship  |
| Households/Sales Account Resources - Get a sales team member | Households/Sales Account Resources Get a sales team member  |
| Households/Source System References - Get a source system reference | Households/Source System References Get a source system reference  |
| Hub Organizations - Get an organization | Hub Organizations Get an organization  |
| Hub Organizations/Additional Identifiers - Get an additional identifier | Hub Organizations/Additional Identifiers Get an additional identifier  |
| Hub Organizations/Additional Names - Get an additional name | Hub Organizations/Additional Names Get an additional name  |
| Hub Organizations/Addresses - Get an address | Hub Organizations/Addresses Get an address  |
| Hub Organizations/Contact Points - Get a contact point | Hub Organizations/Contact Points Get a contact point  |
| Hub Organizations/Customer Classifications - Get a customer classification | Hub Organizations/Customer Classifications Get a customer classification  |
| Hub Organizations/Notes - Get a note | Hub Organizations/Notes Get a note  |
| Hub Organizations/Organization Attachments - Get an attachment | Hub Organizations/Organization Attachments Get an attachment  |
| Hub Organizations/Party Usage Assignments - Get a usage assignment | Hub Organizations/Party Usage Assignments Get a usage assignment  |
| Hub Organizations/Relationships - Get a relationship | Hub Organizations/Relationships Get a relationship  |
| Hub Organizations/Source System References - Get a source system reference | Hub Organizations/Source System References Get a source system reference  |
| Hub Persons - Get a person | Hub Persons Get a person  |
| Hub Persons/Additional Identifiers - Get an additional identifier | Hub Persons/Additional Identifiers Get an additional identifier  |
| Hub Persons/Additional Names - Get an additional name | Hub Persons/Additional Names Get an additional name  |
| Hub Persons/Addresses - Get an address | Hub Persons/Addresses Get an address  |
| Hub Persons/Attachments - Get an attachment | Hub Persons/Attachments Get an attachment  |
| Hub Persons/Contact Points - Get a contact point | Hub Persons/Contact Points Get a contact point  |
| Hub Persons/Customer Classifications - Get a customer classification | Hub Persons/Customer Classifications Get a customer classification  |
| Hub Persons/Notes - Get a note | Hub Persons/Notes Get a note  |
| Hub Persons/Relationships - Get a relationship | Hub Persons/Relationships Get a relationship  |
| Hub Persons/Source System References - Get a source system reference | Hub Persons/Source System References Get a source system reference  |
| Hub Persons/Usage Assignments - Get a usage assignment | Hub Persons/Usage Assignments Get a usage assignment  |
| Import Activities - Get a file import activity | Import Activities Get a file import activity  |
| Import Activities/Import Activity Data Files - Get a file import activity data file | Import Activities/Import Activity Data Files Get a file import activity data file  |
| Import Activity Maps - Get a file import map | Import Activity Maps Get a file import map  |
| Import Activity Maps/Import Activity Map Columns - Get a file import map column | Import Activity Maps/Import Activity Map Columns Get a file import map column  |
| Inbound Message Filters - Get an inbound message filter | Inbound Message Filters Get an inbound message filter  |
| Inbound Messages - Get an inbound message | Inbound Messages Get an inbound message  |
| Inbound Messages/Attachments - Get an attachment | Inbound Messages/Attachments Get an attachment  |
| Inbound Messages/Inbound Message Parts - Get an inbound message part | Inbound Messages/Inbound Message Parts Get an inbound message part  |
| Incentive Compensation Earnings - Get an earning | Incentive Compensation Earnings Get an earning  |
| Incentive Compensation Expressions - Get an expression | Incentive Compensation Expressions Get an expression  |
| Incentive Compensation Expressions/Expression Details - Get an expression detail | Incentive Compensation Expressions/Expression Details Get an expression detail  |
| Incentive Compensation Expressions/Expression Usages - Get an expression usage | Incentive Compensation Expressions/Expression Usages Get an expression usage  |
| Incentive Compensation Rule Hierarchies - Get a rule hierarchy | Incentive Compensation Rule Hierarchies Get a rule hierarchy  |
| Incentive Compensation Rule Hierarchies/Qualifying Criteria - Get a qualifying criterion | Incentive Compensation Rule Hierarchies/Qualifying Criteria Get a qualifying criterion  |
| Incentive Compensation Rule Hierarchies/Qualifying Criteria/Qualifying Attribute Values - Get a qualifying attribute value | Incentive Compensation Rule Hierarchies/Qualifying Criteria/Qualifying Attribute Values Get a qualifying attribute value  |
| Incentive Compensation Rule Hierarchies/Rule Assignments - Get a rule assignment | Incentive Compensation Rule Hierarchies/Rule Assignments Get a rule assignment  |
| Incentive Compensation Rule Hierarchies/Rules - Get a rule | Incentive Compensation Rule Hierarchies/Rules Get a rule  |
| Incentive Compensation Summarized Credits - Get a credit | Incentive Compensation Summarized Credits Get a credit  |
| Incentive Compensation Summarized Earnings - Get a summarized earning | Incentive Compensation Summarized Earnings Get a summarized earning  |
| Incentive Compensation Summarized Earnings Per Intervals - Get an earning | Incentive Compensation Summarized Earnings Per Intervals Get an earning  |
| Incentive Compensation Summarized Team Credits - Get a credit | Incentive Compensation Summarized Team Credits Get a credit  |
| Incentive Compensation Transactions - Get a transaction | Incentive Compensation Transactions Get a transaction  |
| Incentive Compensation Transactions/Credits - Get a credit | Incentive Compensation Transactions/Credits Get a credit  |
| Incentive Compensation Transactions/Transaction Descriptive Flex Fields - Get a transaction descriptive flex field | Incentive Compensation Transactions/Transaction Descriptive Flex Fields Get a transaction descriptive flex field  |
| Industries List of Values - Get an industry | Industries List of Values Get an industry  |
| Interactions - Get an interaction | Interactions Get an interaction  |
| Interactions/Child Interactions - Get a child interaction | Interactions/Child Interactions Get a child interaction  |
| Interactions/Interaction Participants - Get an interaction participant | Interactions/Interaction Participants Get an interaction participant  |
| Interactions/Interaction References - Get an interaction reference | Interactions/Interaction References Get an interaction reference  |
| Internal Service Requests - Get a service request | Internal Service Requests Get a service request  |
| Internal Service Requests/Attachments - Get an attachment | Internal Service Requests/Attachments Get an attachment  |
| Internal Service Requests/Channel Communications - Get a channel communication | Internal Service Requests/Channel Communications Get a channel communication  |
| Internal Service Requests/Contact Members - Get a contact member | Internal Service Requests/Contact Members Get a contact member  |
| Internal Service Requests/Interaction References - Get a service request interaction | Internal Service Requests/Interaction References Get a service request interaction  |
| Internal Service Requests/Messages - Get a message | Internal Service Requests/Messages Get a message  |
| Internal Service Requests/Messages/Attachments - Get an attachment | Internal Service Requests/Messages/Attachments Get an attachment  |
| Internal Service Requests/Messages/Message Channels - Get a channel communication message | Internal Service Requests/Messages/Message Channels Get a channel communication message  |
| Internal Service Requests/Milestones - Get a service request milestone | Internal Service Requests/Milestones Get a service request milestone  |
| Internal Service Requests/Milestones/Milestones History - Get a service request milestone history | Internal Service Requests/Milestones/Milestones History Get a service request milestone history  |
| Internal Service Requests/Resources - Get a resource member | Internal Service Requests/Resources Get a resource member  |
| Internal Service Requests/Service Request References - Get a service request reference | Internal Service Requests/Service Request References Get a service request reference  |
| Internal Service Requests/Smart Actions - Get an action | Internal Service Requests/Smart Actions Get an action  |
| Internal Service Requests/Smart Actions/Smart Action REST Payload Definitions - Get an action request payload definition | Internal Service Requests/Smart Actions/Smart Action REST Payload Definitions Get an action request payload definition  |
| Internal Service Requests/Smart Actions/Smart Action User Interface Definitions - Get an action navigation definition | Internal Service Requests/Smart Actions/Smart Action User Interface Definitions Get an action navigation definition  |
| Internal Service Requests/Smart Actions/Smart Action User Interface Definitions - Get an action path parameter | Internal Service Requests/Smart Actions/Smart Action User Interface Definitions Get an action path parameter  |
| Internal Service Requests/Tags - Get a tag | Internal Service Requests/Tags Get a tag  |
| Lightbox Documents - Get a Lightbox document | Lightbox Documents Get a Lightbox document  |
| Lightbox Documents/Document-shared-with-user Signals - Get a document-shared-with-user signal for a docum... | Lightbox Documents/Document-shared-with-user Signals Get a document-shared-with-user signal for a document  |
| Lightbox Documents/Document-unshared-with-user Signals - Get a document-unshared-with-user signal for a doc... | Lightbox Documents/Document-unshared-with-user Signals Get a document-unshared-with-user signal for a document  |
| Lightbox Documents/Document-viewed-outside-Lightbox Signals - Get a document-viewed-outside-Lightbox signal for ... | Lightbox Documents/Document-viewed-outside-Lightbox Signals Get a document-viewed-outside-Lightbox signal for a document  |
| Lightbox Documents/Document-Viewed-Within-Lightbox Signals - Get a document-viewed-within-Lightbox signal for a... | Lightbox Documents/Document-Viewed-Within-Lightbox Signals Get a document-viewed-within-Lightbox signal for a document  |
| Lightbox Documents/Lightbox Document Pages - Get a Lightbox document page | Lightbox Documents/Lightbox Document Pages Get a Lightbox document page  |
| Lightbox Documents/Lightbox Document Pages/Downloaded-from-cart Signals - Get a downloaded-from-cart signal for a page | Lightbox Documents/Lightbox Document Pages/Downloaded-from-cart Signals Get a downloaded-from-cart signal for a page  |
| Lightbox Documents/Lightbox Document Pages/Page-added-to-cart Signals - Get a page-added-to-cart signal for a page | Lightbox Documents/Lightbox Document Pages/Page-added-to-cart Signals Get a page-added-to-cart signal for a page  |
| Lightbox Documents/Lightbox Document Pages/Page-viewed-outside-Lightbox Signals - Get a page-viewed-outside-Lightbox signal for a pa... | Lightbox Documents/Lightbox Document Pages/Page-viewed-outside-Lightbox Signals Get a page-viewed-outside-Lightbox signal for a page  |
| Lightbox Documents/Lightbox Scoring Signals - Get a signal for a downloaded document | Lightbox Documents/Lightbox Scoring Signals Get a signal for a downloaded document  |
| Lightbox Documents/Shared-person Instances - Get a shared-person instance for a document | Lightbox Documents/Shared-person Instances Get a shared-person instance for a document  |
| Lightbox Presentation Session Feedback - Get a lightbox presentation session feedback item | Lightbox Presentation Session Feedback Get a lightbox presentation session feedback item  |
| Lightbox Presentation Sessions - Get a Lightbox presentation session | Lightbox Presentation Sessions Get a Lightbox presentation session  |
| Lightbox Presentation Sessions/Presentation Session Attendance Events - Get a Lightbox presentation session attendance eve... | Lightbox Presentation Sessions/Presentation Session Attendance Events Get a Lightbox presentation session attendance event  |
| Lightbox Presentation Sessions/Presentation Session Display Events - Get a Lightbox presentation session display event | Lightbox Presentation Sessions/Presentation Session Display Events Get a Lightbox presentation session display event  |
| List of Values/Action Status Conditions - Get a status condition | List of Values/Action Status Conditions Get a status condition  |
| List of Values/Activity Functions - Get an activity function | List of Values/Activity Functions Get an activity function  |
| List of Values/Activity Functions/Activity Type Lookups - Get an activity type | List of Values/Activity Functions/Activity Type Lookups Get an activity type  |
| List of Values/Activity Functions/Activity Type Lookups/Activity Subtypes - Get an activity subtype | List of Values/Activity Functions/Activity Type Lookups/Activity Subtypes Get an activity subtype  |
| List of Values/Activity Subtypes - Get an activity subtype | List of Values/Activity Subtypes Get an activity subtype  |
| List of Values/Activity Type Lookups - Get an activity type | List of Values/Activity Type Lookups Get an activity type  |
| List of Values/Application Roles - Get a single application role | List of Values/Application Roles Get a single application role  |
| List of Values/Assessment Templates - Get an assessment template | List of Values/Assessment Templates Get an assessment template  |
| List of Values/Benefit List Type Values - Get a benefit list type value | List of Values/Benefit List Type Values Get a benefit list type value  |
| List of Values/Capacity Categories - Get a capacity category | List of Values/Capacity Categories Get a capacity category  |
| List of Values/Capacity Categories/Capacities - Get a capacity | List of Values/Capacity Categories/Capacities Get a capacity  |
| List of Values/Capacity Categories/Time Slots - Get a time slot | List of Values/Capacity Categories/Time Slots Get a time slot  |
| List of Values/Channel Types - Get a channel type | List of Values/Channel Types Get a channel type  |
| List of Values/Channel Types/Channels - Get a channel associated with a channel type | List of Values/Channel Types/Channels Get a channel associated with a channel type  |
| List of Values/Class Codes - Get a classification code | List of Values/Class Codes Get a classification code  |
| List of Values/Collaboration Recipients - Get a collaboration recipient | List of Values/Collaboration Recipients Get a collaboration recipient  |
| List of Values/Dynamic Link Values - Get a business unit | List of Values/Dynamic Link Values Get a business unit  |
| List of Values/Feedback Ratings - Get a feedback rating | List of Values/Feedback Ratings Get a feedback rating  |
| List of Values/Geographies - Get a geography | List of Values/Geographies Get a geography  |
| List of Values/Get Configurations - Get a configuration (Not Supported) | List of Values/Get Configurations Get a configuration (Not Supported)  |
| List of Values/HR Service Request Resolve Outcomes - Get an outcome | List of Values/HR Service Request Resolve Outcomes Get an outcome  |
| List of Values/HR Service Request Resolve Outcomes/HR Service Request Resolve Outcome Resolutions - Get an outcome resolution | List of Values/HR Service Request Resolve Outcomes/HR Service Request Resolve Outcome Resolutions Get an outcome resolution  |
| List of Values/HZ Lookups - Get a lookup | List of Values/HZ Lookups Get a lookup  |
| List of Values/Import Export Objects Metadata - Get a file import and export object | List of Values/Import Export Objects Metadata Get a file import and export object  |
| List of Values/Import Export Objects Metadata/Import Export Object Attributes - Get a file import and export object attribute | List of Values/Import Export Objects Metadata/Import Export Object Attributes Get a file import and export object attribute  |
| List of Values/Internal Service Request Outcomes - Get an outcome | List of Values/Internal Service Request Outcomes Get an outcome  |
| List of Values/Internal Service Request Outcomes/Internal Service Request Outcome Resolutions - Get an outcome resolution | List of Values/Internal Service Request Outcomes/Internal Service Request Outcome Resolutions Get an outcome resolution  |
| List of Values/KPI - Get a KPI | List of Values/KPI Get a KPI  |
| List of Values/KPI/KPI History Metadata - Get a KPI history metadata | List of Values/KPI/KPI History Metadata Get a KPI history metadata  |
| List of Values/Lightbox Document Types - Get a lightbox document type | List of Values/Lightbox Document Types Get a lightbox document type  |
| List of Values/Loy Events - Get an event | List of Values/Loy Events Get an event  |
| List of Values/Loy PointTypes - Get a point type | List of Values/Loy PointTypes Get a point type  |
| List of Values/Name and ID Types - Get a Name and Type | List of Values/Name and ID Types Get a Name and Type  |
| List of Values/Note Types - Get a note type | List of Values/Note Types Get a note type  |
| List of Values/Opportunity Status Values - Get an opportunity status value | List of Values/Opportunity Status Values Get an opportunity status value  |
| List of Values/Partner Contact Managers List of Values - Get a partner contact manager | List of Values/Partner Contact Managers List of Values Get a partner contact manager  |
| List of Values/Partner Enrolled Programs List of Values - Get a partner enrolled program | List of Values/Partner Enrolled Programs List of Values Get a partner enrolled program  |
| List of Values/Partner Programs - Get a partner program | List of Values/Partner Programs Get a partner program  |
| List of Values/Partner Types List of Values - Get a partner type | List of Values/Partner Types List of Values Get a partner type  |
| List of Values/Party Usages - Get a party usage | List of Values/Party Usages Get a party usage  |
| List of Values/Phone Country Codes - Get a Country Code | List of Values/Phone Country Codes Get a Country Code  |
| List of Values/Product Groups - Get a product group | List of Values/Product Groups Get a product group  |
| List of Values/Program Tiers - Get a program tier | List of Values/Program Tiers Get a program tier  |
| List of Values/Rated Currencies - Get a rated currency | List of Values/Rated Currencies Get a rated currency  |
| List of Values/Relationship Types - Get a relationship type | List of Values/Relationship Types Get a relationship type  |
| List of Values/Sales Methods - Get a sales method | List of Values/Sales Methods Get a sales method  |
| List of Values/Sales Stages - Get a sales stage | List of Values/Sales Stages Get a sales stage  |
| List of Values/Service Business Units - Get a service business unit | List of Values/Service Business Units Get a service business unit  |
| List of Values/Service Business Units/Business Unit Lookups - Get a reference set enabled lookup | List of Values/Service Business Units/Business Unit Lookups Get a reference set enabled lookup  |
| List of Values/Service Business Units/Categories - Get a category | List of Values/Service Business Units/Categories Get a category  |
| List of Values/Service Business Units/Channels - Get a channel | List of Values/Service Business Units/Channels Get a channel  |
| List of Values/Service Business Units/Profile Options - Get a profile option | List of Values/Service Business Units/Profile Options Get a profile option  |
| List of Values/Service Lookup Properties - Get a service lookup property | List of Values/Service Lookup Properties Get a service lookup property  |
| List of Values/Service Request Outcomes - Get an outcome | List of Values/Service Request Outcomes Get an outcome  |
| List of Values/Service Request Outcomes/Service Request Outcome Resolutions - Get an outcome resolution | List of Values/Service Request Outcomes/Service Request Outcome Resolutions Get an outcome resolution  |
| List of Values/Service Request Statuses - Get a lookup code | List of Values/Service Request Statuses Get a lookup code  |
| List of Values/Service Request Statuses - Get a lookup code | List of Values/Service Request Statuses Get a lookup code  |
| List of Values/Service Work Order Areas - Get a work area | List of Values/Service Work Order Areas Get a work area  |
| List of Values/Service Work Order Areas/Work Order Areas - Get a work order area | List of Values/Service Work Order Areas/Work Order Areas Get a work order area  |
| List of Values/Smart Text  Folders and Texts - Get a folder or smart text | List of Values/Smart Text  Folders and Texts Get a folder or smart text  |
| List of Values/Smart Text  Folders and Texts/Parents - Get parent folder or smart text | List of Values/Smart Text  Folders and Texts/Parents Get parent folder or smart text  |
| List of Values/Smart Text Objects - Get all smart text objects | List of Values/Smart Text Objects Get all smart text objects  |
| List of Values/Smart Text Variables - Get a smart text variable | List of Values/Smart Text Variables Get a smart text variable  |
| List of Values/Source Codes - Get a source code | List of Values/Source Codes Get a source code  |
| List of Values/Tags - Get a tag | List of Values/Tags Get a tag  |
| List of Values/Work Order Status Codes - Get a work order status code | List of Values/Work Order Status Codes Get a work order status code  |
| List of Values/Work Order Type Mappings - Get a work order type mapping | List of Values/Work Order Type Mappings Get a work order type mapping  |
| Lookups - Get a lookup | Lookups Get a lookup  |
| MDF Requests - Get an MDF request | MDF Requests Get an MDF request  |
| MDF Requests/Claims - Get a claim | MDF Requests/Claims Get a claim  |
| MDF Requests/MDF Request Teams - Get a fund request resource | MDF Requests/MDF Request Teams Get a fund request resource  |
| MDF Requests/Notes - Get a note | MDF Requests/Notes Get a note  |
| Milestones - Get a milestone | Milestones Get a milestone  |
| Multi Channel Adapter Events - Get an event | Multi Channel Adapter Events Get an event  |
| Multi-Channel Adapter Toolbars - Get a toolbar | Multi-Channel Adapter Toolbars Get a toolbar  |
| Multi-Channel Adapter Toolbars/Multi-Channel Adapter Toolbar Additions - Get a toolbar addition | Multi-Channel Adapter Toolbars/Multi-Channel Adapter Toolbar Additions Get a toolbar addition  |
| My Self-Service Roles - Get a self-service role | My Self-Service Roles Get a self-service role  |
| Non-Duplicate Records - Get a non-duplicate record | Non-Duplicate Records Get a non-duplicate record  |
| Object Capacities - Get an object capacity | Object Capacities Get an object capacity  |
| Objectives - Get an objective | Objectives Get an objective  |
| Objectives/Objective Splits - Get an objective split | Objectives/Objective Splits Get an objective split  |
| Omnichannel Presence - Get an omnichannel presence | Omnichannel Presence Get an omnichannel presence  |
| Omnichannel Properties - Get an omnichannel property | Omnichannel Properties Get an omnichannel property  |
| Opportunities - Get an opportunity | Opportunities Get an opportunity  |
| Opportunities/Assessments - Get an assessment | Opportunities/Assessments Get an assessment  |
| Opportunities/Assessments/Assessment Answer Groups - Get an assessment answer group | Opportunities/Assessments/Assessment Answer Groups Get an assessment answer group  |
| Opportunities/Assessments/Assessment Answer Groups/Assessment Answers - Get an assessment answer | Opportunities/Assessments/Assessment Answer Groups/Assessment Answers Get an assessment answer  |
| Opportunities/Notes - Get a note | Opportunities/Notes Get a note  |
| Opportunities/Opportunity Competitors - Get an opportunity competitor | Opportunities/Opportunity Competitors Get an opportunity competitor  |
| Opportunities/Opportunity Contacts - Get an opportunity contact | Opportunities/Opportunity Contacts Get an opportunity contact  |
| Opportunities/Opportunity Deals - Get an opportunity deal | Opportunities/Opportunity Deals Get an opportunity deal  |
| Opportunities/Opportunity Leads - Get an opportunity lead | Opportunities/Opportunity Leads Get an opportunity lead  |
| Opportunities/Opportunity Partners - Get an opportunity revenue partner | Opportunities/Opportunity Partners Get an opportunity revenue partner  |
| Opportunities/Opportunity Sources - Get an opportunity source | Opportunities/Opportunity Sources Get an opportunity source  |
| Opportunities/Opportunity Stage Snapshots - Get an opportunity stage snapshot | Opportunities/Opportunity Stage Snapshots Get an opportunity stage snapshot  |
| Opportunities/Opportunity Team Members - Get an opportunity team member | Opportunities/Opportunity Team Members Get an opportunity team member  |
| Opportunities/Revenue Items - Get an opportunity revenue | Opportunities/Revenue Items Get an opportunity revenue  |
| Opportunities/Revenue Items/Child Split Revenues - Get a child split revenue | Opportunities/Revenue Items/Child Split Revenues Get a child split revenue  |
| Opportunities/Revenue Items/Opportunity Revenue Territories - Get an opportunity revenue territory | Opportunities/Revenue Items/Opportunity Revenue Territories Get an opportunity revenue territory  |
| Opportunities/Revenue Items/Product Groups - Get a product group | Opportunities/Revenue Items/Product Groups Get a product group  |
| Opportunities/Revenue Items/Products - Get a product | Opportunities/Revenue Items/Products Get a product  |
| Opportunities/Revenue Items/Recurring Revenues - Get a recurring revenue | Opportunities/Revenue Items/Recurring Revenues Get a recurring revenue  |
| Opportunities/Smart Actions - Get a smart action (Not Supported) | Opportunities/Smart Actions Get a smart action (Not Supported)  |
| Opportunities/Smart Actions/Smart Action REST Path Parameter Definitions - Get an action URL binding (Not Supported) | Opportunities/Smart Actions/Smart Action REST Path Parameter Definitions Get an action URL binding (Not Supported)  |
| Opportunities/Smart Actions/Smart Action REST Payload Definitions - Get an action request payload (Not Supported) | Opportunities/Smart Actions/Smart Action REST Payload Definitions Get an action request payload (Not Supported)  |
| Opportunities/Smart Actions/Smart Action User Interface Definitions - Get an action navigation (Not Supported) | Opportunities/Smart Actions/Smart Action User Interface Definitions Get an action navigation (Not Supported)  |
| Outbound Messages - Get an outbound message | Outbound Messages Get an outbound message  |
| Outbound Messages/Outbound Message Parts - Get an outbound message part | Outbound Messages/Outbound Message Parts Get an outbound message part  |
| Participant Compensation Plans - Get a participant compensation plan | Participant Compensation Plans Get a participant compensation plan  |
| Participant Compensation Plans/Descriptive Flex Fields - Get descriptive flex fields for a participant plan | Participant Compensation Plans/Descriptive Flex Fields Get descriptive flex fields for a participant plan  |
| Participant Compensation Plans/Plan Components - Get a plan component | Participant Compensation Plans/Plan Components Get a plan component  |
| Participant Compensation Plans/Plan Components/Performance Measures - Get a performance measure | Participant Compensation Plans/Plan Components/Performance Measures Get a performance measure  |
| Participant Compensation Plans/Plan Components/Performance Measures/Goals - Get a performance measure goal | Participant Compensation Plans/Plan Components/Performance Measures/Goals Get a performance measure goal  |
| Participant Compensation Plans/Plan Components/Performance Measures/Goals/Interval Goals - Get an interval goal | Participant Compensation Plans/Plan Components/Performance Measures/Goals/Interval Goals Get an interval goal  |
| Participant Compensation Plans/Plan Components/Performance Measures/Goals/Period Goals - Get a period goal | Participant Compensation Plans/Plan Components/Performance Measures/Goals/Period Goals Get a period goal  |
| Participant Compensation Plans/Plan Components/Performance Measures/Scorecards - Get a performance measure scorecard | Participant Compensation Plans/Plan Components/Performance Measures/Scorecards Get a performance measure scorecard  |
| Participant Compensation Plans/Plan Components/Performance Measures/Scorecards/Scorecard Rates - Get a scorecard rate | Participant Compensation Plans/Plan Components/Performance Measures/Scorecards/Scorecard Rates Get a scorecard rate  |
| Participant Compensation Plans/Plan Components/Rate Tables - Get a plan component rate table | Participant Compensation Plans/Plan Components/Rate Tables Get a plan component rate table  |
| Participant Compensation Plans/Plan Components/Rate Tables/Rate Table Rates - Get a rate table rate | Participant Compensation Plans/Plan Components/Rate Tables/Rate Table Rates Get a rate table rate  |
| Participants - Get a participant | Participants Get a participant  |
| Participants List of Values - Get a participant | Participants List of Values Get a participant  |
| Participants/Participant Details - Get a participant detail | Participants/Participant Details Get a participant detail  |
| Participants/Participant Details/Participant Details Descriptive Flex Fields - Get a descriptive flex field | Participants/Participant Details/Participant Details Descriptive Flex Fields Get a descriptive flex field  |
| Participants/Participant Roles - Get a participant role | Participants/Participant Roles Get a participant role  |
| Participants/Participants Descriptive Flex Fields - Get a descriptive flex field | Participants/Participants Descriptive Flex Fields Get a descriptive flex field  |
| Partner Contacts - Get a partner contact | Partner Contacts Get a partner contact  |
| Partner Contacts/Attachments - Get an attachment | Partner Contacts/Attachments Get an attachment  |
| Partner Contacts/Partner Contact User Details - Get an user detail | Partner Contacts/Partner Contact User Details Get an user detail  |
| Partner Programs - Get a partner program | Partner Programs Get a partner program  |
| Partner Programs/Countries - Get a country for a partner program | Partner Programs/Countries Get a country for a partner program  |
| Partner Programs/Program Benefit Details - Get a program benefit for a partner program | Partner Programs/Program Benefit Details Get a program benefit for a partner program  |
| Partner Programs/Tiers - Get a tier for a partner program | Partner Programs/Tiers Get a tier for a partner program  |
| Partner Tiers - Get a partner tier | Partner Tiers Get a partner tier  |
| Partners - Get a partner | Partners Get a partner  |
| Partners/Addresses - Get an address | Partners/Addresses Get an address  |
| Partners/Attachments - Get a partner attachment | Partners/Attachments Get a partner attachment  |
| Partners/Expertises - Get an expertise for a partner | Partners/Expertises Get an expertise for a partner  |
| Partners/Focus Areas - Get a product specialty for a partner | Partners/Focus Areas Get a product specialty for a partner  |
| Partners/Geographies - Get a geography for a partner | Partners/Geographies Get a geography for a partner  |
| Partners/Industries - Get an industry for a partner | Partners/Industries Get an industry for a partner  |
| Partners/Notes - Get a note for a partner | Partners/Notes Get a note for a partner  |
| Partners/Partner Account Team Members - Get a partner account team member for a partner | Partners/Partner Account Team Members Get a partner account team member for a partner  |
| Partners/Partner Announcements - Get a partner announcement | Partners/Partner Announcements Get a partner announcement  |
| Partners/Partner Certifications - Get a certification for a partner | Partners/Partner Certifications Get a certification for a partner  |
| Partners/Partner Contacts - Get a partner contact | Partners/Partner Contacts Get a partner contact  |
| Partners/Partner Contacts/Attachments - Get a partner attachment | Partners/Partner Contacts/Attachments Get a partner attachment  |
| Partners/Partner Contacts/User Account Details - Get a partner contact user details | Partners/Partner Contacts/User Account Details Get a partner contact user details  |
| Partners/Partner Types - Get a partner type | Partners/Partner Types Get a partner type  |
| Partners/Smart Actions - Get a smart action (Not Supported) | Partners/Smart Actions Get a smart action (Not Supported)  |
| Partners/Smart Actions/Smart Action REST Path Parameter Definitions - Get an action URL binding (Not Supported) | Partners/Smart Actions/Smart Action REST Path Parameter Definitions Get an action URL binding (Not Supported)  |
| Partners/Smart Actions/Smart Action REST Payload Definitions - Get an action request payload (Not Supported) | Partners/Smart Actions/Smart Action REST Payload Definitions Get an action request payload (Not Supported)  |
| Partners/Smart Actions/Smart Action User Interface Definitions - Get an action navigation (Not Supported) | Partners/Smart Actions/Smart Action User Interface Definitions Get an action navigation (Not Supported)  |
| Passive Beacon Service Statuses - Get a passive beacon provider status (Not Supporte... | Passive Beacon Service Statuses Get a passive beacon provider status (Not Supported)  |
| Passive Beacon Service Statuses/Passive Beacon Provider Statuses - Get a passive beacon service status (Not Supported... | Passive Beacon Service Statuses/Passive Beacon Provider Statuses Get a passive beacon service status (Not Supported)  |
| Pay Groups - Get a pay group | Pay Groups Get a pay group  |
| Pay Groups/Pay Group Assignments - Get a pay group assignment | Pay Groups/Pay Group Assignments Get a pay group assignment  |
| Pay Groups/Pay Group Roles - Get a pay group role | Pay Groups/Pay Group Roles Get a pay group role  |
| Payment Batch Participant Plan Components List of Values - Get a payment batch participant plan component | Payment Batch Participant Plan Components List of Values Get a payment batch participant plan component  |
| Payment Batches - Get a payment batch | Payment Batches Get a payment batch  |
| Payment Batches/Payment Transactions - Get a payment transaction | Payment Batches/Payment Transactions Get a payment transaction  |
| Payment Batches/Paysheets - Get a paysheet | Payment Batches/Paysheets Get a paysheet  |
| Payment Batches/Paysheets/Payment Transactions - Get a payment transaction | Payment Batches/Paysheets/Payment Transactions Get a payment transaction  |
| Payment Plans - Get a payment plan | Payment Plans Get a payment plan  |
| Payment Plans/Payment Plans Assignments - Get a payment plan assignment | Payment Plans/Payment Plans Assignments Get a payment plan assignment  |
| Payment Plans/Payment Plans Roles - Get a payment plan role | Payment Plans/Payment Plans Roles Get a payment plan role  |
| Payment Transactions - Get a payment transaction | Payment Transactions Get a payment transaction  |
| Paysheets - Get a paysheet | Paysheets Get a paysheet  |
| Paysheets/Payment Transactions - Get a payment transaction | Paysheets/Payment Transactions Get a payment transaction  |
| Performance Intervals List of Values - Get an interval type | Performance Intervals List of Values Get an interval type  |
| Performance Measures - Get a performance measure | Performance Measures Get a performance measure  |
| Performance Measures/Credit Categories - Get a credit category | Performance Measures/Credit Categories Get a credit category  |
| Performance Measures/Credit Categories/Credit Factors - Get a credit factor | Performance Measures/Credit Categories/Credit Factors Get a credit factor  |
| Performance Measures/Credit Categories/Transaction Factors - Get a transaction factor | Performance Measures/Credit Categories/Transaction Factors Get a transaction factor  |
| Performance Measures/Descriptive Flex fields - Get a descriptive flex field | Performance Measures/Descriptive Flex fields Get a descriptive flex field  |
| Performance Measures/Goals - Get a goal | Performance Measures/Goals Get a goal  |
| Performance Measures/Goals/Interval Goals - Get an interval goal | Performance Measures/Goals/Interval Goals Get an interval goal  |
| Performance Measures/Goals/Interval Goals/Period Goals - Get a period goal | Performance Measures/Goals/Interval Goals/Period Goals Get a period goal  |
| Performance Measures/Rate Dimensional Input Expressions - Get a rate dimensional input | Performance Measures/Rate Dimensional Input Expressions Get a rate dimensional input  |
| Performance Measures/Scorecards - Get a scorecard | Performance Measures/Scorecards Get a scorecard  |
| Period Types - Get a period type | Period Types Get a period type  |
| Period Types/End Periods - Get an end period for the period type | Period Types/End Periods Get an end period for the period type  |
| Period Types/Start Period Lookups - Get a start period | Period Types/Start Period Lookups Get a start period  |
| Periods List of Values - Get a period | Periods List of Values Get a period  |
| Phone Verifications - Get a phone verification | Phone Verifications Get a phone verification  |
| Plan Components - Get a plan component | Plan Components Get a plan component  |
| Plan Components/Plan Component - Incentive Formulas - Get an incentive formula | Plan Components/Plan Component - Incentive Formulas Get an incentive formula  |
| Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Dimensional Input Expressions - Get a rate dimensional input | Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Dimensional Input Expressions Get a rate dimensional input  |
| Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Tables - Get a rate table | Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Tables Get a rate table  |
| Plan Components/Plan Component - Performance Measures - Get a performance measure | Plan Components/Plan Component - Performance Measures Get a performance measure  |
| Plan Components/Plan Component Descriptive Flex Fields - Get a descriptive flex field | Plan Components/Plan Component Descriptive Flex Fields Get a descriptive flex field  |
| Plan Document Templates List of Values - Get a plan document template | Plan Document Templates List of Values Get a plan document template  |
| Price Book Headers - Get a pricebook | Price Book Headers Get a pricebook  |
| Price Book Headers/Price Book Items - Get a pricebook item | Price Book Headers/Price Book Items Get a pricebook item  |
| Process Metadatas - Get a process metadata | Process Metadatas Get a process metadata  |
| Product Group Usages - Get a product group usage | Product Group Usages Get a product group usage  |
| Product Group Usages/Product Group Usage Functions - Get a product group usage engine | Product Group Usages/Product Group Usage Functions Get a product group usage engine  |
| Product Group Usages/Product Group Usage Modes - Get a product group usage mode | Product Group Usages/Product Group Usage Modes Get a product group usage mode  |
| Product Group Usages/Product Group Usage Preferences - Get a product group usage preference | Product Group Usages/Product Group Usage Preferences Get a product group usage preference  |
| Product Group Usages/Product Group Usage Roots - Get a root product group | Product Group Usages/Product Group Usage Roots Get a root product group  |
| Product Groups - Get a product group by product group ID | Product Groups Get a product group by product group ID  |
| Product Groups/Attachments - Get an attachment | Product Groups/Attachments Get an attachment  |
| Product Groups/Filter Attributes - Get an attribute by product group attribute ID | Product Groups/Filter Attributes Get an attribute by product group attribute ID  |
| Product Groups/Filter Attributes/Filter Attribute Values - Get an attribute value by attribute ID | Product Groups/Filter Attributes/Filter Attribute Values Get an attribute value by attribute ID  |
| Product Groups/Products - Get a product relationship details | Product Groups/Products Get a product relationship details  |
| Product Groups/Products/EligibilityRules - Get an eligibility rule | Product Groups/Products/EligibilityRules Get an eligibility rule  |
| Product Groups/Related Groups - Get a subgroup relationship | Product Groups/Related Groups Get a subgroup relationship  |
| Product Groups/Subgroups - Get a subgroup details | Product Groups/Subgroups Get a subgroup details  |
| Product Specialities List of Values - Get a product specialty | Product Specialities List of Values Get a product specialty  |
| Products - Get a product | Products Get a product  |
| Products/Default Prices - Get a default price | Products/Default Prices Get a default price  |
| Products/Product Attachments - Get an attachment for a product | Products/Product Attachments Get an attachment for a product  |
| Products/Product Image Attachments - Get an image attachment for a product | Products/Product Image Attachments Get an image attachment for a product  |
| Products/Product Translations - Get a product translation | Products/Product Translations Get a product translation  |
| Program Benefits - Get a program benefit | Program Benefits Get a program benefit  |
| Program Benefits/Benefit List Values - Get a benefit list type value | Program Benefits/Benefit List Values Get a benefit list type value  |
| Program Enrollments - Get an enrollment | Program Enrollments Get an enrollment  |
| Program Enrollments/Notes - Get a note for an enrollment | Program Enrollments/Notes Get a note for an enrollment  |
| Program Enrollments/Partner Programs - Get a partner program for the enrollment | Program Enrollments/Partner Programs Get a partner program for the enrollment  |
| Program Enrollments/Program Benefit Details - Get a program benefit for an enrollment | Program Enrollments/Program Benefit Details Get a program benefit for an enrollment  |
| Queues - Get a queue | Queues Get a queue  |
| Queues/Overflow Queues - Get an overflow queue | Queues/Overflow Queues Get an overflow queue  |
| Queues/Queue Resource Members - Get a resource member | Queues/Queue Resource Members Get a resource member  |
| Queues/Queue Resource Teams - Get a resource team | Queues/Queue Resource Teams Get a resource team  |
| Queues/Queue Resource Teams/Queue Resource Team Members - Get a resource team member | Queues/Queue Resource Teams/Queue Resource Team Members Get a resource team member  |
| Quote and Order Lines - Get a sales order line | Quote and Order Lines Get a sales order line  |
| Rate Dimensions - Get a rate dimension | Rate Dimensions Get a rate dimension  |
| Rate Dimensions/Rate Dimension - Tiers - Get a rate dimension tier | Rate Dimensions/Rate Dimension - Tiers Get a rate dimension tier  |
| Rate Tables - Get a rate table detail | Rate Tables Get a rate table detail  |
| Rate Tables/Rate Table Dimensions - Get a rate dimension details within a rate table | Rate Tables/Rate Table Dimensions Get a rate dimension details within a rate table  |
| Rate Tables/Rate Table Dimensions/Rate Dimensions Tiers - Get a rate dimension tier | Rate Tables/Rate Table Dimensions/Rate Dimensions Tiers Get a rate dimension tier  |
| Rate Tables/Rate Table Rates - Get a rate table rate | Rate Tables/Rate Table Rates Get a rate table rate  |
| Resolution Links - Get a link | Resolution Links Get a link  |
| Resolution Links/Link Members - Get a link member | Resolution Links/Link Members Get a link member  |
| Resolution Requests - Get a resolution request | Resolution Requests Get a resolution request  |
| Resolution Requests/Duplicate Parties - Get a duplicate party | Resolution Requests/Duplicate Parties Get a duplicate party  |
| Resolution Requests/Duplicate Parties/Source System References - Get a source system reference | Resolution Requests/Duplicate Parties/Source System References Get a source system reference  |
| Resolution Requests/Resolution Details - Get a detailed record for a duplicate party resolu... | Resolution Requests/Resolution Details Get a detailed record for a duplicate party resolution  |
| Resource Capacities - Get a resource capacity | Resource Capacities Get a resource capacity  |
| Resource Users - Get a resource user | Resource Users Get a resource user  |
| Resource Users/Resource Role Assignments - Get a resource role assignment | Resource Users/Resource Role Assignments Get a resource role assignment  |
| Resources - Get a resource | Resources Get a resource  |
| Resources/Picture Attachments - Get a resource's picture | Resources/Picture Attachments Get a resource's picture  |
| Roles - Get a specific incentive compensation role detail | Roles Get a specific incentive compensation role detail  |
| Roles/Role - Participants - Get a participant for a role | Roles/Role - Participants Get a participant for a role  |
| Rule Attributes List of Values - Get a rule attribute | Rule Attributes List of Values Get a rule attribute  |
| Rule Types List of Values - Get a rule type | Rule Types List of Values Get a rule type  |
| Sales Leads - Get a sales lead | Sales Leads Get a sales lead  |
| Sales Leads/Lead Qualifications - Get a lead qualification | Sales Leads/Lead Qualifications Get a lead qualification  |
| Sales Leads/Lead Qualifications/Assessment Answer Groups - Get a lead qualification answer group | Sales Leads/Lead Qualifications/Assessment Answer Groups Get a lead qualification answer group  |
| Sales Leads/Lead Qualifications/Assessment Answer Groups/Assessment Answers - Get a lead qualification answer | Sales Leads/Lead Qualifications/Assessment Answer Groups/Assessment Answers Get a lead qualification answer  |
| Sales Leads/Notes - Get a note | Sales Leads/Notes Get a note  |
| Sales Leads/Opportunities - Get an opportunity | Sales Leads/Opportunities Get an opportunity  |
| Sales Leads/Product Groups - Get a product group | Sales Leads/Product Groups Get a product group  |
| Sales Leads/Products - Get a product | Sales Leads/Products Get a product  |
| Sales Leads/Sales Lead Contacts - Get a sales lead contact | Sales Leads/Sales Lead Contacts Get a sales lead contact  |
| Sales Leads/Sales Lead Products - Get a sales lead product | Sales Leads/Sales Lead Products Get a sales lead product  |
| Sales Leads/Sales Lead Resources - Get a sales lead resource | Sales Leads/Sales Lead Resources Get a sales lead resource  |
| Sales Leads/Sales Lead Territories - Get a sales lead territory | Sales Leads/Sales Lead Territories Get a sales lead territory  |
| Sales Leads/Smart Actions - Get a smart action (Not Supported) | Sales Leads/Smart Actions Get a smart action (Not Supported)  |
| Sales Leads/Smart Actions/Smart Action REST Path Parameter Definitions - Get an action URL binding (Not Supported) | Sales Leads/Smart Actions/Smart Action REST Path Parameter Definitions Get an action URL binding (Not Supported)  |
| Sales Leads/Smart Actions/Smart Action REST Payload Definitions - Get an action request payload (Not Supported) | Sales Leads/Smart Actions/Smart Action REST Payload Definitions Get an action request payload (Not Supported)  |
| Sales Leads/Smart Actions/Smart Action User Interface Definitions - Get an action navigation (Not Supported) | Sales Leads/Smart Actions/Smart Action User Interface Definitions Get an action navigation (Not Supported)  |
| Sales Objective Types - Get a sales objective type | Sales Objective Types Get a sales objective type  |
| Sales Order CPQ Integration Configurations - Get a quote integration configuration | Sales Order CPQ Integration Configurations Get a quote integration configuration  |
| Sales Order CPQ Integration Configurations/Sales Order Setup Disabled Revenue Fields - Get a disabled revenue field | Sales Order CPQ Integration Configurations/Sales Order Setup Disabled Revenue Fields Get a disabled revenue field  |
| Sales Order CPQ Integration Configurations/Sales Order Setup Reference Details - Get a quote setup reference detail | Sales Order CPQ Integration Configurations/Sales Order Setup Reference Details Get a quote setup reference detail  |
| Sales Orders - Get a quote | Sales Orders Get a quote  |
| Sales Orders/Quote and Order Lines - Get a sales order line | Sales Orders/Quote and Order Lines Get a sales order line  |
| Sales Promotions - Get a sales promotion | Sales Promotions Get a sales promotion  |
| Sales Territories - Get a territory | Sales Territories Get a territory  |
| Sales Territories/Line of Business - Get a line of business for a territory | Sales Territories/Line of Business Get a line of business for a territory  |
| Sales Territories/Resources - Get a resource for a territory | Sales Territories/Resources Get a resource for a territory  |
| Sales Territories/Rules - Get a rule for a territory | Sales Territories/Rules Get a rule for a territory  |
| Sales Territories/Rules/Rule Boundaries - Get a rule boundary for a territory | Sales Territories/Rules/Rule Boundaries Get a rule boundary for a territory  |
| Sales Territories/Rules/Rule Boundaries/Rule Boundary Values - Get a rule boundary value for a territory | Sales Territories/Rules/Rule Boundaries/Rule Boundary Values Get a rule boundary value for a territory  |
| Sales Territory Proposals - Get a proposal | Sales Territory Proposals Get a proposal  |
| Screen Pop Pages - Get a screen pop page | Screen Pop Pages Get a screen pop page  |
| Screen Pop Pages/Screen Pop Page Parameters - Get a page parameter | Screen Pop Pages/Screen Pop Page Parameters Get a page parameter  |
| Screen Pop Pages/Screen Pop Page Parameters/Screen Pop Page Map Parameters - Get a page map parameter | Screen Pop Pages/Screen Pop Page Parameters/Screen Pop Page Map Parameters Get a page map parameter  |
| Screen Pop Tokens - Get a screen pop token | Screen Pop Tokens Get a screen pop token  |
| Self-Service Registrations - Get a self-service registration request | Self-Service Registrations Get a self-service registration request  |
| Self-Service Roles - Get a role for a self-service user | Self-Service Roles Get a role for a self-service user  |
| Self-Service Users - Get a self-service user | Self-Service Users Get a self-service user  |
| Self-Service Users - Get a self-service user | Self-Service Users Get a self-service user  |
| Self-Service Users/Self-Service Roles - Get a self-service role | Self-Service Users/Self-Service Roles Get a self-service role  |
| Self-Service Users/Self-Service Roles - Get a self-service role | Self-Service Users/Self-Service Roles Get a self-service role  |
| Self-Service Users/Self-Service Roles Histories - Get a self-service role history | Self-Service Users/Self-Service Roles Histories Get a self-service role history  |
| Self-Service Users/Self-Service Roles Histories - Get a self-service role history | Self-Service Users/Self-Service Roles Histories Get a self-service role history  |
| Service Details - Get a service detail | Service Details Get a service detail  |
| Service Providers - Get a service provider | Service Providers Get a service provider  |
| Service Providers/Services - Get a service | Service Providers/Services Get a service  |
| Service Providers/Services/Service Histories - Get a service history | Service Providers/Services/Service Histories Get a service history  |
| Service Requests - Get a service request | Service Requests Get a service request  |
| Service Requests/Activities - Get an activity | Service Requests/Activities Get an activity  |
| Service Requests/Attachments - Get an attachment | Service Requests/Attachments Get an attachment  |
| Service Requests/Channel Communications - Get a channel communication | Service Requests/Channel Communications Get a channel communication  |
| Service Requests/Contact Members - Get a contact | Service Requests/Contact Members Get a contact  |
| Service Requests/Interaction References - Get a service request interaction | Service Requests/Interaction References Get a service request interaction  |
| Service Requests/Messages - Get a message | Service Requests/Messages Get a message  |
| Service Requests/Messages/Attachments - Get an attachment | Service Requests/Messages/Attachments Get an attachment  |
| Service Requests/Messages/Channel Communications - Get a channel communication | Service Requests/Messages/Channel Communications Get a channel communication  |
| Service Requests/Milestones - Get a service request milestone | Service Requests/Milestones Get a service request milestone  |
| Service Requests/Milestones/Milestone Codes - Get a service request milestone history | Service Requests/Milestones/Milestone Codes Get a service request milestone history  |
| Service Requests/Resources - Get a resource member | Service Requests/Resources Get a resource member  |
| Service Requests/Service Request References - Get a service request reference | Service Requests/Service Request References Get a service request reference  |
| Service Requests/Smart Actions - Get a smart action (Not Supported) | Service Requests/Smart Actions Get a smart action (Not Supported)  |
| Service Requests/Smart Actions/Smart Action REST Path Parameter Definitions - Get an action URL binding (Not Supported) | Service Requests/Smart Actions/Smart Action REST Path Parameter Definitions Get an action URL binding (Not Supported)  |
| Service Requests/Smart Actions/Smart Action REST Payload Definitions - Get an action request payload (Not Supported) | Service Requests/Smart Actions/Smart Action REST Payload Definitions Get an action request payload (Not Supported)  |
| Service Requests/Smart Actions/Smart Action User Interface Definitions - Get an action navigation (Not Supported) | Service Requests/Smart Actions/Smart Action User Interface Definitions Get an action navigation (Not Supported)  |
| Service Requests/Tags - Get a tag | Service Requests/Tags Get a tag  |
| Setup Assistants - Get a setup task | Setup Assistants Get a setup task  |
| Setup Assistants/Accounting Calendars - Get an accounting calendar | Setup Assistants/Accounting Calendars Get an accounting calendar  |
| Setup Assistants/Company Profiles - Get a company profile | Setup Assistants/Company Profiles Get a company profile  |
| Setup Assistants/Competitors - Get a competitor | Setup Assistants/Competitors Get a competitor  |
| Setup Assistants/Geographies - Get a geography setup data | Setup Assistants/Geographies Get a geography setup data  |
| Setup Assistants/Opportunities - Get an opportunity setup data | Setup Assistants/Opportunities Get an opportunity setup data  |
| Setup Assistants/Opportunities/Sales Stages - Get sales stages of the selected sales method | Setup Assistants/Opportunities/Sales Stages Get sales stages of the selected sales method  |
| Setup Assistants/Product Groups - Get product group setup details | Setup Assistants/Product Groups Get product group setup details  |
| Setup Assistants/Role Mappings - Get resource role and role mapping | Setup Assistants/Role Mappings Get resource role and role mapping  |
| Setup Assistants/Sales Forecasts - Get sales forecasting setup details | Setup Assistants/Sales Forecasts Get sales forecasting setup details  |
| Setup Assistants/Sales Stages - Get sales stages of the selected sales method | Setup Assistants/Sales Stages Get sales stages of the selected sales method  |
| Setup Assistants/Setup Histories - Get a setup history detail | Setup Assistants/Setup Histories Get a setup history detail  |
| Setup Assistants/Setup Progress - Get a setup task details | Setup Assistants/Setup Progress Get a setup task details  |
| Setup Assistants/Setup Users - Get setup user details | Setup Assistants/Setup Users Get setup user details  |
| Setup Assistants/Top Sales Users - Get top sales user details | Setup Assistants/Top Sales Users Get top sales user details  |
| Skill Resources - Get a skill resource | Skill Resources Get a skill resource  |
| Skills - Get a skill | Skills Get a skill  |
| Skills/Competencies - Get a competency | Skills/Competencies Get a competency  |
| Skills/Competencies/Competency Values - Get a value | Skills/Competencies/Competency Values Get a value  |
| Smart Text Folders - Get a smart text folder | Smart Text Folders Get a smart text folder  |
| Smart Text Folders/Smart Text Child Folders - Get a smart text child folder | Smart Text Folders/Smart Text Child Folders Get a smart text child folder  |
| Smart Text User Variables - Get a smart text user variable | Smart Text User Variables Get a smart text user variable  |
| Smart Texts - Get all smart texts | Smart Texts Get all smart texts  |
| Social Posts - Get a social post | Social Posts Get a social post  |
| Social Posts/Social Post Tags - Get a social post tag | Social Posts/Social Post Tags Get a social post tag  |
| Social Posts/Social Post URLs - Get a social post URL | Social Posts/Social Post URLs Get a social post URL  |
| Social Users - Get a social user | Social Users Get a social user  |
| Source System References - Get a source system reference | Source System References Get a source system reference  |
| Standard Coverages - Get a standard coverage | Standard Coverages Get a standard coverage  |
| Standard Coverages/Adjustments - Get an adjustment | Standard Coverages/Adjustments Get an adjustment  |
| Start Period Lookups - Get a start period | Start Period Lookups Get a start period  |
| Subscription Accounts - Get a subscription account | Subscription Accounts Get a subscription account  |
| Subscription Accounts Roles - Get a subscription account role | Subscription Accounts Roles Get a subscription account role  |
| Subscription Accounts/Attachments - Get an attachment | Subscription Accounts/Attachments Get an attachment  |
| Subscription Accounts/Billing Profiles - Get a billing profile | Subscription Accounts/Billing Profiles Get a billing profile  |
| Subscription Accounts/Notes - Get a note | Subscription Accounts/Notes Get a note  |
| Subscription Accounts/Subscription Account Addresses - Get an address | Subscription Accounts/Subscription Account Addresses Get an address  |
| Subscription Accounts/Subscription Account Relationships - Get a subscription account relationship | Subscription Accounts/Subscription Account Relationships Get a subscription account relationship  |
| Subscription Accounts/Subscription Account Roles - Get a subscription account role | Subscription Accounts/Subscription Account Roles Get a subscription account role  |
| Subscription AI Features - Get product churn feature | Subscription AI Features Get product churn feature  |
| Subscription Asset Transactions - Get a subscription asset transaction | Subscription Asset Transactions Get a subscription asset transaction  |
| Subscription Asset Transactions/Asset Fulfillment Lines - Get a fulfillment line | Subscription Asset Transactions/Asset Fulfillment Lines Get a fulfillment line  |
| Subscription Asset Transactions/Asset Split Lines - Get a split line | Subscription Asset Transactions/Asset Split Lines Get a split line  |
| Subscription Coverage Exceptions - Get a subscription availability exception | Subscription Coverage Exceptions Get a subscription availability exception  |
| Subscription Coverage Exceptions/Subscription Coverage Availabilities - Get a subscription availability schedule | Subscription Coverage Exceptions/Subscription Coverage Availabilities Get a subscription availability schedule  |
| Subscription Coverage Exceptions/Subscription Coverage Availabilities/Subscription Coverage Breaks - Get a subscription availability break | Subscription Coverage Exceptions/Subscription Coverage Availabilities/Subscription Coverage Breaks Get a subscription availability break  |
| Subscription Coverage Schedules - Get a subscription coverage availability schedule | Subscription Coverage Schedules Get a subscription coverage availability schedule  |
| Subscription Coverage Schedules/Subscription Coverage Intervals - Get a subscription coverage availability schedule ... | Subscription Coverage Schedules/Subscription Coverage Intervals Get a subscription coverage availability schedule interval  |
| Subscription Coverage Schedules/Subscription Coverage Intervals/Subscription Coverage Availabilities - Get a subscription coverage availability detail | Subscription Coverage Schedules/Subscription Coverage Intervals/Subscription Coverage Availabilities Get a subscription coverage availability detail  |
| Subscription Coverage Schedules/Subscription Coverage Intervals/Subscription Coverage Availabilities/Subscription Coverage Breaks - Get a subscription coverage availability break | Subscription Coverage Schedules/Subscription Coverage Intervals/Subscription Coverage Availabilities/Subscription Coverage Breaks Get a subscription coverage availability break  |
| Subscription Customer Assets List of Values - Get a customer asset | Subscription Customer Assets List of Values Get a customer asset  |
| Subscription Order Transactions - Get a subscription order transaction | Subscription Order Transactions Get a subscription order transaction  |
| Subscription Products - Get a subscription product | Subscription Products Get a subscription product  |
| Subscription Products/Bill Lines - Get a bill line | Subscription Products/Bill Lines Get a bill line  |
| Subscription Products/Bill Lines/Bill Adjustments - Get a bill adjustment | Subscription Products/Bill Lines/Bill Adjustments Get a bill adjustment  |
| Subscription Products/Charges - Get a charge | Subscription Products/Charges Get a charge  |
| Subscription Products/Charges/Adjustments - Get an adjustment | Subscription Products/Charges/Adjustments Get an adjustment  |
| Subscription Products/Charges/Charge Tiers - Get a charge tier | Subscription Products/Charges/Charge Tiers Get a charge tier  |
| Subscription Products/Covered Levels - Get a covered level | Subscription Products/Covered Levels Get a covered level  |
| Subscription Products/Covered Levels/Bill Lines - Get a bill line | Subscription Products/Covered Levels/Bill Lines Get a bill line  |
| Subscription Products/Covered Levels/Bill Lines/Bill Adjustments - Get a bill adjustment | Subscription Products/Covered Levels/Bill Lines/Bill Adjustments Get a bill adjustment  |
| Subscription Products/Covered Levels/Charges - Get a charge | Subscription Products/Covered Levels/Charges Get a charge  |
| Subscription Products/Covered Levels/Charges/Adjustments - Get an adjustment | Subscription Products/Covered Levels/Charges/Adjustments Get an adjustment  |
| Subscription Products/Covered Levels/Charges/Charge Tiers - Get a charge tier | Subscription Products/Covered Levels/Charges/Charge Tiers Get a charge tier  |
| Subscription Products/Covered Levels/Relationships - Get a relationship | Subscription Products/Covered Levels/Relationships Get a relationship  |
| Subscription Products/Credit Cards - Get a credit card | Subscription Products/Credit Cards Get a credit card  |
| Subscription Products/Descriptive Flexfields - Get a descriptive flexfield | Subscription Products/Descriptive Flexfields Get a descriptive flexfield  |
| Subscription Products/Relationships - Get a relationship | Subscription Products/Relationships Get a relationship  |
| Subscription Products/Sales Credits - Get a sales credit | Subscription Products/Sales Credits Get a sales credit  |
| Subscription Profiles - Get a subscription profile | Subscription Profiles Get a subscription profile  |
| Subscriptions - Get a subscription | Subscriptions Get a subscription  |
| Subscriptions/Credit Cards - Get a credit card | Subscriptions/Credit Cards Get a credit card  |
| Subscriptions/Descriptive Flexfields - Get a descriptive flexfield | Subscriptions/Descriptive Flexfields Get a descriptive flexfield  |
| Subscriptions/Parties - Get a subscription party | Subscriptions/Parties Get a subscription party  |
| Subscriptions/Parties/Contacts - Get a subscription contact | Subscriptions/Parties/Contacts Get a subscription contact  |
| Subscriptions/Products - Get a subscription product | Subscriptions/Products Get a subscription product  |
| Subscriptions/Products/Bill Lines - Get a subscription bill line | Subscriptions/Products/Bill Lines Get a subscription bill line  |
| Subscriptions/Products/Bill Lines/Bill Adjustments - Get a subscription bill adjustment | Subscriptions/Products/Bill Lines/Bill Adjustments Get a subscription bill adjustment  |
| Subscriptions/Products/Charges - Get a subscription charge | Subscriptions/Products/Charges Get a subscription charge  |
| Subscriptions/Products/Charges/Adjustments - Get a subscription adjustment | Subscriptions/Products/Charges/Adjustments Get a subscription adjustment  |
| Subscriptions/Products/Charges/Charge Tiers - Get a subscription charge tier | Subscriptions/Products/Charges/Charge Tiers Get a subscription charge tier  |
| Subscriptions/Products/Covered Levels - Get a covered level | Subscriptions/Products/Covered Levels Get a covered level  |
| Subscriptions/Products/Covered Levels/Bill Lines - Get a subscription bill line | Subscriptions/Products/Covered Levels/Bill Lines Get a subscription bill line  |
| Subscriptions/Products/Covered Levels/Bill Lines/Bill Adjustments - Get a subscription bill adjustment | Subscriptions/Products/Covered Levels/Bill Lines/Bill Adjustments Get a subscription bill adjustment  |
| Subscriptions/Products/Covered Levels/Charges - Get a subscription charge | Subscriptions/Products/Covered Levels/Charges Get a subscription charge  |
| Subscriptions/Products/Covered Levels/Charges/Adjustments - Get a subscription adjustment | Subscriptions/Products/Covered Levels/Charges/Adjustments Get a subscription adjustment  |
| Subscriptions/Products/Covered Levels/Charges/Charge Tiers - Get a subscription charge tier | Subscriptions/Products/Covered Levels/Charges/Charge Tiers Get a subscription charge tier  |
| Subscriptions/Products/Covered Levels/Child Covered Levels - Get a child covered level | Subscriptions/Products/Covered Levels/Child Covered Levels Get a child covered level  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines - Get a subscription bill line | Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines Get a subscription bill line  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines/Bill Adjustments - Get a subscription bill adjustment | Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines/Bill Adjustments Get a subscription bill adjustment  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges - Get a subscription charge | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges Get a subscription charge  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Adjustments - Get a subscription adjustment | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Adjustments Get a subscription adjustment  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Charge Tiers - Get a subscription charge tier | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Charge Tiers Get a subscription charge tier  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Relationships - Get a subscription relationship | Subscriptions/Products/Covered Levels/Child Covered Levels/Relationships Get a subscription relationship  |
| Subscriptions/Products/Covered Levels/Relationships - Get a subscription relationship | Subscriptions/Products/Covered Levels/Relationships Get a subscription relationship  |
| Subscriptions/Products/Credit Cards - Get a credit card | Subscriptions/Products/Credit Cards Get a credit card  |
| Subscriptions/Products/Descriptive Flexfields - Get a descriptive flexfield | Subscriptions/Products/Descriptive Flexfields Get a descriptive flexfield  |
| Subscriptions/Products/Relationships - Get a subscription relationship | Subscriptions/Products/Relationships Get a subscription relationship  |
| Subscriptions/Products/Sales Credits - Get a sales credit | Subscriptions/Products/Sales Credits Get a sales credit  |
| Subscriptions/Sales Credits - Get a sales credit | Subscriptions/Sales Credits Get a sales credit  |
| Subscriptions/Validate Subscriptions - Get a subscription validation | Subscriptions/Validate Subscriptions Get a subscription validation  |
| Survey Configurations - Get a survey configuration | Survey Configurations Get a survey configuration  |
| Survey Configurations/Survey Configuration Attributes - Get a survey configuration attribute | Survey Configurations/Survey Configuration Attributes Get a survey configuration attribute  |
| Surveys - Get a survey | Surveys Get a survey  |
| Surveys/Survey Questions - Get a survey question | Surveys/Survey Questions Get a survey question  |
| Surveys/Survey Questions/Survey Answer Choices - Get a survey answer choice | Surveys/Survey Questions/Survey Answer Choices Get a survey answer choice  |
| Surveys/Survey Requests - Get a survey request | Surveys/Survey Requests Get a survey request  |
| Surveys/Survey Requests/Survey Responses - Get a survey response | Surveys/Survey Requests/Survey Responses Get a survey response  |
| Territories - Get a territory | Territories Get a territory  |
| Territories for Sales - Get a territory | Territories for Sales Get a territory  |
| Territories for Sales/Territory Business - Get a territory line of business | Territories for Sales/Territory Business Get a territory line of business  |
| Territories for Sales/Territory Coverages - Get a territory coverage | Territories for Sales/Territory Coverages Get a territory coverage  |
| Territories for Sales/Territory Resources - Get a territory resource | Territories for Sales/Territory Resources Get a territory resource  |
| Time Code Units - Get a time code unit conversion | Time Code Units Get a time code unit conversion  |
| Universal Work Objects - Get an universal work object | Universal Work Objects Get an universal work object  |
| User Context Data Sources - Get a context data source | User Context Data Sources Get a context data source  |
| User Context Object Types - Get an object type | User Context Object Types Get an object type  |
| User Context Object Types/Object Configurations - Get an object configuration | User Context Object Types/Object Configurations Get an object configuration  |
| User Context Object Types/Object Configurations/Object Configuration Details - Get an object configuration detail | User Context Object Types/Object Configurations/Object Configuration Details Get an object configuration detail  |
| User Context Object Types/Object Criteria - Get an object criterion | User Context Object Types/Object Criteria Get an object criterion  |
| User Context Object Types/Related Objects - Get a related object mapping | User Context Object Types/Related Objects Get a related object mapping  |
| User Relevant Items - Get an user relevant item | User Relevant Items Get an user relevant item  |
| User Relevant Items/User Relevant Item Details - Get an user relevant item detail | User Relevant Items/User Relevant Item Details Get an user relevant item detail  |
| Work Orders - Get a work order | Work Orders Get a work order  |
| Work Orders/Attachments - Get an attachment | Work Orders/Attachments Get an attachment  |
| Wrap Ups - Get a wrap up | Wrap Ups Get a wrap up  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Access Groups - Get all access groups | Access Groups Get all access groups  |
| Access Groups/Access Group Members - Get all access group members | Access Groups/Access Group Members Get all access group members  |
| Accounts - Get all accounts | Accounts Get all accounts  |
| Accounts/Account Attachments - Get all attachments | Accounts/Account Attachments Get all attachments  |
| Accounts/Account Extension Bases - Get all account extension bases | Accounts/Account Extension Bases Get all account extension bases  |
| Accounts/Account Resources - Get all sales team members | Accounts/Account Resources Get all sales team members  |
| Accounts/Account Rollups - Get all account rollups | Accounts/Account Rollups Get all account rollups  |
| Accounts/Additional Identifier - Get all additional identifiers | Accounts/Additional Identifier Get all additional identifiers  |
| Accounts/Additional Names - Get all additional name | Accounts/Additional Names Get all additional name  |
| Accounts/Addresses - Get all addresses | Accounts/Addresses Get all addresses  |
| Accounts/Addresses/Address Locales - Get all address locales | Accounts/Addresses/Address Locales Get all address locales  |
| Accounts/Addresses/Address Purposes - Get all address purposes | Accounts/Addresses/Address Purposes Get all address purposes  |
| Accounts/Aux Classifications - Get all customer classifications | Accounts/Aux Classifications Get all customer classifications  |
| Accounts/Contact Points - Get all contact points | Accounts/Contact Points Get all contact points  |
| Accounts/Notes - Get all notes | Accounts/Notes Get all notes  |
| Accounts/Organization Contacts - Get all account contacts | Accounts/Organization Contacts Get all account contacts  |
| Accounts/Primary Addresses - Get all primary addresses | Accounts/Primary Addresses Get all primary addresses  |
| Accounts/Relationships - Get all relationships | Accounts/Relationships Get all relationships  |
| Accounts/Smart Actions - Get all smart actions | Accounts/Smart Actions Get all smart actions  |
| Accounts/Smart Actions/Smart Action REST Path Parameter Definitions - Get all action URL bindings (Not Supported) | Accounts/Smart Actions/Smart Action REST Path Parameter Definitions Get all action URL bindings (Not Supported)  |
| Accounts/Smart Actions/Smart Action REST Payload Definitions - Get all action request payload (Not Supported) | Accounts/Smart Actions/Smart Action REST Payload Definitions Get all action request payload (Not Supported)  |
| Accounts/Smart Actions/Smart Action User Interface Definitions - Get all action navigations (Not Supported) | Accounts/Smart Actions/Smart Action User Interface Definitions Get all action navigations (Not Supported)  |
| Accounts/Source System References - Get all source system references | Accounts/Source System References Get all source system references  |
| Action Events - Get all action events | Action Events Get all action events  |
| Action Plans - Get All Action Plans | Action Plans Get All Action Plans  |
| Action Plans/Action Plan Actions - Get All Action Plan Actions | Action Plans/Action Plan Actions Get All Action Plan Actions  |
| Action Plans/Action Plan Actions/Action Plan Action Relations - Get all action plan action relations | Action Plans/Action Plan Actions/Action Plan Action Relations Get all action plan action relations  |
| Action Templates - Get All Action Plan Templates | Action Templates Get All Action Plan Templates  |
| Action Templates/Template Actions - Get All Template Actions | Action Templates/Template Actions Get All Template Actions  |
| Action Templates/Template Actions/Action Relations - Get All Action Relations | Action Templates/Template Actions/Action Relations Get All Action Relations  |
| Actions - Get All Actions | Actions Get All Actions  |
| Actions/Action Attributes - Get All Action Attributes | Actions/Action Attributes Get All Action Attributes  |
| Actions/Action Conditions - Get All Action Conditions | Actions/Action Conditions Get All Action Conditions  |
| Activities - Get all activities | Activities Get all activities  |
| Activities/Activity Assignees - Get all activity assignees | Activities/Activity Assignees Get all activity assignees  |
| Activities/Activity Attachments - Get all activity attachments | Activities/Activity Attachments Get all activity attachments  |
| Activities/Activity Contacts - Get all activity contacts | Activities/Activity Contacts Get all activity contacts  |
| Activities/Activity Objectives - Get all activity objectives | Activities/Activity Objectives Get all activity objectives  |
| Activities/Notes - Get all activity notes | Activities/Notes Get all activity notes  |
| Activities/Smart Actions - Get all smart actions | Activities/Smart Actions Get all smart actions  |
| Activities/Smart Actions/Smart Action REST Path Parameter Definitions - GET all action URL bindings (Not Supported) | Activities/Smart Actions/Smart Action REST Path Parameter Definitions GET all action URL bindings (Not Supported)  |
| Activities/Smart Actions/Smart Action REST Payload Definitions - GET all action request payloads (Not Supported) | Activities/Smart Actions/Smart Action REST Payload Definitions GET all action request payloads (Not Supported)  |
| Activities/Smart Actions/Smart Action User Interface Definitions - GET all action navigations (Not Supported) | Activities/Smart Actions/Smart Action User Interface Definitions GET all action navigations (Not Supported)  |
| Activity - Retrieve the click count | Application Usage Insights Retrieve the total click count by day for all users Retrieve the click count  |
| Address Style Formats - Get all address style formats | Address Style Formats Get all address style formats  |
| Address Style Formats/Address Style Format Layouts - Get all address format layouts | Address Style Formats/Address Style Format Layouts Get all address format layouts  |
| Assets - Get all assets | Assets Get all assets  |
| Assets/Asset Contacts - Get all asset contacts | Assets/Asset Contacts Get all asset contacts  |
| Assets/Asset Resources - Get all asset resources | Assets/Asset Resources Get all asset resources  |
| Assets/Attachments - Get all attachments | Assets/Attachments Get all attachments  |
| Assets/Smart Actions - Get all smart actions | Assets/Smart Actions Get all smart actions  |
| Assets/Smart Actions/Smart Action REST Path Parameter Definitions - GET all action URL bindings (Not Supported) | Assets/Smart Actions/Smart Action REST Path Parameter Definitions GET all action URL bindings (Not Supported)  |
| Assets/Smart Actions/Smart Action REST Payload Definitions - GET all action request payloads (Not Supported) | Assets/Smart Actions/Smart Action REST Payload Definitions GET all action request payloads (Not Supported)  |
| Assets/Smart Actions/Smart Action User Interface Definitions - GET all action navigations (Not Supported) | Assets/Smart Actions/Smart Action User Interface Definitions GET all action navigations (Not Supported)  |
| Attribute Predictions - Get all attribute predictions | Attribute Predictions Get all attribute predictions  |
| Attributes Values LOVs - Get all rule attribute values | Attributes Values LOVs Get all rule attribute values  |
| Billing Adjustments - Get all billing adjustments | Billing Adjustments Get all billing adjustments  |
| Billing Adjustments/Billing Adjustment Events - Get all billing adjustment events | Billing Adjustments/Billing Adjustment Events Get all billing adjustment events  |
| Budgets - Get all MDF budgets | Budgets Get all MDF budgets  |
| Budgets/Budget Countries - Get all MDF budget countries | Budgets/Budget Countries Get all MDF budget countries  |
| Budgets/Claims - Get all MDF claims associated to the budget | Budgets/Claims Get all MDF claims associated to the budget  |
| Budgets/Fund Requests - Get all MDF requests associated to the budget | Budgets/Fund Requests Get all MDF requests associated to the budget  |
| Budgets/MDF Budget Teams - Get all MDF budget team members | Budgets/MDF Budget Teams Get all MDF budget team members  |
| Budgets/Notes - Get all notes for an MDF budget | Budgets/Notes Get all notes for an MDF budget  |
| Business Plans - Get all business plans | Business Plans Get all business plans  |
| Business Plans/Business Plan Resources - Get all business plan resources | Business Plans/Business Plan Resources Get all business plan resources  |
| Business Plans/Notes - Get all business plan notes | Business Plans/Notes Get all business plan notes  |
| Business Plans/SWOTs - Get all business plan SWOTs | Business Plans/SWOTs Get all business plan SWOTs  |
| Business Units List of Values - Get all business units | Business Units List of Values Get all business units  |
| Calculation Simulations - Get all calculation simulations | Calculation Simulations Get all calculation simulations  |
| Calculation Simulations/Simulation Earnings - Get all simulation earnings | Calculation Simulations/Simulation Earnings Get all simulation earnings  |
| Calculation Simulations/Simulation Meause Results - Get all simulation measure results | Calculation Simulations/Simulation Meause Results Get all simulation measure results  |
| Calculation Simulations/Simulation Transactions - Get all simulation transactions | Calculation Simulations/Simulation Transactions Get all simulation transactions  |
| Calculation Simulations/Simulation Transactions/Simulation Credits - Get all simulation credits | Calculation Simulations/Simulation Transactions/Simulation Credits Get all simulation credits  |
| Calculation Simulations/Simulation Transactions/Simulation Transaction Descriptive Flex Fields - Get all simulation transaction descriptive flex fi... | Calculation Simulations/Simulation Transactions/Simulation Transaction Descriptive Flex Fields Get all simulation transaction descriptive flex fields  |
| Campaign Members - Get all campaign members | Campaign Members Get all campaign members  |
| Campaigns - Get all campaigns | Campaigns Get all campaigns  |
| Campaigns/Smart Actions - Get all smart actions | Campaigns/Smart Actions Get all smart actions  |
| Campaigns/Smart Actions/Smart Action REST Path Parameter Definitions - Get all action URL bindings (Not Supported) | Campaigns/Smart Actions/Smart Action REST Path Parameter Definitions Get all action URL bindings (Not Supported)  |
| Campaigns/Smart Actions/Smart Action REST Payload Definitions - Get all action request payload (Not Supported) | Campaigns/Smart Actions/Smart Action REST Payload Definitions Get all action request payload (Not Supported)  |
| Campaigns/Smart Actions/Smart Action User Interface Definitions - Get all action navigations (Not Supported) | Campaigns/Smart Actions/Smart Action User Interface Definitions Get all action navigations (Not Supported)  |
| Cases - Get all cases | Cases Get all cases  |
| Cases/Case Contacts - Get all case contacts | Cases/Case Contacts Get all case contacts  |
| Cases/Case Households - Get all case households | Cases/Case Households Get all case households  |
| Cases/Case Messages - Get all case messages | Cases/Case Messages Get all case messages  |
| Cases/Case Opportunities - Get all opportunities | Cases/Case Opportunities Get all opportunities  |
| Cases/Case Resources - Get all case resources | Cases/Case Resources Get all case resources  |
| Cases/Smart Actions - Get all smart actions | Cases/Smart Actions Get all smart actions  |
| Cases/Smart Actions/Path Parameter Definitions - Get all action URL bindings (Not Supported) | Cases/Smart Actions/Path Parameter Definitions Get all action URL bindings (Not Supported)  |
| Cases/Smart Actions/Payload Definitions - Get all action request payloads (Not Supported) | Cases/Smart Actions/Payload Definitions Get all action request payloads (Not Supported)  |
| Cases/Smart Actions/User Interface Definitions - Get all action navigations (Not Supported) | Cases/Smart Actions/User Interface Definitions Get all action navigations (Not Supported)  |
| Catalog Product Groups - Get all product groups | Catalog Product Groups Get all product groups  |
| Catalog Product Groups/Attachments - Get all attachments | Catalog Product Groups/Attachments Get all attachments  |
| Catalog Product Groups/Translations - Get all product group translations | Catalog Product Groups/Translations Get all product group translations  |
| Catalog Products Items - Get all products | Catalog Products Items Get all products  |
| Catalog Products Items/Attachments - Get all attachments | Catalog Products Items/Attachments Get all attachments  |
| Catalog Products Items/Item Translation - Get all item translations | Catalog Products Items/Item Translation Get all item translations  |
| Categories - Get all categories | Categories Get all categories  |
| Channels - Get all channels | Channels Get all channels  |
| Channels/Channel Resources - Get all channel resource members | Channels/Channel Resources Get all channel resource members  |
| Channels/Sender Identification Priorities - Get all the sender priority sequences | Channels/Sender Identification Priorities Get all the sender priority sequences  |
| Chat Authentications - Get all chat authentications (Not Supported) | Chat Authentications Get all chat authentications (Not Supported)  |
| Chat Interactions - Get all chat interactions | Chat Interactions Get all chat interactions  |
| Chat Interactions/Transcripts - Get all transcripts | Chat Interactions/Transcripts Get all transcripts  |
| Claims - Get all MDF claims | Claims Get all MDF claims  |
| Claims/Claim Settlements - Get all claim settlements | Claims/Claim Settlements Get all claim settlements  |
| Claims/Claim Team Members - Get all claim team members | Claims/Claim Team Members Get all claim team members  |
| Claims/Notes - Get all notes | Claims/Notes Get all notes  |
| Collaboration Actions - Get all collaboration actions | Collaboration Actions Get all collaboration actions  |
| Collaboration Actions/Action Attributes - Get all attributes of a collaboration action | Collaboration Actions/Action Attributes Get all attributes of a collaboration action  |
| Compensation Plans - Get all compensation plans | Compensation Plans Get all compensation plans  |
| Compensation Plans/Assigned Participants - Get all assigned participants | Compensation Plans/Assigned Participants Get all assigned participants  |
| Compensation Plans/Compensation Plan - Descriptive Flex Fields - Get all descriptive flex fields | Compensation Plans/Compensation Plan - Descriptive Flex Fields Get all descriptive flex fields  |
| Compensation Plans/Compensation Plan - Plan Components - Get all plan components | Compensation Plans/Compensation Plan - Plan Components Get all plan components  |
| Compensation Plans/Compensation Plan - Roles - Get all roles | Compensation Plans/Compensation Plan - Roles Get all roles  |
| Compensation Plans/Direct Assignment Requests - Get all direct assignment requests | Compensation Plans/Direct Assignment Requests Get all direct assignment requests  |
| Competencies - Get all competencies | Competencies Get all competencies  |
| Competitors - Get all competitors | Competitors Get all competitors  |
| Competitors - Get all competitors | Competitors Get all competitors  |
| Contacts - Get all contacts | Contacts Get all contacts  |
| Contacts/Additional Identifiers - Get all additional identifiers | Contacts/Additional Identifiers Get all additional identifiers  |
| Contacts/Additional Names - Get all additional names | Contacts/Additional Names Get all additional names  |
| Contacts/Attachments - Get all contacts' pictures | Contacts/Attachments Get all contacts' pictures  |
| Contacts/Aux Classifications - Get all customer classifications | Contacts/Aux Classifications Get all customer classifications  |
| Contacts/Contact Addresses - Get all addresses | Contacts/Contact Addresses Get all addresses  |
| Contacts/Contact Addresses/Address Locales - Get all address locales | Contacts/Contact Addresses/Address Locales Get all address locales  |
| Contacts/Contact Addresses/Contact Address Purposes - Get all purposes of an address | Contacts/Contact Addresses/Contact Address Purposes Get all purposes of an address  |
| Contacts/Contact Attachments - Get all attachments | Contacts/Contact Attachments Get all attachments  |
| Contacts/Contact Points - Get all contact points | Contacts/Contact Points Get all contact points  |
| Contacts/Contact Primary Addresses - Get all primary addresses | Contacts/Contact Primary Addresses Get all primary addresses  |
| Contacts/Notes - Get all notes | Contacts/Notes Get all notes  |
| Contacts/Relationships - Get all relationships | Contacts/Relationships Get all relationships  |
| Contacts/Sales Account Resources - Get all sales team members | Contacts/Sales Account Resources Get all sales team members  |
| Contacts/Smart Actions - Get all smart actions | Contacts/Smart Actions Get all smart actions  |
| Contacts/Smart Actions/Smart Action REST Path Parameter Definitions - Get all action URL bindings (Not Supported) | Contacts/Smart Actions/Smart Action REST Path Parameter Definitions Get all action URL bindings (Not Supported)  |
| Contacts/Smart Actions/Smart Action REST Payload Definitions - Get all action request payload (Not Supported) | Contacts/Smart Actions/Smart Action REST Payload Definitions Get all action request payload (Not Supported)  |
| Contacts/Smart Actions/Smart Action User Interface Definitions - Get all action navigations (Not Supported) | Contacts/Smart Actions/Smart Action User Interface Definitions Get all action navigations (Not Supported)  |
| Contacts/Source System References - Get all source system references | Contacts/Source System References Get all source system references  |
| Contest Scores - Get all contest scores | Contest Scores Get all contest scores  |
| Contest Scores/Contest Score Details - Get all contest score details | Contest Scores/Contest Score Details Get all contest score details  |
| Contests - Get all contests | Contests Get all contests  |
| Contests/Contest History - Get all contest history records | Contests/Contest History Get all contest history records  |
| Contests/Contest Resources - Get all contest participants | Contests/Contest Resources Get all contest participants  |
| Contests/Contest Scores - Get all contest scores | Contests/Contest Scores Get all contest scores  |
| Contests/Contest Scores/Contest Score Details - Get all contest score details | Contests/Contest Scores/Contest Score Details Get all contest score details  |
| Contract Asset Transactions - Get all contract asset transactions | Contract Asset Transactions Get all contract asset transactions  |
| Contract Asset Transactions/Contract Asset Transaction Details - Get all contract asset transaction details | Contract Asset Transactions/Contract Asset Transaction Details Get all contract asset transaction details  |
| Contracts - Get all contracts | Contracts Get all contracts  |
| Contracts/Bill Plans - Get a bill plan | Contracts/Bill Plans Get a bill plan  |
| Contracts/Bill Plans/Job Assignment Overrides - Get job assignment overrides | Contracts/Bill Plans/Job Assignment Overrides Get job assignment overrides  |
| Contracts/Bill Plans/Job Rate Overrides - Get job rate overrides | Contracts/Bill Plans/Job Rate Overrides Get job rate overrides  |
| Contracts/Bill Plans/Job Title Overrides - Get  job title overrides | Contracts/Bill Plans/Job Title Overrides Get  job title overrides  |
| Contracts/Bill Plans/Labor Multiplier Overrides - Get labor multiplier overrides | Contracts/Bill Plans/Labor Multiplier Overrides Get labor multiplier overrides  |
| Contracts/Bill Plans/Non Labor Rate Overrides - Get non labor rate overrides | Contracts/Bill Plans/Non Labor Rate Overrides Get non labor rate overrides  |
| Contracts/Bill Plans/Person Rate Overrides - Get person rate overrides | Contracts/Bill Plans/Person Rate Overrides Get person rate overrides  |
| Contracts/Billing Controls - Get all billing controls | Contracts/Billing Controls Get all billing controls  |
| Contracts/Contract Documents - Get all contract documents | Contracts/Contract Documents Get all contract documents  |
| Contracts/Contract Header Flexfields - Get all contract header flexfields | Contracts/Contract Header Flexfields Get all contract header flexfields  |
| Contracts/Contract Header Translations - Get all contract header translations | Contracts/Contract Header Translations Get all contract header translations  |
| Contracts/Contract Lines - Get all contract lines | Contracts/Contract Lines Get all contract lines  |
| Contracts/Contract Lines/Associated Projects - Get all associated projects | Contracts/Contract Lines/Associated Projects Get all associated projects  |
| Contracts/Contract Lines/Billing Controls - Get all billing controls | Contracts/Contract Lines/Billing Controls Get all billing controls  |
| Contracts/Contract Lines/Contract Line Flexfields - Get all contract line flexfields | Contracts/Contract Lines/Contract Line Flexfields Get all contract line flexfields  |
| Contracts/Contract Lines/Contract Line Translations - Get all contract line translations | Contracts/Contract Lines/Contract Line Translations Get all contract line translations  |
| Contracts/Contract Lines/Sales Credits - Get sales credits | Contracts/Contract Lines/Sales Credits Get sales credits  |
| Contracts/Contract Parties - Get all contract parties | Contracts/Contract Parties Get all contract parties  |
| Contracts/Contract Parties/Contract Party Contacts - Get all contract party contacts | Contracts/Contract Parties/Contract Party Contacts Get all contract party contacts  |
| Contracts/Contract Parties/Contract Party Contacts/Contract Party Contact Flexfields - Get all contract party contact flexfields | Contracts/Contract Parties/Contract Party Contacts/Contract Party Contact Flexfields Get all contract party contact flexfields  |
| Contracts/Contract Parties/Contract Party Flexfields - Get all contract party flexfields | Contracts/Contract Parties/Contract Party Flexfields Get all contract party flexfields  |
| Contracts/Related Documents - Get related documents | Contracts/Related Documents Get related documents  |
| Contracts/Revenue Plans - Get revenue plans | Contracts/Revenue Plans Get revenue plans  |
| Contracts/Revenue Plans/Job Assignment Overrides - Get job assignment overrides | Contracts/Revenue Plans/Job Assignment Overrides Get job assignment overrides  |
| Contracts/Revenue Plans/Job Rate Overrides - Get job rate overrides | Contracts/Revenue Plans/Job Rate Overrides Get job rate overrides  |
| Contracts/Revenue Plans/Labor Multiplier Overrides - Get labor multiplier overrides | Contracts/Revenue Plans/Labor Multiplier Overrides Get labor multiplier overrides  |
| Contracts/Revenue Plans/Non Labor Rate Overrides - Get non labor rate overrides | Contracts/Revenue Plans/Non Labor Rate Overrides Get non labor rate overrides  |
| Contracts/Revenue Plans/Person Rate Overrides - Get person rate overrides | Contracts/Revenue Plans/Person Rate Overrides Get person rate overrides  |
| Contracts/Sales Credits - Get sales credits | Contracts/Sales Credits Get sales credits  |
| Contracts/Sections - Get sections | Contracts/Sections Get sections  |
| Contracts/Sections/Clauses - Get all clauses | Contracts/Sections/Clauses Get all clauses  |
| Contracts/Sections/Sub Sections - Get sub sections | Contracts/Sections/Sub Sections Get sub sections  |
| Contracts/Supporting Documents - Get supporting documents | Contracts/Supporting Documents Get supporting documents  |
| Conversation Messages - Get all conversation messages | Conversation Messages Get all conversation messages  |
| Conversation Messages/Attachments - Get all attachments | Conversation Messages/Attachments Get all attachments  |
| Conversation Messages/Conversation Message Recipients - Get all conversation message recipients | Conversation Messages/Conversation Message Recipients Get all conversation message recipients  |
| Conversation Messages/Smart Actions - Get all actions | Conversation Messages/Smart Actions Get all actions  |
| Conversation Messages/Smart Actions/Smart Action REST Payload Definitions - Get all action request payload definitions | Conversation Messages/Smart Actions/Smart Action REST Payload Definitions Get all action request payload definitions  |
| Conversation Messages/Smart Actions/Smart Action User Interface Definitions - Get all action navigation definitions | Conversation Messages/Smart Actions/Smart Action User Interface Definitions Get all action navigation definitions  |
| Conversation Messages/Smart Actions/Smart Action User Interface Definitions - Get all action path parameters | Conversation Messages/Smart Actions/Smart Action User Interface Definitions Get all action path parameters  |
| Conversations - Get all conversations | Conversations Get all conversations  |
| Conversations/Conversation References - Get all conversation references | Conversations/Conversation References Get all conversation references  |
| Copy Maps - Get all copy maps | Copy Maps Get all copy maps  |
| Credit Categories - Get all credit categories | Credit Categories Get all credit categories  |
| Cumulativegrowth - Retrieve object count | Application Usage Insights by day Retrieve object count  |
| Currencies - Get all currencies | Currencies Get all currencies  |
| DaaS Smart Data - Get all smart data | DaaS Smart Data Get all smart data  |
| Deal Registrations - Get all deal registrations | Deal Registrations Get all deal registrations  |
| Deal Registrations/Deal Products - Get all deal products | Deal Registrations/Deal Products Get all deal products  |
| Deal Registrations/Deal Team Members - Get all deal team members | Deal Registrations/Deal Team Members Get all deal team members  |
| Deal Registrations/Notes - Get all notes | Deal Registrations/Notes Get all notes  |
| Deal Registrations/Opportunities - Get all opportunities | Deal Registrations/Opportunities Get all opportunities  |
| Deal Registrations/Product Groups - Get all product groups | Deal Registrations/Product Groups Get all product groups  |
| Deal Registrations/Products - Get all products | Deal Registrations/Products Get all products  |
| Deal Registrations/Smart Actions - Get all smart actions | Deal Registrations/Smart Actions Get all smart actions  |
| Deal Registrations/Smart Actions/Smart Action REST Path Parameter Definitions - Get all action URL bindings (Not Supported) | Deal Registrations/Smart Actions/Smart Action REST Path Parameter Definitions Get all action URL bindings (Not Supported)  |
| Deal Registrations/Smart Actions/Smart Action REST Payload Definitions - Get all action request payloads (Not Supported) | Deal Registrations/Smart Actions/Smart Action REST Payload Definitions Get all action request payloads (Not Supported)  |
| Deal Registrations/Smart Actions/Smart Action User Interface Definitions - Get all action navigations (Not Supported) | Deal Registrations/Smart Actions/Smart Action User Interface Definitions Get all action navigations (Not Supported)  |
| Device Tokens - Get all device tokens | Device Tokens Get all device tokens  |
| Dynamic Link Patterns - Get all dynamic link patterns | Dynamic Link Patterns Get all dynamic link patterns  |
| Eligible Pay Groups List of Values - Get all eligible pay groups | Eligible Pay Groups List of Values Get all eligible pay groups  |
| Email Verifications - Get all email verifications | Email Verifications Get all email verifications  |
| End Periods - Get all end periods for the period type | End Periods Get all end periods for the period type  |
| Entitlements - Get all subscription entitlements | Entitlements Get all subscription entitlements  |
| Entitlements/Entitlement Details - Get all subscription entitlement details | Entitlements/Entitlement Details Get all subscription entitlement details  |
| Entitlements/Entitlement Details/Charge Adjustments - Get all subscription coverage adjustments | Entitlements/Entitlement Details/Charge Adjustments Get all subscription coverage adjustments  |
| Entitlements/Entitlement Details/Entitlement Results - Get all subscription entitlement results | Entitlements/Entitlement Details/Entitlement Results Get all subscription entitlement results  |
| Entitlements/Entitlement Details/Standard Coverages - Get all standard coverages | Entitlements/Entitlement Details/Standard Coverages Get all standard coverages  |
| Export Activities - Get all bulk export activities | Export Activities Get all bulk export activities  |
| Export Activities/Export Attachments - Get all attachment details | Export Activities/Export Attachments Get all attachment details  |
| Export Activities/Export Child Object Activities - Get all bulk export activity child object details | Export Activities/Export Child Object Activities Get all bulk export activity child object details  |
| Export Activities/Export Child Object Activities/Export Child Object Activities - Get all bulk export activity child object details | Export Activities/Export Child Object Activities/Export Child Object Activities Get all bulk export activity child object details  |
| Expression Detail Items List of Values - Get all expression detail items | Expression Detail Items List of Values Get all expression detail items  |
| Expression Detail Items List of Values/Expression Detail Basic Attribute Groups - Get all basic attribute groups | Expression Detail Items List of Values/Expression Detail Basic Attribute Groups Get all basic attribute groups  |
| Expression Detail Items List of Values/Expression Detail Basic Attribute Groups/Expression Detail Basic Attribute Names - Get all basic attribute names | Expression Detail Items List of Values/Expression Detail Basic Attribute Groups/Expression Detail Basic Attribute Names Get all basic attribute names  |
| Expression Detail Items List of Values/Expression Detail Function Groups - Get all function groups | Expression Detail Items List of Values/Expression Detail Function Groups Get all function groups  |
| Expression Detail Items List of Values/Expression Detail Function Groups/Expression Detail Function Names - Get all function names | Expression Detail Items List of Values/Expression Detail Function Groups/Expression Detail Function Names Get all function names  |
| Expression Detail Items List of Values/Expression Detail Operators - Get all operators | Expression Detail Items List of Values/Expression Detail Operators Get all operators  |
| Expression Detail Items List of Values/Expression Detail Performance Measures - Get all performance measures | Expression Detail Items List of Values/Expression Detail Performance Measures Get all performance measures  |
| Expression Detail Items List of Values/Expression Detail Performance Measures/Expression Detail Performance Measure Attributes - Get all performance measure attributes | Expression Detail Items List of Values/Expression Detail Performance Measures/Expression Detail Performance Measure Attributes Get all performance measure attributes  |
| Expression Detail Items List of Values/Expression Detail Plan Components - Get all plan components | Expression Detail Items List of Values/Expression Detail Plan Components Get all plan components  |
| Expression Detail Items List of Values/Expression Detail Plan Components/Expression Detail Plan Component Attributes - Get all plan component attributes | Expression Detail Items List of Values/Expression Detail Plan Components/Expression Detail Plan Component Attributes Get all plan component attributes  |
| Expression Detail Items List of Values/Expression Detail Plan Components/Expression Detail Plan Component Attributes/Expression Detail Plan Component Measures - Get all plan component performance measures | Expression Detail Items List of Values/Expression Detail Plan Components/Expression Detail Plan Component Attributes/Expression Detail Plan Component Measures Get all plan component performance measures  |
| Expression Detail Items List of Values/Expression Detail Plan Components/Expression Detail Plan Component Attributes/Expression Detail Plan Component Measures/Expression Detail Plan Component Measure Result Attributes - Get all plan component measure result attributes | Expression Detail Items List of Values/Expression Detail Plan Components/Expression Detail Plan Component Attributes/Expression Detail Plan Component Measures/Expression Detail Plan Component Measure Result Attributes Get all plan component measure result attributes  |
| Expression Detail Items List of Values/Expression Detail User Defined Functions - Get all user defined functions | Expression Detail Items List of Values/Expression Detail User Defined Functions Get all user defined functions  |
| Expression Detail Items List of Values/Expression Detail User Defined Query Value Sets - Get all user defined queries | Expression Detail Items List of Values/Expression Detail User Defined Query Value Sets Get all user defined queries  |
| Expression Usages List of Values - Get all expression usages | Expression Usages List of Values Get all expression usages  |
| Forecasts - Get all territory forecasts | Forecasts Get all territory forecasts  |
| Forecasts/Adjustment Periods - Get all period adjustments for a forecast | Forecasts/Adjustment Periods Get all period adjustments for a forecast  |
| Forecasts/Forecast Items - Get all forecast items | Forecasts/Forecast Items Get all forecast items  |
| Forecasts/Forecast Products - Get all forecast products | Forecasts/Forecast Products Get all forecast products  |
| Forecasts/Forecast Products/Product Adjustment Periods - Get all period adjustments for a forecast product | Forecasts/Forecast Products/Product Adjustment Periods Get all period adjustments for a forecast product  |
| Forecasts/Revenue Items - Get all unforecasted revenue items | Forecasts/Revenue Items Get all unforecasted revenue items  |
| FullAgentsStatus - Get full agents status | Third Party Routing Sends to the caller a list of relevant agent information that are required for routing process. The list contains only the agents that are assigned to those queues that are routed by the caller (the partner that call this method). The information that are to be sent are grouped by agent and contains <ul><li>the list of automatic channels of each agent</li><li>the agent identifier</li><li>the date of the last work assigned to the agent</li><li>the presence flag of the agent</li></ul> The response is sent without any delay. The response body is a multi-line document, each line being a standalone JSON. We don't format the entire response as a single JSON string, so the partner does not need to wait for the entire string to be received before they can decode it. The partners can decode by line, convert each line into an object and process it before the rest of the response arrives.<br/> In case of backward incompatible changes, a new method will be added, with a new version number. Get full agents status  |
| Geographies List of Values - Get all geographies | Geographies List of Values Get all geographies  |
| Goal Metric - Get all Goal Metrics | Goal Metric Get all Goal Metrics  |
| Goal Metric/Goal Metric Breakdowns - Get all goal metric breakdowns | Goal Metric/Goal Metric Breakdowns Get all goal metric breakdowns  |
| Goal Metric/Goal Metric Detail - Get all Goal Metric Details | Goal Metric/Goal Metric Detail Get all Goal Metric Details  |
| Goals - Get all goals | Goals Get all goals  |
| Goals/Goal History - Get all goal history records | Goals/Goal History Get all goal history records  |
| Goals/Goal Metric - Get all Goal Metrics | Goals/Goal Metric Get all Goal Metrics  |
| Goals/Goal Metric/Goal Metric Breakdowns - Get all goal metric breakdowns | Goals/Goal Metric/Goal Metric Breakdowns Get all goal metric breakdowns  |
| Goals/Goal Metric/Goal Metric Detail - Get all Goal Metric Details | Goals/Goal Metric/Goal Metric Detail Get all Goal Metric Details  |
| Goals/Goal Participants - Get all goal participants | Goals/Goal Participants Get all goal participants  |
| HCM Service Requests - Get all service requests | HCM Service Requests Get all service requests  |
| HCM Service Requests/Attachments - Get all attachments | HCM Service Requests/Attachments Get all attachments  |
| HCM Service Requests/Channel Communications - Get all channel communications | HCM Service Requests/Channel Communications Get all channel communications  |
| HCM Service Requests/Contact Members - Get all contact members | HCM Service Requests/Contact Members Get all contact members  |
| HCM Service Requests/Interaction References - Get all service request interactions | HCM Service Requests/Interaction References Get all service request interactions  |
| HCM Service Requests/Messages - Get all messages | HCM Service Requests/Messages Get all messages  |
| HCM Service Requests/Messages/Attachments - Get all attachments | HCM Service Requests/Messages/Attachments Get all attachments  |
| HCM Service Requests/Messages/Message Channels - Get all channel communication messages | HCM Service Requests/Messages/Message Channels Get all channel communication messages  |
| HCM Service Requests/Milestones - Get all service request milestones | HCM Service Requests/Milestones Get all service request milestones  |
| HCM Service Requests/Milestones/Milestones History - Get all service request milestone history | HCM Service Requests/Milestones/Milestones History Get all service request milestone history  |
| HCM Service Requests/Resources - Get all resource members | HCM Service Requests/Resources Get all resource members  |
| HCM Service Requests/Service Request References - Get all service request references | HCM Service Requests/Service Request References Get all service request references  |
| HCM Service Requests/Smart Actions - Get all actions | HCM Service Requests/Smart Actions Get all actions  |
| HCM Service Requests/Smart Actions/Smart Action REST Payload Definitions - Get all action request payload definitions | HCM Service Requests/Smart Actions/Smart Action REST Payload Definitions Get all action request payload definitions  |
| HCM Service Requests/Smart Actions/Smart Action User Interface Definitions - Get all action navigation definitions | HCM Service Requests/Smart Actions/Smart Action User Interface Definitions Get all action navigation definitions  |
| HCM Service Requests/Smart Actions/Smart Action User Interface Definitions - Get all action path parameters | HCM Service Requests/Smart Actions/Smart Action User Interface Definitions Get all action path parameters  |
| HCM Service Requests/Tags - Get all tags | HCM Service Requests/Tags Get all tags  |
| Households - Get all households | Households Get all households  |
| Households/Additional Identifiers - Get all additional identifiers | Households/Additional Identifiers Get all additional identifiers  |
| Households/Additional Names - Get all additional names | Households/Additional Names Get all additional names  |
| Households/Addresses - Get all addresses | Households/Addresses Get all addresses  |
| Households/Addresses/Address Locales - Get all address locales | Households/Addresses/Address Locales Get all address locales  |
| Households/Addresses/Address Purposes - Get all address purposes | Households/Addresses/Address Purposes Get all address purposes  |
| Households/Attachments - Get all attachments | Households/Attachments Get all attachments  |
| Households/Aux Classifications - Get all customer classifications | Households/Aux Classifications Get all customer classifications  |
| Households/Contact Points - Get all contact points | Households/Contact Points Get all contact points  |
| Households/Notes - Get all notes | Households/Notes Get all notes  |
| Households/Primary Addresses - Get all primary addresses | Households/Primary Addresses Get all primary addresses  |
| Households/Relationships - Get all relationships | Households/Relationships Get all relationships  |
| Households/Sales Account Resources - Get all sales team members | Households/Sales Account Resources Get all sales team members  |
| Households/Source System References - Get all source system references | Households/Source System References Get all source system references  |
| Hub Organizations - Get all organizations | Hub Organizations Get all organizations  |
| Hub Organizations/Additional Identifiers - Get all additional identifiers | Hub Organizations/Additional Identifiers Get all additional identifiers  |
| Hub Organizations/Additional Names - Get all additional names | Hub Organizations/Additional Names Get all additional names  |
| Hub Organizations/Addresses - Get all addresses | Hub Organizations/Addresses Get all addresses  |
| Hub Organizations/Contact Points - Get all contact points | Hub Organizations/Contact Points Get all contact points  |
| Hub Organizations/Customer Classifications - Get all customer classifications | Hub Organizations/Customer Classifications Get all customer classifications  |
| Hub Organizations/Notes - Get all notes | Hub Organizations/Notes Get all notes  |
| Hub Organizations/Organization Attachments - Get all attachments | Hub Organizations/Organization Attachments Get all attachments  |
| Hub Organizations/Party Usage Assignments - Get all usage assignments | Hub Organizations/Party Usage Assignments Get all usage assignments  |
| Hub Organizations/Relationships - Get all relationships | Hub Organizations/Relationships Get all relationships  |
| Hub Organizations/Source System References - Get all source system references | Hub Organizations/Source System References Get all source system references  |
| Hub Persons - Get all persons | Hub Persons Get all persons  |
| Hub Persons/Additional Identifiers - Get all additional identifiers | Hub Persons/Additional Identifiers Get all additional identifiers  |
| Hub Persons/Additional Names - Get all additional names | Hub Persons/Additional Names Get all additional names  |
| Hub Persons/Addresses - Get all addresses | Hub Persons/Addresses Get all addresses  |
| Hub Persons/Attachments - Get all attachments | Hub Persons/Attachments Get all attachments  |
| Hub Persons/Contact Points - Get all contact points | Hub Persons/Contact Points Get all contact points  |
| Hub Persons/Customer Classifications - Get all customer classifications | Hub Persons/Customer Classifications Get all customer classifications  |
| Hub Persons/Notes - Get all notes | Hub Persons/Notes Get all notes  |
| Hub Persons/Relationships - Get all relationships | Hub Persons/Relationships Get all relationships  |
| Hub Persons/Source System References - Get all source system references | Hub Persons/Source System References Get all source system references  |
| Hub Persons/Usage Assignments - Get all usage assignments | Hub Persons/Usage Assignments Get all usage assignments  |
| Import Activities - Get all file import activities | Import Activities Get all file import activities  |
| Import Activities/Import Activity Data Files - Get all file import activity data files | Import Activities/Import Activity Data Files Get all file import activity data files  |
| Import Activity Maps - Get all file import maps | Import Activity Maps Get all file import maps  |
| Import Activity Maps/Import Activity Map Columns - Get all file import map columns | Import Activity Maps/Import Activity Map Columns Get all file import map columns  |
| Inbound Message Filters - Get all inbound message filters | Inbound Message Filters Get all inbound message filters  |
| Inbound Messages - Get all inbound messages | Inbound Messages Get all inbound messages  |
| Inbound Messages/Attachments - Get all attachments | Inbound Messages/Attachments Get all attachments  |
| Inbound Messages/Inbound Message Parts - Get all inbound message parts | Inbound Messages/Inbound Message Parts Get all inbound message parts  |
| Incentive Compensation Earnings - Get all earnings | Incentive Compensation Earnings Get all earnings  |
| Incentive Compensation Expressions - Get all expressions | Incentive Compensation Expressions Get all expressions  |
| Incentive Compensation Expressions/Expression Details - Get all expression details | Incentive Compensation Expressions/Expression Details Get all expression details  |
| Incentive Compensation Expressions/Expression Usages - Get all expression usages | Incentive Compensation Expressions/Expression Usages Get all expression usages  |
| Incentive Compensation Rule Hierarchies - Get all rule hierarchies | Incentive Compensation Rule Hierarchies Get all rule hierarchies  |
| Incentive Compensation Rule Hierarchies/Qualifying Criteria - Get all qualifying criteria | Incentive Compensation Rule Hierarchies/Qualifying Criteria Get all qualifying criteria  |
| Incentive Compensation Rule Hierarchies/Qualifying Criteria/Qualifying Attribute Values - Get all qualifying attribute values | Incentive Compensation Rule Hierarchies/Qualifying Criteria/Qualifying Attribute Values Get all qualifying attribute values  |
| Incentive Compensation Rule Hierarchies/Rule Assignments - Get all rule assignments | Incentive Compensation Rule Hierarchies/Rule Assignments Get all rule assignments  |
| Incentive Compensation Rule Hierarchies/Rules - Get all rules | Incentive Compensation Rule Hierarchies/Rules Get all rules  |
| Incentive Compensation Summarized Credits - Get all credits | Incentive Compensation Summarized Credits Get all credits  |
| Incentive Compensation Summarized Earnings - Get all summarized earnings | Incentive Compensation Summarized Earnings Get all summarized earnings  |
| Incentive Compensation Summarized Earnings Per Intervals - Get all earnings | Incentive Compensation Summarized Earnings Per Intervals Get all earnings  |
| Incentive Compensation Summarized Team Credits - Get all credits | Incentive Compensation Summarized Team Credits Get all credits  |
| Incentive Compensation Transactions - Get all transactions | Incentive Compensation Transactions Get all transactions  |
| Incentive Compensation Transactions/Credits - Get all credits | Incentive Compensation Transactions/Credits Get all credits  |
| Incentive Compensation Transactions/Transaction Descriptive Flex Fields - Get all transactions descriptive flex field | Incentive Compensation Transactions/Transaction Descriptive Flex Fields Get all transactions descriptive flex field  |
| Industries List of Values - Get all industries | Industries List of Values Get all industries  |
| Interactions - Get all interactions | Interactions Get all interactions  |
| Interactions/Child Interactions - Get all child interactions | Interactions/Child Interactions Get all child interactions  |
| Interactions/Interaction Participants - Get all interaction participants | Interactions/Interaction Participants Get all interaction participants  |
| Interactions/Interaction References - Get all interaction references | Interactions/Interaction References Get all interaction references  |
| Internal Service Requests - Get all service requests | Internal Service Requests Get all service requests  |
| Internal Service Requests/Attachments - Get all attachments | Internal Service Requests/Attachments Get all attachments  |
| Internal Service Requests/Channel Communications - Get all channel communications | Internal Service Requests/Channel Communications Get all channel communications  |
| Internal Service Requests/Contact Members - Get all contact members | Internal Service Requests/Contact Members Get all contact members  |
| Internal Service Requests/Interaction References - Get all service request interactions | Internal Service Requests/Interaction References Get all service request interactions  |
| Internal Service Requests/Messages - Get all messages | Internal Service Requests/Messages Get all messages  |
| Internal Service Requests/Messages/Attachments - Get all attachments | Internal Service Requests/Messages/Attachments Get all attachments  |
| Internal Service Requests/Messages/Message Channels - Get all channel communication messages | Internal Service Requests/Messages/Message Channels Get all channel communication messages  |
| Internal Service Requests/Milestones - Get all service request milestones | Internal Service Requests/Milestones Get all service request milestones  |
| Internal Service Requests/Milestones/Milestones History - Get all service request milestone history | Internal Service Requests/Milestones/Milestones History Get all service request milestone history  |
| Internal Service Requests/Resources - Get all resource members | Internal Service Requests/Resources Get all resource members  |
| Internal Service Requests/Service Request References - Get all service request references | Internal Service Requests/Service Request References Get all service request references  |
| Internal Service Requests/Smart Actions - Get all actions | Internal Service Requests/Smart Actions Get all actions  |
| Internal Service Requests/Smart Actions/Smart Action REST Payload Definitions - Get all action request payload definitions | Internal Service Requests/Smart Actions/Smart Action REST Payload Definitions Get all action request payload definitions  |
| Internal Service Requests/Smart Actions/Smart Action User Interface Definitions - Get all action navigation definitions | Internal Service Requests/Smart Actions/Smart Action User Interface Definitions Get all action navigation definitions  |
| Internal Service Requests/Smart Actions/Smart Action User Interface Definitions - Get all action path parameters | Internal Service Requests/Smart Actions/Smart Action User Interface Definitions Get all action path parameters  |
| Internal Service Requests/Tags - Get all tags | Internal Service Requests/Tags Get all tags  |
| Lightbox Documents - Get all Lightbox documents | Lightbox Documents Get all Lightbox documents  |
| Lightbox Documents/Document-shared-with-user Signals - Get all document-shared-with-user signals for a do... | Lightbox Documents/Document-shared-with-user Signals Get all document-shared-with-user signals for a document  |
| Lightbox Documents/Document-unshared-with-user Signals - Get all document-unshared-with-user signals for a ... | Lightbox Documents/Document-unshared-with-user Signals Get all document-unshared-with-user signals for a document  |
| Lightbox Documents/Document-viewed-outside-Lightbox Signals - Get all document-viewed-outside-Lightbox signals f... | Lightbox Documents/Document-viewed-outside-Lightbox Signals Get all document-viewed-outside-Lightbox signals for document  |
| Lightbox Documents/Document-Viewed-Within-Lightbox Signals - Get all document-viewed-within-Lightbox signals fo... | Lightbox Documents/Document-Viewed-Within-Lightbox Signals Get all document-viewed-within-Lightbox signals for a document  |
| Lightbox Documents/Lightbox Document Pages - Get all Lightbox document pages | Lightbox Documents/Lightbox Document Pages Get all Lightbox document pages  |
| Lightbox Documents/Lightbox Document Pages/Downloaded-from-cart Signals - Get all downloaded-from-cart signals for a page | Lightbox Documents/Lightbox Document Pages/Downloaded-from-cart Signals Get all downloaded-from-cart signals for a page  |
| Lightbox Documents/Lightbox Document Pages/Page-added-to-cart Signals - Get all page-added-to-cart signals for pages | Lightbox Documents/Lightbox Document Pages/Page-added-to-cart Signals Get all page-added-to-cart signals for pages  |
| Lightbox Documents/Lightbox Document Pages/Page-viewed-outside-Lightbox Signals - Get all page-viewed-outside-Lightbox signals for a... | Lightbox Documents/Lightbox Document Pages/Page-viewed-outside-Lightbox Signals Get all page-viewed-outside-Lightbox signals for a page  |
| Lightbox Documents/Lightbox Scoring Signals - Get all signals for downloaded documents | Lightbox Documents/Lightbox Scoring Signals Get all signals for downloaded documents  |
| Lightbox Documents/Shared-person Instances - Get all shared-person instances for a document | Lightbox Documents/Shared-person Instances Get all shared-person instances for a document  |
| Lightbox Presentation Session Feedback - Get all lightbox presentation session feedback ite... | Lightbox Presentation Session Feedback Get all lightbox presentation session feedback items  |
| Lightbox Presentation Sessions - Get all Lightbox presentation sessions | Lightbox Presentation Sessions Get all Lightbox presentation sessions  |
| Lightbox Presentation Sessions/Presentation Session Attendance Events - Get all Lightbox presentation session attendance e... | Lightbox Presentation Sessions/Presentation Session Attendance Events Get all Lightbox presentation session attendance events  |
| Lightbox Presentation Sessions/Presentation Session Display Events - Get all Lightbox presentation session display even... | Lightbox Presentation Sessions/Presentation Session Display Events Get all Lightbox presentation session display events  |
| List of Values/Action Status Conditions - Get all status conditions | List of Values/Action Status Conditions Get all status conditions  |
| List of Values/Activity Functions - Get all activity functions | List of Values/Activity Functions Get all activity functions  |
| List of Values/Activity Functions/Activity Type Lookups - Get all activity types | List of Values/Activity Functions/Activity Type Lookups Get all activity types  |
| List of Values/Activity Functions/Activity Type Lookups/Activity Subtypes - Get all activity subtypes | List of Values/Activity Functions/Activity Type Lookups/Activity Subtypes Get all activity subtypes  |
| List of Values/Activity Subtypes - Get all activity subtypes | List of Values/Activity Subtypes Get all activity subtypes  |
| List of Values/Activity Type Lookups - Get all activity types | List of Values/Activity Type Lookups Get all activity types  |
| List of Values/Application Roles - Get all application roles | List of Values/Application Roles Get all application roles  |
| List of Values/Assessment Templates - Get all assessment templates | List of Values/Assessment Templates Get all assessment templates  |
| List of Values/Benefit List Type Values - Get all benefit list type values | List of Values/Benefit List Type Values Get all benefit list type values  |
| List of Values/Capacity Categories - Get all capacity catagories | List of Values/Capacity Categories Get all capacity catagories  |
| List of Values/Capacity Categories/Capacities - Get all capacities | List of Values/Capacity Categories/Capacities Get all capacities  |
| List of Values/Capacity Categories/Time Slots - Get all time slots | List of Values/Capacity Categories/Time Slots Get all time slots  |
| List of Values/Channel Types - Get all channel types | List of Values/Channel Types Get all channel types  |
| List of Values/Channel Types/Channels - Get all channels associated with a channel type | List of Values/Channel Types/Channels Get all channels associated with a channel type  |
| List of Values/Class Codes - Get all classification codes | List of Values/Class Codes Get all classification codes  |
| List of Values/Collaboration Recipients - Get all collaboration recipients | List of Values/Collaboration Recipients Get all collaboration recipients  |
| List of Values/Dynamic Link Values - Get all business units | List of Values/Dynamic Link Values Get all business units  |
| List of Values/Feedback Ratings - Get all feedback ratings | List of Values/Feedback Ratings Get all feedback ratings  |
| List of Values/Geographies - Get all geographies | List of Values/Geographies Get all geographies  |
| List of Values/Get Configurations - Get all configurations | List of Values/Get Configurations Get all configurations  |
| List of Values/HR Service Request Resolve Outcomes - Get all outcomes | List of Values/HR Service Request Resolve Outcomes Get all outcomes  |
| List of Values/HR Service Request Resolve Outcomes/HR Service Request Resolve Outcome Resolutions - Get all outcome resolutions | List of Values/HR Service Request Resolve Outcomes/HR Service Request Resolve Outcome Resolutions Get all outcome resolutions  |
| List of Values/HZ Lookups - Get all lookups | List of Values/HZ Lookups Get all lookups  |
| List of Values/Import Export Objects Metadata - Get all file import and export objects | List of Values/Import Export Objects Metadata Get all file import and export objects  |
| List of Values/Import Export Objects Metadata/Import Export Object Attributes - Get all file import and export object attributes | List of Values/Import Export Objects Metadata/Import Export Object Attributes Get all file import and export object attributes  |
| List of Values/Internal Service Request Outcomes - Get all outcomes | List of Values/Internal Service Request Outcomes Get all outcomes  |
| List of Values/Internal Service Request Outcomes/Internal Service Request Outcome Resolutions - Get all outcome resolutions | List of Values/Internal Service Request Outcomes/Internal Service Request Outcome Resolutions Get all outcome resolutions  |
| List of Values/KPI - Get all KPIs | List of Values/KPI Get all KPIs  |
| List of Values/KPI/KPI History Metadata - Get all KPI history metadata | List of Values/KPI/KPI History Metadata Get all KPI history metadata  |
| List of Values/Lightbox Document Types - Get all lightbox document types | List of Values/Lightbox Document Types Get all lightbox document types  |
| List of Values/Loy Events - Get all events | List of Values/Loy Events Get all events  |
| List of Values/Loy PointTypes - Get all point types | List of Values/Loy PointTypes Get all point types  |
| List of Values/Name and ID Types - Get All Name And Types | List of Values/Name and ID Types Get All Name And Types  |
| List of Values/Note Types - Get all note types | List of Values/Note Types Get all note types  |
| List of Values/Opportunity Status Values - Get all opportunity status values | List of Values/Opportunity Status Values Get all opportunity status values  |
| List of Values/Partner Contact Managers List of Values - Get all partner contact managers | List of Values/Partner Contact Managers List of Values Get all partner contact managers  |
| List of Values/Partner Enrolled Programs List of Values - Get all partner enrolled programs | List of Values/Partner Enrolled Programs List of Values Get all partner enrolled programs  |
| List of Values/Partner Programs - Get all partner programs | List of Values/Partner Programs Get all partner programs  |
| List of Values/Partner Types List of Values - Get all partner types | List of Values/Partner Types List of Values Get all partner types  |
| List of Values/Party Usages - Get all party usages | List of Values/Party Usages Get all party usages  |
| List of Values/Phone Country Codes - Get all Country Codes | List of Values/Phone Country Codes Get all Country Codes  |
| List of Values/Product Groups - Get all product groups | List of Values/Product Groups Get all product groups  |
| List of Values/Program Tiers - Get all program tiers | List of Values/Program Tiers Get all program tiers  |
| List of Values/Rated Currencies - Get all rated currencies | List of Values/Rated Currencies Get all rated currencies  |
| List of Values/Relationship Types - Get all relationship types | List of Values/Relationship Types Get all relationship types  |
| List of Values/Sales Methods - Get all sales methods | List of Values/Sales Methods Get all sales methods  |
| List of Values/Sales Stages - Get all sales stages | List of Values/Sales Stages Get all sales stages  |
| List of Values/Service Business Units - Get all service business units | List of Values/Service Business Units Get all service business units  |
| List of Values/Service Business Units/Business Unit Lookups - Get all reference set enabled lookups | List of Values/Service Business Units/Business Unit Lookups Get all reference set enabled lookups  |
| List of Values/Service Business Units/Categories - Get all categories | List of Values/Service Business Units/Categories Get all categories  |
| List of Values/Service Business Units/Channels - Get all channels | List of Values/Service Business Units/Channels Get all channels  |
| List of Values/Service Business Units/Profile Options - Get all profile options | List of Values/Service Business Units/Profile Options Get all profile options  |
| List of Values/Service Lookup Properties - Get all service lookup properties | List of Values/Service Lookup Properties Get all service lookup properties  |
| List of Values/Service Request Outcomes - Get all outcomes | List of Values/Service Request Outcomes Get all outcomes  |
| List of Values/Service Request Outcomes/Service Request Outcome Resolutions - Get all outcome resolutions | List of Values/Service Request Outcomes/Service Request Outcome Resolutions Get all outcome resolutions  |
| List of Values/Service Request Statuses - Get all lookup codes | List of Values/Service Request Statuses Get all lookup codes  |
| List of Values/Service Request Statuses - Get all lookup codes | List of Values/Service Request Statuses Get all lookup codes  |
| List of Values/Service Work Order Areas - Get all work areas | List of Values/Service Work Order Areas Get all work areas  |
| List of Values/Service Work Order Areas/Work Order Areas - Get all work order areas | List of Values/Service Work Order Areas/Work Order Areas Get all work order areas  |
| List of Values/Smart Text  Folders and Texts - Get folders and smart texts | List of Values/Smart Text  Folders and Texts Get folders and smart texts  |
| List of Values/Smart Text  Folders and Texts/Parents - Get parent folders and smart texts | List of Values/Smart Text  Folders and Texts/Parents Get parent folders and smart texts  |
| List of Values/Smart Text Objects - Get a smart text object | List of Values/Smart Text Objects Get a smart text object  |
| List of Values/Smart Text Variables - Get all smart text variables | List of Values/Smart Text Variables Get all smart text variables  |
| List of Values/Source Codes - Get all source codes | List of Values/Source Codes Get all source codes  |
| List of Values/Tags - Get all tags | List of Values/Tags Get all tags  |
| List of Values/Work Order Status Codes - Get all work order status codes | List of Values/Work Order Status Codes Get all work order status codes  |
| List of Values/Work Order Type Mappings - Get all work order type mappings | List of Values/Work Order Type Mappings Get all work order type mappings  |
| Lookups - Get all lookups | Lookups Get all lookups  |
| MDF Requests - Get all MDF requests | MDF Requests Get all MDF requests  |
| MDF Requests/Claims - Get all claims | MDF Requests/Claims Get all claims  |
| MDF Requests/MDF Request Teams - Get all fund request resource | MDF Requests/MDF Request Teams Get all fund request resource  |
| MDF Requests/Notes - Get all notes | MDF Requests/Notes Get all notes  |
| Milestones - Get all milestones | Milestones Get all milestones  |
| Multi Channel Adapter Events - Get all events | Multi Channel Adapter Events Get all events  |
| Multi-Channel Adapter Toolbars - Get all toolbars | Multi-Channel Adapter Toolbars Get all toolbars  |
| Multi-Channel Adapter Toolbars/Multi-Channel Adapter Toolbar Additions - Get all toolbar additions | Multi-Channel Adapter Toolbars/Multi-Channel Adapter Toolbar Additions Get all toolbar additions  |
| My Self-Service Roles - Get all the self-service roles | My Self-Service Roles Get all the self-service roles  |
| Non-Duplicate Records - Get all non-duplicate records | Non-Duplicate Records Get all non-duplicate records  |
| Object Capacities - Get all object capacities | Object Capacities Get all object capacities  |
| Objectgrowthv2 - Retrieve object create count | Application Usage Insights by channel by day. The channel is the means of accessing the application, such as Web, Mobile, or Email Retrieve object create count  |
| Objectives - Get all objectives | Objectives Get all objectives  |
| Objectives/Objective Splits - Get all objective splits | Objectives/Objective Splits Get all objective splits  |
| Omnichannel Presence - Get all omnichannel presence | Omnichannel Presence Get all omnichannel presence  |
| Omnichannel Properties - Get all omnichannel properties | Omnichannel Properties Get all omnichannel properties  |
| Opportunities - Get all opportunities | Opportunities Get all opportunities  |
| Opportunities/Assessments - Get all assessments | Opportunities/Assessments Get all assessments  |
| Opportunities/Assessments/Assessment Answer Groups - Get all assessment answer groups | Opportunities/Assessments/Assessment Answer Groups Get all assessment answer groups  |
| Opportunities/Assessments/Assessment Answer Groups/Assessment Answers - Get all assessment answers | Opportunities/Assessments/Assessment Answer Groups/Assessment Answers Get all assessment answers  |
| Opportunities/Notes - Get all notes | Opportunities/Notes Get all notes  |
| Opportunities/Opportunity Competitors - Get all opportunity competitors | Opportunities/Opportunity Competitors Get all opportunity competitors  |
| Opportunities/Opportunity Contacts - Get all opportunity contacts | Opportunities/Opportunity Contacts Get all opportunity contacts  |
| Opportunities/Opportunity Deals - Get all opportunity deals | Opportunities/Opportunity Deals Get all opportunity deals  |
| Opportunities/Opportunity Leads - Get all opportunity leads | Opportunities/Opportunity Leads Get all opportunity leads  |
| Opportunities/Opportunity Partners - Get all opportunity revenue partners | Opportunities/Opportunity Partners Get all opportunity revenue partners  |
| Opportunities/Opportunity Sources - Get all opportunity sources | Opportunities/Opportunity Sources Get all opportunity sources  |
| Opportunities/Opportunity Stage Snapshots - Get all opportunity stage snapshots | Opportunities/Opportunity Stage Snapshots Get all opportunity stage snapshots  |
| Opportunities/Opportunity Team Members - Get all opportunity team members | Opportunities/Opportunity Team Members Get all opportunity team members  |
| Opportunities/Revenue Items - Get all opportunity revenues | Opportunities/Revenue Items Get all opportunity revenues  |
| Opportunities/Revenue Items/Child Split Revenues - Get all child split revenues | Opportunities/Revenue Items/Child Split Revenues Get all child split revenues  |
| Opportunities/Revenue Items/Opportunity Revenue Territories - Get all opportunity revenue territories | Opportunities/Revenue Items/Opportunity Revenue Territories Get all opportunity revenue territories  |
| Opportunities/Revenue Items/Product Groups - Get all product groups | Opportunities/Revenue Items/Product Groups Get all product groups  |
| Opportunities/Revenue Items/Products - Get all products | Opportunities/Revenue Items/Products Get all products  |
| Opportunities/Revenue Items/Recurring Revenues - Get all recurring revenues | Opportunities/Revenue Items/Recurring Revenues Get all recurring revenues  |
| Opportunities/Smart Actions - Get all smart actions | Opportunities/Smart Actions Get all smart actions  |
| Opportunities/Smart Actions/Smart Action REST Path Parameter Definitions - Get all action URL bindings (Not Supported) | Opportunities/Smart Actions/Smart Action REST Path Parameter Definitions Get all action URL bindings (Not Supported)  |
| Opportunities/Smart Actions/Smart Action REST Payload Definitions - Get all action request payload (Not Supported) | Opportunities/Smart Actions/Smart Action REST Payload Definitions Get all action request payload (Not Supported)  |
| Opportunities/Smart Actions/Smart Action User Interface Definitions - Get all action navigations (Not Supported) | Opportunities/Smart Actions/Smart Action User Interface Definitions Get all action navigations (Not Supported)  |
| Outbound Messages - Get all outbound messages | Outbound Messages Get all outbound messages  |
| Outbound Messages/Outbound Message Parts - Get all outbound message parts | Outbound Messages/Outbound Message Parts Get all outbound message parts  |
| Participant Compensation Plans - Get all participant compensation plans | Participant Compensation Plans Get all participant compensation plans  |
| Participant Compensation Plans/Descriptive Flex Fields - Get all descriptive flex fields for a participant ... | Participant Compensation Plans/Descriptive Flex Fields Get all descriptive flex fields for a participant plan  |
| Participant Compensation Plans/Plan Components - Get all plan components | Participant Compensation Plans/Plan Components Get all plan components  |
| Participant Compensation Plans/Plan Components/Performance Measures - Get all performance measures | Participant Compensation Plans/Plan Components/Performance Measures Get all performance measures  |
| Participant Compensation Plans/Plan Components/Performance Measures/Goals - Get all performance measure goals | Participant Compensation Plans/Plan Components/Performance Measures/Goals Get all performance measure goals  |
| Participant Compensation Plans/Plan Components/Performance Measures/Goals/Interval Goals - Get all interval goals | Participant Compensation Plans/Plan Components/Performance Measures/Goals/Interval Goals Get all interval goals  |
| Participant Compensation Plans/Plan Components/Performance Measures/Goals/Period Goals - Get all period goals | Participant Compensation Plans/Plan Components/Performance Measures/Goals/Period Goals Get all period goals  |
| Participant Compensation Plans/Plan Components/Performance Measures/Scorecards - Get all performance measure scorecards | Participant Compensation Plans/Plan Components/Performance Measures/Scorecards Get all performance measure scorecards  |
| Participant Compensation Plans/Plan Components/Performance Measures/Scorecards/Scorecard Rates - Get all scorecard rates | Participant Compensation Plans/Plan Components/Performance Measures/Scorecards/Scorecard Rates Get all scorecard rates  |
| Participant Compensation Plans/Plan Components/Rate Tables - Get all plan component rate tables | Participant Compensation Plans/Plan Components/Rate Tables Get all plan component rate tables  |
| Participant Compensation Plans/Plan Components/Rate Tables/Rate Table Rates - Get all rate table rates | Participant Compensation Plans/Plan Components/Rate Tables/Rate Table Rates Get all rate table rates  |
| Participants - Get all participants | Participants Get all participants  |
| Participants List of Values - Get all participants | Participants List of Values Get all participants  |
| Participants/Participant Details - Get all participant details | Participants/Participant Details Get all participant details  |
| Participants/Participant Details/Participant Details Descriptive Flex Fields - Get all descriptive flex fields | Participants/Participant Details/Participant Details Descriptive Flex Fields Get all descriptive flex fields  |
| Participants/Participant Roles - Get all participant roles | Participants/Participant Roles Get all participant roles  |
| Participants/Participants Descriptive Flex Fields - Get all descriptive flex fields | Participants/Participants Descriptive Flex Fields Get all descriptive flex fields  |
| Partner Contacts - Get all partner contacts | Partner Contacts Get all partner contacts  |
| Partner Contacts/Attachments - Get all attachments | Partner Contacts/Attachments Get all attachments  |
| Partner Contacts/Partner Contact User Details - Get all user details | Partner Contacts/Partner Contact User Details Get all user details  |
| Partner Programs - Get all partner programs | Partner Programs Get all partner programs  |
| Partner Programs/Countries - Get all countries for a partner program | Partner Programs/Countries Get all countries for a partner program  |
| Partner Programs/Program Benefit Details - Get all program benefits for a partner program | Partner Programs/Program Benefit Details Get all program benefits for a partner program  |
| Partner Programs/Tiers - Get all tiers for a partner program | Partner Programs/Tiers Get all tiers for a partner program  |
| Partner Tiers - Get all partner tiers | Partner Tiers Get all partner tiers  |
| Partners - Get all partners | Partners Get all partners  |
| Partners/Addresses - Get all addresses | Partners/Addresses Get all addresses  |
| Partners/Attachments - Get all partner attachments | Partners/Attachments Get all partner attachments  |
| Partners/Expertises - Get all expertises for a partner | Partners/Expertises Get all expertises for a partner  |
| Partners/Focus Areas - Get all product specialties for a partner | Partners/Focus Areas Get all product specialties for a partner  |
| Partners/Geographies - Get all geographies for a partner | Partners/Geographies Get all geographies for a partner  |
| Partners/Industries - Get all industries for a partner | Partners/Industries Get all industries for a partner  |
| Partners/Notes - Get all notes for a partner | Partners/Notes Get all notes for a partner  |
| Partners/Partner Account Team Members - Get all partner account team members for a partner | Partners/Partner Account Team Members Get all partner account team members for a partner  |
| Partners/Partner Announcements - Get all partner announcements | Partners/Partner Announcements Get all partner announcements  |
| Partners/Partner Certifications - Get all certifications for a partner | Partners/Partner Certifications Get all certifications for a partner  |
| Partners/Partner Contacts - Get all partner contacts | Partners/Partner Contacts Get all partner contacts  |
| Partners/Partner Contacts/Attachments - Get all partner attachments | Partners/Partner Contacts/Attachments Get all partner attachments  |
| Partners/Partner Contacts/User Account Details - Get all partner contact user details | Partners/Partner Contacts/User Account Details Get all partner contact user details  |
| Partners/Partner Types - Get all partner types | Partners/Partner Types Get all partner types  |
| Partners/Smart Actions - Get all smart actions | Partners/Smart Actions Get all smart actions  |
| Partners/Smart Actions/Smart Action REST Path Parameter Definitions - Get all action URL bindings (Not Supported) | Partners/Smart Actions/Smart Action REST Path Parameter Definitions Get all action URL bindings (Not Supported)  |
| Partners/Smart Actions/Smart Action REST Payload Definitions - Get all action request payload (Not Supported) | Partners/Smart Actions/Smart Action REST Payload Definitions Get all action request payload (Not Supported)  |
| Partners/Smart Actions/Smart Action User Interface Definitions - Get all action navigations (Not Supported) | Partners/Smart Actions/Smart Action User Interface Definitions Get all action navigations (Not Supported)  |
| Passive Beacon Service Statuses - Get all passive beacon provider statuses | Passive Beacon Service Statuses Get all passive beacon provider statuses  |
| Passive Beacon Service Statuses/Passive Beacon Provider Statuses - Get all passive beacon service statuses | Passive Beacon Service Statuses/Passive Beacon Provider Statuses Get all passive beacon service statuses  |
| Pay Groups - Get all pay groups | Pay Groups Get all pay groups  |
| Pay Groups/Pay Group Assignments - Get all pay group assignments | Pay Groups/Pay Group Assignments Get all pay group assignments  |
| Pay Groups/Pay Group Roles - Get all pay group roles | Pay Groups/Pay Group Roles Get all pay group roles  |
| Payment Batch Participant Plan Components List of Values - Get all payment batch participant plan components | Payment Batch Participant Plan Components List of Values Get all payment batch participant plan components  |
| Payment Batches - Get all payment batches | Payment Batches Get all payment batches  |
| Payment Batches/Payment Transactions - Get all payment transactions | Payment Batches/Payment Transactions Get all payment transactions  |
| Payment Batches/Paysheets - Get all paysheets | Payment Batches/Paysheets Get all paysheets  |
| Payment Batches/Paysheets/Payment Transactions - Get all payment transactions | Payment Batches/Paysheets/Payment Transactions Get all payment transactions  |
| Payment Plans - Get all payment plans | Payment Plans Get all payment plans  |
| Payment Plans/Payment Plans Assignments - Get all payment plan assignment | Payment Plans/Payment Plans Assignments Get all payment plan assignment  |
| Payment Plans/Payment Plans Roles - Get all payment plan roles | Payment Plans/Payment Plans Roles Get all payment plan roles  |
| Payment Transactions - Get all payment transactions | Payment Transactions Get all payment transactions  |
| Paysheets - Get all paysheets | Paysheets Get all paysheets  |
| Paysheets/Payment Transactions - Get all payment transactions | Paysheets/Payment Transactions Get all payment transactions  |
| Performance Intervals List of Values - Get all interval types | Performance Intervals List of Values Get all interval types  |
| Performance Measures - Get all performance measures | Performance Measures Get all performance measures  |
| Performance Measures/Credit Categories - Get all credit categories | Performance Measures/Credit Categories Get all credit categories  |
| Performance Measures/Credit Categories/Credit Factors - Get all credit factors | Performance Measures/Credit Categories/Credit Factors Get all credit factors  |
| Performance Measures/Credit Categories/Transaction Factors - Get all transaction factors | Performance Measures/Credit Categories/Transaction Factors Get all transaction factors  |
| Performance Measures/Descriptive Flex fields - Get all descriptive flex fields | Performance Measures/Descriptive Flex fields Get all descriptive flex fields  |
| Performance Measures/Goals - Get all goals | Performance Measures/Goals Get all goals  |
| Performance Measures/Goals/Interval Goals - Get all interval goals | Performance Measures/Goals/Interval Goals Get all interval goals  |
| Performance Measures/Goals/Interval Goals/Period Goals - Get all period goals | Performance Measures/Goals/Interval Goals/Period Goals Get all period goals  |
| Performance Measures/Rate Dimensional Input Expressions - Get all rate dimensional inputs | Performance Measures/Rate Dimensional Input Expressions Get all rate dimensional inputs  |
| Performance Measures/Scorecards - Get all scorecards | Performance Measures/Scorecards Get all scorecards  |
| Period Types - Get all period types | Period Types Get all period types  |
| Period Types/End Periods - Get all end periods for the period type | Period Types/End Periods Get all end periods for the period type  |
| Period Types/Start Period Lookups - Get all start periods | Period Types/Start Period Lookups Get all start periods  |
| Periods List of Values - Get all periods | Periods List of Values Get all periods  |
| Phone Verifications - Get all phone verifications | Phone Verifications Get all phone verifications  |
| Plan Components - Get all plan components | Plan Components Get all plan components  |
| Plan Components/Plan Component - Incentive Formulas - Get all incentive formulas | Plan Components/Plan Component - Incentive Formulas Get all incentive formulas  |
| Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Dimensional Input Expressions - Get all rate dimensional inputs | Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Dimensional Input Expressions Get all rate dimensional inputs  |
| Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Tables - Get all rate tables | Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Tables Get all rate tables  |
| Plan Components/Plan Component - Performance Measures - Get all performance measures | Plan Components/Plan Component - Performance Measures Get all performance measures  |
| Plan Components/Plan Component Descriptive Flex Fields - Get all descriptive flex fields | Plan Components/Plan Component Descriptive Flex Fields Get all descriptive flex fields  |
| Plan Document Templates List of Values - Get all plan document templates | Plan Document Templates List of Values Get all plan document templates  |
| Popflows - Retrieve the top flows by click count | Application Usage Insights Retrieve the top flows by the number of clicks in the flow Retrieve the top flows by click count  |
| Price Book Headers - Get all pricebooks | Price Book Headers Get all pricebooks  |
| Price Book Headers/Price Book Items - Get all pricebook items | Price Book Headers/Price Book Items Get all pricebook items  |
| Process Metadatas - Get all process metadatas | Process Metadatas Get all process metadatas  |
| Product Group Usages - Get all product group usages | Product Group Usages Get all product group usages  |
| Product Group Usages/Product Group Usage Functions - Get all product group usage engines | Product Group Usages/Product Group Usage Functions Get all product group usage engines  |
| Product Group Usages/Product Group Usage Modes - Get all product group usage modes | Product Group Usages/Product Group Usage Modes Get all product group usage modes  |
| Product Group Usages/Product Group Usage Preferences - Get all product group usage preferences | Product Group Usages/Product Group Usage Preferences Get all product group usage preferences  |
| Product Group Usages/Product Group Usage Roots - Get all root product groups | Product Group Usages/Product Group Usage Roots Get all root product groups  |
| Product Groups - Get all product groups | Product Groups Get all product groups  |
| Product Groups/Attachments - Get all attachment for a product group | Product Groups/Attachments Get all attachment for a product group  |
| Product Groups/Filter Attributes - Get all attributes on a product group | Product Groups/Filter Attributes Get all attributes on a product group  |
| Product Groups/Filter Attributes/Filter Attribute Values - Get all attribute values for a given attribute set... | Product Groups/Filter Attributes/Filter Attribute Values Get all attribute values for a given attribute setup  |
| Product Groups/Products - Get all product relationships on a product group | Product Groups/Products Get all product relationships on a product group  |
| Product Groups/Products/EligibilityRules - Get all eligibility rules | Product Groups/Products/EligibilityRules Get all eligibility rules  |
| Product Groups/Related Groups - Get all subgroup relationships | Product Groups/Related Groups Get all subgroup relationships  |
| Product Groups/Subgroups - Get all subgroups within a product group | Product Groups/Subgroups Get all subgroups within a product group  |
| Product Specialities List of Values - Get a list of product specialties | Product Specialities List of Values Get a list of product specialties  |
| Products - Get all products | Products Get all products  |
| Products/Default Prices - Get all default prices | Products/Default Prices Get all default prices  |
| Products/Product Attachments - Get all attachments for a product | Products/Product Attachments Get all attachments for a product  |
| Products/Product Image Attachments - Get all image attachments for a product | Products/Product Image Attachments Get all image attachments for a product  |
| Products/Product Translations - Get all product translations | Products/Product Translations Get all product translations  |
| Program Benefits - Get all program benefits | Program Benefits Get all program benefits  |
| Program Benefits/Benefit List Values - Get all benefit list type values | Program Benefits/Benefit List Values Get all benefit list type values  |
| Program Enrollments - Get all enrollments | Program Enrollments Get all enrollments  |
| Program Enrollments/Notes - Get all notes for an enrollment | Program Enrollments/Notes Get all notes for an enrollment  |
| Program Enrollments/Partner Programs - Get all the partner programs for the enrollment | Program Enrollments/Partner Programs Get all the partner programs for the enrollment  |
| Program Enrollments/Program Benefit Details - Get all program benefit details for an enrollment | Program Enrollments/Program Benefit Details Get all program benefit details for an enrollment  |
| Queues - Get all queues | Queues Get all queues  |
| Queues/Overflow Queues - Get all overflow queues | Queues/Overflow Queues Get all overflow queues  |
| Queues/Queue Resource Members - Get all resource members | Queues/Queue Resource Members Get all resource members  |
| Queues/Queue Resource Teams - Get all resource teams | Queues/Queue Resource Teams Get all resource teams  |
| Queues/Queue Resource Teams/Queue Resource Team Members - Get all resource team members | Queues/Queue Resource Teams/Queue Resource Team Members Get all resource team members  |
| Quote and Order Lines - Get all sales order lines | Quote and Order Lines Get all sales order lines  |
| Rate Dimensions - Get all rate dimensions | Rate Dimensions Get all rate dimensions  |
| Rate Dimensions/Rate Dimension - Tiers - Get all rate dimension tiers | Rate Dimensions/Rate Dimension - Tiers Get all rate dimension tiers  |
| Rate Tables - Get all rate table details | Rate Tables Get all rate table details  |
| Rate Tables/Rate Table Dimensions - Get all rate dimension details within a rate table | Rate Tables/Rate Table Dimensions Get all rate dimension details within a rate table  |
| Rate Tables/Rate Table Dimensions/Rate Dimensions Tiers - Get all rate dimension tiers | Rate Tables/Rate Table Dimensions/Rate Dimensions Tiers Get all rate dimension tiers  |
| Rate Tables/Rate Table Rates - Get all rate table rates | Rate Tables/Rate Table Rates Get all rate table rates  |
| Resolution Links - Get all links | Resolution Links Get all links  |
| Resolution Links/Link Members - Get all link members for the link | Resolution Links/Link Members Get all link members for the link  |
| Resolution Requests - Get all resolution requests | Resolution Requests Get all resolution requests  |
| Resolution Requests/Duplicate Parties - Get all duplicate parties | Resolution Requests/Duplicate Parties Get all duplicate parties  |
| Resolution Requests/Duplicate Parties/Source System References - Get all source system references | Resolution Requests/Duplicate Parties/Source System References Get all source system references  |
| Resolution Requests/Resolution Details - Get all detailed records for all duplicate party r... | Resolution Requests/Resolution Details Get all detailed records for all duplicate party resolutions  |
| Resource Capacities - Get all resource capacities | Resource Capacities Get all resource capacities  |
| Resource Users - Get all resource users | Resource Users Get all resource users  |
| Resource Users/Resource Role Assignments - Get all role assignments for a resource | Resource Users/Resource Role Assignments Get all role assignments for a resource  |
| Resources - Get all resources | Resources Get all resources  |
| Resources/Picture Attachments - Get all resources' pictures | Resources/Picture Attachments Get all resources' pictures  |
| Roles - Get all incentive compensation role details | Roles Get all incentive compensation role details  |
| Roles/Role - Participants - Get all participants for a role | Roles/Role - Participants Get all participants for a role  |
| Rule Attributes List of Values - Get all rule attributes | Rule Attributes List of Values Get all rule attributes  |
| Rule Types List of Values - Get all rule types | Rule Types List of Values Get all rule types  |
| Sales Leads - Get all sales leads | Sales Leads Get all sales leads  |
| Sales Leads/Lead Qualifications - Get all lead qualifications | Sales Leads/Lead Qualifications Get all lead qualifications  |
| Sales Leads/Lead Qualifications/Assessment Answer Groups - Get all lead qualification answer groups | Sales Leads/Lead Qualifications/Assessment Answer Groups Get all lead qualification answer groups  |
| Sales Leads/Lead Qualifications/Assessment Answer Groups/Assessment Answers - Get all lead qualification answers | Sales Leads/Lead Qualifications/Assessment Answer Groups/Assessment Answers Get all lead qualification answers  |
| Sales Leads/Notes - Get all notes | Sales Leads/Notes Get all notes  |
| Sales Leads/Opportunities - Get all opportunities | Sales Leads/Opportunities Get all opportunities  |
| Sales Leads/Product Groups - Get all product groups | Sales Leads/Product Groups Get all product groups  |
| Sales Leads/Products - Get all products | Sales Leads/Products Get all products  |
| Sales Leads/Sales Lead Contacts - Get all sales lead contacts | Sales Leads/Sales Lead Contacts Get all sales lead contacts  |
| Sales Leads/Sales Lead Products - Get all sales lead products | Sales Leads/Sales Lead Products Get all sales lead products  |
| Sales Leads/Sales Lead Resources - Get all sales lead resources | Sales Leads/Sales Lead Resources Get all sales lead resources  |
| Sales Leads/Sales Lead Territories - Get all sales lead territories | Sales Leads/Sales Lead Territories Get all sales lead territories  |
| Sales Leads/Smart Actions - Get all smart actions | Sales Leads/Smart Actions Get all smart actions  |
| Sales Leads/Smart Actions/Smart Action REST Path Parameter Definitions - Get all action URL bindings (Not Supported) | Sales Leads/Smart Actions/Smart Action REST Path Parameter Definitions Get all action URL bindings (Not Supported)  |
| Sales Leads/Smart Actions/Smart Action REST Payload Definitions - Get all action request payload (Not Supported) | Sales Leads/Smart Actions/Smart Action REST Payload Definitions Get all action request payload (Not Supported)  |
| Sales Leads/Smart Actions/Smart Action User Interface Definitions - Get all action navigations (Not Supported) | Sales Leads/Smart Actions/Smart Action User Interface Definitions Get all action navigations (Not Supported)  |
| Sales Objective Types - Get all sales objective types | Sales Objective Types Get all sales objective types  |
| Sales Order CPQ Integration Configurations - Get all quote integration configurations | Sales Order CPQ Integration Configurations Get all quote integration configurations  |
| Sales Order CPQ Integration Configurations/Sales Order Setup Disabled Revenue Fields - Get all disabled revenue fields | Sales Order CPQ Integration Configurations/Sales Order Setup Disabled Revenue Fields Get all disabled revenue fields  |
| Sales Order CPQ Integration Configurations/Sales Order Setup Reference Details - Get all quote setup reference details | Sales Order CPQ Integration Configurations/Sales Order Setup Reference Details Get all quote setup reference details  |
| Sales Orders - Get all quotes | Sales Orders Get all quotes  |
| Sales Orders/Quote and Order Lines - Get all sales order lines | Sales Orders/Quote and Order Lines Get all sales order lines  |
| Sales Promotions - Get all sales promotions | Sales Promotions Get all sales promotions  |
| Sales Territories - Get all territories | Sales Territories Get all territories  |
| Sales Territories/Line of Business - Get all lines of business for a territory | Sales Territories/Line of Business Get all lines of business for a territory  |
| Sales Territories/Resources - Get all resources for a territory | Sales Territories/Resources Get all resources for a territory  |
| Sales Territories/Rules - Get all rules for a territory | Sales Territories/Rules Get all rules for a territory  |
| Sales Territories/Rules/Rule Boundaries - Get all rule boundaries for a territory | Sales Territories/Rules/Rule Boundaries Get all rule boundaries for a territory  |
| Sales Territories/Rules/Rule Boundaries/Rule Boundary Values - Get all rule boundary values for a territory | Sales Territories/Rules/Rule Boundaries/Rule Boundary Values Get all rule boundary values for a territory  |
| Sales Territory Proposals - Get all proposals | Sales Territory Proposals Get all proposals  |
| Screen Pop Pages - Get all screen pop pages | Screen Pop Pages Get all screen pop pages  |
| Screen Pop Pages/Screen Pop Page Parameters - Get all page parameters | Screen Pop Pages/Screen Pop Page Parameters Get all page parameters  |
| Screen Pop Pages/Screen Pop Page Parameters/Screen Pop Page Map Parameters - Get all page map parameters | Screen Pop Pages/Screen Pop Page Parameters/Screen Pop Page Map Parameters Get all page map parameters  |
| Screen Pop Tokens - Get all screen pop tokens | Screen Pop Tokens Get all screen pop tokens  |
| Self-Service Registrations - Get all self-service registration requests | Self-Service Registrations Get all self-service registration requests  |
| Self-Service Roles - Get all roles for a self-service user | Self-Service Roles Get all roles for a self-service user  |
| Self-Service Users - Get all self-service users | Self-Service Users Get all self-service users  |
| Self-Service Users - Get all the self-service users | Self-Service Users Get all the self-service users  |
| Self-Service Users/Self-Service Roles - Get all self-service roles | Self-Service Users/Self-Service Roles Get all self-service roles  |
| Self-Service Users/Self-Service Roles - Get all self-service roles | Self-Service Users/Self-Service Roles Get all self-service roles  |
| Self-Service Users/Self-Service Roles Histories - Get all self-service role histories | Self-Service Users/Self-Service Roles Histories Get all self-service role histories  |
| Self-Service Users/Self-Service Roles Histories - Get all self-service role history | Self-Service Users/Self-Service Roles Histories Get all self-service role history  |
| Service Details - Get all service details | Service Details Get all service details  |
| Service Providers - Get all service providers | Service Providers Get all service providers  |
| Service Providers/Services - Get all services | Service Providers/Services Get all services  |
| Service Providers/Services/Service Histories - Get all service histories | Service Providers/Services/Service Histories Get all service histories  |
| Service Requests - Get all service requests | Service Requests Get all service requests  |
| Service Requests/Activities - Get all activities | Service Requests/Activities Get all activities  |
| Service Requests/Attachments - Get all attachments | Service Requests/Attachments Get all attachments  |
| Service Requests/Channel Communications - Get all channel communications | Service Requests/Channel Communications Get all channel communications  |
| Service Requests/Contact Members - Get all contacts | Service Requests/Contact Members Get all contacts  |
| Service Requests/Interaction References - Get all service request interactions | Service Requests/Interaction References Get all service request interactions  |
| Service Requests/Messages - Get all messages | Service Requests/Messages Get all messages  |
| Service Requests/Messages/Attachments - Get all attachments | Service Requests/Messages/Attachments Get all attachments  |
| Service Requests/Messages/Channel Communications - Get all channel communications | Service Requests/Messages/Channel Communications Get all channel communications  |
| Service Requests/Milestones - Get all service request milestones | Service Requests/Milestones Get all service request milestones  |
| Service Requests/Milestones/Milestone Codes - Get all service request milestone history | Service Requests/Milestones/Milestone Codes Get all service request milestone history  |
| Service Requests/Resources - Get all resource members | Service Requests/Resources Get all resource members  |
| Service Requests/Service Request References - Get all service request references | Service Requests/Service Request References Get all service request references  |
| Service Requests/Smart Actions - Get all smart actions | Service Requests/Smart Actions Get all smart actions  |
| Service Requests/Smart Actions/Smart Action REST Path Parameter Definitions - Get all action URL bindings (Not Supported) | Service Requests/Smart Actions/Smart Action REST Path Parameter Definitions Get all action URL bindings (Not Supported)  |
| Service Requests/Smart Actions/Smart Action REST Payload Definitions - Get all action request payloads (Not Supported) | Service Requests/Smart Actions/Smart Action REST Payload Definitions Get all action request payloads (Not Supported)  |
| Service Requests/Smart Actions/Smart Action User Interface Definitions - Get all action navigations (Not Supported) | Service Requests/Smart Actions/Smart Action User Interface Definitions Get all action navigations (Not Supported)  |
| Service Requests/Tags - Get all tags | Service Requests/Tags Get all tags  |
| Setup Assistants - Get all setup tasks | Setup Assistants Get all setup tasks  |
| Setup Assistants/Accounting Calendars - Get all accounting calendars | Setup Assistants/Accounting Calendars Get all accounting calendars  |
| Setup Assistants/Company Profiles - Get all company profiles | Setup Assistants/Company Profiles Get all company profiles  |
| Setup Assistants/Competitors - Get all competitors | Setup Assistants/Competitors Get all competitors  |
| Setup Assistants/Geographies - Get all geographies setup data | Setup Assistants/Geographies Get all geographies setup data  |
| Setup Assistants/Opportunities - Get all opportunities setup data | Setup Assistants/Opportunities Get all opportunities setup data  |
| Setup Assistants/Opportunities/Sales Stages - Get all sales stages of the selected sales method | Setup Assistants/Opportunities/Sales Stages Get all sales stages of the selected sales method  |
| Setup Assistants/Product Groups - Get all product group setup details | Setup Assistants/Product Groups Get all product group setup details  |
| Setup Assistants/Role Mappings - Get all resource roles and role mappings | Setup Assistants/Role Mappings Get all resource roles and role mappings  |
| Setup Assistants/Sales Forecasts - Get all sales forecasting setup details | Setup Assistants/Sales Forecasts Get all sales forecasting setup details  |
| Setup Assistants/Sales Stages - Get all sales stages of the selected sales method | Setup Assistants/Sales Stages Get all sales stages of the selected sales method  |
| Setup Assistants/Setup Histories - Get all setup history details | Setup Assistants/Setup Histories Get all setup history details  |
| Setup Assistants/Setup Progress - Get all setup task details | Setup Assistants/Setup Progress Get all setup task details  |
| Setup Assistants/Setup Users - Get all setup user details | Setup Assistants/Setup Users Get all setup user details  |
| Setup Assistants/Top Sales Users - Get all top sales user details | Setup Assistants/Top Sales Users Get all top sales user details  |
| Skill Resources - Get all skill resources | Skill Resources Get all skill resources  |
| Skills - Get all skills | Skills Get all skills  |
| Skills/Competencies - Get all competencies | Skills/Competencies Get all competencies  |
| Skills/Competencies/Competency Values - Get all values | Skills/Competencies/Competency Values Get all values  |
| Smart Text Folders - Get all smart text folders | Smart Text Folders Get all smart text folders  |
| Smart Text Folders/Smart Text Child Folders - Get all smart text child folders | Smart Text Folders/Smart Text Child Folders Get all smart text child folders  |
| Smart Text User Variables - Get all smart text user variables | Smart Text User Variables Get all smart text user variables  |
| Smart Texts - Get a smart text | Smart Texts Get a smart text  |
| Social Posts - Get all social posts | Social Posts Get all social posts  |
| Social Posts/Social Post Tags - Get all social post tags | Social Posts/Social Post Tags Get all social post tags  |
| Social Posts/Social Post URLs - Get all social post URLs | Social Posts/Social Post URLs Get all social post URLs  |
| Social Users - Get all social users | Social Users Get all social users  |
| Source System References - Get all source system references | Source System References Get all source system references  |
| Standard Coverages - Get all standard coverages | Standard Coverages Get all standard coverages  |
| Standard Coverages/Adjustments - Get all adjustments | Standard Coverages/Adjustments Get all adjustments  |
| Start Period Lookups - Get all start periods | Start Period Lookups Get all start periods  |
| Subscription Accounts - Get all subscription accounts | Subscription Accounts Get all subscription accounts  |
| Subscription Accounts Roles - Get all subscription account roles | Subscription Accounts Roles Get all subscription account roles  |
| Subscription Accounts/Attachments - Get all attachments | Subscription Accounts/Attachments Get all attachments  |
| Subscription Accounts/Billing Profiles - Get all billing profiles | Subscription Accounts/Billing Profiles Get all billing profiles  |
| Subscription Accounts/Notes - Get all notes | Subscription Accounts/Notes Get all notes  |
| Subscription Accounts/Subscription Account Addresses - Get all addresses | Subscription Accounts/Subscription Account Addresses Get all addresses  |
| Subscription Accounts/Subscription Account Relationships - Get all subscription account relationships | Subscription Accounts/Subscription Account Relationships Get all subscription account relationships  |
| Subscription Accounts/Subscription Account Roles - Get all subscription account roles | Subscription Accounts/Subscription Account Roles Get all subscription account roles  |
| Subscription AI Features - Get all product churn features | Subscription AI Features Get all product churn features  |
| Subscription Asset Transactions - Get all subscription asset transactions | Subscription Asset Transactions Get all subscription asset transactions  |
| Subscription Asset Transactions/Asset Fulfillment Lines - Get all fulfillment lines | Subscription Asset Transactions/Asset Fulfillment Lines Get all fulfillment lines  |
| Subscription Asset Transactions/Asset Split Lines - Get all split lines | Subscription Asset Transactions/Asset Split Lines Get all split lines  |
| Subscription Coverage Exceptions - Get all subscription availability exceptions | Subscription Coverage Exceptions Get all subscription availability exceptions  |
| Subscription Coverage Exceptions/Subscription Coverage Availabilities - Get all subscription availability schedules | Subscription Coverage Exceptions/Subscription Coverage Availabilities Get all subscription availability schedules  |
| Subscription Coverage Exceptions/Subscription Coverage Availabilities/Subscription Coverage Breaks - Get all subscription availability breaks | Subscription Coverage Exceptions/Subscription Coverage Availabilities/Subscription Coverage Breaks Get all subscription availability breaks  |
| Subscription Coverage Schedules - Get all subscription coverage availability schedul... | Subscription Coverage Schedules Get all subscription coverage availability schedules  |
| Subscription Coverage Schedules/Subscription Coverage Intervals - Get all subscription coverage availability schedul... | Subscription Coverage Schedules/Subscription Coverage Intervals Get all subscription coverage availability schedule intervals  |
| Subscription Coverage Schedules/Subscription Coverage Intervals/Subscription Coverage Availabilities - Get all subscription coverage availability details | Subscription Coverage Schedules/Subscription Coverage Intervals/Subscription Coverage Availabilities Get all subscription coverage availability details  |
| Subscription Coverage Schedules/Subscription Coverage Intervals/Subscription Coverage Availabilities/Subscription Coverage Breaks - Get all subscription coverage availability breaks | Subscription Coverage Schedules/Subscription Coverage Intervals/Subscription Coverage Availabilities/Subscription Coverage Breaks Get all subscription coverage availability breaks  |
| Subscription Customer Assets List of Values - Get all customer assets | Subscription Customer Assets List of Values Get all customer assets  |
| Subscription Order Transactions - Get all subscription order transactions | Subscription Order Transactions Get all subscription order transactions  |
| Subscription Products - Get all subscription products | Subscription Products Get all subscription products  |
| Subscription Products/Bill Lines - Get all bill lines | Subscription Products/Bill Lines Get all bill lines  |
| Subscription Products/Bill Lines/Bill Adjustments - Get all bill adjustments | Subscription Products/Bill Lines/Bill Adjustments Get all bill adjustments  |
| Subscription Products/Charges - Get all charges | Subscription Products/Charges Get all charges  |
| Subscription Products/Charges/Adjustments - Get all adjustments | Subscription Products/Charges/Adjustments Get all adjustments  |
| Subscription Products/Charges/Charge Tiers - Get all charge tiers | Subscription Products/Charges/Charge Tiers Get all charge tiers  |
| Subscription Products/Covered Levels - Get all covered levels | Subscription Products/Covered Levels Get all covered levels  |
| Subscription Products/Covered Levels/Bill Lines - Get all bill lines | Subscription Products/Covered Levels/Bill Lines Get all bill lines  |
| Subscription Products/Covered Levels/Bill Lines/Bill Adjustments - Get all bill adjustments | Subscription Products/Covered Levels/Bill Lines/Bill Adjustments Get all bill adjustments  |
| Subscription Products/Covered Levels/Charges - Get all charges | Subscription Products/Covered Levels/Charges Get all charges  |
| Subscription Products/Covered Levels/Charges/Adjustments - Get all adjustments | Subscription Products/Covered Levels/Charges/Adjustments Get all adjustments  |
| Subscription Products/Covered Levels/Charges/Charge Tiers - Get all charge tiers | Subscription Products/Covered Levels/Charges/Charge Tiers Get all charge tiers  |
| Subscription Products/Covered Levels/Relationships - Get all relationships | Subscription Products/Covered Levels/Relationships Get all relationships  |
| Subscription Products/Credit Cards - Get all credit cards | Subscription Products/Credit Cards Get all credit cards  |
| Subscription Products/Descriptive Flexfields - Get all descriptive flexfields | Subscription Products/Descriptive Flexfields Get all descriptive flexfields  |
| Subscription Products/Relationships - Get all relationships | Subscription Products/Relationships Get all relationships  |
| Subscription Products/Sales Credits - Get all sales credits | Subscription Products/Sales Credits Get all sales credits  |
| Subscription Profiles - Get all subscription profiles | Subscription Profiles Get all subscription profiles  |
| Subscriptions - Get all subscriptions | Subscriptions Get all subscriptions  |
| Subscriptions/Credit Cards - Get all credit cards | Subscriptions/Credit Cards Get all credit cards  |
| Subscriptions/Descriptive Flexfields - Get all descriptive flexfields | Subscriptions/Descriptive Flexfields Get all descriptive flexfields  |
| Subscriptions/Parties - Get all subscription parties | Subscriptions/Parties Get all subscription parties  |
| Subscriptions/Parties/Contacts - Get all subscription contacts | Subscriptions/Parties/Contacts Get all subscription contacts  |
| Subscriptions/Products - Get all subscription products | Subscriptions/Products Get all subscription products  |
| Subscriptions/Products/Bill Lines - Get all subscription bill lines | Subscriptions/Products/Bill Lines Get all subscription bill lines  |
| Subscriptions/Products/Bill Lines/Bill Adjustments - Get all subscription bill adjustments | Subscriptions/Products/Bill Lines/Bill Adjustments Get all subscription bill adjustments  |
| Subscriptions/Products/Charges - Get all subscription charges | Subscriptions/Products/Charges Get all subscription charges  |
| Subscriptions/Products/Charges/Adjustments - Get all subscription adjustments | Subscriptions/Products/Charges/Adjustments Get all subscription adjustments  |
| Subscriptions/Products/Charges/Charge Tiers - Get all subscription charge tiers | Subscriptions/Products/Charges/Charge Tiers Get all subscription charge tiers  |
| Subscriptions/Products/Covered Levels - Get all covered levels | Subscriptions/Products/Covered Levels Get all covered levels  |
| Subscriptions/Products/Covered Levels/Bill Lines - Get all subscription bill lines | Subscriptions/Products/Covered Levels/Bill Lines Get all subscription bill lines  |
| Subscriptions/Products/Covered Levels/Bill Lines/Bill Adjustments - Get all subscription bill adjustments | Subscriptions/Products/Covered Levels/Bill Lines/Bill Adjustments Get all subscription bill adjustments  |
| Subscriptions/Products/Covered Levels/Charges - Get all subscription charges | Subscriptions/Products/Covered Levels/Charges Get all subscription charges  |
| Subscriptions/Products/Covered Levels/Charges/Adjustments - Get all subscription adjustments | Subscriptions/Products/Covered Levels/Charges/Adjustments Get all subscription adjustments  |
| Subscriptions/Products/Covered Levels/Charges/Charge Tiers - Get all subscription charge tiers | Subscriptions/Products/Covered Levels/Charges/Charge Tiers Get all subscription charge tiers  |
| Subscriptions/Products/Covered Levels/Child Covered Levels - Get all child covered levels | Subscriptions/Products/Covered Levels/Child Covered Levels Get all child covered levels  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines - Get all subscription bill lines | Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines Get all subscription bill lines  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines/Bill Adjustments - Get all subscription bill adjustments | Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines/Bill Adjustments Get all subscription bill adjustments  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges - Get all subscription charges | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges Get all subscription charges  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Adjustments - Get all subscription adjustments | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Adjustments Get all subscription adjustments  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Charge Tiers - Get all subscription charge tiers | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Charge Tiers Get all subscription charge tiers  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Relationships - Get all subscription relationships | Subscriptions/Products/Covered Levels/Child Covered Levels/Relationships Get all subscription relationships  |
| Subscriptions/Products/Covered Levels/Relationships - Get all subscription relationships | Subscriptions/Products/Covered Levels/Relationships Get all subscription relationships  |
| Subscriptions/Products/Credit Cards - Get all credit cards | Subscriptions/Products/Credit Cards Get all credit cards  |
| Subscriptions/Products/Descriptive Flexfields - Get all descriptive flexfields | Subscriptions/Products/Descriptive Flexfields Get all descriptive flexfields  |
| Subscriptions/Products/Relationships - Get all subscription relationships | Subscriptions/Products/Relationships Get all subscription relationships  |
| Subscriptions/Products/Sales Credits - Get all sales credits | Subscriptions/Products/Sales Credits Get all sales credits  |
| Subscriptions/Sales Credits - Get all sales credits | Subscriptions/Sales Credits Get all sales credits  |
| Subscriptions/Validate Subscriptions - Get all subscription validations | Subscriptions/Validate Subscriptions Get all subscription validations  |
| Survey Configurations - Get all survey configurations | Survey Configurations Get all survey configurations  |
| Survey Configurations/Survey Configuration Attributes - Get all survey configuration attributes | Survey Configurations/Survey Configuration Attributes Get all survey configuration attributes  |
| Surveys - Get all surveys | Surveys Get all surveys  |
| Surveys/Survey Questions - Get all survey questions | Surveys/Survey Questions Get all survey questions  |
| Surveys/Survey Questions/Survey Answer Choices - Get all survey answer choices | Surveys/Survey Questions/Survey Answer Choices Get all survey answer choices  |
| Surveys/Survey Requests - Get all survey requests | Surveys/Survey Requests Get all survey requests  |
| Surveys/Survey Requests/Survey Responses - Get all survey responses | Surveys/Survey Requests/Survey Responses Get all survey responses  |
| Territories - Get all territories | Territories Get all territories  |
| Territories for Sales - Get all territories | Territories for Sales Get all territories  |
| Territories for Sales/Territory Business - Get all territory line of business | Territories for Sales/Territory Business Get all territory line of business  |
| Territories for Sales/Territory Coverages - Get all territory coverages | Territories for Sales/Territory Coverages Get all territory coverages  |
| Territories for Sales/Territory Resources - Get all territory resources | Territories for Sales/Territory Resources Get all territory resources  |
| Time Code Units - Get all time code unit conversions | Time Code Units Get all time code unit conversions  |
| Timespent - Retrieve the top flows by duration | Application Usage Insights Retrieve the top flows by the time spent within the flows Retrieve the top flows by duration  |
| Universal Work Objects - Get all universal work objects | Universal Work Objects Get all universal work objects  |
| User Context Data Sources - Get all context data sources | User Context Data Sources Get all context data sources  |
| User Context Object Types - Get all object types | User Context Object Types Get all object types  |
| User Context Object Types/Object Configurations - Get all object configurations | User Context Object Types/Object Configurations Get all object configurations  |
| User Context Object Types/Object Configurations/Object Configuration Details - Get all object configuration details | User Context Object Types/Object Configurations/Object Configuration Details Get all object configuration details  |
| User Context Object Types/Object Criteria - Get all object criteria | User Context Object Types/Object Criteria Get all object criteria  |
| User Context Object Types/Related Objects - Get all related object mappings | User Context Object Types/Related Objects Get all related object mappings  |
| User Relevant Items - Get all user relevant items | User Relevant Items Get all user relevant items  |
| User Relevant Items/User Relevant Item Details - Get all user relevant item details | User Relevant Items/User Relevant Item Details Get all user relevant item details  |
| Userloginmetrics - Retrieve login count | Application Usage Insights Retrieve the login count by day for all users Retrieve login count  |
| Usersessionmetrics - Retrieve the session duration | Application Usage Insights Retrieve the total session duration for all users Retrieve the session duration  |
| Work Orders - Get all work orders | Work Orders Get all work orders  |
| Work Orders/Attachments - Get all attachments | Work Orders/Attachments Get all attachments  |
| Wrap Ups - Get all wrap ups | Wrap Ups Get all wrap ups  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Access Groups - Update an access group (PATCH) | Access Groups Update an access group  |
| Accounts - Update an account (PATCH) | Accounts Update an account  |
| Accounts/Account Attachments - Replace an attachment (PATCH) | Accounts/Account Attachments Replace an attachment  |
| Accounts/Account Extension Bases - Update an account extension base (PATCH) | Accounts/Account Extension Bases Update an account extension base  |
| Accounts/Account Resources - Update a sales team member (PATCH) | Accounts/Account Resources Update a sales team member  |
| Accounts/Additional Identifier - Update an additional identifier (PATCH) | Accounts/Additional Identifier Update an additional identifier  |
| Accounts/Additional Names - Update an additional name (PATCH) | Accounts/Additional Names Update an additional name  |
| Accounts/Addresses - Update an address (PATCH) | Accounts/Addresses Update an address  |
| Accounts/Addresses/Address Locales - Update an address locale (PATCH) | Accounts/Addresses/Address Locales Update an address locale  |
| Accounts/Addresses/Address Purposes - Update an address purpose (PATCH) | Accounts/Addresses/Address Purposes Update an address purpose  |
| Accounts/Aux Classifications - Update a customer classification (PATCH) | Accounts/Aux Classifications Update a customer classification  |
| Accounts/Contact Points - Update a contact point (PATCH) | Accounts/Contact Points Update a contact point  |
| Accounts/Notes - Update a note (PATCH) | Accounts/Notes Update a note  |
| Accounts/Organization Contacts - Update an account contact (PATCH) | Accounts/Organization Contacts Update an account contact  |
| Accounts/Primary Addresses - Update a primary address (PATCH) | Accounts/Primary Addresses Update a primary address  |
| Accounts/Relationships - Update a relationship (PATCH) | Accounts/Relationships Update a relationship  |
| Accounts/Source System References - Update a source system reference (PATCH) | Accounts/Source System References Update a source system reference  |
| Action Events - Update an action event (PATCH) | Action Events Update an action event  |
| Action Plans - Update an Action Plan (PATCH) | Action Plans Update an Action Plan  |
| Action Plans/Action Plan Actions - Update an Action Plan Action (PATCH) | Action Plans/Action Plan Actions Update an Action Plan Action  |
| Action Plans/Action Plan Actions/Action Plan Action Relations - Update an action plan action relation (PATCH) | Action Plans/Action Plan Actions/Action Plan Action Relations Update an action plan action relation  |
| Action Templates - Update an Action Plan Template (PATCH) | Action Templates Update an Action Plan Template  |
| Action Templates/Template Actions - Update a Template Action (PATCH) | Action Templates/Template Actions Update a Template Action  |
| Action Templates/Template Actions/Action Relations - Update an Action Relation (PATCH) | Action Templates/Template Actions/Action Relations Update an Action Relation  |
| Actions - Update an Action (PATCH) | Actions Update an Action  |
| Actions/Action Attributes - Update an Action Attribute (PATCH) | Actions/Action Attributes Update an Action Attribute  |
| Actions/Action Conditions - Update an Action Condition (PATCH) | Actions/Action Conditions Update an Action Condition  |
| Activities - Update an activity (PATCH) | Activities Update an activity  |
| Activities/Activity Assignees - Update an activity assignee (PATCH) | Activities/Activity Assignees Update an activity assignee  |
| Activities/Activity Attachments - Update an activity attachment (PATCH) | Activities/Activity Attachments Update an activity attachment  |
| Activities/Activity Contacts - Update an activity contact (PATCH) | Activities/Activity Contacts Update an activity contact  |
| Activities/Activity Objectives - Update an activity objective (PATCH) | Activities/Activity Objectives Update an activity objective  |
| Activities/Notes - Update an activity note (PATCH) | Activities/Notes Update an activity note  |
| Assets - Update an asset (PATCH) | Assets Update an asset  |
| Assets/Asset Contacts - Update an asset contact (PATCH) | Assets/Asset Contacts Update an asset contact  |
| Assets/Asset Resources - Update an asset resource (PATCH) | Assets/Asset Resources Update an asset resource  |
| Assets/Attachments - Update an attachment (PATCH) | Assets/Attachments Update an attachment  |
| Attribute Predictions - Update an attribute prediction (PATCH) | Attribute Predictions Update an attribute prediction  |
| Billing Adjustments - Update a billing adjustment (PATCH) | Billing Adjustments Update a billing adjustment  |
| Billing Adjustments/Billing Adjustment Events - Update a billing adjustment event (PATCH) | Billing Adjustments/Billing Adjustment Events Update a billing adjustment event  |
| Budgets - Update a MDF budget (PATCH) | Budgets Update a MDF budget  |
| Budgets/Budget Countries - Update a MDF budget country (PATCH) | Budgets/Budget Countries Update a MDF budget country  |
| Budgets/Claims - Update a MDF claim associated to the budget (PATCH) | Budgets/Claims Update a MDF claim associated to the budget  |
| Budgets/Fund Requests - Update a MDF request associated to the budget (PATCH) | Budgets/Fund Requests Update a MDF request associated to the budget  |
| Budgets/MDF Budget Teams - Update a MDF budget team member (PATCH) | Budgets/MDF Budget Teams Update a MDF budget team member  |
| Budgets/Notes - Update a MDF budget note (PATCH) | Budgets/Notes Update a MDF budget note  |
| Business Plans - Update a business plan (PATCH) | Business Plans Update a business plan  |
| Business Plans/Business Plan Resources - Update a business plan resource (PATCH) | Business Plans/Business Plan Resources Update a business plan resource  |
| Business Plans/Notes - Update a business plan note (PATCH) | Business Plans/Notes Update a business plan note  |
| Business Plans/SWOTs - Update a business plan SWOT (PATCH) | Business Plans/SWOTs Update a business plan SWOT  |
| Calculation Simulations - Update a calculation simulation (PATCH) | Calculation Simulations Update a calculation simulation  |
| Calculation Simulations/Simulation Transactions - Update a simulation transaction (PATCH) | Calculation Simulations/Simulation Transactions Update a simulation transaction  |
| Calculation Simulations/Simulation Transactions/Simulation Credits - Update a simulation credit (PATCH) | Calculation Simulations/Simulation Transactions/Simulation Credits Update a simulation credit  |
| Campaign Members - Update a campaign member (PATCH) | Campaign Members Update a campaign member  |
| Campaigns - Update a campaign (PATCH) | Campaigns Update a campaign  |
| Cases - Assign Case to a Queue (POST) | Cases Assign a case to a queue. You must ensure that assignment rules are configured for the case  before assigning it to a queue. You can view the assignment rules using the task "Manage Service Assignment Task" . You must be a Case Manager to assign a case to a queue. Assign Case to a Queue  |
| Cases - Update a case (PATCH) | Cases Update a case  |
| Cases/Case Contacts - Update a case contact (PATCH) | Cases/Case Contacts Update a case contact  |
| Cases/Case Households - Update a case household (PATCH) | Cases/Case Households Update a case household  |
| Cases/Case Messages - Update a case message (PATCH) | Cases/Case Messages Update a case message  |
| Cases/Case Opportunities - Update an opportunity (PATCH) | Cases/Case Opportunities Update an opportunity  |
| Cases/Case Resources - Update a case resource (PATCH) | Cases/Case Resources Update a case resource  |
| Catalog Product Groups - Update a product group (Not Supported) (PATCH) | Catalog Product Groups Update a product group (Not Supported)  |
| Catalog Product Groups/Attachments - Update an attachment (PATCH) | Catalog Product Groups/Attachments Update an attachment  |
| Catalog Product Groups/Translations - Update a product group translation (Not Supported) (PATCH) | Catalog Product Groups/Translations Update a product group translation (Not Supported)  |
| Catalog Products Items - Update a product item (Not Supported) (PATCH) | Catalog Products Items Update a product item (Not Supported)  |
| Catalog Products Items/Attachments - Update an attachment (PATCH) | Catalog Products Items/Attachments Update an attachment  |
| Catalog Products Items/Item Translation - Update an item translation (Not Supported) (PATCH) | Catalog Products Items/Item Translation Update an item translation (Not Supported)  |
| Categories - Update a category (PATCH) | Categories Update a category  |
| Channels - Update a channel (PATCH) | Channels Update a channel  |
| Channels/Channel Resources - Update a channel resource member (PATCH) | Channels/Channel Resources Update a channel resource member  |
| Channels/Sender Identification Priorities - Update a sender priority sequence (PATCH) | Channels/Sender Identification Priorities Update a sender priority sequence  |
| Chat Interactions - Update a chat interaction (Not supported) (PATCH) | Chat Interactions Update a chat interaction (Not supported)  |
| Chat Interactions/Transcripts - Update a transcript (Not supported) (PATCH) | Chat Interactions/Transcripts Update a transcript (Not supported)  |
| Claims - Update an MDF claim (PATCH) | Claims Update an MDF claim  |
| Claims/Claim Settlements - Update a claim settlement (PATCH) | Claims/Claim Settlements Update a claim settlement  |
| Claims/Claim Team Members - Update a claim team member (PATCH) | Claims/Claim Team Members Update a claim team member  |
| Claims/Notes - Update a note (PATCH) | Claims/Notes Update a note  |
| Collaboration Actions - Update a collaboration action (PATCH) | Collaboration Actions Update a collaboration action  |
| Collaboration Actions/Action Attributes - Update a collaboration action attribute (PATCH) | Collaboration Actions/Action Attributes Update a collaboration action attribute  |
| Compensation Plans - Update a compensation plan (PATCH) | Compensation Plans Update a compensation plan  |
| Compensation Plans/Compensation Plan - Descriptive Flex Fields - Update a descriptive flex field (PATCH) | Compensation Plans/Compensation Plan - Descriptive Flex Fields Update a descriptive flex field  |
| Compensation Plans/Compensation Plan - Plan Components - Update a plan component (PATCH) | Compensation Plans/Compensation Plan - Plan Components Update a plan component  |
| Compensation Plans/Compensation Plan - Roles - Update a role (PATCH) | Compensation Plans/Compensation Plan - Roles Update a role  |
| Competencies - Update a competency (PATCH) | Competencies Update a competency  |
| Competitors - Update a competitor (PATCH) | Competitors Update a competitor  |
| Contacts - Update a contact (PATCH) | Contacts Update a contact  |
| Contacts/Additional Identifiers - Update an additional identifier (PATCH) | Contacts/Additional Identifiers Update an additional identifier  |
| Contacts/Additional Names - Update an additional name (PATCH) | Contacts/Additional Names Update an additional name  |
| Contacts/Attachments - Replace a contact's picture (PATCH) | Contacts/Attachments Replace a contact's picture  |
| Contacts/Aux Classifications - Update a customer classification (PATCH) | Contacts/Aux Classifications Update a customer classification  |
| Contacts/Contact Addresses - Update an address (PATCH) | Contacts/Contact Addresses Update an address  |
| Contacts/Contact Addresses/Address Locales - Update an address locale (PATCH) | Contacts/Contact Addresses/Address Locales Update an address locale  |
| Contacts/Contact Addresses/Contact Address Purposes - Update an address purpose (PATCH) | Contacts/Contact Addresses/Contact Address Purposes Update an address purpose  |
| Contacts/Contact Attachments - Replace an attachment (PATCH) | Contacts/Contact Attachments Replace an attachment  |
| Contacts/Contact Points - Update a contact point (PATCH) | Contacts/Contact Points Update a contact point  |
| Contacts/Contact Primary Addresses - Update a primary address (PATCH) | Contacts/Contact Primary Addresses Update a primary address  |
| Contacts/Notes - Update a note (PATCH) | Contacts/Notes Update a note  |
| Contacts/Relationships - Update a relationship (PATCH) | Contacts/Relationships Update a relationship  |
| Contacts/Sales Account Resources - Update a sales team member (PATCH) | Contacts/Sales Account Resources Update a sales team member  |
| Contacts/Source System References - Update a source system reference (PATCH) | Contacts/Source System References Update a source system reference  |
| Contests - Update a contest (PATCH) | Contests Update a contest  |
| Contests/Contest History - Update a contest history record (PATCH) | Contests/Contest History Update a contest history record  |
| Contests/Contest Resources - Update a contest participant (PATCH) | Contests/Contest Resources Update a contest participant  |
| Contests/Contest Scores - Update a contest score (PATCH) | Contests/Contest Scores Update a contest score  |
| Contracts - Amend Contract (POST) | Contracts Amends a contract. This can be done only for an active contract. Amend Contract  |
| Contracts - Cancel Contract (POST) | Contracts Cancels a contract. Cancel Contract  |
| Contracts - Close Contract (POST) | Contracts Closes the specified contract. Close Contract  |
| Contracts - Create New Version (POST) | Contracts Creates a new version for the contract. This can be done only for a draft or under amendment contract. Create New Version  |
| Contracts - Duplicate Contract (POST) | Contracts Duplicate an existing contract. Duplicate Contract  |
| Contracts - Hold Contract (POST) | Contracts Apply hold on a contract. Hold Contract  |
| Contracts - Preview Contracts (POST) | Contracts Generates a preview of the  contract in PDF , HTML, or RTF format. Preview Contracts  |
| Contracts - Remove Contract Hold (POST) | Contracts Removes a hold on a contract. Remove Contract Hold  |
| Contracts - Revert Contract Amendment (POST) | Contracts Reverts a contract amendment. Revert Contract Amendment  |
| Contracts - Sign Contract (POST) | Contracts Signs contract. This can be done for a contract in pending for signature status. Sign Contract  |
| Contracts - Submit Contract (POST) | Contracts Submits a contract for approval. Submit Contract  |
| Contracts - Update a contract (PATCH) | Contracts Update a contract  |
| Contracts - Validate Contract (POST) | Contracts Validates a contract and returns errors and warnings as a string. Validate Contract  |
| Contracts/Bill Plans - Update a bill plan (PATCH) | Contracts/Bill Plans Update a bill plan  |
| Contracts/Bill Plans/Job Assignment Overrides - Update a job assignment override (PATCH) | Contracts/Bill Plans/Job Assignment Overrides Update a job assignment override  |
| Contracts/Bill Plans/Job Rate Overrides - Update a job rate override (PATCH) | Contracts/Bill Plans/Job Rate Overrides Update a job rate override  |
| Contracts/Bill Plans/Job Title Overrides - Update a job title override (PATCH) | Contracts/Bill Plans/Job Title Overrides Update a job title override  |
| Contracts/Bill Plans/Labor Multiplier Overrides - Update a labor multiplier override (PATCH) | Contracts/Bill Plans/Labor Multiplier Overrides Update a labor multiplier override  |
| Contracts/Bill Plans/Non Labor Rate Overrides - Update a non labor rate override (PATCH) | Contracts/Bill Plans/Non Labor Rate Overrides Update a non labor rate override  |
| Contracts/Bill Plans/Person Rate Overrides - Update a person rate override (PATCH) | Contracts/Bill Plans/Person Rate Overrides Update a person rate override  |
| Contracts/Billing Controls - Update a billing control (PATCH) | Contracts/Billing Controls Update a billing control  |
| Contracts/Contract Documents - Update a contract document (PATCH) | Contracts/Contract Documents Update a contract document  |
| Contracts/Contract Header Flexfields - Update a contract header flexfield (PATCH) | Contracts/Contract Header Flexfields Update a contract header flexfield  |
| Contracts/Contract Header Translations - Update contract header translation (PATCH) | Contracts/Contract Header Translations Update contract header translation  |
| Contracts/Contract Lines - Cancel Contract Line (POST) | Contracts/Contract Lines Cancels a contract line. This can be done only for a draft contract line. Cancel Contract Line  |
| Contracts/Contract Lines - Close Contract (POST) | Contracts/Contract Lines Closes the specified contract. Close Contract  |
| Contracts/Contract Lines - Hold Contract Line (POST) | Contracts/Contract Lines Applies hold on a contract line. Hold Contract Line  |
| Contracts/Contract Lines - Remove Contract Line Hold (POST) | Contracts/Contract Lines Removes the hold on a contract line. Remove Contract Line Hold  |
| Contracts/Contract Lines - Update a contract line (PATCH) | Contracts/Contract Lines Update a contract line  |
| Contracts/Contract Lines/Associated Projects - Update an associated project (PATCH) | Contracts/Contract Lines/Associated Projects Update an associated project  |
| Contracts/Contract Lines/Billing Controls - Update a billing control (PATCH) | Contracts/Contract Lines/Billing Controls Update a billing control  |
| Contracts/Contract Lines/Contract Line Flexfields - Update a contract line flexfield (PATCH) | Contracts/Contract Lines/Contract Line Flexfields Update a contract line flexfield  |
| Contracts/Contract Lines/Contract Line Translations - Update a contract line translation (PATCH) | Contracts/Contract Lines/Contract Line Translations Update a contract line translation  |
| Contracts/Contract Lines/Sales Credits - Update a sales credit (PATCH) | Contracts/Contract Lines/Sales Credits Update a sales credit  |
| Contracts/Contract Parties - Update a contract party (PATCH) | Contracts/Contract Parties Update a contract party  |
| Contracts/Contract Parties/Contract Party Contacts - Update a contract party contact (PATCH) | Contracts/Contract Parties/Contract Party Contacts Update a contract party contact  |
| Contracts/Contract Parties/Contract Party Contacts/Contract Party Contact Flexfields - Update a contract party contact flexfield (PATCH) | Contracts/Contract Parties/Contract Party Contacts/Contract Party Contact Flexfields Update a contract party contact flexfield  |
| Contracts/Contract Parties/Contract Party Flexfields - Update a contract party flexfield (PATCH) | Contracts/Contract Parties/Contract Party Flexfields Update a contract party flexfield  |
| Contracts/Revenue Plans - Update a revenue plan (PATCH) | Contracts/Revenue Plans Update a revenue plan  |
| Contracts/Revenue Plans/Job Assignment Overrides - Update a job assignment override (PATCH) | Contracts/Revenue Plans/Job Assignment Overrides Update a job assignment override  |
| Contracts/Revenue Plans/Job Rate Overrides - Update a job rate override (PATCH) | Contracts/Revenue Plans/Job Rate Overrides Update a job rate override  |
| Contracts/Revenue Plans/Labor Multiplier Overrides - Update a labor multiplier override (PATCH) | Contracts/Revenue Plans/Labor Multiplier Overrides Update a labor multiplier override  |
| Contracts/Revenue Plans/Non Labor Rate Overrides - Update a non labor rate override (PATCH) | Contracts/Revenue Plans/Non Labor Rate Overrides Update a non labor rate override  |
| Contracts/Revenue Plans/Person Rate Overrides - Update a person rate override (PATCH) | Contracts/Revenue Plans/Person Rate Overrides Update a person rate override  |
| Contracts/Sales Credits - Update a sales credit (PATCH) | Contracts/Sales Credits Update a sales credit  |
| Contracts/Supporting Documents - Update a supporting document (PATCH) | Contracts/Supporting Documents Update a supporting document  |
| Conversation Messages - Update a conversation message (PATCH) | Conversation Messages Update a conversation message  |
| Conversation Messages/Attachments - Update an attachment (PATCH) | Conversation Messages/Attachments Update an attachment  |
| Conversations - Update a conversation (PATCH) | Conversations Update a conversation  |
| Conversations/Conversation References - Update a conversation reference (PATCH) | Conversations/Conversation References Update a conversation reference  |
| Credit Categories - Update a credit category (PATCH) | Credit Categories Update a credit category  |
| DaaS Smart Data - Update smart data (PATCH) | DaaS Smart Data Update smart data  |
| Deal Registrations - Update a deal registration (PATCH) | Deal Registrations Update a deal registration  |
| Deal Registrations/Deal Products - Update a deal product (PATCH) | Deal Registrations/Deal Products Update a deal product  |
| Deal Registrations/Deal Team Members - Update a deal team member (PATCH) | Deal Registrations/Deal Team Members Update a deal team member  |
| Deal Registrations/Notes - Update a note (PATCH) | Deal Registrations/Notes Update a note  |
| Device Tokens - Update a device token (PATCH) | Device Tokens Update a device token  |
| Dynamic Link Patterns - Update a dynamic link pattern (PATCH) | Dynamic Link Patterns Update a dynamic link pattern  |
| Email Verifications - Patch an email verification (PATCH) | Email Verifications Patch an email verification  |
| Export Activities - Update a bulk export activity (PATCH) | Export Activities Update a bulk export activity  |
| Export Activities/Export Child Object Activities - Update a bulk export activity child object detail (PATCH) | Export Activities/Export Child Object Activities Update a bulk export activity child object detail  |
| Export Activities/Export Child Object Activities/Export Child Object Activities - Update a bulk export activity child object detail (PATCH) | Export Activities/Export Child Object Activities/Export Child Object Activities Update a bulk export activity child object detail  |
| Forecasts - Update a territory forecast (PATCH) | Forecasts Update a territory forecast  |
| Forecasts/Adjustment Periods - Update a period adjustment for a forecast (PATCH) | Forecasts/Adjustment Periods Update a period adjustment for a forecast  |
| Forecasts/Forecast Items - Update a forecast item (PATCH) | Forecasts/Forecast Items Update a forecast item  |
| Forecasts/Forecast Products - Update a forecast product (PATCH) | Forecasts/Forecast Products Update a forecast product  |
| Forecasts/Forecast Products/Product Adjustment Periods - Update a period adjustment for a forecast product (PATCH) | Forecasts/Forecast Products/Product Adjustment Periods Update a period adjustment for a forecast product  |
| Goals - Update a goal (PATCH) | Goals Update a goal  |
| Goals/Goal History - Update a goal history record (PATCH) | Goals/Goal History Update a goal history record  |
| Goals/Goal Metric - Get a Goal Metric (PATCH) | Goals/Goal Metric Get a Goal Metric  |
| Goals/Goal Metric/Goal Metric Breakdowns - Get a goal metric detail record (PATCH) | Goals/Goal Metric/Goal Metric Breakdowns Get a goal metric detail record  |
| Goals/Goal Metric/Goal Metric Detail - Update a goal metric detail record (PATCH) | Goals/Goal Metric/Goal Metric Detail Update a goal metric detail record  |
| Goals/Goal Participants - Update a goal participant (PATCH) | Goals/Goal Participants Update a goal participant  |
| HCM Service Requests - Update a service request (PATCH) | HCM Service Requests Update a service request  |
| HCM Service Requests/Attachments - Update an attachment (PATCH) | HCM Service Requests/Attachments Update an attachment  |
| HCM Service Requests/Channel Communications - Update a channel communication (PATCH) | HCM Service Requests/Channel Communications Update a channel communication  |
| HCM Service Requests/Contact Members - Update a contact member (PATCH) | HCM Service Requests/Contact Members Update a contact member  |
| HCM Service Requests/Messages - Update a message (PATCH) | HCM Service Requests/Messages Update a message  |
| HCM Service Requests/Messages/Attachments - Update an attachment (PATCH) | HCM Service Requests/Messages/Attachments Update an attachment  |
| HCM Service Requests/Messages/Message Channels - Update a channel communication message (PATCH) | HCM Service Requests/Messages/Message Channels Update a channel communication message  |
| HCM Service Requests/Milestones - Update a service request milestone (PATCH) | HCM Service Requests/Milestones Update a service request milestone  |
| HCM Service Requests/Resources - Update a resource member (PATCH) | HCM Service Requests/Resources Update a resource member  |
| HCM Service Requests/Service Request References - Update a service request reference (PATCH) | HCM Service Requests/Service Request References Update a service request reference  |
| Households - Update a household (PATCH) | Households Update a household  |
| Households/Additional Identifiers - Update an additional identifier (PATCH) | Households/Additional Identifiers Update an additional identifier  |
| Households/Additional Names - Update an additional name (PATCH) | Households/Additional Names Update an additional name  |
| Households/Addresses - Update an address (PATCH) | Households/Addresses Update an address  |
| Households/Addresses/Address Locales - Update an address locale (PATCH) | Households/Addresses/Address Locales Update an address locale  |
| Households/Addresses/Address Purposes - Update an address purpose (PATCH) | Households/Addresses/Address Purposes Update an address purpose  |
| Households/Attachments - Replace an attachment (PATCH) | Households/Attachments Replace an attachment  |
| Households/Aux Classifications - Update a customer classification (PATCH) | Households/Aux Classifications Update a customer classification  |
| Households/Contact Points - Update a contact point (PATCH) | Households/Contact Points Update a contact point  |
| Households/Notes - Update a note (PATCH) | Households/Notes Update a note  |
| Households/Primary Addresses - Update a primary address (PATCH) | Households/Primary Addresses Update a primary address  |
| Households/Relationships - Update a relationship (PATCH) | Households/Relationships Update a relationship  |
| Households/Sales Account Resources - Update a sales team member (PATCH) | Households/Sales Account Resources Update a sales team member  |
| Households/Source System References - Update a source system reference (PATCH) | Households/Source System References Update a source system reference  |
| Hub Organizations - Update an organization (PATCH) | Hub Organizations Update an organization  |
| Hub Organizations/Additional Identifiers - Update an additional identifier (PATCH) | Hub Organizations/Additional Identifiers Update an additional identifier  |
| Hub Organizations/Additional Names - Update an additional name (PATCH) | Hub Organizations/Additional Names Update an additional name  |
| Hub Organizations/Addresses - Update an address (PATCH) | Hub Organizations/Addresses Update an address  |
| Hub Organizations/Contact Points - Update a contact point (PATCH) | Hub Organizations/Contact Points Update a contact point  |
| Hub Organizations/Customer Classifications - Update a customer classification (PATCH) | Hub Organizations/Customer Classifications Update a customer classification  |
| Hub Organizations/Notes - Update a note (PATCH) | Hub Organizations/Notes Update a note  |
| Hub Organizations/Organization Attachments - Update an attachment (PATCH) | Hub Organizations/Organization Attachments Update an attachment  |
| Hub Organizations/Party Usage Assignments - Update a usage assignment (PATCH) | Hub Organizations/Party Usage Assignments Update a usage assignment  |
| Hub Organizations/Relationships - Update a relationship (PATCH) | Hub Organizations/Relationships Update a relationship  |
| Hub Organizations/Source System References - Update a source system reference (PATCH) | Hub Organizations/Source System References Update a source system reference  |
| Hub Persons - Update a person (PATCH) | Hub Persons Update a person  |
| Hub Persons/Additional Identifiers - Update an additional identifier (PATCH) | Hub Persons/Additional Identifiers Update an additional identifier  |
| Hub Persons/Additional Names - Update an additional name (PATCH) | Hub Persons/Additional Names Update an additional name  |
| Hub Persons/Addresses - Update an address (PATCH) | Hub Persons/Addresses Update an address  |
| Hub Persons/Attachments - Update an attachment (PATCH) | Hub Persons/Attachments Update an attachment  |
| Hub Persons/Contact Points - Update a contact point (PATCH) | Hub Persons/Contact Points Update a contact point  |
| Hub Persons/Customer Classifications - Update a customer classification (PATCH) | Hub Persons/Customer Classifications Update a customer classification  |
| Hub Persons/Notes - Update a note (PATCH) | Hub Persons/Notes Update a note  |
| Hub Persons/Relationships - Update a relationship (PATCH) | Hub Persons/Relationships Update a relationship  |
| Hub Persons/Source System References - Update a source system reference (PATCH) | Hub Persons/Source System References Update a source system reference  |
| Hub Persons/Usage Assignments - Update a usage assignment (PATCH) | Hub Persons/Usage Assignments Update a usage assignment  |
| Import Activities - Update a file import activity (PATCH) | Import Activities Update a file import activity  |
| Import Activity Maps - Update a file import map (PATCH) | Import Activity Maps Update a file import map  |
| Import Activity Maps/Import Activity Map Columns - Update a file import map column (PATCH) | Import Activity Maps/Import Activity Map Columns Update a file import map column  |
| Inbound Message Filters - Update an inbound message filter (PATCH) | Inbound Message Filters Update an inbound message filter  |
| Inbound Messages - Update an inbound message (PATCH) | Inbound Messages Update an inbound message  |
| Inbound Messages/Attachments - Update an attachment (PATCH) | Inbound Messages/Attachments Update an attachment  |
| Inbound Messages/Inbound Message Parts - Update an inbound message part (PATCH) | Inbound Messages/Inbound Message Parts Update an inbound message part  |
| Incentive Compensation Expressions - Update an expression (PATCH) | Incentive Compensation Expressions Update an expression  |
| Incentive Compensation Expressions/Expression Details - Update an expression detail (PATCH) | Incentive Compensation Expressions/Expression Details Update an expression detail  |
| Incentive Compensation Rule Hierarchies - Update a rule hierarchy (PATCH) | Incentive Compensation Rule Hierarchies Update a rule hierarchy  |
| Incentive Compensation Rule Hierarchies/Qualifying Criteria - Update a qualifying criterion (PATCH) | Incentive Compensation Rule Hierarchies/Qualifying Criteria Update a qualifying criterion  |
| Incentive Compensation Rule Hierarchies/Qualifying Criteria/Qualifying Attribute Values - Update a qualifying attribute value (PATCH) | Incentive Compensation Rule Hierarchies/Qualifying Criteria/Qualifying Attribute Values Update a qualifying attribute value  |
| Incentive Compensation Rule Hierarchies/Rule Assignments - Update a rule assignment (PATCH) | Incentive Compensation Rule Hierarchies/Rule Assignments Update a rule assignment  |
| Incentive Compensation Rule Hierarchies/Rules - Update a rule (PATCH) | Incentive Compensation Rule Hierarchies/Rules Update a rule  |
| Incentive Compensation Transactions - Update a transaction (PATCH) | Incentive Compensation Transactions Update a transaction  |
| Incentive Compensation Transactions/Credits - Update a credit (PATCH) | Incentive Compensation Transactions/Credits Update a credit  |
| Incentive Compensation Transactions/Transaction Descriptive Flex Fields - Update a transaction descriptive flex field (PATCH) | Incentive Compensation Transactions/Transaction Descriptive Flex Fields Update a transaction descriptive flex field  |
| Interactions - Update an interaction (PATCH) | Interactions Update an interaction  |
| Interactions/Child Interactions - Update a child interaction (PATCH) | Interactions/Child Interactions Update a child interaction  |
| Interactions/Interaction Participants - Update an interaction participant (PATCH) | Interactions/Interaction Participants Update an interaction participant  |
| Interactions/Interaction References - Update an interaction reference (PATCH) | Interactions/Interaction References Update an interaction reference  |
| Internal Service Requests - Update a service request (PATCH) | Internal Service Requests Update a service request  |
| Internal Service Requests/Attachments - Update an attachment (PATCH) | Internal Service Requests/Attachments Update an attachment  |
| Internal Service Requests/Channel Communications - Update a channel communication (PATCH) | Internal Service Requests/Channel Communications Update a channel communication  |
| Internal Service Requests/Contact Members - Update a contact member (PATCH) | Internal Service Requests/Contact Members Update a contact member  |
| Internal Service Requests/Messages - Update a message (PATCH) | Internal Service Requests/Messages Update a message  |
| Internal Service Requests/Messages/Attachments - Update an attachment (PATCH) | Internal Service Requests/Messages/Attachments Update an attachment  |
| Internal Service Requests/Messages/Message Channels - Update a channel communication message (PATCH) | Internal Service Requests/Messages/Message Channels Update a channel communication message  |
| Internal Service Requests/Milestones - Update a service request milestone (PATCH) | Internal Service Requests/Milestones Update a service request milestone  |
| Internal Service Requests/Resources - Update a resource member (PATCH) | Internal Service Requests/Resources Update a resource member  |
| Internal Service Requests/Service Request References - Update a service request reference (PATCH) | Internal Service Requests/Service Request References Update a service request reference  |
| Lightbox Documents - Update a Lightbox document (PATCH) | Lightbox Documents Update a Lightbox document  |
| Lightbox Documents/Lightbox Document Pages - Update a Lightbox document page (PATCH) | Lightbox Documents/Lightbox Document Pages Update a Lightbox document page  |
| Lightbox Presentation Session Feedback - Update a lightbox presentation session feedback it... (PATCH) | Lightbox Presentation Session Feedback Update a lightbox presentation session feedback item  |
| Lightbox Presentation Sessions - Update a Lightbox presentation session (PATCH) | Lightbox Presentation Sessions Update a Lightbox presentation session  |
| List of Values/Action Status Conditions - Update a status condition (PATCH) | List of Values/Action Status Conditions Update a status condition  |
| List of Values/Collaboration Recipients - Update a collaboration recipient (PATCH) | List of Values/Collaboration Recipients Update a collaboration recipient  |
| List of Values/Dynamic Link Values - Update a reference (PATCH) | List of Values/Dynamic Link Values Update a reference  |
| List of Values/KPI - Update a KPI (PATCH) | List of Values/KPI Update a KPI  |
| List of Values/KPI/KPI History Metadata - Update a KPI history metadata (PATCH) | List of Values/KPI/KPI History Metadata Update a KPI history metadata  |
| List of Values/Tags - Update a tag (PATCH) | List of Values/Tags Update a tag  |
| MDF Requests - Update an MDF request (PATCH) | MDF Requests Update an MDF request  |
| MDF Requests/Claims - Update a claim (PATCH) | MDF Requests/Claims Update a claim  |
| MDF Requests/MDF Request Teams - Update a fund request resource (PATCH) | MDF Requests/MDF Request Teams Update a fund request resource  |
| MDF Requests/Notes - Update a note (PATCH) | MDF Requests/Notes Update a note  |
| Milestones - Update a milestone (PATCH) | Milestones Update a milestone  |
| Multi Channel Adapter Events - Update an event (PATCH) | Multi Channel Adapter Events Update an event  |
| Multi-Channel Adapter Toolbars - Update a toolbar (PATCH) | Multi-Channel Adapter Toolbars Update a toolbar  |
| Multi-Channel Adapter Toolbars/Multi-Channel Adapter Toolbar Additions - Update a toolbar addition (PATCH) | Multi-Channel Adapter Toolbars/Multi-Channel Adapter Toolbar Additions Update a toolbar addition  |
| Non-Duplicate Records - Update a non-duplicate record (PATCH) | Non-Duplicate Records Update a non-duplicate record  |
| Object Capacities - Update an object capacity (PATCH) | Object Capacities Update an object capacity  |
| Objectives - Update an objective (PATCH) | Objectives Update an objective  |
| Objectives/Objective Splits - Update an objective split (PATCH) | Objectives/Objective Splits Update an objective split  |
| Omnichannel Presence - Update an omnichannel presence (PATCH) | Omnichannel Presence Update an omnichannel presence  |
| Omnichannel Properties - Update an omnichannel property (PATCH) | Omnichannel Properties Update an omnichannel property  |
| Opportunities - Update an opportunity (PATCH) | Opportunities Update an opportunity  |
| Opportunities/Assessments - Update an assessment (PATCH) | Opportunities/Assessments Update an assessment  |
| Opportunities/Assessments/Assessment Answer Groups - Update an assessment answer group (PATCH) | Opportunities/Assessments/Assessment Answer Groups Update an assessment answer group  |
| Opportunities/Assessments/Assessment Answer Groups/Assessment Answers - Update an assessment answer (PATCH) | Opportunities/Assessments/Assessment Answer Groups/Assessment Answers Update an assessment answer  |
| Opportunities/Notes - Update a note (PATCH) | Opportunities/Notes Update a note  |
| Opportunities/Opportunity Competitors - Update an opportunity competitor (PATCH) | Opportunities/Opportunity Competitors Update an opportunity competitor  |
| Opportunities/Opportunity Contacts - Update an opportunity contact (PATCH) | Opportunities/Opportunity Contacts Update an opportunity contact  |
| Opportunities/Opportunity Leads - Update an opportunity lead (PATCH) | Opportunities/Opportunity Leads Update an opportunity lead  |
| Opportunities/Opportunity Partners - Update an opportunity revenue partner (PATCH) | Opportunities/Opportunity Partners Update an opportunity revenue partner  |
| Opportunities/Opportunity Sources - Update an opportunity source (PATCH) | Opportunities/Opportunity Sources Update an opportunity source  |
| Opportunities/Opportunity Team Members - Update an opportunity team member (PATCH) | Opportunities/Opportunity Team Members Update an opportunity team member  |
| Opportunities/Revenue Items - Update an opportunity revenue (PATCH) | Opportunities/Revenue Items Update an opportunity revenue  |
| Opportunities/Revenue Items/Child Split Revenues - Update a child split revenue (PATCH) | Opportunities/Revenue Items/Child Split Revenues Update a child split revenue  |
| Opportunities/Revenue Items/Opportunity Revenue Territories - Update an opportunity revenue territory (PATCH) | Opportunities/Revenue Items/Opportunity Revenue Territories Update an opportunity revenue territory  |
| Opportunities/Revenue Items/Recurring Revenues - Update a recurring revenue (PATCH) | Opportunities/Revenue Items/Recurring Revenues Update a recurring revenue  |
| Outbound Messages - Update an outbound message (PATCH) | Outbound Messages Update an outbound message  |
| Outbound Messages/Outbound Message Parts - Update an outbound message part (PATCH) | Outbound Messages/Outbound Message Parts Update an outbound message part  |
| Participant Compensation Plans - Update a participant compensation plan (PATCH) | Participant Compensation Plans Update a participant compensation plan  |
| Participant Compensation Plans/Descriptive Flex Fields - Update descriptive flex fields for a participant p... (PATCH) | Participant Compensation Plans/Descriptive Flex Fields Update descriptive flex fields for a participant plan  |
| Participant Compensation Plans/Plan Components - Update a plan component (PATCH) | Participant Compensation Plans/Plan Components Update a plan component  |
| Participant Compensation Plans/Plan Components/Performance Measures/Goals - Update a performance measure goal (PATCH) | Participant Compensation Plans/Plan Components/Performance Measures/Goals Update a performance measure goal  |
| Participant Compensation Plans/Plan Components/Performance Measures/Goals/Period Goals - Update a period goal (PATCH) | Participant Compensation Plans/Plan Components/Performance Measures/Goals/Period Goals Update a period goal  |
| Participant Compensation Plans/Plan Components/Performance Measures/Scorecards/Scorecard Rates - Update a scorecard rate (PATCH) | Participant Compensation Plans/Plan Components/Performance Measures/Scorecards/Scorecard Rates Update a scorecard rate  |
| Participant Compensation Plans/Plan Components/Rate Tables/Rate Table Rates - Update a rate table rate (PATCH) | Participant Compensation Plans/Plan Components/Rate Tables/Rate Table Rates Update a rate table rate  |
| Participants - Update a participant (PATCH) | Participants Update a participant  |
| Participants/Participant Details - Update a participant detail (PATCH) | Participants/Participant Details Update a participant detail  |
| Participants/Participant Details/Participant Details Descriptive Flex Fields - Update a descriptive flex field (PATCH) | Participants/Participant Details/Participant Details Descriptive Flex Fields Update a descriptive flex field  |
| Participants/Participant Roles - Update a participant role (PATCH) | Participants/Participant Roles Update a participant role  |
| Participants/Participants Descriptive Flex Fields - Update a descriptive flex field (PATCH) | Participants/Participants Descriptive Flex Fields Update a descriptive flex field  |
| Partner Contacts - Update a partner contact (PATCH) | Partner Contacts Update a partner contact  |
| Partner Contacts/Attachments - Update an attachment (PATCH) | Partner Contacts/Attachments Update an attachment  |
| Partner Contacts/Partner Contact User Details - Update an user detail (PATCH) | Partner Contacts/Partner Contact User Details Update an user detail  |
| Partner Programs - Update a partner program (PATCH) | Partner Programs Update a partner program  |
| Partner Programs/Countries - Update a country for a partner program (PATCH) | Partner Programs/Countries Update a country for a partner program  |
| Partner Programs/Program Benefit Details - Update a program benefit for a partner program (PATCH) | Partner Programs/Program Benefit Details Update a program benefit for a partner program  |
| Partner Programs/Tiers - Update a tier for a partner program (PATCH) | Partner Programs/Tiers Update a tier for a partner program  |
| Partner Tiers - Delete a product tier and its hierarchy (POST) | Partner Tiers Delete a product tier and its hierarchy Delete a product tier and its hierarchy  |
| Partner Tiers - Downgrade partner tier ranking (POST) | Partner Tiers Move down the partner tier ranking down by one level. Downgrade partner tier ranking  |
| Partner Tiers - Update a partner tier (PATCH) | Partner Tiers Update a partner tier  |
| Partner Tiers - Upgrade partner tier ranking (POST) | Partner Tiers Move up the partner tier ranking down by one level. Upgrade partner tier ranking  |
| Partners - Update a partner (PATCH) | Partners Update a partner  |
| Partners/Addresses - Update an address (PATCH) | Partners/Addresses Update an address  |
| Partners/Attachments - Update a partner attachment (PATCH) | Partners/Attachments Update a partner attachment  |
| Partners/Notes - Update a note for a partner (PATCH) | Partners/Notes Update a note for a partner  |
| Partners/Partner Account Team Members - Update a partner account team member for a partner (PATCH) | Partners/Partner Account Team Members Update a partner account team member for a partner  |
| Partners/Partner Contacts - Update a partner contact (PATCH) | Partners/Partner Contacts Update a partner contact  |
| Partners/Partner Contacts/Attachments - Update a partner attachment (PATCH) | Partners/Partner Contacts/Attachments Update a partner attachment  |
| Partners/Partner Contacts/User Account Details - Update a partner contact user detail (PATCH) | Partners/Partner Contacts/User Account Details Update a partner contact user detail  |
| Pay Groups - Update a pay group (PATCH) | Pay Groups Update a pay group  |
| Pay Groups/Pay Group Assignments - Update a pay group assignments (PATCH) | Pay Groups/Pay Group Assignments Update a pay group assignments  |
| Pay Groups/Pay Group Roles - Update a pay group role (PATCH) | Pay Groups/Pay Group Roles Update a pay group role  |
| Payment Batches/Payment Transactions - Update a payment transaction (PATCH) | Payment Batches/Payment Transactions Update a payment transaction  |
| Payment Batches/Paysheets - Update a paysheet (PATCH) | Payment Batches/Paysheets Update a paysheet  |
| Payment Batches/Paysheets/Payment Transactions - Update a payment transaction (PATCH) | Payment Batches/Paysheets/Payment Transactions Update a payment transaction  |
| Payment Plans - Update a payment plan (PATCH) | Payment Plans Update a payment plan  |
| Payment Plans/Payment Plans Assignments - Update a payment plan assignment (PATCH) | Payment Plans/Payment Plans Assignments Update a payment plan assignment  |
| Payment Plans/Payment Plans Roles - Update a payment plan role (PATCH) | Payment Plans/Payment Plans Roles Update a payment plan role  |
| Payment Transactions - Update a payment transaction (PATCH) | Payment Transactions Update a payment transaction  |
| Paysheets - Update a paysheet (PATCH) | Paysheets Update a paysheet  |
| Paysheets/Payment Transactions - Update a payment transaction (PATCH) | Paysheets/Payment Transactions Update a payment transaction  |
| Performance Measures - Update a performance measure (PATCH) | Performance Measures Update a performance measure  |
| Performance Measures/Credit Categories/Credit Factors - Update a credit factor (PATCH) | Performance Measures/Credit Categories/Credit Factors Update a credit factor  |
| Performance Measures/Credit Categories/Transaction Factors - Update a transaction factor (PATCH) | Performance Measures/Credit Categories/Transaction Factors Update a transaction factor  |
| Performance Measures/Descriptive Flex fields - Update a descriptive flex field (PATCH) | Performance Measures/Descriptive Flex fields Update a descriptive flex field  |
| Performance Measures/Goals - Update a goal (PATCH) | Performance Measures/Goals Update a goal  |
| Performance Measures/Goals/Interval Goals - Update an interval goal (PATCH) | Performance Measures/Goals/Interval Goals Update an interval goal  |
| Performance Measures/Goals/Interval Goals/Period Goals - Update a period goal (PATCH) | Performance Measures/Goals/Interval Goals/Period Goals Update a period goal  |
| Performance Measures/Rate Dimensional Input Expressions - Update a rate dimensional input (PATCH) | Performance Measures/Rate Dimensional Input Expressions Update a rate dimensional input  |
| Performance Measures/Scorecards - Update a scorecard (PATCH) | Performance Measures/Scorecards Update a scorecard  |
| Phone Verifications - Update a phone verification (PATCH) | Phone Verifications Update a phone verification  |
| Plan Components - Update a plan component (PATCH) | Plan Components Update a plan component  |
| Plan Components/Plan Component - Incentive Formulas - Update an incentive formula (PATCH) | Plan Components/Plan Component - Incentive Formulas Update an incentive formula  |
| Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Dimensional Input Expressions - Update a rate dimensional input (PATCH) | Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Dimensional Input Expressions Update a rate dimensional input  |
| Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Tables - Update a rate table (PATCH) | Plan Components/Plan Component - Incentive Formulas/Plan Component - Rate Tables Update a rate table  |
| Plan Components/Plan Component - Performance Measures - Update a performance measure (PATCH) | Plan Components/Plan Component - Performance Measures Update a performance measure  |
| Plan Components/Plan Component Descriptive Flex Fields - Update a descriptive flex field (PATCH) | Plan Components/Plan Component Descriptive Flex Fields Update a descriptive flex field  |
| Price Book Headers - Update a pricebook (PATCH) | Price Book Headers Update a pricebook  |
| Price Book Headers/Price Book Items - Update a pricebook item (PATCH) | Price Book Headers/Price Book Items Update a pricebook item  |
| Process Metadatas - Update a process metadata (PATCH) | Process Metadatas Update a process metadata  |
| Product Groups - Release lock on a product group (POST) | Product Groups The action releases the lock on a product group resource item. Release lock on a product group  |
| Product Groups - Update a product group (PATCH) | Product Groups Update a product group  |
| Product Groups/Attachments - Update an attachment (PATCH) | Product Groups/Attachments Update an attachment  |
| Product Groups/Filter Attributes - Update an attribute by product group attribute ID (PATCH) | Product Groups/Filter Attributes Update an attribute by product group attribute ID  |
| Product Groups/Filter Attributes/Filter Attribute Values - Update an attribute value (PATCH) | Product Groups/Filter Attributes/Filter Attribute Values Update an attribute value  |
| Product Groups/Products - Update a product relationship on a product group (PATCH) | Product Groups/Products Update a product relationship on a product group  |
| Product Groups/Products/EligibilityRules - Update an eligibility rule (PATCH) | Product Groups/Products/EligibilityRules Update an eligibility rule  |
| Product Groups/Related Groups - Update a subgroup relationship (PATCH) | Product Groups/Related Groups Update a subgroup relationship  |
| Product Groups/Subgroups - Update a subgroup (PATCH) | Product Groups/Subgroups Update a subgroup  |
| Products - Update a product (PATCH) | Products Update a product  |
| Products/Default Prices - Update a default price (PATCH) | Products/Default Prices Update a default price  |
| Products/Product Attachments - Update an attachment for a product (PATCH) | Products/Product Attachments Update an attachment for a product  |
| Products/Product Image Attachments - Update an image attachment for a product (PATCH) | Products/Product Image Attachments Update an image attachment for a product  |
| Program Benefits - Update a program benefit (PATCH) | Program Benefits Update a program benefit  |
| Program Benefits/Benefit List Values - Update a benefit list type value (PATCH) | Program Benefits/Benefit List Values Update a benefit list type value  |
| Program Enrollments - Update an enrollment (PATCH) | Program Enrollments Update an enrollment  |
| Program Enrollments/Notes - Update a note for an enrollment (PATCH) | Program Enrollments/Notes Update a note for an enrollment  |
| Queues - Update a queue (PATCH) | Queues Update a queue  |
| Queues/Overflow Queues - Update an overflow queue (PATCH) | Queues/Overflow Queues Update an overflow queue  |
| Queues/Queue Resource Members - Update a resource member (PATCH) | Queues/Queue Resource Members Update a resource member  |
| Queues/Queue Resource Teams - Update a resource team (PATCH) | Queues/Queue Resource Teams Update a resource team  |
| Quote and Order Lines - Update a sales order line (PATCH) | Quote and Order Lines Update a sales order line  |
| Rate Dimensions - Update a rate dimension (PATCH) | Rate Dimensions Update a rate dimension  |
| Rate Dimensions/Rate Dimension - Tiers - Update a rate dimension tier (PATCH) | Rate Dimensions/Rate Dimension - Tiers Update a rate dimension tier  |
| Rate Tables - Update a rate table (PATCH) | Rate Tables Update a rate table  |
| Rate Tables/Rate Table Rates - Update a rate table rate (PATCH) | Rate Tables/Rate Table Rates Update a rate table rate  |
| Resolution Links - Update a link (PATCH) | Resolution Links Update a link  |
| Resolution Links/Link Members - Update a link member (PATCH) | Resolution Links/Link Members Update a link member  |
| Resource Capacities - Update a resource capacity (PATCH) | Resource Capacities Update a resource capacity  |
| Resource Users - Update a resource user (PATCH) | Resource Users Update a resource user  |
| Roles - Update an incentive compensation role (PATCH) | Roles Update an incentive compensation role  |
| Roles/Role - Participants - Update a participant for a role (PATCH) | Roles/Role - Participants Update a participant for a role  |
| Sales Leads - Update a sales lead (PATCH) | Sales Leads Update a sales lead  |
| Sales Leads/Lead Qualifications - Update a lead qualification (PATCH) | Sales Leads/Lead Qualifications Update a lead qualification  |
| Sales Leads/Lead Qualifications/Assessment Answer Groups/Assessment Answers - Update a lead qualification answer (PATCH) | Sales Leads/Lead Qualifications/Assessment Answer Groups/Assessment Answers Update a lead qualification answer  |
| Sales Leads/Notes - Update a note (PATCH) | Sales Leads/Notes Update a note  |
| Sales Leads/Sales Lead Contacts - Update a sales lead contact (PATCH) | Sales Leads/Sales Lead Contacts Update a sales lead contact  |
| Sales Leads/Sales Lead Products - Update a sales lead product (PATCH) | Sales Leads/Sales Lead Products Update a sales lead product  |
| Sales Leads/Sales Lead Resources - Update a sales lead resource (PATCH) | Sales Leads/Sales Lead Resources Update a sales lead resource  |
| Sales Orders - Update a quote (PATCH) | Sales Orders Update a quote  |
| Sales Orders/Quote and Order Lines - Update a sales order line (PATCH) | Sales Orders/Quote and Order Lines Update a sales order line  |
| Sales Promotions - Update a sales promotion (PATCH) | Sales Promotions Update a sales promotion  |
| Sales Territories - Update a territory (PATCH) | Sales Territories Update a territory  |
| Sales Territories/Line of Business - Update a line of business for a territory (PATCH) | Sales Territories/Line of Business Update a line of business for a territory  |
| Sales Territories/Resources - Update a resource for a territory (PATCH) | Sales Territories/Resources Update a resource for a territory  |
| Sales Territories/Rules - Update a rule for a territory (PATCH) | Sales Territories/Rules Update a rule for a territory  |
| Sales Territories/Rules/Rule Boundaries - Update a rule boundary for a territory (PATCH) | Sales Territories/Rules/Rule Boundaries Update a rule boundary for a territory  |
| Sales Territories/Rules/Rule Boundaries/Rule Boundary Values - Update a rule boundary value for a territory (PATCH) | Sales Territories/Rules/Rule Boundaries/Rule Boundary Values Update a rule boundary value for a territory  |
| Sales Territory Proposals - Update a proposal (PATCH) | Sales Territory Proposals Update a proposal  |
| Self-Service Registrations - Update a self-service registration request (PATCH) | Self-Service Registrations Update a self-service registration request  |
| Self-Service Users/Self-Service Roles - Update a self-service role (PATCH) | Self-Service Users/Self-Service Roles Update a self-service role  |
| Service Providers - Update a service provider (PATCH) | Service Providers Update a service provider  |
| Service Providers/Services - Update a service (PATCH) | Service Providers/Services Update a service  |
| Service Requests - Get Service Request milestone diagnostics (POST) | Service Requests Returns the service request milestone diagnostics report results for the specified service request. Get Service Request milestone diagnostics  |
| Service Requests - Refresh Service Request Milestones (POST) | Service Requests Evaluates service request and applies any required changes to its milestones. Refresh Service Request Milestones  |
| Service Requests - Update a service request (PATCH) | Service Requests Update a service request  |
| Service Requests/Activities - Update an activity (PATCH) | Service Requests/Activities Update an activity  |
| Service Requests/Attachments - Update an attachment (PATCH) | Service Requests/Attachments Update an attachment  |
| Service Requests/Channel Communications - Update a channel communication (PATCH) | Service Requests/Channel Communications Update a channel communication  |
| Service Requests/Contact Members - Update a contact (PATCH) | Service Requests/Contact Members Update a contact  |
| Service Requests/Messages - Update a message (PATCH) | Service Requests/Messages Update a message  |
| Service Requests/Messages/Attachments - Update an attachment (PATCH) | Service Requests/Messages/Attachments Update an attachment  |
| Service Requests/Messages/Channel Communications - Update a channel communication (PATCH) | Service Requests/Messages/Channel Communications Update a channel communication  |
| Service Requests/Milestones - Update a service request milestone (PATCH) | Service Requests/Milestones Update a service request milestone  |
| Service Requests/Resources - Update a resource member (PATCH) | Service Requests/Resources Update a resource member  |
| Service Requests/Service Request References - Update a service request reference (PATCH) | Service Requests/Service Request References Update a service request reference  |
| Setup Assistants - Update a setup tasks (PATCH) | Setup Assistants Update a setup tasks  |
| Setup Assistants/Accounting Calendars - Update an accounting calendar (PATCH) | Setup Assistants/Accounting Calendars Update an accounting calendar  |
| Setup Assistants/Company Profiles - Update a company profile (PATCH) | Setup Assistants/Company Profiles Update a company profile  |
| Setup Assistants/Competitors - Update a competitor (PATCH) | Setup Assistants/Competitors Update a competitor  |
| Setup Assistants/Geographies - Update a geography setup data (PATCH) | Setup Assistants/Geographies Update a geography setup data  |
| Setup Assistants/Opportunities - Update opportunity setup data (PATCH) | Setup Assistants/Opportunities Update opportunity setup data  |
| Setup Assistants/Opportunities/Sales Stages - Update a sales stage of the selected sales method (PATCH) | Setup Assistants/Opportunities/Sales Stages Update a sales stage of the selected sales method  |
| Setup Assistants/Product Groups - Update product group setup details (PATCH) | Setup Assistants/Product Groups Update product group setup details  |
| Setup Assistants/Role Mappings - Update a resource role and role mapping (PATCH) | Setup Assistants/Role Mappings Update a resource role and role mapping  |
| Setup Assistants/Sales Forecasts - Update sales forecasting setup details (PATCH) | Setup Assistants/Sales Forecasts Update sales forecasting setup details  |
| Setup Assistants/Sales Stages - Update a sales stage of the selected sales method (PATCH) | Setup Assistants/Sales Stages Update a sales stage of the selected sales method  |
| Setup Assistants/Setup Users - Update a setup user details (PATCH) | Setup Assistants/Setup Users Update a setup user details  |
| Setup Assistants/Top Sales Users - Update a top sales user details (PATCH) | Setup Assistants/Top Sales Users Update a top sales user details  |
| Skill Resources - Update a skill resource (PATCH) | Skill Resources Update a skill resource  |
| Skills - Update a skill (PATCH) | Skills Update a skill  |
| Skills/Competencies - Update a competency (PATCH) | Skills/Competencies Update a competency  |
| Skills/Competencies/Competency Values - Update a value (PATCH) | Skills/Competencies/Competency Values Update a value  |
| Smart Text Folders - Update a smart text folder (PATCH) | Smart Text Folders Update a smart text folder  |
| Smart Text Folders/Smart Text Child Folders - Update a smart text child folder (PATCH) | Smart Text Folders/Smart Text Child Folders Update a smart text child folder  |
| Smart Text User Variables - Update a smart text user variable (PATCH) | Smart Text User Variables Update a smart text user variable  |
| Smart Texts - Process smart text variable substitution (POST) | Smart Texts This action accepts a jsonString with the primary keys of the smart text and the referenced object. The action then substitutes the variables in the smart text with the data obtained from the referenced object. Alternatively, you can use the PUID instead of the primary key to reference the smart text. Process smart text variable substitution  |
| Smart Texts - Update a smart text (PATCH) | Smart Texts Update a smart text  |
| Social Users - Update a social user (PATCH) | Social Users Update a social user  |
| Source System References - Update a source system reference (PATCH) | Source System References Update a source system reference  |
| Standard Coverages - Update a standard coverage (PATCH) | Standard Coverages Update a standard coverage  |
| Standard Coverages/Adjustments - Update an adjustment (PATCH) | Standard Coverages/Adjustments Update an adjustment  |
| Subscription Accounts - Update a subscription account (PATCH) | Subscription Accounts Update a subscription account  |
| Subscription Accounts Roles - Update a subscription account role (PATCH) | Subscription Accounts Roles Update a subscription account role  |
| Subscription Accounts/Attachments - Update an attachment (PATCH) | Subscription Accounts/Attachments Update an attachment  |
| Subscription Accounts/Billing Profiles - Update a billing profile (PATCH) | Subscription Accounts/Billing Profiles Update a billing profile  |
| Subscription Accounts/Notes - Update a note (PATCH) | Subscription Accounts/Notes Update a note  |
| Subscription Accounts/Subscription Account Addresses - Update an address (PATCH) | Subscription Accounts/Subscription Account Addresses Update an address  |
| Subscription Accounts/Subscription Account Relationships - Update a subscription account relationship (PATCH) | Subscription Accounts/Subscription Account Relationships Update a subscription account relationship  |
| Subscription Accounts/Subscription Account Roles - Update a subscription account role (PATCH) | Subscription Accounts/Subscription Account Roles Update a subscription account role  |
| Subscription AI Features - Update a product churn feature (PATCH) | Subscription AI Features Update a product churn feature  |
| Subscription Asset Transactions - Notify fulfillment completed (POST) | Subscription Asset Transactions This method will be used for notifying the completion of fulfillment in case of customer replacement or upgrade transaction. Notify fulfillment completed  |
| Subscription Asset Transactions - Notify pending fulfillment canceled (POST) | Subscription Asset Transactions This method will be used for notifying the fulfillment is  cancelled in case of customer replacement or upgrade transaction. Notify pending fulfillment canceled  |
| Subscription Asset Transactions - Notify return completed (POST) | Subscription Asset Transactions This method will be used for notifying the completion of return in case of customer replacement or upgrade transaction. Notify return completed  |
| Subscription Asset Transactions - Notify shipment completed (POST) | Subscription Asset Transactions Notifies the completion of shipment in case of customer replacement or upgrade transaction. Notify shipment completed  |
| Subscription Products - Cancel Subscription (POST) | Subscription Products Cancel the subscription product. Cancel Subscription  |
| Subscription Products - Close Subscription (POST) | Subscription Products Close a subscription product. Close Subscription  |
| Subscription Products - Put Hold (POST) | Subscription Products Put a subscription product on hold Put Hold  |
| Subscription Products - Remove Hold (POST) | Subscription Products Remove hold on a subscription product Remove Hold  |
| Subscription Products - Resume Subscription (POST) | Subscription Products Resume a suspended subscription product. Resume Subscription  |
| Subscription Products - Suspend Subscription (POST) | Subscription Products Suspends the subscription product. Suspend Subscription  |
| Subscription Products - Update a subscription product (PATCH) | Subscription Products Update a subscription product  |
| Subscription Products/Bill Lines - Update a bill line (PATCH) | Subscription Products/Bill Lines Update a bill line  |
| Subscription Products/Bill Lines/Bill Adjustments - Update a bill adjustment (PATCH) | Subscription Products/Bill Lines/Bill Adjustments Update a bill adjustment  |
| Subscription Products/Charges - Update a charge (PATCH) | Subscription Products/Charges Update a charge  |
| Subscription Products/Charges/Adjustments - Update an adjustment (PATCH) | Subscription Products/Charges/Adjustments Update an adjustment  |
| Subscription Products/Charges/Charge Tiers - Update a charge tier (PATCH) | Subscription Products/Charges/Charge Tiers Update a charge tier  |
| Subscription Products/Covered Levels - Update a covered level (PATCH) | Subscription Products/Covered Levels Update a covered level  |
| Subscription Products/Covered Levels/Bill Lines - Update a bill line (PATCH) | Subscription Products/Covered Levels/Bill Lines Update a bill line  |
| Subscription Products/Covered Levels/Bill Lines/Bill Adjustments - Update a bill adjustment (PATCH) | Subscription Products/Covered Levels/Bill Lines/Bill Adjustments Update a bill adjustment  |
| Subscription Products/Covered Levels/Charges - Update a charge (PATCH) | Subscription Products/Covered Levels/Charges Update a charge  |
| Subscription Products/Covered Levels/Charges/Adjustments - Update an adjustment (PATCH) | Subscription Products/Covered Levels/Charges/Adjustments Update an adjustment  |
| Subscription Products/Covered Levels/Charges/Charge Tiers - Update a charge tier (PATCH) | Subscription Products/Covered Levels/Charges/Charge Tiers Update a charge tier  |
| Subscription Products/Covered Levels/Relationships - Update a relationship (PATCH) | Subscription Products/Covered Levels/Relationships Update a relationship  |
| Subscription Products/Credit Cards - Update a credit card (PATCH) | Subscription Products/Credit Cards Update a credit card  |
| Subscription Products/Descriptive Flexfields - Update a descriptive flexfield (PATCH) | Subscription Products/Descriptive Flexfields Update a descriptive flexfield  |
| Subscription Products/Relationships - Update a relationship (PATCH) | Subscription Products/Relationships Update a relationship  |
| Subscription Products/Sales Credits - Update a sales credit (PATCH) | Subscription Products/Sales Credits Update a sales credit  |
| Subscriptions - Activate subscription (POST) | Subscriptions Activate a subscription. Activate subscription  |
| Subscriptions - Cancel subscription (POST) | Subscriptions Cancel a subscription. Cancel subscription  |
| Subscriptions - Close subscription (POST) | Subscriptions Close a subscription. Close subscription  |
| Subscriptions - Hold subscription (POST) | Subscriptions Put a subscription on hold. Hold subscription  |
| Subscriptions - Preview subcsription (POST) | Subscriptions Preview a subcsription Preview subcsription  |
| Subscriptions - Remove hold (POST) | Subscriptions Remove the hold on a subscription. Remove hold  |
| Subscriptions - Renew subscription (POST) | Subscriptions Renew a subscription Renew subscription  |
| Subscriptions - Update a subscription (PATCH) | Subscriptions Update a subscription  |
| Subscriptions/Credit Cards - Update a credit card (PATCH) | Subscriptions/Credit Cards Update a credit card  |
| Subscriptions/Descriptive Flexfields - Update a descriptive flexfield (PATCH) | Subscriptions/Descriptive Flexfields Update a descriptive flexfield  |
| Subscriptions/Parties - Update a subscription party (PATCH) | Subscriptions/Parties Update a subscription party  |
| Subscriptions/Parties/Contacts - Update a subscription contact (PATCH) | Subscriptions/Parties/Contacts Update a subscription contact  |
| Subscriptions/Products - Cancel subscription product (POST) | Subscriptions/Products Cancel a subscription product. You must provide a Subscription Product Puid to cancel. Cancel subscription product  |
| Subscriptions/Products - Close subscription product (POST) | Subscriptions/Products Close a subscription product. Close subscription product  |
| Subscriptions/Products - Put a subscription product on hold (POST) | Subscriptions/Products Puts a subscription product on hold till you remove the hold. Put a subscription product on hold  |
| Subscriptions/Products - Remove hold on a subscription product (POST) | Subscriptions/Products Remove the hold placed on the subscription product. Remove hold on a subscription product  |
| Subscriptions/Products - Resume subscription product (POST) | Subscriptions/Products Resume a suspended subscription product. Resume subscription product  |
| Subscriptions/Products - Suspend the subscription product (POST) | Subscriptions/Products Suspend a subscription product. You must specify the Subscription Product Puid in order to suspend it. Suspend the subscription product  |
| Subscriptions/Products - Update a subscription product (PATCH) | Subscriptions/Products Update a subscription product  |
| Subscriptions/Products/Bill Lines - Update a subscription bill line (PATCH) | Subscriptions/Products/Bill Lines Update a subscription bill line  |
| Subscriptions/Products/Bill Lines/Bill Adjustments - Update a subscription bill adjustment (PATCH) | Subscriptions/Products/Bill Lines/Bill Adjustments Update a subscription bill adjustment  |
| Subscriptions/Products/Charges - Update a subscription charge (PATCH) | Subscriptions/Products/Charges Update a subscription charge  |
| Subscriptions/Products/Charges/Adjustments - Update a subscription adjustment (PATCH) | Subscriptions/Products/Charges/Adjustments Update a subscription adjustment  |
| Subscriptions/Products/Charges/Charge Tiers - Update a subscription charge tier (PATCH) | Subscriptions/Products/Charges/Charge Tiers Update a subscription charge tier  |
| Subscriptions/Products/Covered Levels - Update a covered level (PATCH) | Subscriptions/Products/Covered Levels Update a covered level  |
| Subscriptions/Products/Covered Levels/Bill Lines - Update a subscription bill line (PATCH) | Subscriptions/Products/Covered Levels/Bill Lines Update a subscription bill line  |
| Subscriptions/Products/Covered Levels/Bill Lines/Bill Adjustments - Update a subscription bill adjustment (PATCH) | Subscriptions/Products/Covered Levels/Bill Lines/Bill Adjustments Update a subscription bill adjustment  |
| Subscriptions/Products/Covered Levels/Charges - Update a subscription charge (PATCH) | Subscriptions/Products/Covered Levels/Charges Update a subscription charge  |
| Subscriptions/Products/Covered Levels/Charges/Adjustments - Update a subscription adjustment (PATCH) | Subscriptions/Products/Covered Levels/Charges/Adjustments Update a subscription adjustment  |
| Subscriptions/Products/Covered Levels/Charges/Charge Tiers - Update a subscription charge tier (PATCH) | Subscriptions/Products/Covered Levels/Charges/Charge Tiers Update a subscription charge tier  |
| Subscriptions/Products/Covered Levels/Child Covered Levels - Update a child covered level (PATCH) | Subscriptions/Products/Covered Levels/Child Covered Levels Update a child covered level  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines - Update a subscription bill line (PATCH) | Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines Update a subscription bill line  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines/Bill Adjustments - Update a subscription bill adjustment (PATCH) | Subscriptions/Products/Covered Levels/Child Covered Levels/Bill Lines/Bill Adjustments Update a subscription bill adjustment  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges - Update a subscription charge (PATCH) | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges Update a subscription charge  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Adjustments - Update a subscription adjustment (PATCH) | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Adjustments Update a subscription adjustment  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Charge Tiers - Update a subscription charge tier (PATCH) | Subscriptions/Products/Covered Levels/Child Covered Levels/Charges/Charge Tiers Update a subscription charge tier  |
| Subscriptions/Products/Covered Levels/Child Covered Levels/Relationships - Update a subscription relationship (PATCH) | Subscriptions/Products/Covered Levels/Child Covered Levels/Relationships Update a subscription relationship  |
| Subscriptions/Products/Covered Levels/Relationships - Update a subscription relationship (PATCH) | Subscriptions/Products/Covered Levels/Relationships Update a subscription relationship  |
| Subscriptions/Products/Credit Cards - Update a credit card (PATCH) | Subscriptions/Products/Credit Cards Update a credit card  |
| Subscriptions/Products/Descriptive Flexfields - Update a descriptive flexfield (PATCH) | Subscriptions/Products/Descriptive Flexfields Update a descriptive flexfield  |
| Subscriptions/Products/Relationships - Update a subscription relationship (PATCH) | Subscriptions/Products/Relationships Update a subscription relationship  |
| Subscriptions/Products/Sales Credits - Update a sales credit (PATCH) | Subscriptions/Products/Sales Credits Update a sales credit  |
| Subscriptions/Sales Credits - Update a sales credit (PATCH) | Subscriptions/Sales Credits Update a sales credit  |
| Subscriptions/Validate Subscriptions - Update a subscription validation (PATCH) | Subscriptions/Validate Subscriptions Update a subscription validation  |
| Survey Configurations - Update a survey configuration (PATCH) | Survey Configurations Update a survey configuration  |
| Survey Configurations/Survey Configuration Attributes - Update a survey configuration attribute (PATCH) | Survey Configurations/Survey Configuration Attributes Update a survey configuration attribute  |
| Surveys - Update a survey (PATCH) | Surveys Update a survey  |
| Surveys/Survey Questions - Update a survey question (PATCH) | Surveys/Survey Questions Update a survey question  |
| Surveys/Survey Questions/Survey Answer Choices - Update a survey answer choice (PATCH) | Surveys/Survey Questions/Survey Answer Choices Update a survey answer choice  |
| Surveys/Survey Requests - Update a survey request (PATCH) | Surveys/Survey Requests Update a survey request  |
| Surveys/Survey Requests/Survey Responses - Update a survey response (PATCH) | Surveys/Survey Requests/Survey Responses Update a survey response  |
| Territories for Sales - Update a territory (PATCH) | Territories for Sales Update a territory  |
| Territories for Sales/Territory Business - Update a territory line of business (PATCH) | Territories for Sales/Territory Business Update a territory line of business  |
| Territories for Sales/Territory Coverages - Update a territory coverage (PATCH) | Territories for Sales/Territory Coverages Update a territory coverage  |
| Territories for Sales/Territory Resources - Update a territory resource (PATCH) | Territories for Sales/Territory Resources Update a territory resource  |
| Universal Work Objects - Update an universal work object (PATCH) | Universal Work Objects Update an universal work object  |
| User Context Data Sources - Update a context data source (PATCH) | User Context Data Sources Update a context data source  |
| User Context Object Types - Update an object type (PATCH) | User Context Object Types Update an object type  |
| User Context Object Types/Object Configurations - Update an object configuration (PATCH) | User Context Object Types/Object Configurations Update an object configuration  |
| User Context Object Types/Object Configurations/Object Configuration Details - Update an object configuration detail (PATCH) | User Context Object Types/Object Configurations/Object Configuration Details Update an object configuration detail  |
| User Context Object Types/Object Criteria - Update an object criterion (PATCH) | User Context Object Types/Object Criteria Update an object criterion  |
| User Context Object Types/Related Objects - Update a related object mapping (PATCH) | User Context Object Types/Related Objects Update a related object mapping  |
| Work Orders - Update a work order (PATCH) | Work Orders Update a work order  |
| Work Orders/Attachments - Update an attachment (PATCH) | Work Orders/Attachments Update an attachment  |
| Wrap Ups - Update a wrap up (PATCH) | Wrap Ups Update a wrap up  |


