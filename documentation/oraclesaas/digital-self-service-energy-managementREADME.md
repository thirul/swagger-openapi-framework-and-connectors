
# digital-self-service-energy-management


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Contact Preferences - Create NotificationContact | Contact Preferences Creates a new Notification Contact Create NotificationContact  |
| Contact Preferences - Create NotificationPreference | Contact Preferences Creates a new Notification Preference for a contact. If the notification preference is for the SMS channel, then the opt in workflow is also triggered.  Create NotificationPreference  |
| Tips - Update Tip Action | Tips Updates the customer's currently recorded action for the tip. Default value for all of a customer's tips is NOT_YET. Update Tip Action  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Contact Preferences - Delete NotificationChannel | Contact Preferences Attempts to delete a Notification channel Delete NotificationChannel  |
| Contact Preferences - Delete NotificationContact | Contact Preferences Attempts to delete a Notification Contact. Delete NotificationContact  |
| Contact Preferences - Delete NotificationPreference | Contact Preferences Attempts to delete a Notification preference Delete NotificationPreference  |
| Threshold - Delete Threshold for a Customer | Threshold Deletes the threshold for the specified customer. Delete Threshold for a Customer  |
| Tips - Delete Tip Action | Tips Remove the customer's currently recorded action for the tip. Idempotent, repeatable invocations do not lead to an error Delete Tip Action  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Bill Comparison - Get Bill Comparison | Bill Comparison Returns a customer's bill comparsion.  Compare a customer's electricity or gas bill to a prior bill (either previous bill or last year's bill) along with an explanation of the cost differences. Get Bill Comparison  |
| Contact Preferences - Get NotificationPreference | Contact Preferences Retrieves the notification preference for the customer, contact, notification channel and event type. Get NotificationPreference  |
| Neighbor Comparison - Get List of Neighbor Comparisons | Neighbor Comparison Returns a neighbor comparison for a customer for each billing period in the given time span. Data is available for the past 12 months, for a maximum of 13 bills. Any billing period partially or fully within the requested time span is included in the response. If start and end dates are not provided the most recent neighbor comparison for the customer is returned. Get List of Neighbor Comparisons  |
| Tips - Get Tip | Tips Retrieve a single tip with associated metadata for a customer. Get Tip  |
| Usage - Get Billing Usage | Usage Returns the billing usage data for the given time period. Billing data can be returned for up to 25 bills in a single request. Data is available for the last two years preceding the time of the API request.  If the active utility account is associated with multiple service points of the same fuel type billed usage per service point is returned. Get Billing Usage  |
| Usage - Get Daily Interval Usage | Usage Returns the AMI usage data for the given time period, aggregated by day. Daily data can be returned for up to 45 days in a single request. Days run from midnight to midnight in the local timezone of the site or service point. If the active utility account is associated with multiple service points of the same fuel type usage data per service point is returned. Get Daily Interval Usage  |
| Usage - Get Raw Interval Usage | Usage Returns the raw AMI usage data for the given time period. Raw data can be returned for up to 100 intervals in a single request. Data is available for last two years (730 days) preceding the time of the API request. If the active utility account is associated with multiple service points of the same fuel type usage data per service point is returned.   Get Raw Interval Usage  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Bill Forecast - Get Bill Forecast | Bill Forecast Retrieve a customer's bill forecast.  Returns forecasted usage and costs for all accounts of a customer with interval data. Forecasting a billing period is based on the calculation date which is the current date (now) by default.  When a Utility Account has no modeled rates only a usage forecast is returned. Get Bill Forecast  |
| Bill Forecast - Get Bill Period | Bill Forecast Retrieves an account bill period, which is provided from a report database or calculated as the median of historical bill periods. Get Bill Period  |
| Contact Preferences - Get Notification Contact List | Contact Preferences Retrieves Notification Contacts Get Notification Contact List  |
| Contact Preferences - Get NotificationPreference List | Contact Preferences Retrieves notification preferences for the customer and contact. Get NotificationPreference List  |
| Customer Engagements - Get Click Engagements | Customer Engagements Returns a list of CLICK engagement events received for the specified utility within a specified time range, or empty response if no events found. NOTE: When startTime and endTime values are not provided, they default to one month prior and the current date and time. Including a date range is highly recommended. Data returned will be greater than or equal to (>=) startTime and less than (<) endTime.  Get Click Engagements  |
| Customer Engagements - Get Complaint Engagements | Customer Engagements Returns a list of SPAM engagement events received for the specified utility within a specified date range, or empty response if no events found. NOTE: When startTime and endTime values are not provided, they default to one month prior and the current date and time. Including a date range is highly recommended. Data returned will be greater than or equal to (>=) startTime and less than (<) endTime.  Get Complaint Engagements  |
| Customer Engagements - Get module names by channel | Customer Engagements Returns a list of modules by channel which represents the module id to name and description binding for any version of modules. If no modules were found, the server will return an empty list. Get module names by channel  |
| Customer Engagements - Get Open Engagements | Customer Engagements Returns a list of OPEN engagement events received for the specified utility within a specified date range, or empty response if no events found. NOTE: When startTime and endTime values are not provided, they default to one month prior and the current date and time. Including a date range is highly recommended. Data returned will be greater than or equal to (>=) startTime and less than (<) endTime.  Get Open Engagements  |
| Customer Engagements - Get Send Engagements | Customer Engagements Returns a list any of HARD_BOUNCE, SOFT_BOUNCE, OTHER_BOUNCE, SENT, NOT_SENT, UNDELIVERABLE engagement events received for the specified utility within a specified date range, or empty response if no events found. NOTE: When startTime and endTime values are not provided, they default to one month prior and the current date and time. Including a date range is highly recommended. Data returned will be greater than or equal to (>=) startTime and less than (<) endTime. Get Send Engagements  |
| Customer Engagements - Get Track & Campaign name info for a utility | Customer Engagements Returns a list of tracks and campaigns with their corresponding ids and names. Get Track & Campaign name info for a utility  |
| Customers - Get Customer List | Customers Returns the list of customers satisfying the query parameters.  Get Customer List  |
| Customers - Get Home Profile | Customers Returns the home profile for a customer, including demographics and information about the home.  Get Home Profile  |
| Customers - Get Utility Account List | Customers Returns the list of utility accounts associated to a given customer satisfying the query parameters.  Get Utility Account List  |
| Neighbor Comparison - Get Latest Neighbor Comparison | Neighbor Comparison Provides the latest neighbor comparison for a customer as of a particular date. Note that the date ranges for gas and electric are usually different. These ranges come from the billing cycle of the customer and it is uncommon for customers to receive both gas and electric bills on the same cycle. For the combined fuel-type, we pro-rate and normalize the gas reads (including those outside the gas bill returned in the "gas" section) to the electric billing cycle. A comparison is not guaranteed for all fuel types. For example, a customer's gas account may have usage gaps that prohibit the generation of a neighbor comparison. Get Latest Neighbor Comparison  |
| Threshold - Get Threshold Value for a Customer | Threshold Returns a customer's threshold value using the utility customer identifier registered in the system. Get Threshold Value for a Customer  |
| Tips - Get Tip List | Tips Retrieves a list of relevant tips. Sort order is the customer's expected energy savings for each tip, from highest to lowest savings. Get Tip List  |
| Tips - Get Tip Name List | Tips Retrieve a sorted list of tip names for a particular customer. Sort order is the customer's expected energy savings for each tip, from highest to lowest savings. Get Tip Name List  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Contact Preferences - Update NotificationContact (PUT) | Contact Preferences Updates an existing Notification Contact. This endpoint returns the same response in both cases when a contact is updated and when the update does not modify the contact. If the update contains a phoneSMS, then the opt in workflow is also triggered.  Update NotificationContact  |
| Customers - Update Home Profile (PUT) | Customers Update the home profile for a customer.  Update Home Profile  |
| Threshold - Set or Update a Customer's Threshold (PUT) | Threshold Sets or updates the threshold value for the specified customer. Set or Update a Customer's Threshold  |


