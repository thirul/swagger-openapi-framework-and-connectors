# Oracle Cloud Applications Connector
This connector implements all the Oracle Cloud Applications REST APIs. For more information please refer to https://docs.oracle.com/en/cloud/saas/index.html

# Connection Tab
## Connection Fields


#### Oracle REST API Service

The URL for the Oracle SaaS descriptor file

**Type** - string

##### Allowed Values

 * Oracle Address Verification Cloud - 2.0
 * Oracle B2C Service Cloud - 21a
 * Oracle Cloud Common Features - 21b
 * Oracle CX Sales and B2B Service Cloud - 21b
 * Oracle Digital Self Service Energy Management
 * Oracle Field Service Cloud - 21a
 * Oracle Financials Cloud - 21b
 * Oracle Global Human Resources (HCM) Cloud - 21b
 * Oracle Procurement Cloud - 21b
 * Oracle Project Portfolio Management Cloud - 21b
 * Oracle Risk Management Cloud - 21b
 * Oracle Supply Chain Management Cloud - 21b


#### Other Oracle REST API Swagger URL

This will override selections so you can provide a url to any swagger file.

**Type** - string



#### URL

The URL for the Service service

**Type** - string



#### AuthenticationType

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - oauth

# Operation Tab


## CREATE/EXECUTE
### Operation Fields


##### URL Path

Specify a URL to execute a specific path. 
			This path can be provided from query response elements such as links/@rel=self.
			Note the if a domain name is included, only the path will be used.

**Type** - string



## UPDATE
### Operation Fields


##### URL Path

Specify a URL to execute a specific path. 
			This path can be provided from query response elements such as links/@rel=self.
			Note the if a domain name is included, only the path will be used.

**Type** - string



## GET
### Operation Fields


##### URL Path

Specify a URL to execute a specific path. 
			This path can be provided from query response elements such as links/@rel=self.
			Note the if a domain name is included, only the path will be used.

**Type** - string



## DELETE
### Operation Fields


##### URL Path

Specify a URL to execute a specific path. 
			This path can be provided from response elements such as links/@rel=self.
			Note the if a domain name is included, only the path will be used.

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. This can be used for entering endpoint specific parameters such as 'expand=' or 'q='. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



##### Use symbolic query filter operators

Oracle HCM use symbol operators (=,>,>=,<,<=) while other products use text operators (eq,gt,ge,lt,le)

**Type** - boolean



##### Allow Field Selection

Allow individually fields to be selected. Otherwise field
				selection is only to choose what related child entities to expand and include
				in the response along with all the fields for each selected entity.
				Specifying individual fields to return requires large API URLs and
				is recommended when only relatively few fields are required.

**Type** - boolean

**Default Value** - false


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% or * wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list, strings in single quotes surround by parenthesis. ie. ('active','pending')  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

