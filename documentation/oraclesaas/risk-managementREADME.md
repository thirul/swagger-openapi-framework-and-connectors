
# risk-management


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Advanced Controls Mass Edit Incidents - Create and run mass edit job for advanced controls... | Advanced Controls Mass Edit Incidents Create and run mass edit job for advanced controls incidents  |
| Advanced Controls/Comments - Create all comments | Advanced Controls/Comments Create all comments  |
| Advanced Controls/Incidents/Comments - Create all comments | Advanced Controls/Incidents/Comments Create all comments  |
| Asynchronous Separation of Duties Simulations - Get a status | Asynchronous Separation of Duties Simulations Returns the status of the provisioning simulation request. When the status is Completed, call the Get all results action. This returns conflicts a user will have if the requested roles and data are granted.<br><br>Results include the control ID and control name, the requested role and its incident path, as well as the roles it conflicts with. Get a status  |
| Asynchronous Separation of Duties Simulations - Perform analysis | Asynchronous Separation of Duties Simulations Use this action to initiate a simulation of active access controls.<br><br>No incidents are created from the use of this REST resource. The results produced by this resource are only a simulation. Perform analysis  |
| Asynchronous Separation of Duties Simulations - POST action not supported | Asynchronous Separation of Duties Simulations POST action not supported POST action not supported  |
| Financial Reporting Compliance Controls - Create a control | Financial Reporting Compliance Controls Create a control  |
| Financial Reporting Compliance Controls/Assertions - Create an assertion | Financial Reporting Compliance Controls/Assertions Create an assertion  |
| Financial Reporting Compliance Controls/Comments - Create comments for controls | Financial Reporting Compliance Controls/Comments Create comments for controls  |
| Financial Reporting Compliance Controls/Test Plans - Create a test plan | Financial Reporting Compliance Controls/Test Plans Create a test plan  |
| Financial Reporting Compliance Controls/Test Plans/Steps - Create a test step | Financial Reporting Compliance Controls/Test Plans/Steps Create a test step  |
| Financial Reporting Compliance Controls/Test Plans/Test Plan Activity Type - Create a plan activity | Financial Reporting Compliance Controls/Test Plans/Test Plan Activity Type Create a plan activity  |
| Financial Reporting Compliance Issues - Create an issue | Financial Reporting Compliance Issues Create an issue  |
| Financial Reporting Compliance Processes - Create a process | Financial Reporting Compliance Processes Create a process  |
| Financial Reporting Compliance Processes/Comments - Create a comment for a process | Financial Reporting Compliance Processes/Comments Create a comment for a process  |
| Financial Reporting Compliance Processes/Related Risks - Create an association of a risk to a process | Financial Reporting Compliance Processes/Related Risks Create an association of a risk to a process  |
| Financial Reporting Compliance Risks - Create a risk | Financial Reporting Compliance Risks Create a risk  |
| Financial Reporting Compliance Risks/Comments - Create a comment for a risk | Financial Reporting Compliance Risks/Comments Create a comment for a risk  |
| Run Advanced Control Jobs - Run all advanced controls | Run Advanced Control Jobs Run all advanced controls  |
| RunIntraRoleCheck - Create an intrarole rules check | Provisioning Rules Create an intrarole rules check  |
| RunUserRoleCheck - Create a rules check for role assignments | Provisioning Rules Create a rules check for role assignments  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Financial Reporting Compliance Controls/Assertions - Delete an assertion | Financial Reporting Compliance Controls/Assertions Delete an assertion  |
| Financial Reporting Compliance Controls/Test Plans - Delete a test plan | Financial Reporting Compliance Controls/Test Plans Delete a test plan  |
| Financial Reporting Compliance Controls/Test Plans/Steps - Delete a test step | Financial Reporting Compliance Controls/Test Plans/Steps Delete a test step  |
| Financial Reporting Compliance Processes/Related Risks - Delete a risk from a process | Financial Reporting Compliance Processes/Related Risks Delete a risk from a process  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| Advanced Controls - Get an advanced control | Advanced Controls Get an advanced control  |
| Advanced Controls Mass Edit Incidents - GET action not supported | Advanced Controls Mass Edit Incidents GET action not supported  |
| Advanced Controls/Advanced Control Flexfields - Get a flexfield | Advanced Controls/Advanced Control Flexfields Get a flexfield  |
| Advanced Controls/Comments - Get a comment | Advanced Controls/Comments Get a comment  |
| Advanced Controls/Incidents - Get an incident | Advanced Controls/Incidents Get an incident  |
| Advanced Controls/Incidents/Advanced Control Flexfields - Get a flexfield | Advanced Controls/Incidents/Advanced Control Flexfields Get a flexfield  |
| Advanced Controls/Incidents/Comments - Get a comment | Advanced Controls/Incidents/Comments Get a comment  |
| Advanced Controls/Incidents/Perspectives - Get a perspective | Advanced Controls/Incidents/Perspectives Get a perspective  |
| Advanced Controls/Incidents/Perspectives/Perspectives - Get a perspective | Advanced Controls/Incidents/Perspectives/Perspectives Get a perspective  |
| Advanced Controls/Perspectives - Get a perspective | Advanced Controls/Perspectives Get a perspective  |
| Advanced Controls/Perspectives/Perspectives - Get a perspective | Advanced Controls/Perspectives/Perspectives Get a perspective  |
| Asynchronous Separation of Duties Simulations - Get a result | Asynchronous Separation of Duties Simulations Get a result  |
| Control Assessment Results - Get a control assessment | Control Assessment Results Get a control assessment  |
| Control Assessment Results/Flexfields - Get a result for a control assessment | Control Assessment Results/Flexfields Get a result for a control assessment  |
| Financial Reporting Compliance Controls - Get a control | Financial Reporting Compliance Controls Get a control  |
| Financial Reporting Compliance Controls/Assertions - Get an assertion | Financial Reporting Compliance Controls/Assertions Get an assertion  |
| Financial Reporting Compliance Controls/Comments - Get a comment for a control | Financial Reporting Compliance Controls/Comments Get a comment for a control  |
| Financial Reporting Compliance Controls/Flexfields - Get a descriptive flexfield value for a control | Financial Reporting Compliance Controls/Flexfields Get a descriptive flexfield value for a control  |
| Financial Reporting Compliance Controls/Perspectives - Get a perspective | Financial Reporting Compliance Controls/Perspectives Get a perspective  |
| Financial Reporting Compliance Controls/Related Risks - Get a related risk for a control | Financial Reporting Compliance Controls/Related Risks Get a related risk for a control  |
| Financial Reporting Compliance Controls/Test Plans - Get a test plan | Financial Reporting Compliance Controls/Test Plans Get a test plan  |
| Financial Reporting Compliance Controls/Test Plans/Steps - Get a test step | Financial Reporting Compliance Controls/Test Plans/Steps Get a test step  |
| Financial Reporting Compliance Controls/Test Plans/Test Plan Activity Type - Get a plan activity | Financial Reporting Compliance Controls/Test Plans/Test Plan Activity Type Get a plan activity  |
| Financial Reporting Compliance Issues - Get an issue | Financial Reporting Compliance Issues Get an issue  |
| Financial Reporting Compliance Issues/Flexfields - Get a descriptive flexfield for an issue | Financial Reporting Compliance Issues/Flexfields Get a descriptive flexfield for an issue  |
| Financial Reporting Compliance Processes - Get a process | Financial Reporting Compliance Processes Get a process  |
| Financial Reporting Compliance Processes/Action Items - Get an action item for a process | Financial Reporting Compliance Processes/Action Items Get an action item for a process  |
| Financial Reporting Compliance Processes/Comments - Get a comment for a process | Financial Reporting Compliance Processes/Comments Get a comment for a process  |
| Financial Reporting Compliance Processes/Flexfields - Get a descriptive flexfield for a process | Financial Reporting Compliance Processes/Flexfields Get a descriptive flexfield for a process  |
| Financial Reporting Compliance Processes/Perspectives - Get a perspective for a process | Financial Reporting Compliance Processes/Perspectives Get a perspective for a process  |
| Financial Reporting Compliance Processes/Related Risks - Get a related risk for a process | Financial Reporting Compliance Processes/Related Risks Get a related risk for a process  |
| Financial Reporting Compliance Risks - Get a risk | Financial Reporting Compliance Risks Get a risk  |
| Financial Reporting Compliance Risks/Comments - Get a comment for a risk | Financial Reporting Compliance Risks/Comments Get a comment for a risk  |
| Financial Reporting Compliance Risks/Flexfields - Get a descriptive flexfield for a risk | Financial Reporting Compliance Risks/Flexfields Get a descriptive flexfield for a risk  |
| Financial Reporting Compliance Risks/Perspectives - Get a perspective value for a risk | Financial Reporting Compliance Risks/Perspectives Get a perspective value for a risk  |
| Financial Reporting Compliance Risks/Related Controls - Get a related control for a risk | Financial Reporting Compliance Risks/Related Controls Get a related control for a risk  |
| Financial Reporting Compliance Risks/Related Processes - Get a related process for a risk | Financial Reporting Compliance Risks/Related Processes Get a related process for a risk  |
| Process Assessment Results - Get a process assessment | Process Assessment Results Get a process assessment  |
| Process Assessment Results/Flexfields - Get a result for a process assessment | Process Assessment Results/Flexfields Get a result for a process assessment  |
| Risk Assessment Results - Get a risk assessment | Risk Assessment Results Get a risk assessment  |
| Risk Assessment Results/Flexfields - Get a result for a risk assessment | Risk Assessment Results/Flexfields Get a result for a risk assessment  |
| Risk Management Jobs - Get a job | Risk Management Jobs Get a job  |
| Run Advanced Control Jobs - GET action not supported | Run Advanced Control Jobs GET action not supported  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Advanced Controls - Get all advanced controls | Advanced Controls Get all advanced controls  |
| Advanced Controls Mass Edit Incidents - GET action not supported | Advanced Controls Mass Edit Incidents GET action not supported  |
| Advanced Controls/Advanced Control Flexfields - Get all flexfields | Advanced Controls/Advanced Control Flexfields Get all flexfields  |
| Advanced Controls/Comments - Get all comments | Advanced Controls/Comments Get all comments  |
| Advanced Controls/Incidents - Get all incidents | Advanced Controls/Incidents Get all incidents  |
| Advanced Controls/Incidents/Advanced Control Flexfields - Get all flexfields | Advanced Controls/Incidents/Advanced Control Flexfields Get all flexfields  |
| Advanced Controls/Incidents/Comments - Get all comments | Advanced Controls/Incidents/Comments Get all comments  |
| Advanced Controls/Incidents/Perspectives - Get all perspectives | Advanced Controls/Incidents/Perspectives Get all perspectives  |
| Advanced Controls/Incidents/Perspectives/Perspectives - Get all perspectives | Advanced Controls/Incidents/Perspectives/Perspectives Get all perspectives  |
| Advanced Controls/Perspectives - Get all perspectives | Advanced Controls/Perspectives Get all perspectives  |
| Advanced Controls/Perspectives/Perspectives - Get all perspectives | Advanced Controls/Perspectives/Perspectives Get all perspectives  |
| Asynchronous Separation of Duties Simulations - Get all results | Asynchronous Separation of Duties Simulations Get all results  |
| Control Assessment Results - Get all assessments for a control | Control Assessment Results Get all assessments for a control  |
| Control Assessment Results/Flexfields - Get all results for control assessments | Control Assessment Results/Flexfields Get all results for control assessments  |
| Financial Reporting Compliance Controls - Get all controls | Financial Reporting Compliance Controls Get all controls  |
| Financial Reporting Compliance Controls/Assertions - Get all assertions | Financial Reporting Compliance Controls/Assertions Get all assertions  |
| Financial Reporting Compliance Controls/Comments - Get all comments for controls | Financial Reporting Compliance Controls/Comments Get all comments for controls  |
| Financial Reporting Compliance Controls/Flexfields - Get all descriptive flexfields for a control | Financial Reporting Compliance Controls/Flexfields Get all descriptive flexfields for a control  |
| Financial Reporting Compliance Controls/Perspectives - Get all perspectives | Financial Reporting Compliance Controls/Perspectives Get all perspectives  |
| Financial Reporting Compliance Controls/Related Risks - Get all related risks for a control | Financial Reporting Compliance Controls/Related Risks Get all related risks for a control  |
| Financial Reporting Compliance Controls/Test Plans - Get all test plans | Financial Reporting Compliance Controls/Test Plans Get all test plans  |
| Financial Reporting Compliance Controls/Test Plans/Steps - Get all test steps | Financial Reporting Compliance Controls/Test Plans/Steps Get all test steps  |
| Financial Reporting Compliance Controls/Test Plans/Test Plan Activity Type - Get all plan activities | Financial Reporting Compliance Controls/Test Plans/Test Plan Activity Type Get all plan activities  |
| Financial Reporting Compliance Issues - Get all issues | Financial Reporting Compliance Issues Get all issues  |
| Financial Reporting Compliance Issues/Flexfields - Get all descriptive flexfields for an issue | Financial Reporting Compliance Issues/Flexfields Get all descriptive flexfields for an issue  |
| Financial Reporting Compliance Processes - Get all processes | Financial Reporting Compliance Processes Get all processes  |
| Financial Reporting Compliance Processes/Action Items - Get all action items for a process | Financial Reporting Compliance Processes/Action Items Get all action items for a process  |
| Financial Reporting Compliance Processes/Comments - Get all comments for a process | Financial Reporting Compliance Processes/Comments Get all comments for a process  |
| Financial Reporting Compliance Processes/Flexfields - Get the descriptive flexfields for a process | Financial Reporting Compliance Processes/Flexfields Get the descriptive flexfields for a process  |
| Financial Reporting Compliance Processes/Perspectives - Get all perspectives for a process | Financial Reporting Compliance Processes/Perspectives Get all perspectives for a process  |
| Financial Reporting Compliance Processes/Related Risks - Get all related risks for a process | Financial Reporting Compliance Processes/Related Risks Get all related risks for a process  |
| Financial Reporting Compliance Risks - Get all risks | Financial Reporting Compliance Risks Get all risks  |
| Financial Reporting Compliance Risks/Comments - Get all comments for a risk | Financial Reporting Compliance Risks/Comments Get all comments for a risk  |
| Financial Reporting Compliance Risks/Flexfields - Get all descriptive flexfields for a risk | Financial Reporting Compliance Risks/Flexfields Get all descriptive flexfields for a risk  |
| Financial Reporting Compliance Risks/Perspectives - Get all perspective values for a risk | Financial Reporting Compliance Risks/Perspectives Get all perspective values for a risk  |
| Financial Reporting Compliance Risks/Related Controls - Get all related controls for a risk | Financial Reporting Compliance Risks/Related Controls Get all related controls for a risk  |
| Financial Reporting Compliance Risks/Related Processes - Get all related processes for a risk | Financial Reporting Compliance Risks/Related Processes Get all related processes for a risk  |
| Process Assessment Results - Get all assessments for a process | Process Assessment Results Get all assessments for a process  |
| Process Assessment Results/Flexfields - Get all results for process assessments | Process Assessment Results/Flexfields Get all results for process assessments  |
| Risk Assessment Results - Get all assessments for a risk | Risk Assessment Results Get all assessments for a risk  |
| Risk Assessment Results/Flexfields - Get all results for risk assessments | Risk Assessment Results/Flexfields Get all results for risk assessments  |
| Risk Management Jobs - Get all jobs | Risk Management Jobs Get all jobs  |
| Run Advanced Control Jobs - GET action not supported | Run Advanced Control Jobs GET action not supported  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Advanced Controls - Update an advanced control (PATCH) | Advanced Controls Update an advanced control  |
| Advanced Controls/Advanced Control Flexfields - Update a flexfield (PATCH) | Advanced Controls/Advanced Control Flexfields Update a flexfield  |
| Advanced Controls/Incidents - Update all incidents (PATCH) | Advanced Controls/Incidents Update all incidents  |
| Advanced Controls/Incidents/Advanced Control Flexfields - Update a flexfield (PATCH) | Advanced Controls/Incidents/Advanced Control Flexfields Update a flexfield  |
| Control Assessment Results - Update a control assessment (PATCH) | Control Assessment Results Update a control assessment  |
| Control Assessment Results/Flexfields - Update a result of a control assessment (PATCH) | Control Assessment Results/Flexfields Update a result of a control assessment  |
| Financial Reporting Compliance Controls - Update a control (PATCH) | Financial Reporting Compliance Controls Update a control  |
| Financial Reporting Compliance Controls/Assertions - Update an assertion (PATCH) | Financial Reporting Compliance Controls/Assertions Update an assertion  |
| Financial Reporting Compliance Controls/Flexfields - Update a descriptive flexfield value for a control (PATCH) | Financial Reporting Compliance Controls/Flexfields Update a descriptive flexfield value for a control  |
| Financial Reporting Compliance Controls/Test Plans - Update a test plan (PATCH) | Financial Reporting Compliance Controls/Test Plans Update a test plan  |
| Financial Reporting Compliance Controls/Test Plans/Steps - Update a test step (PATCH) | Financial Reporting Compliance Controls/Test Plans/Steps Update a test step  |
| Financial Reporting Compliance Issues - Update an issue (PATCH) | Financial Reporting Compliance Issues Update an issue  |
| Financial Reporting Compliance Issues/Flexfields - Update a descriptive flexfield value for an issue (PATCH) | Financial Reporting Compliance Issues/Flexfields Update a descriptive flexfield value for an issue  |
| Financial Reporting Compliance Processes - Update a process (PATCH) | Financial Reporting Compliance Processes Update a process  |
| Financial Reporting Compliance Processes/Flexfields - Update a descriptive flexfield for a process (PATCH) | Financial Reporting Compliance Processes/Flexfields Update a descriptive flexfield for a process  |
| Financial Reporting Compliance Risks - PATCH action not supported (PATCH) | Financial Reporting Compliance Risks PATCH action not supported  |
| Financial Reporting Compliance Risks/Flexfields - Update a descriptive flexfield for a risk (PATCH) | Financial Reporting Compliance Risks/Flexfields Update a descriptive flexfield for a risk  |
| Process Assessment Results - Update a process assessment (PATCH) | Process Assessment Results Update a process assessment  |
| Process Assessment Results/Flexfields - Update a result of a process assessment (PATCH) | Process Assessment Results/Flexfields Update a result of a process assessment  |
| Risk Assessment Results - Update a risk assessment (PATCH) | Risk Assessment Results Update a risk assessment  |
| Risk Assessment Results/Flexfields - Update a result of a risk assessment (PATCH) | Risk Assessment Results/Flexfields Update a result of a risk assessment  |


