# Fama.io API
This connector supports the REST API for fama.io

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### Fama.io API REST API Service URL

The URL for the Fama.io API REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| CreateFlagKit - Create a behavior kit | flagkit Creates a new behavior kit. Must have admin access Create a behavior kit  |
| CreatePerson - Create a person | person Creates a new person and the first report for that person Create a person  |
| CreateUser - Create user | user Creates a specific user. Must have admin rights Create user  |
| ReAddPerson - Generate a new updated report | person Generate a new updated report for an existing person Generate a new updated report  |
| ShareReport - Share report with user | person Share report with user  |
| UpdatePersonReport - Update report | person Generate a new report for Person with the most recent version of the previously used FlagKit. Does not allow the selection of a different FlagKit. Update report  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| CountGroupPersons - Get group's person count | person Get the number of people attached to a group Get group's person count  |
| GetAllReportsPerson - Get all reports | person Get all reports for a specific person Get all reports  |
| GetFlagKit - Get behavior kit | flagkit Get behavior kit  |
| GetPersonPDFReport - Retrieve PDF Report | person Retrieve a specific report for a person in PDF format Retrieve PDF Report  |
| GetPersonReport - Get specific report | person Retrieve a specific report for a person Get specific report  |
| GetTotalFinishedFlagKitBetweenDates - Get total finished candidates by flagkit in date r... | person Get the number of finished candidates in a company for a specificset of flags and between a range of dates Get total finished candidates by flagkit in date range  |
| GetTotalFinishedFlagKitCount - Get total finished reports by flagkit | person Get the number of finished candidates in the company for a specific set of flags Get total finished reports by flagkit  |
| GetTotalReports - Get total reports | person Retrieve the total reports for the company or user group Get total reports  |
| GetUser - Retrieve current user information | user Retrieve current user information  |
| SearchPeople - Search for people | person Search for all people that match specific criteria Search for people  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| article - Get Page of Articles | article Gets a Page of Articles Get Page of Articles  |
| GetGroupPersons - Get group of persons | person Retrieve a page of people from a specific group Get group of persons  |
| GetPageOfFlagKits - Get all Behavior Kits | flagkit Get paged Behavior Kits for company. Must have admin or user rights. Get all Behavior Kits  |
| GetPageOfUsers - Get users | user Retrieve a paged collection of users. Get users  |
| GetReportPaged - Get user/company report CSV | person Get a paged collection of report metrics for in CSV format Get user/company report CSV  |
| GetReportsPage - Get page of reports | person Get a paged collection of reports for a specific person Get page of reports  |
| post - Get Page of Posts | post Gets a Page of Posts Get Page of Posts  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| FinishReview - Set report's review status to review complete (PUT) | person Update a report's review status to review complete Set report's review status to review complete  |
| FinishReviewAll - Update all reports with review status pending to r... (PUT) | person Sets all reports requiring review to review complete Update all reports with review status pending to review complete for all user's reports or all company's reports  |
| SetCompany - Update user's company information (PUT) | company Updates the current user's company information. Must be admin of company Update user's company information  |
| UpdateFlagKit - Update existing behavior kit (PUT) | flagkit Updates the information of an existing Behavior Kit. Update existing behavior kit  |
| UpdateUserInfo - Update user information (PUT) | user Updates a user's information with provided details Update user information  |


