# Accellion Kiteworks REST API
This connector provides access to the REST API for the The Kiteworks content firewall helps IT executives lock down the exchange of confidential enterprise information with customers, suppliers, and partners by unifying visibility and security across siloed third-party communication channels, including email, file sharing, mobile, web forms, managed file transfer, and SFTP. For more information, click here: https://developer.accellion.com/content/api-guides-landing-page.htm

# Connection Tab
## Connection Fields


#### Accellion Kiteworks REST API Service URL

The URL for the Accellion Kiteworks REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default openapi.json file so you can provide a url to any swagger file.

**Type** - string



#### URL

The URL for the Service service

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### Use Authentication Loading Swagger

Select this option if loading a swagger from a URL requires authentication.

**Type** - string



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE | UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| admin - Add a device | admin Add a device. This method is used when a user logs in to kiteworks     to track the device they use. Add a device  | /rest/admin/devices\_\_\_post |
| admin - Add assigned ECM source | admin Add assigned source. Add assigned ECM source  | /rest/admin/sources\_\_\_post |
| admin - Create a client | admin Create a new client that will be able to access the kiteworks system.     e.g.: A new mobile app that is customized for your company. Create a client  | /rest/admin/clients\_\_\_post |
| admin - Create a location entry. | admin Creates a location entry by providing a name. Create a location entry.  | /rest/admin/locations\_\_\_post |
| admin - Create a User | admin Creates a new User in the system by specifying an email address and name. Create a User  | /rest/admin/users\_\_\_post |
| admin - Create an LDAP group | admin Creates an LDAP group Create an LDAP group  | /rest/admin/ldapGroups\_\_\_post |
| admin - Endpoint to bulk update users emails | admin Endpoint to bulk update users emails. Accepts array of old and new user email pairs Endpoint to bulk update users emails  | /rest/admin/users/migrateEmails\_\_\_post |
| admin - Endpoint to bulk update users emails | admin Endpoint to bulk update users emails. Accepts CSV file.  Endpoint to bulk update users emails  | /rest/admin/users/migrateEmailsCsv\_\_\_post |
| admin - Remote wipe the specified device | admin Set the remote wipe flag for the specified install_tag_id Remote wipe the specified device  | /rest/admin/devices/actions/wipe\_\_\_post |
| admin - Upload license | admin Upload the kiteworks license file. Upload license  | /rest/admin/licenses\_\_\_post |
| admin - Uploads a profile image | admin Uploads an image file to use as profile image Uploads a profile image  | /rest/admin/users/{id}/profileImage\_\_\_post |
| clients - Create a client | clients Create a new client that will be able to access the kiteworks system.                e.g.: A new mobile app that is customized for your company. Create a client  | /rest/clients\_\_\_post |
| clients - Create a new container | clients Create a new container under the specified client. Create a new container  | /rest/clients/{id}/containers\_\_\_post |
| comments - Create a Comment on a file | comments files Create a Comment on a file. Create a Comment on a file  | /rest/files/{id}/comments\_\_\_post |
| contacts - Create a contact entry | contacts Creates a kiteworks contact entry that includes a contact name and email address. Create a contact entry  | /rest/contacts\_\_\_post |
| containers - Create a new container | containers Create a new container under the specified parent_id. Create a new container  | /rest/containers/{parent\_id}/containers\_\_\_post |
| containers - Create a new container folder | containers Create a new container folder under the specified container parent_id. Create a new container folder  | /rest/containers/{parent\_id}/folders\_\_\_post |
| devices - Add a device | devices Add a device.                    This method is used when a user logs in to kiteworks to track the device they use. Add a device  | /rest/devices\_\_\_post |
| favorites - Add a favorite | favorites Adding a favorite Add a favorite  | /rest/favorites\_\_\_post |
| favorites - Set multiple folders as favorite | favorites folders Set multiple folders as favorite Set multiple folders as favorite  | /rest/folders/actions/favorite\_\_\_post |
| files - Create a Task on a file | files tasks Creates a task on a file Create a Task on a file  | /rest/files/{id}/tasks\_\_\_post |
| files - promote specified file version | files promote version promote specified file version  | /rest/files/{id}/versions/{version\_id}/actions/promote\_\_\_post |
| files - upload  base64 encoded content | files uploads base64 encoded file content to a folder upload  base64 encoded content  | /rest/folders/{id}/actions/fileBase64Encoded\_\_\_post |
| files - upload content | files uploads file content to a folder upload content  | /rest/folders/{id}/actions/file\_\_\_post |
| files - upload content | files requestFile uploads file content to a request folder upload content  | /rest/requestFile/{ref}/actions/file\_\_\_post |
| files - upload content | files uploads file content to a kp folder upload content  | /rest/sources/{id}/actions/file\_\_\_post |
| folders - add group members (DEPRECATED in 6.1) | folders add group members to the folder add group members (DEPRECATED in 6.1)  | /rest/folders/{id}/members/ldapGroup\_\_\_post |
| folders - add members (DEPRECATED in 6.1) | folders add members to the folder add members (DEPRECATED in 6.1)  | /rest/folders/{id}/members\_\_\_post |
| folders - Create a new folder | folders Create a new folder under the specified parent_id. Create a new folder  | /rest/folders/{parent\_id}/folders\_\_\_post |
| folders - initiate chunk file upload | folders uploads initiates file chunk upload session initiate chunk file upload  | /rest/folders/{id}/actions/initiateUpload\_\_\_post |
| groups - Create a group entry | groups Create a group of users  Create a group entry  | /rest/groups\_\_\_post |
| ldapGroups - Create an LDAP group | ldapGroups Creates an LDAP group Create an LDAP group  | /rest/ldapGroups\_\_\_post |
| licenses - Upload license | licenses Upload the kiteworks license file. Upload license  | /rest/licenses\_\_\_post |
| locations - Create a location entry. | locations Creates a location entry by providing a name. Create a location entry.  | /rest/locations\_\_\_post |
| mail - Create a send message email entry | mail folders Creates a send message email entry Create a send message email entry  | /rest/folders/{id}/actions/sendMessage\_\_\_post |
| mail - Create a sendfile email entry | mail Creates a sendfile email entry Create a sendfile email entry  | /rest/mail/actions/sendFile\_\_\_post |
| mail - Create a sendfile email entry for sending out by a... | mail Creates a sendfile email entry for sending out by an external client Create a sendfile email entry for sending out by an external client  | /rest/mail/actions/sendFileExternal\_\_\_post |
| profiles - Add custom profile | profiles Add new custom profile cloned from built in profile Add custom profile  | /rest/profiles\_\_\_post |
| sources - Add user ECM source | sources Add user source. Add user ECM source  | /rest/sources\_\_\_post |
| uploads - initiate chunk file upload for new file version | uploads files initiates file chunk upload session for new file version initiate chunk file upload for new file version  | /rest/files/{id}/actions/initiateUpload\_\_\_post |
| users - Create a User | users Creates a new User in the system by specifying an email address and name. Create a User  | /rest/users\_\_\_post |
| users - Uploads a profile image | users Uploads an image file to use as profile image Uploads a profile image  | /rest/users/{id}/profileImage\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| admin - Delete a client | admin Delete a client from the client list. Delete a client  | /rest/admin/clients/{id}\_\_\_delete |
| admin - Delete a device | admin Delete a device given the client id Delete a device  | /rest/admin/devices/{id}\_\_\_delete |
| admin - Delete a location. | admin Delete the specified location. Delete a location.  | /rest/admin/locations/{id}\_\_\_delete |
| admin - Deletes a profile image | admin Deletes an image file to use as profile image Deletes a profile image  | /rest/admin/users/{id}/profileImage\_\_\_delete |
| admin - Deletes a User | admin Mark the specified user as deleted. This user will still be returned in the GET Users query. Deletes a User  | /rest/admin/users/{id}\_\_\_delete |
| admin - Deletes an LDAP group | admin Deletes an LDAP group Deletes an LDAP group  | /rest/admin/ldapGroups/{id}\_\_\_delete |
| admin - Deletes specified assigned source | admin Deletes specified assigned source. Deletes specified assigned source  | /rest/admin/sources/{id}\_\_\_delete |
| adminRoles - Delete a user as admin role | adminRoles Delete a user as admin role.                   This method does not delete the user but only the role of admin for that user. Delete a user as admin role  | /rest/adminRoles/{id}/users/{user\_id}\_\_\_delete |
| clients - Delete a client | clients Delete a client from the client list. Delete a client  | /rest/clients/{id}\_\_\_delete |
| comments - Delete a Comment | comments Deletes the comment with the specified id. Delete a Comment  | /rest/comments/{id}\_\_\_delete |
| contacts - Deletes a contact entry | contacts Delete a contact entry from the kiteworks contact list for this user. Deletes a contact entry  | /rest/contacts/{id}\_\_\_delete |
| containers - Mark specified container for deletion. | containers Marks the specified container for deletion. Mark specified container for deletion.  | /rest/containers/{id}\_\_\_delete |
| devices - Delete a device | devices Delete a device given the client id Delete a device  | /rest/devices/{id}\_\_\_delete |
| devices - Logout your current device | devices Log your current device off of kiteworks. Logout your current device  | /rest/devices/me/actions/logout\_\_\_delete |
| favorites - Remove a favorite | favorites Deleting a favorite Remove a favorite  | /rest/favorites/{id}\_\_\_delete |
| favorites - Removes specified folders from favorites | favorites folders Removes specified folders from favorites Removes specified folders from favorites  | /rest/folders/actions/favorite\_\_\_delete |
| files - Deletes list of files | files Deletes list of files. Deletes list of files  | /rest/files\_\_\_delete |
| files - Deletes the specified file version | files Delete version Deletes the specified file version  | /rest/files/{id}/versions/{version\_id}\_\_\_delete |
| files - Mark file for deletion. | files Marks a file with the given ID as deleted. This file will be cleaned up                        after x days as set in the admin. Mark file for deletion.  | /rest/files/{id}\_\_\_delete |
| files - Permanently delete a file | files Permanently delete the specified file. This file will no longer be accessible. Permanently delete a file  | /rest/files/{id}/actions/permanent\_\_\_delete |
| folders - Delete group members | folders Deletes group members in the folder Delete group members  | /rest/folders/{id}/members/ldapGroup/{ldapGroupId}\_\_\_delete |
| folders - Delete members | folders Deletes members in the folder Delete members  | /rest/folders/{id}/members/{member\_id}\_\_\_delete |
| folders - Deletes list of folders | folders Deletes list of folders. Deletes list of folders  | /rest/folders\_\_\_delete |
| folders - Mark specified folder for deletion. | folders Marks the specified folder for deletion.                   The folder is still accessible as a deleted folder                   until it expires and is deleted permanently. Mark specified folder for deletion.  | /rest/folders/{id}\_\_\_delete |
| folders - Permanently delete the specified folder. | folders Permanently delete the specified folder.                This folder will no longer be accessible by any means. Permanently delete the specified folder.  | /rest/folders/{id}/actions/permanent\_\_\_delete |
| folders - Remove ownself from the folder | folders Remove ownself from the folderr Remove ownself from the folder  | /rest/folders/{id}/members/me\_\_\_delete |
| groups - Deletes a group entry | groups Delete a group Deletes a group entry  | /rest/groups/{id}\_\_\_delete |
| ldapGroups - Deletes an LDAP group | ldapGroups Deletes an LDAP group Deletes an LDAP group  | /rest/ldapGroups/{id}\_\_\_delete |
| locations - Delete a location. | locations Delete the specified location. Delete a location.  | /rest/locations/{id}\_\_\_delete |
| mail - Delete an email draft | mail Allows a sender to delete a draft of email Delete an email draft  | /rest/mail/{id}\_\_\_delete |
| notifications - Remove notification options | notifications Remove notification option Remove notification options  | /rest/notifications/{object\_id}\_\_\_delete |
| profiles - Delete custom profile (DEPRECATED in 9) | profiles Delete custom profile Delete custom profile (DEPRECATED in 9)  | /rest/profiles/{id}\_\_\_delete |
| requestFile - Delete uploaded file | requestFile Delete an uploaded file Delete uploaded file  | /rest/requestFile/{ref}/uploads/{object\_id}\_\_\_delete |
| requestFile - Expire the request file | requestFile Expire the request file by setting deleted to true Expire the request file  | /rest/requestFile/{ref}\_\_\_delete |
| sources - Deletes specified source | sources Deletes specified source. Deletes specified source  | /rest/sources/{id}\_\_\_delete |
| tasks - Delete a Task | tasks Deletes a task Delete a Task  | /rest/tasks/{id}\_\_\_delete |
| uploads - Terminate chunk upload session | uploads Terminates chunk upload session Terminate chunk upload session  | /rest/uploads/{id}\_\_\_delete |
| users - Deletes a profile image | users Deletes an image file to use as profile image Deletes a profile image  | /rest/users/{id}/profileImage\_\_\_delete |
| users - Deletes a User | users Mark the specified user as deleted. This user will still be returned in the GET Users query. Deletes a User  | /rest/users/{id}\_\_\_delete |
| users - Deletes Users | users Mark the specified users as deleted.                   These users will still be returned in the GET Users query. Deletes Users  | /rest/users\_\_\_delete |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| admin - Get a client | admin Return the admin settings of a specified client.     e.g.: I want the admin settings for my iOS client; pin timeout, token lifetime, etc.,  Get a client  | /rest/admin/clients/{id}\_\_\_get |
| admin - Get a device | admin Return the details of a specified device. Given the device id,     return the device name, install tag id, client id, and remote wipe flag. Get a device  | /rest/admin/devices/{id}\_\_\_get |
| admin - Get a tenant | admin Returns the details of a specified tenant. Get a tenant  | /rest/admin/tenants/{id}\_\_\_get |
| admin - Get the wipe status | admin Returns the remote wipe status of a specified device identified by install_tag_id Get the wipe status  | /rest/admin/devices/{install\_tag\_id}/wipe\_\_\_get |
| admin - Get User | admin Returns the details of the specified user (this includes email address and name) Get User  | /rest/admin/users/{id}\_\_\_get |
| admin - Get User Settings | admin Returns the user settings Get User Settings  | /rest/admin/users/{id}/settings\_\_\_get |
| admin - Gets an LDAP group | admin Returns the details of a specified LDAP group. This includes the settings from the admin. Gets an LDAP group  | /rest/admin/ldapGroups/{id}\_\_\_get |
| admin - List all added sources | admin Returns a list of all available ECM sources. List all added sources  | /rest/admin/sources\_\_\_get |
| admin - List locations | admin Returns a list of available kiteworks locations.     Locations are logical collection of multiple kiteworks servers,     usually with a common geography or particular purpose. List locations  | /rest/admin/locations\_\_\_get |
| admin - List scopes | admin Return a list of scopes for all clients.     This means to return all API calls that are available     in the kiteworks system that a client could use. List scopes  | /rest/admin/scopes\_\_\_get |
| admin - List scopes of a client | admin Return a list of scopes for a specific client.     This returns the methods that this client is allowed to use     such as folders, files, members, comments, etc. List scopes of a client  | /rest/admin/clients/{client\_id}/scopes\_\_\_get |
| admin - Return admin roles of the specified user id. | admin Returns the details of all adminroles (active and deleted users)     with the specified user id. Return admin roles of the specified user id.  | /rest/admin/users/{id}/adminRoles\_\_\_get |
| admin - Return an user type | admin Returns the details of a specified user type. e.g.: Returns user type id and name. Return an user type  | /rest/admin/profiles/{id}\_\_\_get |
| admin - Return location name. | admin Returns the name of a specified location. Return location name.  | /rest/admin/locations/{id}\_\_\_get |
| admin - Returns requested assigned ECM source | admin Returns requested assigned source. Returns requested assigned ECM source  | /rest/admin/sources/{id}\_\_\_get |
| adminRoles - List admin roles | adminRoles Returns a list of admin roles supported by kiteworks. e.g.:                   Today we return Application and System List admin roles  | /rest/adminRoles\_\_\_get |
| adminRoles - Return an admin role | adminRoles Returns the details of a specified admin role. e.g.: Returns id and admin role name. Return an admin role  | /rest/adminRoles/{id}\_\_\_get |
| clients - Get a client | clients Return the admin settings of a specified client.                e.g.: I want the admin settings for my iOS client; pin timeout, token lifetime, etc.,  Get a client  | /rest/clients/{id}\_\_\_get |
| clients - Get current client | clients Return the settings of the current client.                e.g.: I want the admin settings for my client; pin timeout, token lifetime, etc.,  Get current client  | /rest/clients/me\_\_\_get |
| clients - List scopes of a client | clients Return a list of scopes for a specific client.     This returns the methods that this client is allowed to use     such as folders, files, members, comments, etc. List scopes of a client  | /rest/clients/{id}/scopes\_\_\_get |
| comments - Get Comment by ID | comments Returns the details of a comment based on ID. e.g.:                I want to know the file or folder this comment is attached to and the comment itself. Get Comment by ID  | /rest/comments/{id}\_\_\_get |
| comments - Return the list of permissions available on a comm... | comments permissions Return available permissions Return the list of permissions available on a comment  | /rest/permissions/comment/{comment\_id}\_\_\_get |
| contacts - Get a contact | contacts Returns the details of a specified kiteworks contact. e.g.:                   I want the email address and name of this contact. Get a contact  | /rest/contacts/{id}\_\_\_get |
| containers - Get the specified container | containers Return the details of the specified container. Get the specified container  | /rest/containers/{id}\_\_\_get |
| devices - Get a device | devices Return the details of a specified device. Given the device id,                   return the device name, install tag id, client id, and remote wipe flag. Get a device  | /rest/devices/{id}\_\_\_get |
| devices - Get current device info | devices Return the details of the current device. Get current device info  | /rest/devices/me\_\_\_get |
| devices - Get the wipe status | devices Returns the remote wipe status of a specified device identified by install_tag_id Get the wipe status  | /rest/devices/{install\_tag\_id}/wipe\_\_\_get |
| devices - List devices for a user | devices Return a list of devices for a specific user List devices for a user  | /rest/users/{id}/devices\_\_\_get |
| dli - Allow DLI admin to get specified version | dli Returns the specified version of a file Allow DLI admin to get specified version  | /rest/dli/files/{id}/versions/{version\_id}\_\_\_get |
| dli - Download specified version | dli Download specified version Download specified version  | /rest/dli/files/{id}/versions/{version\_id}/content\_\_\_get |
| dli - Gets the file path by its ID | dli Gets the file path by its ID Gets the file path by its ID  | /rest/dli/files/{id}/path\_\_\_get |
| dli - Read file content | dli Read content of file Read file content  | /rest/dli/files/{id}/content\_\_\_get |
| dli - Retrieve folder information. | dli Return folder information for the specified folder. e.g.:                   I want to know creation date, get a link to the folder,                   find out if the folder was deleted, etc. Retrieve folder information.  | /rest/dli/folders/{id}\_\_\_get |
| dli - Retrieve information about the file preview. | dli Retrieve information about the file preview. Retrieve information about the file preview.  | /rest/dli/files/{id}/preview\_\_\_get |
| dli - Retrieve information about the file specified. | dli Retrieve information about the file specified. This includes file name,                created date, modified date, file deleted, locked, fingerprint, and links to file,                folder, and owner. Retrieve information about the file specified.  | /rest/dli/files/{id}\_\_\_get |
| files - display group member | files Display the specified group member in the file display group member  | /rest/files/{id}/members/ldapGroup/{ldapGroupId}\_\_\_get |
| files - display member | files Display the specified member in the file display member  | /rest/files/{id}/members/{member\_id}\_\_\_get |
| files - download specified version | files download specified version download specified version  | /rest/files/{id}/versions/{version\_id}/content\_\_\_get |
| files - Get specified version | files Returns the specified version of a file Get specified version  | /rest/files/{id}/versions/{version\_id}\_\_\_get |
| files - Gets the file path by its ID | files Gets the file path by its ID Gets the file path by its ID  | /rest/files/{id}/path\_\_\_get |
| files - Read EC file content | files Read content of an EC file Read EC file content  | /rest/sources/{id}/content\_\_\_get |
| files - Read file content | files Read content of file Read file content  | /rest/files/{id}/content\_\_\_get |
| files - Retrieve information about the file preview. | files Retrieve information about the file preview. Retrieve information about the file preview.  | /rest/files/{id}/versions/{version\_id}/preview\_\_\_get |
| files - Retrieve information about the file preview. (DEPR... | files Retrieve information about the file preview. Retrieve information about the file preview. (DEPRECATED in 10.1)  | /rest/files/{id}/preview\_\_\_get |
| files - Retrieve information about the file specified. | files Retrieve information about the file specified. This includes file name, created date,                        modified date, file deleted, locked, fingerprint, and links to file, folder, and owner. Retrieve information about the file specified.  | /rest/files/{id}\_\_\_get |
| files - Return the list of permissions available on a file | files permissions Return available permissions Return the list of permissions available on a file  | /rest/permissions/file/{file\_id}\_\_\_get |
| folders - Display group member | folders Display the specified group member in the folder Display group member  | /rest/folders/{id}/members/ldapGroup/{ldapGroupId}\_\_\_get |
| folders - Display member | folders Display the specified member in the folder Display member  | /rest/folders/{id}/members/{member\_id}\_\_\_get |
| folders - Gets the folder path by its ID (DEPRECATED in 5) | folders Gets the folder path by its ID Gets the folder path by its ID (DEPRECATED in 5)  | /rest/folders/{id}/path\_\_\_get |
| folders - Retrieve folder information. | folders Return folder information for the specified folder. e.g.:                   I want to know creation date, get a link to the folder,                   find out if the folder was deleted, etc. Retrieve folder information.  | /rest/folders/{id}\_\_\_get |
| folders - Return the list of permissions available on a fold... | folders permissions Return available permissions Return the list of permissions available on a folder  | /rest/permissions/folder/{folder\_id}\_\_\_get |
| groups - Return users for the specified group | groups Returns the list of users for the specified group. Return users for the specified group  | /rest/groups/{id}\_\_\_get |
| jobs - Retrieve information about scheduled job in queue. | jobs Return job information for the specified job. e.g.:                   I want to check job status for copy EC file from tray Retrieve information about scheduled job in queue.  | /rest/jobs/{id}\_\_\_get |
| languages - Get a language | languages Returns the details of a specified language including language symbol, name, and link. Get a language  | /rest/languages/{id}\_\_\_get |
| languages - List languages | languages Returns a list of available languages. List languages  | /rest/languages\_\_\_get |
| ldapGroups - Gets an LDAP group | ldapGroups Returns the details of a specified LDAP group.                   This includes the settings from the admin. Gets an LDAP group  | /rest/ldapGroups/{id}\_\_\_get |
| locations - List locations | locations Returns a list of available kiteworks locations.                   Locations are logical collection of multiple kiteworks servers,                   usually with a common geography or particular purpose. List locations  | /rest/locations\_\_\_get |
| locations - Return location name. | locations Returns the name of a specified location. Return location name.  | /rest/locations/{id}\_\_\_get |
| mail - Get an email entry | mail Returns the details of a specified email which includes sender id, date, package id, etc. Get an email entry  | /rest/mail/{id}\_\_\_get |
| mail - Retrieve information about the file preview of giv... | mail Retrieve information about the file preview of given email. Retrieve information about the file preview of given email.  | /rest/mail/{emailId}/attachments/{id}/preview\_\_\_get |
| mail - Return attachment content | mail Returns the content of the specified attachment.                        e.g: return the file in this email attachment. Return attachment content  | /rest/mail/{emailId}/attachments/{id}/content\_\_\_get |
| mail - Return details of attachment | mail Returns the attachment details for a given mail. Return details of attachment  | /rest/mail/{emailId}/attachments/{id}\_\_\_get |
| permissions - Return the list of permissions available on a task | permissions tasks Return available permissions Return the list of permissions available on a task  | /rest/permissions/task/{task\_id}\_\_\_get |
| profiles - Return user type details | profiles Returns the details of a specified user type. e.g.: Returns user type id and name. Return user type details  | /rest/profiles/{id}\_\_\_get |
| requestFile - Read file info | requestFile Read properties of a source file Read file info  | /rest/requestFile/{ref}/sources/{object\_id}\_\_\_get |
| requestFile - Read uploaded file info | requestFile Read properties of a uploaded file Read uploaded file info  | /rest/requestFile/{ref}/uploads/{object\_id}\_\_\_get |
| requestFile - Retrieve information about the file preview. | requestFile Retrieve information about the file preview. Retrieve information about the file preview.  | /rest/requestFile/{ref}/preview/{object\_id}\_\_\_get |
| requestFile - Retrieve Request File attached files included by t... | requestFile Returns attached files included by the requester. Retrieve Request File attached files included by the requester  | /rest/requestFile/{ref}/sources\_\_\_get |
| requestFile - Retrieve Request File information | requestFile Returns Request File information such as uploaded files, source files, requester, etc. Retrieve Request File information  | /rest/requestFile/{ref}\_\_\_get |
| requestFile - Retrieve Request File uploaded files by the login ... | requestFile Returns uploaded files by the login uploader. Retrieve Request File uploaded files by the login uploader  | /rest/requestFile/{ref}/uploads\_\_\_get |
| roles - Get a role | roles Returns the details of a specified role including the mask that indicates                   what functionality this role is allowed to do. Get a role  | /rest/roles/{id}\_\_\_get |
| roles - List user roles | roles Returns a list of available user roles. e.g.:                    kiteworks default roles are Manager, Collaborator, Downloader, Viewer, Uploader. List user roles  | /rest/roles\_\_\_get |
| scopes - List scopes | scopes Return a list of scopes for all clients.                   This means to return all API calls that are available                   in the kiteworks system that a client could use. List scopes  | /rest/scopes\_\_\_get |
| shortLinks - Short Link | shortLinks Returns properties of a short link including the associated entity ID. Short Link  | /rest/shortLinks/{ref}\_\_\_get |
| sources - Get info of an EC file. | sources Get info of an EC file. Get info of an EC file.  | /rest/sources/files/{id}\_\_\_get |
| sources - List sources for current user | sources Returns a list of available ECM sources for current user. List sources for current user  | /rest/sources\_\_\_get |
| sources - Returns requested ECM source | sources Returns requested source. Returns requested ECM source  | /rest/sources/{id}\_\_\_get |
| sources - Returns the list of files of the specified ECM fol... | sources Returns the list of files of the requested ECM folder. Returns the list of files of the specified ECM folder  | /rest/sources/{parent}/files\_\_\_get |
| sources - Returns the list of folders of the specified ECM f... | sources Returns the content of the requested source. Returns the list of folders of the specified ECM folder  | /rest/sources/{id}/folders\_\_\_get |
| sourceTypes - List all ECM source types | sourceTypes Returns a list of available ECM source types. List all ECM source types  | /rest/sourceTypes\_\_\_get |
| sourceTypes - Returns requested ECM source type | sourceTypes Returns requested source type. Returns requested ECM source type  | /rest/sourceTypes/{id}\_\_\_get |
| tasks - Get Task by ID | tasks Returns the details of a task based on ID Get Task by ID  | /rest/tasks/{id}\_\_\_get |
| templates - Get a template | templates Returns the details of a specified template which includes body,                   button link and button text. Get a template  | /rest/templates/{id}\_\_\_get |
| templates - List Email templates | templates Returns a list of available Email templates. List Email templates  | /rest/templates\_\_\_get |
| tenants - Get a tenant | tenants Returns the details of a specified tenant. Get a tenant  | /rest/tenants/{id}\_\_\_get |
| timezones - Get a timezone | timezones Returns the details of a specified timezone. Get a timezone  | /rest/timezones/{id}\_\_\_get |
| timezones - List timezones | timezones Returns a list of timezones supported by kiteworks List timezones  | /rest/timezones\_\_\_get |
| uploads - Get Upload | uploads Returns the details of the specified upload Get Upload  | /rest/uploads/{id}\_\_\_get |
| uploads - Get upload server configuration | uploads Returns the details of the upload server configuration Get upload server configuration  | /rest/uploads/config\_\_\_get |
| users - Get current logged in User | users Returns the details of the current user (this includes email address and name) Get current logged in User  | /rest/users/me\_\_\_get |
| users - Get User | users Returns the details of the specified user (this includes email address and name) Get User  | /rest/users/{id}\_\_\_get |
| users - Get User Settings | users Returns the user settings Get User Settings  | /rest/users/{id}/settings\_\_\_get |
| users - Return admin roles of the specified user id. | users Returns the details of all adminroles                   (active and deleted users) with the specified user id. Return admin roles of the specified user id.  | /rest/users/{id}/adminRoles\_\_\_get |
| web_forms - Get a web form | web_forms Returns the details of a specified web form Get a web form  | /rest/webForms/{id}\_\_\_get |
| web_forms - List web forms | web_forms Returns a list of web forms available to current user. List web forms  | /rest/webForms\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| activities - Return the list of Activities for this file | activities Return the list of Activities for this file. Return the list of Activities for this file  | /rest/files/{file\_id}/activities\_\_\_get |
| activities - Return the list of Activities for this folder | activities Return the list of Activities for this folder. Return the list of Activities for this folder  | /rest/folders/{folder\_id}/activities\_\_\_get |
| activities - Return the list of all Activities | activities Return the list of all Activities. Return the list of all Activities  | /rest/activities\_\_\_get |
| admin - Get a list of Users | admin Returns a list of Users in the system.     This call will only work for users with admin access. Get a list of Users  | /rest/admin/users\_\_\_get |
| admin - List clients | admin Return a list of clients List clients  | /rest/admin/clients\_\_\_get |
| admin - List devices | admin Return a list of devices. e.g.: Returns a list of devices, per user,     that have authenticated on this server.     The list includes the name of the device (iPad, iPhone, etc),     install tag id, client id, and remote wipe flag. List devices  | /rest/admin/devices\_\_\_get |
| admin - List devices for a user | admin Return a list of devices for a specific user List devices for a user  | /rest/admin/users/{id}/devices\_\_\_get |
| admin - List tenants | admin Returns a list of tenants List tenants  | /rest/admin/tenants\_\_\_get |
| admin - List user types | admin Returns a list of user types supported by kiteworks. e.g.:     Today we return Standard and Restricted List user types  | /rest/admin/profiles\_\_\_get |
| admin - Return list of users with the specified types | admin Returns the list of users who have the specified type.     e.g.: Return me the user names and email addresses of all the Restricted Users. Return list of users with the specified types  | /rest/admin/profiles/{id}/users\_\_\_get |
| admin - Returns a list of LDAP groups. | admin Returns a list of LDAP groups that have been enabled through the kiteworks admin. Returns a list of LDAP groups.  | /rest/admin/ldapGroups\_\_\_get |
| adminRoles - Return list of users with the specified admin role | adminRoles Returns the list of admins who have the specified role. e.g.:                   Return me the user names and email addresses of all the Application admins. Return list of users with the specified admin role  | /rest/adminRoles/{id}/users\_\_\_get |
| clients - List clients | clients Return a list of clients List clients  | /rest/clients\_\_\_get |
| clients - List of top level containers of a client | clients Return a list of top level containers for a specific client. List of top level containers of a client  | /rest/clients/{id}/containers\_\_\_get |
| comments - Get Comments for a file | comments files Returns all comments for a file Get Comments for a file  | /rest/files/{id}/comments\_\_\_get |
| comments - Return the list of Comments for this folder | comments folders Returns all comments made on this folder. Return the list of Comments for this folder  | /rest/folders/{id}/comments\_\_\_get |
| contacts - List contacts | contacts Returns a list of contacts in my contacts list in kiteworks. List contacts  | /rest/contacts\_\_\_get |
| containers - List children containers of specified parent | containers Return the list of containers in the specified container including its metadata. List children containers of specified parent  | /rest/containers/{parent\_id}/containers\_\_\_get |
| containers - List children folders of specified parent containe... | containers Return the list of folders in the specified container including its metadata. List children folders of specified parent container  | /rest/containers/{parent\_id}/folders\_\_\_get |
| devices - List devices | devices Return a list of devices. e.g.: Returns a list of devices, per user,                   that have authenticated on this server.                   The list includes the name of the device (iPad, iPhone, etc),                   install tag id, client id, and remote wipe flag. List devices  | /rest/devices\_\_\_get |
| dli - List Recipients | dli Returns a list of recipients for a given email List Recipients  | /rest/dli/mail/{id}/recipients\_\_\_get |
| dli - List versions | dli Returns a list of versions for a given file List versions  | /rest/dli/files/{id}/versions\_\_\_get |
| dli - Retrieve information about the mail specified be a... | dli Retrieve information about the mail specified by one user Retrieve information about the mail specified be a user  | /rest/dli/users/{id}/mail\_\_\_get |
| dli - Return the list of Activities for this file | dli Return the list of Activities for this file. Return the list of Activities for this file  | /rest/dli/files/{fileId}/users/{userId}/activities\_\_\_get |
| dli - Return the list of activities for this folder of t... | dli Return the list of activities for this folder of the specified user Return the list of activities for this folder of the specified user  | /rest/dli/folders/{folderId}/users/{userId}/activities\_\_\_get |
| dli - Return the list of all activities of the specified... | dli Return the list of all activities of the specified user Return the list of all activities of the specified user  | /rest/dli/users/{id}/activities\_\_\_get |
| favorites - List favorites | favorites Returns list of favorites List favorites  | /rest/favorites\_\_\_get |
| files - Get Tasks for a file | files tasks Returns all tasks for a file Get Tasks for a file  | /rest/files/{id}/tasks\_\_\_get |
| files - List members | files Returns a list of members in with access to the file List members  | /rest/files/{id}/members\_\_\_get |
| files - List members | files Returns a list of group members in with access to the file List members  | /rest/files/{id}/members/ldapGroup\_\_\_get |
| files - List versions | files Returns a list of versions for a given file List versions  | /rest/files/{id}/versions\_\_\_get |
| files - Return the list of files in a folder. | files folders Returns the list of files in the specified folder. Return the list of files in a folder.  | /rest/folders/{parent}/files\_\_\_get |
| folders - Get Tasks for a folder | folders tasks Returns all tasks for a folder Get Tasks for a folder  | /rest/folders/{id}/tasks\_\_\_get |
| folders - List children folders of specified parent | folders Return the list of folders in the specified folder including its metadata. List children folders of specified parent  | /rest/folders/{parent\_id}/folders\_\_\_get |
| folders - List LDAP group members | folders Returns a list of LDAP group members in the folder.                        This returns LDAP name, email, and privileges. List LDAP group members  | /rest/folders/{id}/members/ldapGroup\_\_\_get |
| folders - List members | folders Returns a list of members in the folder.                   This returns member name, email, and privileges. List members  | /rest/folders/{id}/members\_\_\_get |
| folders - List top level shared folders | folders Return the list of top level shared folders. List top level shared folders  | /rest/folders/shared\_\_\_get |
| groups - List kiteworks groups | groups Returns the list of kiteworks groups for this user,                   including the name and email address of each member in the group. List kiteworks groups  | /rest/groups\_\_\_get |
| ldapGroups - Returns a list of LDAP groups. | ldapGroups Returns a list of LDAP groups that have been enabled through the kiteworks admin. Returns a list of LDAP groups.  | /rest/ldapGroups\_\_\_get |
| mail - List email attachments | mail Returns a list of attachments for a given mail.                        e.g.: I want to the id's of all the attachments for a mail I sent. List email attachments  | /rest/mail/{emailId}/attachments\_\_\_get |
| mail - List email packages | mail Returns a list of packages for an email List email packages  | /rest/mail/{id}/packages\_\_\_get |
| mail - List emails | mail Returns the list of all emails for this user. This includes sent emails, received emails,                   draft emails, and request a file emails. List emails  | /rest/mail\_\_\_get |
| mail - List recipients | mail Returns a list of recipients for a given email List recipients  | /rest/mail/{id}/recipients\_\_\_get |
| notifications - List notifications | notifications Returns list of notifications List notifications  | /rest/notifications\_\_\_get |
| profiles - List user types | profiles Returns a list of user types supported by kiteworks List user types  | /rest/profiles\_\_\_get |
| profiles - Return list of users with the specified type | profiles Returns the list of users who have the specified type. e.g.:                   Return me the user names and email addresses of all the Restricted Users. Return list of users with the specified type  | /rest/profiles/{id}/users\_\_\_get |
| tenants - Get a tenant's bandwidth usage | tenants Returns the bandwidth usage of a specified tenant. Get a tenant's bandwidth usage  | /rest/tenants/{id}/actions/bandwidth\_\_\_get |
| tenants - Get a tenant's storage usage | tenants Returns the storage usage of a specified tenant. Get a tenant's storage usage  | /rest/tenants/{id}/actions/storage\_\_\_get |
| tenants - List tenants | tenants Returns a list of tenants List tenants  | /rest/tenants\_\_\_get |
| users - Get a list of Users | users Returns a list of Users in the system.                   This call will only work for users with admin access. Get a list of Users  | /rest/users\_\_\_get |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| admin - Change a user type (DEPRECATED in 4) (PUT) | admin Chane user type. The user type is either standard or restrict. Change a user type (DEPRECATED in 4)  | /rest/admin/profiles/{id}/users/{user\_id}\_\_\_put |
| admin - Update a client (PUT) | admin Update the admin settings for the specified client Update a client  | /rest/admin/clients/{id}\_\_\_put |
| admin - Update a device (PUT) | admin Update the details of a device. Can change the mobile key store and messaging registration token. Update a device  | /rest/admin/devices/{id}\_\_\_put |
| admin - Update User (PUT) | admin Updates the details of a user.     e.g.: Change their name, set as deleted, set as active or inactive, etc. Update User  | /rest/admin/users/{id}\_\_\_put |
| admin - Updates an LDAP group (PUT) | admin Updates an LDAP group Updates an LDAP group  | /rest/admin/ldapGroups/{id}\_\_\_put |
| admin - Updates details of the specified ECM source (PUT) | admin Updates requested source. Updates details of the specified ECM source  | /rest/admin/sources/{id}\_\_\_put |
| adminRoles - Promote a user as admin role (PUT) | adminRoles Promote a user to include an admin role. The user will become a system or application admin. Promote a user as admin role  | /rest/adminRoles/{id}/users/{user\_id}\_\_\_put |
| clients - Update a client (PUT) | clients Update the admin settings for the specified client Update a client  | /rest/clients/{id}\_\_\_put |
| comments - Update Comment (PUT) | comments Updates the text of a previously entered comment. Update Comment  | /rest/comments/{id}\_\_\_put |
| contacts - Update a contact entry (PUT) | contacts Update a kiteworks contact. The email address and name can be changed. Update a contact entry  | /rest/contacts/{id}\_\_\_put |
| devices - Tell the server the device had completed the reque... (PATCH) | devices Update the wipe flag to completed Tell the server the device had completed the requested wipe  | /rest/devices/me/actions/wipe\_\_\_patch |
| devices - Update a device (PUT) | devices Update the details of a device. Can change the mobile key store and messaging registration token. Update a device  | /rest/devices/{id}\_\_\_put |
| devices - Update the currently signed-in device (PUT) | devices Update the details of the current device. Can change the mobile key store and messaging registration token. Update the currently signed-in device  | /rest/devices/me\_\_\_put |
| files - Create a Comment on a file (POST) | files requestFile Create a Comment on a file. Create a Comment on a file  | /rest/requestFile/{ref}/comment/{object\_id}\_\_\_post |
| files - Locks multiple files by IDs (PATCH) | files Lock the given files from access.                   Other users will not be able to add a new versions of this files                   until they are unlocked by the owner. Locks multiple files by IDs  | /rest/files/actions/lock\_\_\_patch |
| files - Locks the file by its ID (PATCH) | files Lock the given file from access.                   Other users will not be able to add a new version of this file                   until it is unlocked by the owner. Locks the file by its ID  | /rest/files/{id}/actions/lock\_\_\_patch |
| files - Recovers deleted file. (PATCH) | files Recover deleted file by its ID. This marks an already deleted file as not deleted. Recovers deleted file.  | /rest/files/{id}/actions/recover\_\_\_patch |
| files - Returns file to EC source (PATCH) | files Returns file to EC source Returns file to EC source  | /rest/files/{id}/actions/return\_\_\_patch |
| files - Unlocks multiple files by IDs (PATCH) | files Unlock the given files.                   This will allow other users in the folder to upload a new versions of files. Unlocks multiple files by IDs  | /rest/files/actions/unlock\_\_\_patch |
| files - Unlocks the file by its ID (PATCH) | files Unlock the given file.                   This will allow other users in the folder to upload a new version of the file. Unlocks the file by its ID  | /rest/files/{id}/actions/unlock\_\_\_patch |
| files - Update file (PUT) | files Updates the file details. Update file  | /rest/files/{id}\_\_\_put |
| files - upload new version (DEPRECATED in 8) (POST) | files uploads new file version upload new version (DEPRECATED in 8)  | /rest/files/{id}\_\_\_post |
| folders - Recover deleted folder. (PATCH) | folders Recover the specified folder. This un-marks the folder for deletion. Recover deleted folder.  | /rest/folders/{id}/actions/recover\_\_\_patch |
| folders - update group members (PUT) | folders updates group members in the folder update group members  | /rest/folders/{id}/members/ldapGroup/{ldapGroupId}\_\_\_put |
| folders - update members (PUT) | folders updates members in the folder update members  | /rest/folders/{id}/members/{member\_id}\_\_\_put |
| folders - Update metadata about the folder. (PUT) | folders Update information about the specified folder such as folder name,                   description, and whether this folder is syncable or not. Update metadata about the folder.  | /rest/folders/{id}\_\_\_put |
| groups - Update a group entry (PUT) | groups Update a group Update a group entry  | /rest/groups/{id}\_\_\_put |
| ldapGroups - Updates an LDAP group (PUT) | ldapGroups Updates an LDAP group Updates an LDAP group  | /rest/ldapGroups/{id}\_\_\_put |
| mail - Update the sendfile email entry for sending out by... (PUT) | mail Updates the sendfile email entry for sending out by an external client Update the sendfile email entry for sending out by an external client  | /rest/mail/{id}/actions/sendFileExternal\_\_\_put |
| mail - Updates a sendfile email entry (PUT) | mail Updates a sendfile email entry Updates a sendfile email entry  | /rest/mail/{id}/actions/sendFile\_\_\_put |
| notifications - Add notification option (DEPRECATED in 5) (POST) | notifications Adding notification option Add notification option (DEPRECATED in 5)  | /rest/notifications/{object\_id}\_\_\_post |
| notifications - Update notification options (DEPRECATED in 5) (PUT) | notifications Update existing notification options Update notification options (DEPRECATED in 5)  | /rest/notifications/{object\_id}\_\_\_put |
| profiles - Change user type (DEPRECATED in 4) (PUT) | profiles Change user type for specified user Change user type (DEPRECATED in 4)  | /rest/profiles/{id}/users/{user\_id}\_\_\_put |
| profiles - Update profile (DEPRECATED in 4) (PUT) | profiles Update existing profile Update profile (DEPRECATED in 4)  | /rest/profiles/{id}\_\_\_put |
| sources -  (DEPRECATED in 7.1) (PATCH) | sources Lock an EC file.  (DEPRECATED in 7.1)  | /rest/sources/{id}/actions/lock\_\_\_patch |
| sources - Unlock an EC file. (DEPRECATED in 7.1) (PATCH) | sources Unlock an EC file. Unlock an EC file. (DEPRECATED in 7.1)  | /rest/sources/{id}/actions/unlock\_\_\_patch |
| tasks - Update Task (PUT) | tasks Updates the details of a task Update Task  | /rest/tasks/{id}\_\_\_put |
| uploadChunk - upload chunk to the upload entity (POST) | uploadChunk uploads chunk to the upload entity upload chunk to the upload entity  | /{host\_name}/rest/uploads/{id}\_\_\_post |
| users - Update User (DEPRECATED in 5) (PUT) | users Updates the details of a user. e.g.: Change their name, set as deleted,                    set as active or inactive, etc. Update User (DEPRECATED in 5)  | /rest/users/{id}\_\_\_put |
| users - Update User Settings (PUT) | users Updates the settings for the specified user Update User Settings  | /rest/users/{id}/settings\_\_\_put |


