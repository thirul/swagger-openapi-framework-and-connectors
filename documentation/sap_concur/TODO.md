### SAP Concur

##TODO Entity Pagination Tuning
Concur is *very* inconsistent in regard to pagination parameters. 
 1. Some swaggers have a string offset query parameter and a NextPage in the response (Users). This seems to be the behavior of the v3.0 apis.
 1. Some swaggers have no offset query parameter but do have a NextPage in the response (Opportunities). This is v3 so I guess v3 all have NextOffset but might not have an offset param.
 1. Some have an integer offset query parameter and no NextPage (Budgets). In Budgets case there is a next url in the response but it just has the offset. Seems to be the v4 apis?
 
The following should be analyzed

 * budget - this one is a mess as it has queries without pagination parameters and the response array is at root, not at /Items/* like other apis
 	* /budget/v4/budgetCategory No parameters, returns array without pagination param
 	* /budget/v4/budgetCategory/expenseTypes has userlanguage query param but no pagination params
 	* /budget/v4/budgetItemHeader has offset query param but no limit...
 	* /budget/v4/budgetTrackingField no parameters but returns array with no pagination params
 	* /budget/v4/fiscalYear has query filter params like lastModified query param but no pagination params
 * opportunities 
 	* /insights/opportunities has query filter params but no pagination params
 	* array at root, not at Items*
 * purchaseorderreceipts
 	* /invoice/purchaseorderreceipts no offset/limit query params but does have NextPage in the response
 * suppliers
 	* /common/suppliers no offset/limit query params but does have NextPage in the response
 
One solution is to specify the paths/objectids that are query but don't have offset, in isQuery. We may have to have to solve this with a cookie for each swagger specifying how to implement pagination...


Concur supports the following objects and operations. Each object type has its own swagger file. Query supports simple filtering with an equals operation but no other operation. Sorting and field selection is not supported.

| Object | Query | Get | Create | Update | Delete |
| --- |:---:|:---:|:---:|:---:|:---:|
| Allocations v3 | X | X |  |  |  |
| Attendees v3 | X | X | X | X | X |
| AttendeeTypes v3 | X | X | X | X | X |
| Budget v4 | X | X | X |  | X |
| ConcurRequest v4 | X | X | X | X | X |
| ConnectionRequests v3 | X | X | X | X | X |
| ConnectionRequests v3 | X | X | X | X |  |
| DigitalTaxInvoices v3 | X | X |  | X |  |
| Entries v3 | X | X | X | X | X |
| EntryAttendeeAssociations v3 | X | X | X | X | X |
| ExpenseGroupConfigurations v3 | X | X |  |  |  |
| InvoicePay v4 | X |  |  | X |  |
| Itemizations v3 | X | X | X | X | X |
| LatestBookings v3 | X |  |  |  |  |
| List v4 | X | X | X | X | X |
| ListItem v4 | X | X | X | X | X |
| ListItems v3 | X | X | X | X | X |
| Lists v3 | X | X | X | X |  |
| LocalizedData v3 | X |  |  |  |  |
| Locations v3 | X | X |  |  |  |
| Opportunities v3 | X |  |  |  |  |
| PaymentRequest v3 |  | X | X | X |  |
| PaymentRequestDigest v3 | X | X |  |  |  |
| PurchaseOrderReceipts v3 | X |  | X | X | X |
| PurchaseOrders v3 |  | X | X | X |  |
| PurchaseRequest v4 | X |  | X |  |  |
| QuickExpenses v4 |  |  | X |  |  |
| ReceiptImages v3 | X | X | X | X | X |
| Receipts v3 |  |  | X |  |  |
| Reports v3 | X | X | X | X |  |
| RequestGroupConfigurations v3 | X |  |  |  |  |
| SalesTaxValidationRequest v3 | X |  |  | X |  |
| Suppliers v3 | X | X |  |  |  |
| Users v3 | X |  |  |  |  |
| VendorBank v3 |  |  |  | X |  |
| VendorGroup v3 |  |  |  | X | X |
| Vendors v3 | X |  | X | X | X |
