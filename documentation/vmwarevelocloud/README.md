# VMWare VeloCloud Orchestration API
VMWare VeloCloud Orchestration API

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string

**Default Value** - https://{subdomain}.velocloud.net



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password

# Operation Tab


## Execute
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Configuration Clone And Convert Configuration - Clone and convert a network based profile configur... | configuration all Clones an convert existing network configuration by configurationId. Accepts an enterpriseId or networkId to associate the new config with an enterprise or network. On success, returns an object the ID of the newly created configuration object.  Privileges required:  `CREATE` `ENTERPRISE_PROFILE`, or  `CREATE` `OPERATOR_PROFILE` Clone and convert a network based profile configuration to segment based profile configuration  |
| Configuration Clone Configuration - Clone configuration profile | configuration all Clones the specified configuration (by `configurationId`) and all associated configuration modules. Accepts an `enterpriseId` or `networkId` to associate the new configuration with an enterprise or network. Select modules may also be specified. On success, returns the `id` of the newly created configuration object.  Privileges required:  `CREATE` `ENTERPRISE_PROFILE`, or  `CREATE` `OPERATOR_PROFILE` Clone configuration profile  |
| Configuration Clone Enterprise Template - Clone default enterprise configuration profile | all configuration Creates a new enterprise configuration from the enterprise default configuration. On success, returns the `id` of the newly created configuration object.  Privileges required:  `CREATE` `ENTERPRISE_PROFILE`, or  `CREATE` `OPERATOR_PROFILE` Clone default enterprise configuration profile  |
| Configuration Delete Configuration - Delete a configuration profile | all configuration Delete an existing configuration profile. On success, returns an object indicating the number of rows deleted.  Privileges required:  `DELETE` `ENTERPRISE_PROFILE`, or  `DELETE` `OPERATOR_PROFILE` Delete a configuration profile  |
| Configuration Get Configuration - Get a configuration profile | all configuration Get a configuration profile, optionally with module detail.  Privileges required:  `READ` `ENTERPRISE_PROFILE`, or  `READ` `OPERATOR_PROFILE` Get a configuration profile  |
| Configuration Get Configuration Modules - List the modules that compose a configuration prof... | configuration all Retrieve a list of the configuration modules that compose the given configuration profile.  Privileges required:  `READ` `ENTERPRISE_PROFILE`, or  `READ` `OPERATOR_PROFILE` List the modules that compose a configuration profile  |
| Configuration Get Routable Applications - Get first packet routable applications | all configuration Gets all applications that are first packet routable. If called from an operator or MSP context, then `enterpriseId` is required. Optionally, specify `edgeId` to get the map for a specific Edge.  Privileges required:  `VIEW_FLOW_STATS` `undefined` Get first packet routable applications  |
| Configuration Insert Configuration Module - Insert a new configuration module | configuration all Insert a new configuration module into the given configuration profile.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE`, or  `UPDATE` `OPERATOR_PROFILE` Insert a new configuration module  |
| Configuration Update Configuration Module - Update a configuration module | configuration all Update an existing configuration module with the data. module data contained in the _update object.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE`, or  `UPDATE` `OPERATOR_PROFILE` Update a configuration module  |
| Disaster Recovery Configure Active For Replication - Designate a standby Orchestrator for disaster reco... | all disasterRecovery Configure the current Orchestrator to be active and the specified Orchestrator to be standby for Orchestrator disaster recovery replication. Required attributes are 1) standbyList, a single-entry array containing the standbyAddress and standbyUuid, 2) drVCOUser, a Orchestrator super user available on both the active and standby VCOs, and 3) drVCOPassword, the password of drVCOUser on the standby Orchestrator (unless the autoConfigStandby option is specified as false). The call sets up the active Orchestrator to allow replication from the standby and then (unless autoConfigStandby is false) makes a transitionToStandby API call to the specified standby, expected to have been previously placed in STANDBY_CANDIDATE state via prepareForStandby.  After this call, the active and standby VCOs should be polled via getReplicationStatus until they  both reach STANDBY_RUNNING drState (or a configuration error is reported).  (Note: the drVCOPassword is not persisted.)  Privileges required:  `CREATE` `REPLICATION` Designate a standby Orchestrator for disaster recovery replication  |
| Disaster Recovery Demote Active - Demote current server from active to zombie | all disasterRecovery No input parameters are required.  The active server is expected to be in the drState FAILURE_GET_STANDBY_STATUS or FAILURE_MYSQL_ACTIVE_STATUS, meaning that DR protection had been engaged (with the last successful replication status observed at lastDRProtectedTime) but that active failed a health check since that time.  If the active server is in the drState STANDBY_RUNNING, meaning that it has detected no problems in interacting with the standby server, the operator can force demotion of the active using the optional parameter force passed with value of true; in this case, the operator must ensure the standby server has already been successfully promoted.  Privileges required:  `CREATE` `REPLICATION` Demote current server from active to zombie  |
| Disaster Recovery Get Replication Blob - Get the blob needed to configure replication on th... | all disasterRecovery Get from the active Orchestrator the blob needed to configure replication on the standby. Only used when configureActiveForReplication was called with autoConfigStandby set to false [true by default].  Privileges required:  `CREATE` `REPLICATION` Get the blob needed to configure replication on the standby  |
| Disaster Recovery Get Replication Status - Get disaster recovery status | all disasterRecovery Get disaster recovery replication status, optionally with client contact, state transition history, and storage information.  No input parameters are required.  Can optionally specify 1 or more of the following with parameters: clientContact,clientCount,stateHistory,storageInfo.  Privileges required:  `READ` `REPLICATION` Get disaster recovery status  |
| Disaster Recovery Prepare For Standby - Prepare current Orchestrator to be configured as a... | all disasterRecovery Transitions the current Orchestrator to a quiesced state, ready to be configured as a standby system. No input parameters are required.  After this call, the Orchestrator will be restarted in standby mode. The caller should subsequently poll `getReplicationStatus` until `drState` is `STANDBY_CANDIDATE`.  This is the first step in configuring Orchestrator disaster recovery.  Privileges required:  `CREATE` `REPLICATION` Prepare current Orchestrator to be configured as a standby system  |
| Disaster Recovery Promote Standby To Active - Promote the current server to take over as the sin... | all disasterRecovery The current server is expected to be a standby in the drState FAILURE_MYSQL_STANDBY_STATUS, meaning that DR protection had been engaged (with the last successful replication status observed at lastDRProtectedTime) but that standby has been unable to replicate since that time. If the standby server is in the drState STANDBY_RUNNING, meaning that it has detected no problems in replicating from the active server, the operator can force promotion of the standby using the optional parameter force passed with value of true; in this case, the standby server will call demoteActive/force on the active.  The operator should, if possible, ensure the formerly active server is demoted by running demoteServer directly on that server if the standby server was unable to do so successfully.  Privileges required:  `CREATE` `REPLICATION` Promote the current server to take over as the single standalone VCO  |
| Disaster Recovery Remove Standby - Unconfigure disaster recovery on the current serve... | all disasterRecovery Unconfigure disaster recovery on the current server.  Also, make a best-effort call to removeStandby on the paired DR server. No input parameters are required.  Privileges required:  `CREATE` `REPLICATION` Unconfigure disaster recovery on the current server  |
| Disaster Recovery Transition To Standby - Configure current Orchestrator to transition to st... | all disasterRecovery Configure current Orchestrator to transition to standby in disaster recovery active/standby pair. Requires parameter activeAccessFromStandby, which contains the data needed to configure standby. This data is produced by configureActiveForReplication, which by default, automatically calls transitionToStandby; an explicit call is only needed, with a blob obtained from getReplicationBlob, if configureActiveForReplication is called with autoConfigStandby set false.  Privileges required:  `CREATE` `REPLICATION` Configure current Orchestrator to transition to standby in disaster recovery active/standby pair.  |
| Edge Delete Edge - Delete an edge | all edge Delete an edge by id.  Privileges required:  `DELETE` `EDGE` Delete an edge  |
| Edge Delete Edge Bgp Neighbor Records - Delete edge BGP neighbor records | all edge Deletes BGP record(s) matching the given record keys (neighborIp) on the edges with the given IDs, if they exist.  Privileges required:  `DELETE` `NETWORK_SERVICE` Delete edge BGP neighbor records  |
| Edge Edge Cancel Reactivation - Cancel a pending edge reactivation request | edge all Cancel a pending reactivation edge reactivation request.  Privileges required:  `CREATE` `EDGE` Cancel a pending edge reactivation request  |
| Edge Edge Provision - Provision an edge | edge all Provision an edge prior to activation.  Privileges required:  `CREATE` `EDGE` Provision an edge  |
| Edge Edge Request Reactivation - Reactivate an edge | edge all Update activation state for an edge to REACTIVATION_PENDING.  Privileges required:  `CREATE` `EDGE` Reactivate an edge  |
| Edge Get Client Visibility Mode - Get an edge's client visibility mode | edge all Retrieve an edge's client visibility mode.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined` Get an edge's client visibility mode  |
| Edge Get Edge - Get edge | edge all Gets the specified Edge with optional link, site, configuration, certificate, or enterprise details. Supports queries by Edge `id`, `deviceId`, `activationKey`, and `logicalId`.  Privileges required:  `READ` `EDGE` Get edge  |
| Edge Get Edge Configuration Stack - Get an edge's configuration stack | edge all Retrieve an edge's complete configuration profile, with all modules included.  Privileges required:  `READ` `EDGE`  `READ` `ENTERPRISE_PROFILE` Get an edge's configuration stack  |
| Edge Set Edge Enterprise Configuration - Apply an enterprise configuration to an Edge | all edge Sets the enterprise configuration for the specified Edge (by `edgeId`).  Privileges required:  `UPDATE` `EDGE`  `UPDATE` `ENTERPRISE_PROFILE` Apply an enterprise configuration to an Edge  |
| Edge Set Edge Hand Off Gateways - Set an edge's on-premise hand off gateways | all edge Set an edge's on-premise hand off gateways. A primary and secondary gateway are defined, primary is required, secondary is optional. All existing edge-gateway hand off relationships are moved and are replaced by the the specified primary and secondary gateways.  Privileges required:  `UPDATE` `EDGE` Set an edge's on-premise hand off gateways  |
| Edge Set Edge Operator Configuration - Apply an operator configuration to an Edge | all edge Set an Edge's operator configuration. This overrides any enterprise-assigned operator configuration and the network default operator configuration.  Privileges required:  `UPDATE` `EDGE`  `READ` `OPERATOR_PROFILE` Apply an operator configuration to an Edge  |
| Edge Update Edge Admin Password - Update edge's local UI authentication credentials | edge all Request an update to the edge's local UI authentication credentials. On success, returns a JSON object with the ID of the action queued, status for which can be queried using the edgeAction/getEdgeAction API  Privileges required:  `UPDATE` `EDGE`  `UPDATE` `ENTERPRISE_KEYS` Update edge's local UI authentication credentials  |
| Edge Update Edge Attributes - Update edge attributes | edge all Update basic edge attributes, including edge name, description, site information, or serial number.  Privileges required:  `UPDATE` `EDGE` Update edge attributes  |
| Edge Update Edge Credentials By Configuration - Update edge UI credentials by configuration id | edge all Request an update to the edge-local UI authentication credentials for all edges belonging to a configuration profile. On success, returns a JSON object containing a list of each of the action IDs queued.  Privileges required:  `UPDATE` `EDGE`  `UPDATE` `ENTERPRISE_KEYS` Update edge UI credentials by configuration id  |
| Enterprise Decode Enterprise Key - Decrypt an enterprise key | enterprise all Decrypt an enterprise key  Privileges required:  `READ` `ENTERPRISE_KEYS` Decrypt an enterprise key  |
| Enterprise Delete Enterprise - Delete an enterprise | all enterprise Delete the enterprise specified by the given id parameter. enterpriseId is also a valid alias for id.  Privileges required:  `DELETE` `ENTERPRISE` Delete an enterprise  |
| Enterprise Delete Enterprise Gateway Records - Delete enterprise gateway record(s) | all enterprise Delete the enterprise gateway record(s) matching the given gateway id(s) and neighbor IP addresses.  Privileges required:  `DELETE` `NETWORK_SERVICE` Delete enterprise gateway record(s)  |
| Enterprise Delete Enterprise Network Allocation - Delete an enterprise network allocation | all enterprise Delete an enterprise network allocation, by id.  Privileges required:  `DELETE` `NETWORK_ALLOCATION` Delete an enterprise network allocation  |
| Enterprise Delete Enterprise Service - Delete an enterprise service | all enterprise Delete an enterprise service, by id.  Privileges required:  `DELETE` `NETWORK_SERVICE` Delete an enterprise service  |
| Enterprise Encode Enterprise Key - Encrypt an enterprise key | enterprise all Encrypt an enterprise key  Privileges required:  `CREATE` `ENTERPRISE_KEYS` Encrypt an enterprise key  |
| Enterprise Get Enterprise - Get enterprise | enterprise all Retrieve enterprise data, with optional proxy (partner) detail.  Privileges required:  `READ` `ENTERPRISE` Get enterprise  |
| Enterprise Get Enterprise Addresses - Get enterprise IP address information | all enterprise Retrieve the public IP address information for the management and control entities associated with this enterprise, including Orchestrator(s), Gateway(s), and datacenter(s).  Privileges required:  `READ` `ENTERPRISE` Get enterprise IP address information  |
| Enterprise Get Enterprise Alert Configurations - Get the enterprise alert configuration | all enterprise Get the alert configurations associated with an enterprise.  Privileges required:  `READ` `ENTERPRISE_ALERT` Get the enterprise alert configuration  |
| Enterprise Get Enterprise Alerts - Get triggered enterprise alerts | all enterprise Gets past triggered alerts for the specified enterprise.  Privileges required:  `READ` `ENTERPRISE_ALERT` Get triggered enterprise alerts  |
| Enterprise Get Enterprise All Alert Recipients - List recipients currently receiving ALL enterprise... | all enterprise List the recipients currently configured to receive all alerts for an enterprise.  Privileges required:  `READ` `ENTERPRISE_ALERT` List recipients currently receiving ALL enterprise alerts  |
| Enterprise Get Enterprise Capabilities - Get enterprise capabilities | enterprise all Retrieve a list of the enterprise capabilities currently enabled/disabled on an enterprise (e.g. BGP, COS mapping, PKI, etc.)  Privileges required:  `READ` `ENTERPRISE` Get enterprise capabilities  |
| Enterprise Get Enterprise Configurations - Get enterprise configuration profiles | all enterprise Retrieve a list of configuration profiles existing on an enterprise, with optional edge and/or module detail.  Privileges required:  `READ` `ENTERPRISE_PROFILE` Get enterprise configuration profiles  |
| Enterprise Get Enterprise Edges - Get edges associated with an enterprise | enterprise all Gets all Edges associated with the specified enterprise, including optional site, link, and configuration details.  Privileges required:  `READ` `EDGE` Get edges associated with an enterprise  |
| Enterprise Get Enterprise Gateway Handoff - Get enterprise gateway handoff configuration | enterprise all Get enterprise gateway handoff configuration.  Privileges required:  `READ` `ENTERPRISE` Get enterprise gateway handoff configuration  |
| Enterprise Get Enterprise Network Allocation - Get an enterprise network allocation | enterprise all Retrieve a network allocation object by id.  Privileges required:  `READ` `NETWORK_ALLOCATION` Get an enterprise network allocation  |
| Enterprise Get Enterprise Network Allocations - Get all network allocation objects defined on an e... | all enterprise Retrieve a list of all of the network allocations defined onthe given enterprise.  Privileges required:  `READ` `NETWORK_ALLOCATION` Get all network allocation objects defined on an enterprise  |
| Enterprise Get Enterprise Network Segments - Get all network segment objects defined on an ente... | all enterprise Retrieve a list of all of the network segments defined forthe given enterprise.  Privileges required:  `READ` `NETWORK_ALLOCATION` Get all network segment objects defined on an enterprise  |
| Enterprise Get Enterprise Property - Get enterprise property | enterprise all Get a enterprise property by object id or other attribute.  Privileges required:  `READ` `ENTERPRISE` Get enterprise property  |
| Enterprise Get Enterprise Route Configuration - Get route advertisement and routing preferences po... | all enterprise Get enterprise route advertisement, routing peferences and OSPF, BGP advertisement policy as configured in the Overlay Flow Control table.  Privileges required:  `READ` `ENTERPRISE_PROFILE` Get route advertisement and routing preferences policy  |
| Enterprise Get Enterprise Route Table - Get the enterprise route table | all enterprise Get composite enterprise route table, optionally scoped by profile(s). The returned routes include static routes, directly connected routes and learned routes.  Privileges required:  `READ` `ENTERPRISE_PROFILE` Get the enterprise route table  |
| Enterprise Get Enterprise Services - Get enterprise network service detail | all enterprise Get the network service JSON objects defined for an enterprise.  Privileges required:  `READ` `NETWORK_SERVICE` Get enterprise network service detail  |
| Enterprise Get Enterprise Users - Get list of enterprise users by enterprise id | userMaintenance all undefined  Privileges required:  `READ` `ENTERPRISE`  `READ` `ENTERPRISE_USER` Get list of enterprise users by enterprise id  |
| Enterprise Insert Enterprise - Create enterprise | enterprise all Creates a new enterprise, which is owned by the operator.  Privileges required:  `CREATE` `ENTERPRISE` Create enterprise  |
| Enterprise Insert Enterprise Network Allocation - Insert an enterprise network allocation | enterprise all Insert a new enterprise network allocation.  Privileges required:  `CREATE` `NETWORK_ALLOCATION` Insert an enterprise network allocation  |
| Enterprise Insert Enterprise Network Segment - Insert an enterprise network segment | enterprise all Insert a new enterprise network segment.  Privileges required:  `CREATE` `NETWORK_ALLOCATION` Insert an enterprise network segment  |
| Enterprise Insert Enterprise Service - Insert a new enterprise service | all enterprise Insert a new enterprise service.  Privileges required:  `CREATE` `NETWORK_SERVICE` Insert a new enterprise service  |
| Enterprise Insert Enterprise User - Insert an enterprise user | userMaintenance all Insert an enterprise user.  Privileges required:  `CREATE` `ENTERPRISE_USER` Insert an enterprise user  |
| Enterprise Insert Or Update Enterprise Alert Configurations - Insert, update, or delete enterprise alert configu... | all enterprise Insert, update, or delete enterprise alert configurations. Returns the array of alert configurations submitted, with ids added for the entries that have been successfully inserted. If an entry is not successfully inserted or updated, an `error` property is included in the .  Privileges required:  `CREATE` `ENTERPRISE_ALERT` Insert, update, or delete enterprise alert configurations  |
| Enterprise Insert Or Update Enterprise Capability - Insert or update an enterprise capability | enterprise all Insert or update an enterprise capability.  Privileges required:  `UPDATE` `ENTERPRISE` Insert or update an enterprise capability  |
| Enterprise Insert Or Update Enterprise Gateway Handoff - Insert or update an enterprise gateway handoff con... | enterprise all Insert or update an enterprise gateway handoff configuration.  Privileges required:  `UPDATE` `ENTERPRISE` Insert or update an enterprise gateway handoff configuration  |
| Enterprise Insert Or Update Enterprise Property - Insert or update an enterprise property | enterprise all Insert a enterprise property. If property with the given name already exists, the property will be updated.  Privileges required:  `READ` `ENTERPRISE` Insert or update an enterprise property  |
| Enterprise Proxy Delete Enterprise Proxy User - Delete an enterprise proxy admin user | all userMaintenance Delete an enterprise proxy user by id or username. Note that `enterpriseProxyId` is a required parameter when invoking this method as an operator or partner user.  Privileges required:  `DELETE` `PROXY_USER` Delete an enterprise proxy admin user  |
| Enterprise Proxy Get Enterprise Proxy Edge Inventory - Get a list of all partner enterprises and edge inv... | all enterpriseProxy Get  partner enterprises and their edge inventory.  Privileges required:  `READ` `ENTERPRISE` Get a list of all partner enterprises and edge inventory associated with each enterprise  |
| Enterprise Proxy Get Enterprise Proxy Enterprises - Get a list of all partner enterprises | all enterpriseProxy Get all partner enterprises, optionally including all edges or edge counts.  Privileges required:  `READ` `ENTERPRISE` Get a list of all partner enterprises  |
| Enterprise Proxy Get Enterprise Proxy Gateway Pools - Get list of gateway pools | all enterpriseProxy Get list of gateway pools associated with an enterprise proxy, optionally with lists of gateways or enterprises belonging to each pool.  Privileges required:  `READ` `GATEWAY` Get list of gateway pools  |
| Enterprise Proxy Get Enterprise Proxy Operator Profiles - Get the operator profiles associated with a partne... | all enterpriseProxy Get the operator profiles associated with a proxy (MSP), as assigned by the operator.  Privileges required:  `READ` `OPERATOR_PROFILE` Get the operator profiles associated with a partner  |
| Enterprise Proxy Get Enterprise Proxy User - Get an enterprise proxy user | all userMaintenance Get an enterprise proxy user by id or username.  Privileges required:  `READ` `PROXY_USER`  `READ` `PROXY` Get an enterprise proxy user  |
| Enterprise Proxy Get Enterprise Proxy Users - Get all enterprise proxy admin users | userMaintenance all undefined  Privileges required:  `READ` `ENTERPRISE`  `READ` `PROXY_USER` Get all enterprise proxy admin users  |
| Enterprise Proxy Insert Enterprise Proxy Enterprise - Insert a new enterprise owned by an MSP | enterpriseProxy all Insert an enterprise owned by an MSP. Whereas the `insertEnterprise` method will create an enterprise in the global or network context with no MSP association, this method will create one owned by an MSP, as determined by the credentials of the caller.  Privileges required:  `CREATE` `ENTERPRISE` Insert a new enterprise owned by an MSP  |
| Enterprise Proxy Insert Enterprise Proxy User - Create a new partner admin user | all userMaintenance Create a new partner admin user  Privileges required:  `CREATE` `PROXY_USER` Create a new partner admin user  |
| Enterprise Proxy Update Enterprise Proxy User - Update an enterprise proxy admin user | all userMaintenance Update an enterprise proxy admin user  Privileges required:  `UPDATE` `PROXY_USER` Update an enterprise proxy admin user  |
| Enterprise Set Enterprise All Alert Recipients - Set the recipients who should receive all alerts f... | all enterprise Set the recipients who should receive all alerts for an enterprise.  Privileges required:  `UPDATE` `ENTERPRISE_ALERT` Set the recipients who should receive all alerts for an enterprise  |
| Enterprise Update Enterprise - Update an enterprise | enterprise all Update an enterprise provided an object id or name, and an _update object with the names and values of columns to be updated.  Privileges required:  `UPDATE` `ENTERPRISE` Update an enterprise  |
| Enterprise Update Enterprise Network Allocation - Update an enterprise network allocation | enterprise all Update an enterprise network allocation, provided an object id and an _update object with the names and values of columns to be updated.  Privileges required:  `UPDATE` `NETWORK_ALLOCATION` Update an enterprise network allocation  |
| Enterprise Update Enterprise Network Segment - Update an enterprise network segment | enterprise all Update an enterprise network segment.  Privileges required:  `UPDATE` `NETWORK_ALLOCATION` Update an enterprise network segment  |
| Enterprise Update Enterprise Route - Update an enterprise route | all enterprise Update an enterprise route, set advertisement and cost values. Required parameters include the original route, as returned by enterprise/getEnterpriseRouteTable and the updated route with modified advertisement and route preference ordering.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE` Update an enterprise route  |
| Enterprise Update Enterprise Route Configuration - Update enterprise routing configuration | all enterprise Update enterprise routing configuration, by configuration id or logicalId.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE` Update enterprise routing configuration  |
| Enterprise Update Enterprise Security Policy - Update enterprise security policy | enterprise all Update enterprise security policy in accordance with to the passed ipsec settings.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE` Update enterprise security policy  |
| Enterprise Update Enterprise Service - Update an enterprise service | all enterprise Update the enterprise service with the given id according to the settings specified by the _update field.  Privileges required:  `UPDATE` `NETWORK_SERVICE` Update an enterprise service  |
| Enterprise User Delete Enterprise User - Delete an enterprise user. | all userMaintenance Delete an enterprise user by id or username. Note that `enterpriseId` is a required parameter when invoking this method as an operator or partner user.  Privileges required:  `DELETE` `ENTERPRISE_USER` Delete an enterprise user.  |
| Enterprise User Get Enterprise User - Get an enterprise user | all userMaintenance Get an enterprise user by id or username.  Privileges required:  `READ` `ENTERPRISE_USER` Get an enterprise user  |
| Enterprise User Update Enterprise User - Update an enterprise user | all userMaintenance Update an enterprise user provided an object `id` or other identifying attributes, and an `_update` object with the names and values of columns to be updated.  Privileges required:  `UPDATE` `ENTERPRISE_USER`, or  `UPDATE` `OPERATOR_USER` Update an enterprise user  |
| Event Get Enterprise Events - Get Edge events | event all Gets Edge events in an enterprise or Edge context. Returns an array of Edge events sorted by `eventTime`.  Privileges required:  `READ` `ENTERPRISE_EVENT` Get Edge events  |
| Event Get Operator Events - Get operator events | event all Gets operator events by network ID (optional). If not specified, will be taken for the caller's security context. Optionally, use a filter object to limit the number of events returned. Additionally, specify a time interval with an interval object. If no end date is specified, then the default is the current date. Specify a `gatewayId` to filter events for the specified gateway.  Privileges required:  `READ` `OPERATOR_EVENT` Get operator events  |
| Firewall Get Enterprise Firewall Logs - Get enterprise firewall logs | firewall all Gets firewall logs for the specified enterprise.  Privileges required:  `READ` `EDGE`  `VIEW_FIREWALL_LOGS` `undefined` Get enterprise firewall logs  |
| Gateway Delete Gateway - Delete a gateway | gateway all Delete a gateway by id.  Privileges required:  `DELETE` `GATEWAY` Delete a gateway  |
| Gateway Gateway Provision - Provision a gateway | gateway all Provision a gateway into an operator network.  Privileges required:  `CREATE` `GATEWAY` Provision a gateway  |
| Gateway Get Gateway Edge Assignments - Get edge assignments for a gateway | gateway all Get edge assignments for a gateway  Privileges required:  `READ` `GATEWAY` Get edge assignments for a gateway  |
| Gateway Update Gateway Attributes - Update gateway attributes | gateway all Update gateway attributes (name, ipAddress, on-premise parametrization and description) and associated site attributes  Privileges required:  `UPDATE` `GATEWAY` Update gateway attributes  |
| Link Quality Event Get Link Quality Events - Get link quality data | linkQualityEvent all Returns link quality scores per link for a particular edge within a time interval. Rolls up link quality events to provide an aggregate score for the edge. Returns an empty array if no link quality events are available in the given timeframe.  Privileges required:  `READ` `EDGE` Get link quality data  |
| Login Enterprise Login - Authentication for non-operator users | all login Authentication for non-operator users  |
| Login Operator Login - Authentication for an operator user | all login Authentication for an operator user  |
| Logout - Deactivate a given authorization cookie | all login Deactivate a given authorization cookie  |
| Metrics Get Edge App Link Metrics - Get flow metric aggregate data by link | all metrics Fetch flow metric summaries for the given time interval by link. On success, this method returns an array of flow data where each entry corresponds to a link on the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined` Get flow metric aggregate data by link  |
| Metrics Get Edge App Link Series - Get flow metric time series data by link | all metrics Fetch flow metric time series for the given time interval by link. On success, this method returns an array of flow data where each entry corresponds to a link on the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined` Get flow metric time series data by link  |
| Metrics Get Edge App Metrics - Get flow metric aggregate data by application | all metrics Fetch flow metric summaries for the given time interval by application. On success, this method returns an array of flow data where each entry corresponds to a single application. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined` Get flow metric aggregate data by application  |
| Metrics Get Edge App Series - Get flow metric time series data by application | all metrics Fetch flow metric time series for the given time interval by application. On success, this method returns an array of flow data where each entry corresponds to a single application. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined` Get flow metric time series data by application  |
| Metrics Get Edge Category Metrics - Get flow metric aggregate data by application cate... | all metrics Fetch flow metric summaries for the given time interval by application category. On success, this method returns an array of flow data where each entry corresponds to a category of application traffic that has traversed the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined` Get flow metric aggregate data by application category  |
| Metrics Get Edge Category Series - Get flow metric time series data by application ca... | all metrics Fetch flow metric time series for the given time interval by application category. On success, this method returns an array of flow data where each entry corresponds to a category of application traffic that has traversed the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined` Get flow metric time series data by application category  |
| Metrics Get Edge Dest Metrics - Get flow metric aggregate data by destination | all metrics Fetch flow metric summaries for the given time interval by destination. On success, this method returns an array of flow data where each entry corresponds to a distinct traffic destination. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined` Get flow metric aggregate data by destination  |
| Metrics Get Edge Dest Series - Get flow metric time series data by destination | all metrics Fetch flow metric time series for the given time interval by destination. On success, this method returns an array of flow data where each entry corresponds to a distinct traffic destination. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined` Get flow metric time series data by destination  |
| Metrics Get Edge Device Metrics - Get flow metric aggregate data by client device | all metrics Fetch flow metric summaries for the given time interval by client device. On success, this method returns an array of flow data where each entry corresponds to a distinct device. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_USER_IDENTIFIABLE_FLOW_STATS` `undefined` Get flow metric aggregate data by client device  |
| Metrics Get Edge Device Series - Get flow metric time series data by client device | all metrics Fetch flow metric time series for the given time interval by client device. On success, this method returns an array of flow data where each entry corresponds to a distinct device. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_USER_IDENTIFIABLE_FLOW_STATS` `undefined` Get flow metric time series data by client device  |
| Metrics Get Edge Link Metrics - Get advanced flow metric aggregate data by link | all metrics Fetch advanced flow metric summaries for the given time interval by link. On success, this method returns an array of flow data where each entry corresponds to a link on the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE` Get advanced flow metric aggregate data by link  |
| Metrics Get Edge Link Series - Get advanced flow metric time series data by link | all metrics Fetch advanced flow metric time series for the given time interval by link. On success, this method returns an array of flow data where each entry corresponds to a link on the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE` Get advanced flow metric time series data by link  |
| Metrics Get Edge Os Metrics - Get flow metric aggregate data by client OS | all metrics Fetch flow metric summaries for the given time interval by client OS. On success, this method returns an array of flow data where each entry corresponds to a distinct OS on a client device. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined` Get flow metric aggregate data by client OS  |
| Metrics Get Edge Os Series - Get flow metric time series data by client OS | all metrics Fetch flow metric time series for the given time interval by client OS. On success, this method returns an array of flow data where each entry corresponds to a distinct OS on a client device. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined` Get flow metric time series data by client OS  |
| Metrics Get Edge Segment Metrics - Get flow metric aggregate data by segment Id | all metrics Fetch flow metric summaries for the given time interval by segment id. On success, this method returns an array of flow data where each entry corresponds to a segment id traffic that has traversed the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined` Get flow metric aggregate data by segment Id  |
| Metrics Get Edge Segment Series - Get flow metric time series data by segment id | all metrics Fetch flow metric time series for the given time interval by segment id. On success, this method returns an array of flow data where each entry corresponds to a segment id of traffic that has traversed the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined` Get flow metric time series data by segment id  |
| Monitoring Get Aggregate Edge Link Metrics - Get aggregate Edge link metrics across enterprises | all monitoring Gets aggregate link metrics for the request interval for all active links across all enterprises, where a link is considered to be active if an Edge has reported any activity for it in the last 24 hours. On success, returns an array of network utilization metrics, one per link.  Privileges required:  `READ` `ENTERPRISE`  `READ` `EDGE` Get aggregate Edge link metrics across enterprises  |
| Monitoring Get Aggregate Enterprise Events - Get events across all enterprises | all monitoring Gets events across all enterprises in a paginated list. When called in the MSP/Partner context, queries only enterprises managed by the MSP.  Privileges required:  `READ` `ENTERPRISE`  `READ` `EDGE` Get events across all enterprises  |
| Monitoring Get Aggregates - Get aggregate enterprise and edge information | all monitoring Retrieve a comprehensive listing of all enterprises and edges on a network. Returns an object containing an aggregate `edgeCount`, a list (`enterprises`) containing enterprise objects, and a map (`edges`) which gives edge counts per enterprise.  Privileges required:  `READ` `ENTERPRISE`  `READ` `EDGE` Get aggregate enterprise and edge information  |
| Monitoring Get Enterprise Bgp Peer Status - Get gateway BGP peer status for all enterprise gat... | all monitoring Returns an array where each entry corresponds to a gateway and contains an associated set of BGP peers with state records.  Privileges required:  `READ` `NETWORK_SERVICE` Get gateway BGP peer status for all enterprise gateways  |
| Monitoring Get Enterprise Edge Bgp Peer Status - Get edge BGP peer status for all enterprise edges | all monitoring Returns an array where each entry corresponds to an edge and contains an associated set of BGP peers and state records.  Privileges required:  `READ` `EDGE` Get edge BGP peer status for all enterprise edges  |
| Monitoring Get Enterprise Edge Link Status - Get edge and link status data | all monitoring Get current edge and edge-link status for all enterprise edges.  Privileges required:  `READ` `ENTERPRISE`  `READ` `EDGE` Get edge and link status data  |
| Network Delete Network Gateway Pool - Delete gateway pool | network all Deletes the specified gateway pool (by `id`).  Privileges required:  `DELETE` `GATEWAY` Delete gateway pool  |
| Network Get Network Configurations - Get operator configuration profiles | all network Gets all operator configuration profiles associated with an operator's network. Optionally includes the modules associated with each profile. This call does not return templates.  Privileges required:  `READ` `OPERATOR_PROFILE` Get operator configuration profiles  |
| Network Get Network Enterprises - Get a list of the enterprises on a network | all network Get the enterprises existing on a network, optionally including all edges or edge counts. The `edgeConfigUpdate` "with" option may also be passed to check whether application of configuration updates to edges is enabled for each enterprise.  Privileges required:  `READ` `ENTERPRISE` Get a list of the enterprises on a network  |
| Network Get Network Gateway Pools - Get list of gateway pools | all network Get list of gateway pools associated with a network, optionally with the gateways or enterprises belonging to each pool.  Privileges required:  `READ` `GATEWAY` Get list of gateway pools  |
| Network Get Network Gateways - Get list of gateways | network all Get list of gateways associated with a network.  Privileges required:  `READ` `GATEWAY` Get list of gateways  |
| Network Get Network Operator Users - Get list of operator users for a network | all network Get a list of all of the operator users associated with a network  Privileges required:  `READ` `OPERATOR_USER` Get list of operator users for a network  |
| Network Insert Network Gateway Pool - Insert a gateway pool | network all Insert a gateway pool, associated with a network.  Privileges required:  `CREATE` `GATEWAY` Insert a gateway pool  |
| Network Update Network Gateway Pool Attributes - Update gateway pool attributes | network all Update the configurable attributes of a Gateway Pool. Configurarable attributes are `name`, `description`, and `handOffType`.  Privileges required:  `UPDATE` `GATEWAY` Update gateway pool attributes  |
| Operator User Delete Operator User - Delete an operator user | all userMaintenance Delete an operator user object by `id` or `username`.  Privileges required:  `DELETE` `OPERATOR_USER` Delete an operator user  |
| Operator User Get Operator User - Get an operator user | all userMaintenance Get an operator user object by `id` or `username`.  Privileges required:  `READ` `OPERATOR_USER` Get an operator user  |
| Operator User Insert Operator User - Insert an operator user | all userMaintenance Insert an operator user and associate with an operator's network.  Privileges required:  `CREATE` `OPERATOR_USER` Insert an operator user  |
| Operator User Update Operator User - Update an operator user | all userMaintenance Update an operator user provided an object `id` or `username`, and an `_update` object containing the names and values, of columns to be updated.  Privileges required:  `UPDATE` `OPERATOR_USER` Update an operator user  |
| Role Create Role Customization - Create a role customization | all role Create a role customization given a roleId and an array of privilegeIds.  Privileges required:  `UPDATE` `NETWORK` Create a role customization  |
| Role Delete Role Customization - Delete a role customization | all role Delete a role customization, given a role customization name or forRoleId.  Privileges required:  `UPDATE` `NETWORK` Delete a role customization  |
| Role Get User Type Roles - Get the roles defined for a user type | all role Return the defined roles for a specified user type.  Privileges required:  `READ` `ENTERPRISE_USER`, or  `READ` `PROXY_USER`, or  `READ` `OPERATOR_USER` Get the roles defined for a user type  |
| Role Set Enterprise Delegated To Enterprise Proxy - Grant enterprise access to partner | role all Grants enterprise access to the specified enterprise proxy (partner). When an enterprise is delegated to a proxy, proxy users are granted access to view, configure, and troubleshoot Edges owned by the enterprise. As a security consideration, proxy Support users cannot view personally identifiable information.  Privileges required:  `UPDATE` `ENTERPRISE_DELEGATION` Grant enterprise access to partner  |
| Role Set Enterprise Delegated To Operator - Grant enterprise access to network operator | role all Grants enterprise access to the network operator. When an enterprise is delegated to the operator, operator users are permitted to view, configure, and troubleshoot Edges owned by the enterprise. As a security consideration, operator users cannot view personally identifiable information.  Privileges required:  `UPDATE` `ENTERPRISE_DELEGATION` Grant enterprise access to network operator  |
| Role Set Enterprise Proxy Delegated To Operator - Grant enterprise proxy access to network operator | role all Grants enterprise proxy access to the network operator. When an enterprise proxy is delegated to the operator, operator users are granted access to view, configure and troubleshoot objects owned by the proxy.  Privileges required:  `UPDATE` `ENTERPRISE_PROXY_DELEGATION` Grant enterprise proxy access to network operator  |
| Role Set Enterprise User Management Delegated To Operator - Grant enterprise user access to the network operat... | role all When enterprise user management is delegated to the operator, operator users are granted enterprise-level user management capabilities (user creation, password resets, etc.).  Privileges required:  `UPDATE` `ENTERPRISE_DELEGATION` Grant enterprise user access to the network operator  |
| Set Client Device Host Name - Set hostname for client device | clientDevice all Set hostname for client device  Privileges required:  `UPDATE` `CLIENT_DEVICE` Set hostname for client device  |
| System Property Get System Properties - Get all system properties | systemProperty all Get a list of all configured system properties.  Privileges required:  `READ` `SYSTEM_PROPERTY` Get all system properties  |
| System Property Get System Property - Get system property | systemProperty all Get a system property by object id or other attribute.  Privileges required:  `READ` `SYSTEM_PROPERTY` Get system property  |
| System Property Insert Or Update System Property - Insert or update a system property | systemProperty all Insert a system property. If property with the given name already exists, the property will be updated.  Privileges required:  `CREATE` `SYSTEM_PROPERTY` Insert or update a system property  |
| System Property Insert System Property - Insert a system property | systemProperty all Insert a new system property.  Privileges required:  `CREATE` `SYSTEM_PROPERTY` Insert a system property  |
| System Property Update System Property - Update a system property | systemProperty all Update an existing system property, provided an object `id` or other identifying attributes  Privileges required:  `UPDATE` `SYSTEM_PROPERTY` Update a system property  |
| Vco Inventory Associate Edge - Return inventory items available at this VCO | all disasterRecovery Assigns an edge in the inventory to an Enterprise. To perform the action, the edge should already be in a STAGING state. The assignment can be done at an enterprise level, without selecting a destination Edge profile. In such a case, the inventory edge is assigned to a staging profile within the Enterprise. Optionally a profile or destination edge can be assigned to this inventory edge. The edge in the inventory can be assigned to any profile. The inventory edge can be assigned to an Enterprise edge only if it is in a PENDING/REACTIVATION_PENDING state.  Privileges required:  `CREATE` `ENTERPRISE` Return inventory items available at this VCO  |
| Vco Inventory Get Inventory Items - Return inventory items available at this VCO | all inventoryManagement Retrieve all the inventory information available with this VCO. This method does not have required parameters. The optional parameters are  enterpriseId - Return inventory items belonging to that enterprise. If the caller context is an enterprise, this value will be taken from token itself. modifiedSince - Used to retrieve inventory items that have been modified in the last modifiedSince hours. with - an array containing the string "edge" to get details about details about the provisioned edge if any.  Privileges required:  `READ` `INVENTORY` Return inventory items available at this VCO  |
| Vpn Generate Vpn Gateway Configuration - Provision a non-SD-WAN VPN site | all vpn Provision a non-SD-WAN site (e.g. a data center or cloud service PoP) and generate VPN configuration.  Privileges required:  `CREATE` `NETWORK_SERVICE` Provision a non-SD-WAN VPN site  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |


### GET Operation Types
| Label | Help Text |
| --- | --- |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Meta - Get meta-data on any other API call (POST) | all meta Get meta-data on any other API call  |


