# Petstore Sample Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### URL

The URL for the Petstore service

**Type** - string

**Default Value** - https://petstore.swagger.io

# Operation Tab


## CREATE


## UPDATE


## GET


## DELETE


## QUERY

### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

The query filter supports no groupings (only one expression allowed).

Example:
(foo lessThan 30)

#### Filter Operators

 * Equal To  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts

The query operation does not support sorting.


# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.
 * Additional URI Query Parameters
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| AddPet - Add a new pet to the store | pet  Add a new pet to the store  | /pet\_\_\_post |
| CreateUser - Create user | user This can only be done by the logged in user. Create user  | /user\_\_\_post |
| CreateUsersWithArrayInput - Creates list of users with given input array | user  Creates list of users with given input array  | /user/createWithArray\_\_\_post |
| CreateUsersWithListInput - Creates list of users with given input array | user  Creates list of users with given input array  | /user/createWithList\_\_\_post |
| PlaceOrder - Place an order for a pet | store  Place an order for a pet  | /store/order\_\_\_post |
| UploadFile - uploads an image | pet  uploads an image  | /pet/{petId}/uploadImage\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| DeleteOrder - Delete purchase order by ID | store For valid response try integer IDs with positive integer value. Negative or non-integer values will generate API errors Delete purchase order by ID  | /store/order/{orderId}\_\_\_delete |
| DeletePet - Deletes a pet | pet  Deletes a pet  | /pet/{petId}\_\_\_delete |
| DeleteUser - Delete user | user This can only be done by the logged in user. Delete user  | /user/{username}\_\_\_delete |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| GetOrderById - Find purchase order by ID | store For valid response try integer IDs with value >= 1 and <= 10. Other values will generated exceptions Find purchase order by ID  | /store/order/{orderId}\_\_\_get |
| GetPetById - Find pet by ID | pet Returns a single pet Find pet by ID  | /pet/{petId}\_\_\_get |
| GetUserByName - Get user by user name | user  Get user by user name  | /user/{username}\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| FindPetsByStatus - Finds Pets by status | pet Multiple status values can be provided with comma separated strings Finds Pets by status  | /pet/findByStatus\_\_\_get |
| FindPetsByTags - Finds Pets by tags | pet Multiple tags can be provided with comma separated strings. Use tag1, tag2, tag3 for testing. Finds Pets by tags  | /pet/findByTags\_\_\_get |
| GetInventory - Returns pet inventories by status | store Returns a map of status codes to quantities Returns pet inventories by status  | /store/inventory\_\_\_get |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| UpdatePet - Update an existing pet (PUT) | pet  Update an existing pet  | /pet\_\_\_put |
| UpdateUser - Updated user (PUT) | user This can only be done by the logged in user. Updated user  | /user/{username}\_\_\_put |


