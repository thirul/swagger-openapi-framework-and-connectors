# This document covers the data models and the APIs exposed by Gloat. Before starting out, make sure you have the necessary fields for authenticating to the APIs, namely: your tenant ID and your API key.
This document covers the data models and the APIs exposed by Gloat. Before starting out, make sure you have the necessary fields for authenticating to the APIs, namely: your tenant ID and your API key.

# Connection Tab
## Connection Fields


#### Gloat Customer API REST API Service URL

The URL for the Gloat Customer API REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default openapi.json file so you can provide a url to any swagger file.

**Type** - string



#### URL

The URL for the Service service

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### Use Authentication Loading Swagger

Select this option if loading a swagger from a URL requires authentication.

**Type** - boolean



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| Candidacy Upsert - Upsert candidacy | Candidacy Update or insert a candidacy in Gloat Upsert candidacy  |
| Function Bulk Insert - Insert functions (bulk) | Core Entities Create functions and sub-functions Insert functions (bulk)  |
| Job Upsert - Upsert job | Job Update or insert jobs in Gloat Upsert job  |
| Location Bulk Insert - Insert locations (bulk) | Core Entities Create locations (a.k.a. sites) Insert locations (bulk)  |
| User Upsert - Upsert a single user | User Update or insert a user in Gloat Upsert a single user  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |


### GET Operation Types
| Label | Help Text |
| --- | --- |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| Get Candidacy IDs By Status - Candidacy IDs by status | Candidacy Retrieve list of candidacy ID's by list of statuses Candidacy IDs by status  |
| Get Job By Status - Job IDs by status | Job Retrieve list of Job ID's by list of statuses Job IDs by status  |
| Get User By Status - User IDs by status | User Retrieve list of user ID's by list of statuses User IDs by status  |
| Meta - Test service availability and authorization | Meta Returns a successful response, if authorization succeeded Test service availability and authorization  |
| Search By Keyword - Search for various entities | Search Return short descriptions of entities (jobs, roles, people) according to a free textual query Search for various entities  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Candidacy Patch - Update candidacy data (PATCH) | Candidacy Update candidacy data by ids Update candidacy data  |
| Job Patch - Update job data (PATCH) | Job Update job data by ids Update job data  |


