# Magento REST API Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### Magento Swagger API Swagger URL

This will override selections so you can provide a url to any swagger file.

**Type** - string

**Default Value** - https://devdocs.magento.com/redoc/2.2/



#### URL

The URL for the Service service

**Type** - string



#### AuthenticationType

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - oauth

# Operation Tab


## CREATE


## UPDATE


## GET


## DELETE


## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| BundleProductOptionManagementV1SavePost - bundle-products/options/add | bundle-products/options/add | /V1/bundle-products/options/add\_\_\_post |
| CatalogAttributeSetManagementV1CreatePost - products/attribute-sets | products/attribute-sets | /V1/products/attribute-sets\_\_\_post |
| CatalogBasePriceStorageV1GetPost - products/base-prices-information | products/base-prices-information | /V1/products/base-prices-information\_\_\_post |
| CatalogBasePriceStorageV1UpdatePost - products/base-prices | products/base-prices | /V1/products/base-prices\_\_\_post |
| CatalogCategoryLinkRepositoryV1SavePost - categories/{categoryId}/products | categories/{categoryId}/products | /V1/categories/{categoryId}/products\_\_\_post |
| CatalogCategoryRepositoryV1SavePost - categories | categories | /V1/categories\_\_\_post |
| CatalogCostStorageV1DeletePost - products/cost-delete | products/cost-delete | /V1/products/cost-delete\_\_\_post |
| CatalogCostStorageV1GetPost - products/cost-information | products/cost-information | /V1/products/cost-information\_\_\_post |
| CatalogCostStorageV1UpdatePost - products/cost | products/cost | /V1/products/cost\_\_\_post |
| CatalogProductAttributeGroupRepositoryV1SavePost - products/attribute-sets/groups | products/attribute-sets/groups | /V1/products/attribute-sets/groups\_\_\_post |
| CatalogProductAttributeManagementV1AssignPost - products/attribute-sets/attributes | products/attribute-sets/attributes | /V1/products/attribute-sets/attributes\_\_\_post |
| CatalogProductAttributeMediaGalleryManagementV1CreatePost - products/{sku}/media | products/{sku}/media | /V1/products/{sku}/media\_\_\_post |
| CatalogProductAttributeOptionManagementV1AddPost - products/attributes/{attributeCode}/options | products/attributes/{attributeCode}/options | /V1/products/attributes/{attributeCode}/options\_\_\_post |
| CatalogProductAttributeRepositoryV1SavePost - products/attributes | products/attributes | /V1/products/attributes\_\_\_post |
| CatalogProductCustomOptionRepositoryV1SavePost - products/options | products/options | /V1/products/options\_\_\_post |
| CatalogProductLinkManagementV1SetProductLinksPost - products/{sku}/links | products/{sku}/links | /V1/products/{sku}/links\_\_\_post |
| CatalogProductRepositoryV1SavePost - products | products | /V1/products\_\_\_post |
| CatalogProductWebsiteLinkRepositoryV1SavePost - products/{sku}/websites | products/{sku}/websites | /V1/products/{sku}/websites\_\_\_post |
| CatalogSpecialPriceStorageV1DeletePost - products/special-price-delete | products/special-price-delete | /V1/products/special-price-delete\_\_\_post |
| CatalogSpecialPriceStorageV1GetPost - products/special-price-information | products/special-price-information | /V1/products/special-price-information\_\_\_post |
| CatalogSpecialPriceStorageV1UpdatePost - products/special-price | products/special-price | /V1/products/special-price\_\_\_post |
| CatalogTierPriceStorageV1DeletePost - products/tier-prices-delete | products/tier-prices-delete | /V1/products/tier-prices-delete\_\_\_post |
| CatalogTierPriceStorageV1GetPost - products/tier-prices-information | products/tier-prices-information | /V1/products/tier-prices-information\_\_\_post |
| CatalogTierPriceStorageV1UpdatePost - products/tier-prices | products/tier-prices | /V1/products/tier-prices\_\_\_post |
| CheckoutGuestPaymentInformationManagementV1SavePaymentInformationAndPlaceOrderPost - guest-carts/{cartId}/payment-information | guest-carts/{cartId}/payment-information | /V1/guest-carts/{cartId}/payment-information\_\_\_post |
| CheckoutGuestPaymentInformationManagementV1SavePaymentInformationPost - guest-carts/{cartId}/set-payment-information | guest-carts/{cartId}/set-payment-information | /V1/guest-carts/{cartId}/set-payment-information\_\_\_post |
| CheckoutGuestShippingInformationManagementV1SaveAddressInformationPost - guest-carts/{cartId}/shipping-information | guest-carts/{cartId}/shipping-information | /V1/guest-carts/{cartId}/shipping-information\_\_\_post |
| CheckoutGuestTotalsInformationManagementV1CalculatePost - guest-carts/{cartId}/totals-information | guest-carts/{cartId}/totals-information | /V1/guest-carts/{cartId}/totals-information\_\_\_post |
| CheckoutPaymentInformationManagementV1SavePaymentInformationAndPlaceOrderPost - carts/mine/payment-information | carts/mine/payment-information | /V1/carts/mine/payment-information\_\_\_post |
| CheckoutPaymentInformationManagementV1SavePaymentInformationPost - carts/mine/set-payment-information | carts/mine/set-payment-information | /V1/carts/mine/set-payment-information\_\_\_post |
| CheckoutShippingInformationManagementV1SaveAddressInformationPost - carts/mine/shipping-information | carts/mine/shipping-information | /V1/carts/mine/shipping-information\_\_\_post |
| CheckoutShippingInformationManagementV1SaveAddressInformationPost - carts/{cartId}/shipping-information | carts/{cartId}/shipping-information | /V1/carts/{cartId}/shipping-information\_\_\_post |
| CheckoutTotalsInformationManagementV1CalculatePost - carts/mine/totals-information | carts/mine/totals-information | /V1/carts/mine/totals-information\_\_\_post |
| CheckoutTotalsInformationManagementV1CalculatePost - carts/{cartId}/totals-information | carts/{cartId}/totals-information | /V1/carts/{cartId}/totals-information\_\_\_post |
| CmsBlockRepositoryV1SavePost - cmsBlock | cmsBlock | /V1/cmsBlock\_\_\_post |
| CmsPageRepositoryV1SavePost - cmsPage | cmsPage | /V1/cmsPage\_\_\_post |
| CompanyCompanyRepositoryV1SavePost - company/ | company/ | /V1/company/\_\_\_post |
| CompanyCreditCreditBalanceManagementV1DecreasePost - companyCredits/{creditId}/decreaseBalance | companyCredits/{creditId}/decreaseBalance | /V1/companyCredits/{creditId}/decreaseBalance\_\_\_post |
| CompanyCreditCreditBalanceManagementV1IncreasePost - companyCredits/{creditId}/increaseBalance | companyCredits/{creditId}/increaseBalance | /V1/companyCredits/{creditId}/increaseBalance\_\_\_post |
| CompanyRoleRepositoryV1SavePost - company/role/ | company/role/ | /V1/company/role/\_\_\_post |
| ConfigurableProductLinkManagementV1AddChildPost - configurable-products/{sku}/child | configurable-products/{sku}/child | /V1/configurable-products/{sku}/child\_\_\_post |
| ConfigurableProductOptionRepositoryV1SavePost - configurable-products/{sku}/options | configurable-products/{sku}/options | /V1/configurable-products/{sku}/options\_\_\_post |
| CustomerAccountManagementV1CreateAccountPost - customers | customers | /V1/customers\_\_\_post |
| CustomerAccountManagementV1IsEmailAvailablePost - customers/isEmailAvailable | customers/isEmailAvailable | /V1/customers/isEmailAvailable\_\_\_post |
| CustomerAccountManagementV1ResendConfirmationPost - customers/confirm | customers/confirm | /V1/customers/confirm\_\_\_post |
| CustomerAccountManagementV1ResetPasswordPost - customers/resetPassword | customers/resetPassword | /V1/customers/resetPassword\_\_\_post |
| CustomerBalanceBalanceManagementFromQuoteV1ApplyPost - carts/mine/balance/apply | carts/mine/balance/apply | /V1/carts/mine/balance/apply\_\_\_post |
| CustomerBalanceBalanceManagementFromQuoteV1UnapplyPost - carts/mine/balance/unapply | carts/mine/balance/unapply | /V1/carts/mine/balance/unapply\_\_\_post |
| CustomerGroupRepositoryV1SavePost - customerGroups | customerGroups | /V1/customerGroups\_\_\_post |
| DownloadableLinkRepositoryV1SavePost - products/{sku}/downloadable-links | products/{sku}/downloadable-links | /V1/products/{sku}/downloadable-links\_\_\_post |
| DownloadableSampleRepositoryV1SavePost - products/{sku}/downloadable-links/samples | products/{sku}/downloadable-links/samples | /V1/products/{sku}/downloadable-links/samples\_\_\_post |
| EavAttributeSetManagementV1CreatePost - eav/attribute-sets | eav/attribute-sets | /V1/eav/attribute-sets\_\_\_post |
| GiftCardAccountGiftCardAccountManagementV1SaveByQuoteIdPost - carts/mine/giftCards | carts/mine/giftCards | /V1/carts/mine/giftCards\_\_\_post |
| GiftCardAccountGuestGiftCardAccountManagementV1AddGiftCardPost - carts/guest-carts/{cartId}/giftCards | carts/guest-carts/{cartId}/giftCards | /V1/carts/guest-carts/{cartId}/giftCards\_\_\_post |
| GiftMessageCartRepositoryV1SavePost - carts/mine/gift-message | carts/mine/gift-message | /V1/carts/mine/gift-message\_\_\_post |
| GiftMessageCartRepositoryV1SavePost - carts/{cartId}/gift-message | carts/{cartId}/gift-message | /V1/carts/{cartId}/gift-message\_\_\_post |
| GiftMessageGuestCartRepositoryV1SavePost - guest-carts/{cartId}/gift-message | guest-carts/{cartId}/gift-message | /V1/guest-carts/{cartId}/gift-message\_\_\_post |
| GiftRegistryGuestCartShippingMethodManagementV1EstimateByRegistryIdPost - guest-giftregistry/{cartId}/estimate-shipping-meth... | guest-giftregistry/{cartId}/estimate-shipping-meth... | /V1/guest-giftregistry/{cartId}/estimate-shipping-methods\_\_\_post |
| GiftRegistryShippingMethodManagementV1EstimateByRegistryIdPost - giftregistry/mine/estimate-shipping-methods | giftregistry/mine/estimate-shipping-methods | /V1/giftregistry/mine/estimate-shipping-methods\_\_\_post |
| GiftWrappingWrappingRepositoryV1SavePost - gift-wrappings | gift-wrappings | /V1/gift-wrappings\_\_\_post |
| IntegrationAdminTokenServiceV1CreateAdminAccessTokenPost - integration/admin/token | integration/admin/token | /V1/integration/admin/token\_\_\_post |
| IntegrationCustomerTokenServiceV1CreateCustomerAccessTokenPost - integration/customer/token | integration/customer/token | /V1/integration/customer/token\_\_\_post |
| NegotiableQuoteBillingAddressManagementV1AssignPost - negotiable-carts/{cartId}/billing-address | negotiable-carts/{cartId}/billing-address | /V1/negotiable-carts/{cartId}/billing-address\_\_\_post |
| NegotiableQuoteGiftCardAccountManagementV1SaveByQuoteIdPost - negotiable-carts/{cartId}/giftCards | negotiable-carts/{cartId}/giftCards | /V1/negotiable-carts/{cartId}/giftCards\_\_\_post |
| NegotiableQuoteNegotiableQuoteManagementV1AdminSendPost - negotiableQuote/submitToCustomer | negotiableQuote/submitToCustomer | /V1/negotiableQuote/submitToCustomer\_\_\_post |
| NegotiableQuoteNegotiableQuoteManagementV1CreatePost - negotiableQuote/request | negotiableQuote/request | /V1/negotiableQuote/request\_\_\_post |
| NegotiableQuoteNegotiableQuoteManagementV1DeclinePost - negotiableQuote/decline | negotiableQuote/decline | /V1/negotiableQuote/decline\_\_\_post |
| NegotiableQuoteNegotiableQuotePriceManagementV1PricesUpdatedPost - negotiableQuote/pricesUpdated | negotiableQuote/pricesUpdated | /V1/negotiableQuote/pricesUpdated\_\_\_post |
| NegotiableQuotePaymentInformationManagementV1SavePaymentInformationAndPlaceOrderPost - negotiable-carts/{cartId}/payment-information | negotiable-carts/{cartId}/payment-information | /V1/negotiable-carts/{cartId}/payment-information\_\_\_post |
| NegotiableQuotePaymentInformationManagementV1SavePaymentInformationPost - negotiable-carts/{cartId}/set-payment-information | negotiable-carts/{cartId}/set-payment-information | /V1/negotiable-carts/{cartId}/set-payment-information\_\_\_post |
| NegotiableQuoteShipmentEstimationV1EstimateByExtendedAddressPost - negotiable-carts/{cartId}/estimate-shipping-method... | negotiable-carts/{cartId}/estimate-shipping-method... | /V1/negotiable-carts/{cartId}/estimate-shipping-methods\_\_\_post |
| NegotiableQuoteShippingInformationManagementV1SaveAddressInformationPost - negotiable-carts/{cartId}/shipping-information | negotiable-carts/{cartId}/shipping-information | /V1/negotiable-carts/{cartId}/shipping-information\_\_\_post |
| NegotiableQuoteShippingMethodManagementV1EstimateByAddressIdPost - negotiable-carts/{cartId}/estimate-shipping-method... | negotiable-carts/{cartId}/estimate-shipping-method... | /V1/negotiable-carts/{cartId}/estimate-shipping-methods-by-address-id\_\_\_post |
| QuoteBillingAddressManagementV1AssignPost - carts/mine/billing-address | carts/mine/billing-address | /V1/carts/mine/billing-address\_\_\_post |
| QuoteBillingAddressManagementV1AssignPost - carts/{cartId}/billing-address | carts/{cartId}/billing-address | /V1/carts/{cartId}/billing-address\_\_\_post |
| QuoteCartItemRepositoryV1SavePost - carts/mine/items | carts/mine/items | /V1/carts/mine/items\_\_\_post |
| QuoteCartItemRepositoryV1SavePost - carts/{quoteId}/items | carts/{quoteId}/items | /V1/carts/{quoteId}/items\_\_\_post |
| QuoteCartManagementV1CreateEmptyCartForCustomerPost - carts/mine | carts/mine | /V1/carts/mine\_\_\_post |
| QuoteCartManagementV1CreateEmptyCartForCustomerPost - customers/{customerId}/carts | customers/{customerId}/carts | /V1/customers/{customerId}/carts\_\_\_post |
| QuoteCartManagementV1CreateEmptyCartPost - carts/ | carts/ | /V1/carts/\_\_\_post |
| QuoteGuestBillingAddressManagementV1AssignPost - guest-carts/{cartId}/billing-address | guest-carts/{cartId}/billing-address | /V1/guest-carts/{cartId}/billing-address\_\_\_post |
| QuoteGuestCartItemRepositoryV1SavePost - guest-carts/{cartId}/items | guest-carts/{cartId}/items | /V1/guest-carts/{cartId}/items\_\_\_post |
| QuoteGuestCartManagementV1CreateEmptyCartPost - guest-carts | guest-carts | /V1/guest-carts\_\_\_post |
| QuoteGuestShipmentEstimationV1EstimateByExtendedAddressPost - guest-carts/{cartId}/estimate-shipping-methods | guest-carts/{cartId}/estimate-shipping-methods | /V1/guest-carts/{cartId}/estimate-shipping-methods\_\_\_post |
| QuoteShipmentEstimationV1EstimateByExtendedAddressPost - carts/mine/estimate-shipping-methods | carts/mine/estimate-shipping-methods | /V1/carts/mine/estimate-shipping-methods\_\_\_post |
| QuoteShipmentEstimationV1EstimateByExtendedAddressPost - carts/{cartId}/estimate-shipping-methods | carts/{cartId}/estimate-shipping-methods | /V1/carts/{cartId}/estimate-shipping-methods\_\_\_post |
| QuoteShippingMethodManagementV1EstimateByAddressIdPost - carts/mine/estimate-shipping-methods-by-address-id | carts/mine/estimate-shipping-methods-by-address-id | /V1/carts/mine/estimate-shipping-methods-by-address-id\_\_\_post |
| QuoteShippingMethodManagementV1EstimateByAddressIdPost - carts/{cartId}/estimate-shipping-methods-by-addres... | carts/{cartId}/estimate-shipping-methods-by-addres... | /V1/carts/{cartId}/estimate-shipping-methods-by-address-id\_\_\_post |
| RequisitionListRequisitionListRepositoryV1SavePost - requisition_lists | requisition_lists | /V1/requisition\_lists\_\_\_post |
| RewardRewardManagementV1SetPost - reward/mine/use-reward | reward/mine/use-reward | /V1/reward/mine/use-reward\_\_\_post |
| RmaCommentManagementV1AddCommentPost - returns/{id}/comments | returns/{id}/comments | /V1/returns/{id}/comments\_\_\_post |
| RmaRmaManagementV1SaveRmaPost - returns | returns | /V1/returns\_\_\_post |
| RmaTrackManagementV1AddTrackPost - returns/{id}/tracking-numbers | returns/{id}/tracking-numbers | /V1/returns/{id}/tracking-numbers\_\_\_post |
| SalesCreditmemoCommentRepositoryV1SavePost - creditmemo/{id}/comments | creditmemo/{id}/comments | /V1/creditmemo/{id}/comments\_\_\_post |
| SalesCreditmemoManagementV1NotifyPost - creditmemo/{id}/emails | creditmemo/{id}/emails | /V1/creditmemo/{id}/emails\_\_\_post |
| SalesCreditmemoManagementV1RefundPost - creditmemo/refund | creditmemo/refund | /V1/creditmemo/refund\_\_\_post |
| SalesCreditmemoRepositoryV1SavePost - creditmemo | creditmemo | /V1/creditmemo\_\_\_post |
| SalesInvoiceCommentRepositoryV1SavePost - invoices/comments | invoices/comments | /V1/invoices/comments\_\_\_post |
| SalesInvoiceManagementV1NotifyPost - invoices/{id}/emails | invoices/{id}/emails | /V1/invoices/{id}/emails\_\_\_post |
| SalesInvoiceManagementV1SetCapturePost - invoices/{id}/capture | invoices/{id}/capture | /V1/invoices/{id}/capture\_\_\_post |
| SalesInvoiceManagementV1SetVoidPost - invoices/{id}/void | invoices/{id}/void | /V1/invoices/{id}/void\_\_\_post |
| SalesInvoiceOrderV1ExecutePost - order/{orderId}/invoice | order/{orderId}/invoice | /V1/order/{orderId}/invoice\_\_\_post |
| SalesInvoiceRepositoryV1SavePost - invoices/ | invoices/ | /V1/invoices/\_\_\_post |
| SalesOrderManagementV1AddCommentPost - orders/{id}/comments | orders/{id}/comments | /V1/orders/{id}/comments\_\_\_post |
| SalesOrderManagementV1CancelPost - orders/{id}/cancel | orders/{id}/cancel | /V1/orders/{id}/cancel\_\_\_post |
| SalesOrderManagementV1HoldPost - orders/{id}/hold | orders/{id}/hold | /V1/orders/{id}/hold\_\_\_post |
| SalesOrderManagementV1NotifyPost - orders/{id}/emails | orders/{id}/emails | /V1/orders/{id}/emails\_\_\_post |
| SalesOrderManagementV1UnHoldPost - orders/{id}/unhold | orders/{id}/unhold | /V1/orders/{id}/unhold\_\_\_post |
| SalesOrderRepositoryV1SavePost - orders/ | orders/ | /V1/orders/\_\_\_post |
| SalesRefundInvoiceV1ExecutePost - invoice/{invoiceId}/refund | invoice/{invoiceId}/refund | /V1/invoice/{invoiceId}/refund\_\_\_post |
| SalesRefundOrderV1ExecutePost - order/{orderId}/refund | order/{orderId}/refund | /V1/order/{orderId}/refund\_\_\_post |
| SalesRuleCouponManagementV1DeleteByCodesPost - coupons/deleteByCodes | coupons/deleteByCodes | /V1/coupons/deleteByCodes\_\_\_post |
| SalesRuleCouponManagementV1DeleteByIdsPost - coupons/deleteByIds | coupons/deleteByIds | /V1/coupons/deleteByIds\_\_\_post |
| SalesRuleCouponManagementV1GeneratePost - coupons/generate | coupons/generate | /V1/coupons/generate\_\_\_post |
| SalesRuleCouponRepositoryV1SavePost - coupons | coupons | /V1/coupons\_\_\_post |
| SalesRuleRuleRepositoryV1SavePost - salesRules | salesRules | /V1/salesRules\_\_\_post |
| SalesShipmentCommentRepositoryV1SavePost - shipment/{id}/comments | shipment/{id}/comments | /V1/shipment/{id}/comments\_\_\_post |
| SalesShipmentManagementV1NotifyPost - shipment/{id}/emails | shipment/{id}/emails | /V1/shipment/{id}/emails\_\_\_post |
| SalesShipmentRepositoryV1SavePost - shipment/ | shipment/ | /V1/shipment/\_\_\_post |
| SalesShipmentTrackRepositoryV1SavePost - shipment/track | shipment/track | /V1/shipment/track\_\_\_post |
| SalesShipOrderV1ExecutePost - order/{orderId}/ship | order/{orderId}/ship | /V1/order/{orderId}/ship\_\_\_post |
| SharedCatalogCategoryManagementV1AssignCategoriesPost - sharedCatalog/{id}/assignCategories | sharedCatalog/{id}/assignCategories | /V1/sharedCatalog/{id}/assignCategories\_\_\_post |
| SharedCatalogCategoryManagementV1UnassignCategoriesPost - sharedCatalog/{id}/unassignCategories | sharedCatalog/{id}/unassignCategories | /V1/sharedCatalog/{id}/unassignCategories\_\_\_post |
| SharedCatalogCompanyManagementV1AssignCompaniesPost - sharedCatalog/{sharedCatalogId}/assignCompanies | sharedCatalog/{sharedCatalogId}/assignCompanies | /V1/sharedCatalog/{sharedCatalogId}/assignCompanies\_\_\_post |
| SharedCatalogCompanyManagementV1UnassignCompaniesPost - sharedCatalog/{sharedCatalogId}/unassignCompanies | sharedCatalog/{sharedCatalogId}/unassignCompanies | /V1/sharedCatalog/{sharedCatalogId}/unassignCompanies\_\_\_post |
| SharedCatalogProductManagementV1AssignProductsPost - sharedCatalog/{id}/assignProducts | sharedCatalog/{id}/assignProducts | /V1/sharedCatalog/{id}/assignProducts\_\_\_post |
| SharedCatalogProductManagementV1UnassignProductsPost - sharedCatalog/{id}/unassignProducts | sharedCatalog/{id}/unassignProducts | /V1/sharedCatalog/{id}/unassignProducts\_\_\_post |
| SharedCatalogSharedCatalogRepositoryV1SavePost - sharedCatalog | sharedCatalog | /V1/sharedCatalog\_\_\_post |
| TaxTaxClassRepositoryV1SavePost - taxClasses | taxClasses | /V1/taxClasses\_\_\_post |
| TaxTaxRateRepositoryV1SavePost - taxRates | taxRates | /V1/taxRates\_\_\_post |
| TaxTaxRuleRepositoryV1SavePost - taxRules | taxRules | /V1/taxRules\_\_\_post |
| TemandoShippingCollectionPointCartCollectionPointManagementV1SelectCollectionPointPost - carts/mine/collection-point/select | carts/mine/collection-point/select | /V1/carts/mine/collection-point/select\_\_\_post |
| TemandoShippingCollectionPointGuestCartCollectionPointManagementV1SelectCollectionPointPost - guest-carts/{cartId}/collection-point/select | guest-carts/{cartId}/collection-point/select | /V1/guest-carts/{cartId}/collection-point/select\_\_\_post |
| TemandoShippingQuoteCartCheckoutFieldManagementV1SaveCheckoutFieldsPost - carts/mine/checkout-fields | carts/mine/checkout-fields | /V1/carts/mine/checkout-fields\_\_\_post |
| TemandoShippingQuoteCartDeliveryOptionManagementV1SavePost - carts/mine/delivery-option | carts/mine/delivery-option | /V1/carts/mine/delivery-option\_\_\_post |
| TemandoShippingQuoteGuestCartCheckoutFieldManagementV1SaveCheckoutFieldsPost - guest-carts/{cartId}/checkout-fields | guest-carts/{cartId}/checkout-fields | /V1/guest-carts/{cartId}/checkout-fields\_\_\_post |
| TemandoShippingQuoteGuestCartDeliveryOptionManagementV1SavePost - guest-carts/{cartId}/delivery-option | guest-carts/{cartId}/delivery-option | /V1/guest-carts/{cartId}/delivery-option\_\_\_post |
| WorldpayGuestPaymentInformationManagementProxyV1SavePaymentInformationAndPlaceOrderPost - worldpay-guest-carts/{cartId}/payment-information | worldpay-guest-carts/{cartId}/payment-information | /V1/worldpay-guest-carts/{cartId}/payment-information\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| AmazonPaymentOrderInformationManagementV1RemoveOrderReferenceDelete - amazon/order-ref | amazon/order-ref | /V1/amazon/order-ref\_\_\_delete |
| BundleProductLinkManagementV1RemoveChildDelete - bundle-products/{sku}/options/{optionId}/children/... | bundle-products/{sku}/options/{optionId}/children/... | /V1/bundle-products/{sku}/options/{optionId}/children/{childSku}\_\_\_delete |
| BundleProductOptionRepositoryV1DeleteByIdDelete - bundle-products/{sku}/options/{optionId} | bundle-products/{sku}/options/{optionId} | /V1/bundle-products/{sku}/options/{optionId}\_\_\_delete |
| CatalogAttributeSetRepositoryV1DeleteByIdDelete - products/attribute-sets/{attributeSetId} | products/attribute-sets/{attributeSetId} | /V1/products/attribute-sets/{attributeSetId}\_\_\_delete |
| CatalogCategoryLinkRepositoryV1DeleteByIdsDelete - categories/{categoryId}/products/{sku} | categories/{categoryId}/products/{sku} | /V1/categories/{categoryId}/products/{sku}\_\_\_delete |
| CatalogCategoryRepositoryV1DeleteByIdentifierDelete - categories/{categoryId} | categories/{categoryId} | /V1/categories/{categoryId}\_\_\_delete |
| CatalogProductAttributeGroupRepositoryV1DeleteByIdDelete - products/attribute-sets/groups/{groupId} | products/attribute-sets/groups/{groupId} | /V1/products/attribute-sets/groups/{groupId}\_\_\_delete |
| CatalogProductAttributeManagementV1UnassignDelete - products/attribute-sets/{attributeSetId}/attribute... | products/attribute-sets/{attributeSetId}/attribute... | /V1/products/attribute-sets/{attributeSetId}/attributes/{attributeCode}\_\_\_delete |
| CatalogProductAttributeMediaGalleryManagementV1RemoveDelete - products/{sku}/media/{entryId} | products/{sku}/media/{entryId} | /V1/products/{sku}/media/{entryId}\_\_\_delete |
| CatalogProductAttributeOptionManagementV1DeleteDelete - products/attributes/{attributeCode}/options/{optio... | products/attributes/{attributeCode}/options/{optio... | /V1/products/attributes/{attributeCode}/options/{optionId}\_\_\_delete |
| CatalogProductAttributeRepositoryV1DeleteByIdDelete - products/attributes/{attributeCode} | products/attributes/{attributeCode} | /V1/products/attributes/{attributeCode}\_\_\_delete |
| CatalogProductCustomOptionRepositoryV1DeleteByIdentifierDelete - products/{sku}/options/{optionId} | products/{sku}/options/{optionId} | /V1/products/{sku}/options/{optionId}\_\_\_delete |
| CatalogProductLinkRepositoryV1DeleteByIdDelete - products/{sku}/links/{type}/{linkedProductSku} | products/{sku}/links/{type}/{linkedProductSku} | /V1/products/{sku}/links/{type}/{linkedProductSku}\_\_\_delete |
| CatalogProductRepositoryV1DeleteByIdDelete - products/{sku} | products/{sku} | /V1/products/{sku}\_\_\_delete |
| CatalogProductTierPriceManagementV1RemoveDelete - products/{sku}/group-prices/{customerGroupId}/tier... | products/{sku}/group-prices/{customerGroupId}/tier... | /V1/products/{sku}/group-prices/{customerGroupId}/tiers/{qty}\_\_\_delete |
| CatalogProductWebsiteLinkRepositoryV1DeleteByIdDelete - products/{sku}/websites/{websiteId} | products/{sku}/websites/{websiteId} | /V1/products/{sku}/websites/{websiteId}\_\_\_delete |
| CmsBlockRepositoryV1DeleteByIdDelete - cmsBlock/{blockId} | cmsBlock/{blockId} | /V1/cmsBlock/{blockId}\_\_\_delete |
| CmsPageRepositoryV1DeleteByIdDelete - cmsPage/{pageId} | cmsPage/{pageId} | /V1/cmsPage/{pageId}\_\_\_delete |
| CompanyCompanyRepositoryV1DeleteByIdDelete - company/{companyId} | company/{companyId} | /V1/company/{companyId}\_\_\_delete |
| CompanyRoleRepositoryV1DeleteDelete - company/role/{roleId} | company/role/{roleId} | /V1/company/role/{roleId}\_\_\_delete |
| CompanyTeamRepositoryV1DeleteByIdDelete - team/{teamId} | team/{teamId} | /V1/team/{teamId}\_\_\_delete |
| ConfigurableProductLinkManagementV1RemoveChildDelete - configurable-products/{sku}/children/{childSku} | configurable-products/{sku}/children/{childSku} | /V1/configurable-products/{sku}/children/{childSku}\_\_\_delete |
| ConfigurableProductOptionRepositoryV1DeleteByIdDelete - configurable-products/{sku}/options/{id} | configurable-products/{sku}/options/{id} | /V1/configurable-products/{sku}/options/{id}\_\_\_delete |
| CustomerAddressRepositoryV1DeleteByIdDelete - addresses/{addressId} | addresses/{addressId} | /V1/addresses/{addressId}\_\_\_delete |
| CustomerCustomerRepositoryV1DeleteByIdDelete - customers/{customerId} | customers/{customerId} | /V1/customers/{customerId}\_\_\_delete |
| CustomerGroupRepositoryV1DeleteByIdDelete - customerGroups/{id} | customerGroups/{id} | /V1/customerGroups/{id}\_\_\_delete |
| DownloadableLinkRepositoryV1DeleteDelete - products/downloadable-links/{id} | products/downloadable-links/{id} | /V1/products/downloadable-links/{id}\_\_\_delete |
| DownloadableSampleRepositoryV1DeleteDelete - products/downloadable-links/samples/{id} | products/downloadable-links/samples/{id} | /V1/products/downloadable-links/samples/{id}\_\_\_delete |
| EavAttributeSetRepositoryV1DeleteByIdDelete - eav/attribute-sets/{attributeSetId} | eav/attribute-sets/{attributeSetId} | /V1/eav/attribute-sets/{attributeSetId}\_\_\_delete |
| GiftCardAccountGiftCardAccountManagementV1DeleteByQuoteIdDelete - carts/mine/giftCards/{giftCardCode} | carts/mine/giftCards/{giftCardCode} | /V1/carts/mine/giftCards/{giftCardCode}\_\_\_delete |
| GiftCardAccountGiftCardAccountManagementV1DeleteByQuoteIdDelete - carts/{cartId}/giftCards/{giftCardCode} | carts/{cartId}/giftCards/{giftCardCode} | /V1/carts/{cartId}/giftCards/{giftCardCode}\_\_\_delete |
| GiftCardAccountGuestGiftCardAccountManagementV1DeleteByQuoteIdDelete - carts/guest-carts/{cartId}/giftCards/{giftCardCode... | carts/guest-carts/{cartId}/giftCards/{giftCardCode... | /V1/carts/guest-carts/{cartId}/giftCards/{giftCardCode}\_\_\_delete |
| GiftWrappingWrappingRepositoryV1DeleteByIdDelete - gift-wrappings/{id} | gift-wrappings/{id} | /V1/gift-wrappings/{id}\_\_\_delete |
| NegotiableQuoteCouponManagementV1RemoveDelete - negotiable-carts/{cartId}/coupons | negotiable-carts/{cartId}/coupons | /V1/negotiable-carts/{cartId}/coupons\_\_\_delete |
| NegotiableQuoteGiftCardAccountManagementV1DeleteByQuoteIdDelete - negotiable-carts/{cartId}/giftCards/{giftCardCode} | negotiable-carts/{cartId}/giftCards/{giftCardCode} | /V1/negotiable-carts/{cartId}/giftCards/{giftCardCode}\_\_\_delete |
| QuoteCartItemRepositoryV1DeleteByIdDelete - carts/mine/items/{itemId} | carts/mine/items/{itemId} | /V1/carts/mine/items/{itemId}\_\_\_delete |
| QuoteCartItemRepositoryV1DeleteByIdDelete - carts/{cartId}/items/{itemId} | carts/{cartId}/items/{itemId} | /V1/carts/{cartId}/items/{itemId}\_\_\_delete |
| QuoteCouponManagementV1RemoveDelete - carts/mine/coupons | carts/mine/coupons | /V1/carts/mine/coupons\_\_\_delete |
| QuoteCouponManagementV1RemoveDelete - carts/{cartId}/coupons | carts/{cartId}/coupons | /V1/carts/{cartId}/coupons\_\_\_delete |
| QuoteGuestCartItemRepositoryV1DeleteByIdDelete - guest-carts/{cartId}/items/{itemId} | guest-carts/{cartId}/items/{itemId} | /V1/guest-carts/{cartId}/items/{itemId}\_\_\_delete |
| QuoteGuestCouponManagementV1RemoveDelete - guest-carts/{cartId}/coupons | guest-carts/{cartId}/coupons | /V1/guest-carts/{cartId}/coupons\_\_\_delete |
| RmaRmaRepositoryV1DeleteDelete - returns/{id} | returns/{id} | /V1/returns/{id}\_\_\_delete |
| RmaTrackManagementV1RemoveTrackByIdDelete - returns/{id}/tracking-numbers/{trackId} | returns/{id}/tracking-numbers/{trackId} | /V1/returns/{id}/tracking-numbers/{trackId}\_\_\_delete |
| SalesRuleCouponRepositoryV1DeleteByIdDelete - coupons/{couponId} | coupons/{couponId} | /V1/coupons/{couponId}\_\_\_delete |
| SalesRuleRuleRepositoryV1DeleteByIdDelete - salesRules/{ruleId} | salesRules/{ruleId} | /V1/salesRules/{ruleId}\_\_\_delete |
| SalesShipmentTrackRepositoryV1DeleteByIdDelete - shipment/track/{id} | shipment/track/{id} | /V1/shipment/track/{id}\_\_\_delete |
| SharedCatalogSharedCatalogRepositoryV1DeleteByIdDelete - sharedCatalog/{sharedCatalogId} | sharedCatalog/{sharedCatalogId} | /V1/sharedCatalog/{sharedCatalogId}\_\_\_delete |
| TaxTaxClassRepositoryV1DeleteByIdDelete - taxClasses/{taxClassId} | taxClasses/{taxClassId} | /V1/taxClasses/{taxClassId}\_\_\_delete |
| TaxTaxRateRepositoryV1DeleteByIdDelete - taxRates/{rateId} | taxRates/{rateId} | /V1/taxRates/{rateId}\_\_\_delete |
| TaxTaxRuleRepositoryV1DeleteByIdDelete - taxRules/{ruleId} | taxRules/{ruleId} | /V1/taxRules/{ruleId}\_\_\_delete |
| TemandoShippingCollectionPointCartCollectionPointManagementV1DeleteSearchRequestDelete - carts/mine/collection-point/search-request | carts/mine/collection-point/search-request | /V1/carts/mine/collection-point/search-request\_\_\_delete |
| TemandoShippingCollectionPointGuestCartCollectionPointManagementV1DeleteSearchRequestDelete - guest-carts/{cartId}/collection-point/search-reque... | guest-carts/{cartId}/collection-point/search-reque... | /V1/guest-carts/{cartId}/collection-point/search-request\_\_\_delete |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| AsynchronousOperationsBulkStatusV1GetOperationsCountByBulkIdAndStatusGet - bulk/{bulkUuid}/operation-status/{status} | bulk/{bulkUuid}/operation-status/{status} | /V1/bulk/{bulkUuid}/operation-status/{status}\_\_\_get |
| BundleProductOptionRepositoryV1GetGet - bundle-products/{sku}/options/{optionId} | bundle-products/{sku}/options/{optionId} | /V1/bundle-products/{sku}/options/{optionId}\_\_\_get |
| CatalogAttributeSetRepositoryV1GetGet - products/attribute-sets/{attributeSetId} | products/attribute-sets/{attributeSetId} | /V1/products/attribute-sets/{attributeSetId}\_\_\_get |
| CatalogCategoryAttributeRepositoryV1GetGet - categories/attributes/{attributeCode} | categories/attributes/{attributeCode} | /V1/categories/attributes/{attributeCode}\_\_\_get |
| CatalogCategoryRepositoryV1GetGet - categories/{categoryId} | categories/{categoryId} | /V1/categories/{categoryId}\_\_\_get |
| CatalogInventoryStockRegistryV1GetStockItemBySkuGet - stockItems/{productSku} | stockItems/{productSku} | /V1/stockItems/{productSku}\_\_\_get |
| CatalogInventoryStockRegistryV1GetStockStatusBySkuGet - stockStatuses/{productSku} | stockStatuses/{productSku} | /V1/stockStatuses/{productSku}\_\_\_get |
| CatalogProductAttributeMediaGalleryManagementV1GetGet - products/{sku}/media/{entryId} | products/{sku}/media/{entryId} | /V1/products/{sku}/media/{entryId}\_\_\_get |
| CatalogProductAttributeRepositoryV1GetGet - products/attributes/{attributeCode} | products/attributes/{attributeCode} | /V1/products/attributes/{attributeCode}\_\_\_get |
| CatalogProductCustomOptionRepositoryV1GetGet - products/{sku}/options/{optionId} | products/{sku}/options/{optionId} | /V1/products/{sku}/options/{optionId}\_\_\_get |
| CatalogProductLinkManagementV1GetLinkedItemsByTypeGet - products/{sku}/links/{type} | products/{sku}/links/{type} | /V1/products/{sku}/links/{type}\_\_\_get |
| CatalogProductMediaAttributeManagementV1GetListGet - products/media/types/{attributeSetName} | products/media/types/{attributeSetName} | /V1/products/media/types/{attributeSetName}\_\_\_get |
| CatalogProductRepositoryV1GetGet - products/{sku} | products/{sku} | /V1/products/{sku}\_\_\_get |
| CmsBlockRepositoryV1GetByIdGet - cmsBlock/{blockId} | cmsBlock/{blockId} | /V1/cmsBlock/{blockId}\_\_\_get |
| CmsPageRepositoryV1GetByIdGet - cmsPage/{pageId} | cmsPage/{pageId} | /V1/cmsPage/{pageId}\_\_\_get |
| CompanyCompanyHierarchyV1GetCompanyHierarchyGet - hierarchy/{id} | hierarchy/{id} | /V1/hierarchy/{id}\_\_\_get |
| CompanyCompanyRepositoryV1GetGet - company/{companyId} | company/{companyId} | /V1/company/{companyId}\_\_\_get |
| CompanyCreditCreditLimitManagementV1GetCreditByCompanyIdGet - companyCredits/company/{companyId} | companyCredits/company/{companyId} | /V1/companyCredits/company/{companyId}\_\_\_get |
| CompanyCreditCreditLimitRepositoryV1GetGet - companyCredits/{creditId} | companyCredits/{creditId} | /V1/companyCredits/{creditId}\_\_\_get |
| CompanyRoleRepositoryV1GetGet - company/role/{roleId} | company/role/{roleId} | /V1/company/role/{roleId}\_\_\_get |
| CompanyTeamRepositoryV1GetGet - team/{teamId} | team/{teamId} | /V1/team/{teamId}\_\_\_get |
| ConfigurableProductOptionRepositoryV1GetGet - configurable-products/{sku}/options/{id} | configurable-products/{sku}/options/{id} | /V1/configurable-products/{sku}/options/{id}\_\_\_get |
| CustomerAccountManagementV1ValidateResetPasswordLinkTokenGet - customers/{customerId}/password/resetLinkToken/{re... | customers/{customerId}/password/resetLinkToken/{re... | /V1/customers/{customerId}/password/resetLinkToken/{resetPasswordLinkToken}\_\_\_get |
| CustomerAddressMetadataV1GetAttributeMetadataGet - attributeMetadata/customerAddress/attribute/{attri... | attributeMetadata/customerAddress/attribute/{attri... | /V1/attributeMetadata/customerAddress/attribute/{attributeCode}\_\_\_get |
| CustomerAddressMetadataV1GetAttributesGet - attributeMetadata/customerAddress/form/{formCode} | attributeMetadata/customerAddress/form/{formCode} | /V1/attributeMetadata/customerAddress/form/{formCode}\_\_\_get |
| CustomerAddressRepositoryV1GetByIdGet - customers/addresses/{addressId} | customers/addresses/{addressId} | /V1/customers/addresses/{addressId}\_\_\_get |
| CustomerCustomerMetadataV1GetAttributeMetadataGet - attributeMetadata/customer/attribute/{attributeCod... | attributeMetadata/customer/attribute/{attributeCod... | /V1/attributeMetadata/customer/attribute/{attributeCode}\_\_\_get |
| CustomerCustomerMetadataV1GetAttributesGet - attributeMetadata/customer/form/{formCode} | attributeMetadata/customer/form/{formCode} | /V1/attributeMetadata/customer/form/{formCode}\_\_\_get |
| CustomerCustomerRepositoryV1GetByIdGet - customers/{customerId} | customers/{customerId} | /V1/customers/{customerId}\_\_\_get |
| CustomerGroupManagementV1GetDefaultGroupGet - customerGroups/default/{storeId} | customerGroups/default/{storeId} | /V1/customerGroups/default/{storeId}\_\_\_get |
| CustomerGroupRepositoryV1GetByIdGet - customerGroups/{id} | customerGroups/{id} | /V1/customerGroups/{id}\_\_\_get |
| DirectoryCountryInformationAcquirerV1GetCountryInfoGet - directory/countries/{countryId} | directory/countries/{countryId} | /V1/directory/countries/{countryId}\_\_\_get |
| EavAttributeSetRepositoryV1GetGet - eav/attribute-sets/{attributeSetId} | eav/attribute-sets/{attributeSetId} | /V1/eav/attribute-sets/{attributeSetId}\_\_\_get |
| GiftCardAccountGiftCardAccountManagementV1CheckGiftCardGet - carts/mine/checkGiftCard/{giftCardCode} | carts/mine/checkGiftCard/{giftCardCode} | /V1/carts/mine/checkGiftCard/{giftCardCode}\_\_\_get |
| GiftCardAccountGuestGiftCardAccountManagementV1CheckGiftCardGet - carts/guest-carts/{cartId}/checkGiftCard/{giftCard... | carts/guest-carts/{cartId}/checkGiftCard/{giftCard... | /V1/carts/guest-carts/{cartId}/checkGiftCard/{giftCardCode}\_\_\_get |
| GiftMessageGuestItemRepositoryV1GetGet - guest-carts/{cartId}/gift-message/{itemId} | guest-carts/{cartId}/gift-message/{itemId} | /V1/guest-carts/{cartId}/gift-message/{itemId}\_\_\_get |
| GiftMessageItemRepositoryV1GetGet - carts/mine/gift-message/{itemId} | carts/mine/gift-message/{itemId} | /V1/carts/mine/gift-message/{itemId}\_\_\_get |
| GiftMessageItemRepositoryV1GetGet - carts/{cartId}/gift-message/{itemId} | carts/{cartId}/gift-message/{itemId} | /V1/carts/{cartId}/gift-message/{itemId}\_\_\_get |
| GiftWrappingWrappingRepositoryV1GetGet - gift-wrappings/{id} | gift-wrappings/{id} | /V1/gift-wrappings/{id}\_\_\_get |
| QuoteCartRepositoryV1GetGet - carts/{cartId} | carts/{cartId} | /V1/carts/{cartId}\_\_\_get |
| QuoteGuestCartRepositoryV1GetGet - guest-carts/{cartId} | guest-carts/{cartId} | /V1/guest-carts/{cartId}\_\_\_get |
| RmaRmaAttributesManagementV1GetAttributeMetadataGet - returnsAttributeMetadata/{attributeCode} | returnsAttributeMetadata/{attributeCode} | /V1/returnsAttributeMetadata/{attributeCode}\_\_\_get |
| RmaRmaAttributesManagementV1GetAttributesGet - returnsAttributeMetadata/form/{formCode} | returnsAttributeMetadata/form/{formCode} | /V1/returnsAttributeMetadata/form/{formCode}\_\_\_get |
| RmaRmaRepositoryV1GetGet - returns/{id} | returns/{id} | /V1/returns/{id}\_\_\_get |
| SalesCreditmemoRepositoryV1GetGet - creditmemo/{id} | creditmemo/{id} | /V1/creditmemo/{id}\_\_\_get |
| SalesInvoiceRepositoryV1GetGet - invoices/{id} | invoices/{id} | /V1/invoices/{id}\_\_\_get |
| SalesOrderItemRepositoryV1GetGet - orders/items/{id} | orders/items/{id} | /V1/orders/items/{id}\_\_\_get |
| SalesOrderRepositoryV1GetGet - orders/{id} | orders/{id} | /V1/orders/{id}\_\_\_get |
| SalesRuleCouponRepositoryV1GetByIdGet - coupons/{couponId} | coupons/{couponId} | /V1/coupons/{couponId}\_\_\_get |
| SalesRuleRuleRepositoryV1GetByIdGet - salesRules/{ruleId} | salesRules/{ruleId} | /V1/salesRules/{ruleId}\_\_\_get |
| SalesShipmentRepositoryV1GetGet - shipment/{id} | shipment/{id} | /V1/shipment/{id}\_\_\_get |
| SalesTransactionRepositoryV1GetGet - transactions/{id} | transactions/{id} | /V1/transactions/{id}\_\_\_get |
| SharedCatalogSharedCatalogRepositoryV1GetGet - sharedCatalog/{sharedCatalogId} | sharedCatalog/{sharedCatalogId} | /V1/sharedCatalog/{sharedCatalogId}\_\_\_get |
| TaxTaxClassRepositoryV1GetGet - taxClasses/{taxClassId} | taxClasses/{taxClassId} | /V1/taxClasses/{taxClassId}\_\_\_get |
| TaxTaxRateRepositoryV1GetGet - taxRates/{rateId} | taxRates/{rateId} | /V1/taxRates/{rateId}\_\_\_get |
| TaxTaxRuleRepositoryV1GetGet - taxRules/{ruleId} | taxRules/{ruleId} | /V1/taxRules/{ruleId}\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| AnalyticsLinkProviderV1GetGet - analytics/link | analytics/link | /V1/analytics/link\_\_\_get |
| AsynchronousOperationsBulkStatusV1GetBulkDetailedStatusGet - bulk/{bulkUuid}/detailed-status | bulk/{bulkUuid}/detailed-status | /V1/bulk/{bulkUuid}/detailed-status\_\_\_get |
| AsynchronousOperationsBulkStatusV1GetBulkShortStatusGet - bulk/{bulkUuid}/status | bulk/{bulkUuid}/status | /V1/bulk/{bulkUuid}/status\_\_\_get |
| BackendModuleServiceV1GetModulesGet - modules | modules | /V1/modules\_\_\_get |
| BundleProductLinkManagementV1GetChildrenGet - bundle-products/{productSku}/children | bundle-products/{productSku}/children | /V1/bundle-products/{productSku}/children\_\_\_get |
| BundleProductOptionRepositoryV1GetListGet - bundle-products/{sku}/options/all | bundle-products/{sku}/options/all | /V1/bundle-products/{sku}/options/all\_\_\_get |
| BundleProductOptionTypeListV1GetItemsGet - bundle-products/options/types | bundle-products/options/types | /V1/bundle-products/options/types\_\_\_get |
| CatalogAttributeSetRepositoryV1GetListGet - products/attribute-sets/sets/list | products/attribute-sets/sets/list | /V1/products/attribute-sets/sets/list\_\_\_get |
| CatalogCategoryAttributeOptionManagementV1GetItemsGet - categories/attributes/{attributeCode}/options | categories/attributes/{attributeCode}/options | /V1/categories/attributes/{attributeCode}/options\_\_\_get |
| CatalogCategoryAttributeRepositoryV1GetListGet - categories/attributes | categories/attributes | /V1/categories/attributes\_\_\_get |
| CatalogCategoryLinkManagementV1GetAssignedProductsGet - categories/{categoryId}/products | categories/{categoryId}/products | /V1/categories/{categoryId}/products\_\_\_get |
| CatalogCategoryListV1GetListGet - categories/list | categories/list | /V1/categories/list\_\_\_get |
| CatalogCategoryManagementV1GetTreeGet - categories | categories | /V1/categories\_\_\_get |
| CatalogInventoryStockRegistryV1GetLowStockItemsGet - stockItems/lowStock/ | stockItems/lowStock/ | /V1/stockItems/lowStock/\_\_\_get |
| CatalogProductAttributeGroupRepositoryV1GetListGet - products/attribute-sets/groups/list | products/attribute-sets/groups/list | /V1/products/attribute-sets/groups/list\_\_\_get |
| CatalogProductAttributeManagementV1GetAttributesGet - products/attribute-sets/{attributeSetId}/attribute... | products/attribute-sets/{attributeSetId}/attribute... | /V1/products/attribute-sets/{attributeSetId}/attributes\_\_\_get |
| CatalogProductAttributeMediaGalleryManagementV1GetListGet - products/{sku}/media | products/{sku}/media | /V1/products/{sku}/media\_\_\_get |
| CatalogProductAttributeOptionManagementV1GetItemsGet - products/attributes/{attributeCode}/options | products/attributes/{attributeCode}/options | /V1/products/attributes/{attributeCode}/options\_\_\_get |
| CatalogProductAttributeRepositoryV1GetListGet - products/attributes | products/attributes | /V1/products/attributes\_\_\_get |
| CatalogProductAttributeTypesListV1GetItemsGet - products/attributes/types | products/attributes/types | /V1/products/attributes/types\_\_\_get |
| CatalogProductCustomOptionRepositoryV1GetListGet - products/{sku}/options | products/{sku}/options | /V1/products/{sku}/options\_\_\_get |
| CatalogProductCustomOptionTypeListV1GetItemsGet - products/options/types | products/options/types | /V1/products/options/types\_\_\_get |
| CatalogProductLinkTypeListV1GetItemAttributesGet - products/links/{type}/attributes | products/links/{type}/attributes | /V1/products/links/{type}/attributes\_\_\_get |
| CatalogProductLinkTypeListV1GetItemsGet - products/links/types | products/links/types | /V1/products/links/types\_\_\_get |
| CatalogProductRenderListV1GetListGet - products-render-info | products-render-info | /V1/products-render-info\_\_\_get |
| CatalogProductRepositoryV1GetListGet - products | products | /V1/products\_\_\_get |
| CatalogProductTierPriceManagementV1GetListGet - products/{sku}/group-prices/{customerGroupId}/tier... | products/{sku}/group-prices/{customerGroupId}/tier... | /V1/products/{sku}/group-prices/{customerGroupId}/tiers\_\_\_get |
| CatalogProductTypeListV1GetProductTypesGet - products/types | products/types | /V1/products/types\_\_\_get |
| CheckoutAgreementsCheckoutAgreementsRepositoryV1GetListGet - carts/licence | carts/licence | /V1/carts/licence\_\_\_get |
| CheckoutGuestPaymentInformationManagementV1GetPaymentInformationGet - guest-carts/{cartId}/payment-information | guest-carts/{cartId}/payment-information | /V1/guest-carts/{cartId}/payment-information\_\_\_get |
| CheckoutPaymentInformationManagementV1GetPaymentInformationGet - carts/mine/payment-information | carts/mine/payment-information | /V1/carts/mine/payment-information\_\_\_get |
| CmsBlockRepositoryV1GetListGet - cmsBlock/search | cmsBlock/search | /V1/cmsBlock/search\_\_\_get |
| CmsPageRepositoryV1GetListGet - cmsPage/search | cmsPage/search | /V1/cmsPage/search\_\_\_get |
| CompanyAclV1GetUsersByRoleIdGet - company/role/{roleId}/users | company/role/{roleId}/users | /V1/company/role/{roleId}/users\_\_\_get |
| CompanyCompanyRepositoryV1GetListGet - company/ | company/ | /V1/company/\_\_\_get |
| CompanyCreditCreditHistoryManagementV1GetListGet - companyCredits/history | companyCredits/history | /V1/companyCredits/history\_\_\_get |
| CompanyCreditCreditLimitRepositoryV1GetListGet - companyCredits/ | companyCredits/ | /V1/companyCredits/\_\_\_get |
| CompanyRoleRepositoryV1GetListGet - company/role/ | company/role/ | /V1/company/role/\_\_\_get |
| CompanyTeamRepositoryV1GetListGet - team/ | team/ | /V1/team/\_\_\_get |
| ConfigurableProductLinkManagementV1GetChildrenGet - configurable-products/{sku}/children | configurable-products/{sku}/children | /V1/configurable-products/{sku}/children\_\_\_get |
| ConfigurableProductOptionRepositoryV1GetListGet - configurable-products/{sku}/options/all | configurable-products/{sku}/options/all | /V1/configurable-products/{sku}/options/all\_\_\_get |
| CustomerAccountManagementV1GetConfirmationStatusGet - customers/{customerId}/confirm | customers/{customerId}/confirm | /V1/customers/{customerId}/confirm\_\_\_get |
| CustomerAccountManagementV1GetDefaultBillingAddressGet - customers/me/billingAddress | customers/me/billingAddress | /V1/customers/me/billingAddress\_\_\_get |
| CustomerAccountManagementV1GetDefaultBillingAddressGet - customers/{customerId}/billingAddress | customers/{customerId}/billingAddress | /V1/customers/{customerId}/billingAddress\_\_\_get |
| CustomerAccountManagementV1GetDefaultShippingAddressGet - customers/me/shippingAddress | customers/me/shippingAddress | /V1/customers/me/shippingAddress\_\_\_get |
| CustomerAccountManagementV1GetDefaultShippingAddressGet - customers/{customerId}/shippingAddress | customers/{customerId}/shippingAddress | /V1/customers/{customerId}/shippingAddress\_\_\_get |
| CustomerAccountManagementV1IsReadonlyGet - customers/{customerId}/permissions/readonly | customers/{customerId}/permissions/readonly | /V1/customers/{customerId}/permissions/readonly\_\_\_get |
| CustomerAddressMetadataV1GetAllAttributesMetadataGet - attributeMetadata/customerAddress | attributeMetadata/customerAddress | /V1/attributeMetadata/customerAddress\_\_\_get |
| CustomerAddressMetadataV1GetCustomAttributesMetadataGet - attributeMetadata/customerAddress/custom | attributeMetadata/customerAddress/custom | /V1/attributeMetadata/customerAddress/custom\_\_\_get |
| CustomerCustomerMetadataV1GetAllAttributesMetadataGet - attributeMetadata/customer | attributeMetadata/customer | /V1/attributeMetadata/customer\_\_\_get |
| CustomerCustomerMetadataV1GetCustomAttributesMetadataGet - attributeMetadata/customer/custom | attributeMetadata/customer/custom | /V1/attributeMetadata/customer/custom\_\_\_get |
| CustomerCustomerRepositoryV1GetByIdGet - customers/me | customers/me | /V1/customers/me\_\_\_get |
| CustomerCustomerRepositoryV1GetListGet - customers/search | customers/search | /V1/customers/search\_\_\_get |
| CustomerGroupManagementV1GetDefaultGroupGet - customerGroups/default | customerGroups/default | /V1/customerGroups/default\_\_\_get |
| CustomerGroupManagementV1IsReadonlyGet - customerGroups/{id}/permissions | customerGroups/{id}/permissions | /V1/customerGroups/{id}/permissions\_\_\_get |
| CustomerGroupRepositoryV1GetListGet - customerGroups/search | customerGroups/search | /V1/customerGroups/search\_\_\_get |
| DirectoryCountryInformationAcquirerV1GetCountriesInfoGet - directory/countries | directory/countries | /V1/directory/countries\_\_\_get |
| DirectoryCurrencyInformationAcquirerV1GetCurrencyInfoGet - directory/currency | directory/currency | /V1/directory/currency\_\_\_get |
| DownloadableLinkRepositoryV1GetListGet - products/{sku}/downloadable-links | products/{sku}/downloadable-links | /V1/products/{sku}/downloadable-links\_\_\_get |
| DownloadableSampleRepositoryV1GetListGet - products/{sku}/downloadable-links/samples | products/{sku}/downloadable-links/samples | /V1/products/{sku}/downloadable-links/samples\_\_\_get |
| EavAttributeSetRepositoryV1GetListGet - eav/attribute-sets/list | eav/attribute-sets/list | /V1/eav/attribute-sets/list\_\_\_get |
| GiftCardAccountGiftCardAccountManagementV1GetListByQuoteIdGet - carts/{quoteId}/giftCards | carts/{quoteId}/giftCards | /V1/carts/{quoteId}/giftCards\_\_\_get |
| GiftMessageCartRepositoryV1GetGet - carts/mine/gift-message | carts/mine/gift-message | /V1/carts/mine/gift-message\_\_\_get |
| GiftMessageCartRepositoryV1GetGet - carts/{cartId}/gift-message | carts/{cartId}/gift-message | /V1/carts/{cartId}/gift-message\_\_\_get |
| GiftMessageGuestCartRepositoryV1GetGet - guest-carts/{cartId}/gift-message | guest-carts/{cartId}/gift-message | /V1/guest-carts/{cartId}/gift-message\_\_\_get |
| GiftWrappingWrappingRepositoryV1GetListGet - gift-wrappings | gift-wrappings | /V1/gift-wrappings\_\_\_get |
| NegotiableQuoteAttachmentContentManagementV1GetGet - negotiableQuote/attachmentContent | negotiableQuote/attachmentContent | /V1/negotiableQuote/attachmentContent\_\_\_get |
| NegotiableQuoteBillingAddressManagementV1GetGet - negotiable-carts/{cartId}/billing-address | negotiable-carts/{cartId}/billing-address | /V1/negotiable-carts/{cartId}/billing-address\_\_\_get |
| NegotiableQuoteCartTotalRepositoryV1GetGet - negotiable-carts/{cartId}/totals | negotiable-carts/{cartId}/totals | /V1/negotiable-carts/{cartId}/totals\_\_\_get |
| NegotiableQuoteCommentLocatorV1GetListForQuoteGet - negotiableQuote/{quoteId}/comments | negotiableQuote/{quoteId}/comments | /V1/negotiableQuote/{quoteId}/comments\_\_\_get |
| NegotiableQuotePaymentInformationManagementV1GetPaymentInformationGet - negotiable-carts/{cartId}/payment-information | negotiable-carts/{cartId}/payment-information | /V1/negotiable-carts/{cartId}/payment-information\_\_\_get |
| QuoteBillingAddressManagementV1GetGet - carts/mine/billing-address | carts/mine/billing-address | /V1/carts/mine/billing-address\_\_\_get |
| QuoteBillingAddressManagementV1GetGet - carts/{cartId}/billing-address | carts/{cartId}/billing-address | /V1/carts/{cartId}/billing-address\_\_\_get |
| QuoteCartItemRepositoryV1GetListGet - carts/mine/items | carts/mine/items | /V1/carts/mine/items\_\_\_get |
| QuoteCartItemRepositoryV1GetListGet - carts/{cartId}/items | carts/{cartId}/items | /V1/carts/{cartId}/items\_\_\_get |
| QuoteCartManagementV1GetCartForCustomerGet - carts/mine | carts/mine | /V1/carts/mine\_\_\_get |
| QuoteCartRepositoryV1GetListGet - carts/search | carts/search | /V1/carts/search\_\_\_get |
| QuoteCartTotalRepositoryV1GetGet - carts/mine/totals | carts/mine/totals | /V1/carts/mine/totals\_\_\_get |
| QuoteCartTotalRepositoryV1GetGet - carts/{cartId}/totals | carts/{cartId}/totals | /V1/carts/{cartId}/totals\_\_\_get |
| QuoteCouponManagementV1GetGet - carts/mine/coupons | carts/mine/coupons | /V1/carts/mine/coupons\_\_\_get |
| QuoteCouponManagementV1GetGet - carts/{cartId}/coupons | carts/{cartId}/coupons | /V1/carts/{cartId}/coupons\_\_\_get |
| QuoteGuestBillingAddressManagementV1GetGet - guest-carts/{cartId}/billing-address | guest-carts/{cartId}/billing-address | /V1/guest-carts/{cartId}/billing-address\_\_\_get |
| QuoteGuestCartItemRepositoryV1GetListGet - guest-carts/{cartId}/items | guest-carts/{cartId}/items | /V1/guest-carts/{cartId}/items\_\_\_get |
| QuoteGuestCartTotalRepositoryV1GetGet - guest-carts/{cartId}/totals | guest-carts/{cartId}/totals | /V1/guest-carts/{cartId}/totals\_\_\_get |
| QuoteGuestCouponManagementV1GetGet - guest-carts/{cartId}/coupons | guest-carts/{cartId}/coupons | /V1/guest-carts/{cartId}/coupons\_\_\_get |
| QuoteGuestPaymentMethodManagementV1GetGet - guest-carts/{cartId}/selected-payment-method | guest-carts/{cartId}/selected-payment-method | /V1/guest-carts/{cartId}/selected-payment-method\_\_\_get |
| QuoteGuestPaymentMethodManagementV1GetListGet - guest-carts/{cartId}/payment-methods | guest-carts/{cartId}/payment-methods | /V1/guest-carts/{cartId}/payment-methods\_\_\_get |
| QuoteGuestShippingMethodManagementV1GetListGet - guest-carts/{cartId}/shipping-methods | guest-carts/{cartId}/shipping-methods | /V1/guest-carts/{cartId}/shipping-methods\_\_\_get |
| QuotePaymentMethodManagementV1GetGet - carts/mine/selected-payment-method | carts/mine/selected-payment-method | /V1/carts/mine/selected-payment-method\_\_\_get |
| QuotePaymentMethodManagementV1GetGet - carts/{cartId}/selected-payment-method | carts/{cartId}/selected-payment-method | /V1/carts/{cartId}/selected-payment-method\_\_\_get |
| QuotePaymentMethodManagementV1GetListGet - carts/mine/payment-methods | carts/mine/payment-methods | /V1/carts/mine/payment-methods\_\_\_get |
| QuotePaymentMethodManagementV1GetListGet - carts/{cartId}/payment-methods | carts/{cartId}/payment-methods | /V1/carts/{cartId}/payment-methods\_\_\_get |
| QuoteShippingMethodManagementV1GetListGet - carts/mine/shipping-methods | carts/mine/shipping-methods | /V1/carts/mine/shipping-methods\_\_\_get |
| QuoteShippingMethodManagementV1GetListGet - carts/{cartId}/shipping-methods | carts/{cartId}/shipping-methods | /V1/carts/{cartId}/shipping-methods\_\_\_get |
| RmaCommentManagementV1CommentsListGet - returns/{id}/comments | returns/{id}/comments | /V1/returns/{id}/comments\_\_\_get |
| RmaRmaAttributesManagementV1GetAllAttributesMetadataGet - returnsAttributeMetadata | returnsAttributeMetadata | /V1/returnsAttributeMetadata\_\_\_get |
| RmaRmaAttributesManagementV1GetCustomAttributesMetadataGet - returnsAttributeMetadata/custom | returnsAttributeMetadata/custom | /V1/returnsAttributeMetadata/custom\_\_\_get |
| RmaRmaManagementV1SearchGet - returns | returns | /V1/returns\_\_\_get |
| RmaTrackManagementV1GetShippingLabelPdfGet - returns/{id}/labels | returns/{id}/labels | /V1/returns/{id}/labels\_\_\_get |
| RmaTrackManagementV1GetTracksGet - returns/{id}/tracking-numbers | returns/{id}/tracking-numbers | /V1/returns/{id}/tracking-numbers\_\_\_get |
| SalesCreditmemoManagementV1GetCommentsListGet - creditmemo/{id}/comments | creditmemo/{id}/comments | /V1/creditmemo/{id}/comments\_\_\_get |
| SalesCreditmemoRepositoryV1GetListGet - creditmemos | creditmemos | /V1/creditmemos\_\_\_get |
| SalesInvoiceManagementV1GetCommentsListGet - invoices/{id}/comments | invoices/{id}/comments | /V1/invoices/{id}/comments\_\_\_get |
| SalesInvoiceRepositoryV1GetListGet - invoices | invoices | /V1/invoices\_\_\_get |
| SalesOrderItemRepositoryV1GetListGet - orders/items | orders/items | /V1/orders/items\_\_\_get |
| SalesOrderManagementV1GetCommentsListGet - orders/{id}/comments | orders/{id}/comments | /V1/orders/{id}/comments\_\_\_get |
| SalesOrderManagementV1GetStatusGet - orders/{id}/statuses | orders/{id}/statuses | /V1/orders/{id}/statuses\_\_\_get |
| SalesOrderRepositoryV1GetListGet - orders | orders | /V1/orders\_\_\_get |
| SalesRuleCouponRepositoryV1GetListGet - coupons/search | coupons/search | /V1/coupons/search\_\_\_get |
| SalesRuleRuleRepositoryV1GetListGet - salesRules/search | salesRules/search | /V1/salesRules/search\_\_\_get |
| SalesShipmentManagementV1GetCommentsListGet - shipment/{id}/comments | shipment/{id}/comments | /V1/shipment/{id}/comments\_\_\_get |
| SalesShipmentManagementV1GetLabelGet - shipment/{id}/label | shipment/{id}/label | /V1/shipment/{id}/label\_\_\_get |
| SalesShipmentRepositoryV1GetListGet - shipments | shipments | /V1/shipments\_\_\_get |
| SalesTransactionRepositoryV1GetListGet - transactions | transactions | /V1/transactions\_\_\_get |
| SearchV1SearchGet - search | search | /V1/search\_\_\_get |
| SharedCatalogCategoryManagementV1GetCategoriesGet - sharedCatalog/{id}/categories | sharedCatalog/{id}/categories | /V1/sharedCatalog/{id}/categories\_\_\_get |
| SharedCatalogCompanyManagementV1GetCompaniesGet - sharedCatalog/{sharedCatalogId}/companies | sharedCatalog/{sharedCatalogId}/companies | /V1/sharedCatalog/{sharedCatalogId}/companies\_\_\_get |
| SharedCatalogProductManagementV1GetProductsGet - sharedCatalog/{id}/products | sharedCatalog/{id}/products | /V1/sharedCatalog/{id}/products\_\_\_get |
| SharedCatalogSharedCatalogRepositoryV1GetListGet - sharedCatalog/ | sharedCatalog/ | /V1/sharedCatalog/\_\_\_get |
| StoreGroupRepositoryV1GetListGet - store/storeGroups | store/storeGroups | /V1/store/storeGroups\_\_\_get |
| StoreStoreConfigManagerV1GetStoreConfigsGet - store/storeConfigs | store/storeConfigs | /V1/store/storeConfigs\_\_\_get |
| StoreStoreRepositoryV1GetListGet - store/storeViews | store/storeViews | /V1/store/storeViews\_\_\_get |
| StoreWebsiteRepositoryV1GetListGet - store/websites | store/websites | /V1/store/websites\_\_\_get |
| TaxTaxClassRepositoryV1GetListGet - taxClasses/search | taxClasses/search | /V1/taxClasses/search\_\_\_get |
| TaxTaxRateRepositoryV1GetListGet - taxRates/search | taxRates/search | /V1/taxRates/search\_\_\_get |
| TaxTaxRuleRepositoryV1GetListGet - taxRules/search | taxRules/search | /V1/taxRules/search\_\_\_get |
| TemandoShippingCollectionPointCartCollectionPointManagementV1GetCollectionPointsGet - carts/mine/collection-point/search-result | carts/mine/collection-point/search-result | /V1/carts/mine/collection-point/search-result\_\_\_get |
| TemandoShippingCollectionPointGuestCartCollectionPointManagementV1GetCollectionPointsGet - guest-carts/{cartId}/collection-point/search-resul... | guest-carts/{cartId}/collection-point/search-resul... | /V1/guest-carts/{cartId}/collection-point/search-result\_\_\_get |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| AmazonPaymentAddressManagementV1GetBillingAddressPut - amazon-billing-address/{amazonOrderReferenceId} (PUT) | amazon-billing-address/{amazonOrderReferenceId} | /V1/amazon-billing-address/{amazonOrderReferenceId}\_\_\_put |
| AmazonPaymentAddressManagementV1GetShippingAddressPut - amazon-shipping-address/{amazonOrderReferenceId} (PUT) | amazon-shipping-address/{amazonOrderReferenceId} | /V1/amazon-shipping-address/{amazonOrderReferenceId}\_\_\_put |
| BundleProductLinkManagementV1AddChildByProductSkuPost - bundle-products/{sku}/links/{optionId} (POST) | bundle-products/{sku}/links/{optionId} | /V1/bundle-products/{sku}/links/{optionId}\_\_\_post |
| BundleProductLinkManagementV1SaveChildPut - bundle-products/{sku}/links/{id} (PUT) | bundle-products/{sku}/links/{id} | /V1/bundle-products/{sku}/links/{id}\_\_\_put |
| BundleProductOptionManagementV1SavePut - bundle-products/options/{optionId} (PUT) | bundle-products/options/{optionId} | /V1/bundle-products/options/{optionId}\_\_\_put |
| CatalogAttributeSetRepositoryV1SavePut - products/attribute-sets/{attributeSetId} (PUT) | products/attribute-sets/{attributeSetId} | /V1/products/attribute-sets/{attributeSetId}\_\_\_put |
| CatalogCategoryLinkRepositoryV1SavePut - categories/{categoryId}/products (PUT) | categories/{categoryId}/products | /V1/categories/{categoryId}/products\_\_\_put |
| CatalogCategoryManagementV1MovePut - categories/{categoryId}/move (PUT) | categories/{categoryId}/move | /V1/categories/{categoryId}/move\_\_\_put |
| CatalogCategoryRepositoryV1SavePut - categories/{id} (PUT) | categories/{id} | /V1/categories/{id}\_\_\_put |
| CatalogInventoryStockRegistryV1UpdateStockItemBySkuPut - products/{productSku}/stockItems/{itemId} (PUT) | products/{productSku}/stockItems/{itemId} | /V1/products/{productSku}/stockItems/{itemId}\_\_\_put |
| CatalogProductAttributeGroupRepositoryV1SavePut - products/attribute-sets/{attributeSetId}/groups (PUT) | products/attribute-sets/{attributeSetId}/groups | /V1/products/attribute-sets/{attributeSetId}/groups\_\_\_put |
| CatalogProductAttributeMediaGalleryManagementV1UpdatePut - products/{sku}/media/{entryId} (PUT) | products/{sku}/media/{entryId} | /V1/products/{sku}/media/{entryId}\_\_\_put |
| CatalogProductAttributeRepositoryV1SavePut - products/attributes/{attributeCode} (PUT) | products/attributes/{attributeCode} | /V1/products/attributes/{attributeCode}\_\_\_put |
| CatalogProductCustomOptionRepositoryV1SavePut - products/options/{optionId} (PUT) | products/options/{optionId} | /V1/products/options/{optionId}\_\_\_put |
| CatalogProductLinkRepositoryV1SavePut - products/{sku}/links (PUT) | products/{sku}/links | /V1/products/{sku}/links\_\_\_put |
| CatalogProductRepositoryV1SavePut - products/{sku} (PUT) | products/{sku} | /V1/products/{sku}\_\_\_put |
| CatalogProductTierPriceManagementV1AddPost - products/{sku}/group-prices/{customerGroupId}/tier... (POST) | products/{sku}/group-prices/{customerGroupId}/tier... | /V1/products/{sku}/group-prices/{customerGroupId}/tiers/{qty}/price/{price}\_\_\_post |
| CatalogProductWebsiteLinkRepositoryV1SavePut - products/{sku}/websites (PUT) | products/{sku}/websites | /V1/products/{sku}/websites\_\_\_put |
| CatalogTierPriceStorageV1ReplacePut - products/tier-prices (PUT) | products/tier-prices | /V1/products/tier-prices\_\_\_put |
| CmsBlockRepositoryV1SavePut - cmsBlock/{id} (PUT) | cmsBlock/{id} | /V1/cmsBlock/{id}\_\_\_put |
| CmsPageRepositoryV1SavePut - cmsPage/{id} (PUT) | cmsPage/{id} | /V1/cmsPage/{id}\_\_\_put |
| CompanyAclV1AssignRolesPut - company/assignRoles (PUT) | company/assignRoles | /V1/company/assignRoles\_\_\_put |
| CompanyCompanyHierarchyV1MoveNodePut - hierarchy/move/{id} (PUT) | hierarchy/move/{id} | /V1/hierarchy/move/{id}\_\_\_put |
| CompanyCompanyRepositoryV1SavePut - company/{companyId} (PUT) | company/{companyId} | /V1/company/{companyId}\_\_\_put |
| CompanyCreditCreditHistoryManagementV1UpdatePut - companyCredits/history/{historyId} (PUT) | companyCredits/history/{historyId} | /V1/companyCredits/history/{historyId}\_\_\_put |
| CompanyCreditCreditLimitRepositoryV1SavePut - companyCredits/{id} (PUT) | companyCredits/{id} | /V1/companyCredits/{id}\_\_\_put |
| CompanyRoleRepositoryV1SavePut - company/role/{id} (PUT) | company/role/{id} | /V1/company/role/{id}\_\_\_put |
| CompanyTeamRepositoryV1CreatePost - team/{companyId} (POST) | team/{companyId} | /V1/team/{companyId}\_\_\_post |
| CompanyTeamRepositoryV1SavePut - team/{teamId} (PUT) | team/{teamId} | /V1/team/{teamId}\_\_\_put |
| ConfigurableProductConfigurableProductManagementV1GenerateVariationPut - configurable-products/variation (PUT) | configurable-products/variation | /V1/configurable-products/variation\_\_\_put |
| ConfigurableProductOptionRepositoryV1SavePut - configurable-products/{sku}/options/{id} (PUT) | configurable-products/{sku}/options/{id} | /V1/configurable-products/{sku}/options/{id}\_\_\_put |
| CustomerAccountManagementV1ActivateByIdPut - customers/me/activate (PUT) | customers/me/activate | /V1/customers/me/activate\_\_\_put |
| CustomerAccountManagementV1ActivatePut - customers/{email}/activate (PUT) | customers/{email}/activate | /V1/customers/{email}/activate\_\_\_put |
| CustomerAccountManagementV1ChangePasswordByIdPut - customers/me/password (PUT) | customers/me/password | /V1/customers/me/password\_\_\_put |
| CustomerAccountManagementV1InitiatePasswordResetPut - customers/password (PUT) | customers/password | /V1/customers/password\_\_\_put |
| CustomerAccountManagementV1ValidatePut - customers/validate (PUT) | customers/validate | /V1/customers/validate\_\_\_put |
| CustomerCustomerGroupConfigV1SetDefaultCustomerGroupPut - customerGroups/default/{id} (PUT) | customerGroups/default/{id} | /V1/customerGroups/default/{id}\_\_\_put |
| CustomerCustomerRepositoryV1SavePut - customers/me (PUT) | customers/me | /V1/customers/me\_\_\_put |
| CustomerCustomerRepositoryV1SavePut - customers/{customerId} (PUT) | customers/{customerId} | /V1/customers/{customerId}\_\_\_put |
| CustomerGroupRepositoryV1SavePut - customerGroups/{id} (PUT) | customerGroups/{id} | /V1/customerGroups/{id}\_\_\_put |
| DownloadableLinkRepositoryV1SavePut - products/{sku}/downloadable-links/{id} (PUT) | products/{sku}/downloadable-links/{id} | /V1/products/{sku}/downloadable-links/{id}\_\_\_put |
| DownloadableSampleRepositoryV1SavePut - products/{sku}/downloadable-links/samples/{id} (PUT) | products/{sku}/downloadable-links/samples/{id} | /V1/products/{sku}/downloadable-links/samples/{id}\_\_\_put |
| EavAttributeSetRepositoryV1SavePut - eav/attribute-sets/{attributeSetId} (PUT) | eav/attribute-sets/{attributeSetId} | /V1/eav/attribute-sets/{attributeSetId}\_\_\_put |
| GiftCardAccountGiftCardAccountManagementV1SaveByQuoteIdPut - carts/{cartId}/giftCards (PUT) | carts/{cartId}/giftCards | /V1/carts/{cartId}/giftCards\_\_\_put |
| GiftMessageGuestItemRepositoryV1SavePost - guest-carts/{cartId}/gift-message/{itemId} (POST) | guest-carts/{cartId}/gift-message/{itemId} | /V1/guest-carts/{cartId}/gift-message/{itemId}\_\_\_post |
| GiftMessageItemRepositoryV1SavePost - carts/mine/gift-message/{itemId} (POST) | carts/mine/gift-message/{itemId} | /V1/carts/mine/gift-message/{itemId}\_\_\_post |
| GiftMessageItemRepositoryV1SavePost - carts/{cartId}/gift-message/{itemId} (POST) | carts/{cartId}/gift-message/{itemId} | /V1/carts/{cartId}/gift-message/{itemId}\_\_\_post |
| GiftWrappingWrappingRepositoryV1SavePut - gift-wrappings/{wrappingId} (PUT) | gift-wrappings/{wrappingId} | /V1/gift-wrappings/{wrappingId}\_\_\_put |
| NegotiableQuoteCouponManagementV1SetPut - negotiable-carts/{cartId}/coupons/{couponCode} (PUT) | negotiable-carts/{cartId}/coupons/{couponCode} | /V1/negotiable-carts/{cartId}/coupons/{couponCode}\_\_\_put |
| NegotiableQuoteNegotiableCartRepositoryV1SavePut - negotiableQuote/{quoteId} (PUT) | negotiableQuote/{quoteId} | /V1/negotiableQuote/{quoteId}\_\_\_put |
| NegotiableQuoteNegotiableQuoteShippingManagementV1SetShippingMethodPut - negotiableQuote/{quoteId}/shippingMethod (PUT) | negotiableQuote/{quoteId}/shippingMethod | /V1/negotiableQuote/{quoteId}/shippingMethod\_\_\_put |
| QuoteCartItemRepositoryV1SavePut - carts/mine/items/{itemId} (PUT) | carts/mine/items/{itemId} | /V1/carts/mine/items/{itemId}\_\_\_put |
| QuoteCartItemRepositoryV1SavePut - carts/{cartId}/items/{itemId} (PUT) | carts/{cartId}/items/{itemId} | /V1/carts/{cartId}/items/{itemId}\_\_\_put |
| QuoteCartManagementV1AssignCustomerPut - carts/{cartId} (PUT) | carts/{cartId} | /V1/carts/{cartId}\_\_\_put |
| QuoteCartManagementV1PlaceOrderPut - carts/mine/order (PUT) | carts/mine/order | /V1/carts/mine/order\_\_\_put |
| QuoteCartManagementV1PlaceOrderPut - carts/{cartId}/order (PUT) | carts/{cartId}/order | /V1/carts/{cartId}/order\_\_\_put |
| QuoteCartRepositoryV1SavePut - carts/mine (PUT) | carts/mine | /V1/carts/mine\_\_\_put |
| QuoteCartTotalManagementV1CollectTotalsPut - carts/mine/collect-totals (PUT) | carts/mine/collect-totals | /V1/carts/mine/collect-totals\_\_\_put |
| QuoteCouponManagementV1SetPut - carts/mine/coupons/{couponCode} (PUT) | carts/mine/coupons/{couponCode} | /V1/carts/mine/coupons/{couponCode}\_\_\_put |
| QuoteCouponManagementV1SetPut - carts/{cartId}/coupons/{couponCode} (PUT) | carts/{cartId}/coupons/{couponCode} | /V1/carts/{cartId}/coupons/{couponCode}\_\_\_put |
| QuoteGuestCartItemRepositoryV1SavePut - guest-carts/{cartId}/items/{itemId} (PUT) | guest-carts/{cartId}/items/{itemId} | /V1/guest-carts/{cartId}/items/{itemId}\_\_\_put |
| QuoteGuestCartManagementV1AssignCustomerPut - guest-carts/{cartId} (PUT) | guest-carts/{cartId} | /V1/guest-carts/{cartId}\_\_\_put |
| QuoteGuestCartManagementV1PlaceOrderPut - guest-carts/{cartId}/order (PUT) | guest-carts/{cartId}/order | /V1/guest-carts/{cartId}/order\_\_\_put |
| QuoteGuestCartTotalManagementV1CollectTotalsPut - guest-carts/{cartId}/collect-totals (PUT) | guest-carts/{cartId}/collect-totals | /V1/guest-carts/{cartId}/collect-totals\_\_\_put |
| QuoteGuestCouponManagementV1SetPut - guest-carts/{cartId}/coupons/{couponCode} (PUT) | guest-carts/{cartId}/coupons/{couponCode} | /V1/guest-carts/{cartId}/coupons/{couponCode}\_\_\_put |
| QuoteGuestPaymentMethodManagementV1SetPut - guest-carts/{cartId}/selected-payment-method (PUT) | guest-carts/{cartId}/selected-payment-method | /V1/guest-carts/{cartId}/selected-payment-method\_\_\_put |
| QuotePaymentMethodManagementV1SetPut - carts/mine/selected-payment-method (PUT) | carts/mine/selected-payment-method | /V1/carts/mine/selected-payment-method\_\_\_put |
| QuotePaymentMethodManagementV1SetPut - carts/{cartId}/selected-payment-method (PUT) | carts/{cartId}/selected-payment-method | /V1/carts/{cartId}/selected-payment-method\_\_\_put |
| RmaRmaManagementV1SaveRmaPut - returns/{id} (PUT) | returns/{id} | /V1/returns/{id}\_\_\_put |
| SalesCreditmemoManagementV1CancelPut - creditmemo/{id} (PUT) | creditmemo/{id} | /V1/creditmemo/{id}\_\_\_put |
| SalesOrderAddressRepositoryV1SavePut - orders/{parent_id} (PUT) | orders/{parent_id} | /V1/orders/{parent\_id}\_\_\_put |
| SalesOrderRepositoryV1SavePut - orders/create (PUT) | orders/create | /V1/orders/create\_\_\_put |
| SalesRuleCouponRepositoryV1SavePut - coupons/{couponId} (PUT) | coupons/{couponId} | /V1/coupons/{couponId}\_\_\_put |
| SalesRuleRuleRepositoryV1SavePut - salesRules/{ruleId} (PUT) | salesRules/{ruleId} | /V1/salesRules/{ruleId}\_\_\_put |
| SharedCatalogSharedCatalogRepositoryV1SavePut - sharedCatalog/{id} (PUT) | sharedCatalog/{id} | /V1/sharedCatalog/{id}\_\_\_put |
| TaxTaxClassRepositoryV1SavePut - taxClasses/{classId} (PUT) | taxClasses/{classId} | /V1/taxClasses/{classId}\_\_\_put |
| TaxTaxRateRepositoryV1SavePut - taxRates (PUT) | taxRates | /V1/taxRates\_\_\_put |
| TaxTaxRuleRepositoryV1SavePut - taxRules (PUT) | taxRules | /V1/taxRules\_\_\_put |
| TemandoShippingCollectionPointCartCollectionPointManagementV1SaveSearchRequestPut - carts/mine/collection-point/search-request (PUT) | carts/mine/collection-point/search-request | /V1/carts/mine/collection-point/search-request\_\_\_put |
| TemandoShippingCollectionPointGuestCartCollectionPointManagementV1SaveSearchRequestPut - guest-carts/{cartId}/collection-point/search-reque... (PUT) | guest-carts/{cartId}/collection-point/search-reque... | /V1/guest-carts/{cartId}/collection-point/search-request\_\_\_put |
| TemandoShippingRmaRmaShipmentManagementV1AssignShipmentIdsPut - temando/rma/{rmaId}/shipments (PUT) | temando/rma/{rmaId}/shipments | /V1/temando/rma/{rmaId}/shipments\_\_\_put |


