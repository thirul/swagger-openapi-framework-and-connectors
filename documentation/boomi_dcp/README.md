# Visa Cybersource Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### URL

The URL for the Boomi DCP service

**Type** - string



#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password

# Operation Tab


## CREATE


## UPDATE


## DELETE


## GET


## QUERY
### Operation Fields


##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Offset

The number of rows to skip before beginning to return rows. An offset of 0 is the same as omitting the offset parameter.

**Type** - integer

**Default Value** - 0


### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

The query filter supports no groupings (only one expression allowed).

Example:
(foo lessThan 30)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts

The query operation does not support sorting.


# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.
 * Additional URI Query Parameters
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Comments Feedback Create - Toggle feedback flags on comments | Collaboration Toggles like or dislike flag on a comment. Toggle feedback flags on comments  | /comments/{id}/feedback/\_\_\_post |
| Data-sets Columns Glossary Assign Create - Assign glossary to dataset column | Glossary Assigns a business glossary to a given dataset column. Assign glossary to dataset column  | /data-sets/columns/{id}/glossary/assign/\_\_\_post |
| Data-sets Columns Glossary Remove Create - Remove glossary from dataset column | Glossary Removes a business glossary from a given dataset column. Remove glossary from dataset column  | /data-sets/columns/{id}/glossary/remove/\_\_\_post |
| Data-sets Comments Create - Create a comment on a dataset | Collaboration Creates a comment on a given dataset.  In order to add tags of objects in the system use @ or # and type the object name. Create a comment on a dataset  | /data-sets/{id}/comments/\_\_\_post |
| Data-sets Csv-files Columns Create - Create a column for a csvfile dataset | Datasets Creates a new column for a csvfile dataset. This is usually done via the file crawler or through dataset import. Create a column for a csvfile dataset  | /data-sets/csv-files/{id}/columns/\_\_\_post |
| Data-sets Database-tables Columns Create - Create a column for a database table dataset | Datasets Creates a new column for a database table dataset. This is usually done via the dataset import. Create a column for a database table dataset  | /data-sets/database-tables/{id}/columns/\_\_\_post |
| Data-sets Database-tables Foreign-keys Create - Create a foreign key for a database table dataset | Datasets Creates a new foreign key object for a database table dataset. This is usually done via the dataset import. Create a foreign key for a database table dataset  | /data-sets/database-tables/{id}/foreign-keys/\_\_\_post |
| Data-sets Database-tables Indexes Create - Create an index for a database table dataset | Datasets Creates a new index object for a database table dataset. This is usually done via the dataset import. Create an index for a database table dataset  | /data-sets/database-tables/{id}/indexes/\_\_\_post |
| Data-sets Database-tables Primary-key Create - Create a primary key for a database table dataset | Datasets Creates a new primary key object for a database table dataset. This is usually done via the dataset import. Create a primary key for a database table dataset  | /data-sets/database-tables/{id}/primary-key/\_\_\_post |
| Data-sets Endorsement Create - Gets dataset endorsement details | Datasets Retrieves the endorsement details of a given dataset. Gets dataset endorsement details  | /data-sets/{id}/endorsement/\_\_\_post |
| Data-sets Glossary Assign Create - Assign glossary to dataset | Glossary Assigns a business glossary to a given dataset. Assign glossary to dataset  | /data-sets/{id}/glossary/assign/\_\_\_post |
| Data-sets Glossary Remove Create - Remove glossary from dataset | Glossary Removes a business glossary from a given dataset. Remove glossary from dataset  | /data-sets/{id}/glossary/remove/\_\_\_post |
| Data-sources File-info Statistics-activities Create - Create sample statistics for dataset | Statistics Triggers sample statistics for dataset of a provided data source. Create sample statistics for dataset  | /data-sources/{id}/file-info/statistics-activities/\_\_\_post |
| Data-sources Glossary Assign Create - Assign glossary to data source | Glossary Assigns a business glossary to a given data source. Assign glossary to data source  | /data-sources/{id}/glossary/assign/\_\_\_post |
| Data-sources Glossary Remove Create - Remove glossary from data source | Glossary Removes a business glossary from a given data source. Remove glossary from data source  | /data-sources/{id}/glossary/remove/\_\_\_post |
| Data-sources Import-metadata Create - Create metadata import task | Data Sources Triggers metadata import task for a provided data source. Create metadata import task  | /data-sources/{id}/import-metadata/\_\_\_post |
| Data-sources Import-schedules Create - Create a source import schedule | Data Sources Creates a import schedules for a data source. Create a source import schedule  | /data-sources/import-schedules/\_\_\_post |
| Data-types Create - Create a new datatype | Data Types Create a new datatype. Create a new datatype  | /data-types/\_\_\_post |
| Datastore-adapters Create - Create datastore adapter | Adapters Create a datastore adapter on the DCP instance. Required permissions: Role permissions based on "adapter_perms" field. Create datastore adapter  | /datastore-adapters/\_\_\_post |
| Glossary Bulk Assign Create - Assign multiple objects to a list of glossaries | Glossary Assigns multiple data sources, datasets and datasets columns to a list of business glossaries. If any of the lists is ['all'], all of the objects of that type will be assigned. For example:  sources: ['all'] will assign all sources to the glossaries in the glossary list.  glossary: ['all'] will assign all of the objects in the other lists to all of the existing glossaries. Assign multiple objects to a list of glossaries  | /glossary/bulk/assign/\_\_\_post |
| Glossary Bulk Remove Create - Remove multiple objects from a list of glossaries | Glossary Removes multiple data sources, datasets and datasets columns from a list of business glossaries. If any of the lists is ['all'], all of the objects of that type will be removed. For example:  sources: ['all'] will remove all sources from the glossaries in the glossary list.  glossary: ['all'] will remove all of the objects in the other lists from all of the existing glossaries. Remove multiple objects from a list of glossaries  | /glossary/bulk/remove/\_\_\_post |
| Glossary Comments Create - Create a comment on a glossary | Collaboration Creates a comment on a given glossary.  In order to add tags of objects in the system use @ or # and type the object name. Create a comment on a glossary  | /glossary/{id}/comments/\_\_\_post |
| Glossary Create - Create a glossary | Glossary Creates a business glossary object. Create a glossary  | /glossary/\_\_\_post |
| Glossary Endorsement Create - Gets glossary endorsement details | Glossary Retrieves the endorsement details of a given glossary. Gets glossary endorsement details  | /glossary/{id}/endorsement/\_\_\_post |
| Groups Create - Create a group | Permissions Creates a new group object. Create a group  | /groups/\_\_\_post |
| Install-adapter Create - Install adapter | Adapters This request installs the latest version of the given adapter on the DCP instance. Install adapter  | /install-adapter/\_\_\_post |
| Pii-rules Create - Creates a pii rule. | Security Creates a new pii/masking rule. Creates a pii rule.  | /pii-rules/\_\_\_post |
| Roles Create - Create a role | Permissions Create a role.  Each permission field has is an array representing different operations, such as CRUD.The permissions options for each operation are:  Y - Always allow. N - Never allow. O - partial permissions (Object level permissions). None - Not applicable.  For most of the permission fields, the array represents the Create, Read, Update and Delete operations respectively, e.g. ['N', 'Y', 'O', None] means  N - Create: No permissions to create. Y - Read: Always allow to read. O - Update: partial permissions to update - usually it means that an additional object level permission is required. None - Delete: delete is not applicable for this permission category.  For data_content_perms, the array represents [Data Read, Data Read Masked, Data Endorse] permissions. Create a role  | /roles/\_\_\_post |
| Search Dataimport Create - Import Data to Solr | Search Import data to Solr by indexing the metastore. Import Data to Solr  | /search/dataimport/\_\_\_post |
| Upgrade-adapter Create - Upgrade adapter | Adapters This request upgrades the given adapter to its latest version on the DCP instance.  Upgrade adapter  | /upgrade-adapter/\_\_\_post |
| Users Create - Create a user | Permissions Creates a new user object. Create a user  | /users/\_\_\_post |
| Users Transfer-ownership Create - Transfer ownership | Permissions Transfers ownership from a given user to a new user.  Ownership is determined by the created_by fields of various objects.  This call will update these fields to be that by the new user. Transfer ownership  | /users/{id}/transfer-ownership/\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Comments Delete - Delete comment | Collaboration Deletes an existing comment and all its replies. Delete comment  | /comments/{id}/\_\_\_delete |
| Data-sets Columns Delete - Delete a dataset column | Datasets Deletes an existing dataset column and all its dependents. Delete a dataset column  | /data-sets/columns/{id}/\_\_\_delete |
| Data-sets Csv-files Columns Delete - Delete csvfile column | Datasets Deletes an existing csvfile column and all its dependents. Delete csvfile column  | /data-sets/csv-files/{csvfile\_pk}/columns/{id}/\_\_\_delete |
| Data-sets Csv-files Delete - Delete csvfile dataset | Datasets Deletes an existing csvfile dataset and all its dependents. Delete csvfile dataset  | /data-sets/csv-files/{id}/\_\_\_delete |
| Data-sets Database-tables Columns Delete - Delete database column | Datasets Deletes an existing database column and all its dependents. Delete database column  | /data-sets/database-tables/{dbtable\_pk}/columns/{id}/\_\_\_delete |
| Data-sets Database-tables Delete - Delete database table dataset | Datasets Deletes an existing database table dataset and all its dependents. Delete database table dataset  | /data-sets/database-tables/{id}/\_\_\_delete |
| Data-sets Database-tables Foreign-keys Delete - Delete foreign key | Datasets Deletes an existing foreign key object and all its dependents. Delete foreign key  | /data-sets/database-tables/{dbtable\_pk}/foreign-keys/{id}/\_\_\_delete |
| Data-sets Database-tables Indexes Delete - Delete database index | Datasets Deletes an existing database table index object and all its dependents. Delete database index  | /data-sets/database-tables/{dbtable\_pk}/indexes/{id}/\_\_\_delete |
| Data-sets Database-tables Primary-key Delete - Delete database primary key | Datasets Deletes an existing database table primary key object and all its dependents. Delete database primary key  | /data-sets/database-tables/{dbtable\_pk}/primary-key/{id}/\_\_\_delete |
| Data-sets Tags Delete - Remove tag from dataset | Datasets Collaboration Removes a tag from a given dataset object. Remove tag from dataset  | /data-sets/{id}/tags/{tag\_name}/\_\_\_delete |
| Data-sources Delete - Delete a data source | Data Sources Deletes an existing data source and all its dependents. Delete a data source  | /data-sources/{id}/\_\_\_delete |
| Data-sources Import-activities Delete - Delete a source import activity | Data Sources Deletes an existing source import activity and all its dependents. Delete a source import activity  | /data-sources/import-activities/{id}/\_\_\_delete |
| Data-sources Import-schedules Delete - Delete a source import schedule | Glossary Deletes an existing source import schedule and all its dependents. Delete a source import schedule  | /data-sources/import-schedules/{id}/\_\_\_delete |
| Data-sources Import-schedules Tags Delete - Remove tag from source import schedule | Data Sources Collaboration Removes a tag from a given source import schedule object. Remove tag from source import schedule  | /data-sources/import-schedules/{id}/tags/{tag\_name}/\_\_\_delete |
| Data-sources Tags Delete - Remove tag from data source | Data Sources Collaboration Removes a tag from a given data source object. Remove tag from data source  | /data-sources/{id}/tags/{tag\_name}/\_\_\_delete |
| Data-types Delete - Delete a data type | Data Types Delete the data type of the given ID. Delete a data type  | /data-types/{id}/\_\_\_delete |
| Data-types Tags Delete - Remove tag from data type | Data Types Collaboration Removes a tag from a given data type object. Remove tag from data type  | /data-types/{id}/tags/{tag\_name}/\_\_\_delete |
| Datastore-adapters Delete - Delete datastore adapter | Adapters Deletes an existing datastore adapter and all its dependents. Delete datastore adapter  | /datastore-adapters/{id}/\_\_\_delete |
| Glossary Delete - Delete a glossary | Glossary Deletes an existing business glossary and all its dependents. Delete a glossary  | /glossary/{id}/\_\_\_delete |
| Glossary Tags Delete - Remove tag from business glossary | Glossary Collaboration Removes a tag from a given business glossary object. Remove tag from business glossary  | /glossary/{id}/tags/{tag\_name}/\_\_\_delete |
| Groups Delete - Delete group | Permissions Deletes an existing group. Delete group  | /groups/{id}/\_\_\_delete |
| Pii-rules Delete - Deletes PII rule. | Security Deletes an existing PII rule by its id. Deletes PII rule.  | /pii-rules/{id}/\_\_\_delete |
| Pii-rules Tags Delete - Remove tag from PII rule | Security Collaboration Removes a tag from a given PII rule object. Remove tag from PII rule  | /pii-rules/{id}/tags/{tag\_name}/\_\_\_delete |
| Plugins Tags Delete - Remove tag from data type plugin | Data Types Collaboration Removes a tag from a given data type plugin object. Remove tag from data type plugin  | /plugins/{id}/tags/{tag\_name}/\_\_\_delete |
| Recycle-bin Delete - Empty recycle bin | Trash Empties all deleted items from the recycle bin. Empty recycle bin  | /recycle-bin/\_\_\_delete |
| Recycle-bin Delete - Permanently delete deleted item | Trash Permanently deletes a deleted item from the recycle bin. Permanently delete deleted item  | /recycle-bin/{id}/\_\_\_delete |
| Roles Delete - Delete role | Permissions Deletes an existing role.  Built-in roles cannot be deleted.  Roles with users assign to them cannot be deleted. Delete role  | /roles/{id}/\_\_\_delete |
| Users Delete - Delete user | Permissions Deletes an existing user. Delete user  | /users/{id}/\_\_\_delete |
| Users Tags Delete - Remove tag from user | Permissions Collaboration Removes a tag from a given user object. Remove tag from user  | /users/{id}/tags/{tag\_name}/\_\_\_delete |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Comments Read - Get comment | Collaboration Retrieves an existing comment by its id. Get comment  | /comments/{id}/\_\_\_get |
| Data-sets Columns Read - Get dataset column | Datasets Retrieves an existing dataset column by its id. Get dataset column  | /data-sets/columns/{id}/\_\_\_get |
| Data-sets Csv-files Columns Read - Get csvfile column | Datasets Retrieves an existing csvfile column by its id. Get csvfile column  | /data-sets/csv-files/{csvfile\_pk}/columns/{id}/\_\_\_get |
| Data-sets Csv-files Read - Get csvfile dataset | Datasets Retrieves an existing csvfile dataset by its id. Get csvfile dataset  | /data-sets/csv-files/{id}/\_\_\_get |
| Data-sets Database-tables Columns Read - Get database column | Datasets Retrieves an existing database column by its id. Get database column  | /data-sets/database-tables/{dbtable\_pk}/columns/{id}/\_\_\_get |
| Data-sets Database-tables Foreign-keys Read - Get foreign key | Datasets Retrieves an existing foreign key object by its id. Get foreign key  | /data-sets/database-tables/{dbtable\_pk}/foreign-keys/{id}/\_\_\_get |
| Data-sets Database-tables Indexes Read - Get database index | Datasets Retrieves an existing database table index object by its id. Get database index  | /data-sets/database-tables/{dbtable\_pk}/indexes/{id}/\_\_\_get |
| Data-sets Database-tables Primary-key Read - Get database primary key | Datasets Retrieves an existing database table primary key object by its id. Get database primary key  | /data-sets/database-tables/{dbtable\_pk}/primary-key/{id}/\_\_\_get |
| Data-sets Database-tables Read - Get database table dataset | Datasets Retrieves an existing database table dataset by its id. Get database table dataset  | /data-sets/database-tables/{id}/\_\_\_get |
| Data-sets Endorsement Read - Gets dataset endorsement details | Datasets Retrieves the endorsement details of a given dataset. Gets dataset endorsement details  | /data-sets/{id}/endorsement/\_\_\_get |
| Data-sets Endorsements Read - Gets dataset endorsement details | Datasets Retrieves the endorsement details of a given endorsement object. Gets dataset endorsement details  | /data-sets/endorsements/{id}/\_\_\_get |
| Data-sources File-info Statistics-activity Read - Get the status of a triggered statistics activity | Statistics Fetch the status of a triggered statistics activity for a dataset of a provided data source. Get the status of a triggered statistics activity  | /data-sources/{id}/file-info/statistics-activity/\_\_\_get |
| Data-sources Import-activities Read - Get a source import activity | Data Sources Retrieves an existing source import activity by its id. Get a source import activity  | /data-sources/import-activities/{id}/\_\_\_get |
| Data-sources Import-schedule Read - List the details of import schedule | Data Sources Retrieves the details of import schedule for a provided data source. List the details of import schedule  | /data-sources/{source\_id}/import-schedule/\_\_\_get |
| Data-sources Import-schedules Read - Get a source import schedule | Glossary Retrieves an existing source import schedule by its id. Get a source import schedule  | /data-sources/import-schedules/{id}/\_\_\_get |
| Data-sources Oauth-init List - Start OAuth | Data Sources Starts OAuth authorization for this source. Only run if OAuth is needed. Start OAuth  | /data-sources/{id}/oauth-init/\_\_\_get |
| Data-sources Oauth-needed List - Check if source OAuth needed | Data Sources Checks if OAuth authorization is needed for this source. Check if source OAuth needed  | /data-sources/{id}/oauth-needed/\_\_\_get |
| Data-sources Oauth-status List - Get OAuth status | Data Sources Gets OAuth authorization status for this source. Get OAuth status  | /data-sources/{id}/oauth-status/\_\_\_get |
| Data-sources Read - Get a data source | Data Sources Retrieves an existing data source by its id. Get a data source  | /data-sources/{id}/\_\_\_get |
| Data-types Column-count List - Gets columns count for a data type. | Security Retrieves columns count for an existing data type by its id. Gets columns count for a data type.  | /data-types/{id}/column-count/\_\_\_get |
| Data-types Read - Get data type detail | Data Types Retrieves the details of a data type of the given ID. Get data type detail  | /data-types/{id}/\_\_\_get |
| Datastore-adapters Read - Get datastore adapter | Adapters Retrieves an existing datastore adapter by its id. Get datastore adapter  | /datastore-adapters/{id}/\_\_\_get |
| Glossary Endorsement Read - Gets glossary endorsement details | Glossary Retrieves the endorsement details of a given glossary. Gets glossary endorsement details  | /glossary/{id}/endorsement/\_\_\_get |
| Glossary Endorsements Read - Gets glossary endorsement details | Glossary Retrieves the endorsement details of a given endorsement object. Gets glossary endorsement details  | /glossary/endorsements/{id}/\_\_\_get |
| Glossary Read - Get a glossary | Glossary Retrieves an existing business glossary by its id. Get a glossary  | /glossary/{id}/\_\_\_get |
| Groups Read - Get group | Permissions Retrieves an existing group by its id. Get group  | /groups/{id}/\_\_\_get |
| Notifications Read - Get notification detail | Notifications Retrieves the details of a notification of a given ID. Get notification detail  | /notifications/{id}/\_\_\_get |
| Pii-rules Column-count List - Gets columns count for a PII Rule. | Security Retrieves columns count for an existing PII rule by its id. Gets columns count for a PII Rule.  | /pii-rules/{id}/column-count/\_\_\_get |
| Pii-rules Read - Gets PII rule. | Security Retrieves an existing PII rule by its id. Gets PII rule.  | /pii-rules/{id}/\_\_\_get |
| Plugins Read - Gets plugin details. | Data Types Retrieves plugin details for the given plugin id. Gets plugin details.  | /plugins/{id}/\_\_\_get |
| Recycle-bin Read - Get deleted item | Trash Retrieves a deleted item from the recycle bin. Get deleted item  | /recycle-bin/{id}/\_\_\_get |
| Roles Read - Get role | Permissions Retrieves an existing role by its id. Get role  | /roles/{id}/\_\_\_get |
| Search Core-create List - Create Solr core | Search Creates Solr core. The core must not exist in Solr yet. Create Solr core  | /search/core-create/\_\_\_get |
| Search Core-reload List - Reload Solr core | Search Reloads an existing Solr core. Reload Solr core  | /search/core-reload/\_\_\_get |
| Search Core-status List - Get Solr core status | Search Returns Solr core status. Get Solr core status  | /search/core-status/\_\_\_get |
| Search Core-unload List - Unload Solr core | Search Unloads an existing Solr core. Unload Solr core  | /search/core-unload/\_\_\_get |
| Search Select List - Search Solr | Search Search Solr by sending a query. Search Solr  | /search/select/\_\_\_get |
| Search Server-status List - Get Solr server status | Search Returns Solr server status. Get Solr server status  | /search/server-status/\_\_\_get |
| Search Suggest List - Search query suggestion | Search Get Solr search suggestions for a given query parameter. Search query suggestion  | /search/suggest/\_\_\_get |
| Sync-plugins List - Syncs the plugin and data type info between execut... | Data Types Syncs the plugin and data type info to the metastore after reading from the executor service. Syncs the plugin and data type info between executor and metastore.  | /sync-plugins/\_\_\_get |
| Unifi-settings Read - Get setting | Settings Retrieves an existing setting by its id. Get setting  | /unifi-settings/{setting\_name}/\_\_\_get |
| Users Ownership-count Read - Get user object ownership count | Permissions Counts the number of objects that the user owns per object type.  Ownership is determined by the created_by fields of various objects. Get user object ownership count  | /users/{id}/ownership-count/\_\_\_get |
| Users Read - Get user | Permissions Retrieves an existing user by its id. Get user  | /users/{id}/\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Comments List - List comments | Collaboration Retrieves a list of comment objects. List comments  | /comments/\_\_\_get |
| Comments Replies List - List comment replies | Collaboration Retrieves a list of replies for a given comment. List comment replies  | /comments/{id}/replies/\_\_\_get |
| Data-sets Columns Glossary List - List glossaries associated with dataset column | Glossary Retrieves a list of business glossary objects associated with a given dataset column. List glossaries associated with dataset column  | /data-sets/columns/{id}/glossary/\_\_\_get |
| Data-sets Columns Groups Read - List dataset columns for the given group | Datasets Permissions Retrieves a list of all the dataset columns with permission fields          annotated for the given group List dataset columns for the given group  | /data-sets/columns/groups/{group\_id}/\_\_\_get |
| Data-sets Columns List - List dataset columns | Datasets Retrieves a list of all the dataset columns. List dataset columns  | /data-sets/columns/\_\_\_get |
| Data-sets Columns Statistics List - List column statistics | Statistics Retrives all the statistics attributes computed for this dataset column. List column statistics  | /data-sets/columns/{id}/statistics/\_\_\_get |
| Data-sets Columns Users Read - List dataset columns for the given user | Datasets Permissions Retrieves a list of all the dataset columns with permission fields          annotated for the given user List dataset columns for the given user  | /data-sets/columns/users/{user\_id}/\_\_\_get |
| Data-sets Comments List - List comments of a dataset | Collaboration Retrieves a list of comments for a given dataset. List comments of a dataset  | /data-sets/{id}/comments/\_\_\_get |
| Data-sets Comments Users List - List users who commented on a dataset | Collaboration Retrieves a list of users who commented on a given dataset. List users who commented on a dataset  | /data-sets/{id}/comments/users/\_\_\_get |
| Data-sets Csv-files Columns List - List columns for a csvfile dataset | Datasets Retrieves a list of columns for a csvfile dataset. List columns for a csvfile dataset  | /data-sets/csv-files/{id}/columns/\_\_\_get |
| Data-sets Csv-files List - List file datasets | Datasets Retrieves a list of file based datasets. List file datasets  | /data-sets/csv-files/\_\_\_get |
| Data-sets Csv-files Sample-data List - Get sample data for a csvfile dataset | Datasets Retrieves a sample data for a csvfile dataset.         This fetches the first N rows from the file associated with the csvfile dataset. N is configured by          the setting DEFAULT_SAMPLE_SIZE. For hierarchical datasets like JSON, PARQUET and XML, this returns          a flattened sample along with a hierarchical version of the sample data. Flattening is done by recursively         unnesting complex fields in the schema until the dataset consists only of simple fields. Flattening can         result in an explosion of the number of rows of a dataset.   Get sample data for a csvfile dataset  | /data-sets/csv-files/{id}/sample-data/\_\_\_get |
| Data-sets Database-tables Columns List - List columns for a database table dataset | Datasets Retrieves a list of columns for a database table dataset. List columns for a database table dataset  | /data-sets/database-tables/{id}/columns/\_\_\_get |
| Data-sets Database-tables Foreign-keys List - List foreign keys for a database table dataset | Datasets Retrieves a list of foreign key objects for a database table dataset. List foreign keys for a database table dataset  | /data-sets/database-tables/{id}/foreign-keys/\_\_\_get |
| Data-sets Database-tables Indexes List - List indexes for a database table dataset | Datasets Retrieves a list of indexes for a database table dataset. List indexes for a database table dataset  | /data-sets/database-tables/{id}/indexes/\_\_\_get |
| Data-sets Database-tables List - List database datasets | Datasets Retrieves a list of database table based datasets. List database datasets  | /data-sets/database-tables/\_\_\_get |
| Data-sets Database-tables Primary-key List - List primary keys for a database table dataset | Datasets Retrieves a list of primary key objects for a database table dataset. List primary keys for a database table dataset  | /data-sets/database-tables/{id}/primary-key/\_\_\_get |
| Data-sets Database-tables Sample-data List - Get sample data for a database dataset | Datasets Retrieves a sample data for a database table dataset.         This fetches the first N rows from the table associated with the database dataset. N is configured by          the setting DEFAULT_SAMPLE_SIZE.    Get sample data for a database dataset  | /data-sets/database-tables/{id}/sample-data/\_\_\_get |
| Data-sets Endorsements List - List current datasets endorsements | Datasets Retrieves a list of endorsed datasets along with their current endorsement status. The possible values are 'NE' (not endorsed), 'TR' (trusted) and 'DE' (deprecated). Datasets that were not endorsed are ommited from the response unless filter_dataset__in=[] was indicated in the request. List current datasets endorsements  | /data-sets/endorsements/\_\_\_get |
| Data-sets Glossary List - List glossaries associated with dataset | Glossary Retrieves a list of business glossary objects associated with a given dataset. List glossaries associated with dataset  | /data-sets/{id}/glossary/\_\_\_get |
| Data-sets Groups Read - Get all datasets with annotated group permissions | Permissions Datasets Lists all datasets along with permission annotation to the provided input group.  The response includes a only minimal information about the dataset itself and   the permission information is provided via the is_owner and has_permissions fields.   Note that groups are never owners of datasets, therefore is_owner will be always false Get all datasets with annotated group permissions  | /data-sets/groups/{group\_id}/\_\_\_get |
| Data-sets Impacted List - List objects impacted by dataset delete | Datasets Trash Retrieves a objects impacted by dataset delete. For example, if a dataset is used as a target dataset in a job, deleting it will impact the job. List objects impacted by dataset delete  | /data-sets/{id}/impacted/\_\_\_get |
| Data-sets List - List datasets | Datasets Retrieves a list of datasets. List datasets  | /data-sets/\_\_\_get |
| Data-sets Tags List - List dataset tags | Datasets Collaboration Retrieves a list of tag names and their count that are being used in datasets. List dataset tags  | /data-sets/tags/\_\_\_get |
| Data-sets Users Read - Get all datasets with annotated user permissions | Permissions Datasets Lists all datasets along with permission annotation to the provided input user.  The response includes a only minimal information about the dataset itself and   the permission information is provided via the is_owner and has_permissions fields Get all datasets with annotated user permissions  | /data-sets/users/{user\_id}/\_\_\_get |
| Data-sources Databases List - List data source databases | Data Sources Retrieves a list of databases for a given data source. List data source databases  | /data-sources/{id}/databases/\_\_\_get |
| Data-sources Db-schema-browser List - List data source schema | Data Sources Retrieves data source schema information for a given data source. List data source schema  | /data-sources/{id}/db-schema-browser/\_\_\_get |
| Data-sources File-browser List - List data source file system information | Data Sources Retrieves data source file system information for a given data source. List data source file system information  | /data-sources/{id}/file-browser/\_\_\_get |
| Data-sources File-info Column Statistics List - List dataset column statistics | Data Sources Statistics Retrieves column statistics for a dataset of a provided data source. List dataset column statistics  | /data-sources/{id}/file-info/column/statistics/\_\_\_get |
| Data-sources File-info Desc-path List - List path description of file | Data Sources Retrieves path description for a file of a provided data source. List path description of file  | /data-sources/{id}/file-info/desc-path/\_\_\_get |
| Data-sources File-info List - List sample data of file | Data Sources Retrieves sample data of a file for a provided filesystem based data source. List sample data of file  | /data-sources/{id}/file-info/\_\_\_get |
| Data-sources File-info Statistics List - List dataset level statistics | Statistics Data Sources Retrieves dataset level statistics for a dataset of a provided data source. List dataset level statistics  | /data-sources/{id}/file-info/statistics/\_\_\_get |
| Data-sources Glossary List - List glossaries associated with data source | Glossary Retrieves a list of business glossary objects associated with a given data source. List glossaries associated with data source  | /data-sources/{id}/glossary/\_\_\_get |
| Data-sources Groups Read - Get all data sources with annotated group permissi... | Permissions Data Sources Lists all data sources along with permission annotation to the provided input group.  The permission information is provided via the is_owner and has_permissions fields.   Note that groups are never owners of objects, therefore is_owner will be always false Get all data sources with annotated group permissions  | /data-sources/groups/{group\_id}/\_\_\_get |
| Data-sources Import-activities Dependents List - List delete dependents of a source import activity | Data Sources Trash Retrieves a list of delete dependents of a source import activity. Objects on this list will be deleted when the import activity is deleted. List delete dependents of a source import activity  | /data-sources/import-activities/{id}/dependents/\_\_\_get |
| Data-sources Import-activities List - List data source import activities | Data Sources Retrieves data source import activities for a provided data source. List data source import activities  | /data-sources/{source\_id}/import-activities/\_\_\_get |
| Data-sources Import-schedules Activities List - Lists activities for the source import schedule | Data Sources Retrieves a list of all activities for the given source import schedule. Lists activities for the source import schedule  | /data-sources/import-schedules/{schedule\_id}/activities/\_\_\_get |
| Data-sources Import-schedules Dependents List - List delete dependents of a source import schedule | Data Sources Trash Retrieves a list of delete dependents of a source import schedule. Objects on this list will be deleted when the import schedule is deleted. List delete dependents of a source import schedule  | /data-sources/import-schedules/{id}/dependents/\_\_\_get |
| Data-sources Import-schedules List - List source import schedules | Data Sources Retrieves a list of source import schedules. List source import schedules  | /data-sources/import-schedules/\_\_\_get |
| Data-sources Import-schedules Tags List - List source import schedule tags | Data Sources Collaboration Retrieves a list of tag names and their count that are being used in source import schedules. List source import schedule tags  | /data-sources/import-schedules/tags/\_\_\_get |
| Data-sources Table-browser List - List table information | Data Sources Retrieves the table information of a provided data source. List table information  | /data-sources/{id}/table-browser/\_\_\_get |
| Data-sources Tags List - List data source tags | Data Sources Collaboration Retrieves a list of tag names and their count that are being used in data sources. List data source tags  | /data-sources/tags/\_\_\_get |
| Data-sources Users Read - Get all data sources with annotated group permissi... | Permissions Data Sources Lists all data sources along with permission annotation to the provided input group.  The permission information is provided via the is_owner and has_permissions fields. Get all data sources with annotated group permissions  | /data-sources/users/{user\_id}/\_\_\_get |
| Data-types Dependents List - Gets objects dependent on a data type. | Data Types Retrieves the list of objects dependent on a data type if user is admin or has permissions on all dependents columns. Gets objects dependent on a data type.  | /data-types/{id}/dependents/\_\_\_get |
| Data-types List - List all data types implemented by all plugins | Data Types Retrieves all data types implemented by all plugins. List all data types implemented by all plugins  | /data-types/\_\_\_get |
| Data-types Tags List - List data type tags | Data Types Collaboration Retrieves a list of tag names and their count that are being used in data types. List data type tags  | /data-types/tags/\_\_\_get |
| Datastore-adapters List - List datastore adapters | Adapters Retrieves a list of installed DatastoreAdapter objects on the DCP instance. Required permissions: Role permissions based on "adapter_perms" field. List datastore adapters  | /datastore-adapters/\_\_\_get |
| Feed Active-users List - List the most active users | Collaboration Retrieves a list of active users with the most feed events, and the number of events they participated in. List the most active users  | /feed/active-users/\_\_\_get |
| Feed List - List feed events | Collaboration Retrieves a list of feed events that occurred in the system. List feed events  | /feed/\_\_\_get |
| Glossary Columns List - List dataset columns associated with glossary | Glossary Retrieves a list of dataset column objects associated with a given business glossary. List dataset columns associated with glossary  | /glossary/{id}/columns/\_\_\_get |
| Glossary Comments List - List comments of a glossary | Collaboration Retrieves a list of comments for a given glossary. List comments of a glossary  | /glossary/{id}/comments/\_\_\_get |
| Glossary Comments Users List - List users who commented on a glossary | Collaboration Retrieves a list of users who commented on a given glossary. List users who commented on a glossary  | /glossary/{id}/comments/users/\_\_\_get |
| Glossary Data-sets List - List datasets associated with glossary | Glossary Retrieves a list of dataset objects associated with a given business glossary. List datasets associated with glossary  | /glossary/{id}/data-sets/\_\_\_get |
| Glossary Data-sources List - List data sources associated with glossary | Glossary Retrieves a list of data sources objects associated with a given business glossary. List data sources associated with glossary  | /glossary/{id}/data-sources/\_\_\_get |
| Glossary Dependents List - List delete dependents of a glossary | Glossary Trash Retrieves a list of delete dependents of a business glossary. Objects on this list will be deleted when the glossary is deleted. List delete dependents of a glossary  | /glossary/{id}/dependents/\_\_\_get |
| Glossary Endorsements List - List current business glossary endorsements | Glossary Retrieves a list of endorsed glossaries along with their current endorsement status. The possible values are 'NE' (not endorsed), 'TR' (trusted) and 'DE' (deprecated). Glossaries that were not endorsed are ommited from the response unless filter_glossary__in=[] was indicated in the request. List current business glossary endorsements  | /glossary/endorsements/\_\_\_get |
| Glossary List - List glossaries | Glossary Retrieves a list of business glossary objects. List glossaries  | /glossary/\_\_\_get |
| Glossary Tags List - List business glossary tags | Glossary Collaboration Retrieves a list of tag names and their count that are being used in business glossaries. List business glossary tags  | /glossary/tags/\_\_\_get |
| Groups List - List groups | Permissions Retrieves a list of groups. List groups  | /groups/\_\_\_get |
| Notification-types List - List the notification type objects | Notifications Retrieves the list of notification type objects. List the notification type objects  | /notification-types/\_\_\_get |
| Notifications List - List the notifications | Notifications Retrieves list of notifications for the current logged-in user. List the notifications  | /notifications/\_\_\_get |
| Notifications Unread-count List - Get number of unread notifications | Notifications Retrieves the number of unread notifications for the current logged-in user. Get number of unread notifications  | /notifications/unread-count/\_\_\_get |
| Pii-rules Dependents List - Gets objects dependent on a PII Rule. | Security Retrieves the list of objects dependent on a PII Rule if user is admin or has permissions on all dependents columns. Gets objects dependent on a PII Rule.  | /pii-rules/{id}/dependents/\_\_\_get |
| Pii-rules List - List of pii rules. | Security Retrieves a list of existing pii/masking rules. List of pii rules.  | /pii-rules/\_\_\_get |
| Pii-rules Tags List - List PII rule tags | Security Collaboration Retrieves a list of tag names and their count that are being used in PII rules. List PII rule tags  | /pii-rules/tags/\_\_\_get |
| Plugins Data-types List - List of data types. | Data Types Retrieves a list of data types for a given plugin id. List of data types.  | /plugins/{id}/data-types/\_\_\_get |
| Plugins List - List of executor plugins. | Data Types Retrieves a list of executor plugins. List of executor plugins.  | /plugins/\_\_\_get |
| Plugins Tags List - List data type plugin tags | Data Types Collaboration Retrieves a list of tag names and their count that are being used in data type plugins. List data type plugin tags  | /plugins/tags/\_\_\_get |
| Recycle-bin Items List - List deleted item dependencies | Trash Get a list of a deleted item dependencies. The dependencies are the objects that were deleted as part of the main item delete. For example, deleting a Source will also delete its Datasets, which will appear as its dependents. List deleted item dependencies  | /recycle-bin/{id}/items/\_\_\_get |
| Recycle-bin List - List deleted items | Trash Retrieves a list of deleted items from the recycle bin. List deleted items  | /recycle-bin/\_\_\_get |
| Roles List - List roles | Permissions Retrieves a list of roles. List roles  | /roles/\_\_\_get |
| Roles Options List - List role fields and options | Permissions Retrieves a list of role fields with their allowed options. The possible options are 'Y' (always), 'N' (never), 'O' (based on object permissions). Different categories and operations support different options, and this view shows them. Only categories that are supported by the running product (catalog/platform) are returned. List role fields and options  | /roles/options/\_\_\_get |
| Supported-adapters List - List supported adapters | Adapters Retrieves a list of adapters supported by DCP. List supported adapters  | /supported-adapters/\_\_\_get |
| Unifi-settings List - List settings | Settings Retrieves a list of setting objects. List settings  | /unifi-settings/\_\_\_get |
| Users List - List users | Permissions Retrieves a list of users. List users  | /users/\_\_\_get |
| Users Role List - Get the user role | Permissions Get the role assigned to the given user. Get the user role  | /users/{id}/role/\_\_\_get |
| Users Tags List - List user tags | Permissions Collaboration Retrieves a list of tag names and their count that are being used in users. List user tags  | /users/tags/\_\_\_get |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Comments Update - Update comment (PUT) | Collaboration Updates an existing comment by adding or modifying the given fields. Update comment  | /comments/{id}/\_\_\_put |
| Data-sets Csv-files Columns Update - Update csvfile column (PUT) | Datasets Updates an existing csvfile column by adding or modifying the given fields. Update csvfile column  | /data-sets/csv-files/{csvfile\_pk}/columns/{id}/\_\_\_put |
| Data-sets Csv-files Update - Update csvfile dataset (PUT) | Datasets Updates an existing csvfile dataset by adding or modifying the given fields. Update csvfile dataset  | /data-sets/csv-files/{id}/\_\_\_put |
| Data-sets Database-tables Columns Update - Update database column (PUT) | Datasets Updates an existing database column by adding or modifying the given fields. Update database column  | /data-sets/database-tables/{dbtable\_pk}/columns/{id}/\_\_\_put |
| Data-sets Database-tables Foreign-keys Update - Update foregin key (PUT) | Datasets Updates an existing foreign key object by adding or modifying the given fields. Update foregin key  | /data-sets/database-tables/{dbtable\_pk}/foreign-keys/{id}/\_\_\_put |
| Data-sets Database-tables Indexes Update - Update database index (PUT) | Datasets Updates an existing database table index object by adding or modifying the given fields. Update database index  | /data-sets/database-tables/{dbtable\_pk}/indexes/{id}/\_\_\_put |
| Data-sets Database-tables Primary-key Update - Update database primary key (PUT) | Datasets Updates an existing database table primary key object by adding or modifying the given fields. Update database primary key  | /data-sets/database-tables/{dbtable\_pk}/primary-key/{id}/\_\_\_put |
| Data-sets Database-tables Update - Update database table dataset (PUT) | Datasets Updates an existing database table dataset by adding or modifying the given fields. Update database table dataset  | /data-sets/database-tables/{id}/\_\_\_put |
| Data-sets Endorsement Partial Update - Gets dataset endorsement details (PATCH) | Datasets Retrieves the endorsement details of a given dataset. Gets dataset endorsement details  | /data-sets/{id}/endorsement/\_\_\_patch |
| Data-sets Endorsement Update - Gets dataset endorsement details (PUT) | Datasets Retrieves the endorsement details of a given dataset. Gets dataset endorsement details  | /data-sets/{id}/endorsement/\_\_\_put |
| Data-sets Tags Create - Add tag to dataset (POST) | Datasets Collaboration Adds a tag to a given dataset object. Add tag to dataset  | /data-sets/{id}/tags/{tag\_name}/\_\_\_post |
| Data-sources Import-schedules Tags Create - Add tag to source import schedule (POST) | Data Sources Collaboration Adds a tag to a given source import schedule object. Add tag to source import schedule  | /data-sources/import-schedules/{id}/tags/{tag\_name}/\_\_\_post |
| Data-sources Import-schedules Update - Update a source import schedule (PUT) | Glossary Updates an existing source import schedule by adding or modifying the given fields. Update a source import schedule  | /data-sources/import-schedules/{id}/\_\_\_put |
| Data-sources Tags Create - Add tag to data source (POST) | Data Sources Collaboration Adds a tag to a given data source object. Add tag to data source  | /data-sources/{id}/tags/{tag\_name}/\_\_\_post |
| Data-sources Update - Update a data source (PUT) | Data Sources Updates an existing data source by adding or modifying the given fields. Update a data source  | /data-sources/{id}/\_\_\_put |
| Data-types Tags Create - Add tag to data type (POST) | Data Types Collaboration Adds a tag to a given data type object. Add tag to data type  | /data-types/{id}/tags/{tag\_name}/\_\_\_post |
| Datastore-adapters Update - Update datastore adapter (PUT) | Adapters Updates an existing datastore adapter by adding or modifying the given fields. Update datastore adapter  | /datastore-adapters/{id}/\_\_\_put |
| Glossary Endorsement Partial Update - Gets glossary endorsement details (PATCH) | Glossary Retrieves the endorsement details of a given glossary. Gets glossary endorsement details  | /glossary/{id}/endorsement/\_\_\_patch |
| Glossary Endorsement Update - Gets glossary endorsement details (PUT) | Glossary Retrieves the endorsement details of a given glossary. Gets glossary endorsement details  | /glossary/{id}/endorsement/\_\_\_put |
| Glossary Tags Create - Add tag to business glossary (POST) | Glossary Collaboration Adds a tag to a given business glossary object. Add tag to business glossary  | /glossary/{id}/tags/{tag\_name}/\_\_\_post |
| Glossary Update - Update a glossary (PUT) | Glossary Updates an existing business glossary by adding or modifying the given fields. Update a glossary  | /glossary/{id}/\_\_\_put |
| Groups Update - Update group (PUT) | Permissions Updates an existing group by adding or modifying the given fields. Update group  | /groups/{id}/\_\_\_put |
| Notifications Mark-all-read Update - Update all notifications as read (PUT) | Notifications Marks all of the current user's notifications as read. Update all notifications as read  | /notifications/mark-all-read/\_\_\_put |
| Notifications Toggle-read-status Update - Update read status of a notification (PUT) | Notifications Change the read status of a notification of a given ID. Update read status of a notification  | /notifications/{id}/toggle-read-status/\_\_\_put |
| Pii-rules Tags Create - Add tag to PII rule (POST) | Security Collaboration Adds a tag to a given PII rule object. Add tag to PII rule  | /pii-rules/{id}/tags/{tag\_name}/\_\_\_post |
| Plugins Tags Create - Add tag to data type plugin (POST) | Data Types Collaboration Adds a tag to a given data type plugin object. Add tag to data type plugin  | /plugins/{id}/tags/{tag\_name}/\_\_\_post |
| Recycle-bin Update - Restore deleted item (PUT) | Trash Restores a deleted item from the recycle bin. Restore deleted item  | /recycle-bin/{id}/\_\_\_put |
| Roles Update - Update role (PUT) | Permissions Updates an existing role by adding or modifying the given fields. Update role  | /roles/{id}/\_\_\_put |
| Unifi-settings Update - Update setting (PUT) | Settings Updates an existing setting by adding or modifying the given fields. Update setting  | /unifi-settings/\_\_\_put |
| Users Tags Create - Add tag to user (POST) | Permissions Collaboration Adds a tag to a given user object. Add tag to user  | /users/{id}/tags/{tag\_name}/\_\_\_post |
| Users Update - Update user (PUT) | Permissions Updates an existing user by adding or modifying the given fields. Update user  | /users/{id}/\_\_\_put |


