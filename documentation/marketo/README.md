# Marketo REST API Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### SAP Concur OpenAPI Service

The URL for the SAP Concur OpenAPI descriptor file

**Type** - string

##### Allowed Values

 * Allocations
 * Attendees
 * AttendeeTypes
 * Budget
 * ConcurRequest
 * ConnectionRequests 3.0
 * ConnectionRequests 3.2
 * DigitalTaxInvoices
 * Entries
 * EntryAttendeeAssociations
 * ExpenseGroupConfigurations
 * InvoicePay
 * Itemizations
 * LatestBookings
 * List
 * ListItem
 * ListItems
 * Lists
 * LocalizedData
 * Locations
 * Opportunities
 * PaymentRequest
 * PaymentRequestDigest
 * PurchaseOrderReceipts
 * PurchaseOrders
 * PurchaseRequest
 * QuickExpenses
 * ReceiptImages
 * Receipts
 * Reports
 * RequestGroupConfigurations
 * SalesTaxValidationRequest
 * Suppliers
 * Users
 * VendorBank
 * VendorGroup
 * Vendors


#### Other SAP Concur REST API Swagger URL

This will override selections so you can provide a url to any swagger file.

**Type** - string



#### URL

The URL for the Service service

**Type** - string



#### AuthenticationType

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - oauth

# Operation Tab


## CREATE/EXECUTE


## UPDATE


## GET


## DELETE


## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1


### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

The query filter supports any number of non-nested expressions which will be AND'ed together.

Example:
((foo lessThan 30) AND (baz lessThan 42))

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts




# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| AddCustomActivityUsingPOST - Add Custom Activities | Activities Allows insertion of custom activities associated to given lead records.  Requires provisioning of custom activity types to utilize.  Required Permissions: Read-Write Activity Add Custom Activities  | /rest/v1/activities/external.json\_\_\_post |
| AddCustomObjectTypeFieldsUsingPOST - Add Custom Object Type Fields | Custom Objects Adds fields to custom object type.  Required Permissions: Read-Write Custom Object Type Add Custom Object Type Fields  | /rest/v1/customobjects/schema/{apiName}/addField.json\_\_\_post |
| AddLeadsToListUsingPOST - Add to List | Static Lists Adds a given set of person records to a target static list.  There is a limit of 300 lead ids per request.  Required Permissions: Read-Write Lead Add to List  | /rest/v1/lists/{listId}/leads.json\_\_\_post |
| AddNamedAccountListMembersUsingPOST - Add Named Account List Members | Named Account Lists Adds named account records to a named account list.  Required Permissions: Read-Write Named Account Add Named Account List Members  | /rest/v1/namedAccountList/{id}/namedAccounts.json\_\_\_post |
| ApproveCustomActivityTypeUsingPOST - Approve Custom Activity Type | Activities Approves the current draft of the type, and makes it the live version.  This will delete the current live version of the type.  Required Permissions: Read-Write Activity Metadata Approve Custom Activity Type  | /rest/v1/activities/external/type/{apiName}/approve.json\_\_\_post |
| ApproveCustomObjectTypeUsingPOST - Approve Custom Object Type | Custom Objects Approves the current draft of the type, and makes it the live version.  This will delete the current live version of the type.  Required Permissions: Read-Write Custom Object Type Approve Custom Object Type  | /rest/v1/customobjects/schema/{apiName}/approve.json\_\_\_post |
| AssociateLeadUsingPOST - Associate Lead | Leads Associates a known Marketo lead record to a munchkin cookie and its associated web acitvity history.  Required Permissions: Read-Write Lead Associate Lead  | /rest/v1/leads/{leadId}/associate.json\_\_\_post |
| CancelExportActivitiesUsingPOST - Cancel Export Activity Job | Bulk Export Activities Cancel export job.  Required Permissions: Read-Only Activity Cancel Export Activity Job  | /bulk/v1/activities/export/{exportId}/cancel.json\_\_\_post |
| CancelExportLeadsUsingPOST - Cancel Export Lead Job | Bulk Export Leads Cancel export job.  Required Permissions: Read-Only Lead Cancel Export Lead Job  | /bulk/v1/leads/export/{exportId}/cancel.json\_\_\_post |
| CancelExportProgramMembersUsingPOST - Cancel Export Program Member Job | Bulk Export Program Members Cancel export job.  Required Permissions: Read-Only Lead Cancel Export Program Member Job  | /bulk/v1/program/members/export/{exportId}/cancel.json\_\_\_post |
| ChangeLeadProgramStatusUsingPOST - Change Lead Program Status | Leads Changes the program status of a list of leads in a target program.  Only existing members of the program may have their status changed with this API.  Required Permissions: Read-Write Lead Change Lead Program Status  | /rest/v1/leads/programs/{programId}/status.json\_\_\_post |
| CreateCustomActivityTypeAttributesUsingPOST - Create Custom Activity Type Attributes | Activities Adds activity attributes to the target type.  These are added to the draft version of the type.  Required Permissions: Read-Write Activity Metadata Create Custom Activity Type Attributes  | /rest/v1/activities/external/type/{apiName}/attributes/create.json\_\_\_post |
| CreateCustomActivityTypeUsingPOST - Create Custom Activity Type | Activities Creates a new custom activity type draft in the target instance.  Required Permissions: Read-Write Activity Metadata Create Custom Activity Type  | /rest/v1/activities/external/type.json\_\_\_post |
| CreateExportActivitiesUsingPOST - Create Export Activity Job | Bulk Export Activities Create export job for search criteria defined via "filter" parameter.  Request returns the "exportId" which is passed as a parameter in subsequent calls to Bulk Export Activities endpoints.  Use Enqueue Export Activity Job endpoint to queue the export job for processing.  Use Get Export Activity Job Status endpoint to retrieve status of export job.  Required Permissions: Read-Only Activity Create Export Activity Job  | /bulk/v1/activities/export/create.json\_\_\_post |
| CreateExportLeadsUsingPOST - Create Export Lead Job | Bulk Export Leads Create export job for search criteria defined via "filter" parameter.  Request returns the "exportId" which is passed as a parameter in subsequent calls to Bulk Export Leads endpoints.  Use Enqueue Export Lead Job endpoint to queue the export job for processing.  Use Get Export Lead Job Status endpoint to retrieve status of export job.  Required Permissions: Read-Only Lead Create Export Lead Job  | /bulk/v1/leads/export/create.json\_\_\_post |
| CreateExportProgramMembersUsingPOST - Create Export Program Member Job | Bulk Export Program Members Create export job for search criteria defined via "filter" parameter.  Request returns the "exportId" which is passed as a parameter in subsequent calls to Bulk Export Program Members endpoints.  Use Enqueue Export Program Member Job endpoint to queue the export job for processing.  Use Get Export Program Member Job Status endpoint to retrieve status of export job.  Required Permissions: Read-Only Lead Create Export Program Member Job  | /bulk/v1/program/members/export/create.json\_\_\_post |
| DeleteCompaniesUsingPOST - Delete Companies | Companies Deletes the included list of company records from the destination instance.  Required Permissions: Read-Write Company Delete Companies  | /rest/v1/companies/delete.json\_\_\_post |
| DeleteCustomActivityTypeAttributesUsingPOST - Delete Custom Activity Type Attributes | Activities Deletes the target attributes from the custom activity type draft.  The apiName of each attribute is the primary key for the update.  Required Permissions: Read-Write Activity Metadata Delete Custom Activity Type Attributes  | /rest/v1/activities/external/type/{apiName}/attributes/delete.json\_\_\_post |
| DeleteCustomActivityTypeUsingPOST - Delete Custom Activity Type | Activities Deletes the target custom activity type.  The type must first be removed from use by any assets, such as triggers or filters.  Required Permissions: Read-Write Activity Metadata Delete Custom Activity Type  | /rest/v1/activities/external/type/{apiName}/delete.json\_\_\_post |
| DeleteCustomObjectsUsingPOST - Delete Custom Objects | Custom Objects Deletes a given set of custom object records.  Required Permissions: Read-Write Custom Object Delete Custom Objects  | /rest/v1/customobjects/{customObjectName}/delete.json\_\_\_post |
| DeleteCustomObjectTypeFieldsUsingPOST - Delete Custom Object Type Fields | Custom Objects Deletes fields from custom object type.  Required Permissions: Read-Write Custom Object Type Delete Custom Object Type Fields  | /rest/v1/customobjects/schema/{apiName}/deleteField.json\_\_\_post |
| DeleteCustomObjectTypeUsingPOST - Delete Custom Object Type | Custom Objects Deletes the target custom object type.  The type must first be removed from use by any assets, such as triggers or filters.  Required Permissions: Read-Write Custom Object Type Delete Custom Object Type  | /rest/v1/customobjects/schema/{apiName}/delete.json\_\_\_post |
| DeleteLeadsUsingPOST - Delete Leads | Leads Delete a list of leads from the destination instance.  Required Permissions: Read-Write Lead Delete Leads  | /rest/v1/leads/delete.json\_\_\_post |
| DeleteNamedAccountListsUsingPOST - Delete Named Account Lists | Named Account Lists Delete named account lists by dedupe fields, or by id field.  Required Permissions: Read-Write Named Account List Delete Named Account Lists  | /rest/v1/namedAccountLists/delete.json\_\_\_post |
| DeleteNamedAccountsUsingPOST - Delete NamedAccounts | Named Accounts Deletes a list of namedaccount records from the target instance.  Input records should have only one member, based on the value of 'dedupeBy'.  Required Permissions: Read-Write Named Account Delete NamedAccounts  | /rest/v1/namedaccounts/delete.json\_\_\_post |
| DeleteOpportunitiesUsingPOST - Delete Opportunities | Opportunities Deletes a list of opportunity records from the target instance.  Input records should only have one member, based on the value of 'dedupeBy'.  Required Permissions: Read-Write Named Opportunity Delete Opportunities  | /rest/v1/opportunities/delete.json\_\_\_post |
| DeleteOpportunityRolesUsingPOST - Delete Opportunity Roles | Opportunities Deletes a list of opportunities from the target instance.  Required Permissions: Read-Write Named Opportunity Delete Opportunity Roles  | /rest/v1/opportunities/roles/delete.json\_\_\_post |
| DeleteSalesPersonUsingPOST - Delete SalesPersons | Sales Persons Deletes a list of salesperson records from the target instance.  Input records should have only one member, based on the value of 'dedupeBy'.  Required Permissions: Read-Write Sales Person Delete SalesPersons  | /rest/v1/salespersons/delete.json\_\_\_post |
| DiscardCustomObjectTypeUsingPOST - Discard Custom Object Type Draft | Custom Objects Discards the current draft of the custom object type.  Required Permissions: Read-Write Custom Object Type Discard Custom Object Type Draft  | /rest/v1/customobjects/schema/{apiName}/discardDraft.json\_\_\_post |
| DiscardDraftofCustomActivityTypeUsingPOST - Discard Custom Activity Type Draft | Activities Discards the current draft of the custom activity type.  Required Permissions: Read-Write Activity Metadata Discard Custom Activity Type Draft  | /rest/v1/activities/external/type/{apiName}/discardDraft.json\_\_\_post |
| EnqueueExportActivitiesUsingPOST - Enqueue Export Activity Job | Bulk Export Activities Enqueue export job. This will place export job in queue, and will start the job when computing resources become available.  The export job must be in "Created" state.  Use Get Export Activity Job Status endpoint to retrieve status of export job.  Required Permissions: Read-Only Activity Enqueue Export Activity Job  | /bulk/v1/activities/export/{exportId}/enqueue.json\_\_\_post |
| EnqueueExportLeadsUsingPOST - Enqueue Export Lead Job | Bulk Export Leads Enqueue export job. This will place export job in queue, and will start the job when computing resources become available.  The export job must be in "Created" state.  Use Get Export Lead Job Status endpoint to retrieve status of export job.  Required Permissions: Read-Only Lead Enqueue Export Lead Job  | /bulk/v1/leads/export/{exportId}/enqueue.json\_\_\_post |
| EnqueueExportProgramMembersUsingPOST - Enqueue Export Program Member Job | Bulk Export Program Members Enqueue export job. This will place export job in queue, and will start the job when computing resources become available.  The export job must be in "Created" state.  Use Get Export Program Member Job Status endpoint to retrieve status of export job.  Required Permissions: Read-Only Lead Enqueue Export Program Member Job  | /bulk/v1/program/members/export/{exportId}/enqueue.json\_\_\_post |
| ImportCustomObjectUsingPOST - Import Custom Objects | Bulk Import Custom Objects Imports a file containing data records into the target instance.  Required Permissions: Read-Write Custom Object Import Custom Objects  | /bulk/v1/customobjects/{apiName}/import.json\_\_\_post |
| ImportLeadUsingPOST - Import Leads | Bulk Import Leads Imports a file containing data records into the target instance.  Required Permissions: Read-Write Lead Import Leads  | /bulk/v1/leads.json\_\_\_post |
| ImportProgramMemberUsingPOST - Import Program Members | Bulk Import Program Members Imports a file containing data records into the target instance.  Required Permissions: Read-Write Lead Import Program Members  | /bulk/v1/program/{programId}/members/import.json\_\_\_post |
| MergeLeadsUsingPOST - Merge Leads | Leads Merges two or more known lead records into a single lead record.  Required Permissions: Read-Write Lead Merge Leads  | /rest/v1/leads/{leadId}/merge.json\_\_\_post |
| PushToMarketoUsingPOST - Push Lead to Marketo | Leads Upserts a lead, and generates a Push Lead to Marketo activity.  Required Permissions: Read-Write Lead Push Lead to Marketo  | /rest/v1/leads/push.json\_\_\_post |
| RemoveNamedAccountListMembersUsingPOST - Remove Named Account List Members | Named Account Lists Removes named account members from a named account list.  Required Permissions: Read-Write Named Account Remove Named Account List Members  | /rest/v1/namedAccountList/{id}/namedAccounts/remove.json\_\_\_post |
| ScheduleCampaignUsingPOST - Schedule Campaign | Campaigns Remotely schedules a batch campaign to run at a given time.  My tokens local to the campaign's parent program can be overridden for the run to customize content.  When using the "cloneToProgramName" parameter described below, this endpoint is limited to 20 calls per day. Required Permissions: Execute Campaign Schedule Campaign  | /rest/v1/campaigns/{campaignId}/schedule.json\_\_\_post |
| SyncCompaniesUsingPOST - Sync Companies | Companies Allows inserting, updating, or upserting of company records into Marketo. Required Permissions: Read-Write Company Sync Companies  | /rest/v1/companies.json\_\_\_post |
| SyncCustomObjectsUsingPOST - Sync Custom Objects | Custom Objects Inserts, updates, or upserts custom object records to the target instance.  Required Permissions: Read-Write Custom Object Sync Custom Objects  | /rest/v1/customobjects/{customObjectName}.json\_\_\_post |
| SyncCustomObjectTypeUsingPOST - Sync Custom Object Type | Custom Objects Inserts, updates, or upserts custom object type record to the target instance.  Required Permissions: Read-Write Custom Object Type Sync Custom Object Type  | /rest/v1/customobjects/schema.json\_\_\_post |
| SyncLeadUsingPOST - Sync Leads | Leads Syncs a list of leads to the target instance.  Required Permissions: Read-Write Lead Sync Leads  | /rest/v1/leads.json\_\_\_post |
| SyncNamedAccountListsUsingPOST - Sync Named Account Lists | Named Account Lists Creates and/or updates named account list records.  Required Permissions: Read-Write Named Account List Sync Named Account Lists  | /rest/v1/namedAccountLists.json\_\_\_post |
| SyncNamedAccountsUsingPOST - Sync NamedAccounts | Named Accounts Allows inserts, updates, or upserts of namedaccounts to the target instance.  Required Permissions: Read-Write Named Account Sync NamedAccounts  | /rest/v1/namedaccounts.json\_\_\_post |
| SyncOpportunitiesUsingPOST - Sync Opportunities | Opportunities Allows inserting, updating, or upserting of opportunity records into the target instance.  Required Permissions: Read-Write Named Opportunity Sync Opportunities  | /rest/v1/opportunities.json\_\_\_post |
| SyncOpportunityRolesUsingPOST - Sync Opportunity Roles | Opportunities Allows inserts, updates and upserts of Opportunity Role records in the target instance.  Required Permissions: Read-Write Named Opportunity Sync Opportunity Roles  | /rest/v1/opportunities/roles.json\_\_\_post |
| SyncSalesPersonsUsingPOST - Sync SalesPersons | Sales Persons Allows inserts, updates, or upserts of salespersons to the target instance.  Required Permissions: Read-Write Sales Person Sync SalesPersons  | /rest/v1/salespersons.json\_\_\_post |
| TriggerCampaignUsingPOST - Request Campaign | Campaigns Passes a set of leads to a trigger campaign to run through the campaign's flow.  The designated campaign must have a Campaign is Requested: Web Service API trigger, and must be active.  My tokens local to the campaign's parent program can be overridden for the run to customize content. A maximum of 100 leads are allowed per call. Required Permissions: Execute Campaign Request Campaign  | /rest/v1/campaigns/{campaignId}/trigger.json\_\_\_post |
| UpdateCustomActivityTypeAttributesUsingPOST - Update Custom Activity Type Attributes | Activities Updates the attributes of the custom activity type draft.  The apiName of each attribute is the primary key for the update.  Required Permissions: Read-Write Activity Metadata Update Custom Activity Type Attributes  | /rest/v1/activities/external/type/{apiName}/attributes/update.json\_\_\_post |
| UpdateCustomActivityTypeUsingPOST - Update Custom Activity Type | Activities Updates the target custom activity type.  All changes are applied to the draft version of the type.  Required Permissions: Read-Write Activity Metadata Update Custom Activity Type  | /rest/v1/activities/external/type/{apiName}.json\_\_\_post |
| UpdateCustomObjectTypeFieldUsingPOST - Update Custom Object Type Field | Custom Objects Updates a field in custom object type.  Required Permissions: Read-Write Custom Object Type Update Custom Object Type Field  | /rest/v1/customobjects/schema/{apiName}/{fieldApiName}/updateField.json\_\_\_post |
| UpdatePartitionsUsingPOST - Update Lead Partition | Leads Updates the lead partition for a list of leads.  Required Permissions: Read-Write Lead Update Lead Partition  | /rest/v1/leads/partitions.json\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| RemoveLeadsFromListUsingDELETE - Remove from List | Static Lists Removes a given set of person records from a target static list.  Required Permissions: Read-Write Lead Remove from List  | /rest/v1/lists/{listId}/leads.json\_\_\_delete |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| AreLeadsMemberOfListUsingGET - Member of List | Static Lists Checks if leads are members of a given static list.  Required Permissions: Read-Write Lead Member of List  | /rest/v1/lists/{listId}/leads/ismember.json\_\_\_get |
| DescribeCustomActivityTypeUsingGET - Describe Custom Activity Type | Activities Returns metadata for a specific custom activity type.  Required Permissions: Read-Only Activity Metadata, Read-Write Activity Metadata Describe Custom Activity Type  | /rest/v1/activities/external/type/{apiName}/describe.json\_\_\_get |
| DescribeCustomObjectTypeUsingGET - Describe Custom Object Type | Custom Objects Returns metadata regarding a given custom object type (including relationships and fields).  Required Permissions: Read-Only Custom Object Type, Read-Write Custom Object Type Describe Custom Object Type  | /rest/v1/customobjects/schema/{apiName}/describe.json\_\_\_get |
| DescribeOpportunityRoleUsingGET - Describe Opportunity Role | Opportunities Returns object and field metadata for Opportunity Roles in the target instance.  Required Permissions: Read-Only Opportunity, Read-Write Named Opportunity Describe Opportunity Role  | /rest/v1/opportunities/roles/describe.json\_\_\_get |
| DescribeProgramMemberUsingGET - Describe Program Member | Leads Returns metadata about program member objects in the target instance, including a list of all fields available for interaction via the APIs.  Required Permissions: Read-Only Lead, Read-Write Lead Describe Program Member  | /rest/v1/program/members/describe.json\_\_\_get |
| DescribeUsingGET - Describe Companies | Companies Returns metadata about companies and the fields available for interaction via the API.  Required Permissions: Read-Only Company, Read-Write Company Describe Companies  | /rest/v1/companies/describe.json\_\_\_get |
| DescribeUsingGET 1 - Describe Custom Objects | Custom Objects Returns metadata regarding a given custom object.  Required Permissions: Read-Only Custom Object, Read-Write Custom Object Describe Custom Objects  | /rest/v1/customobjects/{customObjectName}/describe.json\_\_\_get |
| DescribeUsingGET 2 - Describe Lead | Leads Returns metadata about lead objects in the target instance, including a list of all fields available for interaction via the APIs.  Required Permissions: Read-Only Lead, Read-Write Lead Describe Lead  | /rest/v1/leads/describe.json\_\_\_get |
| DescribeUsingGET 3 - Describe NamedAccounts | Named Accounts Returns metadata about namedaccounts and the fields available for interaction via the API.  Required Permissions: Read-Only Named Account, Read-Write Named Account Describe NamedAccounts  | /rest/v1/namedaccounts/describe.json\_\_\_get |
| DescribeUsingGET 4 - Describe Opportunity | Opportunities Returns object and field metadata for Opportunity type records in the target instance.  Required Permissions: Read-Only Opportunity, Read-Write Named Opportunity Describe Opportunity  | /rest/v1/opportunities/describe.json\_\_\_get |
| DescribeUsingGET 5 - Describe SalesPersons | Sales Persons Returns metadata about salespersons and the fields available for interaction via the API.  Required Permissions: Read-Only Sales Person, Read-Write Sales Person Describe SalesPersons  | /rest/v1/salespersons/describe.json\_\_\_get |
| DescribeUsingGET 6 - Describe Lead2 | Leads Returns list of searchable fields on lead objects in the target instance.  Required Permissions: Read-Only Lead, Read-Write Lead Describe Lead2  | /rest/v1/leads/describe2.json\_\_\_get |
| GetActivitiesPagingTokenUsingGET - Get Paging Token | Activities Returns a paging token for use in retrieving activities and data value changes.  Required Permissions: Read-Only Activity, Read-Write Activity Get Paging Token  | /rest/v1/activities/pagingtoken.json\_\_\_get |
| GetAllActivityTypesUsingGET - Get Activity Types | Activities Returns a list of available activity types in the target instance, along with associated metadata of each type.  Required Permissions: Read-Only Activity, Read-Write Activity Get Activity Types  | /rest/v1/activities/types.json\_\_\_get |
| GetCampaignByIdUsingGET - Get Campaign By Id | Campaigns Returns the record of a campaign by its id.  Required Permissions: Read-Only Campaigns, Read-Write Campaigns<br><br>Note: This endpoint has been superceded.  Use <a href="/rest-api/endpoint-reference/asset-endpoint-reference/#!/Smart_Campaigns/getSmartCampaignByIdUsingGET">Get Smart Campaign by Id</a> endpoint instead. Get Campaign By Id  | /rest/v1/campaigns/{campaignId}.json\_\_\_get |
| GetCampaignsUsingGET - Get Campaigns | Campaigns Returns a list of campaign records.  Required Permissions: Read-Only Campaigns, Read-Write Campaigns<br><br>Note: This endpoint has been superceded.  Use <a href="/rest-api/endpoint-reference/asset-endpoint-reference/#/Smart_Campaigns/getAllSmartCampaignsGET">Get Smart Campaigns</a> endpoint instead. Get Campaigns  | /rest/v1/campaigns.json\_\_\_get |
| GetCompaniesUsingGET - Get Companies | Companies Retrieves company records from the destination instance based on the submitted filter.  Required Permissions: Read-Only Company, Read-Write Company Get Companies  | /rest/v1/companies.json\_\_\_get |
| GetCustomActivityTypeUsingGET - Get Custom Activity Types | Activities Returns metadata regarding custom activities provisioned in the target instance.  Required Permissions: Read-Only Activity Metadata, Read-Write Activity Metadata Get Custom Activity Types  | /rest/v1/activities/external/types.json\_\_\_get |
| GetCustomObjectsUsingGET - Get Custom Objects | Custom Objects Retrieves a list of custom objects records based on filter and set of values.  There are two unique types of requests for this endpoint: one is executed normally using a GET with URL parameters, the other is by passing a JSON object in the body of a POST and specifying _method=GET in the querystring.  The latter is used when dedupeFields attribute has more than one field, which is known as a "compound key".  Required Permissions: Read-Only Custom Object, Read-Write Custom Object Get Custom Objects  | /rest/v1/customobjects/{customObjectName}.json\_\_\_get |
| GetCustomObjectTypeDependentAssetsUsingGET - Get Custom Object Dependent Assets | Custom Objects Returns a list of dependent assets for a custom object type, including their in-instance location.  Required Permissions: Read-Only Custom Object Type, Read-Write Custom Object Type Get Custom Object Dependent Assets  | /rest/v1/customobjects/schema/{apiName}/dependentAssets.json\_\_\_get |
| GetCustomObjectTypeFieldDataTypesUsingGET - Get Custom Object Type Field Data Types | Custom Objects Returns a list of permissible data types that are assigned to custom object fields.  Required Permissions: Read-Only Custom Object Type, Read-Write Custom Object Type Get Custom Object Type Field Data Types  | /rest/v1/customobjects/schema/fieldDataTypes.json\_\_\_get |
| GetCustomObjectTypeLinkableObjectsUsingGET - Get Custom Object Linkable Objects | Custom Objects Returns a list of linkable custom objects and their fields.  Required Permissions: Read-Only Custom Object Type, Read-Write Custom Object Type Get Custom Object Linkable Objects  | /rest/v1/customobjects/schema/linkableObjects.json\_\_\_get |
| GetDailyErrorsUsingGET - Get Daily Errors | Usage Retrieves a list of API users and a count of each error type they have encountered in the current day.  Required Permissions: None Get Daily Errors  | /rest/v1/stats/errors.json\_\_\_get |
| GetDailyUsageUsingGET - Get Daily Usage | Usage Returns a list of API users and the number of calls they have consumed for the day.  Required Permissions: None Get Daily Usage  | /rest/v1/stats/usage.json\_\_\_get |
| GetDeletedLeadsUsingGET - Get Deleted Leads | Activities Returns a list of leads deleted after a given datetime.  Deletions greater than 14 days old may be pruned.  Required Permissions: Read-Only Activity, Read-Write Activity Get Deleted Leads  | /rest/v1/activities/deletedleads.json\_\_\_get |
| GetExportActivitiesFileUsingGET - Get Export Activity File | Bulk Export Activities Returns the file content of an export job.  The export job must be in "Completed" state.  Use Get Export Activity Job Status endpoint to retrieve status of export job.  Required Permissions: Read-Only Activity<br><br>The file format is specified by calling the Create Export Activity Job endpoint. The following is an example of the default file format ("CSV"). Note that the "attributes" field is formatted as JSON.<br><br><code>marketoGUID,leadId,activityDate,activityTypeId,campaignId,primaryAttributeValueId,primaryAttributeValue, attributes</code><br><code>122323,6,2013-09-26T06:56:35+0000,12,11,6,Owyliphys Iledil,[{"name":"Source Type","value":"Web page visit"}]</code> Get Export Activity File  | /bulk/v1/activities/export/{exportId}/file.json\_\_\_get |
| GetExportActivitiesStatusUsingGET - Get Export Activity Job Status | Bulk Export Activities Returns status of an export job.  Job status is available for 30 days after Completed or Failed status was reached. Required Permissions: Read-Only Activity Get Export Activity Job Status  | /bulk/v1/activities/export/{exportId}/status.json\_\_\_get |
| GetExportActivitiesUsingGET - Get Export Activity Jobs | Bulk Export Activities Returns a list of export jobs that were created in the past 7 days.  Required Permissions: Read-Only Activity Get Export Activity Jobs  | /bulk/v1/activities/export.json\_\_\_get |
| GetExportLeadsFileUsingGET - Get Export Lead File | Bulk Export Leads Returns the file content of an export job.  The export job must be in "Completed" state.  Use Get Export Lead Job Status endpoint to retrieve status of export job.  Required Permissions: Read-Only Lead<br><br>The file format is specified by calling the Create Export Lead Job endpoint. The following is an example of the default file format ("CSV").<br><br><code>firstName,lastName,email</code><br><code>Marvin,Gaye,marvin.gaye@motown.com</code> Get Export Lead File  | /bulk/v1/leads/export/{exportId}/file.json\_\_\_get |
| GetExportLeadsStatusUsingGET - Get Export Lead Job Status | Bulk Export Leads Returns status of an export job.  Job status is available for 30 days after Completed or Failed status was reached. Required Permissions: Read-Only Lead Get Export Lead Job Status  | /bulk/v1/leads/export/{exportId}/status.json\_\_\_get |
| GetExportLeadsUsingGET - Get Export Lead Jobs | Bulk Export Leads Returns a list of export jobs that were created in the past 7 days.  Required Permissions: Read-Only Lead Get Export Lead Jobs  | /bulk/v1/leads/export.json\_\_\_get |
| GetExportProgramMembersFileUsingGET - Get Export Program Member File | Bulk Export Program Members Returns the file content of an export job.  The export job must be in "Completed" state.  Use Get Export Program Member Job Status endpoint to retrieve status of export job.  Required Permissions: Read-Only Lead<br><br>The file format is specified by calling the Create Export Program Member Job endpoint. The following is an example of the default file format ("CSV").<br><br><code>firstName,lastName,email</code><br><code>Marvin,Gaye,marvin.gaye@motown.com</code> Get Export Program Member File  | /bulk/v1/program/members/export/{exportId}/file.json\_\_\_get |
| GetExportProgramMembersStatusUsingGET - Get Export Program Member Job Status | Bulk Export Program Members Returns status of an export job.  Job status is available for 30 days after Completed or Failed status was reached. Required Permissions: Read-Only Lead Get Export Program Member Job Status  | /bulk/v1/program/members/export/{exportId}/status.json\_\_\_get |
| GetExportProgramMembersUsingGET - Get Export Program Member Jobs | Bulk Export Program Members Returns a list of export jobs that were created in the past 7 days.  Required Permissions: Read-Only Lead Get Export Program Member Jobs  | /bulk/v1/program/members/export.json\_\_\_get |
| GetImportCustomObjectFailuresUsingGET - Get Import Custom Object Failures | Bulk Import Custom Objects Returns the list of failures for the import batch job.  Required Permissions: Read-Write Custom Object Get Import Custom Object Failures  | /bulk/v1/customobjects/{apiName}/import/{batchId}/failures.json\_\_\_get |
| GetImportCustomObjectStatusUsingGET - Get Import Custom Object Status | Bulk Import Custom Objects Returns the status of an import batch job.  Required Permissions: Read-Write Custom Object Get Import Custom Object Status  | /bulk/v1/customobjects/{apiName}/import/{batchId}/status.json\_\_\_get |
| GetImportCustomObjectWarningsUsingGET - Get Import Custom Object Warnings | Bulk Import Custom Objects Returns the list of warnings for the import batch job.  Required Permissions: Read-Write Custom Object Get Import Custom Object Warnings  | /bulk/v1/customobjects/{apiName}/import/{batchId}/warnings.json\_\_\_get |
| GetImportLeadFailuresUsingGET - Get Import Lead Failures | Bulk Import Leads Returns the list of failures for the import batch job.  Required Permissions: Read-Write Lead Get Import Lead Failures  | /bulk/v1/leads/batch/{batchId}/failures.json\_\_\_get |
| GetImportLeadStatusUsingGET - Get Import Lead Status | Bulk Import Leads Returns the status of an import batch job.  Required Permissions: Read-Write Lead Get Import Lead Status  | /bulk/v1/leads/batch/{batchId}.json\_\_\_get |
| GetImportLeadWarningsUsingGET - Get Import Lead Warnings | Bulk Import Leads Returns the list of warnings for the import batch job.  Required Permissions: Read-Write Lead Get Import Lead Warnings  | /bulk/v1/leads/batch/{batchId}/warnings.json\_\_\_get |
| GetImportProgramMemberFailuresUsingGET - Get Import Program Member Failures | Bulk Import Program Members Returns the list of failures for the import batch job.  Required Permissions: Read-Write Lead Get Import Program Member Failures  | /bulk/v1/program/members/import/{batchId}/failures.json\_\_\_get |
| GetImportProgramMemberStatusUsingGET - Get Import Program Member Status | Bulk Import Program Members Returns the status of an import batch job.  Required Permissions: Read-Write Lead Get Import Program Member Status  | /bulk/v1/program/members/import/{batchId}/status.json\_\_\_get |
| GetImportProgramMemberWarningsUsingGET - Get Import Program Member Warnings | Bulk Import Program Members Returns the list of warnings for the import batch job.  Required Permissions: Read-Write Lead Get Import Program Member Warnings  | /bulk/v1/program/members/import/{batchId}/warnings.json\_\_\_get |
| GetLast7DaysErrorsUsingGET - Get Weekly Errors | Usage Returns a list of API users and a count of each error type they have encountered in the past 7 days.  Required Permissions: None Get Weekly Errors  | /rest/v1/stats/errors/last7days.json\_\_\_get |
| GetLast7DaysUsageUsingGET - Get Weekly Usage | Usage Returns a list of API users and the number of calls they have consumed in the past 7 days.  Required Permissions: None Get Weekly Usage  | /rest/v1/stats/usage/last7days.json\_\_\_get |
| GetLeadActivitiesUsingGET - Get Lead Activities | Activities Returns a list of activities from after a datetime given by the nextPageToken parameter.  Also allows for filtering by lead static list membership, or by a list of up to 30 lead ids.  Required Permissions: Read-Only Activity, Read-Write Activity Get Lead Activities  | /rest/v1/activities.json\_\_\_get |
| GetLeadByIdUsingGET - Get Lead by Id | Leads Retrieves a single lead record through its Marketo id.  Required Permissions: Read-Only Lead, Read-Write Lead Get Lead by Id  | /rest/v1/lead/{leadId}.json\_\_\_get |
| GetLeadChangesUsingGET - Get Lead Changes | Activities Returns a list of Data Value Changes and New Lead activities after a given datetime. Required Permissions: Read-Only Activity, Read-Write Activity Get Lead Changes  | /rest/v1/activities/leadchanges.json\_\_\_get |
| GetLeadPartitionsUsingGET - Get Lead Partitions | Leads Returns a list of available partitions in the target instance.  Required Permissions: Read-Only Lead, Read-Write Lead Get Lead Partitions  | /rest/v1/leads/partitions.json\_\_\_get |
| GetLeadsByFilterUsingGET - Get Leads by Filter Type | Leads Returns a list of up to 300 leads based on a list of values in a particular field.  Required Permissions: Read-Only Lead, Read-Write Lead Get Leads by Filter Type  | /rest/v1/leads.json\_\_\_get |
| GetLeadsByListIdUsingGET - Get Leads By List Id | Static Lists Retrieves person records which are members of the given static list.  Required Permissions: Read-Only Lead, Read-Write Lead Get Leads By List Id  | /rest/v1/list/{listId}/leads.json\_\_\_get |
| GetLeadsByListIdUsingGET 1 - Get Leads By List Id | Static Lists Retrieves person records which are members of the given static list.  Required Permissions: Read-Only Lead, Read-Write Lead Get Leads By List Id  | /rest/v1/lists/{listId}/leads.json\_\_\_get |
| GetLeadsByProgramIdUsingGET - Get Leads by Program Id | Leads Retrieves a list of leads which are members of the designated program.  Required Permissions: Read-Only Lead, Read-Write Lead Get Leads by Program Id  | /rest/v1/leads/programs/{programId}.json\_\_\_get |
| GetListByIdUsingGET - Get List by Id | Static Lists Returns a list record by its id.  Required Permissions: Read-Only Lead, Read-Write Lead Get List by Id  | /rest/v1/lists/{listId}.json\_\_\_get |
| GetListMembershipUsingGET - Get Lists by Lead Id | Leads Query static list membership for one lead.  Required Permissions: Read-Only Asset Get Lists by Lead Id  | /rest/v1/leads/{leadId}/listMembership.json\_\_\_get |
| GetListsUsingGET - Get Lists | Static Lists Returns a set of static list records based on given filter parameters. Required Permissions: Read-Only Lead, Read-Write Lead Get Lists  | /rest/v1/lists.json\_\_\_get |
| GetNamedAccountListMembersUsingGET - Get Named Account List Members | Named Account Lists Retrieves the named accounts which are members of the given list.  Required Permissions: Read-Only Named Account, Read-Write Named Account Get Named Account List Members  | /rest/v1/namedAccountList/{id}/namedAccounts.json\_\_\_get |
| GetNamedAccountListsUsingGET - Get Named Account Lists | Named Account Lists Retrieves a list of named account list records based on the filter type and values given.  Required Permissions: Read-Only Named Account List, Read-Write Named Account List Get Named Account Lists  | /rest/v1/namedAccountLists.json\_\_\_get |
| GetNamedAccountsUsingGET - Get NamedAccounts | Named Accounts Retrieves namedaccount records from the destination instance based on the submitted filter.  Required Permissions: Read-Only Named Account, Read-Write Named Account Get NamedAccounts  | /rest/v1/namedaccounts.json\_\_\_get |
| GetOpportunitiesUsingGET - Get Opportunities | Opportunities Returns a list of opportunities based on a filter and set of values.  Required Permissions: Read-Only Opportunity, Read-Write Named Opportunity Get Opportunities  | /rest/v1/opportunities.json\_\_\_get |
| GetOpportunityRolesUsingGET - Get Opportunity Roles | Opportunities Returns a list of opportunity roles based on a filter and set of values.  Required Permissions: Read-Only Opportunity, Read-Write Named Opportunity Get Opportunity Roles  | /rest/v1/opportunities/roles.json\_\_\_get |
| GetProgramMembershipUsingGET - Get Programs by Lead Id | Leads Query program membership for one lead.  Required Permissions: Read-Only Asset Get Programs by Lead Id  | /rest/v1/leads/{leadId}/programMembership.json\_\_\_get |
| GetSalesPersonUsingGET - Get SalesPersons | Sales Persons Retrieves salesperson records from the destination instance based on the submitted filter.  Required Permissions: Read-Only Sales Person, Read-Write Sales Person Get SalesPersons  | /rest/v1/salespersons.json\_\_\_get |
| GetSmartCampaignMembershipUsingGET - Get Smart Campaigns by Lead Id | Leads Query smart campaign membership for one lead.  Required Permissions: Read-Only Asset Get Smart Campaigns by Lead Id  | /rest/v1/leads/{leadId}/smartCampaignMembership.json\_\_\_get |
| ListCustomObjectsUsingGET - List Custom Objects | Custom Objects Returns a list of Custom Object types available in the target instance, along with id and deduplication information for each type.  Required Permissions: Read-Only Custom Object, Read-Write Custom Object List Custom Objects  | /rest/v1/customobjects.json\_\_\_get |
| ListCustomObjectTypesUsingGET - List Custom Object Types | Custom Objects Returns a list of Custom Object Types available in the target instance, along with id, deduplication, relationship, and field information for each type.  Required Permissions: Read-Only Custom Object Type, Read-Write Custom Object Type List Custom Object Types  | /rest/v1/customobjects/schema.json\_\_\_get |


