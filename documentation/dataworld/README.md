# This connector provides access to the REST API for the Data World Cloud-Native enterprise data catalog solution.
This connector provides access to the REST API for the Data World Cloud-Native enterprise data catalog solution.

# Connection Tab
## Connection Fields


#### Alternate Swagger URL

This will override the default openapi.json file so you can provide a url to any swagger file.

**Type** - string



#### URL

The URL for the Service service

**Type** - string



#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### Use Authentication Loading Swagger

Select this option if loading a swagger from a URL requires authentication.

**Type** - boolean

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| AddFilesBySource - Add files from URLs | files Add files from URLs to a dataset. This method allows files published on the web to be added to a data.world dataset via their URL. This method can also be used to retrieve data via web APIs, with advanced options for http method, request payload and authentication.  The source URL will be stored so you can easily update your file anytime it changes via the *fetch latest* link on the [data.world](https://data.world/) dataset page or by triggering the `GET:/sync` endpoint.   Add files from URLs  |
| CreateSavedQuery - Create a saved query in a specified dataset. | queries Create a saved query in a specified dataset. Create a saved query in a specified dataset.  |
| CreateSavedQuery - Create a saved query in a specified project. | queries Create a saved query in a specified project. Create a saved query in a specified project.  |
| ExecuteQueryWithPost - Execute a saved query (with parameters) | queries Execute a saved query (same as GET:/queries/{id}/results) with the option to specify named query parameters. Execute a saved query (with parameters)  |
| Sync - Sync files | files Sync files within a dataset. This method will process the latest data available for files added from URLs or via streams. Sync files  |
| UploadFiles - Upload files | files Upload multiple files at once to a dataset.  This endpoint expects requests of type `multipart/form-data` and you can include one or more parts named `file`, each containing a different file to be uploaded.  For example, assuming that you want to upload two local files named `file1.csv` and `file2.csv` to a hypothetical dataset `https://data.world/awesome-user/awesome-dataset`, this is what the cURL command would look like.  ```bash curl \   -H "Authorization: Bearer <YOUR_API_TOKEN>" \   -F "file=@file1.csv" \   -F "file=@file2.csv" \   https://api.data.world/v0/uploads/awesome-user/awesome-dataset/files ```  Swagger clients may limit this method of upload to one file at a time. Other HTTP clients capable of making multipart/form-data requests can be used to upload multiple files in a single request. Upload files  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| DeleteDataset - Delete a dataset | datasets Delete a dataset and associated data. This operation cannot be undone, but you may recreate the dataset using the same id. Delete a dataset  |
| DeleteDoi - Delete dataset DOI | DOIs Delete a DOI ([Digital Object Identifier](https://www.doi.org/)) associated with a dataset. Delete dataset DOI  |
| DeleteFileAndSyncSource - Delete a file | files Delete a single file from a dataset. Delete a file  |
| DeleteFilesAndSyncSources - Delete files | files Delete one or more files from a dataset.     **Batching**   Note that the `name` parameter can be include multiple times in the query string, once for each file that is to be deleted together in a single request. Delete files  |
| DeleteInsight - Delete an insight | insights Delete an insight. Delete an insight  |
| DeleteProject - Delete a data project | projects Delete a project and associated data. This operation cannot be undone, but you may recreate the project using the same id. Delete a data project  |
| DeleteRecords - Delete all records | streams Delete all records previously appended to stream. Delete all records  |
| DeleteSavedQuery - Delete a saved query in a specified dataset. | queries Delete a saved query in a specified dataset. Delete a saved query in a specified dataset.  |
| DeleteSavedQuery - Delete a saved query in a specified project. | queries Delete a saved query in a specified project. Delete a saved query in a specified project.  |
| DeleteVersionDoi - Delete dataset version DOI | DOIs Delete a DOI ([Digital Object Identifier](https://www.doi.org/)) associated with a version of a dataset. Delete dataset version DOI  |
| RemoveLinkedDataset - Unlink dataset | projects Remove a linked dataset from a project. Unlink dataset  |
| UnsubscribeFromDataset - Unsubscribe from dataset | webhooks Delete webhook subscription associated with the currently authenticated user and to a given dataset. Unsubscribe from dataset  |
| UnsubscribeFromProject - Unsubscribe from data project | webhooks Delete webhook subscription associated with the currently authenticated user and to a given data project. Unsubscribe from data project  |
| UnsubscribeFromUser - Unsubscribe from account | webhooks Delete webhook subscription associated with the currently authenticated user and to a given organization or user account. Unsubscribe from account  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| DownloadDataset - Download a dataset | datasets Download a .zip file containing all files within a dataset as originally uploaded. Prefer `POST:/sql` or `POST:/sparql` for retrieving clean and structured data. Download a dataset  |
| DownloadFile - Download a file | files Download a file within the dataset as originally uploaded. Prefer `POST:/sql` or `POST:/sparql` for retrieving clean and structured data. Download a file  |
| GetAccount - Retrieve a user's profile | users Retrieve user profile information for the specified account. Retrieve a user's profile  |
| GetDataset - Retrieve a dataset | datasets Retrieve a dataset. The definition of the dataset will be returned, not its data. Use `GET:/download/{owner}/{id}` or `GET:/file_download/{owner}/{id}/{file}` to retrieve the original files content, or `POST:/sql/{owner}/{id}` or `POST:/sparql/{owner}/{id}` to query the data. Retrieve a dataset  |
| GetDatasetByVersion - Retrieve a dataset version | datasets Retrieve a version of a dataset. The definition of the dataset will be returned, not its data. Retrieve a dataset version  |
| GetForDataset - Retrieve dataset subscription | webhooks Retrieve webhook subscription associated with the currently authenticated user and to a given dataset. Retrieve dataset subscription  |
| GetForProject - Retrieve data project subscription | webhooks Retrieve webhook subscription associated with the currently authenticated user and to a given data project. Retrieve data project subscription  |
| GetForUser - Retrieve account subscription | webhooks Retrieve webhook subscription associated with the currently authenticated user and to a given organization or user account. Retrieve account subscription  |
| GetInsight - Retrieve an insight | insights Retrieve an insight. Retrieve an insight  |
| GetInsightByVersion - Retrieve an insight version | insights Retrieve an insight version. Retrieve an insight version  |
| GetInsightsForProject - List insights | insights List insights associated with a project. List insights  |
| GetProject - Retrieve a data project | projects Retrieve a project. The definition of the project will be returned, not the associated data. Use `POST:/sql/{owner}/{id}` or `POST:/sparql/{owner}/{id}` to query the data or use dataset APIs to retrieve data from linked datasets. Retrieve a data project  |
| GetProjectByVersion - Retrieve a data project version | projects Retrieve a project version. The definition of the project will be returned. Retrieve a data project version  |
| GetQuery - Retrieve a saved query | queries Retrieve a saved query. Query definitions will be returned, not query results. To retrieve query results use `GET:/queries/{id}/results`. Retrieve a saved query  |
| GetQueryVersion - Retrieve a saved query version | queries Retrieve a version of a saved query. Query definitions will be returned, not query results. Retrieve a saved query version  |
| SparqlGet - SPARQL query (via GET) | queries Same as `POST:/sparql`. SPARQL query (via GET)  |
| SqlGet - SQL query (via GET) | queries Same as `POST:/sql/{owner}/{id}` SQL query (via GET)  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| ExecuteQuery - Execute a saved query | queries Execute a saved query.  SQL results are available in a variety of formats. By default, `application/json` will be returned. Set the `Accept` header to one of the following values in accordance with your preference:  * `text/csv` * `application/json` * `application/json-l` * `application/x-ndjson`  SPARQL results are available in a variety of formats. By default, `application/sparql-results+json` will be returned. Set the `Accept` header to one of the following values in accordance with your preference:  - `application/sparql-results+xml` - `application/sparql-results+json` - `application/rdf+json` - `application/rdf+xml` - `text/csv` - `text/tab-separated-values` - `text/turtle` Execute a saved query  |
| FetchContributingDatasets - List datasets as contributor | user List datasets that the currently authenticated user has access to because he or she is a contributor. List datasets as contributor  |
| FetchContributingProjects - List projects as contributor | user List projects that the currently authenticated user has access to because he or she is a contributor. List projects as contributor  |
| FetchDatasets - List datasets as owner | user List datasets that the currently authenticated user has access to because he or she is the owner. List datasets as owner  |
| FetchLikedDatasets - List liked (bookmarked) datasets | user List datasets that the currently authenticated user liked (bookmarked). List liked (bookmarked) datasets  |
| FetchLikedProjects - List liked (bookmarked) projects | user List projects that the currently authenticated user liked (bookmarked). List liked (bookmarked) projects  |
| FetchProjects - List projects owned | user List projects that the currently authenticated user has access to because he or she is the owner. List projects owned  |
| GetDatasetQueries - List saved queries | queries List saved queries associated with a dataset. Query definitions will be returned, not the query results. To retrieve query results use `GET:/queries/{id}/results`. List saved queries  |
| GetProjectQueries - List saved queries | queries List saved queries associated with a project. List saved queries  |
| GetStreamSchema - Retrieve stream schema | streams Retrieve a stream's schema. Retrieve stream schema  |
| GetUserData - Retrieve user data | user Retrieve user profile information of the currently authenticated user. Retrieve user data  |
| GetWebhooks - List subscriptions | webhooks List webhook subscriptions associated with the currently authenticated user. List subscriptions  |
| SyncViaGet - Sync files (via GET) | files Same as `POST:/dataset/{owner}/{id}/sync`. Sync files (via GET)  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| AddDoi - Create dataset DOI (PUT) | DOIs Associate a DOI ([Digital Object Identifier](https://www.doi.org/)) with a dataset. Create dataset DOI  |
| AddLinkedDataset - Link dataset (PUT) | projects Add a linked dataset to a project. Link dataset  |
| AddVersionDoi - Create dataset version DOI (PUT) | DOIs Associate a DOI ([Digital Object Identifier](https://www.doi.org/)) with a version of a dataset. Create dataset version DOI  |
| AppendRecords - Append record(s) (POST) | streams Append JSON data to a stream associated with a dataset.   data.world streams are append-only by default. Alternatively, if a primary key is specified (see: `POST:/streams/{owner}/{id}/{streamId}/schema`), data.world will replace records with the same primary key value.  **Streams don't need to be created before you can append data to them**. They will be created on-demand, when the first record is appended or by defining its schema.  Multiple records can be appended at once by using JSON-L (`application/json-l`) as the request content type.  **IMPORTANT**  Data uploaded to a dataset via a stream is not immediatelly processed. Instead, it is processed automatically in accordance with the dataset settings (default: daily) or as a result of calling `POST:/datasets/{owner}/{id}/sync`.  Once processed, the contents of a stream will appear as part of the respective dataset as a `.jsonl` file (e.g. `my-stream` will produce a file named `my-stream.jsonl`). Append record(s)  |
| CreateDataset - Create a dataset (POST) | datasets Create a new dataset. Create a dataset  |
| CreateInsight - Create an insight (POST) | insights Create a new insight. Create an insight  |
| CreateProject - Create a data project (POST) | projects Create a new project. Create a data project  |
| PatchDataset - Update a dataset (PATCH) | datasets Update an existing dataset. Only elements or files included in the request will be updated. All omitted elements or files will remain untouched. Update a dataset  |
| PatchProject - Update a data project (PATCH) | projects Update an existing project. Only elements included in the request will be updated. All omitted elements will remain untouched. Update a data project  |
| PatchStreamSchema - Set / Update stream schema (PATCH) | streams Set or update a stream's schema.  The schema of a stream defines its primary key(s) and sort/sequence field.   data.world streams are append-only by default. Alternatively, if a primary key is specified, data.world will replace records with the same primary key value. data.world will sort records by sequence field value and will discard all but the last record appended for each given primary key value.  The `updateMethod` parameter specifies how data.world should handle existing records when schema is updated. Currently, the only `updateMethod` supported is `TRUNCATED`. data.world will discard all records when the schema is updated. Set / Update stream schema  |
| ReplaceDataset - Create / Replace a dataset (PUT) | datasets Create or replace a dataset with a given id. If a dataset exists with the same id, this call will reset such dataset and all the data contained in it. Create / Replace a dataset  |
| ReplaceInsight - Replace an insight (PUT) | insights Replace an insight. Replace an insight  |
| ReplaceProject - Create / Replace a data project (PUT) | projects Create or replace a project with a given id. If a project exists with the same id, this call will reset such project redefining all its attributes. Create / Replace a data project  |
| SparqlPost - SPARQL query (POST) | queries Execute a SPARQL query against a dataset or data project.  SPARQL results are available in a variety of formats. By default, `application/sparql-results+json` will be returned. Set the `Accept` header to one of the following values in accordance with your preference:  - `application/sparql-results+xml` - `application/sparql-results+json` - `application/rdf+json` - `application/rdf+xml` - `text/csv` - `text/tab-separated-values` - `text/turtle`  New to SPARQL? Check out data.world's [SPARQL tutorial](https://docs.data.world/tutorials/sparql/). SPARQL query  |
| SqlPost - SQL query (POST) | queries Execute a SQL query against a dataset or data project.  SQL results are available in a variety of formats. By default, `application/json` will be returned. Set the `Accept` header to one of the following values in accordance with your preference:  * `text/csv` * `application/json` * `application/json-l` * `application/x-ndjson`  New to SQL? Check out data.world's [SQL manual](https://docs.data.world/tutorials/dwsql/) . SQL query  |
| SubscribeToDataset - Subscribe to dataset (PUT) | webhooks Create webhook subscription associated with the currently authenticated user and to a given dataset. Subscribe to dataset  |
| SubscribeToProject - Subscribe to data project (PUT) | webhooks Create webhook subscription associated with the currently authenticated user and to a given data project. Subscribe to data project  |
| SubscribeToUser - Subscribe to account (PUT) | webhooks Create webhook subscription associated with the currently authenticated user and to a given organization or user account. Subscribe to account  |
| UpdateInsight - Update an insight (PATCH) | insights Update an insight. Note that only elements included in the request will be updated. All omitted elements will remain untouched. Update an insight  |
| UpdateSavedQuery - Update a saved query in a specified dataset. (PUT) | queries Update a saved query in a specified dataset. Update a saved query in a specified dataset.  |
| UpdateSavedQuery - Update a saved query in a specified project. (PUT) | queries Update a saved query in a specified project. Update a saved query in a specified project.  |
| UploadFile - Upload a file (PUT) | files Upload one file at a time to a dataset.  This endpoint expects requests of type `application/octet-stream`.  For example, assuming that you want to upload a local file named `file1.csv` to a hypothetical dataset `https://data.world/awesome-user/awesome-dataset` and choose its name on data.world to be `better-name.csv`, this is what the cURL command would look like.  ```bash curl \   -H "Authorization: Bearer <YOUR_API_TOKEN>" \   -X PUT -H "Content-Type: application/octet-stream" \   --data-binary @file1.csv \   https://api.data.world/v0/uploads/awesome-user/awesome-dataset/files/better-name.csv ```  This method of upload is typically not supported by Swagger clients. Other HTTP clients can be used to supply the contents of the file directly in the body of the request. Upload a file  |


