# Visa Cybersource Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### Amdocs Swagger URL

The URL for the Amdocs Swagger descriptor file

**Type** - string

##### Allowed Values

 * Customer Experience Management
 * Pod Management
 * Shopping Cart Management


#### URL

The URL for the Amdocs service

**Type** - string

# Operation Tab


## CREATE


## UPDATE


## GET


## DELETE


## QUERY

### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

The query filter supports no groupings (only one expression allowed).

Example:
(foo lessThan 30)

#### Filter Operators

 * Equal To  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts

The query operation does not support sorting.


# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.
 * Additional URI Query Parameters
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| CreateCustomer - This operation creates a new customer resource.  | Customer This operation creates a new customer resource.   | /customer\_\_\_post |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| GetCustomer - This operation retrieves the customer along with t... | Customer This operation retrieves the customer along with the following information: - Basic customer information, for example, name, type, and subtype - Financial accounts - Contacts - Contact method, for example, postal address, phone, and email   | /customer/{customerId}\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| GetCustomers - This operation retrieves a list of customers throu... | Customer This operation retrieves a list of customers through applied filters. One of the following filters must be applied: - *id* - *owningIndividual.firstName* - *owningIndividual.lastName*   | /customer\_\_\_get |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| PatchResource - Update the customer resource by ID (PATCH) | Customer This operation partially updates the Customer resource identified by the customer ID. If provided in the request, it owns an Individual entity. Currently, the following entities can be updated: - *name* - *marketingPreference* - *billingAddress [ContactMedium]* - *owningIndivual [RelatedIndiviualRefOrValue]* - *owningIndividual.contactMedium [ContactMedium]* - *owningIndividual.identification [Identification]*  Update the customer resource by ID  | /customer/{customerId}\_\_\_patch |


