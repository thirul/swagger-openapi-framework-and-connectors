# Openbank Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### Openbank Swagger URL

The URL for the Openbank Swagger descriptor file

**Type** - string

**Default Value** - resources/openbank/openbank-account-info-swagger.json



#### URL

The URL for the Openbank service

**Type** - string

**Default Value** - https://apisandbox.openbankproject.com



#### OAuth 2.0

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - oauth

# Operation Tab


## CREATE


## DELETE


## GET


## QUERY
### Operation Fields


##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Offset

The number of rows to skip before beginning to return rows. An offset of 0 is the same as omitting the offset parameter.

**Type** - integer

**Default Value** - 0


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

The query filter supports any number of non-nested expressions which will be AND'ed together.

Example:
((foo lessThan 30) AND (baz lessThan 42))

#### Filter Operators

 * Equal To  (Supported Types:  string, boolean, date)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.
 * Alternate URL for expanding results, populated with a Link value from a prior query operation
 * Additional URI Query Parameters
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| CreateAccountAccessConsents - Create Account Access Consents | Create Account Access Consents | /account-access-consents\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| DeleteAccountAccessConsentsConsentId - Delete Account Access Consents | Delete Account Access Consents | /account-access-consents/{ConsentId}\_\_\_delete |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| GetAccountAccessConsentsConsentId - Get Account Access Consents | Get Account Access Consents | /account-access-consents/{ConsentId}\_\_\_get |
| GetAccountsAccountId - Get Accounts | Get Accounts | /accounts/{AccountId}\_\_\_get |
| GetAccountsAccountIdStatementsStatementId - Get Statements | Get Statements | /accounts/{AccountId}/statements/{StatementId}\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| GetAccounts - Get Accounts | Get Accounts | /accounts\_\_\_get |
| GetAccountsAccountIdBalances - Get Balances | Get Balances | /accounts/{AccountId}/balances\_\_\_get |
| GetAccountsAccountIdBeneficiaries - Get Beneficiaries | Get Beneficiaries | /accounts/{AccountId}/beneficiaries\_\_\_get |
| GetAccountsAccountIdDirectDebits - Get Direct Debits | Get Direct Debits | /accounts/{AccountId}/direct-debits\_\_\_get |
| GetAccountsAccountIdOffers - Get Offers | Get Offers | /accounts/{AccountId}/offers\_\_\_get |
| GetAccountsAccountIdParty - Get Party | Get Party | /accounts/{AccountId}/party\_\_\_get |
| GetAccountsAccountIdProduct - Get Products | Get Products | /accounts/{AccountId}/product\_\_\_get |
| GetAccountsAccountIdScheduledPayments - Get Scheduled Payments | Get Scheduled Payments | /accounts/{AccountId}/scheduled-payments\_\_\_get |
| GetAccountsAccountIdStandingOrders - Get Standing Orders | Get Standing Orders | /accounts/{AccountId}/standing-orders\_\_\_get |
| GetAccountsAccountIdStatements - Get Statements | Get Statements | /accounts/{AccountId}/statements\_\_\_get |
| GetAccountsAccountIdStatementsStatementIdFile - Get Statements | Get Statements | /accounts/{AccountId}/statements/{StatementId}/file\_\_\_get |
| GetAccountsAccountIdStatementsStatementIdTransactions - Get Transactions | Get Transactions | /accounts/{AccountId}/statements/{StatementId}/transactions\_\_\_get |
| GetAccountsAccountIdTransactions - Get Transactions | Get Transactions | /accounts/{AccountId}/transactions\_\_\_get |
| GetBalances - Get Balances | Get Balances | /balances\_\_\_get |
| GetBeneficiaries - Get Beneficiaries | Get Beneficiaries | /beneficiaries\_\_\_get |
| GetDirectDebits - Get Direct Debits | Get Direct Debits | /direct-debits\_\_\_get |
| GetOffers - Get Offers | Get Offers | /offers\_\_\_get |
| GetParty - Get Party | Get Party | /party\_\_\_get |
| GetProducts - Get Products | Get Products | /products\_\_\_get |
| GetScheduledPayments - Get Scheduled Payments | Get Scheduled Payments | /scheduled-payments\_\_\_get |
| GetStandingOrders - Get Standing Orders | Get Standing Orders | /standing-orders\_\_\_get |
| GetStatements - Get Statements | Get Statements | /statements\_\_\_get |
| GetTransactions - Get Transactions | Get Transactions | /transactions\_\_\_get |


