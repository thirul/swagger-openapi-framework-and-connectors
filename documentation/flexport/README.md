# Flexport REST API Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### Flexport Swagger URL

The URL for the Flexport Swagger descriptor file

**Type** - string

**Default Value** - resources/flexport/flexportswagger3.json



#### URL

The URL for the Flexport service

**Type** - string



#### OAuth 2.0

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - oauth

# Operation Tab


## CREATE


## UPDATE


## GET


## QUERY
### Operation Fields


##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Offset

The number of rows to skip before beginning to return rows. An offset of 0 is the same as omitting the offset parameter.

**Type** - integer

**Default Value** - 0


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

The query filter supports any number of non-nested expressions which will be AND'ed together.

Example:
((foo lessThan 30) AND (baz lessThan 42))

#### Filter Operators

 * Equal To  (Supported Types:  string, boolean, date)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.
 * Alternate URL for expanding results, populated with a Link value from a prior query operation
 * Additional URI Query Parameters
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Booking Amendment Create - Create and return a booking amendment - a request ... | Create and return a booking amendment - a request ... | /booking\_amendments\_\_\_post |
| Booking Create - Create and return a booking | Create and return a booking | /bookings\_\_\_post |
| Booking Line Item Create - Create a booking line item | Create a booking line item | /booking\_line\_items\_\_\_post |
| Carbon Calculation Create - Return a new carbon calculation. Rate limited to 1... | Return a new carbon calculation. Rate limited to 1... | /carbon\_calculation\_\_\_post |
| Commercial Invoices Create - Create and return a new commercial invoice. Specia... | Create and return a new commercial invoice. Specia... | /commercial\_invoices\_\_\_post |
| Network Company Create - Create a new company object | Create a new company object | /network/companies\_\_\_post |
| Network Company Entity Create - Create and return a new company entity | Create and return a new company entity | /network/company\_entities\_\_\_post |
| Network Contact Create - Create a new contact object | Create a new contact object | /network/contacts\_\_\_post |
| Network Location Create - Create and return a new location | Create and return a new location | /network/locations\_\_\_post |
| Product Create - Create a product | Create a product | /products\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Booking Line Item Show - Retrieves the details of a single booking line ite... | Retrieves the details of a single booking line ite... | /booking\_line\_items/{id}\_\_\_get |
| Booking Show - Retrieve a booking | Retrieve a booking | /bookings/{id}\_\_\_get |
| Commercial Invoices Show - Retrieve a commercial invoice | Retrieve a commercial invoice | /commercial\_invoices/{id}\_\_\_get |
| Container Show - Retrieve a container. | Retrieve a container. | /ocean/shipment\_containers/{id}\_\_\_get |
| Customs Entries Show - Retrieve a customs entry | Retrieve a customs entry | /customs\_entries/{id}\_\_\_get |
| Documents Show - Retrieve a document | Retrieve a document | /documents/{id}\_\_\_get |
| Events Show - Retrieve a single webhook event | Retrieve a single webhook event | /events/{id}\_\_\_get |
| Invoices Show - Retrieve an invoice | Retrieve an invoice | /invoices/{id}\_\_\_get |
| Network Company Entity Show - Retrieve a company entity | Retrieve a company entity | /network/company\_entities/{id}\_\_\_get |
| Network Company Show - Retrieve a company | Retrieve a company | /network/companies/{id}\_\_\_get |
| Network Contact Show - Retrieve a contact | Retrieve a contact | /network/contacts/{id}\_\_\_get |
| Network Location Show - Retrieve a location by id | Retrieve a location by id | /network/locations/{id}\_\_\_get |
| Ocean Container Legs Show - Retrieve a container leg on an Ocean shipment | Retrieve a container leg on an Ocean shipment | /ocean/shipment\_container\_legs/{id}\_\_\_get |
| Product Show - Retrieve a single product | Retrieve a single product | /products/{id}\_\_\_get |
| Purchase Order Line Item Show - Retrieve a purchase order line item | Retrieve a purchase order line item | /purchase\_order\_line\_items/{id}\_\_\_get |
| Purchase Order Show - Retrieve a purchase order | Retrieve a purchase order | /purchase\_orders/{id}\_\_\_get |
| Shipment Leg Show - Retrieve a shipment route leg | Retrieve a shipment route leg | /shipment\_legs/{id}\_\_\_get |
| Shipment Show - Retrieve a shipment | Retrieve a shipment | /shipments/{id}\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Booking Line Item Index - List all booking line items | List all booking line items | /booking\_line\_items\_\_\_get |
| Bookings Index - List all bookings | List all bookings | /bookings\_\_\_get |
| Commercial Invoices Index - List commercial invoices | List commercial invoices | /commercial\_invoices\_\_\_get |
| Company Entity Index - List of company entity objects | List of company entity objects | /network/company\_entities\_\_\_get |
| Container List - List all containers. | List all containers. | /ocean/shipment\_containers\_\_\_get |
| Customs Entry Index - List customs entry objects | List customs entry objects | /customs\_entries\_\_\_get |
| Documents Download - Download a document | Download a document | /documents/{id}/download\_\_\_get |
| Documents Index - List document objects | List document objects | /documents\_\_\_get |
| Events Index - List all webhook events | List all webhook events | /events\_\_\_get |
| Invoice Index - List all invoices | List all invoices | /invoices\_\_\_get |
| Location Index - List of location objects | List of location objects | /network/locations\_\_\_get |
| Network Company Index - List company objects | List company objects | /network/companies\_\_\_get |
| Network Company Me - Retrieve your company | Retrieve your company | /network/me/companies\_\_\_get |
| Network Contact Index - List contact objects | List contact objects | /network/contacts\_\_\_get |
| Ocean Container Legs Index - List all container legs | List all container legs | /ocean/shipment\_container\_legs\_\_\_get |
| Ports Index - List of ports | List of ports | /ports\_\_\_get |
| Product Index - List all products for a client | List all products for a client | /products\_\_\_get |
| Purchase Order Index - List all purchase orders | List all purchase orders | /purchase\_orders\_\_\_get |
| Purchase Order Line Item Index - List all purchase order line items | List all purchase order line items | /purchase\_order\_line\_items\_\_\_get |
| Shipment Index - List all shipments | List all shipments | /shipments\_\_\_get |
| Shipment Leg Index - List all shipment route legs | List all shipment route legs | /shipment\_legs\_\_\_get |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Commercial Invoices Update - Update an existing commercial invoice. Special per... (PATCH) | Update an existing commercial invoice. Special per... | /commercial\_invoices\_\_\_patch |
| Location Update - Update a network location (PATCH) | Update a network location | /network/locations/{id}\_\_\_patch |
| Network Company Entity Update - Update a company entity (PATCH) | Update a company entity | /network/company\_entities/{id}\_\_\_patch |
| Network Company Update - Update an existing company (PATCH) | Update an existing company | /network/companies/{id}\_\_\_patch |
| Network Contact Update - Update an existing contact (PATCH) | Update an existing contact | /network/contacts/{id}\_\_\_patch |
| Product Update - Update a product (PATCH) | Update a product | /products/{id}\_\_\_patch |
| Shipment Update - Update an existing shipment (PATCH) | Update an existing shipment | /shipments/{id}\_\_\_patch |


