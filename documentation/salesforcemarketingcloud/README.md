# Salesforce Marketing Cloud REST API
Marketing Cloud's REST API is our newest API. It supports multi-channel use cases, is much more lightweight and easy to use than our SOAP API, and is getting more comprehensive with every release.

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### OAuth 2.0 Client Credentials

OAuth 2.0 Client Credentials authentication is available for select end points with specific scope values configured. For more information refer to: https://developer.salesforce.com/docs/atlas.en-us.mc-app-development.meta/mc-app-development/access-token-s2s.htm

**Type** - oauth

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

The query filter supports no groupings (only one expression allowed).

Example:
(foo lessThan 30)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts

The query operation does not support sorting.

Only one direction of sorting is supported.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| CreateAsset - createAsset | asset Creates a new asset. createAsset  |
| CreateCampaign - createCampaign | campaign Creates a campaign. createCampaign  |
| CreateEmailDefinition - createEmailDefinition | transactionalMessaging Creates the definition for an email. createEmailDefinition  |
| CreateSmsDefinition - createSmsDefinition | transactionalMessaging Creates the definition for an SMS. createSmsDefinition  |
| SendEmailToMultipleRecipients - sendEmailToMultipleRecipients | transactionalMessaging Sends a message to multiple recipients using an email definition. You can provide a messageKey in the request; otherwise, the messageKey is automatically generated in the response. sendEmailToMultipleRecipients  |
| SendSmsToMultipleRecipients - sendSmsToMultipleRecipients | transactionalMessaging Sends a message to multiple recipients using an email definition. You can provide a messageKey in the request; otherwise, the messageKey is automatically generated in the response. sendSmsToMultipleRecipients  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| DeleteAssetById - deleteAssetById | asset Deletes an asset. deleteAssetById  |
| DeleteCampaignById - deleteCampaignById | campaign Deletes a campaign. deleteCampaignById  |
| DeleteEmailDefinition - deleteEmailDefinition | transactionalMessaging Deletes an email definition. You can't restore a deleted definition. The deleted definition is archived, and a delete location of the definition key is provided in the response for reference. You can reuse a deleted definition key because the information associated with it is copied to a new unique identifier. deleteEmailDefinition  |
| DeleteQueuedMessagesForEmailDefinition - deleteQueuedMessagesForEmailDefinition | transactionalMessaging Deletes the queue for an email definition. The email definition must be in inactive status. deleteQueuedMessagesForEmailDefinition  |
| DeleteQueuedMessagesForSmsDefinition - deleteQueuedMessagesForSmsDefinition | transactionalMessaging Deletes the queue for a SMS definition. The SMS definition must be in inactive status. deleteQueuedMessagesForSmsDefinition  |
| DeleteSmsDefinition - deleteSmsDefinition | transactionalMessaging Deletes an sms definition. You can't restore a deleted definition. The deleted definition is archived, and a delete location of the definition key is provided in the response for reference. You can reuse a deleted definition key because the information associated with it is copied to a new unique identifier. deleteSmsDefinition  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| GetAssetById - getAssetById | asset Gets an asset by ID. getAssetById  |
| GetCampaignById - getCampaignById | campaign Retrieves a campaign. getCampaignById  |
| GetEmailDefinition - getEmailDefinition | transactionalMessaging Gets email definition configuration details for a definition key. getEmailDefinition  |
| GetEmailSendStatusForRecipient - getEmailSendStatusForRecipient | transactionalMessaging Gets the send status for a message. Because this route is rate-limited, use it for infrequent verification of a messageKey. To collect send status at scale, subscribe to transactional send events using the Event Notification Service. getEmailSendStatusForRecipient  |
| GetQueueMetricsForEmailDefinition - getQueueMetricsForEmailDefinition | transactionalMessaging Gets metrics for the messages of an email definition. Applies to messages that are accepted but not yet processed. getQueueMetricsForEmailDefinition  |
| GetQueueMetricsForSmsDefinition - getQueueMetricsForSmsDefinition | transactionalMessaging Gets metrics for the messages of a SMS definition. Applies to messages that are accepted but not yet processed. getQueueMetricsForSmsDefinition  |
| GetSmsDefinition - getSmsDefinition | transactionalMessaging Gets SMS definition configuration details for a definition key. getSmsDefinition  |
| GetSmsSendStatusForRecipient - getSmsSendStatusForRecipient | transactionalMessaging Gets the send status for a message. Because this route is rate-limited, use it for infrequent verification of a messageKey. To collect send status at scale, subscribe to transactional send events using the Event Notification Service. getSmsSendStatusForRecipient  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| GetEmailDefinitions - getEmailDefinitions | transactionalMessaging Gets a list of email definitions. getEmailDefinitions  |
| GetEmailsNotSentToRecipients - getEmailsNotSentToRecipients | transactionalMessaging Gets a paginated list of messages that were not sent, ordered from oldest to newest. getEmailsNotSentToRecipients  |
| GetSmsDefinitions - getSmsDefinitions | transactionalMessaging Gets a list of SMS definitions. getSmsDefinitions  |
| GetSMSsNotSentToRecipients - getSMSsNotSentToRecipients | transactionalMessaging Gets a paginated list of messages that were not sent, ordered from oldest to newest. getSMSsNotSentToRecipients  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| PartiallyUpdateAssetById - partiallyUpdateAssetById (PATCH) | asset Updates part of an asset. partiallyUpdateAssetById  |
| PartiallyUpdateEmailDefinition - partiallyUpdateEmailDefinition (PATCH) | transactionalMessaging Updates a specific email definition. partiallyUpdateEmailDefinition  |
| PartiallyUpdateSmsDefinition - partiallyUpdateSmsDefinition (PATCH) | transactionalMessaging Updates a specific SMS definition. partiallyUpdateSmsDefinition  |
| SendEmailToSingleRecipient - sendEmailToSingleRecipient (POST) | transactionalMessaging Sends a message to a single recipient via an email definition using a messageKey path parameter. sendEmailToSingleRecipient  |
| SendSmsToSingleRecipient - sendSmsToSingleRecipient (POST) | transactionalMessaging Sends a message to a single recipient via a SMS definition using a messageKey path parameter. sendSmsToSingleRecipient  |


