# Brizio REST API Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### OpenAPI Swagger URL

The URL for the OpenAPI descriptor file

**Type** - string

**Default Value** - resources/brizio/swagger.json



#### URL

The URL for the OpenAPI service

**Type** - string

**Default Value** - https://sandbox.safaricom.co.ke/mpesa



#### AuthenticationType

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string

**Default Value** - NONE

##### Allowed Values

 * None
 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - oauth

# Operation Tab


## CREATE
# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| services - Initialize the service agreement between the publi... | services  Initialize the service agreement between the publisher and the consumer.  | /api/v1/brizo/services/access/initialize\_\_\_post |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |


