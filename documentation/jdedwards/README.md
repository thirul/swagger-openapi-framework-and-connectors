# JDE Edwards REST API Connector
This connector provides an easy to use point and click interface to the JDE Edwards v9.2 REST API. You can access any version of the API by specifying a URL to the swagger service file.

# Connection Tab
## Connection Fields


#### URL

The URL for the JD Edwards API Service

**Type** - string



#### (optional) REST API Swagger URL

This will override selections so you can provide a url to any swagger file to override the default 2020.10.13 9.2 version. For more information, please refer to https://docs.oracle.com/en/applications/jd-edwards/cross-product/9.2/rest-api/

**Type** - string



#### AuthenticationType

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - oauth

# Operation Tab


## CREATE/EXECUTE


## UPDATE


## GET


## DELETE


## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| AddMediaObjectText - Add Text Attachment v2 | Media Object Service Add a text attachment for the given media object key. Add Text Attachment v2  | /v2/file/addtext\_\_\_post |
| AddMediaObjectUrl - Add URL Attachment v1 | Media Object Service Add a URL type media object for the given structure and key. Add URL Attachment v1  | /file/addurl\_\_\_post |
| AddMediaObjectUrl 1 - Add URL Attachment v2 | Media Object Service Add a URL type media object for the given structure and key. Add URL Attachment v2  | /v2/file/addurl\_\_\_post |
| AnalyzeUnitTestState - Analyze Configuration Test Data | Scheduler Service After requesting the "Start Configuration Test for a Cluster" service with /startClusterUnitTest endpoint, use this service to analyze the configuration. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. Analyze Configuration Test Data  | /v2/scheduler/analyzeUnitTestState\_\_\_post |
| ApplicationStack - Execute Forms Keeping State v1 | Application Stack Service Execute stateful calls to applications including flows from one form to another. Execute Forms Keeping State v1  | /appstack\_\_\_post |
| ApplicationStack 1 - Execute Forms Keeping State v2 | Application Stack Service Execute stateful calls to applications including flows from one form to another. Execute Forms Keeping State v2  | /v2/appstack\_\_\_post |
| BatchFormService - Execute Multiple Forms v1 | Batch Form Service Execute multiple EnterpriseOne forms, pass data, and perform actions on each of the forms. Execute Multiple Forms v1  | /batchformservice\_\_\_post |
| BatchFormService 1 - Execute Multiple Forms v2 | Batch Form Service Execute multiple EnterpriseOne forms, pass data, and perform actions on each of the forms. Execute Multiple Forms v2  | /v2/batchformservice\_\_\_post |
| DeleteMediaObject - Delete an Attachment v1 | Media Object Service Delete the media object for the given structure, key, and sequence. Delete an Attachment v1  | /file/delete\_\_\_post |
| DeleteMediaObject 1 - Delete an Attachment v2 | Media Object Service Delete the media object for the given structure, key, and sequence. Delete an Attachment v2  | /v2/file/delete\_\_\_post |
| GetMediaObject - Download File Attachment v1 | Media Object Service Execute stateful calls to applications including flows from one form to another. Download File Attachment v1  | /file/download\_\_\_post |
| GetMediaObject 1 - Download File Attachment v2 | Media Object Service Execute stateful calls to applications including flows from one form to another. Download File Attachment v2  | /v2/file/download\_\_\_post |
| GetMediaObjectList - List Attachments v1 | Media Object Service Get a list of media objects for the specific structure and key. List Attachments v1  | /file/list\_\_\_post |
| GetMediaObjectList 1 - List Attachments v2 | Media Object Service Get a list of media objects for the specific structure and key. List Attachments v2  | /v2/file/list\_\_\_post |
| GetMediaObjectText - Get Text Attachment v1 | Media Object Service Get the first text media object for a given structure and key. Get Text Attachment v1  | /file/gettext\_\_\_post |
| GetMediaObjectText 1 - Get Text Attachment v2 | Media Object Service Get text media objects for a given structure and key. Get Text Attachment v2  | /v2/file/gettext\_\_\_post |
| GetSubscribers - Get Notification Subscriptions v2 | Notification Service Get the list of subscribers to a specific notification. Get Notification Subscriptions v2  | /v2/notification/subscriptions\_\_\_post |
| GetUBEOutput - FTP Report Output v2 | External Service Using the report job number, transfer the output of the report to an external FTP server. FTP Report Output v2  | /v2/external/ftp/report\_\_\_post |
| JargonService - Fetch Jargon and Translations v1 | Jargon Service Jargon service allows users to get the jargon and language specific names for data dictionary items in EnterpriseOne. Fetch Jargon and Translations v1  | /jargonservice\_\_\_post |
| JargonService 1 - Fetch Jargon and Translations v2 | Jargon Service Jargon service allows users to get the jargon and language specific names for data dictionary items in EnterpriseOne. Fetch Jargon and Translations v2  | /v2/jargonservice\_\_\_post |
| LaunchUBE - Execute a Report v2 | Report Service Execute the specified report. Execute a Report v2  | /v2/report/execute\_\_\_post |
| LaunchUBEDiscover - Discover Details of a Report v2 | Report Service Discover the processing option, data selection, and data sequencing for a report. Discover Details of a Report v2  | /v2/report/discover\_\_\_post |
| LaunchUBEStatus - Get the Completion Status of a Report v2 | Report Service Use the batch job number to get the completion status of the report. Get the Completion Status of a Report v2  | /v2/report/status\_\_\_post |
| ListExecutingJobs - List Jobs Actively Executing During At This Snapsh... | Scheduler Service Show the attributes of all the notifications and orchestrations that are currently executing on this server. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. List Jobs Actively Executing During At This Snapshot In time v2  | /v2/scheduler/listExecuting\_\_\_post |
| ListJobs - List a Collection of Services v2 | Scheduler Service List the status of one or more notifications or orchestrations that is scheduled to run. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. List a Collection of Services v2  | /v2/scheduler/listJobs\_\_\_post |
| ListScheduledJobs - List Scheduled Jobs v2 | Scheduler Service List the status of all notifications and orchestrations that are scheduled to run. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. List Scheduled Jobs v2  | /v2/scheduler/list\_\_\_post |
| NextLinkPOST - Execute Next Record Request v2 | Application Stack Service Execute stateful calls to get the next record set for a grid. Execute Next Record Request v2  | /v2/appstack/next\_\_\_post |
| NextLinkPOST 1 - Execute Next Record Request v2 | Data Service Execute stateful calls to get the next record set for a grid. Execute Next Record Request v2  | /v2/dataservice/next\_\_\_post |
| PingScheduler - Ping Scheduler v2 | Scheduler Service Query a server to determine if its scheduler is running. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. Ping Scheduler v2  | /v2/scheduler/pingScheduler\_\_\_post |
| PingSchedulers - Ping Schedulers v2 | Scheduler Service Query a list of servers to determine if their schedulers are running. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. Ping Schedulers v2  | /v2/scheduler/pingSchedulers\_\_\_post |
| PostDataRequest - Query or Aggregate Tables and Views v1 | Data Service Execute queries over EnterpriseOne tables and business views. Query or Aggregate Tables and Views v1  | /dataservice\_\_\_post |
| PostDataRequest 1 - Query or Aggregate Tables and Views v2 | Data Service Execute queries over EnterpriseOne tables and business views. Query or Aggregate Tables and Views v2  | /v2/dataservice\_\_\_post |
| PostDiscover - List Notifications v2 | Notification Service Request a list of available notifications. List Notifications v2  | /v2/notification/discover\_\_\_post |
| PostDiscover 1 - List Notification Schedules v2 | Scheduler Service Request a list of scheduled notifications and orchestrations. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. List Notification Schedules v2  | /v2/scheduler/discover\_\_\_post |
| PostE1Rest - Get Configuration v1 | Default Config Service Get AIS Server configuration information and available capabilities. Get Configuration v1  | /defaultconfig\_\_\_post |
| PostE1Rest 1 - Get Configuration v2 | Default Config Service Get AIS Server configuration information and available capabilities. Get Configuration v2  | /v2/defaultconfig\_\_\_post |
| PostE1RestLogout - Logout Session | Orchestrator Service Log out with this token and terminate the session held for the token passed. Logout Session  | /orchestrator/jde-logout\_\_\_post |
| PostE1RestLogout 1 - Logout Session v2 | Orchestrator Service Log out with this token and terminate the session held for the token passed. Logout Session v2  | /v2/orchestrator/jde-logout\_\_\_post |
| PostE1RestLogout 2 - Logout Session v3 | Orchestrator Service Log out with this token and terminate the session held for the token passed. Logout Session v3  | /v3/orchestrator/jde-logout\_\_\_post |
| PostE1RestLogout 3 - Logout Session v1 | Token Request Service Log out with this token and terminate the session held for the token passed. Logout Session v1  | /tokenrequest/logout\_\_\_post |
| PostE1RestLogout 4 - Logout Session v2 | Token Request Service Log out with this token and terminate the session held for the token passed. Logout Session v2  | /v2/tokenrequest/logout\_\_\_post |
| PostE1RestToken - Request a Token | Orchestrator Service Request an AIS token to be used for subsequent requests. Authentication is performed with given credentials and a session token is returned. Request a Token  | /orchestrator/jde-login\_\_\_post |
| PostE1RestToken 1 - Request a Token v2 | Orchestrator Service Request an AIS token to be used for subsequent requests. Authentication is performed with given credentials and a session token is returned. Request a Token v2  | /v2/orchestrator/jde-login\_\_\_post |
| PostE1RestToken 2 - Request a Token v3 | Orchestrator Service Request an AIS token to be used for subsequent requests. Authentication is performed with given credentials and a session token is returned. Request a Token v3  | /v3/orchestrator/jde-login\_\_\_post |
| PostE1RestToken 3 - Request a Token v1 | Token Request Service Request an AIS token to be used for subsequent requests. Authentication is performed with given credentials and a session token is returned. Request a Token v1  | /tokenrequest\_\_\_post |
| PostE1RestToken 4 - Request a Token v2 | Token Request Service Request an AIS token to be used for subsequent requests. Authentication is performed with given credentials and a session token is returned. Request a Token v2  | /v2/tokenrequest\_\_\_post |
| PostE1RestToken 5 - Request Logging v1 | Logging Service Request to add an entry for the passed message to the AIS Server log. Request Logging v1  | /log\_\_\_post |
| PostE1RestToken 6 - Request Logging v2 | Logging Service Request to add an entry for the passed message to the AIS Server log. Request Logging v2  | /v2/log\_\_\_post |
| PostJAS - Execute a Form v1 | Form Service Request to execute an EnterpriseOne form. Optionally, perform actions on the form and receive data from the form in the response. Execute a Form v1  | /formservice\_\_\_post |
| PostJAS 1 - Manage Preferences v1 | Preference Service Enables management of preference records in the User Overrides Table (F98950). Manage Preferences v1  | /preference\_\_\_post |
| PostJAS 2 - Get Menu Tasks v1 | Task Authorization Service Receive a list of authorized tasks based on either a task view ID or the task ID and parent ID with in a task view. Get Menu Tasks v1  | /taskauthorization\_\_\_post |
| PostJAS 3 - Execute a Watchlist v1 | Watchlist Service Execute the specified Watchlist. The response includes record count as well as Watchlist definition metadata. Execute a Watchlist v1  | /watchlist\_\_\_post |
| PostJAS 4 - Execute a Form v2 | Form Service Request to execute an EnterpriseOne form. Optionally, perform actions on the form and receive data from the form in the response. Execute a Form v2  | /v2/formservice\_\_\_post |
| PostJAS 5 - Send Messages v2 | Message Service Sends an EnterpriseOne message (PPAT) to external email systems or the EnterpriseOne Work Center email system. Send Messages v2  | /v2/message\_\_\_post |
| PostJAS 6 - Manage Preferences v2 | Preference Service Enables management of preference records in the User Overrides Table (F98950). Manage Preferences v2  | /v2/preference\_\_\_post |
| PostJAS 7 - Get Menu Tasks v2 | Task Authorization Service Receive a list of authorized tasks based on either a task view ID or the task ID and parent ID with in a task view. Get Menu Tasks v2  | /v2/taskauthorization\_\_\_post |
| PostJAS 8 - Execute a Watchlist v2 | Watchlist Service Execute the specified Watchlist. The response includes record count as well as Watchlist definition metadata. Execute a Watchlist v2  | /v2/watchlist\_\_\_post |
| PostJASExternalMultiPartREST - Send Report Output to External REST Service v2 | External Service Send the output of a report to an external REST service (using multi-part/formdata). Send Report Output to External REST Service v2  | /v2/external/rest/report\_\_\_post |
| PostValidateSession - Validate Token | Orchestrator Service Determine if the the token passed is still valid. Validate Token  | /orchestrator/jde-validate-session\_\_\_post |
| PostValidateSession 1 - Validate Token v2 | Orchestrator Service Determine if the the token passed is still valid. Validate Token v2  | /v2/orchestrator/jde-validate-session\_\_\_post |
| PostValidateSession 2 - Validate Token v3 | Orchestrator Service Determine if the the token passed is still valid. Validate Token v3  | /v3/orchestrator/jde-validate-session\_\_\_post |
| PostValidateSession 3 - Validate Token v2 | Token Request Service Determine if the the token passed is still valid. Validate Token v2  | /v2/tokenrequest/validate\_\_\_post |
| PostView - View aggregate of system v2 | Scheduler Service View the scheduler system. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. View aggregate of system v2  | /v2/scheduler/view\_\_\_post |
| ProcessingOptionService - Get Processing Options v1 | Processing Option Service Get the values of the processing options for the specified application and version. Get Processing Options v1  | /poservice\_\_\_post |
| ProcessingOptionService 1 - Get Processing Options v2 | Processing Option Service Get the values of the processing options for the specified application and version. Get Processing Options v2  | /v2/poservice\_\_\_post |
| StartClusterUnitTest - Start Configuration Test for a Cluster | Scheduler Service Starts jobs, one for each host requested, and runs them in sequence.  If more than one host is requested, the hosts are started and stopped, testing that all of the hosts are correctly configured.  Start Configuration Test for a Cluster  | /v2/scheduler/startClusterUnitTest\_\_\_post |
| StartJobs - Start a Collection of Services v2 | Scheduler Service Start one or more notifications or orchestrations. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. Start a Collection of Services v2  | /v2/scheduler/startJobs\_\_\_post |
| StartScheduledJobs - Start All Scheduled Notifications and Orchestratio... | Scheduler Service Starts the scheduler process for all notifications and orchestrations that have schedules associated to them. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service and use the token for calls to all scheduler services. Start All Scheduled Notifications and Orchestrations v2  | /v2/scheduler/start\_\_\_post |
| StartScheduler - Start a Scheduler v2 | Scheduler Service Start the scheduler on a given port. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services.. Start a Scheduler v2  | /v2/scheduler/startScheduler\_\_\_post |
| StartSchedulers - Start a Collection of Schedulers v2 | Scheduler Service Start the scheduler on a this server. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. Start a Collection of Schedulers v2  | /v2/scheduler/startSchedulers\_\_\_post |
| StopScheduledJobs - Stop the Scheduler Engine v2 | Scheduler Service Stop all currently running schedules and also stop the scheduler engine. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service and use the token for calls to all scheduler services. Stop the Scheduler Engine v2  | /v2/scheduler/stop\_\_\_post |
| StopScheduler - Stop a Scheduler v2 | Scheduler Service Stop the scheduler on a this server. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. Stop a Scheduler v2  | /v2/scheduler/stopScheduler\_\_\_post |
| StopSchedulers - Stop a Collection of Schedulers v2 | Scheduler Service Stop scheduler instance on one or more servers. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. Stop a Collection of Schedulers v2  | /v2/scheduler/stopSchedulers\_\_\_post |
| StopsJobs - Stop a Collection of Services v2 | Scheduler Service Stop one or more notifications or orchestrations. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. Stop a Collection of Services v2  | /v2/scheduler/stopJobs\_\_\_post |
| TaskSearchJAS - Task Search v2 | Task Authorization Service Receive a list of tasks matching the provided search terms. Task Search v2  | /v2/taskauthorization/search\_\_\_post |
| UpdateMediaObjectText - Update Text Attachment v1 | Media Object Service Update the text of the given text media object. Update Text Attachment v1  | /file/updatetext\_\_\_post |
| UpdateMediaObjectText 1 - Update Text Attachment v2 | Media Object Service Update the text of a text media object for a given structure, key, and sequence. If sequence is not provided, the first text media object will be updated. Update Text Attachment v2  | /v2/file/updatetext\_\_\_post |
| UploadMediaObject - Upload File Attachment v1 | Media Object Service Upload a file type media object for the given structure and key. The 'moAdd' parameter takes a JSON string in the form of the MediaObjectUploadRequest class Upload File Attachment v1  | /file/upload\_\_\_post |
| UploadMediaObject 1 - Upload File Attachment v2 | Media Object Service Upload a file type media object for the given structure and key. The 'moAdd' parameter takes a JSON string in the form of the MediaObjectUploadRequest class Upload File Attachment v2  | /v2/file/upload\_\_\_post |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| GetBaseOpenAPICatalog 1 - Get Orchestration v3 | OpenAPI 3.0 Returns information about a single orchestration REST API's. Get Orchestration v3  | /v3/open-api-catalog3/{orchName}\_\_\_get |
| GetBaseSwaggerCatalog 1 - Get Orchestration v2 | Swagger 2.0 Returns information about a single orchestration REST API's. Get Orchestration v2  | /v2/open-api-catalog/{orchName}\_\_\_get |
| GetBaseSwaggerCatalog 3 - Get Orchestration v3 | Swagger 2.0 Returns information about a single orchestration REST API's. Get Orchestration v3  | /v3/open-api-catalog/{orchName}\_\_\_get |
| GetForm - Simple Form Service Query v2 | Form Service Execute simple queries over EnterpriseOne forms. Simple Form Service Query v2  | /v2/formservice/{application}/{form}\_\_\_get |
| GetFormVersion - Simple Form Service Query with Version v2 | Form Service Execute simple queries over EnterpriseOne forms Simple Form Service Query with Version v2  | /v2/formservice/{application}/{form}/{version}\_\_\_get |
| GetTable - Simple Table Query v2 | Data Service Execute simple queries over EnterpriseOne tables. Simple Table Query v2  | /v2/dataservice/table/{tableName}\_\_\_get |
| GetView - Simple View Query v2 | Data Service Execute simple queries over EnterpriseOne business views. Simple View Query v2  | /v2/dataservice/view/{viewName}\_\_\_get |
| GetWithPath - Execute an Orchestration | Orchestrator Service Execute the orchestration specified in the URL. Execute an Orchestration  | /orchestrator/{orchestration}\_\_\_get |
| GetWithPath 1 - Execute an Orchestration v2 | Orchestrator Service Execute the orchestration specified in the URL. Execute an Orchestration v2  | /v2/orchestrator/{orchestration}\_\_\_get |
| GetWithPath 2 - Execute an Orchestration v3 | Orchestrator Service Execute the orchestration specified in the URL. Execute an Orchestration v3  | /v3/orchestrator/{orchestration}\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| GetBaseOpenAPICatalog - Get All Orchestrations v3 | OpenAPI 3.0 Returns information about all orchestration REST API's. Get All Orchestrations v3  | /v3/open-api-catalog3\_\_\_get |
| GetBaseSwaggerCatalog - Get All Orchestrations v2 | Swagger 2.0 Returns information about all orchestration REST API's. Get All Orchestrations v2  | /v2/open-api-catalog\_\_\_get |
| GetBaseSwaggerCatalog 2 - Get All Orchestrations v3 | Swagger 2.0 Returns information about all orchestration REST API's. Get All Orchestrations v3  | /v3/open-api-catalog\_\_\_get |
| GetDiscover - List Orchestrations | Orchestration Discovery Service Request a list of available orchestrations. List Orchestrations  | /discover\_\_\_get |
| GetE1Rest - Get Configuration v1 | Default Config Service Get AIS Server configuration information and available capabilities. Get Configuration v1  | /defaultconfig\_\_\_get |
| GetE1Rest 1 - Get Configuration v2 | Default Config Service Get AIS Server configuration information and available capabilities. Get Configuration v2  | /v2/defaultconfig\_\_\_get |
| GetE1RestLogout - Logout Session | Orchestrator Service Log out with this token and terminate the session held for the token passed. Logout Session  | /orchestrator/jde-logout\_\_\_get |
| GetE1RestLogout 1 - Logout Session v2 | Orchestrator Service Log out with this token and terminate the session held for the token passed. Logout Session v2  | /v2/orchestrator/jde-logout\_\_\_get |
| GetE1RestLogout 2 - Logout Session v3 | Orchestrator Service Log out with this token and terminate the session held for the token passed. Logout Session v3  | /v3/orchestrator/jde-logout\_\_\_get |
| GetE1RestToken - Request a Token | Orchestrator Service Request an AIS token to be used for subsequent requests. Authentication is performed with given credentials and a session token is returned. Request a Token  | /orchestrator/jde-login\_\_\_get |
| GetE1RestToken 1 - Request a Token v2 | Orchestrator Service Request an AIS token to be used for subsequent requests. Authentication is performed with given credentials and a session token is returned. Request a Token v2  | /v2/orchestrator/jde-login\_\_\_get |
| GetE1RestToken 2 - Request a Token v3 | Orchestrator Service Request an AIS token to be used for subsequent requests. Authentication is performed with given credentials and a session token is returned. Request a Token v3  | /v3/orchestrator/jde-login\_\_\_get |
| GetE1RestToken 3 - Request a Token v2 | Token Request Service Request an AIS token to be used for subsequent requests. Authentication is performed with given credentials and a session token is returned. Request a Token v2  | /v2/tokenrequest\_\_\_get |
| GetValidateSession - Validate Token | Orchestrator Service Determine if the the token passed is still valid. Validate Token  | /orchestrator/jde-validate-session\_\_\_get |
| GetValidateSession 1 - Validate Token v2 | Orchestrator Service Determine if the the token passed is still valid. Validate Token v2  | /v2/orchestrator/jde-validate-session\_\_\_get |
| GetValidateSession 2 - Validate Token v3 | Orchestrator Service Determine if the the token passed is still valid. Validate Token v3  | /v3/orchestrator/jde-validate-session\_\_\_get |
| NextLink - Execute Next Record Request v2 | Application Stack Service Execute stateful calls to get the next record set for a grid. Execute Next Record Request v2  | /v2/appstack/next\_\_\_get |
| NextLink 1 - Execute Next Record Request v2 | Data Service Execute stateful calls to get the next record set for a grid. Execute Next Record Request v2  | /v2/dataservice/next\_\_\_get |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| PostWithPath - Execute an Orchestration (POST) | Orchestrator Service Execute the orchestration specified in the URL. Execute an Orchestration  | /orchestrator/{orchestration}\_\_\_post |
| PostWithPath 1 - Execute an Orchestration v2 (POST) | Orchestrator Service Execute the orchestration specified in the URL. Execute an Orchestration v2  | /v2/orchestrator/{orchestration}\_\_\_post |
| PostWithPath 2 - Execute an Orchestration v3 (POST) | Orchestrator Service Execute the orchestration specified in the URL. Execute an Orchestration v3  | /v3/orchestrator/{orchestration}\_\_\_post |
| RunNotification - Execute a Notification v2 (POST) | Notification Service Execute a notification. Execute a Notification v2  | /v2/notification/{notification}\_\_\_post |
| StartJob - Start a Scheduled Job v2 (POST) | Scheduler Service Start a job using the name or OMW object name of the notification or orchestration. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. Start a Scheduled Job v2  | /v2/scheduler/startjob/{endpoint}\_\_\_post |
| StopJob - Stop a Scheduled Job v2 (POST) | Scheduler Service Stop a scheduled notification or orchestration by passing the name or OMW object name in the endpoint parameter. IMPORTANT: If using a token for authentication, you must get the token using the v2 tokenrequest service for all calls to scheduler services. Stop a Scheduled Job v2  | /v2/scheduler/stopjob/{endpoint}\_\_\_post |
| UdoManagerOps - Get UDO Details v1 (POST) | UDO Service Request details of Watchlist or query user defined objects (UDOs). Get UDO Details v1  | /udomanager/{operation}\_\_\_post |
| UdoManagerOps 1 - Get UDO Details v2 (POST) | UDO Service Request details of Watchlist or query user defined objects (UDOs). Get UDO Details v2  | /v2/udomanager/{operation}\_\_\_post |


