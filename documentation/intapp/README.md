# IntApp REST API Connector
 **/GenericConnectorDescriptor/description NOT SET IN DESCRIPTOR FILE**

# Connection Tab
## Connection Fields


#### Other Intapp REST API Swagger URL

This will override selections so you can provide a url to any swagger file other than those presented in an operation.

**Type** - string



#### URL

The URL for the Service service

**Type** - string



#### Intapp OpenAPI Service

The URL for the Intapp OpenAPI descriptor file

**Type** - string

##### Allowed Values

 * Intapp Time API
 * Clients/Matters Service
 * Intapp API


#### Other Oracle REST API Swagger URL

This will override selections so you can provide a url to any swagger file.

**Type** - string



#### AuthenticationType

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - oauth

# Operation Tab


## CREATE


## UPDATE


## DELETE


## GET


## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1


### Query Options


#### Fields

The connector does not support field selection. All fields will be returned by default.


#### Filters

The query filter supports any number of non-nested expressions which will be AND'ed together.

Example:
((foo lessThan 30) AND (baz lessThan 42))

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts




# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Client CreateClient - Creates a new Client. | Creates a new Client. | /time/api/v1/clients\_\_\_post |
| Client CreateClients - Creates a set of new Clients. | Creates a set of new Clients. | /time/api/v1/clients/\_bulk\_\_\_post |
| Contact CreateContacts - Creates a set of new Contacts. | Creates a set of new Contacts. | /time/api/v1/capturecontacts/\_bulk\_\_\_post |
| Group CreateGroup - Creates a Group. | Creates a Group. | /time/api/v1/groups\_\_\_post |
| Group CreateGroups - Creates a set of new Groups. | Creates a set of new Groups. | /time/api/v1/groups/\_bulk\_\_\_post |
| GroupMember AddGroupMembers - Creates a set of new GroupMembers. | Creates a set of new GroupMembers. | /time/api/v1/groupmembers/\_bulk\_\_\_post |
| Matter CreateMatter - Creates a new Matter. | Creates a new Matter. | /time/api/v1/matters\_\_\_post |
| Matter CreateMatters - Creates a set of new Matters. | Creates a set of new Matters. | /time/api/v1/matters/\_bulk\_\_\_post |
| NarrativeCode CreateNarrativeCode - Creates a Narrative Code. | Creates a Narrative Code. | /time/api/v1/narrativecodes\_\_\_post |
| NarrativeCode CreateNarrativeCodes - Creates a set of new Narrative Codes. | Creates a set of new Narrative Codes. | /time/api/v1/narrativecodes/\_bulk\_\_\_post |
| PersonalNarrativeCode CreatePersonalNarrativeCode - Creates a Personal Narrative Code. | Creates a Personal Narrative Code. | /time/api/v1/personalnarrativecodes\_\_\_post |
| PersonalNarrativeCode CreatePersonalNarrativeCodes - Creates a set of new Personal Narrative Codes. | Creates a set of new Personal Narrative Codes. | /time/api/v1/personalnarrativecodes/\_bulk\_\_\_post |
| RestrictedText CreateRestrictedText - Create new Restricted Text. | Create new Restricted Text. | /time/api/v1/restrictedtexts\_\_\_post |
| RestrictedText CreateRestrictedTexts - Creates set of Restricted Texts. | Creates set of Restricted Texts. | /time/api/v1/restrictedtexts/\_bulk\_\_\_post |
| RestrictedTextGroup CreateRestrictedTextGroup - Creates a Restricted Text Group. | Creates a Restricted Text Group. | /time/api/v1/restrictedtextgroups\_\_\_post |
| RestrictedTextGroup CreateRestrictedTextGroups - Creates a set of new Restricted Text groups. | Creates a set of new Restricted Text groups. | /time/api/v1/restrictedtextgroups/\_bulk\_\_\_post |
| RightsAccess CreateRightsAccess - Creates a rights access row | Creates a rights access row | /time/api/v1/rightsaccess\_\_\_post |
| RightsAccess CreateRightsAccesses - Creates set of rights accesses rows | Creates set of rights accesses rows | /time/api/v1/rightsaccess/\_bulk\_\_\_post |
| TimeEntry CreateTimeEntries - Creates time entries from a list | Creates time entries from a list | /time/api/v1/timeentries/\_bulk\_\_\_post |
| TimeEntry CreateTimeEntry - Creates a new Time Entry. | Creates a new Time Entry. | /time/api/v1/timeentries\_\_\_post |
| TimeEntry Release - Releases a time entry. | Releases a time entry. | /time/api/v1/timeentries/release\_\_\_post |
| TimeEntry Release Async - Releases a time entry in async manner. | Releases a time entry in async manner. | /time/api/v1/timeentries/release/\_async\_\_\_post |
| UserCode CreateUserCode - Creates a User Code. | Creates a User Code. | /time/api/v1/usercodes\_\_\_post |
| UserCode CreateUserCodes - Creates User Codes in bulk. | Creates User Codes in bulk. | /time/api/v1/usercodes/\_bulk\_\_\_post |
| UserList CreateUserCodeList - Creates a User Code List. | Creates a User Code List. | /time/api/v1/usercodelists\_\_\_post |
| UserList CreateUserCodeLists - Creates a set of new User Code List. | Creates a set of new User Code List. | /time/api/v1/usercodelists/\_bulk\_\_\_post |
| WorkingProfile CreateWorkingProfileLinks - Creates Working Profile Links in bulk. | Creates Working Profile Links in bulk. | /time/api/v1/workingprofiles/links/\_bulk\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Client DeleteClient - Delete a Client. | Delete a Client. | /time/api/v1/clients/{clientId}\_\_\_delete |
| Group DeleteGroup - Deletes a Group by Id. | Deletes a Group by Id. | /time/api/v1/groups/{groupId}\_\_\_delete |
| Matter DeleteMatter - Deletes a Matter. | Deletes a Matter. | /time/api/v1/matters/{matterId}\_\_\_delete |
| NarrativeCode DeleteNarrativeCode - Deletes a Narrative Code by Id. | Deletes a Narrative Code by Id. | /time/api/v1/narrativecodes/{code}\_\_\_delete |
| PersonalNarrativeCode DeletePersonalNarrativeCode - Deletes a Personal Narrative Code by Id. | Deletes a Personal Narrative Code by Id. | /time/api/v1/personalnarrativecodes/{code}\_\_\_delete |
| RestrictedText DeleteRestrictedText - Deletes Restricted Text. | Deletes Restricted Text. | /time/api/v1/restrictedtexts/{restrictedTextId}\_\_\_delete |
| RestrictedTextGroup DeleteRestrictedTextGroup - Deletes a Restricted Text Group by Id. | Deletes a Restricted Text Group by Id. | /time/api/v1/restrictedtextgroups/{groupId}\_\_\_delete |
| RuleStorage DeleteRuleData - Delete a Rule Data by its key. | Delete a Rule Data by its key. | /time/api/v1/rulestorage/{storageKey}\_\_\_delete |
| Template DeleteTemplate - Deletes a Template by Id. | Deletes a Template by Id. | /time/api/v1/templates/{templateId}\_\_\_delete |
| TimeEntry DeleteTimeEntry - Deletes a Time Entry. | Deletes a Time Entry. | /time/api/v1/timeentries/{entryId}\_\_\_delete |
| Timer DeleteTimer - Deletes a Timer by Id. | Deletes a Timer by Id. | /time/api/v1/timers/{timerId}\_\_\_delete |
| UserCode DeleteUserCode - Deletes a User Code by Id. | Deletes a User Code by Id. | /time/api/v1/usercodes/{userCodeId}\_\_\_delete |
| UserList DeleteUserCodeList - Deletes a User Code Lists by Id. | Deletes a User Code Lists by Id. | /time/api/v1/usercodelists/{listId}\_\_\_delete |
| WorkingProfile DeleteWorkingProfile - Deletes a Working Profile by Id. | Deletes a Working Profile by Id. | /time/api/v1/workingprofiles/{workingProfileId}\_\_\_delete |
| WorkingProfileWeek DeleteWorkingProfileWeek - Deletes a Working Profile Week by Id. | Deletes a Working Profile Week by Id. | /time/api/v1/workingprofileweeks/{workingProfileWeekId}\_\_\_delete |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Activity GetCapturedActivity - Gets the Capture Activity by specified activity id... | Gets the Capture Activity by specified activity id... | /time/api/v1/captureactivities/{activityId}\_\_\_get |
| CaptureConfig GetcaptureConfig - Gets a Capture Config by its name. | Gets a Capture Config by its name. | /time/api/v1/captureconfigs/{configName}\_\_\_get |
| Client GetClient - Gets a Client by Client Id. | Gets a Client by Client Id. | /time/api/v1/clients/{clientId}\_\_\_get |
| Contact GetContact - Gets the contact by identifier. | Gets the contact by identifier. | /time/api/v1/capturecontacts/{contactId}\_\_\_get |
| EventPreference GetEventPreference - Gets the Event Preference. | Gets the Event Preference. | /time/api/v1/captureeventprefs/{eventTypeId}\_\_\_get |
| EventType GetEventType - Gets a Event Type by Id. | Gets a Event Type by Id. | /time/api/v1/captureeventtypes/{eventTypeId}\_\_\_get |
| Group GetGroup - Gets a Group by Group Id. | Gets a Group by Group Id. | /time/api/v1/groups/{groupId}\_\_\_get |
| JournalPreference GetJournalPreferencesByUserName - Gets Journal Preferences by provided user name. | Gets Journal Preferences by provided user name. | /time/api/v1/capturejournalprefs/{userName}\_\_\_get |
| Matter GetMatter - Gets a Matter by Matter Id. | Gets a Matter by Matter Id. | /time/api/v1/matters/{matterId}\_\_\_get |
| NarrativeCode GetNarrativeCode - Get a Narrative Code. | Get a Narrative Code. | /time/api/v1/narrativecodes/{code}\_\_\_get |
| PersonalNarrativeCode GetPersonalNarrativeCode - Gets a Personal Narrative Code by Id. | Gets a Personal Narrative Code by Id. | /time/api/v1/personalnarrativecodes/{code}\_\_\_get |
| RestrictedText GetRestrictedText - Gets a Restricted Text. | Gets a Restricted Text. | /time/api/v1/restrictedTexts/{restrictedTextId}\_\_\_get |
| RestrictedTextGroup GetRestrictedTextGroup - Gets a Restricted Text Group by Id. | Gets a Restricted Text Group by Id. | /time/api/v1/restrictedtextgroups/{groupId}\_\_\_get |
| RightsAccess GetRightsAccess - Gets all Rights Access Rows for a Principal. | Gets all Rights Access Rows for a Principal. | /time/api/v1/rightsaccess/{targetId}\_\_\_get |
| RuleStorage GetRuleData - Gets a Rule Data by its key. | Gets a Rule Data by its key. | /time/api/v1/rulestorage/{storageKey}\_\_\_get |
| Setting GetSetting - Gets specific setting by setting id | Gets specific setting by setting id | /time/api/v1/settings/{settingId}\_\_\_get |
| Template GetTemplate - Get Template by Id | Get Template by Id | /time/api/v1/templates/{templateId}\_\_\_get |
| TimeEntry GetAsyncReleaseRequestOverallStatusAsync - Gets Async Release Request Overall Status. | Gets Async Release Request Overall Status. | /time/api/v1/timeentries/release/\_async/{releaseRequestId}\_\_\_get |
| TimeEntry GetTimeEntry - Get a Time Entry by Id. | Get a Time Entry by Id. | /time/api/v1/timeentries/{entryId}\_\_\_get |
| Timer GetTimer - Get Timer by Id | Get Timer by Id | /time/api/v1/timers/{timerId}\_\_\_get |
| UserCode GetUserCode - Gets a User Code. | Gets a User Code. | /time/api/v1/usercodes/{userCodeId}\_\_\_get |
| UserList GetUserCodeList - Gets a User Code List. | Gets a User Code List. | /time/api/v1/usercodelists/{listId}\_\_\_get |
| WorkingProfile GetWorkingProfile - Gets a Working Profile by Id. | Gets a Working Profile by Id. | /time/api/v1/workingprofiles/{workingProfileId}\_\_\_get |
| WorkingProfileWeek GetWorkingProfileWeek - Gets a Working Profile Week by Id. | Gets a Working Profile Week by Id. | /time/api/v1/workingprofileweeks/{workingProfileWeekId}\_\_\_get |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Activity GetCapturedActivities - Gets the Capture Activities by specified filter. | Gets the Capture Activities by specified filter. | /time/api/v1/captureactivities\_\_\_get |
| CaptureConfig GetCaptureConfigs - Get a list of Capture Configs. | Get a list of Capture Configs. | /time/api/v1/captureconfigs\_\_\_get |
| Client GetClients - Get a list of Clients. | Get a list of Clients. | /time/api/v1/clients\_\_\_get |
| Contact GetContacts - Gets the contacts by provided filter. | Gets the contacts by provided filter. | /time/api/v1/capturecontacts\_\_\_get |
| EventPreference GetEventPreferences - Get Event Preferences. | Get Event Preferences. | /time/api/v1/captureeventprefs\_\_\_get |
| EventType GetEventTypes - Get a list of Event Type. | Get a list of Event Type. | /time/api/v1/captureeventtypes\_\_\_get |
| Group GetGroups - Get a list of Groups. | Get a list of Groups. | /time/api/v1/groups\_\_\_get |
| GroupMember GetGroupMembers - Get a list of GroupMembers. | Get a list of GroupMembers. | /time/api/v1/groupmembers\_\_\_get |
| JournalPreference GetJournalPreferences - Gets Journal Preferences by specified filter. | Gets Journal Preferences by specified filter. | /time/api/v1/capturejournalprefs\_\_\_get |
| Matter GetMatters - Get a list of Matters. | Get a list of Matters. | /time/api/v1/matters\_\_\_get |
| NarrativeCode GetNarrativeCodes - Get a list of Narrative Codes. | Get a list of Narrative Codes. | /time/api/v1/narrativecodes\_\_\_get |
| PersonalNarrativeCode GetPersonalNarrativeCodes - Get a list of Personal Narrative Codes. | Get a list of Personal Narrative Codes. | /time/api/v1/personalnarrativecodes\_\_\_get |
| RestrictedText GetRestrictedTexts - Gets a list of Restricted Texts. | Gets a list of Restricted Texts. | /time/api/v1/restrictedtexts\_\_\_get |
| RestrictedTextGroup GetRestrictedTextGroups - Gets a list of Restricted Text Group. | Gets a list of Restricted Text Group. | /time/api/v1/restrictedtextgroups\_\_\_get |
| RightsAccess GetRightsAccesses - Gets all Right Access Rows from System. | Gets all Right Access Rows from System. | /time/api/v1/rightsaccess\_\_\_get |
| RuleStorage GetRuleDataList - Gets the Rule Data list. | Gets the Rule Data list. | /time/api/v1/rulestorage\_\_\_get |
| Setting GetSettings - Get a list of settings. | Get a list of settings. | /time/api/v1/settings\_\_\_get |
| Template GetTemplates - Get a list of Templates. | Get a list of Templates. | /time/api/v1/templates\_\_\_get |
| TimeEntry GetTimeEntries - Get time entries by params. | Get time entries by params. | /time/api/v1/timeentries\_\_\_get |
| TimeEntry Release Async RequestDetails - Gets async release request details by provided asy... | Gets async release request details by provided asy... | /time/api/v1/timeentries/release/\_async/{releaseRequestId}/details\_\_\_get |
| Timer GetTimers - Gets the timers. | Gets the timers. | /time/api/v1/timers\_\_\_get |
| UserCode GetUserCodes - Gets the User Codes. | Gets the User Codes. | /time/api/v1/usercodes\_\_\_get |
| UserCode GetUserCodesForDefaultList - Gets the User Codes for Default List. | Gets the User Codes for Default List. | /time/api/v1/usercodes/defaultlist\_\_\_get |
| UserList GetUserCodeLists - Gets the User Code list. | Gets the User Code list. | /time/api/v1/usercodelists\_\_\_get |
| WorkingProfile GetWorkingProfileLinks - Gets a list of Working Profile Links. | Gets a list of Working Profile Links. | /time/api/v1/workingprofiles/links\_\_\_get |
| WorkingProfile GetWorkingProfiles - Gets a list of Working Profiles. | Gets a list of Working Profiles. | /time/api/v1/workingprofiles\_\_\_get |
| WorkingProfileWeek GetWorkingProfileWeeks - Get a list of Working Profile Weeks. | Get a list of Working Profile Weeks. | /time/api/v1/workingprofileweeks\_\_\_get |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| Activity UpdateActivities - Updates set of activities. (PATCH) | Updates set of activities. | /time/api/v1/captureactivities\_\_\_patch |
| Client UpdateClient - Partially update an existing Client. (PATCH) | Partially update an existing Client. | /time/api/v1/clients/{clientId}\_\_\_patch |
| Client UpsertClients - Creates or updates set of Clients. (PATCH) | Creates or updates set of Clients. | /time/api/v1/clients\_\_\_patch |
| Contact UpsertContacts - Creates or updates Contacts in bulk. (PATCH) | Creates or updates Contacts in bulk. | /time/api/v1/capturecontacts\_\_\_patch |
| Group UpdateGroup - Updates an existing Group. (PATCH) | Updates an existing Group. | /time/api/v1/groups/{groupId}\_\_\_patch |
| Group UpsertGroups - Creates or updates set of Groups. (PATCH) | Creates or updates set of Groups. | /time/api/v1/groups\_\_\_patch |
| Matter UpdateMatter - Partially update an existing Matter. (PATCH) | Partially update an existing Matter. | /time/api/v1/matters/{matterId}\_\_\_patch |
| Matter UpsertMatters - Creates or updates set of Matters. (PATCH) | Creates or updates set of Matters. | /time/api/v1/matters\_\_\_patch |
| NarrativeCode UpdateNarrativeCode - Updates an existing Narrative Code. (PATCH) | Updates an existing Narrative Code. | /time/api/v1/narrativecodes/{code}\_\_\_patch |
| NarrativeCode UpsertNarrativeCodes - Creates or updates set of Narrative Codes. (PATCH) | Creates or updates set of Narrative Codes. | /time/api/v1/narrativecodes\_\_\_patch |
| PersonalNarrativeCode UpdatePersonalNarrativeCode - Updates an existing Personal Narrative Code. (PATCH) | Updates an existing Personal Narrative Code. | /time/api/v1/personalnarrativecodes/{code}\_\_\_patch |
| PersonalNarrativeCode UpsertPersonalNarrativeCodes - Creates or updates set of Personal Narrative Code. (PATCH) | Creates or updates set of Personal Narrative Code. | /time/api/v1/personalnarrativecodes\_\_\_patch |
| RestrictedText UpdateRestrictedText - Partially update an existing Restricted Text. (PATCH) | Partially update an existing Restricted Text. | /time/api/v1/restrictedtexts/{restrictedTextId}\_\_\_patch |
| RestrictedText UpsertRestrictedTexts - Creates or updates set of Restricted Texts. (PATCH) | Creates or updates set of Restricted Texts. | /time/api/v1/restrictedtexts\_\_\_patch |
| RestrictedTextGroup UpdateRestrictedTextGroup - Updates an existing Restricted Text group. (PATCH) | Updates an existing Restricted Text group. | /time/api/v1/restrictedtextgroups/{groupId}\_\_\_patch |
| RestrictedTextGroup UpsertRestrictedTextGroups - Creates or updates set of Restricted Text Groups. (PATCH) | Creates or updates set of Restricted Text Groups. | /time/api/v1/restrictedtextgroups\_\_\_patch |
| RightsAccess UpdateRightsAccess - Updates a Rights Access Rows. (PATCH) | Updates a Rights Access Rows. | /time/api/v1/rightsaccess/{targetId}\_\_\_patch |
| RightsAccess UpsertRightsAccesses - Creates or updates Rights Access rows in bulk. (PATCH) | Creates or updates Rights Access rows in bulk. | /time/api/v1/rightsaccess\_\_\_patch |
| RuleStorage UpsertRuleData - Create or Update Rule Data. (PATCH) | Create or Update Rule Data. | /time/api/v1/rulestorage/{storageKey}\_\_\_patch |
| Setting UpdateSetting - Updates an existing Setting. (PATCH) | Updates an existing Setting. | /time/api/v1/settings/{settingId}\_\_\_patch |
| Setting UpsertSettings - Creates or updates settings in bulk (PATCH) | Creates or updates settings in bulk | /time/api/v1/settings\_\_\_patch |
| Template UpsertTemplates - Creates or updates set of templates (PATCH) | Creates or updates set of templates | /time/api/v1/templates\_\_\_patch |
| TimeEntry UpdateTimeEntry - Partially update an existing Time Entry. (PATCH) | Partially update an existing Time Entry. | /time/api/v1/timeentries/{entryId}\_\_\_patch |
| TimeEntry UpsertTimeEntries - Creates or updates set of Time Entries. (PATCH) | Creates or updates set of Time Entries. | /time/api/v1/timeentries\_\_\_patch |
| Timer UpsertTimers - Creates or updates set of timers (PATCH) | Creates or updates set of timers | /time/api/v1/timers\_\_\_patch |
| UserCode UpdateUserCode - Updates a User Code. (PATCH) | Updates a User Code. | /time/api/v1/usercodes/{userCodeId}\_\_\_patch |
| UserCode UpsertUserCodes - Creates or updates User Codes in bulk. (PATCH) | Creates or updates User Codes in bulk. | /time/api/v1/usercodes\_\_\_patch |
| UserList UpdateUserCodeList - Updates an existing User Code List. (PATCH) | Updates an existing User Code List. | /time/api/v1/usercodelists/{listId}\_\_\_patch |
| UserList UpsertUserCodeLists - Creates or updates set of User Code List. (PATCH) | Creates or updates set of User Code List. | /time/api/v1/usercodelists\_\_\_patch |
| WorkingProfile UpsertWorkingProfiles - Creates or updates set of Working Profiles. (PATCH) | Creates or updates set of Working Profiles. | /time/api/v1/workingprofiles\_\_\_patch |
| WorkingProfileWeek UpsertWorkingProfileWeeks - Creates or updates set of Working Profile Weeks. (PATCH) | Creates or updates set of Working Profile Weeks. | /time/api/v1/workingprofileweeks\_\_\_patch |


