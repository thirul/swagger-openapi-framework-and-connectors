# Facebook Marketing
Facebook Marketing REST API for more info https://developers.facebook.com/docs/marketing-apis

# Connection Tab
## Connection Fields


#### Facebook Marketing REST API Service URL

The URL for the Facebook Marketing  REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default openapi.json file so you can provide a url to any swagger file.

**Type** - string



#### URL

The URL for the Service service

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 flavors: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE/EXECUTE


## UPDATE


## GET


## GET


## DELETE


## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  (Supported Types:  date, number, string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
The connector does not support inbound document properties that can be set by a process before an connector shape.
# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.

# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |
| <label not found> |  **helpText NOT SET IN DESCRIPTOR FILE** | /{pixelId}/events\_\_\_post |


### DELETE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |


### GET Operation Types
| Label | Help Text | ID |
| --- | --- | --- |


### QUERY Operation Types
| Label | Help Text | ID |
| --- | --- | --- |


### UPDATE Operation Types
| Label | Help Text | ID |
| --- | --- | --- |


