# Guidewire InsuranceSuite Cloud API
The InsuranceSuite Cloud API is a set of RESTful system APIs that caller applications can use to request data from or initiate action within an InsuranceSuite application. These APIs provide content for the REST API framework that is present in all InsuranceSuite applications. The APIs are built using the Swagger 2.0 Specification. These are also referred to as the system APIs.

The system APIs can be used by browser-based applications and service-to-service applications. This documentation uses the term caller application to generically refer to any application or service calling a system API.

For further details, refer to the Guidewire Cloud API documentation on consuming Cloud APIs. For more info: https://docs.guidewire.com/cloud/cc/202104/cloudapibf/cloudAPI/topics/S01_Consuming/p_consuming-the-Cloud-APIl.html , https://docs.guidewire.com/cloud/cc/202104/apiref/docs/

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### Guidewire InsuranceSuite Cloud API Service URL

The URL for the Guidewire InsuranceSuite Cloud API Service server.

**Type** - string



#### Open API Service

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string

**Default Value** - CLAIM

##### Allowed Values

 * Claim API
 * Admin API
 * Common API
 * Composite API


#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Starts With  **helpText NOT SET IN DESCRIPTOR FILE**

 * Contains  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| AddInvoice - Creates a new service request invoice | Creates a new service request invoice Creates a new service request invoice  |
| AddServiceRequestToClaim - Adds a service request to the claim | Adds a service request to the claim Adds a service request to the claim  |
| AssignClaim - Assign claim to a user or a group | Assign claim to a user or a group Assign claim to a user or a group  |
| AssignExposure - Assign exposure to a user or a group | Assign exposure to a user or a group Assign exposure to a user or a group  |
| AssignServiceRequest - Assigns a service request | Assigns a service request Assigns a service request  |
| Cancel - Cancels and removes the given claim; only allowed ... | Cancels and removes the given claim; only allowed on draft claims. Cancels and removes the given claim; only allowed on draft claims.  |
| CancelServiceRequest - Cancels a service request | Records that the specialist does not intend to perform any further work on this service request Cancels a service request  |
| CloseClaim - Close this claim and transition it to the 'closed'... | Close this claim and transition it to the 'closed' state Close this claim and transition it to the 'closed' state  |
| CloseExposure - Close the exposure | Close the exposure Close the exposure  |
| CompleteRequestedWork - Indicates that the specialist has completed work | Indicates that the specialist has completed work Indicates that the specialist has completed work  |
| CreateClaim - Create a new draft claim | Create a new draft claim Create a new draft claim  |
| CreateClaimActivity - Creates a new activity associated with this claim | Creates a new activity associated with this claim Creates a new activity associated with this claim  |
| CreateClaimContact - Create a new contact on the given claim | Create a new contact on the given claim Create a new contact on the given claim  |
| CreateClaimDocument - Create a new document on the given claim | Create a new document on the given claim Create a new document on the given claim  |
| CreateClaimDwellingIncident - Create a new dwelling incident | Create a new dwelling incident Create a new dwelling incident  |
| CreateClaimExposure - Create a new exposure on the given claim | Create a new exposure on the given claim Create a new exposure on the given claim  |
| CreateClaimFixedPropertyIncident - Create a new fixed property incident on the given ... | Create a new fixed property incident on the given claim Create a new fixed property incident on the given claim  |
| CreateClaimInjuryIncident - Create a new injury incident on the given claim | Create a new injury incident on the given claim Create a new injury incident on the given claim  |
| CreateClaimLivingExpensesIncident - Create a new living expense incident | Create a new living expense incident Create a new living expense incident  |
| CreateClaimNote - Create a new note on this claim | Create a new note on this claim Create a new note on this claim  |
| CreateClaimVehicleIncident - Create a new vehicle incident | Create a new vehicle incident Create a new vehicle incident  |
| DeclineServiceRequest - Declines a service request | Records that the specialist has declined to perform this service request Declines a service request  |
| InternalCancelServiceRequest - Cancels a service request without specialist respo... | Internally cancels this service request without further input from the specialist Cancels a service request without specialist response  |
| PostUnverifiedLocationBasedRiskUnit - Create a location-based risk unit | Create a location-based risk unit Create a location-based risk unit  |
| PostUnverifiedLocationBasedRiskUnitCoverage - Create a coverage on a location based risk unit | Create a coverage on a location based risk unit Create a coverage on a location based risk unit  |
| PostUnverifiedPolicy - Create an unverified policy | Create an unverified policy for a new claim; this endpoint cannot be used in isolation, and can only be used as part of a composite request followed by a /claims POST Create an unverified policy  |
| PostUnverifiedPolicyContact - Create a policy contact | Create a contact on an unverified policy that has not yet been attached to a claim; if this policy has already been attached to a claim, use /claims/{claimId}/contacts POST instead Create a policy contact  |
| PostUnverifiedPolicyCoverage - Create a policy coverage on an unverified policy | Create a coverage on an unverified policy Create a policy coverage on an unverified policy  |
| PostUnverifiedPolicyLocation - Create a policy location | Create a policy location on an unverified policy Create a policy location  |
| PostUnverifiedVehicleRiskUnit - Create a vehicle risk unit | Create a vehicle risk unit Create a vehicle risk unit  |
| PostUnverifiedVehicleRiskUnitCoverage - Create a coverage on a vehicle risk unit | Create a coverage on a vehicle risk unit Create a coverage on a vehicle risk unit  |
| SearchClaims - Searches for claims that match the specified crite... | Searches for claims that match the specified criteria Searches for claims that match the specified criteria  |
| SubmitClaim - Submit this claim and transition it to the 'open' ... | Submit this claim and transition it to the 'open' state Submit this claim and transition it to the 'open' state  |
| SubmitServiceRequest - Requests service from specialist | Submits the service request to the specialist for acceptance (with optional instruction) Requests service from specialist  |
| Validate - Validate a claim | Validate a claim Validate a claim  |
| ValidateExposure - Validate the exposure | Validate the exposure Validate the exposure  |
| WithdrawInvoice - Withdraws an invoice that has been submitted | Withdraws an invoice that has been submitted Withdraws an invoice that has been submitted  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| DeleteClaimContact - Delete a contact on the given claim | Delete a contact on the given claim Delete a contact on the given claim  |
| DeleteClaimDocument - Delete a document on the given claim | Delete a document on the given claim Delete a document on the given claim  |
| DeleteExposure - Delete the draft exposure | Delete the draft exposure Delete the draft exposure  |
| DeleteFixedPropertyIncident - Delete a fixed property incident on the given clai... | Delete a fixed property incident on the given claim Delete a fixed property incident on the given claim  |
| DeleteInjuryIncident - Delete an injury incident on the given claim | Delete an injury incident on the given claim Delete an injury incident on the given claim  |
| DeleteVehicleIncident - Delete a vehicle incident on the given claim | Delete a vehicle incident on the given claim Delete a vehicle incident on the given claim  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| GetClaim - Retrieve a claim | Retrieve a claim Retrieve a claim  |
| GetClaimContact - The details of a specific contact | The details of a specific contact The details of a specific contact  |
| GetClaimDocument - The details of a specific document | The details of a specific document The details of a specific document  |
| GetContactRoleConstraint - The constraints of the given contact role | The constraints of the given contact role The constraints of the given contact role  |
| GetDwellingIncident - Retrieve a dwelling incident | Retrieve a dwelling incident Retrieve a dwelling incident  |
| GetExposure - The details of a specific exposure | The details of a specific exposure The details of a specific exposure  |
| GetFixedPropertyIncident - The details of a specific fixed property incident | The details of a specific fixed property incident The details of a specific fixed property incident  |
| GetInjuryIncident - The details of a specific injury incident | The details of a specific injury incident The details of a specific injury incident  |
| GetInvoice - Retrieves a service request invoice by its id | Retrieves a service request invoice by its id Retrieves a service request invoice by its id  |
| GetLivingExpensesIncident - Retrieve a living expense incident | Retrieve a living expense incident Retrieve a living expense incident  |
| GetLocationBasedRiskUnit - Retrieve a location based risk unit | Retrieve a location based risk unit Retrieve a location based risk unit  |
| GetPolicyCoverage - The policy level coverage associated with a given ... | The policy level coverage associated with a given claim The policy level coverage associated with a given claim  |
| GetPolicyEndorsement - The policy endorsement of a given claim | The policy endorsement of a given claim The policy endorsement of a given claim  |
| GetPolicyLocation - Details of a location on the given claim's policy | Details of a location on the given claim's policy Details of a location on the given claim's policy  |
| GetServiceRequest - Retrieves a service request by its id | Retrieves a service request by its id Retrieves a service request by its id  |
| GetUnverifiedLocationBasedRiskUnit - Get a location based risk unit | Get a location based risk unit Get a location based risk unit  |
| GetUnverifiedLocationBasedRiskUnitCoverage - Details of a coverage on a location based risk uni... | Details of a coverage on a location based risk unit Details of a coverage on a location based risk unit  |
| GetUnverifiedPolicy - Get details of an unverified policy | Get details of an unverified policy Get details of an unverified policy  |
| GetUnverifiedPolicyCoverage - Details of a coverage on an unverified policy | Details of a coverage on an unverified policy Details of a coverage on an unverified policy  |
| GetUnverifiedPolicyLocation - Details of a location on a policy | Details of a location on a policy Details of a location on a policy  |
| GetUnverifiedVehicleRiskUnit - Get a vehicle risk unit | Get a vehicle risk unit Get a vehicle risk unit  |
| GetUnverifiedVehicleRiskUnitCoverage - Details of a coverage on a vehicle risk unit | Details of a coverage on a vehicle risk unit Details of a coverage on a vehicle risk unit  |
| GetVehicleIncident - Retrieve a vehicle incident | Retrieve a vehicle incident Retrieve a vehicle incident  |
| GetVehicleRiskUnit - Retrieve a vehicle risk unit | Retrieve a vehicle risk unit Retrieve a vehicle risk unit  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |
| GetClaimActivities - Returns a list of activities associated with this ... | Returns a list of activities associated with this claim Returns a list of activities associated with this claim  |
| GetClaimActivityAssignees - The list of suggested people, groups, queues, etc.... | The list of suggested people, groups, queues, etc. that new activities on this claim can be assigned to  The list of suggested people, groups, queues, etc. that new activities on this claim can be assigned to  |
| GetClaimActivityPatterns - Returns a list of activity patterns that can be us... | Returns a list of activity patterns that can be used to create activities on this claim Returns a list of activity patterns that can be used to create activities on this claim  |
| GetClaimContactRoleOwners - The contact role owners associated with a given cl... | The contact role owners associated with a given claim. This includes the claim, policy, incidents, exposures, matters, negotiations, and evaluations. Worker's comp injury incidents are not included.  The contact role owners associated with a given claim, including incidents, exposures, policy, and more.  |
| GetClaimContacts - The contacts associated with a given claim | The contacts associated with a given claim The contacts associated with a given claim  |
| GetClaimDocuments - The documents associated with a given claim | The documents associated with a given claim The documents associated with a given claim  |
| GetClaimExposures - The exposures associated with a given claim | The exposures associated with a given claim The exposures associated with a given claim  |
| GetClaimFixedPropertyIncidents - The fixed property incidents associated with a giv... | The fixed property incidents associated with a given claim The fixed property incidents associated with a given claim  |
| GetClaimGraphSchema - Return the JSON Schema Draft-7 representation of t... | Return the JSON Schema Draft-7 representation of the claim graph schema Return the JSON Schema Draft-7 representation of the claim graph schema  |
| GetClaimInjuryIncidents - The injury incidents associated with a given claim | The injury incidents associated with a given claim The injury incidents associated with a given claim  |
| GetClaimNotes - The notes associated with a given claim | The notes associated with a given claim The notes associated with a given claim  |
| GetClaimRelatedObjects - The list of objects related to this claim | The list of objects related to this claim which can be used as the values for the "relatedTo" field on a note or activity. This includes the claim, contacts, unpromoted service requests, service request specialists, exposures, and matters.  The list of objects related to this claim  |
| GetClaims - Retrieves a list of claims | Retrieves a list of assigned claims Retrieves a list of claims  |
| GetClaimVehicleIncidents - Retrieve vehicle incidents on a claim | Retrieve vehicle incidents on a claim Retrieve vehicle incidents on a claim  |
| GetContactRolesConstraints - All contact role constraints list | All contact role constraints list All contact role constraints list  |
| GetDocumentContent - The contents of a document | The contents of a document The contents of a document  |
| GetDwellingIncidents - Retrieve dwelling incidents on a claim | Retrieve dwelling incidents on a claim Retrieve dwelling incidents on a claim  |
| GetLivingExpensesIncidents - Retrieve living expense incidents on a claim | Retrieve living expense incidents on a claim Retrieve living expense incidents on a claim  |
| GetLocationBasedRiskUnits - The location based risk units of a policy associat... | The location based risk units of a policy associated with a given claim The location based risk units of a policy associated with a given claim  |
| GetPolicy - The policy associated with a given claim | The policy associated with a given claim The policy associated with a given claim  |
| GetPolicyCoverages - The policy level coverages associated with a given... | The policy level coverages associated with a given claim The policy level coverages associated with a given claim  |
| GetPolicyEndorsements - The policy endorsements of a given claim | The policy endorsements of a given claim The policy endorsements of a given claim  |
| GetPolicyLocations - The locations associated with a given claim's poli... | The locations associated with a given claim's policy The locations associated with a given claim's policy  |
| GetServiceRequestInvoices - Retrieves the invoices on a service request | Retrieves the invoices on a service request Retrieves the invoices on a service request  |
| GetServiceRequests - Retrieves service requests | Retrieves a list of assigned service requests Retrieves service requests  |
| GetServiceRequestsForClaim - Returns a list of service requests associated with... | Returns a list of service requests associated with the claim Returns a list of service requests associated with the claim  |
| GetUnverifiedLocationBasedRiskUnitCoverages - Retrieves a list of coverages on a location based ... | Retrieves a list of coverages on a location based risk unit Retrieves a list of coverages on a location based risk unit  |
| GetUnverifiedLocationBasedRiskUnits - Retrieves a list of location based risk units | Retreives a list of location based risk units Retrieves a list of location based risk units  |
| GetUnverifiedPolicies - Get all unverified policies | Get all unverified policies Get all unverified policies  |
| GetUnverifiedPolicyCoverages - Retrieves a list of policy coverages on an unverif... | Retrieves a list of policy coverages on an unverified policy Retrieves a list of policy coverages on an unverified policy  |
| GetUnverifiedPolicyLocations - Retrieves a list of policy locations | Retreives a list of policy locations Retrieves a list of policy locations  |
| GetUnverifiedVehicleRiskUnitCoverages - Retrieves a list of coverages on a vehicle risk un... | Retrieves a list of coverages on a vehicle risk unit Retrieves a list of coverages on a vehicle risk unit  |
| GetUnverifiedVehicleRiskUnits - Retrieves a list of vehicle risk units | Retreives a list of vehicle risk units Retrieves a list of vehicle risk units  |
| GetVehicleRiskUnits - The vehicle risk units of a policy associated with... | The vehicle risk units of a policy associated with a given claim The vehicle risk units of a policy associated with a given claim  |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| ChangeServiceRequest - Modifies an existing service request (PATCH) | Modifies an existing service request Modifies an existing service request  |
| PatchClaim - Update a claim (PATCH) | Update a claim Update a claim  |
| PatchClaimContact - Update the details of the specific contact on the ... (PATCH) | Update the details of the specific contact on the given claim Update the details of the specific contact on the given claim  |
| PatchClaimDocument - Update a document on the given claim (PATCH) | Update a document on the given claim Update a document on the given claim  |
| PatchDwellingIncident - Update a dwelling incident (PATCH) | Update a dwelling incident Update a dwelling incident  |
| PatchExposure - Update the details of the specific exposure on the... (PATCH) | Update the details of the specific exposure on the given claim Update the details of the specific exposure on the given claim  |
| PatchFixedPropertyIncident - Update the details of a specific fixed property in... (PATCH) | Update the details of a specific fixed property incident Update the details of a specific fixed property incident  |
| PatchInjuryIncident - Update the details of a specific injury incident (PATCH) | Update the details of a specific injury incident Update the details of a specific injury incident  |
| PatchLivingExpensesIncident - Update a living expense incident (PATCH) | Update a living expense incident Update a living expense incident  |
| PatchUnverifiedLocationBasedRiskUnit - Update a location based risk unit (PATCH) | Update a location based risk unit Update a location based risk unit  |
| PatchUnverifiedLocationBasedRiskUnitCoverage - Update a coverage on a location based risk unit (PATCH) | Update a coverage on a location based risk unit Update a coverage on a location based risk unit  |
| PatchUnverifiedPolicy - Update an unverified policy (PATCH) | Update an unverified policy Update an unverified policy  |
| PatchUnverifiedPolicyCoverage - Update a coverage on an unverified policy (PATCH) | Update a coverage on an unverified policy Update a coverage on an unverified policy  |
| PatchUnverifiedPolicyLocation - Update a location on a policy (PATCH) | Update a location on a policy Update a location on a policy  |
| PatchUnverifiedVehicleRiskUnit - Update a vehicle risk unit (PATCH) | Update a vehicle risk unit Update a vehicle risk unit  |
| PatchUnverifiedVehicleRiskUnitCoverage - Update a coverage on a vehicle risk unit (PATCH) | Update a coverage on a vehicle risk unit Update a coverage on a vehicle risk unit  |
| PatchVehicleIncident - Update a vehicle incident (PATCH) | Update a vehicle incident Update a vehicle incident  |


