# Elite 3E API
The 3E API provides low level access to the 3E Financial System application logic layer for the decoupled 3E User Interface. For more info please click https://developerportal.thomsonreuters.com/3e-api

# Connection Tab
## Connection Fields


#### Server URL

The URL for the service server

**Type** - string



#### Elite 3E API REST API Service URL

The URL for the Elite 3E API REST API Service server.

**Type** - string



#### Alternate Swagger URL

This will override the default swagger file embedded in the connector so you can provide a url to any swagger file.

**Type** - string



#### AuthenticationType

Allows user to select between BASIC and OAUTH 2.0 Authentication

**Type** - string

**Default Value** - BASIC

##### Allowed Values

 * Basic
 * OAuth 2.0


#### User Name

User name for Basic Authentication. Leave blank for other authenticate types.

**Type** - string



#### Password

Password for Basic Authentication. Leave blank for other authenticate types.

**Type** - password



#### OAuth 2.0

The OAuth 2.0 tab provides settings for 3 options: Authorization Code, Resource Owner Credentials and Client Credentials. Select the option use by your API provider

**Type** - oauth

# Operation Tab


## CREATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## UPDATE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## GET
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## DELETE
### Operation Fields


##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string



## QUERY
### Operation Fields


##### Page Size

Specifies the number of documents to retrieve with each page transaction.

**Type** - integer

**Default Value** - 20



##### Maximum Documents

Limits the number of documents returned. If value is less than 1, all records are returned.

**Type** - integer

**Default Value** - -1



##### Extra URL Query Parameters

Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

**Type** - string


### Query Options


#### Fields

Use the checkboxes in the *Fields* list to select which fields are returned by the Query operation. Selecting only the fields required can improve performance.


#### Filters

 The query filter supports any arbitrary grouping and nesting of AND's and OR's.

Example:
((foo lessThan 30) OR (baz lessThan 42) OR ((bar isNull) AND (bazz isNotNull))) AND (buzz greaterThan 55)

#### Filter Operators

 * Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Equal To  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Greater Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Less Than Or Equal  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Between 2 comma separated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Between 2 comma seperated values  (Supported Types:  date, number)  **helpText NOT SET IN DESCRIPTOR FILE**

 * In a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not in a comma delimited list  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Is Not Null  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**

 * Not Like (% wildcard, case insensitive)  (Supported Types:  string)  **helpText NOT SET IN DESCRIPTOR FILE**



#### Sorts



The sort order can be set to either ascending and descinding.
# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **Extra URL Query Parameters** - Extra URL Query Parameters are appended to the http URL used to execute an API transaction. Do not include a leading ? nor &. For example: extra1=value1&extra2=&value2

# Outbound Document Properties
The connector does not support outbound document properties that can be read by a process after a connector shape.


# 


# Operations and Object Types Provided

### CREATE Operation Types
| Label | Help Text |
| --- | --- |
| AnonymousPrint ProcessPrintJobStatusUpdate - Processes the status update for the specified prin... | AnonymousPrint Processes the status update for the specified print job.  |
| Attachment UploadDMSAttachment - Uploads DMS Attachment. | Attachment Uploads DMS Attachment.  |
| Attachment UploadFileAttachment - Uploads File Attachment. | Attachment Uploads File Attachment.  |
| Attachment UploadICAttachment - Uploads IC Attachment. | Attachment Uploads IC Attachment.  |
| Client CreateClient - Creates a new Client. | Client Creates a new Client.  |
| Client ValidateClients - Validates one or more existing Clients. | Client Validates one or more existing Clients.  |
| Cost CreatePendingCostCard - Creates a new PendingCostCard. | Cost Creates a new PendingCostCard.  |
| Cost CreatePostedCostCard - Creates a new CostCard. | Cost Creates a new CostCard.  |
| Cost ValidatePendingCostCards - Validates one or more existing PendingCostCards. | Cost Validates one or more existing PendingCostCards.  |
| Cost ValidatePostedCostCards - Validates one or more existing CostCards. | Cost Validates one or more existing CostCards.  |
| Data BatchAdd - Adds list of ItemIDs to Process Worklist. | Data Adds list of ItemIDs to Process Worklist.  |
| Data CreateParametersContext - Creates a context for parameters data. | Data Creates a context for parameters data.  |
| Data Expand - Executes OnExpand on specified object and returns ... | Data Executes OnExpand on specified object and returns new state.  |
| Data GlobalReplace - Global replace operation to replace all attibute v... | Data Global replace operation to replace all attibute values with in process.  |
| Data PostStateAndGetData - Allows patching state and retrieving data in one r... | Data Allows patching state and retrieving data in one request.  |
| Entity CloneEntity - Clones one or more Entities. | Entity Clones one or more Entities.  |
| Entity CreateOrganization - Creates a new Entity Organization. | Entity Creates a new Entity Organization.  |
| Entity CreatePerson - Creates a new Entity Person. | Entity Creates a new Entity Person.  |
| File Email - Emails the specified attachment to the specified a... | File Emails the specified attachment to the specified addresses.  |
| File Upload - Uploads a file with streaming. The request Body sh... | File Uploads a file with streaming. The request Body should include the file content as multipart-form data.  |
| Find ConvertToXOQL - Converts SJQL select to XOQL. | Find Converts SJQL select to XOQL.  |
| Find LookupAdvanced - Advanced find search for an archetype. | Find Advanced find search for an archetype.  |
| Find LookupQuickFind - Performas quick find for lookup. | Find Performas quick find for lookup.  |
| Find QueryInfo - Gets the query information for the specified objec... | Find Gets the query information for the specified object.  |
| Find SJQLQuery - Gets a result set for a SJQL object. | Find Gets a result set for a SJQL object.  |
| Find WorklistAdvanced - Advanced find search for a page. | Find Advanced find search for a page.  |
| Find WorklistQuickFind - Performs quick find for worklist. | Find Performs quick find for worklist.  |
| GenericDataObject CreateDataObject - Creates a new data object. | GenericDataObject Creates a new data object.  |
| GenericDataObject GetLookupData - Gets a lookup for a data object. | GenericDataObject Gets a lookup for a data object.  |
| GenericDataObject ValidateDataObjects - Validates one or more existing Data Objects. | GenericDataObject Validates one or more existing Data Objects.  |
| LookupList Post - Gets the list of values to be shown in a dropdown ... | LookupList Gets the list of values to be shown in a dropdown for an archetype that has IsBigList property set to False.  |
| Matter CreateMatter - Creates a new Matter. | Matter Creates a new Matter.  |
| Matter CreateMatterNickname - Creates a new Matter Nickname. | Matter Creates a new Matter Nickname.  |
| Matter CreateTempMatter - Creates a new Temp Matter. | Matter Creates a new Temp Matter.  |
| Matter ReplaceTempMatter - Replaces a Temp Matter with an actual matter. | Matter Replaces a Temp Matter with an actual matter.  |
| Matter ValidateMatters - Validates one or more existing Matters. | Matter Validates one or more existing Matters.  |
| Metadata Properties - Creates the the appobject specified. | Metadata Creates the the appobject specified.  |
| Presentation ExecuteActionForObject - Executes report action in report view using object... | Presentation Executes report action in report view using object id and E3E.API.Presentation.Models.Action.ReportActionRequest.  |
| Presentation ExecuteActionForPresentation - Executes report action in report view using presen... | Presentation Executes report action in report view using presentation id and E3E.API.Presentation.Models.Action.ReportActionRequest.  |
| Presentation ExportForObject - Exports report data in report view using object id... | Presentation Exports report data in report view using object id and E3E.API.Presentation.Models.ReportDataRequest.  |
| Presentation ExportForPresentation - Exports presentation data in report view using pre... | Presentation Exports presentation data in report view using presentation id and E3E.API.Presentation.Models.ReportDataRequest.  |
| Presentation GenerateSaveAsPresentationMetadata - Gets a metadata required for save a new presentait... | Presentation Gets a metadata required for save a new presentaiton based on existing state.  |
| Presentation GenerateSavePresentationMetadata - Gets a metadata required for update presentaiton b... | Presentation Gets a metadata required for update presentaiton based on existing state.  |
| Presentation GetMetricDataForObject - Gets presentation data in metric view using object... | Presentation Gets presentation data in metric view using object id and E3E.API.Presentation.Models.MetricDataRequest.  |
| Presentation GetMetricDataForPresentation - Gets presentation data in metric view using presen... | Presentation Gets presentation data in metric view using presentation id and E3E.API.Presentation.Models.MetricDataRequest.  |
| Presentation GetReportDataForObject - Gets presentation data in report view using object... | Presentation Gets presentation data in report view using object id and E3E.API.Presentation.Models.ReportDataRequest.  |
| Presentation GetReportDataForPresentation - Gets presentation data in report view using presen... | Presentation Gets presentation data in report view using presentation id and E3E.API.Presentation.Models.ReportDataRequest.  |
| Presentation PrintForObject - Prints report data in report view using object id ... | Presentation Prints report data in report view using object id and E3E.API.Presentation.Models.ReportDataRequest.  |
| Presentation PrintForPresentation - Prints presentation data in report view using pres... | Presentation Prints presentation data in report view using presentation id and E3E.API.Presentation.Models.ReportDataRequest.  |
| Presentation SaveAsPresentation - Generates a new presentaiton. | Presentation Generates a new presentaiton.  |
| Print Email - Emails the output file to the specified addresses. | Print Emails the output file to the specified addresses.  |
| Print GetStatus - Gets the statuses for the specified print jobs. | Print Gets the statuses for the specified print jobs.  |
| Print Update - Updates or adds templates for NxPrinterTemplate. | Print Updates or adds templates for NxPrinterTemplate.  |
| Process Cancel - Cancels the process instance defined by the Proces... | Process The request header must contain E3E.API.Hosting.HeaderNames.ProcessItemId for the process instance to be cancelled. Cancels the process instance defined by the ProcessItemId in the E3E.API.Hosting.HeaderNames.ProcessItemId request header.  |
| Process Export - Exports the whole process data. | Process Exports the whole process data.  |
| Process Print - Prints the active record of process data. | Process Prints the active record of process data.  |
| SampleApp CloneClient - Creates new client(s) by cloning the given client. | SampleApp Creates new client(s) by cloning the given client.  |
| SampleApp CloneCostcard - Creates new costcard(s) by cloning the given costc... | SampleApp Creates new costcard(s) by cloning the given costcard.  |
| SampleApp CloneMatter - Creates new matter(s) by cloning the given matter. | SampleApp Creates new matter(s) by cloning the given matter.  |
| SampleApp CloneTimecard - Creates new timecard(s) by cloning the given timec... | SampleApp Creates new timecard(s) by cloning the given timecard.  |
| SampleApp CloneTimekeeper - Creates new Timekeeper(s) by cloning the given Tim... | SampleApp Creates new Timekeeper(s) by cloning the given Timekeeper.  |
| SampleApp CreateClient - Creates new client. | SampleApp Creates new client.  |
| SampleApp CreateEntityPerson - Creates new Entity Person from scratch. | SampleApp Creates new Entity Person from scratch.  |
| SampleApp CreateFullMatter - Creates new matter from scratch. | SampleApp Creates new matter from scratch.  |
| SampleApp CreateMatter - Creates new matter from scratch. | SampleApp Creates new matter from scratch.  |
| SampleApp CreateOrganizationPerson - Creates new Entity Organization from scratch. | SampleApp Creates new Entity Organization from scratch.  |
| SampleApp CreateTimecard - Creates new timecard(s). | SampleApp Creates new timecard(s).  |
| SampleApp CreateTimekeeper - Creates new Timekeeper from scratch. | SampleApp Creates new Timekeeper from scratch.  |
| Scheduler Start - Starts scheduler. | Scheduler Starts scheduler.  |
| Scheduler Stop - Stops scheduler. | Scheduler Stops scheduler.  |
| ServerPage Action - Executes an action for a server page instance. | ServerPage Executes an action for a server page instance.  |
| ServerPage Print - Prints a server page instance. | ServerPage Prints a server page instance.  |
| ServerPage Renew - Renews the lease of the state of a server page ins... | ServerPage Renews the lease of the state of a server page instance so it stays in server side cache for longer. The UI should call this if the server page is still being used and before the expiration date and time of the cache. Doing so will mitigate the need to send all the state back to the server.  |
| Session Post - Creates a new session for the currently authentica... | Session Creates a new session for the currently authenticated user.  |
| SpellCheck Info - Performs info generation and sort for the attbiute... | SpellCheck Performs info generation and sort for the attbiutes.  |
| SpellCheck Post - Performs a spell check for the specified request. | SpellCheck Performs a spell check for the specified request.  |
| Time AddPendingTimecard - Add a new pending timecard with default values. | Time Add a new pending timecard with default values.  |
| Time ClearTimecapturePostExceptions - Cleaars timecapture post exceptions. | Time Cleaars timecapture post exceptions.  |
| Time ClonePendingTimecards - Clones one or more existing pending timecards. | Time Clones one or more existing pending timecards.  |
| Time ClonePostedTimecards - Clones one or more existing posted timecards. | Time Clones one or more existing posted timecards.  |
| Time ClonePostedTimecardsAsPendingTimecards - Clones one or more existing posted timecards and c... | Time Clones one or more existing posted timecards and creates one or more pending timecards from them.  |
| Time CloneTimecaptureCard - Clones one or more existing timecapture records. | Time Clones one or more existing timecapture records.  |
| Time CreatePendingTimecard - Creates a pending timecard. | Time Creates a pending timecard.  |
| Time CreatePostedTimecard - Creates a posted timecard. | Time Creates a posted timecard.  |
| Time CreateTimeCaptureCard - Creates a new timecapture record. | Time Creates a new timecapture record.  |
| Time CreateTimecaptureCardFromPosted - Clones one or more existing posted timecards and c... | Time Clones one or more existing posted timecards and creates one or more timecapture records from them.  |
| Time CreateTimeCaptureModel - Creates a TimeCapture model. | Time Creates a TimeCapture model.  |
| Time PostPendingTimecards - Posts one or more existing pending timecards. | Time Posts one or more existing pending timecards.  |
| Time QueryTimeCaptureAllCards - Queries timecapture records (both posted and pendi... | Time Queries timecapture records (both posted and pending) filtered out according to request body conditions.  |
| Time QueryTimeCapturePendingCards - Queries timecapture records (pending only) filtere... | Time Queries timecapture records (pending only) filtered out according to request body conditions.  |
| Time StartStopTimer - Starts/stops a timer for a given pending timecard. | Time Starts/stops a timer for a given pending timecard.  |
| Time ValidatePendingTimecards - Validates one or more existing pending timecards. | Time Validates one or more existing pending timecards.  |
| Time ValidatePostedTimecards - Validates one or more existing timecards. | Time Validates one or more existing timecards.  |
| Time ValidateTimecaptureCard - Validates one or more existing timecapture records... | Time Validates one or more existing timecapture records.  |
| Timekeeper CreateTimekeeper - Creates a new Timekeeper. | Timekeeper Creates a new Timekeeper.  |
| Timekeeper ValidateTimekeepers - Validates one or more existing Timekeepers. | Timekeeper Validates one or more existing Timekeepers.  |
| Workflow Email - Sends email notification for all specified workflo... | Workflow Sends email notification for all specified workflows.  |
| Workflow GetUserOpenWorkflows - Gets all open workflows for a user and returns a W... | Workflow Gets all open workflows for a user and returns a WorkflowUserResponse.  |
| Workflow Open - Opens the given workflow process. | Workflow Opens the given workflow process.  |
| Workflow Reassign - Reassignes the given workflows to the given user/r... | Workflow Reassignes the given workflows to the given user/role.  |
| Workflow Terminate - Terminates all specified workflows. | Workflow Terminates all specified workflows.  |


### DELETE Operation Types
| Label | Help Text |
| --- | --- |
| Cache Clear - Clears the entire cache. | Cache Clears the entire cache.  |
| Dashboard Delete - Deletes the specified dashboard. | Dashboard Deletes the specified dashboard.  |
| Dashboard DeleteOverrides - Deletes the dashboard overrides of the current use... | Dashboard Deletes the dashboard overrides of the current user for the specified dashboard.  |
| Data Delete - Deletes the row in the specified path. | Data Deletes the row in the specified path.  |
| Data DeleteModel - Deletes the specified model. | Data Deletes the specified model.  |
| Data DeleteParametersContext - Deletes the context for parameters data. | Data Deletes the context for parameters data.  |
| Entity DeleteEntity - Deletes one or more existing Entity. | Entity Deletes one or more existing Entity.  |
| File Delete - Deletes an uploaded file. | File Deletes an uploaded file.  |
| GenericDataObject DeleteDataObjects - Deletes one or more data objects. | GenericDataObject Deletes one or more data objects.  |
| Matter DeleteMatterNickname - Deletes one or more existing matter nicknames. | Matter Deletes one or more existing matter nicknames.  |
| Matter DeleteTempMatter - Deletes the specified Temp Matters. | Matter Deletes the specified Temp Matters.  |
| Me RemoveFromFavorites - Removes an app object from the current user's favo... | Me Removes an app object from the current user's favorites.  |
| Me RemoveRecordFromFavorites - Removes a Record from the current user's favorites... | Me Removes a Record from the current user's favorites.  |
| Messages ResetCustomMessage - Deletes custom messages for the field. | Messages Deletes custom messages for the field.  |
| Metadata DeleteAppObject - Deletes the the appobject specified. | Metadata Deletes the the appobject specified.  |
| Metadata ResetQueryLookupGridOverride - Deletes old models and returns QueryLookupGrid lat... | Metadata Deletes old models and returns QueryLookupGrid latest model regarding to scope.  |
| Metadata RestoreAppObject - Restores the the appobject specified. | Metadata Restores the the appobject specified.  |
| Presentation Delete - Deletes presentation. | Presentation Deletes presentation.  |
| Print Delete - Deletes the specified print job. | Print Deletes the specified print job.  |
| Print DeleteAll - Deletes all the print jobs for the current user. | Print Deletes all the print jobs for the current user.  |
| Print DeleteSelected - Deletes the specified print jobs. | Print Deletes the specified print jobs.  |
| ServerPage Delete - Deletes an instance of the server page from any as... | ServerPage Deletes an instance of the server page from any associated cache. The UI should call this once it is done with the server page.  |
| Time CleanupPendingTimecards - Cleans up pending timecards, so that only timecard... | Time Cleans up pending timecards, so that only timecards with works hours or timecards with work hours or narrative remain.  |
| Time DeletePendingTimecards - Deletes one or more existing pending timecards. | Time Deletes one or more existing pending timecards.  |
| Time DeleteTimeCaptureModel - Deletes a TimeCapture model. | Time Deletes a TimeCapture model.  |
| WatchList Delete - Deletes the specified item from the watch list. | WatchList Deletes the specified item from the watch list.  |


### GET Operation Types
| Label | Help Text |
| --- | --- |
| ActionList Get - Gathers information about the open processes for t... | ActionList Gathers information about the open processes for the current user.  |
| ActionList GetExtended - Gathers extended information about the open proces... | ActionList Gathers extended information about the open processes for the current user.  |
| Attachment DownloadDMSAttachment - Downloads a DMS attachment. | Attachment Downloads a DMS attachment.  |
| Attachment DownloadFileAttachment - Downloads a File attachment. | Attachment Downloads a File attachment.  |
| Attachment DownloadICAttachment - Downloads an IC attachment. | Attachment Downloads an IC attachment.  |
| Attachment GetAttachments - Gets Attachments and returns a AttachmentGetRespon... | Attachment Gets Attachments and returns a AttachmentGetResponse.  |
| Attachment GetDMSParameters - Collects DMS parameters and returns DMSParametersG... | Attachment Collects DMS parameters and returns DMSParametersGetResponse.  |
| Cache Get - Gathers items that currently exist in the cache. | Cache Gathers items that currently exist in the cache.  |
| Client GetClients - Gets Clients and returns a ClientGetResponse. | Client Gets Clients and returns a ClientGetResponse.  |
| Client GetClientSchema - Gets the schema for Client. | Client Gets the schema for Client.  |
| Client GetNewClient - Gets a new Client with default values. | Client This method does not launch a process or add any data in 3E. It is intended to be used with CreateClient. e.g. call this method, then set whichever attributes need to be changed and then call CreateClient with the modified data. Gets a new Client with default values.  |
| Client ModelFromClients - Gets cloned Clients and returns a ClientGetRespons... | Client Gets cloned Clients and returns a ClientGetResponse.  |
| Cost GetNewPendingCostCard - Gets a new PendingCostCard with default values. | Cost This method does not launch a process or add any data in 3E. It is intended to be used with CreateCostCard. e.g. call this method, then set whichever attributes need to be changed and then call CreateCostCard with the modified data. Gets a new PendingCostCard with default values.  |
| Cost GetNewPostedCostCard - Gets a new CostCard with default values. | Cost This method does not launch a process or add any data in 3E. It is intended to be used with CreateCostCard. e.g. call this method, then set whichever attributes need to be changed and then call CreateCostCard with the modified data. Gets a new CostCard with default values.  |
| Cost GetPendingCostCards - Gets PendingCostCards and returns a CostCardGetRes... | Cost Gets PendingCostCards and returns a CostCardGetResponse.  |
| Cost GetPendingCostCardSchema - Gets the schema for PendingCostCard. | Cost Gets the schema for PendingCostCard.  |
| Cost GetPostedCostCards - Gets CostCards and returns a CostCardGetResponse. | Cost Gets CostCards and returns a CostCardGetResponse.  |
| Cost GetPostedCostCardSchema - Gets the schema for CostCard. | Cost Gets the schema for CostCard.  |
| Cost ModelFromPendingCostCards - Gets cloned PendingCostCards and returns a CostCar... | Cost Gets cloned PendingCostCards and returns a CostCardGetResponse.  |
| Cost ModelFromPostedCostCards - Gets cloned CostCards and returns a CostCardGetRes... | Cost Gets cloned CostCards and returns a CostCardGetResponse.  |
| Dashboard Get - Gathers information about the default dashboard fo... | Dashboard Gathers information about the default dashboard for the current user.  |
| Dashboard GetDashboard - Gathers information about the specified dashboard. | Dashboard Gathers information about the specified dashboard.  |
| Data Find - Executes Find on specified object and returns resp... | Data Executes Find on specified object and returns response.  |
| Data Get - Gets the data in the specified path for the curren... | Data Gets the data in the specified path for the current process.  |
| Data GetMessage - Gets the message data for the attribute in the spe... | Data Gets the message data for the attribute in the specified path.  |
| Data GetModelList - Loads existing process data models. | Data Loads existing process data models.  |
| Data GetState - Gets the current data state. | Data Gets the current data state.  |
| Data LockedInfo - Get LockedInfo for a locked object. | Data Get LockedInfo for a locked object.  |
| Entity GetAllEntities - Gets Entities and returns a EntityGetResponse. | Entity Gets Entities and returns a EntityGetResponse.  |
| Entity GetNewOrganization - Gets a new EntityOrg with default values. | Entity This method does not launch a process or add any data in 3E. It is intended to be used with CreateMatter. e.g. call this method, then set whichever attributes need to be changed and then call CreateEntityOrg with the modified data. Gets a new EntityOrg with default values.  |
| Entity GetNewPerson - Gets a new EntityPerson with default values. | Entity This method does not launch a process or add any data in 3E. It is intended to be used with CreateMatter. e.g. call this method, then set whichever attributes need to be changed and then call CreateEntityPerson with the modified data. Gets a new EntityPerson with default values.  |
| Entity GetOrganizations - Gets EntityOrganizations and returns a EntityGetRe... | Entity Gets EntityOrganizations and returns a EntityGetResponse.  |
| Entity GetOrganizationSchema - Gets the schema for Matter. | Entity Gets the schema for Matter.  |
| Entity GetPersons - Gets EntityPersons and returns a EntityGetResponse... | Entity Gets EntityPersons and returns a EntityGetResponse.  |
| Entity GetPersonSchema - Gets the schema for EntityPerson. | Entity Gets the schema for EntityPerson.  |
| Entity ModelFromEntities - Gets cloned Entities and returns a EntityGetRespon... | Entity Gets cloned Entities and returns a EntityGetResponse.  |
| ExpansionCode Get - Searches the expansion code in user, unit and syst... | ExpansionCode Searches the expansion code in user, unit and system expansion code lists and returns its value.  |
| File Download - Gets an uploaded file. | File Gets an uploaded file.  |
| File Get - Gets an uploaded file with a token. | File Gets an uploaded file with a token.  |
| Find AdvancedFindAddChildArchetypes - Gets the archetypes that can be added as a child a... | Find Gets the archetypes that can be added as a child archetype to an archetype.  |
| Find AdvancedFindAddChildAttributes - Gets the attributes that can be added as a child a... | Find Gets the attributes that can be added as a child attribute between two archetypes.  |
| Find AdvancedFindJoinAttributes - Gets the attributes that can be joined on between ... | Find Gets the attributes that can be joined on between two archetypes.  |
| Find LookupAdvancedAttributes - Gets the advanced attributes for an archetype. | Find Gets the advanced attributes for an archetype.  |
| Find WorklistAdvancedAttributes - Gets the advanced attributes for a process page. | Find Gets the advanced attributes for a process page.  |
| GenericDataObject GetDataObjects - Gets Data Objects and returns a DataObjectGetRespo... | GenericDataObject Gets Data Objects and returns a DataObjectGetResponse.  |
| GenericDataObject GetDataObjectSchema - Gets the schema for a data object. | GenericDataObject Gets the schema for a data object.  |
| GenericDataObject GetNewDataObject - Gets a new data object with default values. | GenericDataObject This method does not launch a process or add any data in 3E. It is intended to be used with the CreateDataObject method. e.g. call this method, then set whichever attributes need to be changed and then call CreateDataObject with the modified data. Gets a new data object with default values.  |
| GenericDataObject ModelFromDataObject - Gets cloned Data Objects and returns a DataObjectG... | GenericDataObject Gets cloned Data Objects and returns a DataObjectGetResponse.  |
| Image Get - Gets the image in the specified path. | Image Gets the image in the specified path.  |
| Info Get - Gathers information about the 3E server and underl... | Info Gathers information about the 3E server and underlying api, framework and managers.  |
| Languages Get - Gathers information about the list of supported la... | Languages Gathers information about the list of supported languages.  |
| LookupList Get - Gets the list of values to be shown in a dropdown ... | LookupList Gets the list of values to be shown in a dropdown for an archetype that has IsBigList property set to False.  |
| Matter GetMatterNicknames - Gets Matter Nicknames and returns a MatterNickname... | Matter Gets Matter Nicknames and returns a MatterNicknameGetResponse.  |
| Matter GetMatters - Gets Matters and returns a MatterGetResponse. | Matter Gets Matters and returns a MatterGetResponse.  |
| Matter GetMatterSchema - Gets the schema for Matter. | Matter Gets the schema for Matter.  |
| Matter GetNewMatter - Gets a new Matter with default values. | Matter This method does not launch a process or add any data in 3E. It is intended to be used with CreateMatter. e.g. call this method, then set whichever attributes need to be changed and then call CreateMatter with the modified data. Gets a new Matter with default values.  |
| Matter GetTempMatterNameList - Gets high level Temp Matter name list and returns ... | Matter Gets high level Temp Matter name list and returns a TempMatterGetResponse.  |
| Matter GetTempMatters - Gets Temp Matters and returns a TempMatterGetRespo... | Matter Gets Temp Matters and returns a TempMatterGetResponse.  |
| Matter ModelFromMatters - Gets cloned Matters and returns a MatterGetRespons... | Matter Gets cloned Matters and returns a MatterGetResponse.  |
| Me Get - Gathers information about the current 3E user. | Me Gathers information about the current 3E user.  |
| Me GetFavorites - Gathers information about the favorites for the cu... | Me Gathers information about the favorites for the current user.  |
| Me GetOptionsDefaultValues - Gets an options values for the current user. | Me Gets an options values for the current user.  |
| Me GetOptionValue - Gets an option value for the current user. | Me Gets an option value for the current user.  |
| Messages Get - Gathers the list of localized framework messages a... | Messages Gathers the list of localized framework messages and captions.  |
| Messages LoadCustomMessage - Get the list of custom messages regarding to msgId... | Messages Get the list of custom messages regarding to msgId.  |
| Metadata CheckPageIsParameterPage - Checks if selected page is a Parameter Page. | Metadata Checks if selected page is a Parameter Page.  |
| Metadata GenerateInterfaceData - Generates default interface data for the page spec... | Metadata Generates default interface data for the page specified.  |
| Metadata GenerateReportQuery - Generates default report query for the bound objec... | Metadata Generates default report query for the bound object specified.  |
| Metadata Get - Gets the metadata for the specified appobject type... | Metadata Gets the metadata for the specified appobject type and id.  |
| Metadata GetAppObjectInterfaceData - Gets the interface data for the appobject specifie... | Metadata Gets the interface data for the appobject specified.  |
| Metadata GetAppObjectMetadata - Gets the metadata for the appobject specified. | Metadata Gets the metadata for the appobject specified.  |
| Metadata GetAppObjectOverride - Gets the metadata override for the appobject and i... | Metadata Gets the metadata override for the appobject and item id specified.  |
| Metadata GetAppObjectPages - Gets the pages for the appobject specified. | Metadata Gets the pages for the appobject specified.  |
| Metadata GetAppObjectReportQueries - Gets the report queries for the appobject specifie... | Metadata Gets the report queries for the appobject specified.  |
| Metadata GetAvailableAttributes - Gets available form attributes for given objectId. | Metadata Gets available form attributes for given objectId.  |
| Metadata GetDefaultViews - Get the list of default view Updates the default v... | Metadata Get the list of default view Updates the default view of the baseobject  |
| Navigation Get - Gathers information about the navigation details f... | Navigation Gathers information about the navigation details for the current user.  |
| Presentation Get - Gathers information about the specified presentati... | Presentation Gathers information about the specified presentation.  |
| Presentation GetDefaultOptions - Returns default print options for report. | Presentation Returns default print options for report.  |
| Presentation GetPresentationOfObject - Gathers information about the generated presentati... | Presentation Gathers information about the generated presentation of the specified object.  |
| Print Cancel - Cancels the specified print job. | Print Cancels the specified print job.  |
| Print GetLog - Gets the logs for the specified print job separate... | Print Gets the logs for the specified print job separated by new line.  |
| Print GetPrinterProperties - Gets the properties for the specified printer. | Print Gets the properties for the specified printer.  |
| Print GetPrinterStatus - Gets the status of the specified printer. | Print Gets the status of the specified printer.  |
| Print GetPrintFile - Downloads and displays the output file with a prin... | Print Downloads and displays the output file with a print dialog for the specified print job.  |
| Print GetPrintJobs - Gets all print jobs for the current user and retur... | Print Gets all print jobs for the current user and returns a PrintJobResponse.  |
| Print GetSaveFile - Downloads and saves the output file for the specif... | Print Downloads and saves the output file for the specified print job.  |
| Print GetViewFile - Downloads and displays the output file for the spe... | Print Downloads and displays the output file for the specified print job.  |
| Print GetXML - Gets the content of Data XML file for the specifie... | Print Gets the content of Data XML file for the specified print job.  |
| Process Get - Loads a new process defined by the processId and r... | Process Loads a new process defined by the processId and returns ProcessResponse. Alternatively, if the processId refers to a guid, then loads an existing process defined by the process item id and returns ProcessResponse.  |
| Process GetConfiguration - Returns configuration settings for processes. | Process Returns configuration settings for processes.  |
| Process GetDefaultOptions - Returns default print options for process. | Process Returns default print options for process.  |
| Process GetFolderInfo - Creates and returns folder information of process. | Process Creates and returns folder information of process.  |
| Process GetProcessByProcessItemId - Returns current step in the process instance defin... | Process The request header must contain E3E.API.Hosting.HeaderNames.ProcessItemId for the process instance to be cancelled. Returns current step in the process instance defined by the ProcessItemId in the E3E.API.Hosting.HeaderNames.ProcessItemId request header.  |
| Process HasChanges - Returns if process has changes that are not commit... | Process Returns if process has changes that are not committed.  |
| Scheduler Get - Gets the current state of the scheduler on this se... | Scheduler Gets the current state of the scheduler on this server.  |
| Search Get - Generates a suggested search result based on the s... | Search Generates a suggested search result based on the supplied parameters request.  |
| ServerPage Compile - Compiles the specified server page. This should be... | ServerPage Compiles the specified server page. This should be called to see information about the compilation of the server page.  |
| ServerPage Get - Gets the current state of an existing instance of ... | ServerPage Gets the current state of an existing instance of a server page. The UI can call this if it wants the current state snapshot of the server page instance.  |
| ServerPage GetDefaultOptions - Returns default print options for process. | ServerPage Returns default print options for process.  |
| Session Get - Gathers information about the current session or a... | Session Gathers information about the current session or a specified session.  |
| Time GetActiveTimers - Gets active timers for a given timekeeper and retu... | Time Gets active timers for a given timekeeper and returns a ActiveTimersResponse.  |
| Time GetCalendarReport - Gets calendar data report for a given timekeeper a... | Time Gets calendar data report for a given timekeeper and period.  |
| Time GetNewPendingTimecard - Gets a new pending timecard with default values. | Time This method does not launch a process or add any data in 3E. It is intended to be used with CreateTimecardPending. e.g. call this method, then set whichever attributes need to be changed and then call CreateTimecardPending with the modified RootData. Gets a new pending timecard with default values.  |
| Time GetNewPostedTimecard - Gets a new posted timecard with default values. | Time This method does not launch a process or add any data in 3E. It is intended to be used with CreatePendingTimecard. e.g. call this method, then set whichever attributes need to be changed and then call CreateTimecardPosted with the modified RootData. Gets a new posted timecard with default values.  |
| Time GetNewTimeCaptureCard - Gets a new timecapture timecard with default value... | Time This method does not launch a process or add any data in 3E. It is intended to be used with CreateTimecardPending. e.g. call this method, then set whichever attributes need to be changed and then call CreateTimecardPending with the modified RootData. Gets a new timecapture timecard with default values.  |
| Time GetPendingTimecards - Gets pending timecards for a given timekeeper and ... | Time Gets pending timecards for a given timekeeper and returns a TimecardGetResponse.  |
| Time GetPendingTimecardSchema - Gets the schema for pending timecards. | Time Gets the schema for pending timecards.  |
| Time GetPostedTimecards - Gets posted timecards for a given timekeeper and r... | Time Gets posted timecards for a given timekeeper and returns a TimecardGetResponse.  |
| Time GetPostedTimecardSchema - Gets the schema for posted timecards. | Time Gets the schema for posted timecards.  |
| Time GetTimeCaptureAllCards - Gets timecapture records (both posted and pending)... | Time Gets timecapture records (both posted and pending) for a given timekeeper and returns a TimecardGetAllResponse.  |
| Time GetTimeCaptureModels - Gets TimeCapture models. | Time Gets TimeCapture models.  |
| Time GetTimeCapturePendingCards - Gets timecapture pending records for a given timek... | Time Gets timecapture pending records for a given timekeeper and returns a TimecardGetResponse.  |
| Time GetTimecards - Gets timecards (both posted and pending) for a giv... | Time Gets timecards (both posted and pending) for a given timekeeper and returns a TimecardGetAllResponse.  |
| Time GetTimecardsGroupedByDay - Gets pending and posted timecards grouped for disp... | Time The following attributes are always returned by default (in addition to anything specified in AttributesToInclude): WorkDate, TimePendIndex, TimeIndex, WorkHrs, IsNB, IsNoCharge, WorkType, TimeType, Office, Matter, Phase, Task, Activity, IsFlatFeeComplete, Phase2, Task2, Activity2. Gets pending and posted timecards grouped for display in weekly view.  |
| Time ModelFromPendingTimecards - Gets cloned pending timecards and returns a Timeca... | Time Gets cloned pending timecards and returns a TimecardGetResponse.  |
| Time ModelFromPostedTimecards - Gets cloned timecards and returns a TimecardGetRes... | Time Gets cloned timecards and returns a TimecardGetResponse.  |
| Time SpellcheckPendingTimecards - Spellchecks one or more existing pending timecards... | Time Spellchecks one or more existing pending timecards.  |
| Timekeeper GetNewTimekeeper - Gets a new Timekeeper with default values. | Timekeeper This method does not launch a process or add any data in 3E. It is intended to be used with CreateTimekeeper. e.g. call this method, then set whichever attributes need to be changed and then call CreateTimekeeper with the modified data. Gets a new Timekeeper with default values.  |
| Timekeeper GetTimekeepers - Gets Timekeepers and returns a TimekeeperGetRespon... | Timekeeper Gets Timekeepers and returns a TimekeeperGetResponse.  |
| Timekeeper GetTimekeeperSchema - Gets the schema for Timekeeper. | Timekeeper Gets the schema for Timekeeper.  |
| Timekeeper ModelFromTimekeepers - Gets cloned Timekeepers and returns a TimekeeperGe... | Timekeeper Gets cloned Timekeepers and returns a TimekeeperGetResponse.  |
| User GetUserActivity - Gets logged in users. | User Gets logged in users.  |
| WatchList Get - Gathers information about the watch list for the c... | WatchList Gathers information about the watch list for the current user.  |
| Workflow GetOpenAccess - Determines if the current user has access to open ... | Workflow Determines if the current user has access to open the process associated with given workflow.  |
| Workflow GetOpenWorkflows - Gets all workflows for all users and returns a Wor... | Workflow Gets all workflows for all users and returns a WorkflowResponse.  |
| Workflow GetTreeView - Gets a tree structure of workflows for provided us... | Workflow Gets a tree structure of workflows for provided user and returns a TreeViewResponse.  |
| Workflow GetWorkflowSummary - Gets workflow summary for a current user and retur... | Workflow Gets workflow summary for a current user and returns a WorkflowSummaryResponse.  |


### QUERY Operation Types
| Label | Help Text |
| --- | --- |


### UPDATE Operation Types
| Label | Help Text |
| --- | --- |
| Client UpdateClient - Updates a Client. (PATCH) | Client Updates a Client.  |
| Cost UpdatePendingCostCard - Updates a PendingCostCard. (PATCH) | Cost Updates a PendingCostCard.  |
| Cost UpdatePostedCostCard - Updates a CostCard. (PATCH) | Cost Updates a CostCard.  |
| Dashboard Create - Creates a new dashboard. (PUT) | Dashboard Creates a new dashboard.  |
| Dashboard Patch - Modifies the dashboard panels and saves the dashbo... (PATCH) | Dashboard Modifies the dashboard panels and saves the dashboard.  |
| Dashboard PatchOverrides - Saves the partial changes to the dashboard metadat... (PATCH) | Dashboard Saves the partial changes to the dashboard metadata as user overrides.  |
| Data ApplyModel - Creates and adds new data object to process using ... (POST) | Data Creates and adds new data object to process using the specified model id.  |
| Data CreateModel - Creates a new model from the selected process work... (POST) | Data Creates a new model from the selected process worklist item.  |
| Data CreateModelFromDB - Creates and adds new data object to process, by us... (POST) | Data Creates and adds new data object to process, by using selected row from query window.  |
| Data EditModel - Patches the specifed model. (PATCH) | Data Patches the specifed model.  |
| Data ExecuteAction - Executes the specified interface action in the dat... (POST) | Data Executes the specified interface action in the data for the specified path in the current process.  |
| Data Patch - Patches the data with the specified payload. (PATCH) | Data The request header must contain E3E.API.Hosting.HeaderNames.ProcessItemId if data exists in a process context. It's not required for presentation or dashboard context. Patches the data with the specified payload.  |
| Data PatchState - Patches the state with the specified payload. (PATCH) | Data Patches the state with the specified payload.  |
| Data PutMessage - Creates or updates the message data for the attrib... (PUT) | Data Creates or updates the message data for the attribute in the specified path.  |
| Data UpdateParametersContext - Updates the context for parameters data. (PUT) | Data Updates the context for parameters data.  |
| Entity UpdateEntity - Updates an Entity. (PATCH) | Entity Updates an Entity.  |
| GenericDataObject UpdateDataObject - Updates a Data Object. (PATCH) | GenericDataObject Updates a Data Object.  |
| Matter UpdateMatter - Updates a Matter. (PATCH) | Matter Updates a Matter.  |
| Matter UpdateMatterNickname - Updates a Matter Nickname. (PATCH) | Matter Updates a Matter Nickname.  |
| Me AddRecordToFavorites - Adds a Record to the current user's favorites. (PUT) | Me Adds a Record to the current user's favorites.  |
| Me AddToFavorites - Adds an app object to the current user's favorites... (PUT) | Me Adds an app object to the current user's favorites. Optionally add a Record.  |
| Me ModifyFavoriteItem - Modifies a Favorite Item. (PATCH) | Me Modifies a Favorite Item.  |
| Me SetOptionsValues - Sets an options values for the current user. (PUT) | Me Sets an options values for the current user.  |
| Messages SaveCustomMessage - Saves/Updates custom messages for the field. (PUT) | Messages Saves/Updates custom messages for the field.  |
| Metadata SaveAppObject - Saves or updates the the appobject specified. (PUT) | Metadata Saves or updates the the appobject specified.  |
| Metadata SaveAppObjectOverride - Saves or updates the the appobject override specif... (PUT) | Metadata Saves or updates the the appobject override specified.  |
| Metadata SaveAsAppObject - Creates the the appobject specified. (POST) | Metadata Creates the the appobject specified.  |
| Metadata SaveQueryLookupGridOverride - Saves or updates the the appobject override specif... (PUT) | Metadata Saves or updates the the appobject override specified.  |
| Metadata UpdateDefaultView - Updates the default view of the baseobject (PUT) | Metadata Updates the default view of the baseobject  |
| Presentation GetPresentationWithParameterSet - Gathers information about the specified presentati... (POST) | Presentation Gathers information about the specified presentation and overrides parameter set if provided.  |
| Presentation SavePresentation - Updates an existing presentaiton. (PUT) | Presentation Updates an existing presentaiton.  |
| Process ExecuteOutput - Executes the specified output in the process insta... (POST) | Process The request header must contain E3E.API.Hosting.HeaderNames.ProcessItemId for the process instance to be cancelled. Executes the specified output in the process instance defined by the ProcessItemId in the E3E.API.Hosting.HeaderNames.ProcessItemId request header.  |
| Process SetConfiguration - Returns configuration settings for processes. (PUT) | Process Returns configuration settings for processes.  |
| Process UpdateFolderInfo - Updates process folder information. (PATCH) | Process Updates process folder information.  |
| ServerPage Post - Creates a new instance of a specified server page.... (POST) | ServerPage Creates a new instance of a specified server page. The UI should call this when a new server page instance is requested.  |
| ServerPage Put - Persists the entire state of a server page instanc... (PUT) | ServerPage Persists the entire state of a server page instance. The UI should call this when it recieves a NotFound response from any of the other endpoints as the state may have expired and remove from server side cache.  |
| Session Proxy - Gathers information about the current 3E user. (POST) | Session Gathers information about the current 3E user.  |
| SpellCheck SpellCheckUserDictionaryPatch - Executes the specified action in request and retur... (PATCH) | SpellCheck Executes the specified action in request and returns latest user dictionary list.  |
| Time ApplyTimeCaptureModel - Applies a TimeCapture model. (PUT) | Time Applies a TimeCapture model.  |
| Time UpdatePendingTimecard - Updates a pending timecard. (PATCH) | Time Updates a pending timecard.  |
| Time UpdatePostedTimecard - Updates a posted timecard. (PATCH) | Time Updates a posted timecard.  |
| Time UpdateTimeCaptureCard - Updates an existing timecapture record. (PATCH) | Time Updates an existing timecapture record.  |
| Time UpdateTimeCaptureModel - Updates a TimeCapture model. (PATCH) | Time Updates a TimeCapture model.  |
| Timekeeper UpdateTimekeeper - Updates a Timekeeper. (PATCH) | Timekeeper Updates a Timekeeper.  |


